//----------------------------------------------------------------------------
// Copyright (C) 2003  Rafael H. Bordini and Jomi F. Hubner
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// To contact the authors:
// http://www.inf.ufrgs.br/~bordini
// http://www.das.ufsc.br/~jomi
//
//----------------------------------------------------------------------------
package common;
import java.util.logging.Level;
import java.util.logging.Logger;

import agent.type.arch.SuccessRecorderArch;
import jason.JasonException;
import jason.infra.centralised.CentralisedAgArch;
import jason.infra.centralised.RunCentralisedMAS;
import jason.mas2j.AgentParameters;

/**
 * Extends centralized case to use multiagentarch as the default instead of centralizedarch; this
 * is required in order to intercept activity execution results before the TS grabs them and does 'stuff'.
 */
public class RunCampBDIMAS extends RunCentralisedMAS {

	private static Logger logger = Logger.getLogger(RunCampBDIMAS.class.getName());


	public static void main(String[] args) throws JasonException {
		runner = new RunCampBDIMAS();
		runner.init(args);
		runner.create();
		runner.start();
		runner.waitEnd();
		runner.finish();
	}

	@Override
	public void createAgs() throws JasonException {
		logger.setLevel(Level.INFO);
		logger.info("Agents; " + super.getProject().getAgents());
		// create the agents
		for (AgentParameters ap : super.getProject().getAgents()) {
			String agName = ap.name;
			for (int cAg = 0; cAg < ap.qty; cAg++) {
				String numberedAg = agName;
				if (ap.qty > 1) {
					numberedAg += (cAg + 1);
				}
				logger.severe("Creating agent " + numberedAg + " (" + (cAg + 1) + "/" + ap.qty + ")");
				CentralisedAgArch agArch= null;
				//This is the bit we added/edited...
				logger.severe("Create new arch for " + agName);
				agArch = new SuccessRecorderArch();
				agArch.setAgName(numberedAg);
				logger.finer("Set name " + numberedAg);
				agArch.setEnvInfraTier(super.getEnvironmentInfraTier());
				logger.finer("Create archs;\n" + ap.getAgArchClasses() + "\n" + 
						ap.agClass.getClassName() + "\n" + ap.getBBClass() + "\n" +
						ap.asSource.toString());
				agArch.createArchs(	ap.getAgArchClasses(), 
						ap.agClass.getClassName(), 
						ap.getBBClass(), 
						ap.asSource.toString(), 
						ap.getAsSetts(super.isDebug(), super.getProject().getControlClass() != null), 
						this);
				addAg(agArch);
			}
		}
	}

	@Override
	public CentralisedAgArch getAg(String name){
		CentralisedAgArch rt = super.getAg(name);
		logger.log(Level.SEVERE, "getAg returning\n" + rt + "\n" + rt.getClass().getCanonicalName());
		return rt;
	}
}
