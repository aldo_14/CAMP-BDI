/**
 * 
 */
package common;

import jason.asSyntax.ASSyntax;
import jason.asSyntax.Atom;

import java.util.logging.Level;

/**
 * @author Alan White
 *
 * This class defines <b>constant</b> values common to both simulator/environment and agent implementation.  Note
 * that these values would be otherwise duplicated - this is not a tool to ensure that variable values are kept
 * consistent across agent and controlled entity.
 */
public class UniversalConstants {
	/**
	 * Annotation added to literals which equate to an object type; i.e. vehicle(A)[object]
	 */
	public static final Atom OBJECT_DEF_ANNOT = ASSyntax.createAtom("objectType");

	public static final Level 	ARCH_LOGGER_LEVEL 				= Level.INFO,
								AGENT_LOGGER_LEVEL 				= Level.INFO,
								BB_LOGGER_LEVEL 				= Level.OFF,
								PLANNER_LOGGER_LEVEL 			= Level.SEVERE,
								ENV_LOGGER_LEVEL				= Level.FINER, //set to INFO for result messages for actions...
								CAPABILITY_LOGGER_LEVEL 		= Level.OFF,
								CAP_SRVC_LOGGER_LEVEL 			= Level.OFF,
								INTERNAL_ACTION_LOGGER_LEVEL 	= Level.OFF,
								ACTOR_LOGGER_LEVEL 				= Level.OFF,
								UTILS_LOGGER_LEVEL				= Level.OFF,
								FACTORY_LOGGER_LEVEL			= Level.OFF,
								STATS_LOGGER_LEVEL 				= Level.OFF,
								FAILURE_CHANCE_LOGGER_LEVEL 	= Level.FINE,
								MP_LOGGING_LEVEL				= Level.OFF,
								MT_LOGGING_LEVEL				= Level.OFF,
								SIM_LOGGER_LEVEL 				= Level.INFO;

	/**
	 * Configures whether or not any debug-specific stuff is done.
	 */
	public static boolean DEBUG = false;
	
	/**
	 * Configures whether or not debug-related agent uis are displayed
	 */
	public static final boolean SHOW_AGENT_UI = false;
	
	/**
	 * Set this to true for some step-by-step demo output in form of dialog windows...
	 */
	public static boolean DEMO_MODE = false;
	
	/**
	 * If true, then we show a dialog for every result or failure recorded by an agent
	 */
	public static boolean SHOW_ALL_ACTION_RESULTS = false;
	
	/**
	 * If true, special messaging for contract formation process
	 */
	public static boolean DETAILED_CONTRACT_FORMATION = false;
	
	
	/**
	 * If true, simulation of environmental variables (cargo, attacks, rainstorms, etc) starts
	 * automatically.  If false, the kickOffSim method needs to be called.
	 */
	public static final boolean AUTO_START_SIM = false;	
	
	/**
	 * Random number seed; if explicitly stated, can use for reproducible 
	 * environments and behaviours.  Otherwise, will use currentTimeMillis as seed.
	 */
	public static final String 	PROPERTY_SEED 	= "RandomSeed";
	
    /**
     * Used as the key to identify default agent policies, if cannot find a policy specific to
     * agent and/or capability
     */
	public static final String DEFAULT_MP_KEY = "DEFAULT_MAINT_POLICY";
	
	/**
	 * Default subdirectory for policies
	 */
	public static final String POLICY_DIRECTORY = "policy";
	
	/**
	 * Annotations for joint actions etc...
	 * 
	 * i.e.
	 * move(A,B)[dependant("HQ"), dependantIntention(2), index(2), maintainedIntention(0,2)]
	 * 
	 * Maintained intention has two args; the number of changes made between an activity being executed, and the
	 * total number of times an intention has been altered.  Both are used to prevent 'thrashing', as we don't have
	 * a decommitment mechanism beyond failure.
	 */
	public static final String 	ANNOTATION_DEPENDANT 			= "dependant", 
								ANNOTATION_DEPENDANT_INTENTION 	= "dependantIntention", 
								ANNOTATION_ID_IN_PLAN 			= "index", 
								ANNOTATION_PLAN_LABEL 			= "planLabel",
								ANNOT_MAINTAINED 				= "maintainedIntention",
								ANNOT_REPLANNED 				= "replanned",
								ANNOT_MAINTAIN_ATTMPT 			= "maintAttempts";
	/**
	 * Annotation employed for cancelled dependency results
	 */
	public static final String ANNOT_FOR_CANCELLED_DP = "forCancelledDep";
	
	/**
	 * If true, we DON'T delete the temp PDDL files used/generated in planning.
	 */
	public static final boolean PRESERVE_EM_PLAN_FILES = true;
	
	/*
	 * Enumerators...
	 */
	
	/** 
	 * Agent type - set of combined physical and logical agent types
	 */
	public static enum AgentType {truck, towtruck, helicopter, bulldozer, apc, hazmzat, commander, crane};
	
	/** 
	 * Entity type; corresponds to a subset of agents, represents actor types in the
	 * environment (i.e. physical agents)
	 */
	public static enum EntityType {truck, towtruck, helicopter, bulldozer, apc, hazmat, crane};
	
	/**
	 * Possible health values for vehicles/entities
	 */
	public enum HealthState{
		healthy(2),/* slightlydamaged(3),*/ damaged(1), /*severelydamaged(1),*/ mortal(0);
		int value;
		HealthState(int val)	{	value = val;	}
		public int getValue()	{	return value;	}
		public HealthState getWorse()	{
			return HealthState.mapToState(value-1);
		}
		public HealthState getBetter()	{
			return HealthState.mapToState(value+1);
		}
		public boolean equals(HealthState h)	{
			return value == h.getValue();
		}

		public static HealthState mapToState(int value)	{
			if(value > healthy.value) value=healthy.value;
			else if(value < mortal.value) value=mortal.value;
			switch(value)	{
			case 1: return damaged;
			case 2: return healthy;
			default: return mortal;
			}
		}
	}
		
	/**
	 * Defines all of actions that can be performed in the world/environment
	 * i.e. the set of <i>possible</i> effectors.  These are mapped to entity types 
	 * and corresponding physical agents
	 */
	public enum WorldAction	{	move,repair,	 		//(generic) vehicle
								load, unload, 			//truck / heli
								attach, release, tow,	//tow truck (link agent to agent, tow along road)
								unblock,				//bulldozer (along road, j-j)
								secureArea, 			//APC/Military (at junction)
								decontaminate, 			//Hazmat (equivalent to bulldozer unblock)
								fly, takeOff, land,		//Helicopter/plane
								rendezvous,				//meet on road at exact co-ords
								consume, 				//logical hq - release cargo
								free,					//virtual activity; represent transition to unstuck in planning process
								unloadFrom, loadOnto	//crane - transfer support
		}
	
	/**
	 * Internal actions known to agents; i.e.  NOT effectors, but relate to internal
	 * logic functionality
	 */
	public enum InternalAction	{doRoute, planRoute}

	/**
	 * Used to construct road uids; need to be careful with this to prevent PDDL character errors - i.e.
	 * j1_j2 is ok but j1:j2 is NOT.  Neither is j1-j2 for JavaGP.
	 */
	public static final String ROAD_ID_SEPARATOR_CHAR = "_";

	
	/** Road (surface) types */
	public enum RoadType {tarmac, mud};
	
	/** Road conditions */
	public enum RoadCondition {dry, slippery, flooded}

}
