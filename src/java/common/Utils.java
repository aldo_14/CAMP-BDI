/**
 * 
 */
package common;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import java.util.Vector;
import java.util.logging.Logger;

import truckworld.vehicle.Vehicle;
import truckworld.world.World;
import truckworld.world.interfaces.JunctionInf;
import truckworld.world.interfaces.RoadInf;



/**
 * @author Alan White
 * Shared static utilities class.  
 */
public class Utils {
	
	/**
	 * Enum of heuristics implemented for calculting FScore (score to reach goal)
	 */
	public enum HeuristicsForHScore {distanceOnly}
	
	/**
	 * Enum of heuristics implemented for calculating GScore (score to reach a node)
	 */
	public enum HeuristicsForGScore {distanceOnly, distanceDivBySpeed, confidenceBased}
	
	
	private static Logger getLogger(String methodName){
		Logger l = Logger.getLogger("Utils:" + methodName);
		l.setLevel(UniversalConstants.UTILS_LOGGER_LEVEL);
		return l;
	}

	/**
	 * Performs an A* calculation of a route from start-end for a specified vehicle
	 * @param start - origin
	 * @param end - destination
	 * @param traveller - travelling vehicle, to check if route is traversable
	 * @return Stack of junctions to visit, with the first out being the start junction and the last 
	 * the destination.  Null indicates no route found.
	 */
	public static Stack<JunctionInf> getRoute(	JunctionInf start, JunctionInf end, 
												Map<String, RoadInf> roads, Vehicle traveller){
		Stack<JunctionInf> route = Utils.getRoute(	start, end, roads, traveller, 
													HeuristicsForHScore.distanceOnly, 
													HeuristicsForGScore.distanceOnly, false, true);
		if(route==null || (route.isEmpty() && !(start.getId().equals(end.getId()))))	{
			route = Utils.getRoute(	start, end, roads, traveller, 
									HeuristicsForHScore.distanceOnly, 
									HeuristicsForGScore.distanceOnly, true, true);
		}
		return route;
	}
	
	/**
	 * See other getRoute method; this just facilitates specific heuristic settings
	 * @param start
	 * @param end
	 * @param traveller
	 * @param h
	 * @param g
	 * @return
	 */
	public static Stack<JunctionInf> getRoute(JunctionInf start, JunctionInf end, Map<String, RoadInf> roads,
			Vehicle traveller,
			HeuristicsForHScore h, HeuristicsForGScore g, boolean allowSlippery){
		Stack<JunctionInf> route = Utils.getRoute(start, end, roads, traveller, h, g, false, allowSlippery);
		if(route==null || (route.isEmpty() && !(start.getId().equals(end.getId()))))	{
			Logger logger = Utils.getLogger("getRoute");
			logger.setLevel(UniversalConstants.UTILS_LOGGER_LEVEL);
			logger.severe("No route found, trying a route without constraints");
			route = Utils.getRoute(start, end, roads, traveller, h, g, true, allowSlippery);
			if(route == null || route.isEmpty())	{
				logger.severe(	"No route found from " + start.getId() + " to " + end.getId() + 
								", even without constraints");
			}
			else	{
				logger.info(	"Route found with relaxed constraints from " + start.getId() +
								" to " + end.getId());
			}
		}
		return route;
	}
	
	
	/**
	 * Allows us to override constraints and find ANY route of road movements without worrying
	 * about traversability, if loose == true.
	 * @param v
	 * @param r
	 * @param loose - if true, then consider blocked or dangerous roads
	 * @return
	 */
	public static boolean canTravel(Vehicle v, RoadInf r, boolean loose, boolean allowSlippery)	{
		//special case; we allow toxic, blocked etc roads as these can be fixed
		if(loose)	{
			boolean rj1Dangerous = r.getEnds()[0].isDangerous();
			boolean rj2Dangerous = r.getEnds()[1].isDangerous();
			boolean rBlocked = r.isBlocked();
			boolean rToxic = r.isToxic();
			if(rj1Dangerous)	{	r.getEnds()[0].setDangerous(false);	}
			if(rj2Dangerous)	{	r.getEnds()[1].setDangerous(false);	}
			if(rBlocked)		{	r.setBlocked(false);	}
			if(rToxic)			{	r.setToxic(false);	}
			boolean result = canTravel(v, r, false, allowSlippery);

			if(rj1Dangerous)	{	r.getEnds()[0].setDangerous(true);	}
			if(rj2Dangerous)	{	r.getEnds()[1].setDangerous(true);	}
			if(rBlocked)		{	r.setBlocked(true);	}
			if(rToxic)			{	r.setToxic(true);	}
			return result;
		}
		else	{	
			if(!allowSlippery && (r.isSlippery() || r.isFlooded())) {
				return false;
			}
			return v.canTravelAlong(r);	
		}
	}
	
	/**
	 * 
	 * @param start
	 * @param end
	 * @param traveller
	 * @param hHeuristic
	 * @param gHeuristic
	 * @param looseConstraints - if true, we don't consider whether roads are blocked/dangerous/toxic
	 * @return
	 */
	public static Stack<JunctionInf> getRoute(JunctionInf start, JunctionInf end, Map<String, RoadInf> roads,
			Vehicle traveller, 
			HeuristicsForHScore hHeuristic, HeuristicsForGScore gHeuristic, boolean looseConstraints, boolean allowSlippery){
		Logger logger = Utils.getLogger("getRoute");
		logger.setLevel(UniversalConstants.UTILS_LOGGER_LEVEL);
		logger.finer("Get route; " + start.getId() + "->" + end.getId() + ", for " + traveller.getClass().getName());
		//quick check for pointless tasks...
		if(start.equals(end)){return new Stack<JunctionInf>();}
		
		//A* algorithm; use a straight line distance heuristic
		
		//open list - init with start node
		Collection<JunctionInf> open = new Vector<JunctionInf>();
		
		//closed/evaluated list - init empty
		Collection<JunctionInf> closed = new Vector<JunctionInf>();
		
		//map; key is junction, value the 'reached from' node
		//i.e. a,b -> a is reached from b
		Map<JunctionInf, JunctionInf> map = new HashMap<JunctionInf, JunctionInf>();
		
		//junction g score - movement cost from START to current square (to get there)
		Map<JunctionInf, Float> gScore = new HashMap<JunctionInf, Float>();
		
		//junction h score - heuristic score estimating movement from current to END
		//H is an estimate - can use straight line distance
		Map<JunctionInf, Float> hScore = new HashMap<JunctionInf, Float>();
		
		//junction f score - is G+H.  Prefer lower cost - lower F - score junction; i.e.
		//want to select the 'cheapest' node each stage of the route
		Map<JunctionInf, Float> fScore = new HashMap<JunctionInf, Float>();
		
		//init adjacent of start
		Collection<JunctionInf> initNeighbours = start.getNeighbours().values();
		
		for(JunctionInf n:initNeighbours){
			logger.finer("init; neighbours of " + start.getId() + " = " + n.getId());
			
			final RoadInf connection = roads.get(start.getConnection(n)); //road used
			if(canTravel(traveller, connection, looseConstraints, allowSlippery) )	{
				open.add(n);
				map.put(n, start); //n reached from start
				
				//g = movement cost from start to n
				gScore.put(n, scoreGCost(traveller, connection, gHeuristic));
				
				//h = distance based heuristic
				hScore.put(n, scoreHCost(end, n, hHeuristic));
				
				//f = g + h
				fScore.put(n, gScore.get(n) + hScore.get(n));
				logger.finest("Road ok: " + connection.toString());
			}
			else	{
				logger.finest(traveller.getId() + " cannot use road " + connection.toString() + "!");
			}
		}
		//should now have f, g, h scores for all viable adjacencies to Start node
		
		/*==================================================================
		 * BEGIN PATH-FINDING...
		 ==================================================================*/
		
		//while ! open is empty (no route found) OR target node is on closed list (route found)
		while(!open.isEmpty())	{
			//select node with lowest F-score
			double lowestF = Double.MAX_VALUE;//temp
			JunctionInf selected = null;
			for(JunctionInf current: open){
				//get lowest f-cost square
				if(fScore.get(current).doubleValue() < lowestF){
					lowestF = fScore.get(current).doubleValue();
					selected = current;
				}
			}
			
			//take selected node -> move from open to closed list
			open.remove(selected);
			closed.add(selected);
			
			if(selected == null){
				logger.warning("formRoute: Null selected, returning null");
				return null;
			}
			else	{
			//do for each adjacent node...
				Collection<JunctionInf> neighbours = selected.getNeighbours().values();
				for(JunctionInf n:neighbours){
					/*
					 * If road is untraversable or junction if on the closed list, ignore!
					 */
					if(	closed.contains(n))	{
						logger.finest("Cannot; closed " + n.toString());
					} 
					else {
						RoadInf road = roads.get(selected.getConnection(n));
						boolean canTravel = canTravel(traveller, road, looseConstraints, allowSlippery);
						logger.finer("Iteration; neighbour of " + selected.getId() + " = " + n.getId() + "\n" + road + " travel? " + canTravel);
						if(!canTravel)	{
							logger.finest("Cannot travel " + road);
						}
						else if(n.equals(start)){
							logger.finest("Disregarding start node");
						}
						else	{
							//calculated f, g, h costs
							//straight line distance to destination
							double hCost = scoreHCost(end, n, hHeuristic);

							//g cost; to reach this node, distance from selected PLUS 
							//the gScore of selected from the paren
							double gCost = 	scoreGCost(traveller, road, gHeuristic)  + 
											gScore.get(selected); 
							
							//h + g cost...
							double fCost = hCost + gCost;
							if(!open.contains(n))	{
								//add to open, set parent and f,g,h values
								open.add(n);
								map.put(n, selected); //n->selected link
								gScore.put(n, new Float(gCost));
								hScore.put(n, new Float(hCost));
								fScore.put(n, new Float(fCost));
							}
							else if(gScore.get(n).doubleValue() > gCost){
								//compare g cost and find out if this connection is cheaper
								//if so, then change the parent to be this node
								map.put(n, selected); //n->selected link
								gScore.put(n, new Float(gCost));
								hScore.put(n, new Float(hCost));
								fScore.put(n, new Float(fCost));
							}//elseif
						}
					}
				}	//end for
			}//end else !closed
		}//end while loop

		//if closed list contains end destination
		if(closed.contains(end))	{
			//need FILO structure...
			Stack<JunctionInf> route = new Stack<JunctionInf>();

			logger.finer("Route was found from " + start.getId() + " and " + end.getId());
			JunctionInf current = end;
			while(!route.contains(start)){
				route.add(current);
				current = map.get(current);
				logger.finest("Route stack add: " + current);
			}
			return route;
		}
		else	{
			logger.warning(	"Closed does not have end=>no route was found between " + 
							start.getId() + " and " + end.getId());
			logger.finest("Closed set=" + closed.toString());
			//implies no route found
			return null; //indicates no route
		}
	}//end findroute

	/**
	 * Heuristic for calculating score-to-destination
	 * @param destination
	 * @param current
	 * @return
	 */
	protected static float scoreHCost(JunctionInf destination,
			JunctionInf current,  HeuristicsForHScore hType) {
		//insert case statement if we have other types of hscore...
		return (float)current.getCoordinate().distance(destination.getCoordinate());
	}

	/**
	 * Score hueristic for traveller using connection; returns a float, the higher the better
	 * (the more likely connection is to be used)
	 * @param v
	 * @param r
	 * @return
	 */
	protected static float scoreGCost(Vehicle v, RoadInf r, HeuristicsForGScore h) {
		if(h.equals(HeuristicsForGScore.distanceOnly))	{
			return r.getLength();
		}
		else if(h.equals(HeuristicsForGScore.distanceDivBySpeed))	{
			return r.getLength() / v.maxSpeed(r);
		}
		else if(h.equals(HeuristicsForGScore.confidenceBased)){
			return v.confidenceScore(r);
		}
		return r.getLength();
	}
	
	/**
	 * Finds the road which a particular point lies upon; has a 0.1 margin of error in co-ordinate
	 * space, i.e. will return true if point is within 0.1 units of the line
	 * @param p
	 * @return null if no road found
	 */
	public static RoadInf getRoad(Point2D p, World w){
		Vector<RoadInf> rs = w.getAllRoads();
		for(RoadInf r: rs)	{
			Line2D rLine = 
					new Line2D.Double(r.getEnds()[0].getCoordinate(), r.getEnds()[1].getCoordinate());
			if(p.equals(r.getEnds()[0].getCoordinate()) || p.equals(r.getEnds()[1].getCoordinate())){
				//at a junction, not on road....
			}
			//distance based check, with 0.1 margin of error
			else if(rLine.ptLineDist(p)<=0.1)	{	return r;	}
		}
		return null;
	}
	
	/**
	 * Returns the junction corresponding to a particular point
	 * @param p
	 * @param w
	 * @return null if this point is not the centre of a junction
	 */
	public static JunctionInf getJunction(Point2D p, Collection<JunctionInf> js)	{
		for(JunctionInf j: js){
			if(j.getCoordinate().equals(p)) return j;
		}
		return null;
	}

	/**
	 * Identifies whether l2 is entirely contained within l1, or vice versa.  Returns true
	 * if one line can be considered as equivalent to a subsection of another.
	 * 
	 * i.e. A--->B--->C-->D
	 * l1 = AB, l2 = AC returns true
	 * l1 = AB, l2 = BC returns false
	 * l1 = BC, l2 = AD returns true
	 * @param roadLine
	 * @param otherRoadLine
	 * @return
	 */
	public static boolean containsLine(Line2D l1, Line2D l2) {
		//need to check m and c from y=mx+c
		//m = (y2-y1) / (x2 - x1)
		double m1 = (l1.getY2() - l1.getY1()) / (l1.getX2() - l1.getX1());
		double m2 = (l2.getY2() - l2.getY1()) / (l2.getX2() - l2.getX1());
		if(m1!=m2){
			return false;
		}
		//else, find c...
		//y = mx + c y-c = mx -c = mx+y c = -1(mx+y)
		double c1 = -((m1*l1.getX1())+l1.getY1());
		double c2 = -((m2*l2.getX1())+l2.getY1());
		
		if(c1!=c2){
			return false; //different intersect
		}
		
		//return true if either endpoint of l2 is on this line...
		return 	(l1.ptSegDist(l2.getP1())==0) || (l1.ptSegDist(l2.getP2())==0) ||
				(l2.ptSegDist(l1.getP1())==0) || (l2.ptSegDist(l1.getP2())==0);
	}

	/**
	 * Intersection check used as an acceptability test for non-flyover neighbours
	 * @param l1
	 * @param l2
	 * @return
	 */
	public static boolean intersects(Line2D l1, Line2D l2) {
		if(l1.equals(l2))	{	return true;	}
		
		boolean containedWithinEachOther  = Utils.containsLine(l1, l2);
		if(containedWithinEachOther)	{ return true;	}

		boolean sharedEndpoints = (	l1.getP1().equals(l2.getP1())	||
									l1.getP1().equals(l2.getP2())	||
									l1.getP2().equals(l2.getP1())	||
									l1.getP2().equals(l2.getP2())		);
		if(sharedEndpoints)	{	return false;	}
		
		//test intersection - does l1 / l2 cross over the other?
		return l1.intersectsLine(l2);
	}
}
