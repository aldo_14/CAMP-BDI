package common.ia;

import static truckworld.env.Percepts.AT_COORD;
import static truckworld.env.Percepts.AT_JUNCTION;
import static truckworld.env.Percepts.BUSY;
import static truckworld.env.Percepts.ON_ROAD;
import static truckworld.env.Percepts.RESTING;
import static truckworld.env.Percepts.STUCK;
import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.IntendedMeans;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.ASSyntax;
import jason.asSyntax.ListTerm;
import jason.asSyntax.Literal;
import jason.asSyntax.Term;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import agent.AgentUtils;
import agent.beliefBase.WorldBeliefBase;

import common.UniversalConstants;
import common.UniversalConstants.EntityType;
import common.UniversalConstants.HealthState;

public class findNearest extends DefaultInternalAction {
	private static final long serialVersionUID = 340251456924063326L;
	
	@Override
    public int getMinArgs() { return 3; }
	
	@Override
    public int getMaxArgs() { return 3; }
	
	protected boolean findNearestAgentType(Logger logger, TransitionSystem ts, Unifier un, Term[] args, EntityType type)	{
		//get source info - agent ids to avoid
		Vector<String> avoidIds = new Vector<String>();
		IntendedMeans curPlan = new AgentUtils().intentionIntoIMsQueue(ts.getC().getSelectedIntention()).getLast();
		if(curPlan!=null)	{
			Literal trigger = curPlan.getTrigger().getLiteral();
			//dependency annots
			ListTerm dep = trigger.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT);
			if(dep!=null && !dep.isEmpty())	{
				List<Term> ids = ((Literal)dep.get(0)).getTerms();
				for(Term id:ids)	{
					String aid = id.toString().replaceAll("\"", "");
					if(aid.toLowerCase().startsWith(type.name().toLowerCase()))	{
						avoidIds.add(aid);
					}
				}
			}
		}
		
		//id of junction...
		String jId = args[2].toString().replaceAll("\"", "");
		WorldBeliefBase wbb = (WorldBeliefBase)ts.getAg().getBB();
		logger.finer("Find nearest " + type + " to " + jId + "...got wbb");

		//assemble a list of busy trucks
		//is this agent busy?
		Vector<String> busy = getBusySet(wbb);
		logger.finer("Busy = " + busy);

		//get list of healthy trucks
		Vector<String> healthy = getHealthySet(wbb);
		logger.finer("Healthy = " + healthy);

		//damaged
		Vector<String> damaged = getDamagedSet(wbb);
		logger.finer("damaged = " + damaged);
		
		//stuck
		Vector<String> stuck = getStuckSet(wbb);
		logger.finer("stuck = " + stuck);
		
		//resting (Cannot be unstuck)
		Vector<String> resting = getRestingSet(wbb);
		logger.finer("resting = " + resting);
		
		Iterator<Literal> il = wbb.getCandidateBeliefs(Literal.parseLiteral(type.toString() + "(T)"), null);

		if(il == null || !il.hasNext())	{
			logger.severe("((err1)No " + type + "s found in BB!");
			return false;
		}
		/*
		 * We need to search for vehicles; there are 2 conditions / precedences
		 * 1/ closest healthy vehicle
		 * 2/ if no closest healthy vehicle, closest non-mortal vehicle
		 */
		String healthyV = null; //closest healthy vehicle
		String damagedV = null; //closest damaged vehicle
		double closestHv = Double.MAX_VALUE;
		double closestDv = Double.MAX_VALUE;
		
		String healthyStuckV = null; //closest healthy vehicle
		String damagedStuckV = null; //closest damaged vehicle
		double closestStuckHv = Double.MAX_VALUE;
		double closestStuckDv = Double.MAX_VALUE;

		//iterate through all truck type vehicles
		while(il.hasNext())	{
			Literal vehLit = il.next();
			logger.finer("Checking apc; " + vehLit);
			String vehicle = vehLit.getTerm(0).toString().replaceAll("\"", "");

			if(busy.contains(vehicle))	{	
				logger.finer(vehicle + " is busy");	
			}
			else if(resting.contains(vehicle))	{
				logger.info(vehicle + " is resting and cannot move");
			}
			else if(!healthy.contains(vehicle) && !damaged.contains(vehicle))	{
				logger.info(vehicle + " is mortal!");
			}
			else	{
				logger.finer(vehicle + " is free!");

				//find exact location using at(A, X, Y) percept
				Literal chk = Literal.parseLiteral(AT_COORD + "(A, X, Y)");
				logger.finer("checkLIt = " + chk);
				Iterator<Literal> result = wbb.getCandidateBeliefs(chk, null);

				//if we can't find any 'at' percepts, cannot find a result
				if(result==null)	{	
					logger.severe("No AT percepts found in env??");	
					return false;
				}

				while(result.hasNext())	{
					Literal rl = result.next();

					if(rl.getTerm(0).toString().replaceAll("\"", "").equals(vehicle))	{
						logger.finer("Got at: " + rl);
						double 	x = Float.parseFloat(rl.getTerm(1).toString().replaceAll("\"", "")),
								y = Float.parseFloat(rl.getTerm(2).toString().replaceAll("\"", ""));
						double distance = new java.awt.geom.Point2D.Double(x,y).distance(
								wbb.getJunction(jId).getCoordinate());

						//distance...
						logger.finer("Distance = " + distance);
						
						if(stuck.contains(vehicle))	{
							if(healthy.contains(vehicle) && (distance < closestStuckHv))	{
								if(!avoidIds.contains(vehicle) || healthyStuckV==null )	{
									closestStuckHv = distance;
									healthyStuckV = vehicle;
								}
							}
							else if(damaged.contains(vehicle) && (distance < closestStuckDv))	{
								if(!avoidIds.contains(vehicle) || damagedStuckV==null )	{
									closestStuckDv = distance;
									damagedStuckV = vehicle;
								}
							}
						}
						else	{
							if(healthy.contains(vehicle) && (distance < closestHv))	{
								if(!avoidIds.contains(vehicle) || healthyV==null )	{
									closestHv = distance;
									healthyV = vehicle;
								}
							}
							else if(damaged.contains(vehicle) && (distance < closestDv))	{
								if(!avoidIds.contains(vehicle) || damagedV==null )	{
									closestDv = distance;
									damagedV = vehicle;
								}
							}
						}
					}//endif check for id
				}//end hasNext
			}

		}//end while

		//end of loop; check results
		if(healthyV == null && damagedV == null)	{
			//try stuck
			if(healthyStuckV!=null)			{	
				return un.unifies(args[0], ASSyntax.createString(healthyStuckV));	
			}
			else if(damagedStuckV!=null)	{	
				return un.unifies(args[0], ASSyntax.createString(damagedStuckV));	
			}

			logger.warning("No unstuck vehicles found");
			if(logger.isLoggable(Level.FINER))	{ //debug out!
				Iterator<Literal> it =  ts.getAg().getBB().iterator();
				while(it.hasNext())	{	logger.finer("BB:" + it.next().toString());	}
			}
			return false;
		}
		else	{
			String v;
			if(healthyV!=null)	{	v = healthyV;	}
			else				{	v = damagedV;	}
			//bind apc to arg-0
			un.unifies(args[0], ASSyntax.createString(v));	
				
			//get location and bind to arg[1] using atJ
			//we are assuming not onR...
			Literal chk = Literal.parseLiteral(AT_JUNCTION + "(\"" + v +  "\", JID)");
			logger.finer("checkLIt = " + chk);
			Iterator<Literal> result = wbb.getCandidateBeliefs(chk, null);

			//if we can't find any 'at' percepts, cannot find a result
			if(result==null)	{	
				logger.severe("No AT percepts found in env??");	
				return false;
			}

			String loc = "";
			while(result.hasNext())	{
				Literal rl = result.next();
				if(rl.getTerm(0).toString().replaceAll("\"", "").equals(v))	{
					loc = rl.getTerm(1).toString().replaceAll("\"", "");
					logger.info(v + " found at j " + loc);
				}
				else	{	logger.warning("Mishit; " + rl + " for " + v);	}
			}
			
			//road check
			if(loc.equals(""))	{
				chk = Literal.parseLiteral(ON_ROAD + "(\"" + v +  "\", JID)");
				result = wbb.getCandidateBeliefs(chk, null);

				//if we can't find any 'at' percepts, cannot find a result
				if(result==null)	{	
					logger.severe("No onR percepts found in env??");	
					return false;
				}
				else	{
					while(result.hasNext())	{
						Literal rl = result.next();
						if(rl.getTerm(0).toString().replaceAll("\"", "").equals(v))	{
							loc = rl.getTerm(1).toString().replaceAll("\"", "");
							logger.info(v + " found on road " + loc);
						}
						else	{	logger.warning("Mishit; " + rl + " for " + v);	}
					}
				}
			}
			
			un.unifies(args[1], ASSyntax.createString(loc));	

			logger.fine("Nearest vehicle found; " + un.toString());
		}

		return true;
	}
	
	protected Vector<String> getStuckSet(WorldBeliefBase wbb) {
		final Iterator<Literal> stuck = wbb.getCandidateBeliefs(Literal.parseLiteral(STUCK + "(A)"), null);
		final Vector<String> stuckSet = new Vector<>();
		while(stuck!=null && stuck.hasNext())	{
			Literal btL = stuck.next();
			stuckSet.add(btL.getTerm(0).toString().replaceAll("\"", ""));
		}
		return stuckSet;
	}
	
	protected Vector<String> getRestingSet(WorldBeliefBase wbb) {
		final Iterator<Literal> stuck = wbb.getCandidateBeliefs(Literal.parseLiteral(RESTING + "(A)"), null);
		final Vector<String> stuckSet = new Vector<>();
		while(stuck!=null && stuck.hasNext())	{
			Literal btL = stuck.next();
			stuckSet.add(btL.getTerm(0).toString().replaceAll("\"", ""));
		}
		return stuckSet;
	}

	protected Vector<String> getBusySet(WorldBeliefBase wbb) {
		final Iterator<Literal> busy = wbb.getCandidateBeliefs(Literal.parseLiteral(BUSY + "(A)"), null);
		final Vector<String> busyTrucks = new Vector<>();
		while(busy!=null && busy.hasNext())	{
			Literal btL = busy.next();
			busyTrucks.add(btL.getTerm(0).toString().replaceAll("\"", ""));
		}
		return busyTrucks;
	}


	protected Vector<String> getHealthySet(WorldBeliefBase wbb) {
		final Iterator<Literal> hl = wbb.getCandidateBeliefs(Literal.parseLiteral(
				HealthState.healthy + "(AID)"), null);
		final Vector<String> healthy = new Vector<>();
		while(hl!=null && hl.hasNext())	{
			Literal l = hl.next();
			healthy.add(l.getTerm(0).toString().replaceAll("\"", ""));
		}
		return healthy;
	}

	protected Vector<String> getDamagedSet(WorldBeliefBase wbb) {
		final Iterator<Literal> dm = wbb.getCandidateBeliefs(Literal.parseLiteral(
				HealthState.damaged + "(AID)"), null);
		final Vector<String> damaged = new Vector<>();
		while(dm!=null && dm.hasNext())	{
			Literal l = dm.next();
			damaged.add(l.getTerm(0).toString().replaceAll("\"", ""));
		}
		return damaged;
	}
}
