package common.ia;

import static truckworld.env.Percepts.AT_COORD;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.ASSyntax;
import jason.asSyntax.Literal;
import jason.asSyntax.Term;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import agent.beliefBase.WorldBeliefBase;

import common.UniversalConstants;
public class findNearestTruckOrHeli  extends findNearest	{

	private static final long serialVersionUID = 3354445058394510756L;
	protected Logger logger;

	@Override
    public int getMinArgs() { return 2; }
	
	@Override
    public int getMaxArgs() { return 2; }
	
	/**
	 * Searches for the nearest truck type object (arg 0) to a given location (arg 1) then unifies
	 * arg 0 to that trucks value.  Bias towards trucks over helis.
	 * @param ts
	 * @param un
	 * @param args
	 * @param type
	 * @return
	 * @throws Exception
	 */
	@Override
	public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
		logger = Logger.getLogger(getClass().getSimpleName() + ":" + ts.getUserAgArch().getAgName());
		logger.setLevel(UniversalConstants.INTERNAL_ACTION_LOGGER_LEVEL);
		logger.fine("FindNearestTruck executing with unifier " + un.toString());

		//force an update to agent bb with beliefs
		synchronized(ts.getAg().getBB())	{	
			List<Literal> perceive = ts.getUserAgArch().perceive();
			if(perceive!=null)	{	ts.getAg().buf(perceive);	}
		}
		
		if(logger.isLoggable(Level.FINER))	{
			for(int i=0; i<args.length; i++){	
				logger.finer("Arg[" + i + "]=" + args[i] + ", ground=" + args[i].isGround());	
			}
		}
		
		//get BB...
		if(!(ts.getAg().getBB() instanceof WorldBeliefBase)){
			logger.severe(ts.getUserAgArch().getAgName() + "; find cargo is using incompatible BB type");
			return false;
		}

		//id of junction...
		final String jId = args[1].toString().replaceAll("\"", "");
		//id of junction...
		final String deliveryjId = args[2].toString().replaceAll("\"", "");
		final WorldBeliefBase wbb = (WorldBeliefBase)ts.getAg().getBB();
		logger.finer("Find nearest truck to " + jId + "...got wbb");

		//assemble a list of busy trucks
		//is this agent busy?
		Vector<String> busy = getBusySet(wbb);
		logger.finer("Busy = " + busy);

		//get list of healthy trucks
		Vector<String> healthy = getHealthySet(wbb);
		logger.finer("Healthy = " + healthy);

		//damaged
		Vector<String> damaged = getDamagedSet(wbb);
		logger.finer("damaged = " + damaged);
		
		//stuck
		Vector<String> stuck = getStuckSet(wbb);
		logger.finer("stuck  = " + stuck);

		//resting (Cannot be unstuck)
		Vector<String> resting = getRestingSet(wbb);
		logger.finer("resting = " + resting);
		
		Vector<Literal> set = new Vector<Literal>();
		
		//need to get all trucks...
		Iterator<Literal> il = wbb.getCandidateBeliefs(
				Literal.parseLiteral(UniversalConstants.EntityType.truck.toString() + "(T)"),  
				null);
		if(il!=null)	{
			while(il.hasNext())	{	set.add(il.next());	}
		}
		
		//consider helicopters if destination is an airport
		if(wbb.getAirports().contains(jId) && wbb.getAirports().contains(deliveryjId))	{
			il = wbb.getCandidateBeliefs(Literal.parseLiteral(
					UniversalConstants.EntityType.helicopter.toString() + "(T)"), null);
			if(il!=null)	{
				while(il.hasNext())	{	set.add(il.next());	}
			}
		}
		
		/*
		 * We need to search for vehicles; there are 2 conditions / precedences
		 * 1/ closest healthy vehicle
		 * 2/ if no closest healthy vehicle, closest non-mortal vehicle
		 */
		String healthyV = null; //closest healthy vehicle
		String damagedV = null; //closest damaged vehicle
		String healthyStuckV = null; //closest stuck vehicle
		String damagedStuckV = null; //closest stuck vehicle
		double closestHv = Double.MAX_VALUE;
		double closestDv = Double.MAX_VALUE;
		double closestStuckHv = Double.MAX_VALUE;
		double closestStuckDv = Double.MAX_VALUE;
		
		
		il = set.iterator();

		if(il == null || !il.hasNext())	{
			logger.severe("((err1)No vehicles found in BB!");
			return false;
		}
		
		//iterate through all truck type vehicles
		while(il.hasNext())	{
			Literal vehLit = il.next();
			logger.finer("Checking vehicle; " + vehLit);
			String vehicle = vehLit.getTerm(0).toString().replaceAll("\"", "");

			if(busy.contains(vehicle))			{	logger.finer(vehicle + " is busy");		}
			else if(resting.contains(vehicle))	{	logger.finer(vehicle + " is resting");	}
			else if(!healthy.contains(vehicle) && !damaged.contains(vehicle))	{
				logger.info(vehicle + " is mortal!");
			}
			else	{
				logger.finer(vehicle + " is free!");

				//find exact location using at(A, X, Y) percept
				Literal chk = Literal.parseLiteral(AT_COORD + "(A, X, Y)");
				Iterator<Literal> result = wbb.getCandidateBeliefs(chk, null);

				//if we can't find any 'at' percepts, cannot find a result
				if(result==null)	{	
					logger.severe("No AT percepts found in env??");	
					return false;
				}

				while(result.hasNext())	{
					Literal rl = result.next();

					if(rl.getTerm(0).toString().replaceAll("\"", "").equals(vehicle))	{
						logger.finer("Got at: " + rl);
						double 	x = Float.parseFloat(rl.getTerm(1).toString().replaceAll("\"", "")),
								y = Float.parseFloat(rl.getTerm(2).toString().replaceAll("\"", ""));
						double distance = new java.awt.geom.Point2D.Double(x,y).distance(
								wbb.getJunction(jId).getCoordinate());

						//distance...
						logger.finer(vehicle + " distance = " + distance);
						if(stuck.contains(vehicle))	{
							if(healthy.contains(vehicle) && (distance < closestStuckHv))	{
								closestStuckHv = distance;
								healthyStuckV = vehicle;
							}
							else if(damaged.contains(vehicle) && (distance < closestStuckDv))	{
								closestStuckDv = distance;
								damagedStuckV = vehicle;
							}
						}
						else	{
							if(healthy.contains(vehicle) && (distance < closestHv))	{
								closestHv = distance;
								healthyV = vehicle;
							}
							else if(damaged.contains(vehicle) && (distance < closestDv))	{
								closestDv = distance;
								damagedV = vehicle;
							}
						}
					}//endif check for id
				}//end hasNext
			}

		}//end while

		//end of loop; check results
		if(healthyV == null && damagedV == null)	{
			
			//try stuck
			if(healthyStuckV!=null)			{	return un.unifies(args[0], ASSyntax.createString(healthyStuckV));	}
			else if(damagedStuckV!=null)	{	return un.unifies(args[0], ASSyntax.createString(damagedStuckV));	}
			
			logger.warning("No suitable vehicles found");
			if(logger.isLoggable(Level.FINER))	{ //debug out!
				Iterator<Literal> it =  ts.getAg().getBB().iterator();
				while(it.hasNext())	{	logger.finer("BB:" + it.next().toString());	}
			}
			return false;
		}
		else	{
			if(healthyV!=null)	{	un.unifies(args[0], ASSyntax.createString(healthyV));	}
			else				{	un.unifies(args[0], ASSyntax.createString(damagedV));	}
			logger.fine("Nearest vehicle found; " + un.toString());
		}

		return true;
	}//end method
}
