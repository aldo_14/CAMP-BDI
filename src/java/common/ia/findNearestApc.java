package common.ia;

import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.Literal;
import jason.asSyntax.Term;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import agent.beliefBase.WorldBeliefBase;

import common.UniversalConstants;
import common.UniversalConstants.EntityType;

public class findNearestApc  extends findNearest {

	private static final long serialVersionUID = 3354445058394510756L;
	protected Logger logger;

	
	/**
	 * Searches for the nearest apc/heli type object (arg 0) to a given location (arg 2) then unifies
	 * arg 0 to the id and arg 1 to the loc
	 * @param ts
	 * @param un
	 * @param args
	 * @param type
	 * @return
	 * @throws Exception
	 */
	@Override
	public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
		logger = Logger.getLogger(getClass().getSimpleName() + ":" + ts.getUserAgArch().getAgName());
		logger.setLevel(UniversalConstants.INTERNAL_ACTION_LOGGER_LEVEL);
		logger.fine("Executing with unifier " + un.toString());

		//force an update to agent bb with beliefs
		synchronized(ts.getAg().getBB())	{	
			List<Literal> perceive = ts.getUserAgArch().perceive();
			if(perceive!=null)	{	ts.getAg().buf(perceive);	}
		}
		
		if(logger.isLoggable(Level.FINER))	{
			for(int i=0; i<args.length; i++){	
				logger.finer("Arg[" + i + "]=" + args[i] + ", ground=" + args[i].isGround());	
			}
		}
		
		//get BB...
		if(!(ts.getAg().getBB() instanceof WorldBeliefBase)){
			logger.severe(ts.getUserAgArch().getAgName() + "; find apc is using incompatible BB type");
			return false;
		}
		
		return findNearestAgentType(logger, ts, un, args, EntityType.apc);
	}//end method


}
