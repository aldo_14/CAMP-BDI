package common.ia;

import static truckworld.env.Percepts.CARGO_AT;

import java.awt.geom.Point2D;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import agent.beliefBase.WorldBeliefBase;
import common.UniversalConstants;
import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.ASSyntax;
import jason.asSyntax.Literal;

import jason.asSyntax.Term;

public class findNearestCargo extends DefaultInternalAction {
	private static final long serialVersionUID = -8762187375772488899L;
	
	private Logger logger;
	
	@Override
    public int getMinArgs() { return 3; }
	
	@Override
    public int getMaxArgs() { return 3; }
	
	/**
	 * Finds the nearest cargo object to a given location, identified by arg at index 2
	 * Fills in unifier vars such that 0= carg name and 1= location of cargo
	 * @param ts
	 * @param un
	 * @param args
	 * @return
	 * @throws Exception
	 */
	@Override
	public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
		logger = Logger.getLogger(this.getClass().getSimpleName() + ":" + ts.getUserAgArch().getAgName());
		logger.setLevel(UniversalConstants.INTERNAL_ACTION_LOGGER_LEVEL);

		try	{
			//force an update to agent bb with beliefs
			synchronized(ts.getAg().getBB())	{	
				List<Literal> perceive = ts.getUserAgArch().perceive();
				if(perceive!=null)	{	ts.getAg().buf(perceive);	}
			}
			
			logger.fine("FindCargo executing with unifier " + un.toString());
			if(logger.isLoggable(Level.FINER))	{
				for(int i=0; i<args.length; i++){	logger.fine("Arg[" + i + "]=" + args[i]);	}
			}
			
			if(!(ts.getAg().getBB() instanceof WorldBeliefBase)){
				logger.severe(ts.getUserAgArch().getAgName() + "; find cargo is using incompatible BB type");
				return false;
			}

			WorldBeliefBase wbb = (WorldBeliefBase)ts.getAg().getBB();
			logger.finer("Find nearest cargo...got wbb");

			//get arg location
			String startId = args[2].toString().replaceAll("\"", "");

			if(wbb.getJunction(startId)==null)	{
				logger.info("Junction not found! " + startId);
				return false;
			}

			Point2D startCoo = wbb.getJunction(startId).getCoordinate();

			//find nearest by iterating through results
			String closest = null, cargoName = null;
			double shortestDistance = Double.MAX_VALUE; //use this to minimize recalcs...
			Iterator<Literal> il = wbb.getCandidateBeliefs(
					Literal.parseLiteral(CARGO_AT + "(C, J)"),  null);

			if(il!=null){
				while(il.hasNext()){
					Literal currentCargoAt = il.next();
					logger.finer("Considering cargo; " + currentCargoAt);
					String cn = currentCargoAt.getTerms().get(0).toString().replace("\"", "");
					String locationId = currentCargoAt.getTerms().get(1).toString().replace("\"", "");
					double distance = wbb.getJunction(locationId).getCoordinate().distance(startCoo);
					if (closest == null) {
						closest = locationId;
						cargoName = cn;
					} else if (distance < shortestDistance) {
						shortestDistance = distance;
						closest = locationId;
						cargoName = cn;
					}
					logger.finest("shortest distance cargo; " + cargoName + "@" + closest);
				}
			}
			else	{	logger.warning("CargoAt search returned null!");	}

			//return result; fail if none found
			if(closest == null)	{
				logger.warning("No cargo found!");
				return false;
			}
			else	{
				logger.finer("Closest cargo; " + cargoName + " at " + closest);
				un.unifiesNoUndo(args[0], ASSyntax.createString(cargoName));
				un.unifiesNoUndo(args[1], ASSyntax.createString(closest));
				logger.finer("Unified; " + un.toString());
			}

			return true;
		}
		catch(Exception e){
			StackTraceElement[] ste = e.getStackTrace();
			logger.severe(e.getMessage());
			for(StackTraceElement s: ste)	{
				logger.info(s.toString());
			}
			throw e;
		}
	}
	

}
