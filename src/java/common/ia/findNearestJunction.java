package common.ia;

import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.ASSyntax;
import jason.asSyntax.Literal;
import jason.asSyntax.Term;

import java.awt.geom.Point2D;
import java.util.Iterator;
import java.util.logging.Logger;

import truckworld.world.interfaces.JunctionInf;
import truckworld.world.interfaces.RoadInf;
import agent.beliefBase.WorldBeliefBase;

/**
 * 
 * @author Alan White
 *
 * Determines the nearest junction and sets unifier accordingly
 */
public class findNearestJunction extends DefaultInternalAction  {
	private static final long serialVersionUID = -427271052272191335L;

	@Override
    public int getMinArgs() { return 1; }
	
	@Override
    public int getMaxArgs() { return 1; }
	
	/**
	 * When executed, identifies the nearestjunction based on straight-line distance.  Args[0] specifies the agent id
	 * @throws Exception if no repair centre can be found
	 * @return unifier to bind the arg[0] value as the repair location (i.e. findRepairCentre(R); moveTo(R), etc)
	 * @see 	jason.asSemantics.DefaultInternalAction#execute(jason.asSemantics.TransitionSystem, 
	 * jason.asSemantics.Unifier, jason.asSyntax.Term[])
	 */
	public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
		//are we at a junction?
		Logger logger = Logger.getLogger(this.getClass().getSimpleName()+":"+ts.getUserAgArch().getAgName());
		
		String aid = args[0].toString().replaceAll("\"", "");
		
		WorldBeliefBase wbb = (WorldBeliefBase)ts.getAg().getBB();
		
		Iterator<Literal> il = wbb.getCandidateBeliefs(Literal.parseLiteral("at(\"" + aid + "\", X, Y)"),  null);
		
		boolean found = false;
		Point2D pos = null;
		whileLoop:
		while(il.hasNext())	{
			Literal l = il.next().copy();
			if(l.getTerm(0).toString().replaceAll("\"", "").equals(aid))	{
				double x = Double.parseDouble(l.getTerm(1).toString().replace("\"", ""));
				double y = Double.parseDouble(l.getTerm(2).toString().replace("\"", ""));
				pos = new Point2D.Double(x,y);
				found = true; //end loop
				break whileLoop;
			}
		}
		
		if(!found)	{	
			logger.severe("Location for Agent " + args[0] + " not found!");
			return false;
		}
		
		il = wbb.getCandidateBeliefs(Literal.parseLiteral("atJ(A, J)"),  null);
		if(il!=null)	{
			while(il.hasNext())	{
				Literal l = il.next().copy();
				if(l.getTerm(0).toString().replaceAll("\"", "").equals(aid))	{
						String jid = l.getTerm(1).toString().replaceAll("\"", "");
						un.unifies(args[1], ASSyntax.createString(jid));
						logger.finer("Found junction; " + aid + " is a " + jid + "(" + l + "l");
						return true; //done!
				}
				//else, onwards!
			}
		}//end il!=null
		
		//if not, are we on a road?
		il = wbb.getCandidateBeliefs(Literal.parseLiteral("onR(A, R)"),  null);
		
		if(il!=null)	{
			while(il.hasNext())	{
				Literal l = il.next().copy();
				if(l.getTerm(0).toString().replaceAll("\"", "").equals(aid))	{
					String rid = l.getTerm(1).toString().replace("\"", "");
					RoadInf r = wbb.getRoad(rid);
					double dist = r.getEnds()[0].getCoordinate().distance(pos);
					String id1 = r.getEndIds()[0];
					String id2 = r.getEndIds()[0];
					JunctionInf j1 = wbb.getJunction(id1);
					JunctionInf j2 = wbb.getJunction(id2);
					
					if(j1.isDangerous() && !j2.isDangerous())	{
						un.unifies(args[1], ASSyntax.createString(j2.getId()));
						logger.info("On road " + rid + "; non DZ junction found, unifier= " + un);
					}
					else if(j2.isDangerous() && !j1.isDangerous())	{
						un.unifies(args[1], ASSyntax.createString(j1.getId()));
						logger.info("On road " + rid + "; non DZ junction found, unifier= " + un);
					}
					else	{
						if(r.getEnds()[1].getCoordinate().distance(pos) > dist){
							un.unifies(args[1], ASSyntax.createString(r.getEnds()[0].getId()));
						}
						else	{
							un.unifies(args[1], ASSyntax.createString(r.getEnds()[1].getId()));
						}
						logger.info("On road " + rid + "; closest junction found, unifier= " + un);
					}
					return true;	
				}
			}//end hasNext...
		}
		
		logger.warning("Unable to resolve closest location");
		return false;
	}
}
