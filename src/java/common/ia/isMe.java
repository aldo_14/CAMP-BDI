package common.ia;
import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.Term;

import java.util.logging.Logger;

import common.UniversalConstants;

/**
 * 
 * @author alanwhite
 *
 * Return true if the executing agent is that identified in the first (and only)
 * argument term - return false if not, or null term
 */
public class isMe extends DefaultInternalAction {
	private static final long serialVersionUID = -6649906058885273308L;
	private Logger logger;

	@Override public int getMinArgs() { return 1; }
    @Override public int getMaxArgs() { return 1; }
    
	public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
		logger = Logger.getLogger(getClass().getSimpleName() + ":" + ts.getUserAgArch().getAgName());
		logger.setLevel(UniversalConstants.INTERNAL_ACTION_LOGGER_LEVEL);
		logger.finest("FindNearestTruck executing with unifier " + un.toString());
		
		Term name = args[0];
		if(name.isGround())	{
			String testName = args[0].toString().replaceAll("\"", "");
			String myName = ts.getUserAgArch().getAgName();
			logger.finest(
					"Compare arg string " + args[0] + " to " + myName + "=" + testName.equals(myName));
			return testName.equals(myName);
		}
		else	{
			logger.severe("Passed in unground arg!" + args[0]);
			return false;
		}
	}
}
