package common.capability;

import jason.asSyntax.Literal;
import jason.bb.BeliefBase;
import agent.model.capability.ConfidencePrecondition;
import agent.model.capability.PrimitiveCapability;
import agent.model.goal.GoalTask;
import agent.type.CapabilityAwareAgent;

/**
 * 
 * @author Alawhite
 *
 * Capability for the register task; treats confidence as always 1, equiv. to
 * an internal action (in effect).  Present to allow representation of the
 * 'registered' belief in agent plans and assist causal link inference.
 */
public class RegisterCapability extends PrimitiveCapability {
	private final ConfidencePrecondition[] confPre;
	public RegisterCapability(CapabilityAwareAgent holder, GoalTask t) {
		super(holder, 
				holder.getTaskFactory().factory("register"), 
				holder.getTaskFactory().factory("register").getSignature(), 
				holder.getTaskFactory().factory("register").getPreconditions(), 
				holder.getTaskFactory().factory("register").getGoalEffects());
		confPre = new ConfidencePrecondition[preconditions.length];
		for(int i=0; i<confPre.length;i++) {
			confPre[i] = new ConfidencePrecondition(holder.getId(), sigLit, preconditions[i], 1);
		}
	}
	
	@Override
	public boolean preconditionsHold(BeliefBase bb, Literal action) {	return true;	}
	@Override //always one!
	public float getGeneralConfidence(BeliefBase bb) 	{	return 1;	}

	@Override
	public float getSpecificConfidence(BeliefBase bb, Literal action) {	return 1;	}

	@Override
	public ConfidencePrecondition[] getConfidenceBasedPreconditions(BeliefBase context) {
		return confPre;
	}
	
}
