package common;

import java.util.HashMap;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class Analytics {
	public enum AnalyticsOperations{
		formingPlans, agendaFormation, planFileReading, planFileForming, taskHandling,
		failedTaskHandling, successfulTaskHandling, successPlanning, failedPlanning, 
		bbIteratorFormation, cloneBeliefBase, cloneBBLiterals, agentActing
	}
	
	private final HashMap<AnalyticsOperations, Long> totalTime;
	private final HashMap<AnalyticsOperations, Integer> totalCalls;
	private final static Analytics _instance = new Analytics();	
	private final JTable details;
	private final JFrame aui;
	private final String[] detailsCols = 
			new String[]{"action", "calls", "average (ms)", "total (ms)"};
	private Analytics()	{
		totalTime = new HashMap<>();
		totalCalls = new HashMap<>();
		aui = new JFrame();
		aui.setSize(500, 300);
		aui.setResizable(true);
		
		totalTime.put(AnalyticsOperations.agendaFormation, 0l);
		totalCalls.put(AnalyticsOperations.agendaFormation, 0);
		totalTime.put(AnalyticsOperations.bbIteratorFormation, 0l);
		totalCalls.put(AnalyticsOperations.bbIteratorFormation, 0);
		totalTime.put(AnalyticsOperations.cloneBeliefBase, 0l);
		totalCalls.put(AnalyticsOperations.cloneBeliefBase, 0);
		totalTime.put(AnalyticsOperations.cloneBBLiterals, 0l);
		totalCalls.put(AnalyticsOperations.cloneBBLiterals, 0);
		totalTime.put(AnalyticsOperations.failedPlanning, 0l);
		totalCalls.put(AnalyticsOperations.failedPlanning, 0);
		totalTime.put(AnalyticsOperations.failedTaskHandling, 0l);
		totalCalls.put(AnalyticsOperations.failedTaskHandling, 0);
		totalTime.put(AnalyticsOperations.formingPlans, 0l);
		totalCalls.put(AnalyticsOperations.formingPlans, 0);
		totalTime.put(AnalyticsOperations.planFileForming, 0l);
		totalCalls.put(AnalyticsOperations.planFileForming, 0);
		totalTime.put(AnalyticsOperations.planFileReading, 0l);
		totalCalls.put(AnalyticsOperations.planFileReading, 0);
		totalTime.put(AnalyticsOperations.successfulTaskHandling, 0l);
		totalCalls.put(AnalyticsOperations.successfulTaskHandling, 0);
		totalTime.put(AnalyticsOperations.successPlanning, 0l);
		totalCalls.put(AnalyticsOperations.successPlanning, 0);
		totalTime.put(AnalyticsOperations.taskHandling, 0l);
		totalCalls.put(AnalyticsOperations.taskHandling, 0);
		totalTime.put(AnalyticsOperations.agentActing, 0l);
		totalCalls.put(AnalyticsOperations.agentActing, 0);
		
		Object[][] detailsData = new Object[totalCalls.keySet().size()][detailsCols.length];
		details = new JTable(detailsData, detailsCols);
		JScrollPane scrollPane = new JScrollPane(details);
		details.setFillsViewportHeight(true);
		details.setEnabled(false);
		details.setAutoCreateRowSorter(true);
		aui.getContentPane().add(scrollPane);
		
		updateModelContents();
		
		aui.setVisible(true);
	}

	private void updateModelContents() {
		int i=0;
		for(AnalyticsOperations aop: totalCalls.keySet())	{
			details.getModel().setValueAt(
					aop.toString(), i, 0);
			Integer totalRecords = totalCalls.get(aop);
			details.getModel().setValueAt(
					totalRecords, i, 1);
			if(totalRecords > 0)	{
				details.getModel().setValueAt(
					(totalTime.get(aop)/1000000)/totalRecords, i, 2);
			}
			else	{
				details.getModel().setValueAt(0, i, 2);
			}
			details.getModel().setValueAt(
					totalTime.get(aop)/1000000, i, 3);
			//TODO autosort
			i++;
		}
	}
	
	public synchronized static final Analytics getInstance()	{
		return _instance;
	}
	
	public synchronized void logTime(final AnalyticsOperations op, final long time)	{
		long initTime = 0;
		if(totalTime.containsKey(op))	{
			initTime = totalTime.get(op);
		}
		totalTime.put(op, initTime + time);
		int calls = 0;
		if(totalCalls.containsKey(op))	{
			calls = totalCalls.get(op);
		}
		totalCalls.put(op, calls + 1);

		updateModelContents();
	}
	
	public String toString()	{
		String to = "Action\tCalls\tAverage(ms)\tTotal(ms)\n";
		if(totalTime.isEmpty())	{
			return "Not started/no results";
		}
		final Set<AnalyticsOperations> keySet = totalTime.keySet();

		for(AnalyticsOperations op: keySet)	{
			int callCount = totalCalls.get(op);
			long callTime = totalTime.get(op)/1000000;//ms conversion
			to = to + op.name() + "\t" + callCount + "\t" + callTime + "ms\t"
					+ (callTime/callCount) + "ms\n"; 

		}
		return to;
	}
}
