package common;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import agent.planning.PlanResult;
import common.UniversalConstants.EntityType;
import jason.asSemantics.Message;
import jason.asSyntax.Literal;
import jason.asSyntax.Structure;
import jason.asSyntax.Term;
import truckworld.env.EnvironmentConstants.ActionResult;

/**
 * 
 * @author Alan White
 * 
 *         Statistics recorder for benchmarking; intended to be linked/used by
 *         an environment
 */
public class StatsRecorder {

	/**
	 * Singleton pattern instance
	 */
	private static StatsRecorder _instance;

	/**
	 * Stats counters for agent loggers
	 */
	private final Map<String, Integer> succeedMaint;
	private final Map<String, Integer> succeedNotMaint;
	private final Map<String, Integer> failedMaint;
	private final Map<String, Integer> failedNotMaint;

	private final Map<String, Integer> messageCount;

	/**
	 * Planning stats
	 */
	private int planningCalls = 0, successfulPlanningCalls = 0, maintCalls = 0, successfulMaintCalls = 0,
			totalLength = 0;
	private double totalPlanningTime = 0;
	/**
	 * Recorded by type
	 */
	private Map<String, Integer> planningCallsPerAgent, succesfullCallsPerAgent, mtCallsPerAgent,
			succesfullMtCallsPerAgent;

	/**
	 * Messaging stats
	 */
	private long lastRecord = System.currentTimeMillis();

	/**
	 * Singleton getter
	 * 
	 * @return
	 */
	public static final StatsRecorder getInstance(String id) {
		if (_instance == null) {
			_instance = new StatsRecorder(id);
		}
		return _instance;
	}

	/**
	 * Singleton getter
	 * 
	 * @return
	 */
	public static final StatsRecorder getInstance() {
		if (_instance == null) {
			_instance = new StatsRecorder("default");
		}
		return _instance;
	}

	private Logger logger;

	/**
	 * Writers for stats / step summaries
	 */
	private BufferedWriter actionStatsWriter, stepStatsWriter, displayRecordWriter, planStatsWriter;

	private String lastStepWritten = "", lastPlanStatsWritten = "";

	/**
	 * Directory for creating logger files
	 */
	private final String directory;

	/**
	 * Used to set time vars
	 */
	private int pulse = 0;

	/**
	 * Private constructor to restrict to singleton instance / 'get' method
	 */
	private StatsRecorder(final String dirPrepend) {
		logger = Logger.getLogger(getClass().getSimpleName());
		logger.setLevel(UniversalConstants.STATS_LOGGER_LEVEL);

		succeedMaint = new HashMap<String, Integer>();
		succeedNotMaint = new HashMap<String, Integer>();
		failedMaint = new HashMap<String, Integer>();
		failedNotMaint = new HashMap<String, Integer>();
		messageCount = new HashMap<>();
		planningCallsPerAgent = new HashMap<String, Integer>();
		succesfullCallsPerAgent = new HashMap<String, Integer>();
		mtCallsPerAgent = new HashMap<String, Integer>();
		succesfullMtCallsPerAgent = new HashMap<String, Integer>();

		initializeMaps();

		String tempDir = new File("").getAbsolutePath() + File.separatorChar + "stats" + File.separatorChar + dirPrepend;

		int i = 1;
		File dir = new File(tempDir+"-"+i);
		while(dir.exists()) {
			if(i > 10) {
				throw new RuntimeException("Can't create directory!");
			}
			dir = new File(tempDir+"-"+(++i));
		}
		directory = dir.getPath();
		logger.info("Creating stats in " + directory);
		final File actionStatsFile = new File(dir.getPath() + File.separatorChar + "stats.txt");
		final File stepStatsFile = new File(dir.getPath() + File.separatorChar + "stepStats.txt");
		final File stepRecordFile = new File(dir.getPath() + File.separatorChar + "record.txt");
		final File planStatsFile = new File(dir.getPath() + File.separatorChar + "planStats.txt");

		try {
			dir.mkdirs();
			actionStatsFile.createNewFile();
			actionStatsWriter = new BufferedWriter(new FileWriter(actionStatsFile));
			actionStatsWriter.write("Step, time (ms), result, agent, actionFunctor, args"); // headers

			stepStatsFile.createNewFile();
			stepStatsWriter = new BufferedWriter(new FileWriter(stepStatsFile));
			stepStatsWriter.write("Step, Requests, Deliveries, Avg distance travelled, Total distance (all), "
					+ "Avg time acting (ms), Total time (all), Avg cost per delivery, Total cost,"
					+ "Healthy, Damaged, Mortal, Roads, Slippery, Flooded, Toxic, Blocked, " + "Junctions, DZs, "
					+ "Activities, Pass, Fail(args), Fail(pre), Failed (exo), Fail(nd), " + "Intentions (Total), "
					+ "Success (Total), Success (Not Maintained), Success (Maintained), "
					+ "Failed (Total), Failed (Not Maintained), Failed (Maintained)");

			stepRecordFile.createNewFile();
			displayRecordWriter = new BufferedWriter(new FileWriter(stepRecordFile));

			planStatsFile.createNewFile();
			planStatsWriter = new BufferedWriter(new FileWriter(planStatsFile));
			planStatsWriter
					.write("Step, Calls, Success, Total gen time, Avg gen time, Total length, Avg length, "
							+ "logisticshq calls, logisticshq success, militaryhq calls, militaryhq success, "
							+ "apc calls, apc success, bulldozer calls, bulldozer success, "
							+ "hazmat calls, hazmat success, helicopter calls, helicopter success, "
							+ "truck calls, truck success" +
							// mt fragment
							"Maint Calls, Maint Success, logisticshq calls, logisticshq success, "
							+ "militaryhq calls, militaryhq success, apc calls, apc success, "
							+ "bulldozer calls, bulldozer success, hazmat calls, hazmat success, "
							+ "helicopter calls, helicopter success, truck calls, truck success");

		} catch (IOException e) {
			JOptionPane.showMessageDialog(null,
					new String[] { e.toString(), "Dir=" + dir.getAbsolutePath(),
							"Action File=" + actionStatsFile.getAbsolutePath(),
							"Step File=" + stepStatsFile.getAbsolutePath() },
					this.getClass().getName(), JOptionPane.ERROR_MESSAGE);
			throw new RuntimeException(e);
		}
	}

	private void initializeMaps() {
		planningCallsPerAgent.put(EntityType.apc.name().toLowerCase(), 0);
		planningCallsPerAgent.put(EntityType.bulldozer.name().toLowerCase(), 0);
		planningCallsPerAgent.put(EntityType.hazmat.name().toLowerCase(), 0);
		planningCallsPerAgent.put(EntityType.truck.name().toLowerCase(), 0);
		planningCallsPerAgent.put(EntityType.helicopter.name().toLowerCase(), 0);
		planningCallsPerAgent.put("logisticshq", 0);
		planningCallsPerAgent.put("militaryhq", 0);
		succesfullCallsPerAgent.put(EntityType.apc.name().toLowerCase(), 0);
		succesfullCallsPerAgent.put(EntityType.bulldozer.name().toLowerCase(), 0);
		succesfullCallsPerAgent.put(EntityType.hazmat.name().toLowerCase(), 0);
		succesfullCallsPerAgent.put(EntityType.truck.name().toLowerCase(), 0);
		succesfullCallsPerAgent.put(EntityType.helicopter.name().toLowerCase(), 0);
		succesfullCallsPerAgent.put("logisticshq", 0);
		succesfullCallsPerAgent.put("militaryhq", 0);

		mtCallsPerAgent.put(EntityType.apc.name().toLowerCase(), 0);
		mtCallsPerAgent.put(EntityType.bulldozer.name().toLowerCase(), 0);
		mtCallsPerAgent.put(EntityType.hazmat.name().toLowerCase(), 0);
		mtCallsPerAgent.put(EntityType.truck.name().toLowerCase(), 0);
		mtCallsPerAgent.put(EntityType.helicopter.name().toLowerCase(), 0);
		mtCallsPerAgent.put("logisticshq", 0);
		mtCallsPerAgent.put("militaryhq", 0);
		succesfullMtCallsPerAgent.put(EntityType.apc.name().toLowerCase(), 0);
		succesfullMtCallsPerAgent.put(EntityType.bulldozer.name().toLowerCase(), 0);
		succesfullMtCallsPerAgent.put(EntityType.hazmat.name().toLowerCase(), 0);
		succesfullMtCallsPerAgent.put(EntityType.truck.name().toLowerCase(), 0);
		succesfullMtCallsPerAgent.put(EntityType.helicopter.name().toLowerCase(), 0);
		succesfullMtCallsPerAgent.put("logisticshq", 0);
		succesfullMtCallsPerAgent.put("militaryhq", 0);
	}

	public synchronized void heartBeat() {
		pulse++;
	}

	public void recordMaintResult(String agName, boolean b) {
		this.maintCalls++;
		if (b) {
			this.successfulMaintCalls++;
		}

		agName = agName.toLowerCase();
		if (agName.startsWith(EntityType.truck.name().toLowerCase())) {
			int count = mtCallsPerAgent.get(EntityType.truck.name().toLowerCase());
			if (!succesfullMtCallsPerAgent.containsKey(EntityType.truck.name().toLowerCase())) {
				succesfullMtCallsPerAgent.put(EntityType.truck.name().toLowerCase(), 0);
			}
			mtCallsPerAgent.put(EntityType.truck.name().toLowerCase(), ++count);

			if (b) {
				count = succesfullMtCallsPerAgent.get(EntityType.truck.name().toLowerCase());
				succesfullMtCallsPerAgent.put(EntityType.truck.name().toLowerCase(), ++count);
			}
		} else if (agName.startsWith(EntityType.apc.name().toLowerCase())) {
			int count = planningCallsPerAgent.get(EntityType.apc.name().toLowerCase());
			if (!succesfullMtCallsPerAgent.containsKey(EntityType.apc.name().toLowerCase())) {
				succesfullMtCallsPerAgent.put(EntityType.apc.name().toLowerCase(), 0);
			}

			mtCallsPerAgent.put(EntityType.apc.name().toLowerCase(), ++count);
			if (b) {
				count = succesfullMtCallsPerAgent.get(EntityType.apc.name().toLowerCase());
				succesfullMtCallsPerAgent.put(EntityType.apc.name().toLowerCase(), ++count);
			}
		}

		else if (agName.startsWith(EntityType.bulldozer.name().toLowerCase())) {
			int count = mtCallsPerAgent.get(EntityType.bulldozer.name().toLowerCase());

			if (!succesfullMtCallsPerAgent.containsKey(EntityType.bulldozer.name().toLowerCase())) {
				succesfullMtCallsPerAgent.put(EntityType.bulldozer.name().toLowerCase(), 0);
			}

			mtCallsPerAgent.put(EntityType.bulldozer.name().toLowerCase(), ++count);
			if (b) {
				count = succesfullMtCallsPerAgent.get(EntityType.bulldozer.name().toLowerCase());
				succesfullMtCallsPerAgent.put(EntityType.bulldozer.name().toLowerCase(), ++count);
			}
		} else if (agName.startsWith(EntityType.hazmat.name().toLowerCase())) {
			int count = mtCallsPerAgent.get(EntityType.hazmat.name().toLowerCase());

			if (!succesfullMtCallsPerAgent.containsKey(EntityType.hazmat.name().toLowerCase())) {
				succesfullMtCallsPerAgent.put(EntityType.hazmat.name().toLowerCase(), 0);
			}

			mtCallsPerAgent.put(EntityType.hazmat.name().toLowerCase(), ++count);
			if (b) {
				count = succesfullMtCallsPerAgent.get(EntityType.hazmat.name().toLowerCase());
				succesfullMtCallsPerAgent.put(EntityType.hazmat.name().toLowerCase(), ++count);
			}
		} else if (agName.startsWith(EntityType.helicopter.name().toLowerCase())) {
			int count = mtCallsPerAgent.get(EntityType.helicopter.name().toLowerCase());

			if (!succesfullMtCallsPerAgent.containsKey(EntityType.helicopter.name().toLowerCase())) {
				succesfullMtCallsPerAgent.put(EntityType.helicopter.name().toLowerCase(), 0);
			}

			mtCallsPerAgent.put(EntityType.helicopter.name().toLowerCase(), ++count);
			if (b) {
				count = succesfullMtCallsPerAgent.get(EntityType.helicopter.name().toLowerCase());
				succesfullMtCallsPerAgent.put(EntityType.helicopter.name().toLowerCase(), ++count);
			}
		} else {
			int count = 0;
			if (mtCallsPerAgent.containsKey(agName)) {
				count = mtCallsPerAgent.get(agName);
			}
			if (!succesfullMtCallsPerAgent.containsKey(agName)) {
				succesfullMtCallsPerAgent.put(agName, 0);
			}

			mtCallsPerAgent.put(agName, ++count);
			if (b) {
				count = succesfullMtCallsPerAgent.get(agName);
				mtCallsPerAgent.put(agName, ++count);
			}
		}
	}

	/**
	 * Records the results of planning operations
	 * 
	 * @param b
	 */
	public void recordPlanningResult(String agName, PlanResult pr) {
		// global stats
		planningCalls++;
		boolean result = pr.getPlan() != null;
		if (result) {
			totalLength += pr.getPlanLength();
		}
		totalPlanningTime += pr.getGenerationTime();

		if (result) {
			successfulPlanningCalls++;
		}
		agName = agName.toLowerCase();
		if (agName.startsWith(EntityType.truck.name().toLowerCase())) {
			int count = planningCallsPerAgent.get(EntityType.truck.name().toLowerCase());
			if (!succesfullCallsPerAgent.containsKey(EntityType.truck.name().toLowerCase())) {
				succesfullCallsPerAgent.put(EntityType.truck.name().toLowerCase(), 0);
			}
			planningCallsPerAgent.put(EntityType.truck.name().toLowerCase(), ++count);

			if (result) {
				count = succesfullCallsPerAgent.get(EntityType.truck.name().toLowerCase());
				succesfullCallsPerAgent.put(EntityType.truck.name().toLowerCase(), ++count);
			}
		} else if (agName.startsWith(EntityType.apc.name().toLowerCase())) {
			int count = planningCallsPerAgent.get(EntityType.apc.name().toLowerCase());
			if (!succesfullCallsPerAgent.containsKey(EntityType.apc.name().toLowerCase())) {
				succesfullCallsPerAgent.put(EntityType.apc.name().toLowerCase(), 0);
			}

			planningCallsPerAgent.put(EntityType.apc.name().toLowerCase(), ++count);
			if (result) {
				count = succesfullCallsPerAgent.get(EntityType.apc.name().toLowerCase());
				succesfullCallsPerAgent.put(EntityType.apc.name().toLowerCase(), ++count);
			}
		}

		else if (agName.startsWith(EntityType.bulldozer.name().toLowerCase())) {
			int count = planningCallsPerAgent.get(EntityType.bulldozer.name().toLowerCase());

			if (!succesfullCallsPerAgent.containsKey(EntityType.bulldozer.name().toLowerCase())) {
				succesfullCallsPerAgent.put(EntityType.bulldozer.name().toLowerCase(), 0);
			}

			planningCallsPerAgent.put(EntityType.bulldozer.name().toLowerCase(), ++count);
			if (result) {
				count = succesfullCallsPerAgent.get(EntityType.bulldozer.name().toLowerCase());
				succesfullCallsPerAgent.put(EntityType.bulldozer.name().toLowerCase(), ++count);
			}
		} else if (agName.startsWith(EntityType.hazmat.name().toLowerCase())) {
			int count = planningCallsPerAgent.get(EntityType.hazmat.name().toLowerCase());

			if (!succesfullCallsPerAgent.containsKey(EntityType.hazmat.name().toLowerCase())) {
				succesfullCallsPerAgent.put(EntityType.hazmat.name().toLowerCase(), 0);
			}

			planningCallsPerAgent.put(EntityType.hazmat.name().toLowerCase(), ++count);
			if (result) {
				count = succesfullCallsPerAgent.get(EntityType.hazmat.name().toLowerCase());
				succesfullCallsPerAgent.put(EntityType.hazmat.name().toLowerCase(), ++count);
			}
		} else if (agName.startsWith(EntityType.helicopter.name().toLowerCase())) {
			int count = planningCallsPerAgent.get(EntityType.helicopter.name().toLowerCase());

			if (!succesfullCallsPerAgent.containsKey(EntityType.helicopter.name().toLowerCase())) {
				succesfullCallsPerAgent.put(EntityType.helicopter.name().toLowerCase(), 0);
			}

			planningCallsPerAgent.put(EntityType.helicopter.name().toLowerCase(), ++count);
			if (result) {
				count = succesfullCallsPerAgent.get(EntityType.helicopter.name().toLowerCase());
				succesfullCallsPerAgent.put(EntityType.helicopter.name().toLowerCase(), ++count);
			}
		} else {
			int count = 0;
			if (planningCallsPerAgent.containsKey(agName)) {
				count = planningCallsPerAgent.get(agName);
			}
			if (!succesfullCallsPerAgent.containsKey(agName)) {
				succesfullCallsPerAgent.put(agName, 0);
			}

			planningCallsPerAgent.put(agName, ++count);
			if (result) {
				count = succesfullCallsPerAgent.get(agName);
				succesfullCallsPerAgent.put(agName, ++count);
			}
		}

	}

	public void recordGoalResult(String agentId, Literal task, boolean result, boolean maintained) {
		// summary stats
		int sm = 0, snm = 0, fm = 0, fnm = 0;
		if (succeedMaint.containsKey(agentId)) {
			sm = succeedMaint.get(agentId);
		}
		if (succeedNotMaint.containsKey(agentId)) {
			snm = succeedNotMaint.get(agentId);
		}
		if (failedMaint.containsKey(agentId)) {
			fm = failedMaint.get(agentId);
		}
		if (failedNotMaint.containsKey(agentId)) {
			fnm = failedNotMaint.get(agentId);
		}

		if (result && maintained) {
			sm++;
			succeedMaint.put(agentId, sm);
		}
		if (result && !maintained) {
			snm++;
			succeedNotMaint.put(agentId, snm);
		}
		if (!result && maintained) {
			fm++;
			failedMaint.put(agentId, fm);
		}
		if (!result && !maintained) {
			fnm++;
			failedNotMaint.put(agentId, fnm);
		}

		try {
			actionStatsWriter.newLine();
			// i.e. intended for CSV
			String args = "";
			for (Term t : task.getTerms()) {
				args = args + " " + t.toString().replaceAll("\"", "");
			}
			String resString = "pass";
			if (!result) {
				resString = "failed";
			}
			if (maintained) {
				resString = resString + " (Maintained)";
			}
			actionStatsWriter.write(System.currentTimeMillis() + ", " + resString + "," + agentId + ", "
					+ task.getFunctor() + ", " + args);
		} catch (Exception e) {
			String s = "";
			StackTraceElement[] ste = e.getStackTrace();
			for (int i = 0; i < ste.length; i++) {
				s = s + "\n" + ste[i];
			}
			logger.severe(e.getMessage() + s);
		}
	}

	public void recordResult(String agName, Structure action, ActionResult result) {
		logger.info(agName + " " + action + ", " + result);
		try {
			actionStatsWriter.newLine();
			// i.e. intended for CSV
			String args = "";
			for (Term t : action.getTerms()) {
				args = args + " " + t.toString().replaceAll("\"", "");
			}
			args.substring(1);

			actionStatsWriter.write(pulse + ", " + System.currentTimeMillis() + ", " + result.name() + "," + agName
					+ ", " + action.getFunctor() + ", " + args);
		} catch (Exception e) {
			String s = "";
			StackTraceElement[] ste = e.getStackTrace();
			for (int i = 0; i < ste.length; i++) {
				s = s + "\n" + ste[i];
			}
			logger.severe(e.getMessage() + s);
		}
	}

	public void recordString(String rec) {
		try {
			displayRecordWriter.newLine();
			displayRecordWriter.write(rec);

		} catch (Exception e) {
			String s = "";
			StackTraceElement[] ste = e.getStackTrace();
			for (int i = 0; i < ste.length; i++) {
				s = s + "\n" + ste[i];
			}
			logger.severe(e.getMessage() + s);
		}
	}

	public void recordStepSummary(int totalCargoReq, int delivered, int healthV, int dmgV, int mortV, int numRoads,
			int numSlipperyRoads, int numFloodedRoads, int numToxicRoads, int numBlockedRoads, int numJunctions,
			int numDz, int actionsTried, int actionsPassed, int failedDueToArgs, int failedDueToPre,
			int failedDueToExog, int failedDueToStochastic, float avgD, float totalD, float avgAct, long totAct,
			int totalCost, float averageCostPerDelivery) {
		try {
			// csv format, again
			String stepStatsToWrite = totalCargoReq + ", " + delivered + ", " + avgD + ", " + totalD + ", " + avgAct
					+ ", " + totAct + ", " + averageCostPerDelivery + ", " + totalCost + "," + +healthV + ", " + dmgV
					+ ", " + mortV + ", " + numRoads + "," + numSlipperyRoads + ", " + numFloodedRoads + ", "
					+ numToxicRoads + ", " + numBlockedRoads + ", " + numJunctions + ", " + numDz + ", " + actionsTried
					+ ", " + actionsPassed + ", " + failedDueToArgs + ", " + failedDueToPre + ", " + failedDueToExog
					+ ", " + failedDueToStochastic;

			// do agents intentions summary...
			int sucNM = 0, sucM = 0, failM = 0, failNM = 0;
			for (Integer i : succeedMaint.values()) {
				sucNM += i;
			}
			for (Integer i : succeedNotMaint.values()) {
				sucM += i;
			}
			for (Integer i : failedMaint.values()) {
				failM += i;
			}
			for (Integer i : failedNotMaint.values()) {
				failNM += i;
			}
			int sucTot = sucM + sucNM;
			int failTot = failM + failNM;
			int total = sucTot + failTot;
			// "Intentions (Total), Success (Total), Success (Not Maintained),
			// Success (Maintained), " +
			// "Failed (Total), Failed (Not Maintained), Failed (Maintained)"
			final String agentStats = total + ", " + sucTot + ", " + sucNM + ", " + sucM + ", " + failTot + ", " + failNM
					+ ", " + failM;
			stepStatsToWrite = stepStatsToWrite + "," + agentStats;

			if (!lastStepWritten.equals(stepStatsToWrite)) {
				stepStatsWriter.newLine();
				stepStatsWriter.write(pulse + ", " + stepStatsToWrite);
				lastStepWritten = stepStatsToWrite;
			}

			// do planning stats
			String pStats = planningCalls + "," + successfulPlanningCalls + ",";

			// time, avg/length, avg
			// avoid div by 0
			double averagePlanningCalls = 0;
			float averagePlanLength = 0;
			if (planningCalls > 0) {
				averagePlanningCalls = (totalPlanningTime / planningCalls);
				averagePlanLength = (totalLength / planningCalls);
			}

			pStats = pStats + totalPlanningTime + "," + averagePlanningCalls + "," + totalLength + ","
					+ averagePlanLength + "," + planningCallsPerAgent.get("logisticshq") + ","
					+ succesfullCallsPerAgent.get("logisticshq") + "," + planningCallsPerAgent.get("militaryhq") + ","
					+ succesfullCallsPerAgent.get("militaryhq") + "," + planningCallsPerAgent.get(EntityType.apc.name())
					+ "," + succesfullCallsPerAgent.get(EntityType.apc.name()) + ","
					+ planningCallsPerAgent.get(EntityType.bulldozer.name()) + ","
					+ succesfullCallsPerAgent.get(EntityType.bulldozer.name()) + ","
					+ planningCallsPerAgent.get(EntityType.hazmat.name()) + ","
					+ succesfullCallsPerAgent.get(EntityType.hazmat.name()) + ","
					+ planningCallsPerAgent.get(EntityType.helicopter.name()) + ","
					+ succesfullCallsPerAgent.get(EntityType.helicopter.name()) + ","
					+ planningCallsPerAgent.get(EntityType.truck.name()) + ","
					+ succesfullCallsPerAgent.get(EntityType.truck.name()) + "," + maintCalls + ","
					+ successfulMaintCalls + "," + mtCallsPerAgent.get("logisticshq") + ","
					+ succesfullMtCallsPerAgent.get("logisticshq") + "," + mtCallsPerAgent.get("militaryhq") + ","
					+ succesfullMtCallsPerAgent.get("militaryhq") + "," + mtCallsPerAgent.get(EntityType.apc.name())
					+ "," + succesfullMtCallsPerAgent.get(EntityType.apc.name()) + ","
					+ mtCallsPerAgent.get(EntityType.bulldozer.name()) + ","
					+ succesfullMtCallsPerAgent.get(EntityType.bulldozer.name()) + ","
					+ mtCallsPerAgent.get(EntityType.hazmat.name()) + ","
					+ succesfullMtCallsPerAgent.get(EntityType.hazmat.name()) + ","
					+ mtCallsPerAgent.get(EntityType.helicopter.name()) + ","
					+ succesfullMtCallsPerAgent.get(EntityType.helicopter.name()) + ","
					+ mtCallsPerAgent.get(EntityType.truck.name()) + ","
					+ succesfullMtCallsPerAgent.get(EntityType.truck.name());

			if (!pStats.equals(lastPlanStatsWritten)) {
				planStatsWriter.newLine();
				planStatsWriter.write(pulse + "," + pStats);
				lastPlanStatsWritten = pStats;
			} else {
				logger.finest("Duplicate\n" + pStats + "\nequals\n" + lastPlanStatsWritten);
			}
		} catch (Exception e) {
			String s = "";
			StackTraceElement[] ste = e.getStackTrace();
			for (int i = 0; i < ste.length; i++) {
				s = s + "\n" + ste[i];
			}
			logger.severe(e.getMessage() + s);
		}
	}

	public void recordSentMessage(final Message m) {
		final String performative = m.getIlForce();
		final int sum = messageCount.containsKey(performative) ? messageCount.get(performative) : 0;
		messageCount.put(performative, sum + 1);
	}

	public void flush() {
		try {
			actionStatsWriter.flush();
			stepStatsWriter.flush();
			displayRecordWriter.flush();
			planStatsWriter.flush();
		} catch (final IOException e) {
			logger.severe(e.toString());
		}
	}
	
	/**
	 * Write out a summary of recorded statistics to file; called on killing the
	 * object
	 */
	public void close() {
		try {
			writeMsgStats();
			actionStatsWriter.close();
			stepStatsWriter.close();
			displayRecordWriter.close();
			planStatsWriter.close();
		} catch (final IOException e) {
			logger.severe(e.toString());
			throw new RuntimeException(e);
		}
	}

	private void writeMsgStats() throws IOException {
		final File msgStatsFile = new File(directory + File.separatorChar + "msgStats.txt");
		final BufferedWriter msgStatsWriter = new BufferedWriter(new FileWriter(msgStatsFile));
		final StringBuffer heading = new StringBuffer();
		final StringBuffer stats = new StringBuffer();
		final Iterator<String> keys = messageCount.keySet().iterator();
		while (keys.hasNext()) {
			final String key = keys.next();
			heading.append(key + (keys.hasNext() ? ", " : ""));
			stats.append(messageCount.get(key) + (keys.hasNext() ? ", " : ""));
		}
		writeAndCloseMessageStats(msgStatsWriter, heading, stats);
	}

	private void writeAndCloseMessageStats(final BufferedWriter msgStatsWriter, final StringBuffer heading,
			final StringBuffer stats) throws IOException {
		msgStatsWriter.append(heading);
		msgStatsWriter.newLine();
		msgStatsWriter.append(stats);
		msgStatsWriter.flush();
		msgStatsWriter.close();
	}

	@Override
	protected void finalize() throws Throwable {
		try {
			close();
			super.finalize();
		} catch (Throwable e) {
			String stack = "";
			for (StackTraceElement ste : e.getStackTrace()) {
				stack = stack + "\n" + ste;
			}
			logger.severe(e.getMessage() + stack);
			throw e;
		}

	}

}
