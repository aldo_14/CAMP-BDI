package agent.type;

import java.util.HashMap;
import java.util.Map;

import agent.maintenance.ContinualMaintenancePolicy;
import agent.maintenance.DefaultMaintenancePolicy;
import agent.maintenance.MaintenancePolicy;
import agent.maintenance.NoHandleMaintenancePolicy;
import agent.maintenance.ReplanMaintenancePolicy;
import common.UniversalConstants;
import jason.asSyntax.Literal;

/**
 * Abstract class encapsulating MP holding and access
 */
public abstract class MaintenancePolicyHolder extends CapabilityAwareAgent {
	
	/**
	 * Maps a policy to a capability; keyed by capability name (string) OR by 'DEFAULT'.
	 */
	private final Map<String, MaintenancePolicy> policyMap = new HashMap<String, MaintenancePolicy>();
	
	/**
	 * Do we have a specifically associated MP?
	 */
	protected boolean hasMaintenancePolicy(String functor)	{	return policyMap.containsKey(functor);	}


	/**
	 * Fetch mp by action, using the functor
	 * @param action Literal
	 * @return MaintenancePolicy as per getMaintenancePolicy(String)
	 */
	public MaintenancePolicy getMaintenancePolicy(Literal action){
		return getMaintenancePolicy(action.getFunctor());
	}
	
	/**
	 * Returns the maintenance policy associated with the given name, or default if none exists
	 * @param c String; the task signatures used as a key
	 * @return MaintenancePolicy object
	 */
	public MaintenancePolicy getMaintenancePolicy(String functor){
		//for experimentation - lazy shortcut
		if( this instanceof MaintainingAgent)	{
			MaintainingAgent castMa = (MaintainingAgent) this;
			if (!castMa.planConfConst() && !castMa.isMaintaining()) {
				return castMa.isReplanning() ? new ReplanMaintenancePolicy() : new NoHandleMaintenancePolicy();
			}
			if(castMa.isContinualMaintenance() && castMa.isMaintaining()) {
				return new ContinualMaintenancePolicy();
			}
			if (hasMaintenancePolicy(functor)) {
				return policyMap.get(functor);
			} else if (hasMaintenancePolicy(UniversalConstants.DEFAULT_MP_KEY)) {
				return policyMap.get(UniversalConstants.DEFAULT_MP_KEY);
			}
		}
		return new DefaultMaintenancePolicy();
	}

	/**
	 * Adds an mp to the mp set
	 * @param cap capability this policy applies to
	 * @param mp
	 */
	public void addMaintenancePolicy(String cap, MaintenancePolicy mp){	policyMap.put(cap, mp);	}

	/**
	 * Removes the MP for the specified capability, so long as cap != DEFAULT_CAPABILITY_NAME
	 * @param cap name of capability to remove this policy for
	 */
	protected void removeMaintenancePolicy(String cap){ 
		if(!cap.equals(UniversalConstants.DEFAULT_MP_KEY))	{	policyMap.remove(cap);	}
		else	{ logger.warning("Attempted to remove default maintenace policy from mp set!");	}
	}
}
