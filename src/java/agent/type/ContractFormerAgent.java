package agent.type;
import static agent.AgentUtils.formUnifierFromArgs;
import static agent.type.MessageTypes.IL_CONFIRM_OBL_CONTRACT;
import static agent.type.MessageTypes.IL_CONTRACT_CHANGED;
import static agent.type.MessageTypes.IL_DEPENDENCY_CANCEL;
import static agent.type.MessageTypes.IL_DEPENDENCY_REQ;
import static agent.type.MessageTypes.IL_DEP_MAINTAINED;
import static agent.type.MessageTypes.IL_OBLIGANT_ACCEPT;
import static agent.type.MessageTypes.IL_OBLIGANT_REFUSE;
import static agent.type.MessageTypes.IL_OB_FAILED;
import static agent.type.MessageTypes.IL_OB_MAINTAINED;
import static agent.type.MessageTypes.IL_OB_SUCCEED;
import static truckworld.env.Percepts.BUSY;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Level;

import javax.swing.JOptionPane;

import agent.maintenance.MaintenancePolicy;
import agent.model.capability.Capability;
import agent.model.capability.CompositeCapability;
import agent.model.capability.ConfidencePrecondition;
import agent.model.capability.ExternalCapability;
import agent.model.dependency.Contract;
import agent.model.goal.GoalTask;
import agent.type.arch.MultiagentArch;
import agent.ui.CaaUiWindow;
import common.StatsRecorder;
import common.UniversalConstants;
import jason.asSemantics.ActionExec;
import jason.asSemantics.Agent;
import jason.asSemantics.Circumstance;
import jason.asSemantics.IntendedMeans;
import jason.asSemantics.Intention;
import jason.asSemantics.Message;
import jason.asSemantics.Option;
import jason.asSemantics.Unifier;
import jason.asSyntax.ASSyntax;
import jason.asSyntax.Literal;
import jason.asSyntax.LogExpr;
import jason.asSyntax.LogicalFormula;
import jason.asSyntax.NumberTerm;
import jason.asSyntax.Plan;
import jason.asSyntax.PlanBody;
import jason.asSyntax.PlanBodyImpl;
import jason.asSyntax.Term;
import jason.asSyntax.Trigger.TEOperator;
import jason.asSyntax.Trigger.TEType;
import jason.asSyntax.parser.ParseException;
import jason.bb.BeliefBase;

/**
 * 
 * @author Alan White
 * 
 *         Abstract class for agents which form <i>Dependency</i> or
 *         <i>Obligation</i> contracts - preemptive agreements to perform some
 *         task on request, and to observed a particular maintenance policy in
 *         order to fulfill them.
 */
public abstract class ContractFormerAgent extends ContractHoldingAgent {
	protected boolean sendUpdateMessages = false;

	/**
	 * EXtends select option with a filtering mechanism that aims to remove
	 * plans with confidence below the maintenance threshold. If no plans exist
	 * above this threshold, we will attempt a lower confidence plan on the
	 * proviso that maintenance should repair it
	 */
	@Override
	public Option selectOption(List<Option> options) {
		List<Option> filtered = new Vector<Option>();

		// remove confidence check for replanning agents, as these aren't
		// supposed to employ
		// any confidence knowledge
		if (this instanceof MaintainingAgent) {
			MaintainingAgent ma = (MaintainingAgent) this;
			if (ma.isReplanning() && !ma.planConfConst()) {
				return super.selectOption(options);
			}
		}

		Option best = null;
		float bestC = -1;
		for (Option o : options) {
			if (o.getPlan().getTrigger().getType().equals(TEType.achieve)
					&& o.getPlan().getTrigger().getOperator().equals(TEOperator.del)) {
				logger.severe("Failure case for plan, don't bother with confidence....\n" + o.getPlan().toASString());
				return o; // prioritise failure response over all other options
			}

			Plan p = (Plan) o.getPlan().clone();
			p.apply(o.getUnifier());
			float conf = calculatePlanConfidence(p, getBB().clone());
			float mt = getMaintenancePolicy(p.getTrigger().getLiteral()).maintenanceAcceptanceThreshold();

			// special case for replanning - we don't filter out, just select
			// the BEST option
			if (this instanceof MaintainingAgent) {
				MaintainingAgent ma = (MaintainingAgent) this;
				if (ma.isReplanning() && !ma.planConfConst()) {
					mt = 0;
				}
			}

			// NB: -1 returned if can't calculate confidence
			if (conf > mt || conf == -1) {
				filtered.add(o);
			} else {
				logger.fine("Option confidence is too low; " + conf + "<=" + mt + " for\n" + p.toASString());
			}

			if (conf > bestC) {
				best = o;
				bestC = conf;
			}
		}

		if (filtered.isEmpty() && !options.isEmpty() && best == null) {
			logger.severe(options + " filtering; No options found!");
			StatsRecorder.getInstance()
					.recordString(dateFormat.format(new Date()) + "; " + options + " filtering; No options found!");
			return super.selectOption(options);
		} else if (filtered.isEmpty() && !options.isEmpty() && best != null) {
			logger.severe(options + " filtering; No options found, but got best; " + best);
			return best;
		}

		// else, sort by cost...
		logger.info("Original option list; " + options + "\n" + "Filtered option list; " + filtered);

		// select best
		return findCheapestOption(filtered);
	}

	private Option findCheapestOption(List<Option> filtered) {
		Option cheapest = null;
		int cheapestCost = Integer.MAX_VALUE;
		for (Option o : filtered) {
			Plan p = (Plan) o.getPlan().clone();
			p.apply(o.getUnifier());
			int cost = calculatePlanCost(p, getBB().clone());
			if (cost < cheapestCost & cost >= 0) {
				logger.finer("Got a new cheapest option; " + o + "(" + cost + ")");
				cheapestCost = cost;
				cheapest = o;
			}
		}
		return cheapest;
	}

	/**
	 * Adds in a special check for orphaned dependencies...
	 * 
	 * @param is
	 * @return
	 */
	@Override
	public Intention selectIntention(Queue<Intention> is) {
		checkExistingDependencies();
		return super.selectIntention(is);
	}

	/**
	 * Debugging activity; checks whether we have any dependencies that don't
	 * match to existing intentions
	 */
	private void checkExistingDependencies() {
		if (!getDependencies().isEmpty()) {
			cancelDeadDependencies(getCurrentIntentionIds());
		}
	}

	private Collection<Integer> getCurrentIntentionIds() {
		final Collection<Integer> ids = new Vector<Integer>();
		final Circumstance c = getTS().getC();
		if (c.getSelectedIntention() != null) {
			ids.add(c.getSelectedIntention().getId());
		}
		if (!c.getPendingIntentions().isEmpty()) {
			for (Intention i : c.getPendingIntentions().values()) {
				if (!ids.contains(i.getId())) {
					ids.add(i.getId());
				}
			}
		}
		if (!c.getIntentions().isEmpty()) {
			for (Intention i : c.getIntentions()) {
				if (!ids.contains(i.getId())) {
					ids.add(i.getId());
				}
			}
		}
		if (!c.getPendingActions().isEmpty()) {
			for (ActionExec ae : c.getPendingActions().values()) {
				Intention i = ae.getIntention();
				if (!ids.contains(i.getId())) {
					ids.add(i.getId());
				}
			}
		}
		logger.finer("Got ids set: " + ids + " in \n" + c);
		return ids;
	}

	private void cancelDeadDependencies(final Collection<Integer> currentIntentionIds) {
		final Vector<Literal> cancelList = new Vector<Literal>();

		if (currentIntentionIds.isEmpty()) {
			cancelList.addAll(getDependencies());
		} else { // ok, check intentions vs dependencies
			for (Literal dep : getDependencies()) {
				// get id of intention from the dependency object
				List<Term> annot = ((Literal) dep.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT_INTENTION).get(0))
						.getTerms();
				logger.finest("Annot ids; " + annot + " for " + dep);
				// check last entry
				int intId = new Integer((int) ((NumberTerm) annot.get(annot.size() - 1)).solve());
				if (!currentIntentionIds.contains(intId)) {
					logger.severe("Cannot match intention id to our intention list " + currentIntentionIds
							+ ", add to cancel list");
					cancelList.add(dep);
				}
			}

			if (!cancelList.isEmpty()) {
				logger.fine("Need to cancel the following orphan dependencies; " + cancelList);
				for (Literal l : cancelList) {
					cancelDependency(l, false);
				}
			} else {
				logger.finest("No cancellations detected in " + getDependencies());
			}
		}
	}

	/**
	 * Prepend for intentions suspended during their contract formation (wait
	 * for reply) period
	 */
	protected final String INTENTION_SUSPENDED = "suspendedForContract";

	/*
	 * =========================================================================
	 * ===============================
	 * 
	 * Constants
	 * 
	 * =========================================================================
	 * ===============================
	 */

	/**
	 * Use to constrain how many steps into the future we go when estimating
	 * confidence
	 */
	protected int intentionConfidenceLookahead;

	/*
	 * =========================================================================
	 * ======================================
	 * 
	 * Variables
	 * 
	 * =========================================================================
	 * ====================================
	 */

	/**
	 * Data object used to prevent infinite looping when forming contracts;
	 * records when an attempt fails <i>immediately prior</i> to executing said
	 * action.
	 */
	protected List<Literal> contractRejectImmediatelyPriorToExec = new Vector<Literal>();

	/*
	 * =========================================================================
	 * ======================================
	 * 
	 * Temporary data structures used in contract formation
	 * 
	 * =========================================================================
	 * ====================================
	 */

	/**
	 * This maps an intention (by int id) to a set of literals corresponding to
	 * ongoing dependency/obligation formation processes; once the entry is
	 * empty (dependencies all formed / failed), then the intention can be
	 * resumed. Note that this only maps the <i>ongoing</i> messaging process,
	 * it is not a way to store permanently which actions are to be delegated.
	 */
	private HashMap<Integer, Collection<Literal>> waitingForDep = new HashMap<Integer, Collection<Literal>>();

	/**
	 * literals waiting for contract responses; intention ID should be set in
	 * the annotations
	 */
	private HashMap<Literal, Collection<String>> waitForOblAccept = new HashMap<Literal, Collection<String>>();

	/**
	 * Used to temporarily hold contracts returned by the primary until all
	 * obligants accept and (ergo) can set this as the dependency contract.
	 * Obligants don't need to temp-store contracts as they receive a confirmed
	 * contract in the <i>setObligationContract</i> message sent by the
	 * dependant.
	 */
	private HashMap<Literal, Contract> proposedContractFromObl = new HashMap<Literal, Contract>();

	/*
	 * =========================================================================
	 * =======================================
	 *
	 * Methods...
	 *
	 * =========================================================================
	 * =====================================
	 */

	/**
	 * Synchronization object for 'buf' method calls
	 */
	private Object bufSych = new Object();

	private List<Literal> lastPercepts = new Vector<Literal>();
	
	/**
	 * Allows us to 'turn off' auto-send of capability state.
	 */
	public void buf(final List<Literal> percepts, final boolean sendCb, final String reason) {
		synchronized (bufSych) {
			if (percepts != null && !percepts.isEmpty() && !equalPerceptSets(percepts, lastPercepts)) {
				// create copy to avert concurrentModificationException
				final List<Literal> perceptCopy = new Vector<Literal>();
				for (int i = 0; i < percepts.size(); i++) {
					perceptCopy.add(percepts.get(i).copy());
				}
				super.buf(perceptCopy);
				if (sendCb) {
					logger.severe("Buf is using sendCb\n" + reason );
					setExternalCapabilities();
				}
				lastPercepts.clear();
				lastPercepts.addAll(percepts);
			}
		}
	}

	private boolean equalPerceptSets(final List<Literal> perceptsRcv, final List<Literal> previous) {
		final List<String> rcvString = convertToString(perceptsRcv);
		final List<String> previousString = convertToString(previous);
		for (final String lit : rcvString) {
			if (!previousString.contains(lit)) {
				logger.info("New percept: " + lit);
				return false;
			}
		}
		return true;
	}

	private List<String> convertToString(final List<? extends Object> toConvert) {
		final List<String> stringContents = new Vector<String>();
		if(toConvert != null) {
			for(final Object o: toConvert) {
				stringContents.add(o.toString());
			}
		}
		return stringContents;
	}

	/**
	 * Override to check capability confidence after we've updated the BB with
	 * the new percepts
	 */
	@Override
	public void buf(final List<Literal> percepts) {
		buf(percepts, true, "Default call");
	}

	@Override
	public void setExternalCapabilities() {
		super.setExternalCapabilities();

		// check obligations
		if (!getObligations().isEmpty() && this.sendUpdateMessages) {
			logger.info("Checking obligation confidence..." + getObligations());
			Set<Literal> obs = getObligations();
			for (Literal o : obs) {
				if (hasObligationContract(o) && !isExecuting(o)) {
					Contract c = getObligationContract(o);

					if (c.getExternalCapability() != null) {
						// Won't break from an NPE with the EC
						BeliefBase execContext;
						synchronized (getBB()) {
							execContext = insertChangeSetIntoBB(c.getBeliefChangesFromDependent(), getBB().clone());
						}
						float newCost = Integer.MAX_VALUE; // will be set
						float currConf = c.getExternalCapability().getGeneralConfidence(execContext);
						float newConf = -1; // will be set...

						// pending action / executing; use to set confidence
						if (getTS().getC().getPendingIntentions().containsKey(o.toString())) {
							Plan pl = (Plan) getTS().getC().getPendingIntentions().get(o.toString()).peek().getPlan()
									.clone();
							newConf = calculatePlanConfidence(pl, execContext);
							newCost = calculatePlanCost(pl, execContext);
							logger.info(o + " got pending intention for confidence revision: " + pl + ", conf="
									+ newConf + ", old=" + currConf + " (Change=" + (currConf - newConf) + ")");
						} else {
							logger.info(o + " pending cannot be found...");
							// use general estimate
							Capability estC = this.getCapability(o.getFunctor());
							if (estC == null) {
								logger.severe("Cannot find capability for obligation " + o);
							} else {
								newConf = estC.getGeneralConfidence(execContext);
								newCost = estC.getGeneralCostEstimate();
							}
						}

						if (newConf != currConf) {
							logger.info(o + "Change in confidence from " + currConf + " to " + newConf);
							ExternalCapability current = c.getExternalCapability();
							ExternalCapability newEc = new ExternalCapability(current.getSignature(),
									current.achieves(), current.getHolder(),
									new ConfidencePrecondition(current.getHolderName(), current.getSignature(),
											current.getPreconditions()[0], newConf),
									current.getPostEffects(), newCost, current.isPrimitive());
							c.setExternalCapability(newEc);
						}
					}
				} else if (!hasObligationContract(o)) {
					logger.warning("Set ec; No contract for " + o);
				} else if (isExecuting(o)) {
					logger.warning("Set ec;" + o + " is executing\n" + ts.getC());
				}
			}
		}
	}

	/**
	 * Extend selectMessage with internal handling of contract request messages
	 */
	@Override
	public Message selectMessage(Queue<Message> messages) {
		synchronized (messages) {
			logger.info("Handling messages; " + messages);
			// iterate through and handle/remove contract request messages
			Vector<Message> removed = new Vector<Message>();
			// update perception - for handling dependency messages
			List<Literal> perceive = getTS().getUserAgArch().perceive();
			if (perceive != null && !perceive.isEmpty()) {
				buf(perceive, false, "Start of selectMessages; percepts " + perceive);
			}
			for (Message m : messages) {
				if (m.getIlForce().equals(IL_DEPENDENCY_CANCEL)) {
					logger.finer("Handling " + m.getIlForce() + " message " + m.toString());
					handleObligationCancellationMsg(m);
					removed.add(m);
				} else if (m.getIlForce().equals(IL_DEPENDENCY_REQ)) {
					logger.finer("Handling " + m.getIlForce() + " message " + m.toString());
					handleDependencyRequestMsg(m);
					removed.add(m);
				} else if (m.getIlForce().equals(IL_OBLIGANT_ACCEPT)) {
					logger.finer("Handling " + m.getIlForce() + " message " + m.toString());
					handleObligantAcceptanceMsg(m);
					removed.add(m);
				} else if (m.getIlForce().equals(IL_OBLIGANT_REFUSE)) {
					logger.finer("Handling " + m.getIlForce() + " message " + m.toString());
					handleObligantRefusalMsg(m);
					removed.add(m);
				} else if (m.getIlForce().equals(IL_CONFIRM_OBL_CONTRACT)) {
					logger.finer("Handling " + m.getIlForce() + " message " + m.toString());
					handleSetObligationContractMsg(m);
					removed.add(m);
				} else if (m.getIlForce().equals(IL_CONTRACT_CHANGED)) {
					logger.finer("Handling " + m.getIlForce() + " message " + m.toString());
					handleUpdateContractMsg(m);
					removed.add(m);
				} else if (m.getIlForce().equals(IL_OB_MAINTAINED)) {
					logger.finer("Handling " + m.getIlForce() + " message " + m.toString());
					handleObligationMaintained(m);
					removed.add(m);
				}
				/*
				 * This type is handled elsewhere, but included here to prevent
				 * kqml errors
				 */
				else if (m.getIlForce().equals(IL_DEP_MAINTAINED)) {
					logger.finer("Removing " + m.getIlForce() + " message " + m.toString());
					removed.add(m);
				}
				// else {}//do nothing
				if (ui != null) {
					ui.repaint();
				}
			}
			messages.removeAll(removed);
			return super.selectMessage(messages);
		}
	}

	/**
	 * Extends to include obligation info as annotations to the signature
	 */
	@Override
	protected Collection<ExternalCapability> getEcsToAdvertise() {
		Collection<ExternalCapability> ecs = super.getEcsToAdvertise();
		for (ExternalCapability e : ecs) {
			if (!getObligations().isEmpty()) {
				for (Literal ob : getObligations()) {
					e.getSignature().addAnnots(ob.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT));
					e.getSignature().addAnnots(ob.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT_INTENTION));
				}
			}
			logger.finest("Register ec: " + e + "\n\tobligations are " + getObligations());
		}
		return ecs;
	}

	/**
	 * Returns true if we are waiting upon some contracts forming...
	 * 
	 * @return
	 */
	public final boolean waitingOnContractsToForm() {
		return !waitForOblAccept.isEmpty();
	}

	/**
	 * Get the set of activities and obligants when waiting for contract
	 * responses
	 * 
	 * @return
	 */
	public final HashMap<Literal, Collection<String>> contractWaitList() {
		return waitForOblAccept;
	}

	/**
	 * Get the list of agents waiting for an obligation confirmation
	 * 
	 * @param l
	 *            - the activity being delegated
	 * @return
	 */
	public final Collection<String> actionWaitList(Literal l) {
		Collection<String> rt = waitForOblAccept.get(l);
		if (rt == null) {
			rt = new Vector<String>();
		}
		return rt;
	}

	/**
	 * Extends the default logic to reject any obligation exec. requests that
	 * don't have a corresponding obligation contract. If we do have an
	 * obligation, return the result of the parent implementation.
	 * 
	 * @param task
	 * @return boolean
	 */
	protected boolean canPerformObligation(Literal task) {
		if (!hasObligationContract(task)) {
			logger.warning("Unable to handle " + task + " as we do not have a corresponding" + " obligation contract:"
					+ getObligations());
			return false;
		} else {
			return super.canPerformObligation(task);
		}
	}

	// Methods to handle messaging below...

	/**
	 * Handle a message confirming the contract (MP) of an obligation; this
	 * message is sent by the dependant to all obligants once the primary
	 * obligant (the actor in our model of execution) has confirmed it. </br>
	 * </br>
	 * This message thus acts as a formal confirmation of the obligation. </br>
	 * </br>
	 * It can also be used in other potential implementations to 'force' a
	 * particular MP, or to indicate the result of multi-party negotiations. In
	 * any case, this is a formality in this situation, where only the first
	 * named obligant actually performs the action and the rest remain 'free'.
	 * 
	 * @param m
	 */
	private void handleSetObligationContractMsg(Message m) {
		Contract c = ((Contract) m.getPropCont());
		Literal action = c.getActivity();
		setObligationContract(action, c);
		logger.finer("Set obligation contract for " + action + "; " + c.toString());

		// send first capability advertisement
		Intention pend = getTS().getC().getPendingIntentions().get(action.toString());
		if (pend == null) {
			logger.severe("Pending intention not found for obligation " + action + "\n" + getTS().getC());
		} else {
			BeliefBase execContext = getBB().clone();
			execContext = insertChangeSetIntoBB(c.getBeliefChangesFromDependent(), execContext);
			// confidence advertisement check
			float conf = calculateIntentionConfidence(pend, execContext);

			if (conf < 0 && hasCapability(action.getFunctor())) {
				logger.warning("Unable to determine specific confidence, returning a general value " + "(" + conf
						+ ") for " + action);
				conf = getCapability(action.getFunctor()).getGeneralConfidence(execContext);
			}
			logger.finer("Sending confidence update for accepted obligation " + action + ", conf=" + conf);
			updateObligationConfidence(action, conf);
		}
	}

	/**
	 * Handles messages updating the BB contract with new provided states
	 * 
	 * @param m
	 */
	private void handleUpdateContractMsg(Message m) {
		try {
			if (m.getSender().equals(this.getId())) {
				logger.warning("Sent contract to self!\n" + m);
				return;
			}

			Contract c = ((Contract) m.getPropCont());
			Literal a = c.getActivity();
			if (hasObligationContract(a)) {
				getObligationContract(a).setBeliefChangesFromDependent(c.getBeliefChangesFromDependent());
				// update pending intention
				if (!isExecuting(a)) { // if we aren't yet executing...
					if (getTS().getC().getPendingIntentions().containsKey(a.toString())) {
						Intention pend = getTS().getC().getPendingIntentions().get(a.toString());
						logger.fine("Pending intention exists for " + a + "\n" + pend);
						Plan pp = pend.peek().getPlan();
						Literal tl = pp.getTrigger().getLiteral();
						Capability ppc = getCapability(tl.getFunctor());
						Unifier u = formUnifierFromArgs(ppc.getSignature(), tl);
						logger.finer("Plan for pending = " + pp);
						LogicalFormula precond = null;
						if (pp.getContext() != null) {
							precond = (LogicalFormula) (pp.getContext().clone());
							precond.apply(u);
						} else {
							logger.warning("No precondition for selected plan " + pp + ", using \"true\"");
							precond = LogExpr.parseExpr("true");
						}
						logger.finer("Plan formed precond = " + precond.toString());

						// setup BB...
						List<Literal> perceive = getTS().getUserAgArch().perceive();
						if (perceive != null) {
							buf(perceive, false, "Adding percepts for handling update " + m);
						}
						BeliefBase context = getBB().clone();
						Agent test = CapabilityAwareAgent.getFutureAgent(context);
						context = insertChangeSetIntoBB(getObligationContract(a).getBeliefChangesFromDependent(),
								context);

						boolean preHold = true; // default for cases with no
												// context constraint(s)
						if (precond != null) {
							preHold = precond.logicalConsequence(test, new Unifier()).hasNext();
						}
						if (!preHold) {
							logger.finer("Preconditions do NOT hold for " + pp);
							boolean updated = formPendingIntentionForContract(c, context);
							if (updated) {
								logger.finer("Contract updated; " + c);
								sendContractChange(c);
							}
						}

					} else {
						logger.fine("No pending intention stored for " + a.toString());
						BeliefBase execContext = getBB().clone();
						insertChangeSetIntoBB(c.getBeliefChangesFromDependent(), execContext);
						logger.info("Trying to find a pending intention for " + a.toString());
						formPendingIntentionForContract(c, execContext);
						sendContractChange(getObligationContract(a));
					}
				}
				logger.info("Updated obligation; " + getObligationContract(a));
			} else if (hasDependencyContract(a)) {
				getDependencyContract(a).setExternalCapability(c.getExternalCapability());
				logger.info("Updated dependency; " + getDependencyContract(a));
			}
		} catch (NullPointerException e) {
			JOptionPane.showMessageDialog(getUi(), new String[] { e.toString(), m.getPropCont().toString() });
			logger.severe("NPE: " + e.toString() + ", handling message " + m.toString());
			throw e;
		}
	}

	/**
	 * Handles message sent when an obligant has performed maintenance upon some
	 * obligation. If we don't have a dependency for the identified task, then
	 * we send a cancellation message to remove it on the obligant end.
	 * 
	 * @param m;
	 *            body content is the literal trigger id associated with the
	 *            obligation
	 */
	protected void handleObligationMaintained(Message m) {
		Contract c = (Contract) m.getPropCont();
		Literal obL = c.getActivity();
		logger.info("Received notification of maintenance for " + obL + " from " + m.getSender());

		if (hasDependencyContract(obL)) {
			int count = this.getMaintCount(obL);
			setMaintCount(obL, ++count);
			setDependency(obL, c);
			logger.finer("Received notification of maintenance for " + obL + " from " + m.getSender() + "(maintained "
					+ count + "times)");
		} else {
			logger.severe("Not a dependency - " + obL + ", so sending cancellation message!");
			this.cancelDependency(obL, false);
		}
	}

	/**
	 * Handles acceptance of an obligation by an obligant. We handle primary and
	 * secondary obligants slightly differently - the former presents a merged
	 * MP.
	 * 
	 * @param m
	 */
	private void handleObligantAcceptanceMsg(Message m) {
		Contract c = ((Contract) m.getPropCont());

		// check we are waiting on this result....
		Literal task = c.getActivity();
		if (waitForOblAccept.containsKey(task)) {
			waitForOblAccept.get(task).remove(m.getSender());
			logger.fine("OBLG ACCEPT: Updated waitSet for " + task + "; " + waitForOblAccept.get(task));

			// is primary? Then we do 'stuff' with the contract...
			LinkedList<String> involvedAgents = getInvolvedAgents(task);
			if (involvedAgents.isEmpty() || !involvedAgents.contains(m.getSender())) {
				logger.severe("Unable to id sender of " + m + " in involved agent set " + involvedAgents);
				// big problem, highlight with message...
				JOptionPane
						.showMessageDialog(
								getUi(), new String[] { "Unable to id sender of ", m.toString(),
										" in involved agent set ", involvedAgents.toString() },
								"Msg error!", JOptionPane.ERROR_MESSAGE);
			}

			if (involvedAgents.get(0).equals(m.getSender())) {
				// store until all confirm
				proposedContractFromObl.put(task, c);
			}

			// is empty? Then we can confirm the contract!
			if (waitForOblAccept.get(task).isEmpty()) {
				logger.finer("Received all required replies for " + task);
				waitForOblAccept.remove(task);

				// in case this isn't primary, fetch contract... and empty temp
				// object
				c = proposedContractFromObl.remove(task);

				// send contract messages & store dependency info
				setDependency(task, c);

				// get intention of interest id
				List<Term> iTrace = c.getIntentionTrace();
				Integer intId = new Integer(iTrace.get(iTrace.size() - 1).toString().replaceAll("\"", ""));

				// remove this action from the relevant temp data entry
				if (!waitingForDep.containsKey(intId)) {
					logger.severe("Wait set does not contain int id (" + intId
							+ ") as a waiting/suspended intention? \n" + "WaitingForDep=" + waitingForDep);
				}

				waitingForDep.get(intId).remove(task);

				// get recipients...
				LinkedList<String> obligants = involvedAgents;
				obligants.remove(getId());

				// form a new message
				Message confirm = new Message(IL_CONFIRM_OBL_CONTRACT, getId(), null, c);
				confirm.setInReplyTo(m.getMsgId());
				try {
					// send to all obs
					for (String rcvr : obligants) {
						confirm.setReceiver(rcvr);
						getTS().getUserAgArch().sendMsg(confirm);
						// NB: presume an ACK is NOT required - i.e. assuming
						// perfect delivery
						logger.finer("Confirm dependency message sent to " + rcvr);
					} // end for msg

					// update message
					if (UniversalConstants.DETAILED_CONTRACT_FORMATION) {
						JOptionPane.showMessageDialog(getUi(),
								new String[] { "Formed dependency for", task.toString() }, getId(),
								JOptionPane.INFORMATION_MESSAGE);
					}

				} catch (Exception e) {
					if (UniversalConstants.DETAILED_CONTRACT_FORMATION) {
						JOptionPane.showMessageDialog(getUi(),
								new String[] { "Exception sending dependency confirmation", confirm.toString() },
								getId(), JOptionPane.ERROR_MESSAGE);
					}
					String st = "";
					for (StackTraceElement s : e.getStackTrace()) {
						st = st + "\n" + s;
					}
					logger.severe("Exception sending confirm message to " + m.getReceiver() + "\nMessage; " + confirm
							+ "\n" + e.toString() + st);
				}

				// clear skipform, as we're not in a last-contract failed type
				// scenario
				contractRejectImmediatelyPriorToExec.remove(normalize(task));

				if (waitingForDep.get(intId).isEmpty()) {
					logger.fine("Resuming intention " + intId + " for action " + task);
					wakeupIntention(intId);
				}
			}
		} else {
			logger.warning("Disregarding message as not waiting on activity; " + m);
		}
	}

	/**
	 * Handles rejection of an obligation.
	 * 
	 * @param m
	 */
	private void handleObligantRefusalMsg(Message m) {
		Contract c = ((Contract) m.getPropCont());

		logger.severe("Dependency rejected by obligant, for " + c.getActivity() + "\nDependencies=" + getDependencies()
				+ "\nts.C= " + getTS().getC());

		if (UniversalConstants.DETAILED_CONTRACT_FORMATION) {
			JOptionPane.showMessageDialog(getUi(),
					new String[] { "Dependency rejected by obligant, for", c.getActivity().toString(),
							"...Cancelling any existing contracts held by other obligants" },
					getId(), JOptionPane.WARNING_MESSAGE);
		}

		// simple response... just cancel everything and send out cancellation
		// messages...
		LinkedList<String> agents = getInvolvedAgents(c.getActivity());
		agents.remove(getId());// don't send to self

		Message cancel = new Message(IL_DEPENDENCY_CANCEL, getId(), null, c);
		cancel.setInReplyTo(m.getMsgId());
		for (String rcvr : agents) {
			cancel.setReceiver(rcvr);
			try {
				getTS().getUserAgArch().sendMsg(cancel);
				// NB: presume an ACK is NOT required - i.e. assuming perfect
				// delivery
				logger.finer("Cancel obl message sent to " + rcvr);
			} catch (Exception e) {
				logger.severe("Exception sending cancel message to " + m.getReceiver());
				logger.severe("Message; " + cancel.toString());
				logger.severe(e.toString());
				for (StackTraceElement s : e.getStackTrace()) {
					logger.severe(s.toString());
				}
			}
		}

		// remove obl parts from temp data store
		waitForOblAccept.remove(c.getActivity());
		proposedContractFromObl.remove(c.getActivity());

		// remove from waitDep set; we can still try execution after failure, as
		// we try and form contracts
		// as far in advance of exec as possible...
		List<Term> iTrace = c.getIntentionTrace();
		Integer intId = new Integer(iTrace.get(iTrace.size() - 1).toString().replaceAll("\"", ""));

		// check...
		assert waitingForDep.containsKey(intId) : "Wait set does not contain int id (" + intId
				+ ") as a waiting/suspended intention? \n" + "WaitingForDep=" + waitingForDep;

		waitingForDep.get(intId).remove(c.getActivity());

		// get intention by int it
		// try pending intentions, i.e. for the first formContract call...
		Intention intention = getTS().getC().getPendingIntentions().get(INTENTION_SUSPENDED + intId);

		if (intention == null) {
			// see if we can get from pending actions...
			forLoop: for (ActionExec pe : getTS().getC().getPendingActions().values()) {
				if (pe.getIntention().getId() == intId) {
					logger.finer("Got intention: " + pe.getIntention());
					intention = pe.getIntention();
					break forLoop;
				}
			} // end for
		} // end if

		/*
		 * Check whether the next action to be executed in the intention is the
		 * one we just failed to form a contract for....
		 */
		if (intention != null) {
			Term action = intention.peek().getCurrentStep().getBodyTerm();
			logger.finer("Got intention next action: " + action);
			if (action.isLiteral()) {
				Literal al = (Literal) action.clone();
				al.apply(intention.peek().getUnif());
				MultiagentArch arch = (MultiagentArch) getTS().getUserAgArch();
				Literal topTrigger = agentUtils().intentionIntoIMsQueue(intention).peekLast().getTrigger().getLiteral();

				if (!action.isInternalAction()) {
					arch.setActionAnnotations(getId(), intention.getId(), topTrigger, // intention.peek().getTrigger().getLiteral(),
							al);
				}

				logger.warning("Next exec: " + al);
				if (al.equals(c.getActivity()) && !contractRejectImmediatelyPriorToExec.contains(normalize(al))) {
					logger.warning("Next action will have no contract! " + al);
					contractRejectImmediatelyPriorToExec.add(normalize(al));
				} else {
					logger.info("Don't need to worry about contract failure yet...;\n" + "curr failed is "
							+ c.getActivity() + ", next is " + action);
				}
			}
		} // end if !=null
		else {
			logger.severe("Unable to get the intention object for id " + intId);
		}

		// ? unecessary?
		if (waitingForDep.get(intId).isEmpty()) {
			logger.finer(intId + ": Not waiting for any more dependents");
			wakeupIntention(intId);
		} else {
			logger.finer(intId + " waiting on " + waitingForDep.get(intId));
		}
	}

	/**
	 * Handles a dependency request- that is, where this agent is requested to
	 * become an obligant and perform the action specified in the contract at
	 * the time of request.
	 * 
	 * @param m
	 */
	private void handleDependencyRequestMsg(Message m) {
		logger.info(getId() + " handle dependency request; " + m);
		Contract c = ((Contract) m.getPropCont());
		Literal activity = c.getActivity();

		// accept or refuse?
		// default to refuse; we don't need to do anything other than set
		// ilForce if we refuse...
		String il = IL_OBLIGANT_REFUSE;
		Message response;

		if (canAcceptObligationContract(m.getSender(), c)) {
			il = IL_OBLIGANT_ACCEPT; // yes!
			BeliefBase execContext = getBB().clone();
			execContext = insertChangeSetIntoBB(c.getBeliefChangesFromDependent(), execContext);
			// do we have a MP for this activity?
			// if not, we just adopt the parent rather than merge with our
			// default...
			boolean primaryAgent = getInvolvedAgents(c.getActivity()).get(0).equals(this.getId());
			if (primaryAgent) {// are we primary?
				if (hasMaintenancePolicy(activity.getFunctor())) {
					MaintenancePolicy newMP = getMaintenancePolicy(activity.getFunctor());
					// merged and update in C
					newMP = newMP.merge(c.getMp());
					c.setMp(newMP);
					logger.finer("Updated contract with a merged MP: " + newMP);
				} else {
					logger.finer("No specific MP, so just using original contract MP");
				}
			} else {
			} // secondary - don't need to worry... primary sets contract and
				// does maintenance

			// accepting, so add obligation?
			logger.finer("Adding accepted obligation contract: " + c.toString());
			setObligationContract(activity, c);

			/*
			 * For primary, form the intention in advance and suspend it; we can
			 * maintain using the associated contract to check the BB conditions
			 */
			if (primaryAgent) {
				logger.info("Doing plan identification for primary actor in obligation " + activity);
				// which plan?
				formPendingIntentionForContract(c, execContext);
			}
		} else {
			// rejection
			logger.severe("Rejecting request;\n" + c.toString());
		}

		// form response message
		response = new Message(il, getId(), m.getSender(), c);
		response.setInReplyTo(m.getMsgId());

		// send response message
		try {
			logger.info("Sending obligation message for " + activity + "\n" + response);
			getTS().getUserAgArch().sendMsg(response);
			logger.finest("Sent response to dep request : " + response);
		} catch (Exception e) {
			String st = "";
			for (StackTraceElement s : e.getStackTrace()) {
				st = st + "\n" + s;
			}
			logger.severe("Error sending response message : " + response.toString() + ";\n" + e.toString() + st);
		}
	}

	/**
	 * Attempts to find and add a pending intention for the given contract; the
	 * PI will be given a key = c.getActivity().toString()
	 * 
	 * @param c
	 * @param execContext
	 * @return true if contract changed
	 */
	protected boolean formPendingIntentionForContract(final Contract c, final BeliefBase execContext) {
		final Literal activity = c.getActivity();
		final Capability used = getCapability(activity.getFunctor());
		Plan p = null;
		if (used != null && used.isPrimitive()) {
			Unifier u = formUnifierFromArgs(used.getSignature(), activity);
			logger.finer("Unifier formed from primitive capability = " + u);
			// just an action
			logger.finer("Obligation will use the primitive capability");

			// trigger is the +!activity
			long t = System.currentTimeMillis();
			String uuid = "handlePlan" + t;
			String trigger = "+!" + activity.toString(); // presumes annotations
															// exist
			String context = "true"; // no selection criteria needed, as
										// pre-scheduled
			String body = activity.toString() + "."; // i.e. just call the
														// action; note, ever
														// time we will add
														// increasing numbers of
														// plans...

			p = Plan.parse("@" + uuid + "[source(self)] " + trigger + ":" + context + "<-" + body);
			logger.finer("Pending intention = " + p.toASString());
			scheduleAsPendingIntention(activity, p, u);

			// set ec
			logger.info("Update contract with plan EC\n" + p);

			Collection<String> holding = used.getHoldingPreconds(execContext, activity);
			if (!holding.isEmpty()) {
				try {
					p.setContext(ASSyntax.parseFormula(holding.iterator().next()));
				} catch (ParseException e) {
					String es = "Exception determining plan context; " + e.toString();
					for (StackTraceElement s : e.getStackTrace()) {
						es = es + "\n\t" + s;
					}
					logger.severe(es);
				}
			}

			Collection<String> effects = agentUtils().getPostEffectsOfPlan(this, p.getBody());
			String[] eff = new String[effects.size()];
			for (int i = 0; i < eff.length; i++) {
				eff[i] = (String) effects.toArray()[i];
			}

			// confidence, cost estimates
			float conf = calculatePlanConfidence(p, execContext);
			float cost = calculatePlanCost(p, execContext);

			String pre = "";
			// set the ec precondition based upon the holding LF for this action
			if (p.getContext() != null) {
				pre = p.getContext().toString();
			} else if (used.isPrimitive()) {
				pre = holding.iterator().next();
			}
			ExternalCapability newEc = new ExternalCapability(used.getSignature(), used.achieves(), this,
					new ConfidencePrecondition(getId(), used.getSignature(), pre, conf), eff, cost, used.isPrimitive());
			logger.info("Set specific EC for contract\n" + newEc);
			c.setExternalCapability(newEc);
			setObligationContract(activity, c);
			return true;
		} else if (used != null && !used.isPrimitive()) {
			// note, this basically copies the acceptance check code...
			String pl = "";
			for (Plan lp : getPL().getPlans()) {
				pl = pl + "\n" + lp;
			}
			logger.finer("Obligation will use a composite capability " + used + "\nPlan lib=" + pl);

			CompositeCapability cc = (CompositeCapability) used;
			logger.finer("Got a candidate cap; " + cc);
			Collection<Plan> matches = cc.getMeans(activity, execContext);
			logger.severe("Got poss means; " + matches);

			// find the plan to schedule...
			if (matches.size() == 1) {
				logger.finer("Selected plan is " + matches.iterator().next().toASString());
			} else if (matches.size() > 1) { // select best from the options
				logger.finer("Multiple plan matches, selecting by confidence...");
				Plan bestPlan = getBestPlan(activity, cc, execContext);
				logger.finer("Got bp..." + bestPlan.toASString());
				matches.clear();
				matches.add(bestPlan);
			}

			if (matches.isEmpty()) {
				logger.severe("Plan not found for accepted obligation?!" + activity + ", busy="
						+ (bb.getCandidateBeliefs(ASSyntax.createLiteral("busy", ASSyntax.createAtom(getId())),
								null) != null));
				bbDump(execContext, Level.INFO);
				return false;
			} else {
				p = (Plan) (matches.iterator().next().clone());
				Unifier u = formUnifierFromArgs(p.getTrigger().getLiteral(), activity);
				logger.finer("Unifier formed from composite plan trigger (" + p.getTrigger().getLiteral() + ", "
						+ activity + ") = " + u);
				p.getTrigger().setLiteral((Literal) activity.clone());
				p.getTrigger().setTrigOp(TEOperator.add);
				scheduleAsPendingIntention(activity, p, u);

				// we can update the EC as we have a specific plan...
				ExternalCapability oldEc = c.getExternalCapability();
				logger.info("Update EC (" + oldEc + ") with plan\n" + p);
				Collection<String> effects = agentUtils().getPostEffectsOfPlan(this, p.getBody());
				String[] eff = new String[effects.size()];
				for (int i = 0; i < eff.length; i++) {
					eff[i] = (String) effects.toArray()[i];
				}

				// confidence, cost estimates
				float conf = calculatePlanConfidence(p, execContext);
				float cost = calculatePlanCost(p, execContext);
				String pre = "";
				// set the precondition as being that of the selected intention
				if (p.getContext() != null) {
					pre = p.getContext().toString();
				}
				ExternalCapability newEc = new ExternalCapability(p.getTrigger().getLiteral(),
						cc.achieves(), this,
						new ConfidencePrecondition(getId(), p.getTrigger().getLiteral(), pre, conf), eff, cost,
						cc.isPrimitive());
				logger.info("Set specific EC for contract\n" + newEc);
				c.setExternalCapability(newEc);
				setObligationContract(activity, c);
				return true;
			}
		} else {
			logger.severe("Capability not found for accepted obligation!" + activity);
			return false;
		}
	}

	/**
	 * Takes the given plan and schedules it as a pending intention, to be
	 * resumed by the receipt of a 'do'
	 * 
	 * @param activity
	 *            - Literal; trigger action
	 * @param plan
	 *            - Plan to be formed as (1 level only) intention.
	 * @param unifier
	 *            - unifier formed from comparison between activity and
	 *            signature
	 */
	protected Intention scheduleAsPendingIntention(Literal activity, Plan plan, Unifier u) {
		logger.info("Adding pending intention for " + activity + ", plan;\n" + plan.toASString() + " - " + u);
		Intention pend = new Intention();
		plan.apply(u);
		pend.push(new IntendedMeans(new Option(plan, u), plan.getTrigger()));
		pend.setSuspended(true);
		getTS().getC().getPendingIntentions().put(activity.toString(), pend);
		logger.finer("Added pending intention;\n" + getTS().getC().toString());
		return pend;
	}

	protected Intention scheduleAsPendingIntention(final Literal activity, final Intention i) {
		logger.info("Adding pending intention for " + activity + ", i;\n" + i);
		i.setSuspended(true);
		getTS().getC().getPendingIntentions().put(normalize(activity).toString(), i);
		logger.finer("Added pending intention;\n" + getTS().getC().toString());
		return i;
	}

	/**
	 * Extension with a check to see if there is a ready-suspended pending
	 * intention for the obligation activity being triggered
	 */
	protected void doObligation(Message m) {
		Literal task = (Literal) m.getPropCont();
		logger.severe("Do obligation " + task + ", obSet=" + getObligations());
		if (isExecuting(task)) {
			logger.severe("Already executing ob " + task);
		}

		if (getTS().getC().getPendingIntentions().containsKey(task.toString())) {
			resumePending(task);
		} else {
			logger.severe("No cached plan for " + task + " in " + getTS().getC().getPendingIntentions().keySet()
					+ "\n...using super-method to create\n" + getTS().getC());
			super.doObligation(m);
		}
	}

	protected void resumePending(Literal task) {
		Intention pend = getTS().getC().getPendingIntentions().remove(task.toString());
		logger.info("Retrieved pending intention for " + task + "\n" + pend.toString());
		pend.setSuspended(false);
		if (!isExecuting(task)) {
			setExecuting(task);
		}
		getTS().getC().resumeIntention(pend);
		logger.info("Resumed pending intention for " + task + "\n" + getTS().getC());
	}

	/**
	 * Assesses if we can accept this obligation contract; checks capability
	 * confidence vs contract threshold, whether activity is ground, and whether
	 * it is a sub-activity of any existing obligations
	 * 
	 * @param dependant
	 * @param contract
	 * @return
	 */
	protected boolean canAcceptObligationContract(String dependant, Contract contract) {
		buf(ts.getUserAgArch().perceive(), false, "Checking can accept " + contract); // update
																						// bb
		Literal activity = contract.getActivity();

		// only accept ground requests?
		if (!activity.isGround()) {
			logger.severe("REJECT: Cannot accept requests for an unground activity: " + activity);
			return false;
		}

		if (UniversalConstants.DETAILED_CONTRACT_FORMATION && UniversalConstants.DEMO_MODE) {
			int result = JOptionPane.showConfirmDialog(getUi(),
					new String[] { "Accept obligation? \n" + contract, "(YES -> agent makes decision, NO -> reject)" },
					"Obligation override for " + getId(), JOptionPane.YES_NO_OPTION);
			if (result == JOptionPane.NO_OPTION) {
				logger.severe("Manually rejected " + activity);
				return false;
			}
		}

		// check - are we subordinate? If so, can accept as not leading agent...
		LinkedList<String> involved = MultiagentArch.getInvolvedAgents(activity, this);
		if (involved.contains(getId()) && !involved.getFirst().equals(getId())) {
			logger.info("Accepting obligation for " + activity + " as a non-leading participant");
			return true;
		}

		// check if we are actually capable of this activity...
		if (hasCapability(activity.getFunctor())) {
			Capability inUse = getCapability(activity.getFunctor());
			// check confidence of our capability vs the contract maint.
			// threshold
			logger.warning("Capability for " + activity + " request;\n" + inUse);
			// get causal links from contract and predict future confidence!
			BeliefBase execContext = getBB().clone();
			execContext = insertChangeSetIntoBB(contract.getBeliefChangesFromDependent(), execContext);
			execContext.remove(ASSyntax.createLiteral(BUSY, ASSyntax.createString(getId())));
			float confidence = inUse.getSpecificConfidence(execContext, activity);
			logger.warning("General conf for " + activity + "=" + confidence);
			boolean preHold = false;

			if (inUse.isPrimitive()) {
				preHold = inUse.preconditionsHold(execContext, activity);
			} else {
				preHold = ((CompositeCapability) inUse).preconditionsHoldAndPlanExists(execContext, activity);
			}

			if (!preHold) {
				String type = "Primitive";
				if (!inUse.isPrimitive()) {
					type = "Composite";
				}
				logger.severe("Preconds do not hold for identified capability " + inUse + " (" + type + ")");
				bbDump(execContext, Level.INFO);

				if (UniversalConstants.DETAILED_CONTRACT_FORMATION) {
					JOptionPane.showMessageDialog(getUi(),
							new String[] { "Rejected obligation request for", activity.toString(),
									"No preconditions hold for this capability!" },
							getId(), JOptionPane.INFORMATION_MESSAGE);
				}

				return false;
			}

			// try to only drop confidence in exception circumstances, as the
			// dependent may persue a below maint threshold
			// confidence task as being the ONLY option in current conditions
			// only applied for maintaining agents
			if ((this instanceof MaintainingAgent) && ((MaintainingAgent) this).isMaintaining()
					&& confidence < contract.getMp().dropConfidenceThreshold()) {
				logger.warning("REJECT: Agent confidence in " + activity + " is below (" + confidence
						+ ") contract maint trigger (" + contract.getMp().dropConfidenceThreshold() + ")");
				return false;
			}

			// conf ok, we can continue....
			if (inUse.isPrimitive()) {
				// (Literal sig, GoalTask task, String holder, String pre,String
				// postEffects, float conf, float cost) {
				ExternalCapability projected = new ExternalCapability(inUse.getSignature(), inUse.achieves(), this,
						new ConfidencePrecondition(inUse.getHolder().getId(), inUse.getSignature(),
								inUse.getHoldingPreconds(execContext, activity).iterator().next(), confidence),
						inUse.getPostEffects(), inUse.getGeneralCostEstimate(), inUse.isPrimitive());
				logger.info(activity + ": Set external capability for contract; " + projected);
				contract.setExternalCapability(projected);
			} else {// composite
				Plan bestPlan = getBestPlan(activity, (CompositeCapability) inUse, execContext);

				if (bestPlan != null) {
					// post effects...
					Collection<String> pe = agentUtils().getPostEffectsOfPlan(this, bestPlan.getBody());
					String[] postEffects = new String[pe.size()];
					for (int i = 0; i < postEffects.length; i++) {
						postEffects[i] = (String) pe.toArray()[i];
					}
					// set into capability...
					String precondition = "true";
					if (bestPlan.getContext() != null) {
						precondition = bestPlan.getContext().toString();
					}

					ExternalCapability projected = new ExternalCapability(bestPlan.getTrigger().getLiteral(), // inUse.getSignature(),
							inUse.achieves(), this,
							new ConfidencePrecondition(getId(), bestPlan.getTrigger().getLiteral(), precondition,
									confidence),
							postEffects, agentUtils().getActionCount(bestPlan.getBody(), false), inUse.isPrimitive());
					contract.setExternalCapability(projected);
					logger.info(activity + ": Set external capability for contract; " + projected);
				} else {
					logger.severe(
							activity + " - No best plan found!... rejecting...\n" + inUse.toString() + "\n" + contract);
					bbDump(execContext, Level.SEVERE);
					return false;
				}
			}
		} else {// no capability for task....
			logger.severe("REJECT: Do not have capability " + activity + ", cannot accept obligation contract from "
					+ dependant);
			if (UniversalConstants.DETAILED_CONTRACT_FORMATION) {
				JOptionPane
						.showMessageDialog(getUi(),
								new String[] { "Rejected obligation request for", activity.toString(),
										"Do not have capability for this action" },
								getId(), JOptionPane.INFORMATION_MESSAGE);
			}
			return false;
		}

		// criteria is similar to accepting execution obligations - if no
		// obligaitons, can accept immediately...
		if (getObligations().isEmpty()) {
			logger.finer("ACCEPT: " + activity + " will be our only obligation contract");

			if (UniversalConstants.DETAILED_CONTRACT_FORMATION) {
				JOptionPane.showMessageDialog(getUi(), new String[] { "Accepting obligation request for",
						activity.toString(), "No existing obligations" }, getId(), JOptionPane.INFORMATION_MESSAGE);
			}

			return true;
		} else if (hasObligationContract(activity)) {
			logger.severe("REJECT: Duplicate obligation request for " + activity + " from " + dependant);

			if (UniversalConstants.DETAILED_CONTRACT_FORMATION) {
				JOptionPane
						.showMessageDialog(getUi(),
								new String[] { "Rejected obligation request for", activity.toString(),
										"Duplicate request, from " + dependant },
								getId(), JOptionPane.INFORMATION_MESSAGE);
			}

			return false;
		}
		// otherwise, only accept 'sub' obligations; use the annotations to
		// identify requests for actions that
		// are immediate or otherwise child tasks of an accepted one.
		else {
			// check each - if we get a viable parent, then accept
			return canAcceptObligationAsChildOfExistingOb(activity);
		}
	}

	/**
	 * Can we accept an obligation for activity as a sub-child/sub-obligation of
	 * an existing obligation held by us? Yes if it's in the same plan, shares
	 * the same parent, or is the subchild of at least one existing Ob.
	 */
	private boolean canAcceptObligationAsChildOfExistingOb(Literal activity) {
		for (Literal l : getObligations()) {
			if (agentUtils().isSubChildOf(activity, l)) {
				logger.finer("ACCEPT: " + activity + " is child of existing obligation for " + l);

				if (UniversalConstants.DETAILED_CONTRACT_FORMATION) {
					JOptionPane
							.showMessageDialog(getUi(),
									new String[] { "Accepting obligation request for", activity.toString(),
											"Child of existing obligation to " + l },
									getId(), JOptionPane.INFORMATION_MESSAGE);
				}

				return true;
			} else if (agentUtils().isInSamePlan(activity, l)) {
				logger.finer("ACCEPT: Execution of " + activity + " follows existing obligation for " + l);

				if (UniversalConstants.DETAILED_CONTRACT_FORMATION) {
					JOptionPane.showMessageDialog(getUi(),
							new String[] { "Accepting obligation request for", activity.toString(),
									"Same source plan/intention as existing obligation to " + l },
							getId(), JOptionPane.INFORMATION_MESSAGE);
				}

				return true;
			} else if (agentUtils().sharedParent(activity, l)) {
				logger.finer("ACCEPT: Execution of " + activity + " shares parent with existing obligation for " + l);

				if (UniversalConstants.DETAILED_CONTRACT_FORMATION) {
					JOptionPane
							.showMessageDialog(getUi(),
									new String[] { "Accepting obligation request for", activity.toString(),
											"Shares a parent int/dep with " + l },
									getId(), JOptionPane.INFORMATION_MESSAGE);
				}

				return true;
			}
		}
		// otherwise, not hits = false;

		logger.severe("REJECT: Have pre-existing obligations, cannot find parent to " + activity
				+ "; returning false\n\t\tObligations= " + getObligations() + "\n\t\tMessages= "
				+ getTS().getC().getMailBox());

		// NB: seems fixed now.
		// threaded to stop screwing up other stuff...
		// if(UniversalConstants.DETAILED_CONTRACT_FORMATION) {
		final String as = activity.toString();
		final String[] osL = new String[this.getObligations().size()];
		for (int i = 0; i < osL.length; i++) {
			osL[i] = this.getObligations().toArray()[i].toString();
		}
		String[] display = new String[osL.length + 3];
		display[0] = "Rejected obligation request for";
		display[1] = as;
		display[2] = "Have existing, unrelated, obligations";
		for (int i = 0; i < osL.length; i++) {
			display[i + 3] = osL[i];
		}
		JOptionPane.showMessageDialog(getUi(), display, getId(), JOptionPane.INFORMATION_MESSAGE);
		// }

		return false;
	}

	/**
	 * Given an action <i>activity</i>, and an execution context, find the best
	 * plan from the associated composite capability, on the basis of estimated
	 * confidence
	 * 
	 * @param activity
	 * @param inUse
	 * @param execContext
	 * @return Plan (ground by activity contents)
	 */
	private Plan getBestPlan(Literal activity, CompositeCapability cc, BeliefBase execContext) {
		// This is a wee bit trickier, as we want to project a specific single
		// plan with a single
		// precondition and single set of effects
		List<Plan> means = cc.getMeans(activity, execContext);
		logger.fine("getBestPlan: Set of possible means = " + means);
		Plan bestPlan = null;
		float highestConf = -1;
		for (Plan p : means) {
			// calculate plan confidence
			p = (Plan) p.clone();
			p.apply(formUnifierFromArgs(cc.getSignature(), activity));
			float conf = calculatePlanConfidence(p, execContext);
			logger.finer("getBestPlan: Planscore " + conf + "\n" + p.toASString());
			if (conf > highestConf) {
				highestConf = conf;
				bestPlan = p;
			}
		}
		if (bestPlan != null) {
			logger.info("getBestPlan: Got best plan; score = " + highestConf + "\n" + bestPlan.toASString());
		} else {
			logger.warning("getBestPlan: Unable to find a plan... ");
		}
		return bestPlan;
	}

	/**
	 * Goes through an intention and returns the confidence, determined as the
	 * lowest of all action confidences. If a precondition does not hold,
	 * returns a zero.
	 * 
	 * @param i
	 *            - Intention
	 * @param ec
	 *            - Belief Base; the execution context
	 * @return float value
	 */
	protected float calculateIntentionConfidence(Intention i, BeliefBase ec) {
		long time = System.currentTimeMillis();
		ec = ec.clone(); // TODO double-check
		LinkedList<IntendedMeans> means = agentUtils().intentionIntoIMsQueue(i);
		float lowest = 1.1f; // returned for complete intentions...
		boolean popTop = false;
		int count = 0; // lookahead count
		meansLoop: while (!means.isEmpty()) {
			IntendedMeans current = (IntendedMeans) means.pop().clone();
			if (current.getCurrentStep() != null) {
				if (count > intentionConfidenceLookahead) {
					break meansLoop;
				}
				++count;
				PlanBody pb = current.getCurrentStep().clonePB();
				pb.apply(current.getUnif());
				if (!popTop) {
					// i.e. bottom level, we check the first plan body
					popTop = true; // only do this loop once
				} else {
					// iterate past the first entry as this corresponds to the
					// already handled, previous loops
					pb = pb.getBodyNext();
				}
				if (pb != null && pb.getBodyTerm() != null) {
					float pc = calculateGroundPlanConfidence(pb, ec);
					if (pc == -1 && hasCapability(current.getTrigger().getLiteral().getFunctor())) {
						logger.warning(pb + "\n>>No capabilities found, using trigger " + current.getTrigger()
								+ " confidence");
						pc = getCapability(current.getTrigger().getLiteral().getFunctor()).getSpecificConfidence(ec,
								(Literal) pb.getBodyTerm()); // was general
																// conf?
					}

					if (pc < lowest && pc != -1f) {
						lowest = pc;
					}
				}
			} // end check for non-null ims
		}

		long elapsed = System.currentTimeMillis() - time;
		logger.finer("CalculateIntentionConf took " + elapsed + "ms for " + i.getId() + ": Returning confidence "
				+ lowest + " for\n" + i.toString());
		return lowest;
	}

	/**
	 * Returns a collection of all literals with an estimated confidence <
	 * limit, in plan under the ec defined bb. Observes the same lookahead limit
	 * as confidence estimation
	 * 
	 * @param pb
	 *            - incremented at end
	 * @param bb
	 *            - updated at end
	 * @param limit
	 * @return 0..* literals
	 * @see calculatePlanConfidence
	 */
	protected Collection<Literal> getActivitiesUnderThreshold(PlanBody pb, BeliefBase bb, final float minConf) {
		Collection<Literal> underList = new Vector<Literal>();
		logger.info("Getting activities under " + minConf + " for " + pb);
		// iteration...
		if (pb == null) {
			logger.warning("Plan for getActivitiesUnderThreshold is null");
			return underList;
		} else if (pb.getBodyTerm().isInternalAction()) {
			PlanBody pb2 = pb.clonePB();
			boolean allInternal = true;
			while (pb2.getBodyNext() != null && allInternal) {
				if (!pb.isInternalAction() || hasCapability(pb2)) {
					allInternal = false;
				}
				pb2 = pb2.getBodyNext();
			}
			if (allInternal) {
				logger.finer(pb + " only internal actions without capabilities, return empty");
				return underList;
			}
		}
		int count = 0;
		while (pb != null && count < intentionConfidenceLookahead) {
			count++;
			Capability c = null;
			// get lit
			Literal action = (Literal) pb.getBodyTerm();
			logger.info(pb + "; Calculating confidence; type=" + pb.getBodyType());
			if (pb.getBodyType().equals(PlanBodyImpl.BodyType.addBel)
					|| pb.getBodyType().equals(PlanBodyImpl.BodyType.delAddBel)
					|| pb.getBodyType().equals(PlanBodyImpl.BodyType.delBel)
					|| pb.getBodyType().equals(PlanBodyImpl.BodyType.test)) {
				logger.severe("Calculate confidence; skipping " + action);
			} else if (pb.getBodyType().equals(PlanBodyImpl.BodyType.achieve)
					|| pb.getBodyType().equals(PlanBodyImpl.BodyType.achieveNF)
					|| pb.getBodyType().equals(PlanBodyImpl.BodyType.action)) {
				// get confidence....
				boolean isDep = hasDependencyContract(action);
				logger.info("Calculate confidence; " + action + ", (dep? " + isDep + ") ");
				// priority - do we have a dependency contract for this action?
				if (isDep && getDependencyContract(action).getExternalCapability() != null) {
					c = getDependencyContract(action).getExternalCapability();
					float actConf = c.getSpecificConfidence(bb, action);
					if (actConf < minConf) {
						logger.severe("Obligation ec for " + action + " conf = " + actConf + "<" + minConf);
						underList.add(normalize((Literal) pb.getBodyTerm()));
					}
				}

				// new if clause, to handle situations where have a contract but
				// no EC specified in it
				if (c == null && hasCapability(pb)) {
					c = getCapability(pb);
					float actConf = 0;
					if (action.isGround()) {
						actConf = c.getSpecificConfidence(bb, action);
					} else {
						actConf = c.getGeneralConfidence(bb);
					}
					if (actConf < minConf) {
						logger.severe("local capability " + action + " conf = " + actConf + "<" + minConf);
						underList.add(normalize((Literal) pb.getBodyTerm()));
					}
				}
				// external?
				else if (c == null && knowsProviderForExtCapability(action.getFunctor())) {
					// get set of capabilities and make sure we can use at least
					// one (based on precond)
					logger.info(
							"Get ecs for " + action + ", involved = " + MultiagentArch.getInvolvedAgents(action, this));
					Collection<ExternalCapability> caps = getExtCapability(action);

					if (caps.isEmpty()) {
						logger.info("Unable to find external capability for plan conf calculation;" + action);
					} else {
						logger.info("Got " + caps.size() + " capabilities for " + action);
						float maxEcConf = -1f;// highest confidence of all
												// potential ec operators
						ExternalCapability bestEc = null;
						for (final ExternalCapability ec : caps) {
							boolean preconditionsHold = ec.preconditionsHold(bb, action);
							if (obligantIsExecuting(action) || preconditionsHold) {
								// take the highest executable confidence /
								// assume we are going highest confidence
								float conf = ec.getSpecificConfidence(bb, action);
								logger.info("Confidence for viable EC " + action + " = " + conf + ", max = " + maxEcConf
										+ ", executing ob? " + obligantIsExecuting(action));
								if (conf > maxEcConf) {
									maxEcConf = conf;
									bestEc = ec;
								}
							} else if (!preconditionsHold) {
								String precond = "";
								for (String pre : ec.getPreconditions()) {
									precond = precond + "\n\tPre: " + pre;
								}
								logger.severe(action + " no preconditions holding in BB for this EC " + precond);
							}
							logger.finer(action + " confidence (external capability) = " + maxEcConf + "\n\tTS.C = "
									+ getTS().getC());
						} // end for
						logger.info(action + " highest confidence (external) = " + maxEcConf);
						if (maxEcConf < minConf) {
							logger.severe(
									"Best external capability " + action + " conf = " + maxEcConf + "<" + minConf);
							underList.add(normalize((Literal) pb.getBodyTerm()));
						} else {
							c = bestEc;
						}
					} // end else
				} else if (c == null && !action.isInternalAction()) {
					logger.severe("(confidence activity listing) Capability in plan NOT found ; " + "action was "
							+ action + ", next pb=" + pb.getBodyNext() + "\nPC=" + getPrimitiveCapabilities().keySet()
							+ "\nCC=" + getCompositeCapabilities().keySet() + "\nEC=" + getExtCapabilities() + "\nDeps="
							+ getDependencies());
					// return 0; //just keep going - don't just assume agent has
					// full knowledge....
				}
			}
			// progress bb
			if (c != null) {
				bb = estimatePostExecutionBB(bb, action, c);
			} else {
				logger.warning("Form under list; No c set for " + action);
			}

			// next
			pb = pb.getBodyNext();
		}

		logger.info("Plan activities under conf " + minConf + "=" + underList);
		return underList;
	}

	/**
	 * Internal - calculates confidence for a fully ground plan p; we have
	 * capabilities for all actions as plans originate from operators that
	 * themselves are inferred from the capability set. Also updates bb to
	 * post-execution state.
	 */
	public final float calculatePlanConfidence(Plan p, BeliefBase bb) {
		if (p.getBody() == null) {
			logger.warning("Trying to get confidence for an empty plan!");
			return 1.15f; // doing nothing always succeeds; 1.15 value is for
							// tracing in debug log
		}
		if (!p.isGround()) {
			logger.finer("Unground; Using super-method for plan calculation...\n" + p);
			return calculateUngroundPlanConfidence(p, bb);
		}

		float conf = calculateGroundPlanConfidence(p.getBody(), bb);
		/*
		 * If we can't calculate a confidence for ANY of the plan actions, we
		 * get -1. If so, try and see if we can use the parent (trigger)
		 * capability for this value
		 */
		if (conf == -1 && hasCapability(p.getTrigger().getLiteral().getFunctor())) {
			logger.info("Confidence for plan " + p.toASString() + " is " + conf + ", so trying parent C");
			conf = getCapability(p.getTrigger().getLiteral().getFunctor()).getGeneralConfidence(bb);
			logger.info("Confidence for plan parent C (" + p.getTrigger().getLiteral() + ") is " + conf);
		}
		logger.finest("Confidence for plan " + p.toASString() + " is " + conf);
		return conf;
	}

	/**
	 * Calculates confidence where we can safely assume a ground plan and thus
	 * use specific confidence values
	 * 
	 * @param pb
	 * @param bb
	 * @return
	 */
	public final float calculateGroundPlanConfidence(PlanBody pb, BeliefBase bb) {
		if (Capability.CONF_STRATEGY.equals(Capability.confidenceStrategy.min)) {
			return calculatePlanConfidenceMin(pb, bb);
		} else if (Capability.CONF_STRATEGY.equals(Capability.confidenceStrategy.average)) {
			return calculatePlanConfidenceAvg(pb, bb);
		} else if (Capability.CONF_STRATEGY.equals(Capability.confidenceStrategy.weightedAverage)) {
			return calculatePlanConfidenceWeightedAvg(pb, bb);
		} else {
			throw new UnsupportedOperationException("Not implemented handling yet!");
		}
	}

	protected float calculatePlanConfidenceWeightedAvg(PlanBody pb, BeliefBase bb) {
		String pString = "Plan: " + pb;
		float sum = 0; // default return
		float scoreSum = 0; // used to cal average

		if (pb == null) {
			logger.warning("Plan for confidence calculation is null, will return 1!");
			return 1;
		} else {
			if (pb.isInternalAction()) {
				PlanBody pb2 = pb.clonePB();
				boolean allInternal = true;
				while (pb2.getBodyNext() != null && allInternal) {
					if (!pb.isInternalAction() || hasCapability(pb2)) {
						allInternal = false;
					}
					pb2 = pb2.getBodyNext();
				}
				if (allInternal) {
					logger.finer(pb + " only internal actions without capabilities, return 1");
					return 1;
				} else {
					while (pb.isInternalAction()) {
						pb = pb.getBodyNext(); // skip internal
					}
				}
			}
		}
		float count = 1;
		while (pb != null && count < intentionConfidenceLookahead) {
			float weight = (new Float(intentionConfidenceLookahead - count)) / (count);
			logger.info("Weight at " + count + " is " + weight);
			count++;
			Capability c = null;
			// get lit
			Literal action = (Literal) pb.getBodyTerm();
			logger.info(pb + "; Calculating confidence; type=" + pb.getBodyType() + " ia?"
					+ pb.getBodyTerm().isInternalAction());
			if (pb.getBodyType().equals(PlanBodyImpl.BodyType.addBel)
					|| pb.getBodyType().equals(PlanBodyImpl.BodyType.delAddBel)
					|| pb.getBodyType().equals(PlanBodyImpl.BodyType.delBel)
					|| pb.getBodyType().equals(PlanBodyImpl.BodyType.test)) {
				logger.severe("Calculate confidence; skipping " + action);
			} else if (pb.getBodyType().equals(PlanBodyImpl.BodyType.achieve)
					|| pb.getBodyType().equals(PlanBodyImpl.BodyType.achieveNF)
					|| pb.getBodyType().equals(PlanBodyImpl.BodyType.action)) {
				// get confidence....
				boolean isDep = hasDependencyContract(action);
				logger.info("Calculate confidence; " + action + ", (dep? " + isDep + "), " + "dependencies="
						+ getDependencies());
				// priority - do we have a dependency contract for this action?
				if (isDep && getDependencyContract(action).getExternalCapability() != null) {
					c = getDependencyContract(action).getExternalCapability();
					float actConf = c.getSpecificConfidence(bb, action);
					logger.info(action + " confidence (obligation contract) = " + actConf);
					sum += (weight * actConf);
					scoreSum += weight;
				}

				// new if clause, to handle situations where have a contract but
				// no EC specified in it
				if (c == null && hasCapability(pb)) {
					c = getCapability(pb);
					float actConf = 0;
					if (!c.preconditionsHold(bb, action)) {
						logger.severe("conf value zero as preconds don't hold for " + action);
					} else {
						if (action.isGround()) {
							actConf = c.getSpecificConfidence(bb, action);
						} else {
							actConf = c.getGeneralConfidence(bb);
						}
					}
					logger.info(action + " confidence (internal capability) = " + actConf);
					sum += (weight * actConf);
					scoreSum += weight;
				}
				// external?
				else if (c == null && knowsProviderForExtCapability(action.getFunctor())) {
					// get set of capabilities and make sure we can use at least
					// one (based on precond)
					logger.info(
							"Get ecs for " + action + ", involved = " + MultiagentArch.getInvolvedAgents(action, this));
					Collection<ExternalCapability> caps = getExtCapability(action);

					if (caps.isEmpty()) {
						logger.info("Unable to find external capability for plan conf calculation;" + action);
					} else {
						logger.info("Got " + caps.size() + " ecs");
						float maxEcConf = -1f;// highest confidence of all
												// potential ec operators
						for (ExternalCapability ec : caps) {
							boolean preconditionsHold = ec.preconditionsHold(bb, action);
							if (obligantIsExecuting(action) || preconditionsHold) {
								// take the highest executable confidence /
								// assume we are going highest confidence
								float conf = ec.getSpecificConfidence(bb, action);
								logger.info("Confidence for viable EC " + action + " = " + conf + ", max = " + maxEcConf
										+ ", executing ob? " + obligantIsExecuting(action));
								if (conf > maxEcConf) {
									maxEcConf = conf;
								}
							} else if (!preconditionsHold) {
								String precond = "";
								for (String pre : ec.getPreconditions()) {
									precond = precond + "\n\tPre: " + pre;
								}
								logger.severe(action + " no preconditions holding in BB for this EC " + precond);
							}
							logger.finer(action + " confidence (external capability) = " + maxEcConf + "\n\tTS.C = "
									+ getTS().getC());
						} // end for
						logger.info(action + " highest confidence (external) = " + maxEcConf);
						sum += (weight * maxEcConf);
						scoreSum += weight;
					} // end else
				} else if (c == null) {
					logger.warning("Unable to get cap/conf values for " + action + " (internal? "
							+ action.isInternalAction() + ", get ext? " + getExtCapability(action) + ")");
				}
			}
			// progress bb
			if (c != null) {
				bb = estimatePostExecutionBB(bb, action, c);
			}
			// next
			pb = pb.getBodyNext();
		}

		logger.info("Plan confidence calculated as " + sum + "\\" + scoreSum + " resolving to="
				+ (scoreSum == 0 ? 1 : sum / scoreSum) + "\n" + pString);
		return (scoreSum == 0) ? 1 : sum / scoreSum;
	}

	protected float calculatePlanConfidenceAvg(PlanBody pb, BeliefBase bb) {
		String pString = "Plan: " + pb;
		float sum = 0; // default return
		if (pb == null) {
			logger.warning("Plan for confidence calculation is null, will return 1!");
			return 1;
		} else if (pb.getBodyTerm().isInternalAction()) {
			PlanBody pb2 = pb.clonePB();
			boolean allInternal = true;
			while (pb2.getBodyNext() != null && allInternal) {
				if (!pb.isInternalAction() || hasCapability(pb2)) {
					allInternal = false;
				}
				pb2 = pb2.getBodyNext();
			}
			if (allInternal) {
				logger.finer(pb + " only internal actions without capabilities, return 1");
				return 1;
			}
		}
		int count = 0;
		while (pb != null && count < intentionConfidenceLookahead) {
			count++;
			Capability c = null;
			// get lit
			Literal action = (Literal) pb.getBodyTerm();
			logger.info(pb + "; Calculating confidence; type=" + pb.getBodyType());
			if (pb.getBodyType().equals(PlanBodyImpl.BodyType.addBel)
					|| pb.getBodyType().equals(PlanBodyImpl.BodyType.delAddBel)
					|| pb.getBodyType().equals(PlanBodyImpl.BodyType.delBel)
					|| pb.getBodyType().equals(PlanBodyImpl.BodyType.test)) {
				logger.severe("Calculate confidence; skipping " + action);
			} else if (pb.getBodyType().equals(PlanBodyImpl.BodyType.achieve)
					|| pb.getBodyType().equals(PlanBodyImpl.BodyType.achieveNF)
					|| pb.getBodyType().equals(PlanBodyImpl.BodyType.action)) {
				// get confidence....
				boolean isDep = hasDependencyContract(action);
				logger.info("Calculate confidence; " + action + ", (dep? " + isDep + "), " + "dependencies="
						+ getDependencies());
				// priority - do we have a dependency contract for this action?
				if (isDep && getDependencyContract(action).getExternalCapability() != null) {
					c = getDependencyContract(action).getExternalCapability();
					float actConf = c.getSpecificConfidence(bb, action);
					logger.info(action + " confidence (obligation contract) = " + actConf);
					sum += actConf;
				}

				// new if clause, to handle situations where have a contract but
				// no EC specified in it
				if (c == null && hasCapability(pb)) {
					c = getCapability(pb);
					float actConf = 0;
					if (!c.preconditionsHold(bb, action)) {
						logger.severe("conf value zero as preconds don't hold for " + action);
					} else {
						if (action.isGround()) {
							actConf = c.getSpecificConfidence(bb, action);
						} else {
							actConf = c.getGeneralConfidence(bb);
						}
					}
					logger.info(action + " confidence (internal capability) = " + actConf);
					sum += actConf;
				}
				// external?
				else if (c == null && knowsProviderForExtCapability(action.getFunctor())) {
					// get set of capabilities and make sure we can use at least
					// one (based on precond)
					logger.info(
							"Get ecs for " + action + ", involved = " + MultiagentArch.getInvolvedAgents(action, this));
					Collection<ExternalCapability> caps = getExtCapability(action);

					if (caps.isEmpty()) {
						logger.info("Unable to find external capability for plan conf calculation;" + action);
					} else {
						logger.info("Got " + caps.size() + " ecs");
						float maxEcConf = -1f;// highest confidence of all
												// potential ec operators
						for (ExternalCapability ec : caps) {
							boolean preconditionsHold = ec.preconditionsHold(bb, action);
							if (obligantIsExecuting(action) || preconditionsHold) {
								// take the highest executable confidence /
								// assume we are going highest confidence
								float conf = ec.getSpecificConfidence(bb, action);
								logger.info("Confidence for viable EC " + action + " = " + conf + ", max = " + maxEcConf
										+ ", executing ob? " + obligantIsExecuting(action));
								if (conf > maxEcConf) {
									maxEcConf = conf;
								}
							} else if (!preconditionsHold) {
								String precond = "";
								for (String pre : ec.getPreconditions()) {
									precond = precond + "\n\tPre: " + pre;
								}
								logger.severe(action + " no preconditions holding in BB for this EC " + precond);
							}
							logger.finer(action + " confidence (external capability) = " + maxEcConf + "\n\tTS.C = "
									+ getTS().getC());
						} // end for
						logger.info(action + " highest confidence (external) = " + maxEcConf);
						sum += maxEcConf;
					} // end else
				} else if (c == null && !action.isInternalAction()) {
					logger.severe("(confidence calc) Capability in plan NOT found ; " + "action was " + action
							+ ", next pb=" + pb.getBodyNext() + "\nPC=" + getPrimitiveCapabilities().keySet() + "\nCC="
							+ getCompositeCapabilities().keySet() + "\nEC=" + getExtCapabilities() + "\nDeps="
							+ getDependencies());
					// return 0; //just keep going - don't just assume agent has
					// full knowledge....
				} else if (c == null) {
					logger.warning("Unable to get cap/conf values for " + action + " (internal? "
							+ action.isInternalAction() + ", get ext? " + getExtCapability(action) + ")");
				} else if (c != null) {
					logger.finer("Cap; " + c);
				}
			}
			// progress bb
			if (c != null) {
				bb = estimatePostExecutionBB(bb, action, c);
			} else {
				logger.warning("No c set for " + action);
			}

			// next
			pb = pb.getBodyNext();
		}

		logger.info("Plan confidence calculated as " + sum + "\\" + count + "=" + (sum / count) + "\n" + pString);
		return sum / count;
	}

	/**
	 * Gets confidence for a plan, starting from PB with execution context
	 * defined by bb. Also modifies bb.
	 * 
	 * @param pb
	 * @param bb
	 * @return
	 */
	protected float calculatePlanConfidenceMin(PlanBody pb, BeliefBase bb) {
		String pString = "Plan: " + pb;
		float minConf = -1f; // default return
		if (pb == null) {
			logger.warning("Plan for confidence calculation is null, will return 1!");
			return 1;
		} else if (pb.getBodyTerm().isInternalAction()) {
			PlanBody pb2 = pb.clonePB();
			boolean allInternal = true;
			while (pb2.getBodyNext() != null && allInternal) {
				if (!pb.isInternalAction() || hasCapability(pb2)) {
					allInternal = false;
				}
				pb2 = pb2.getBodyNext();
			}
			if (allInternal) {
				logger.finer(pb + " only internal actions without capabilities, return 1");
				return 1;
			}
		}
		int count = 0;
		while (pb != null && count < intentionConfidenceLookahead) {
			count++;
			Capability c = null;
			// get lit
			Literal action = (Literal) pb.getBodyTerm();
			logger.info(pb + "; Calculating confidence; type=" + pb.getBodyType());
			if (pb.getBodyType().equals(PlanBodyImpl.BodyType.addBel)
					|| pb.getBodyType().equals(PlanBodyImpl.BodyType.delAddBel)
					|| pb.getBodyType().equals(PlanBodyImpl.BodyType.delBel)
					|| pb.getBodyType().equals(PlanBodyImpl.BodyType.test)) {
				logger.severe("Calculate confidence; skipping " + action);
			} else if (pb.getBodyType().equals(PlanBodyImpl.BodyType.achieve)
					|| pb.getBodyType().equals(PlanBodyImpl.BodyType.achieveNF)
					|| pb.getBodyType().equals(PlanBodyImpl.BodyType.action)) {
				// get confidence....
				boolean isDep = hasDependencyContract(action);
				logger.info("Calculate confidence; " + action + ", (dep? " + isDep + "), " + "dependencies="
						+ getDependencies());
				// priority - do we have a dependency contract for this action?
				if (isDep && getDependencyContract(action).getExternalCapability() != null) {
					c = getDependencyContract(action).getExternalCapability();
					float actConf = c.getSpecificConfidence(bb, action);
					logger.info(action + " confidence (obligation contract) = " + actConf);
					if (actConf < minConf || minConf == -1) {
						minConf = actConf;
					}
				}

				// new if clause, to handle situations where have a contract but
				// no EC specified in it
				if (c == null && hasCapability(pb)) {
					c = getCapability(pb);
					float actConf = 0;
					if (!c.preconditionsHold(bb, action)) {
						logger.severe("conf value zero as preconds don't hold for " + action);
						actConf = 0;
					} else {
						if (action.isGround()) {
							actConf = c.getSpecificConfidence(bb, action);
						} else {
							actConf = c.getGeneralConfidence(bb);
						}
					}
					logger.info(action + " confidence (internal capability) = " + actConf);
					if (actConf < minConf || minConf == -1) {
						minConf = actConf;
					}
				}
				// external?
				else if (c == null && knowsProviderForExtCapability(action.getFunctor())) {
					// get set of capabilities and make sure we can use at least
					// one (based on precond)
					logger.info(
							"Get ecs for " + action + ", involved = " + MultiagentArch.getInvolvedAgents(action, this));
					Collection<ExternalCapability> caps = getExtCapability(action);

					if (caps.isEmpty()) {
						logger.info("Unable to find external capability for plan conf calculation;" + action);
					} else {
						logger.info("Got " + caps.size() + " ecs");
						float maxEcConf = -1f;// highest confidence of all
												// potential ec operators
						ExternalCapability high = null;
						for (ExternalCapability ec : caps) {
							boolean preconditionsHold = ec.preconditionsHold(bb, action);
							if (obligantIsExecuting(action) || preconditionsHold) {
								// take the highest executable confidence /
								// assume we are going highest confidence
								float conf = ec.getSpecificConfidence(bb, action);
								logger.info("Confidence for viable EC " + action + " = " + conf + ", max = " + maxEcConf
										+ ", executing ob? " + obligantIsExecuting(action));
								if (conf > maxEcConf) {
									maxEcConf = conf;
									high = ec;
								}
							} else if (!preconditionsHold) {
								String precond = "";
								for (String pre : ec.getPreconditions()) {
									precond = precond + "\n\tPre: " + pre;
								}
								logger.severe(action + " no preconditions holding in BB for this EC " + precond);
							}
							logger.finer(action + " confidence (external capability) = " + maxEcConf + "\n\tTS.C = "
									+ getTS().getC());
						} // end for
						logger.info(action + " highest confidence (external) = " + maxEcConf);
						if (maxEcConf < minConf || minConf == -1) {
							minConf = maxEcConf;
						}
						logger.finest(action + " overall confidence (external) = " + minConf);
						c = high;
						logger.info(action + ": Set c to highest EC confidence found (minConf=" + minConf + ", maxConf="
								+ maxEcConf + "); HC cap; " + c);
					} // end else
				} else if (c == null && !action.isInternalAction()) {
					logger.severe("(confidence calc) Capability in plan NOT found ; " + "action was " + action
							+ ", next pb=" + pb.getBodyNext() + "\nPC=" + getPrimitiveCapabilities().keySet() + "\nCC="
							+ getCompositeCapabilities().keySet() + "\nEC=" + getExtCapabilities() + "\nDeps="
							+ getDependencies());
					// return 0; //just keep going - don't just assume agent has
					// full knowledge....
				} else if (c == null) {
					logger.warning("Unable to get cap/conf values for " + action + " (internal? "
							+ action.isInternalAction() + ", get ext? " + getExtCapability(action) + ")");
				} else if (c != null) {
					logger.finer("Cap; " + c);
				}
			}
			// progress bb
			if (c != null) {
				bb = estimatePostExecutionBB(bb, action, c);
			} else {
				logger.warning("No c set for " + action);
			}

			// next
			pb = pb.getBodyNext();
		}

		logger.info("Plan confidence calculated as " + minConf + "\n" + pString);
		return minConf;
	}

	/**
	 * Returns true if we have delegated 'action' and are waiting on execution
	 * to complete
	 * 
	 * @param action
	 */
	public boolean obligantIsExecuting(Literal action) {
		synchronized (getWaitingForReply()) {
			boolean rt = false;
			Set<ActionExec> waitSet = getWaitingForReply().keySet();

			loop: for (ActionExec ae : waitSet) {
				if (ae.getActionTerm().equals(action)) {
					rt = true;
					break loop;
				}
			}
			logger.finer("Waiting on " + action + "=" + rt + "; AEs=\n\t" + waitSet + "Deps=" + getDependencies());
			return rt;
		}
	}

	/**
	 * Gets confidence for a plan, starting from PB with execution context
	 * defined by bb. Also modifies bb.
	 * 
	 * @param pb
	 * @param bb
	 * @return
	 */
	protected int calculatePlanCost(Plan p, BeliefBase bb) {
		PlanBody pb = p.getBody();
		int cost = calculatePlanCost(pb, bb);
		logger.finer("Cost " + cost + " for \n\t" + p.toASString());
		return cost;
	}

	/**
	 * Get cost starting from a particular plan body. All internal activities
	 * will return 0 unless they have an associated capability; minimum rt value
	 * for the whole plan is 0.
	 * 
	 * @param pb
	 * @param bb
	 * @return
	 */
	protected int calculatePlanCost(PlanBody pb, BeliefBase bb) {
		int cost = 0; // default return
		while (pb != null) {
			Capability c = null;
			// get lit
			Literal action = (Literal) pb.getBodyTerm();
			// get confidence....
			logger.finest("Calculate confidence; " + action);
			List<Literal> actionL = new Vector<Literal>();
			actionL.add(action);

			// new if clause, to handle situations where have a contract but no
			// EC specified in it
			if (c == null && hasCapability(pb)) {
				c = getCapability(pb);
			}

			// contract with capability info?
			if (c == null && hasDependencyContract(action)) {
				c = getDependencyContract(action).getExternalCapability();
			}

			// external, not in contrac?
			if (c == null && knowsProviderForExtCapability(action.getFunctor())) {
				// get set of capabilities and make sure we can use at least one
				// (based on precond)
				Collection<ExternalCapability> caps = getExtCapability(action);

				if (caps.isEmpty()) {
					logger.warning("Unable to find external capability for plan conf calculation;" + action);
				} else {
					logger.finer("Found " + caps.size() + " candidate capabilities for " + action);
					// presume we picked the higest confidence
					float maxEcConf = -1;// highest confidence of all potential
											// ec operators
					ExternalCapability high = null;
					for (ExternalCapability ec : caps) {
						if (ec.preconditionsHold(bb, action)) { // take the
																// highest
																// executable
																// confidence
							logger.finer("Identified ec with preconds met: " + ec.getPreconditions()[0]);
							float conf = ec.getSpecificConfidence(bb, action);
							if (conf > maxEcConf) {
								maxEcConf = conf;
								high = ec;
							}
						} else {
							String precond = "";
							for (String pre : ec.getPreconditions()) {
								precond = precond + "\n\tPre: " + pre;
							}
							logger.finer("Tested cap for " + action + " had no preconditions holding in BB" + precond);
						}
					}
					logger.finest(action + " (cost) highest confidence (external) = " + maxEcConf);
					c = high;
					logger.finest("Set c to highest confidence cap; " + c);
				} // end else
			}

			// progress bb
			if (c != null) {
				cost += c.getCostEstimate(action, bb);// can't do this if
														// unground...
				if (pb.isGround()) {
					bb = estimatePostExecutionBB(bb, action, c);
				}
			} else if (c == null && !action.isInternalAction()) {
				cost += 1;
			}

			// next
			pb = pb.getBodyNext();
		}
		return cost;
	}

	/**
	 * Inserts the changes described in a given collection, into a given bb.
	 * i.e. -terms are removed, +terms are added
	 * 
	 * @param provided
	 * @param estExecContext
	 * @return modified BB - passed by reference
	 */
	public BeliefBase insertChangeSetIntoBB(final Collection<String> provided, final BeliefBase bb) {
		if (provided.isEmpty()) {
			return bb;
		} // no changes
		logger.finer("CL changes; add " + provided + " to \n" + bb.toString());

		// iterate and add all changes
		for (String s : provided) {
			try {
				s = s.trim();
				boolean add = s.startsWith("+"); // else, remove the entry
				String tS = s.substring(1);// remove + or -
				Literal l = ASSyntax.parseLiteral(tS);
				if (!l.isGround()) {
					logger.severe("Given an unground term for addition/removal to BB; " + s);
				} else {
					if (add) {
						bb.add(l);
					} else {
						// need some extra code as removal requires matching of
						// any annotations
						Iterator<Literal> remIt = bb.getCandidateBeliefs(l, null);
						Vector<Literal> removeSet = new Vector<Literal>();
						// to prevent concurrentmod exceptions, as iterator tied to bb
						if (remIt != null) {
							while (remIt.hasNext()) {
								removeSet.add(remIt.next());
							}
							for (Literal rl : removeSet) {
								if (rl.equalsAsStructure(l)) {
									logger.finer("Remove " + l);
									bb.remove(rl);
								}
							} // end for rl
						} // end if remIt != null
					} // end remove case
				} // end ground case
			} catch (Exception e) {
				String se = "EXCEPTION in bb change " + s + "; " + e.toString();
				for (StackTraceElement ste : e.getStackTrace()) {
					se = se + "\n" + (ste.toString());
				}
				logger.severe(se);
			}
		}
		return bb;
	}

	/**
	 * Handles cancellation of an obligation; i.e. where the intention fails
	 * during execution or is dropped. Assumed to be received by an obligant and
	 * sent by the dependant.
	 * 
	 * @param m
	 */
	private void handleObligationCancellationMsg(final Message m) {
		try {
			final Literal activity = normalize(((Contract) m.getPropCont()).getActivity());

			/*
			 * If we've already removed, don't do anything - this presumes all
			 * associated depndencies are already purged.
			 */
			if (!hasObligationContract(activity)) {
				logger.finer("Skip handle for cancel " + activity + " as already removed\n" + "Ob=" + getObligations()
						+ "\nDep=" + getDependencies());
				return;
			}

			// are we currently executing?
			if (isExecuting(activity)) {
				if (UniversalConstants.DETAILED_CONTRACT_FORMATION) {
					JOptionPane.showMessageDialog(getUi(),
							new String[] { "Cancelling EXECUTING Obligation", activity.toString() },
							getId() + " Obligation cancelled", JOptionPane.INFORMATION_MESSAGE);
				}

				// handle cancellation of ongoing obligation - get the intention
				// and pop it all...
				// need to abort intention and send a result message...
				logger.info("Cancelling obligation; " + activity + "\nAgent " + getTS().getC());
				// end intention

				Intention i = findIntentionFromIntentions(activity);
				i = i != null ? i : tryGetIntentionFromSelectedIntention(activity);

				// try current action
				if (getTS().getC().getAction() != null) {
					Intention ai = getTS().getC().getAction().getIntention();
					if (ai != null) {
						Literal topTrigger = agentUtils().intentionIntoIMsQueue(ai).peekLast().getTrigger()
								.getLiteral();
						if (topTrigger.equalsAsStructure(activity)) {
							logger.finer(activity + " Matched selected intention: " + ai);
							i = ai;
						}
					}
				}

				// try pending actions - needed for AIs
				if (i == null) {
					Map<Integer, ActionExec> pa = getTS().getC().getPendingActions();
					pendLoop: for (ActionExec ae : pa.values()) {
						Intention in = ae.getIntention();
						Literal topTrigger = agentUtils().intentionIntoIMsQueue(in).peekLast().getTrigger()
								.getLiteral();
						if (topTrigger.equalsAsStructure(activity)) {
							logger.finer(activity + " Matched Q intention: " + in);
							i = in;
							break pendLoop;
						}
					}
				}

				if (i != null) {
					logger.info("Cancellation; ending i; " + i);
					logger.finer("i.peek().getCurrentStep(); " + i.peek().getCurrentStep());
					i.peek().getCurrentStep().setBodyNext(null);
					LinkedList<IntendedMeans> q = agentUtils().intentionIntoIMsQueue(i);
					for (int j = 1; j < q.size(); j++) {
						logger.finer("q(" + j + ") is " + q.get(j));
						q.get(j).getCurrentStep().setBodyNext(null);
					}
					if (i.peek() != null) {
						logger.info("Updated to end i; " + i + ", top is " + i.peek() + ", trigger is "
								+ i.peek().getTrigger());
					} else {
						logger.info("Updated to end i; " + i + ", top is null");
					}
				}

				// send a result to ensure dependant doesn't keep waiting
				// (suspend intention)
				m.setInReplyTo(m.getMsgId());
				m.setReceiver(m.getSender());
				m.setSender(getId());
				m.setPropCont(activity);
				// we don't register result at recipient anyway; just mark as
				// no-fail to continue execution(s)
				m.setIlForce(IL_OB_SUCCEED);

				try {
					getTS().getUserAgArch().sendMsg(m);
					logger.severe("Cancelled obligation " + activity + " with " + m + ", TS is now;\n" + getTS().getC());
				} catch (Exception e) {
					String stack = "";
					for (StackTraceElement s : e.getStackTrace()) {
						stack = stack + "\n" + s;
					}
					logger.severe("Error sending cancel response message for " + activity + "\nMsg;" + m.toString()
							+ "\n" + e.toString() + stack);
				}

			} else {
				if (UniversalConstants.DETAILED_CONTRACT_FORMATION) {
					JOptionPane.showMessageDialog(getUi(),
							new String[] { "Cancelling Obligation", activity.toString() },
							getId() + " Obligation cancelled", JOptionPane.INFORMATION_MESSAGE);
				}
			}

			completedExecution(activity);
			removeObligationContract(activity);
			removeObligationConfidence(activity);
			logger.info("Cancelled obligation contract for " + activity);

			if (getTS().getC().getPendingIntentions().containsKey(activity.toString())) {
				logger.info("Removing pending intention for obligation " + activity);
				getTS().getC().removePendingIntention(activity.toString());
			}
			else {
				logger.severe("Couldn't find pending intention? " + activity + "\n"
						+ getTS().getC().getPendingIntentions().keySet() + "\n" + getTS().getC());
			}

			// kill any dependencies formed for this obligation
			for (Literal ob : getDependencies()) {
				if (agentUtils().isSubChildOf(ob, activity)) {
					// this will kill the dependency and all 'children' of it;
					// i.e. that were formed to meet the obligation
					logger.finer("Cancelled obligation had associated dependency for " + ob + "...");
					cancelDependency(ob, true);
				}
			}
		} catch (Exception e) {
			String st = "";
			for (StackTraceElement s : e.getStackTrace()) {
				st = st + "\n\t" + s;
			}
			logger.severe(e + " handling " + m + st);
		}
	}

	private Intention tryGetIntentionFromSelectedIntention(final Literal activity) {
		Intention si = getTS().getC().getSelectedIntention();
		if (si != null) {
			Literal topTrigger = agentUtils().intentionIntoIMsQueue(si).peekLast().getTrigger().getLiteral();
			if (topTrigger.equalsAsStructure(activity)) {
				logger.finer(activity + " Matched selected intention: " + si);
				return si;
			}
		}
		return null;
	}

	private Intention findIntentionFromIntentions(final Literal activity) {
		Queue<Intention> iq = getTS().getC().getIntentions();
		for (Intention in : iq) {
			Literal topTrigger = agentUtils().intentionIntoIMsQueue(in).peekLast().getTrigger().getLiteral();
			if (topTrigger.equalsAsStructure(activity)) {
				logger.finer(activity + " Matched Q intention: " + in);
				return in;
			}
		}
		return null;
	}

	public boolean isContinual() {
		return false;
	} // default

	public boolean isContinualMaintenance() {
		return false;
	} // default

	/**
	 * Searches the intention IMs and attempts to form contracts for any
	 * <b>ground</b> joint actions requiring them. Presumes a ground
	 * intention. </br>
	 * </br>
	 * This method also checks and updates any contracts where the bb changes
	 * provided by the dependent have changed, i.e. due to maintenance activity
	 * inserting new actions and/or removing old ones. </br>
	 * </br>
	 * Clones the passed in intention, such that we create new action literals
	 * for the contract requests; expected that the literal is set with the same
	 * annotations for action-request calls.
	 * 
	 * @param in
	 */
	public void formContracts(Intention in) {
		if (in.peek().getTrigger().getLiteral().getFunctor().startsWith("kqml_")) {
			return;
		}

		int id = in.getId();
		Literal topTrigger = agentUtils().intentionIntoIMsQueue(in).peekLast().getTrigger().getLiteral();
		logger.info("Forming contracts for intention id " + id + ", topTrigger=" + topTrigger);

		// finished bad, suspended ok (not warning situ), wrong arch severe
		if (!(getTS().getUserAgArch() instanceof MultiagentArch)) {
			logger.severe("CFRM: User arch is not compatible; type is "
					+ getTS().getUserAgArch().getClass().getCanonicalName());
			return;
		}
		if (in.isFinished()) {
			logger.warning(id + "; CFRM: aborting contract formation for finished intention!");
			return;
		}
		if (in.isSuspended()) {
			logger.finer(id + "; CFRM: contract formation for suspended intention");
			return;
		}
		logger.finer("CFRM: Intention details; " + in.toString());

		in = in.clone(); // prevents reference-related issues when iterating
							// through ims

		// required for testing for joint actions
		MultiagentArch arch = (MultiagentArch) getTS().getUserAgArch();
		Iterator<IntendedMeans> inIt = agentUtils().intentionIntoIMsQueue(in).iterator();

		// NB: if we don't have a contract AND have tried AND this is next IM,
		// skip step and fail on
		// next exec

		// changes made to the BB (cumulative) after this and any preceding
		// action complete. Used for
		// setting the estimated exec. context in the FOLLOWING action(s)
		Collection<String> bbChanges = new Vector<String>();
		LoopForIMs: while (inIt.hasNext()) {
			IntendedMeans imeans = inIt.next();
			logger.fine(id + ":CFRM: Form contracts: imeans = " + imeans + ", cause=" + imeans.getTrigger());

			// current plan body
			PlanBody current = imeans.getCurrentStep();
			// get effects associated with each action, from the current
			while (current != null) {
				logger.fine(id + ":CFRM: Form contracts: currentStep = " + current.getBodyTerm());

				// check term is suitable
				// we support only action types; goal addition should be treated
				// as an action call, which is then
				// treated as an outsourced action by the delegation logic in
				// CapabilityAwareAgent
				// get the post-effects from the associated task knowledge
				if (current.getBodyType().equals(PlanBody.BodyType.achieve)) {
					logger.finer("CFRM: Body == goal addition; " + current);
					Literal l = (Literal) current.getBodyTerm();
					if (getTaskFactory().factory(l) != null) {
						GoalTask goal = getTaskFactory().factory(l);
						l.apply(imeans.getUnif());
						logger.fine("CFRM: got goal task " + goal + ", unified action=" + l);

						if (!l.isGround()) {
							logger.fine("CFRM: unground action=" + l);
							break LoopForIMs;
						}

						bbChanges = agentUtils().putEffectsIntoBBChanges(goal.getSignature(), goal.getGoalEffects(), l,
								bbChanges);
						logger.fine("CFRM: inserted goal task changes;\n" + bbChanges);
					}
				} else if (!current.getBodyType().equals(PlanBody.BodyType.action)) {
					logger.fine("CFRM: Body != action type, body is " + current.getBodyType() + ", -> skipping "
							+ current.getBodyTerm());
				} else if (!current.getBodyTerm().isInternalAction() && current.getBodyTerm().isLiteral()) {
					// Checks if this is the next action to be performed;
					// performed here before the addition of annotations
					// using string comparator for more reliable check than
					// object.equals

					// we need to clone and then unify vars to compare...
					// clone of the top IMs for the intention
					IntendedMeans peekImClone = (IntendedMeans) (in.peek().clone());

					// apply unifiers to current steps in the (cloned copy)
					// current IM
					if (peekImClone.getCurrentStep() != null && peekImClone.getUnif() != null) {
						peekImClone.getCurrentStep().apply(peekImClone.getUnif());
					}

					logger.fine("CFRM: Current = " + current + " PeekImClone.current=" + peekImClone.getCurrentStep()
							+ " unif=" + peekImClone.getUnif());

					// situation where peekImClone may be null...
					// if we have executed the lastmost action in the current
					// IMeans/plan, then next step is null
					// the TS will pop the IM and progress the upper IM
					// actions... we need to do the same.
					Unifier prevU = new Unifier();// for refs over 2 levels
					if (peekImClone.getCurrentStep() == null) {
						Iterator<IntendedMeans> innit = in.iterator();
						prevU = innit.next().getUnif(); // i.e. set to current,
														// so we can move to
														// next...
						if (innit.hasNext()) {
							peekImClone = (IntendedMeans) innit.next().clone();
						}
						logger.fine(id + " CFRM: execution is about to pop IM level, so set clone to " + peekImClone);
						logger.fine("CFRM: Unifier old = " + prevU + ", current = " + peekImClone.getUnif());
					}

					// unification....
					final Literal action = (Literal) current.getBodyTerm();
					current.getBodyTerm().apply(imeans.getUnif());

					final Literal dependencyCheckingClone = (Literal) action.clone();
					final Literal obligationCheckingClone = (Literal) action.clone();

					boolean isNextAction;

					if (peekImClone.getCurrentStep() == null) {
						logger.finer(id + " CFRM:  can't get a current IM figured out...");
						isNextAction = false;
					} else {
						// use string compare because literal/term doesn't seem
						// to do equals() properly(?)
						isNextAction = current.getBodyTerm().toString()
								.equals(peekImClone.getCurrentStep().getBodyTerm().toString());
					}

					logger.finer("CFRM: Next action ? = " + isNextAction);
					logger.finer("CFRM: Skipform fail list; " + contractRejectImmediatelyPriorToExec);

					// set annotations here for the actions
					final Literal intentionTrigger = imeans.getTrigger().getLiteral();// source
																						// of
																						// dependent
																						// id,
																						// etc
					arch.setActionAnnotations(getId(), id, topTrigger, dependencyCheckingClone);
					obligationCheckingClone.setAnnots(intentionTrigger.getAnnots());

					// check action is ground and, if so, does it require a
					// contract to be formed?
					if (action.isGround() && MultiagentArch.isJointActivity(action, this)
							&& !hasObligationContract(obligationCheckingClone)
							&& !hasDependencyContract(dependencyCheckingClone)) {

						// set the annots that the delegated obligation will
						// have for this action
						arch.setActionAnnotations(getId(), id, topTrigger/* intentionTrigger */, action);
						logger.finer(id + ": CFRM: Trying to form contract: " + action.toString() + ", dependencies = "
								+ getDependencies());

						if (isNextAction && contractRejectImmediatelyPriorToExec.contains(normalize(action))) {
							logger.info(
									id + ": CFRM: Have previously failed with " + action + ", so aborting contract");
							// skip contract formation and exit
							// first remove entry to avoid skipForm getting too
							// long
							contractRejectImmediatelyPriorToExec.remove(normalize(action));
							// exit loop to end
							break LoopForIMs;
						} else {
							logger.fine(id + ":CFRM: OkGO - imeans=" + imeans + "\n\t- in=" + in.peek()
									+ "\n\t- current=" + current + "\n\t- bb changes=" + bbChanges);
							formContractForStep(current, in, intentionTrigger, bbChanges);
						}
					} else {
						if (!action.isGround()) {
							logger.info("CFRM: Unground " + action + ";" + peekImClone.getUnif());
						}
						if (!MultiagentArch.isJointActivity(action, this)) {
							logger.finer("CFRM: Not JA: " + action);
						}
						if (hasObligationContract(obligationCheckingClone)) {
							logger.info("CFRM: Is an obligation " + obligationCheckingClone);
						}
						// i.e. agent has assumed its obligation to us
						if (hasDependencyContract(dependencyCheckingClone)) {
							logger.info("CFRM: Dependency exists " + dependencyCheckingClone);
							Contract c = getDependencyContract(dependencyCheckingClone);
							/*
							 * check contract has correct BB changes; if not,
							 * send an update... (NB: that is, check established
							 * conditions for the action, NOT FROM the action)
							 */
							if (changesEqual(bbChanges, c.getBeliefChangesFromDependent())) {
								logger.finer("CFRM: No changes between contract " + c.getActivity() + " bb changes \n("
										+ c.getBeliefChangesFromDependent() + ")\nand this loops calculated changes\n("
										+ bbChanges + ")");
							} else {
								// if the bb changes are different, we update
								// the contract and send a message to indicate
								logger.info("CFRM: Differences between contract " + c.getActivity() + " bb changes \n("
										+ c.getBeliefChangesFromDependent() + ")\nand this loops calculated changes\n("
										+ bbChanges + ")");
								c.setBeliefChangesFromDependent(bbChanges);
								sendContractChange(c);
							}
						}
						logger.fine("Dependencies; " + getDependencies());
						logger.fine("Obligations; " + getObligations());
					}

					// moving towards handling the next action...
					// add post-effects into the bbchanges set, now that we've
					// done everything else

					// do we have contract with a specific capability info set?
					// if so, we can use this more specific information
					if (action.isGround() && hasDependencyContract(action)
							&& (getDependencyContract(action).getExternalCapability() != null)) {
						Capability c = getDependencyContract(action).getExternalCapability();
						logger.info(action + ": Getting change info from dependency contract...\nc=" + c);
						bbChanges = agentUtils().putEffectsIntoBBChanges(c.getSignature(), c.getPostEffects(), action,
								bbChanges);
					} else if (hasCapability(current)) {
						logger.info("Preparing to update bbChanges set (" + bbChanges + ") for " + action);
						if (action.isGround()) {
							Capability c = getCapability(current);
							bbChanges = agentUtils().putEffectsIntoBBChanges(c.getSignature(), c.getPostEffects(),
									action, bbChanges);
						}
					}
					// alternate - no contract, so try using task info?
					else {
						final GoalTask g = getTaskFactory().factory(action);
						if (g != null) {
							logger.info(action + ": Getting info from goal task; " + g.toString());
							bbChanges = agentUtils().putEffectsIntoBBChanges(g.getSignature(), g.getGoalEffects(),
									action, bbChanges);
						}
					}
				}

				// next iteration...
				current = current.getBodyNext();
			}

			// check trigger event; if this 'level' of IM was triggered by
			// failure on the next abstract level, then
			// we do NOT try and form contracts
			if (imeans.getTrigger().getType().equals(TEType.achieve)
					&& imeans.getTrigger().getOperator().equals(TEOperator.del)) {
				logger.fine(id + ":Skip upper layers of IMs due to trigger " + imeans.getTrigger()
						+ "being failed goal/activity");
				break LoopForIMs;
			}
		}

		// end of IMs loop; bb changes corresponds to cumulative changes of this
		// plan / intention
		if (hasObligationContract(topTrigger)) {
			Contract c = getObligationContract(topTrigger);
			if (!changesEqual(bbChanges, c.getBeliefChangesFromDependent())) {
				logger.info(topTrigger + " updating ob contract with new effects:\n" + bbChanges);
				c.setBeliefChangesFromObligant(bbChanges);
				sendContractChange(c);
			}
		}
	}

	/**
	 * @param changes1
	 * @param changes2
	 * @return true if both sets are identical in content, ordering is not
	 *         important
	 */
	protected static final boolean changesEqual(Collection<String> changes1, Collection<String> changes2) {
		if (changes1.size() != changes2.size()) {
			return false;
		}
		final Iterator<String> it1 = changes1.iterator(), it2 = changes2.iterator();
		while (it1.hasNext()) {
			if (!(it1.next().equals(it2.next()))) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Send a message that we've changed (potentially) a dependency contract;
	 * changes (at time of writing) can stem either from the dependent modifying
	 * the casual states established, or from the obligant changing the external
	 * capability representing (an abstract view of) how it achieves the task.
	 * 
	 * @param c
	 */
	protected void sendContractChange(final Contract c) {
		if (sendUpdateMessages) {
			final LinkedList<String> recip = new LinkedList<>();
			if (c.getType().equals(Contract.Type.Dependency)) {
				recip.addAll(getInvolvedAgents(c.getActivity()));
			} else {
				recip.add(c.getDependentId());
			}
			recip.remove(getId());
			final Message update = new Message(IL_CONTRACT_CHANGED, getId(), null, c);
			for (final String agent : recip) {
				update.setReceiver(agent);
				try {
					getTS().getUserAgArch().sendMsg(update);
				} catch (final Exception e) {
					throw new RuntimeException(e);
				}
			}
			logger.info("Sent contract change " + c.getActivity() + " to " + recip + "\n\t" + c);
		}
	}
	
	/**
	 * Forms a contract for the action in PlanBody <i>current</i>, from the
	 * passed <i>in</i> intention
	 * 
	 * @param current
	 * @param in
	 * @param collection
	 */
	private void formContractForStep(PlanBody current, Intention in, Literal trigger, Collection<String> effects) {
		Literal action = (Literal) current.getBodyTerm();
		logger.finer("Form contract for step: " + action);

		if (waitForOblAccept.containsKey(action)) {
			logger.warning("Already waiting on dependency for " + action + "; waitSet = " + waitForOblAccept.get(action));
			return;
		}

		if (hasDependencyContract(action)) {
			logger.warning("Trying to form contract for " + action + " when we already have a dependency established");
			return;
		}

		if (UniversalConstants.DETAILED_CONTRACT_FORMATION) {
			JOptionPane.showMessageDialog(getUi(), new String[] { "Trying to form contract: ", action.toString(),
					"Provided effects=", effects.toString() }, this.getId(), JOptionPane.INFORMATION_MESSAGE);
		}
		logger.finer("Trying to form contract for step " + action + " in intention " + in.getId()
				+ ", CL effects to be added to BB=" + effects.toString());

		// get agents involved in this activity
		// and remove self from list (we cannot be obligant and dependant)
		final LinkedList<String> involved = MultiagentArch.getInvolvedAgents(action, this);
		involved.remove(getId());

		// add temp store info for intention; wake intention when all requests
		// complete
		if (!waitingForDep.containsKey(in.getId())) {
			waitingForDep.put(in.getId(), new Vector<Literal>());
		}
		waitingForDep.get(in.getId()).add(action);
		logger.finer("Waiting for dependencies; " + in.getId());

		// start contract formation process
		for (final String ai : involved) {
			sendDependencyRequestToObligant(trigger, effects, action, ai);
		}
		sleepIntention(in);
	}

	/**
	 * Sends initial messages to potential obligants
	 * 
	 * @param trigger
	 *            - parent intention trigger, used to get MP if there isn't one
	 *            directly associated with the action
	 * @param effects
	 *            - post-effects of preceding action - may be empty, but is used
	 *            to adjust the expected execution context with states
	 *            established at the time of execuction
	 * @param action
	 *            - the action literal
	 * @param ai
	 *            - the name of the obligant to message
	 */
	private void sendDependencyRequestToObligant(Literal trigger, Collection<String> effects, Literal action,
			String ai) {
		try {
			logger.finer(
					"Sending contract request to potential obligant " + ai + " for " + action + " trigger=" + trigger);

			// do annotations - dependant, dependantIntention
			if (UniversalConstants.DETAILED_CONTRACT_FORMATION) {
				JOptionPane.showMessageDialog(getUi(),
						new String[] { "Sending contract request to potential obligant " + ai, " for " + action,
								" where trigger=" + trigger },
						getId() + ": message " + ai, JOptionPane.INFORMATION_MESSAGE);
			}

			// get MP; use specific for action if possible, then for trigger -
			// if none for latter, default will
			// be automatically returned instead
			MaintenancePolicy mpForRequest;
			if (hasMaintenancePolicy(action.getFunctor())) {
				mpForRequest = getMaintenancePolicy(action.getFunctor());
			} else {
				mpForRequest = getMaintenancePolicy(trigger.getFunctor());
			}

			// contract for proposal
			Contract contract = new Contract(Contract.Type.Obligation, action, mpForRequest, effects);
			logger.finer("Set contract " + contract.toString());

			// form the message
			Message request = new Message(IL_DEPENDENCY_REQ, null, null, contract);
			request.setSender(getId());
			request.setReceiver(ai);

			// send the message
			this.getTS().getUserAgArch().getArchInfraTier().sendMsg(request);

			// wait for result; store required info in temp data objects
			// add to msg waitlist for literal
			if (waitForOblAccept.containsKey(action)) {
				waitForOblAccept.get(action).add(ai);
			} else {
				Collection<String> waitList = new Vector<String>();
				waitList.add(ai);
				waitForOblAccept.put(action, waitList);
			}
			logger.finer("Waitset for response for action " + action + " is " + waitForOblAccept.get(action));
		} catch (Exception e) {
			logger.severe(
					"Exception in formContract; args are trigger=" + trigger + ", action=" + action + ", ai=" + ai);
			logger.severe(e.toString());
			for (StackTraceElement s : e.getStackTrace()) {
				logger.severe(s.toString());
			}
		}
	}

	/**
	 * @param in
	 */
	protected void sleepIntention(Intention in) {
		in.setSuspended(true); // suspend intention...
		getTS().getC().addPendingIntention(INTENTION_SUSPENDED + in.getId(), in);// add
																					// to
																					// pending
																					// set
		getTS().getC().removeIntention(in); // remove from intentions under
											// consideration
		logger.finer("Suspended " + in.getId() + "\n" + getTS().getC());
	}

	/**
	 * Wakes up a suspended (pending) intention
	 * 
	 * @param intId
	 */
	protected void wakeupIntention(int intId) {
		Intention in = getTS().getC().removePendingIntention(INTENTION_SUSPENDED + intId);
		if (in == null) {
			logger.severe("Intention not found in PA! " + intId + ";\n C=" + getTS().getC().toString());
		} else {
			in.setSuspended(false);
			getTS().getC().addIntention(in);// .resumeIntention(in);
			logger.finer("Resumed intention " + in);
		}

	}

	/*
	 * =========================================================================
	 * ============================ Action result handling
	 * =========================================================================
	 * ============================
	 */

	/**
	 * Overrides to handle situations where result is for a dep. with no
	 * contract... treats these results as successes at all times, so they can't
	 * 'fail' the intention. Makes assumption that we don't care about failure
	 * of non-contracted activities.
	 */
	@Override
	protected void handleResult(Message m) {
		Literal action = (Literal) m.getPropCont();
		if (!hasDependencyContract(action) && m.getIlForce().equals(IL_OB_FAILED)) {
			logger.severe("No dependency formed or held for action " + action + "\nDiscard:" + m);
			action.addAnnot(ASSyntax.createString(UniversalConstants.ANNOT_FOR_CANCELLED_DP));
			m.setPropCont(action);
			m.setIlForce(IL_OB_SUCCEED); // force pass and increment to next...
		}
		super.handleResult(m);
	}

	@Override
	protected void completedDependency(final ActionExec ae) {
		removeDependencyContract((Literal) ae.getActionTerm());
	}

	@Override
	public void recordSuccess(Literal g) {
		logger.info("Succeed " + g + " (obligation?=" + isExecuting(g) + ")" + ", executing obligations="
				+ currentlyExecuting() + ", dependency?=" + getDependencies().contains(normalize(g)));
		contractRejectImmediatelyPriorToExec.clear();
		super.recordSuccess(g);
		if (hasDependencyContract(g)) {
			logger.finer("RS:Cancel dependency(s) for " + g);
			cancelDependency(g, false); // false - keep related contracts
		} else if (hasObligationContract(g)) {
			logger.finer("RS:Remove obligation for " + g);
			removeObligationContract(g);
			removeObligationConfidence(g);
		}
	}

	@Override
	public void intentionDropped(Intention i) {
		logger.severe("Intention dropped; CFA call for " + i);
		if (i != null) {
			Collection<Literal> rem = new Vector<Literal>();
			for (Literal ob : getDependencies()) {
				if (agentUtils().dependencyForIntention(ob, getId(), i.getId())) {
					logger.severe("IDrop: remove dependency entry for subtask " + ob + " of failed " + i);
					rem.add(ob);
				}
			}

			if (!rem.isEmpty()) {
				logger.severe("IDrop: Removal set for " + i.getId() + "=" + rem);
				for (Literal removeMe : rem) {
					cancelDependency(removeMe, true);
				}
			}
		}
		super.intentionDropped(i);
	}

	/**
	 * Records failure; searches for associated dependencies and cancels them.
	 * Assumes obligation related messages are performed prior to invocation
	 */
	@Override
	public void recordFailure(final Literal g) {
		logger.severe("Record failure for " + g + ", C=" + getTS().getC());
		Intention i = ts.getC().getSelectedIntention();
		contractRejectImmediatelyPriorToExec.clear();
		if (i != null) {
			this.intentionDropped(i);
		}
		super.recordFailure(g);
	}

	/**
	 * Cancels the held dependency, and optionally subordinate
	 * dependencies/obligations, for the goal identified by the passed in
	 * literal g.
	 * 
	 * @param g
	 *            Literal; should include annotations for the dependency and
	 *            dependant intention id.
	 * @param cancelRelated;
	 *            if true, also remove associated dependencies - used for
	 *            failure case
	 */
	protected void cancelDependency(Literal g, boolean cancelRelated) {
		g = normalize(g);

		logger.finer("Cancelling dependencies for " + g + " from " + getDependencies());

		// remove sub-dependencies
		final Vector<Literal> cancel = new Vector<>();
		if (hasDependencyContract(g) && !cancel.contains(g)) {
			cancel.add(g);
		}

		// check for plan-associated dependencies; in the same plan, or a
		// subchild of
		if (cancelRelated) {
			for (final Literal action : getDependencies()) {
				if (agentUtils().isSubChildOf(action, g) && !cancel.contains(action)) {
					logger.info(action + " is child of " + g + ", cancelling dep");
					cancel.add(action);
				} else if (agentUtils().isInSamePlan(action, g) && !cancel.contains(action)) {
					logger.info(action + " within same plan as " + g + ", cancelling dep");
					cancel.add(action);
				}
			}
		}
		String cancelString = "";
		for (final Literal a : cancel) {
			cancelString = cancelString + "\n\t" + a;
		}
		logger.warning("Cancelling dependencies;\n" + cancelString);
		for (final Literal l : cancel) {
			if (hasDependencyContract(l)) {
				logger.info("Cancelling dependency contract for " + l);
				Contract c = getDependencyContract(l);
				removeDependencyContract(l);

				final List<String> actors = getInvolvedAgents(l);
				actors.remove(getId());
				if (!contractWaitList().isEmpty() && contractWaitList().containsKey(l)) {
					waitForOblAccept.remove(l);
				}

				for (final String rcv : actors) {
					Message cancellation = new Message(IL_DEPENDENCY_CANCEL, getId(), rcv, c);
					try {
						this.getTS().getUserAgArch().sendMsg(cancellation);
					} catch (Exception e) {
						logger.severe("Error sending cancellation message " + cancellation + " to " + rcv);
						logger.severe(e.toString());
						for (StackTraceElement s : e.getStackTrace()) {
							logger.severe(s.toString());
						}
					}
				}
			}
		}

		logger.fine("Done cancellation; dependencies are now " + getDependencies());
	}

	private Intention lastContractF = null;

	/**
	 * Handles forming contracts for unfinished / unsuspended intentions. Should
	 * (?) handle both atomic and non-atomic intentions; called when the
	 * intention is updated and added after an action has executed.
	 * 
	 * @param i
	 *            - intention object
	 */
	@Override
	protected void moveToNextIntentionAction(Intention i) {
		if ((i.peek() != null) && (i.peek().getCurrentStep() != null) & (!i.isSuspended()) && (!i.isFinished())) {
			/*
			 * Check to ensure that we only make one formContract call before
			 * each action execution - as otherwise we can get infinite loops
			 * from failed contract requests, as receipt of the 'fail' message
			 * causes a resumeIntention that triggers this, then formContract,
			 * then a wait for result...
			 */
			if (lastContractF != null && i.peek().getCurrentStep().equals(lastContractF.peek().getCurrentStep())) {
				logger.info("Not repeating formContract call for intention" + "\n\t" + i.peek() + "\nLF=" + "\n\t"
						+ lastContractF.peek());
			} else {
				logger.info("formContract call for intention\n" + i);
				formContracts(i);
				lastContractF = i.clone();
			}
		}
	}

	/**
	 * Scans both the internal, external and obligation associated capabilities
	 * for a matching result (precedence is obligation>internal>external)
	 * 
	 * @param action
	 *            - literal action; expect this to be ground, but not restricted
	 *            to it
	 * @param bb
	 *            - belief base, used for execution
	 * @return
	 */
	public Capability findActionCapability(final PlanBody action, final BeliefBase context) {
		Literal l = (Literal) action.getBodyTerm();
		if (hasDependencyContract(l) && getDependencyContract(l).getExternalCapability() != null) {
			return getDependencyContract(l).getExternalCapability();
		}
		if (hasDependencyContract(l) && getDependencyContract(l).getExternalCapability() != null) {
			return getDependencyContract(l).getExternalCapability();
		}

		// if we don't have anything stored, search
		if (hasCapability(action)) {
			return getCapability(action);
		} else if (knowsProviderForExtCapability(l.getFunctor())) {
			Collection<ExternalCapability> cSet = getExtCapability(l);
			logger.warning("Got " + cSet.size() + " external providers for: " + action.toString());
			ExternalCapability bestCap = null;
			float highestEcConf = -1;
			// select highest confidence rep
			for (ExternalCapability ec : cSet) {
				if (ec.getGeneralConfidence(context) > highestEcConf) {
					highestEcConf = ec.getGeneralConfidence(context);
					bestCap = ec;
				}
			}
			logger.fine("The highest EC confidence for " + action + " was " + highestEcConf);
			return bestCap;
		} else {
			logger.severe("Unable to find a capability for " + action);
			return null;
		}
	}

	/**
	 * Fetch all invovled agents for the given MA activity
	 * 
	 * @param activity
	 * @return
	 */
	public LinkedList<String> getInvolvedAgents(Literal activity) {
		return MultiagentArch.getInvolvedAgents(activity, this);
	}

	/*
	 * =========================================================================
	 * ============================ UI setup; show contracts held as well as
	 * intentions, obligations, etc in parent impl
	 * =========================================================================
	 * ===========================
	 */

	/**
	 * Creates a debug ui window; different from that used in the superclass...
	 */
	@Override
	protected void createDebugUi() {
		ui = new CaaUiWindow(this);
	}
}
