package agent.type;

import jason.asSyntax.Literal;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import agent.maintenance.MaintenancePolicy;
import agent.model.dependency.Contract;

import common.UniversalConstants;

/**
 * This class exists purely to encapsule the data structures holding obligation an
 * dependency info, in order to force classes to go via this API.
 */
public abstract class ContractHoldingAgent extends MaintenancePolicyHolder {

	/**
	 * Stores contracts for dependencies - where this agent is relying upon one or more obligant
	 * to perform a given action literal.  The agents involved can be inferred from the 
	 * arguments in the action literal
	 * </br></br>
	 * Note; literals should have dependency info in their annotations, to be used as unique keys
	 */
	private Map<Literal, Contract> dependenciesFormed = new HashMap<Literal, Contract>();

	/**
	 * Contracts for obligations - where this agent is contracted to perform an action (form
	 * a relevant intention) for another agent, for the given action literal
	 * </br></br>
	 * Note; literals should have dependency info in their annotations, to be used as unique keys
	 */
	private Map<Literal, Contract> obligationsHeld = new HashMap<Literal, Contract>();

	/**
	 * A counter for the number of times an obligation is maintained by a dependant; used because we can't
	 * annotate the trigger literal itself without also breaking the various literal-keyed maps.
	 */
	private Map<Literal, Integer> dependencyMaintainedByObligant = new HashMap<Literal, Integer>();

	/**
	 * Stores the last change in obligation confidence
	 */
	private Map<Literal, Float> myConfidenceInObligations = new HashMap<Literal, Float>();
	

	/*===============================================================================================================
	 * 
	 * Contract get/set/fetch etc type methods
	 * 
	 *=============================================================================================================*/

	public final boolean hasDependencyContract(Literal action)	{	
		synchronized(dependenciesFormed)	{
			action = normalize(action);
			return dependenciesFormed.containsKey(action);		
		}
	}

	public final Contract getDependencyContract(Literal action)	{	
		synchronized(dependenciesFormed)	{
			action = normalize(action);
			return dependenciesFormed.get(action);		
		}
	}

	public final void setDependency(Literal action, Contract c)	{
		synchronized(dependenciesFormed)	{
			action = normalize(action);
			dependenciesFormed.put(action, c);
		}
	}


	public final void removeDependencyContract(Literal action)	{
		action = normalize(action);
		dependenciesFormed.remove(action);
		dependencyMaintainedByObligant.remove(action);
	}


	public final boolean hasObligationContract(Literal action)	{	
		synchronized(obligationsHeld)	{
			action = normalize(action);
			return obligationsHeld.containsKey(action);	
		}
	}

	public final Contract getObligationContract(Literal action)	{	
		synchronized(obligationsHeld)	{
			action = normalize(action);
			return obligationsHeld.get(action);			
		}
	}

	public final void setObligationContract(Literal action, Contract c)	{
		synchronized(obligationsHeld)	{
			action = normalize(action);
			obligationsHeld.put(action, c);
		}
	}

	public final void removeObligationContract(Literal action)	{
		action = normalize(action);
		obligationsHeld.remove(action);
		executingObligations.remove(action);
		myConfidenceInObligations.remove(action);

		if(getTS().getC().getPendingIntentions().containsKey(action.toString()))	{
			logger.finer("Removing cancelled pending obligation for " + action);
			getTS().getC().getPendingIntentions().remove(action.toString());
		}
	}

	/**
	 * Get literals identifying all dependencies held (delegated to other agents as
	 * obligations)
	 * @return
	 */
	public final Set<Literal> getDependencies()	{	
		synchronized(dependenciesFormed)	{
			Set<Literal>set = new HashSet<Literal>();
			set.addAll(dependenciesFormed.keySet());
			return set;		
		}
	}

	/**
	 * Get literals identifying all obligations held (delegated by other agents)
	 * @return
	 */
	public final Set<Literal> getObligations()	{
		synchronized(obligationsHeld)	{
			Set<Literal>set = new HashSet<Literal>();
			set.addAll(obligationsHeld.keySet());
			return set;			
		}
	}

	/*
	 * Confidence in obligations, sent to dependant
	 */

	/**
	 * Return true if we've previously calculated and stored a confidence for obligation denoted by
	 * the literal trigger 'ob'
	 * @param ob
	 * @return
	 */
	public final boolean hasPreviousConfidenceForOb(Literal ob)	{	
		synchronized(myConfidenceInObligations)	{
			ob = normalize(ob);
			return myConfidenceInObligations.containsKey(ob);	
		}
	}

	/**
	 * Returns the last confidence value recorded for the given obligation, or null if none present
	 * @param ob - literal
	 * @return float/Float
	 */
	public final float lastObligationConfidence(Literal ob)	{
		synchronized(myConfidenceInObligations)	{
			ob = normalize(ob);
			return myConfidenceInObligations.get(ob);
		}
	}

	/**
	 * Stores the given value newValue as confidence for the obligation ob
	 * @param ob
	 * @param newValue
	 */
	public final void updateObligationConfidence(Literal ob, float newValue)	{
		synchronized(myConfidenceInObligations)	{
			ob = normalize(ob);
			myConfidenceInObligations.put(ob, newValue);	
		}
	}


	public final void removeObligationConfidence(Literal ob) {
		synchronized(myConfidenceInObligations)	{
			ob = normalize(ob);
			myConfidenceInObligations.remove(ob);
		}
	}

	/*
	 * Related to maint. messaging from obligant
	 */
	////dependencyMaintainedByObligant
	/**
	 * Return true if this dependency was previously maintained by the obligant
	 * @param activity
	 * @return
	 */
	public final boolean dependencyPreviouslyMaintained(Literal activity)	{
		synchronized(dependencyMaintainedByObligant)	{
			activity = normalize(activity);
			return dependencyMaintainedByObligant.containsKey(activity);
		}
	}

	/**
	 * Return the number of times activity was maintained by the/a dependant
	 * @param activity
	 * @return
	 */
	public final int getMaintCount(Literal activity)	{
		synchronized(dependencyMaintainedByObligant)	{
			activity = normalize(activity);
			if(dependencyMaintainedByObligant.containsKey(activity))	{
				return dependencyMaintainedByObligant.get(activity);
			}
			else	{	return 0;	}
		}
	}


	/**
	 * Update maintenance count for activity
	 * @param activity
	 * @param count
	 */
	public final void setMaintCount(Literal activity, int count)	{
		synchronized(dependencyMaintainedByObligant)	{
			activity = normalize(activity);
			dependencyMaintainedByObligant.put(activity, count);
		}
	}


	public final void removeMaintCount(Literal activity)	{
		synchronized(dependencyMaintainedByObligant)	{
			activity = normalize(activity);
			dependencyMaintainedByObligant.remove(activity);
		}
	}

	//EXECUTION MONITORING FOR OBLIGATIONS
	/**
	 * Stores literals corresponding to executing obligations; used to determine if we have to message the dependant
	 * etc upon completion
	 */
	private Vector<Literal> executingObligations = new Vector<Literal>();

	@Override
	protected final void setExecuting(Literal action)	{
		synchronized(executingObligations)	{
			action = normalize(action);
			//avoid duplication...
			if(!executingObligations.contains(action))	{
				executingObligations.add(action);
			}
		}
	}

	/**
	 * Removes the obligation from the 'executing' list
	 */
	@Override
	protected final void completedExecution(Literal action)	{
		synchronized(executingObligations)	{
			executingObligations.remove(normalize(action));
		}
	}

	@Override
	protected final boolean isExecuting(Literal action)	{
		synchronized(executingObligations)	{
			action = normalize(action);
			return executingObligations.contains(action);
		}
	}

	@Override
	protected final List<Literal> currentlyExecuting()	{
		synchronized(executingObligations)	{
			return executingObligations;
		}
	}


	/**
	 * Fetch mp by action, using the functor.  Extended to check for a maint. policy in
	 * a dependency or obligation contract.
	 * @param action Literal
	 * @return MaintenancePolicy
	 */
	@Override
	public MaintenancePolicy getMaintenancePolicy(Literal action){
		if(hasObligationContract(action))	{
			return getObligationContract(action).getMp();
		}
		else if(hasDependencyContract(action))	{
			return getDependencyContract(action).getMp();
		}
		else	{
			return super.getMaintenancePolicy(action);
		}
	}


	/**
	 * Takes a given literal l, and removes all annotations except for 'dependant', 
	 * 'dependent intention', 'index' and 'planLabel'.  Used to ensure consistence in 
	 * keys for obligations and depdencies
	 * @return
	 */
	protected final Literal normalize(Literal in)	{
		Literal out = null;
		synchronized(in)	{	out = in.copy();	}
		out.clearAnnots();

		//add annots...
		out.clearAnnots();
		if(!in.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT).isEmpty())	{
			out.addAnnot(in.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT).get(0));
		}
		if(!in.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT_INTENTION).isEmpty())	{
			out.addAnnot(in.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT_INTENTION).get(0));
		}
		if(!in.getAnnots(UniversalConstants.ANNOTATION_ID_IN_PLAN).isEmpty())	{
			out.addAnnot(in.getAnnots(UniversalConstants.ANNOTATION_ID_IN_PLAN).get(0));
		}
		if(!in.getAnnots(UniversalConstants.ANNOTATION_PLAN_LABEL).isEmpty())	{
			out.addAnnot(in.getAnnots(UniversalConstants.ANNOTATION_PLAN_LABEL).get(0));
		}

		logger.finest("Normalized " + in + " to " + out);
		return out;
	}

}
