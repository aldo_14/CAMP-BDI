package agent.type.arch;

import static agent.AgentUtils.formUnifierFromArgs;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Logger;

import agent.model.capability.Capability;
import agent.type.CapabilityAwareAgent;
import agent.type.ContractFormerAgent;
import agent.type.GoalScoringAgent;
import agent.type.MaintainingAgent;
import common.StatsRecorder;
import common.UniversalConstants;
import jason.architecture.AgArch;
import jason.asSemantics.ActionExec;
import jason.asSemantics.Agent;
import jason.asSemantics.Message;
import jason.asSemantics.Unifier;
import jason.asSyntax.ASSyntax;
import jason.asSyntax.ListTerm;
import jason.asSyntax.Literal;
import jason.asSyntax.Term;
/**
 * 
 * @author Alan White
 * 
 * Architecture supporting task dispatch - success retrieval as a result of dependency 
 * and/or obligation formation
 */
public class MultiagentArch extends AgArch {
	private static int DEBUG_COUNTER = -1;
	
	protected Agent me;
	protected Logger logger;
	
	
	@Override
	public void init() throws Exception {
		super.init();
		me = this.getTS().getAg();
		logger = Logger.getLogger(me.getTS().getUserAgArch().getAgName() + 
				":" + this.getClass().getSimpleName());
		logger.setLevel(UniversalConstants.ARCH_LOGGER_LEVEL);
	}
	
	
	/**
	 * Override with forced ui update (if ui!=null) at start of reasoning cycle
	 */
	@Override
	public void reasoningCycleStarting()	{
		//ui update, only if circumstances have changed...
		if((getTS().getAg()!=null) && getTS().getAg() instanceof GoalScoringAgent)	{
			GoalScoringAgent a = (GoalScoringAgent)getTS().getAg();
			if(a.getUi()!=null)	{	a.getUi().repaint();	}
		}

		super.reasoningCycleStarting();
	}
	

    public void sendMsg(Message m) throws Exception {
    	StatsRecorder.getInstance().recordSentMessage(m);
    	logger.severe(getAgName() + " Sending;" + m);
    	super.sendMsg(m);
    }

	/**
	 * Extends the default 'act' process to recognise the process of delegation and contract formation.
	 */
	@Override
	public void act(ActionExec a, List<ActionExec> feedback){
		logger.info("Action exec " + a.toString() + "\n" + me.getTS().getC());

		if(a.getIntention().isSuspended())	{
			logger.severe("Intention for action " + a.getActionTerm() + " is suspended!: return");
			return;
		}

		if(a.getIntention().isFinished())	{
			logger.severe("Intention for action " + a.getActionTerm() + " is finished!");
		}

		logger.finest("Act start - c=" + getTS().getC());
		
		DEBUG_COUNTER++;
		Literal act = a.getActionTerm();	
		if(a.getFailureMsg()!=null)	{
			logger.severe("Action " + a.getActionTerm() + " already failed?! (" + a.getFailureMsg() + ")");
		}

		/*
		 * We need to annotate every action, so we can check their impact on the parent intention and any associated
		 * dependencies 
		 */
		Literal topTrigger = ((GoalScoringAgent)getTS().getAg()).agentUtils().intentionIntoIMsQueue(
				a.getIntention()).peekLast().getTrigger().getLiteral();
			
		setActionAnnotations(
				me.getTS().getUserAgArch().getAgName(), 
				a.getIntention().getId(), 
				topTrigger, //a.getIntention().peek().getTrigger().getLiteral(), 
				act);
		logger.finer("Annotated action " + a.toString());
		
		//execution part
		//if capability aware, can check for multiagent involvement
		if(me instanceof CapabilityAwareAgent)	{
			logger.info("Using CapabilityAwareAgent path of execution... execute " + act.toString() + 
					" (intention " + a.getIntention().getId() + "\n" + a.getIntention() + "\n)");

			if(!isJointActivity(act, (CapabilityAwareAgent)me))	{
				logger.info("(ACT " + DEBUG_COUNTER + ")" + act + " does not require other agents...");
				super.act(a, feedback);
			}
			else	{
				logger.info("(ACT " + DEBUG_COUNTER + ")" + act + " requires other agents...");
				delegateAct(a, feedback);
			} //delegation part
		}
		else	{	
			super.act(a, feedback);	
		}
	}

	/**
	 * Performs the delegation operations for executing an action which requires >=1 other agents
	 * to be involved
	 * @param a
	 * @param feedback
	 */
	private void delegateAct(ActionExec a, List<ActionExec> feedback) {
		try	{
			// CAAs delegate actions which require other agents to be involved
			if(me instanceof ContractFormerAgent)	{
				ContractFormerAgent cfa = (ContractFormerAgent)me;
				//only attempt execution if we have a contract...
				if(!cfa.hasDependencyContract(a.getActionTerm()))	{
					//check cap
					if(cfa.hasCapability(a.getActionTerm().getFunctor()))	{
						//check if goal task already holds?
						Capability c = cfa.getCapability(a.getActionTerm().getFunctor());
						String[] add = c.achieves().getAddEffects();
						String[] rem = c.achieves().getDeleteEffects();
						Unifier u = formUnifierFromArgs(c.achieves().getSignature(), a.getActionTerm());
						
						//get unifier
						boolean ok = true;
						
						//check necessary states exist
						addLoop:
						for(String s: add)	{
							Literal l = ASSyntax.parseLiteral(s);
							l.apply(u);
							if(cfa.getBB().contains(l)==null)	{	ok = false;	break addLoop;}
						}
						
						//check for absence of REMOVED states
						remLoop:
						if(ok)	{
								for(String s: rem)	{
									Literal l = ASSyntax.parseLiteral(s);
									l.apply(u);
									if(cfa.getBB().contains(l)!=null)	{	ok = false;	break remLoop;}
								}	
						}
						
						if(ok)	{
							logger.severe("Goals already hold in belief base, so aborting " + a.getActionTerm() + " and return pass!");
							a.setResult(true);
							feedback.add(a);
							return;
						}
					}
					
					logger.severe("No contract for " + a.getActionTerm() + ", cannot execute");
					a.setResult(false);
					a.setFailureReason(null, "No dependency contract held by " + cfa.getId());
					
					if(cfa instanceof MaintainingAgent && ((MaintainingAgent)cfa).isReplanning())	{
						MaintainingAgent ma = ((MaintainingAgent)cfa);
						boolean outcome = ma.replanForCurrentStep(a.getIntention(), true);
						if(outcome)	{
							a.setResult(outcome);				
							logger.finer("MAA: succesfully replanned " + a);
						}
					}
					
					feedback.add(a);
					cfa.getTS().getC().addFeedbackAction(a);
				}
				else	{
					logger.finer("Have contract for " + a.getActionTerm() + ", executing");
					sendDelegationMsg(a, feedback);
				}
			}
			else if(me instanceof CapabilityAwareAgent)	{	sendDelegationMsg(a, feedback);	}
			else	{
				logger.warning("(ACT " + DEBUG_COUNTER + ")" + this.getAgName() + 
						"; Not a capability aware agent, using default impl.");
				super.act(a, feedback);
			}			
		}
		catch(Exception e){
			logger.severe("(ACT " + DEBUG_COUNTER + ") Exception in arch; " + e.toString());
			for(StackTraceElement ste: e.getStackTrace()){	logger.severe(ste.toString());	}
			if(a.getIntention().isSuspended()){
				//getTS().getC().removeIntention(a.getIntention()); //to avoid duplication
				a.getIntention().setSuspended(false); //re-enable
				//getTS().getC().resumeIntention(a.getIntention());
				wake();
			}
		}
	}

	/**
	 * Adds dependent and dependantIntention annotations to the literal part of the 
	 * given action.  If is an internalAction
	 * @param agName - agent name to append to annotations
	 * @param intentionId - intention id to append
	 * @param trigger - trigger literal; source of annotation prepend
	 * @param act - literal for action, which will have the annotations added (pass
	 * by reference)
	 */
	public void setActionAnnotations(String agName, int intentionId, Literal trigger, Literal act) {
		Logger logger  = Logger.getLogger("MultiagentArch:setActionAnnotation:" + agName);
		logger.setLevel(UniversalConstants.ARCH_LOGGER_LEVEL);
		logger.finer("Call: id = " + intentionId + ", trigger=" + trigger + ", act=" + act);
		
		try	{
			if(act.isInternalAction())	{
				logger.warning(act + " is an internal action and cannot receive annotations!");
				return;
			}
			//annotation contents; used for delegation
			String dAnnot 	= agName;
			String diAnnot	= Integer.toString(intentionId);

			//update intention stack if required...
			//Literal trigger = a.getIntention().peek().getTrigger().getLiteral();
			logger.info("(ACT " + DEBUG_COUNTER + ") Parent trigger = " + trigger + ", annots= " + trigger.forceFullLiteralImpl().getAnnots());

			//annotations for dependency info
			ListTerm trigDependantAnnots = 
					trigger.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT);
			ListTerm trigIntentionAnnots = 
					trigger.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT_INTENTION);
			ListTerm maintIntentionAnnots = 
					trigger.getAnnots(UniversalConstants.ANNOT_MAINTAINED);
			
			//remove maint attempt limited
			ListTerm maintAttAnnots = 
					trigger.getAnnots(UniversalConstants.ANNOT_MAINTAIN_ATTMPT);
			trigger.getAnnots().removeAll(maintAttAnnots);

			//check for matching size
			assert trigDependantAnnots.size()==trigIntentionAnnots.size(): 
				"Mismatched annotation sizes! " + trigger.toString() ;

			//does the trigger literal have annotations (signifying its parent origins)?
			//i.e. if the intention is stimulated by an obligation, the trigger will have associated annotations
			if((trigIntentionAnnots.size()>0) && (trigDependantAnnots.size()>0)){
				logger.info(
						"(ACT " + DEBUG_COUNTER + ") ti=" + trigIntentionAnnots + ", td=" + trigDependantAnnots);

				String 	existingDep = ((Literal)trigDependantAnnots.get(0)).getTerm(0).toString(),
						existingInt = ((Literal)trigIntentionAnnots.get(0)).getTerm(0).toString();

				//adds all entries in order
				for(int i=1; i<((Literal)trigDependantAnnots.get(0)).getTerms().size(); i++)	{
					existingDep = existingDep + "," + ((Literal)trigDependantAnnots.get(0)).getTerm(i).toString();
					existingInt = existingInt + "," + ((Literal)trigIntentionAnnots.get(0)).getTerm(i).toString();
					logger.info("Updated " + existingDep);
				}

				dAnnot = existingDep + "," + dAnnot;
				diAnnot = existingInt + "," + diAnnot;

				logger.info("(ACT " + DEBUG_COUNTER + ") Action term=" + act + ", dAnnot=" + dAnnot + ", diAnnot=" + diAnnot);
			}
			else	{
				dAnnot = agName;
				diAnnot = Integer.toString(intentionId);
				logger.finer("(ACT " + DEBUG_COUNTER + ") " +
						trigger + " Didn't have annotations (" + trigIntentionAnnots + ", " + trigDependantAnnots + 
						"), so set them as dep=" + dAnnot + " and int=" + diAnnot);
			}

			//remove any existing annots from the action to be delegated, so we can add in new, updated versions
			//otherwise we can end up with multiple annotations of the same functor but different comments
			//NB: it's quite possible, if we have repeat calls, that this may repeat addition of the annotations... otherwise we
			//don't expect this so much
			if(act.getAnnots()!=null)	{
				ListTerm depAgAnnots = act.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT);
				if(!depAgAnnots.isEmpty())	{	act.getAnnots().removeAll(depAgAnnots);	}
				
				ListTerm depIntAnnots = act.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT_INTENTION);
				if(!depIntAnnots.isEmpty())	{	act.getAnnots().removeAll(depIntAnnots);	}
			}

			//add an annotation for use in reply
			act.addAnnot(	ASSyntax.parseLiteral(
					UniversalConstants.ANNOTATION_DEPENDANT + "(" + dAnnot + ")"));
			act.addAnnot(	ASSyntax.parseLiteral(
					UniversalConstants.ANNOTATION_DEPENDANT_INTENTION+ "(" + diAnnot + ")"));
			if(maintIntentionAnnots==null & !maintIntentionAnnots.isEmpty())	{
				act.addAnnot(maintIntentionAnnots.get(0));
			}
			logger.fine("(ACT " + DEBUG_COUNTER + ") Updated task literal: " + act + ", " + getTS().getC() + "\n" + getTS().getAg().getPL());
			
		}
		catch(Exception e)	{
			logger.severe("Exception, with act=" + act.toString());
			logger.severe(e.toString());
			for(StackTraceElement s: e.getStackTrace())	{	logger.severe(s.toString());	}
		}
	}

	/**
	 * Delegates an activity with messaging; does not check for existing dependencies etc.
	 * @param a ActionExec - used to get literal
	 * @param feedback - used where we exec the action as primary, and notify secondaries...
	 * @throws Exception
	 */
	private void sendDelegationMsg(ActionExec a, List<ActionExec> feedback) throws Exception {
		logger.info("(ACT " + DEBUG_COUNTER + ") Making execution call to obligations " + a.getActionTerm());

		List<String> waitingOn = new Vector<String>();
		
		//executing agent; assumed / needs to be capability aware
		CapabilityAwareAgent cme = (CapabilityAwareAgent)me;
		String cmeId = cme.getId();
		
		//action literal
		Literal act = a.getActionTerm();
		
		//list of agents specified in the action args
		LinkedList<String> aNames = MultiagentArch.getInvolvedAgents(act, cme);

		//lead - recipient makes the action call to the environment
		//delegate to other agents; this agent is top of a 'pyramid' of actors
		//send messages and wait for feedback; suspend intention until getMessage satisfied...
		
		//check if we are named as a non-primary agent here...
		if(aNames.contains(cmeId) && !aNames.peek().equals(cmeId)){
			logger.severe("(ACT " + DEBUG_COUNTER + ")" + cmeId + 
					" is not the principal in joint action!" + a.toString());
			a.setResult(false);
			a.setFailureReason(	null, 
					cmeId + " is not the principal in joint action!" + a.toString());
			this.getTS().getC().addFeedbackAction(a);
		}
		else if(aNames.peek().equals(cmeId))	{ //actor is primary agent
			aNames.pop(); //should be cme, i.e. as seen in peek op...
			logger.finer("We (" + cmeId + ") are executing this action with other agents " + aNames);

			Message m = new Message("do", cmeId, null, act);
			logger.finer("(ACT " + DEBUG_COUNTER + ") Sending message to secondary participants:" + aNames);
			while(!aNames.isEmpty()){
				String performer = aNames.pop();
				m.setReceiver(performer);
				this.sendMsg(m);
				waitingOn.add(performer);
				logger.finest("(ACT " + DEBUG_COUNTER + ") Sent the message: " + m);
			}
			
			//now have informed the secondaries, need to suspend
			a.getIntention().setSuspended(true);
			cme.addWaitForResponse(a, waitingOn); //wait list... need to return result of this op too!
			super.act(a, feedback); //use the default behaviour to actually perform the action...
		}
		else	{
			//ilF, sender, recipient, message object
			Message m = new Message("do", cmeId, null, act);

			//send messages and suspend intention until responded to
			//this might involve sending to self...
			logger.finer("(ACT " + DEBUG_COUNTER + ") Sending message to participants:" + aNames);
			while(!aNames.isEmpty()){
				String performer = aNames.pop();
				m.setReceiver(performer);
				this.sendMsg(m);
				waitingOn.add(performer);
				logger.finest("(ACT " + DEBUG_COUNTER + ") Sent message: " + m);
			}

			logger.fine("(ACT " + DEBUG_COUNTER + ") Suspending intention for " + a);
			//suspend until obligants return
			a.getIntention().setSuspended(true);
			cme.addWaitForResponse(a, waitingOn);
			logger.finest("(ACT " + DEBUG_COUNTER + ") Done suspension intention for " + a);
		}
	}

	/**
	 * Gets a list of any agent ids specified at the start of the action arguments.
	 * @param act - action; we check the args(terms) of this for agents at the start
	 * @param a - agent; required to get a list of gets from the Infra arch tier
	 * @return LinkedList of string agent names; may be empty
	 */
	public static LinkedList<String> getInvolvedAgents(final Literal act, final Agent a) {
		final LinkedList<String> aNames = new LinkedList<String>();
		if(act.isInternalAction() || !act.isGround())	{	
			return aNames;	
		}
		final Set<String> agents = a.getTS().getUserAgArch().getArchInfraTier().getRuntimeServices().getAgentsNames();

		for (final Term t : act.getTerms()) {
			String tS = t.toString().replaceAll("\"", "");
			if (agents.contains(tS) && !aNames.contains(tS)) {
				aNames.addLast(tS);
			} else {
				return aNames;
			}
		}
		return aNames;
	}


	/**
	 * Checks whether or not an action requires the use of other agents, based on the arguments in the
	 * literal, when executed by this agent.
	 * @param action - to be executed; literal functor(term.. args)
	 * @param caa - executing agent
	 * @return true if action literal has >1 agents in args (at start) Not equal to the id of agent a
	 */
	public static boolean isJointActivity(Literal action, CapabilityAwareAgent caa){
		if(action.isInternalAction() || !action.isGround())	{	return false;	}
		/*
		 *Is a joint action if involves >1 agent; that is >1 named agents, or an agent involved that is not
		 *this one.  Note that this is not a test for delegation semantics...
		 */
		final LinkedList<String> aNames = MultiagentArch.getInvolvedAgents(action, caa);
		//boolean agentIncapableButOthersAre = !caa.hasCapability(action) && !caa.getExtCapability(action).isEmpty();
		boolean otherAgentSpecifiedInActivity = (aNames.size()==1) && !aNames.contains(caa.getId());
		boolean multipleAgentsRequired = aNames.size()>1;
		return (otherAgentSpecifiedInActivity || multipleAgentsRequired);
	}

}
