package agent.type.arch;

import java.util.logging.Logger;

import agent.type.CapabilityAwareAgent;
import agent.type.MaintainingAgent;
import common.UniversalConstants;
import jason.asSemantics.ActionExec;
import jason.asSemantics.IntendedMeans;
import jason.asSemantics.Intention;
import jason.asSyntax.Structure;
import jason.infra.centralised.CentralisedAgArch;


/**
 * 
 * @author Alan White
 * 
 * Simple customized agent architecture; listens to the results of action execution and updates 
 * the parent agent with that result, <b>on the provision</b> that the agent is an instance of 
 * GoalScoringAgent
 * @see agent.type.GoalScoringAgent
 */
public class SuccessRecorderArch extends CentralisedAgArch {
	protected Logger logger = null;

	@Override
	public void init() throws Exception {
		super.init();
	}

	private final Logger logger()	{
		if(logger == null)	{
			logger = Logger.getLogger(	getTS().getUserAgArch().getAgName() + 
					":" + getClass().getSimpleName());
			logger.setLevel(UniversalConstants.ARCH_LOGGER_LEVEL);
		}
		return logger;
	}

	public void actionExecuted(ActionExec action) {
		logger().info("Intercepted result; " + action + 
				"\nFeedback\n" + getTS().getC().getFeedbackActions());// + 
		result(action);
		super.actionExecuted(action);
	}

	/**
	 * Record the result of action
	 * @param action
	 */
	public void result(final ActionExec action) {
		final MaintainingAgent gs = (MaintainingAgent)getTS().getAg();
		logger().info("SRA: Handling result for action " + action + " pass=" + action.getResult() + "\n" + getTS().getC());
		if(action.getResult())	{	
			gs.recordSuccess(action.getActionTerm());	
			if(gs.isContinual())	{
				final Intention i = action.getIntention();
				if(!i.isFinished() && i.iterator().hasNext()) {
					final IntendedMeans top = i.peek();
					if(moreActionsToExecute(top)) {
						final boolean replanSuccess = doReplanFor(action, gs, false);
						logger.info("Replan " + (replanSuccess?"passed":"failed") + " for " + action.getActionTerm() + " of\n" + action.getIntention());
					}
				}
			}	
		}
		else					{	
			if(gs.isReplanning())	{
				final boolean replanSuccess = doReplanFor(action, gs, true);
				if(replanSuccess) { return; }
			}

			//update advertisement of capability, in case other agents do planning (FIXME do we need this?)
			if(gs instanceof CapabilityAwareAgent)	{
				logger.severe("set ec after result " + action);
				((CapabilityAwareAgent) gs).setExternalCapabilities();
			}

			gs.recordFailure(action.getActionTerm());	
			logger().info(	"SRA: Failed intention; " + action.getIntention() + 
					"\n" + getTS().getC().toString());
		}
	}

	private boolean moreActionsToExecute(final IntendedMeans top) {
		return top!=null && top.getCurrentStep()!=null && top.getCurrentStep().getBodyNext()!=null;
	}

	private boolean doReplanFor(final ActionExec action, final MaintainingAgent gs, final boolean failed) {
		final Structure at = action.getActionTerm();
		final boolean hasCapability = gs.hasCapability(at.getFunctor());
		final boolean hasDependencyContract = gs.hasDependencyContract(at);
		final boolean hasExternalCapability = !gs.getExtCapability(at.getFunctor()).isEmpty();

		logger().severe("SRA: try and replan!? " + at + "(hasCap? " + hasCapability + 
				"|| hasDepC? " + hasDependencyContract + "|| has ExtC?" + hasExternalCapability + ")");

		if(	hasCapability || hasDependencyContract || hasExternalCapability)	{
			if(gs.replanForCurrentStep(action.getIntention(), failed))	{
				action.setResult(true);				
				logger().finer("SRA: succesfully replanned, skip out recording;  " + action);
				return true;
			}
			else	{
				logger.warning("SRA: replan failure for " + action);
			}
		}
		return false;
	}
}
