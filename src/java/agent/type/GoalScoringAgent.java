package agent.type;

import jason.asSemantics.ActionExec;
import jason.asSemantics.Agent;
import jason.asSemantics.CircumstanceListener;
import jason.asSemantics.Event;
import jason.asSemantics.GoalListener;
import jason.asSemantics.IntendedMeans;
import jason.asSemantics.Intention;
import jason.asSemantics.Option;
import jason.asSyntax.Literal;
import jason.asSyntax.Plan;
import jason.asSyntax.Trigger;
import jason.bb.BeliefBase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import agent.AgentUtils;
import agent.beliefBase.WorldBeliefBase;

import common.StatsRecorder;
import common.UniversalConstants;


/**
 * 
 * @author Alan White
 *
 * An agent which records failure and success counts for repetitive goals.  We can also pause it.
 */
public abstract class GoalScoringAgent extends Agent implements GoalListener, CircumstanceListener {
	public static enum operatingSystem {Windows, Linux};
	private operatingSystem os;
	private AgentUtils util;
	public AgentUtils agentUtils()	{	return util;	}
	/**
	 * Incremented on success of a goal (denoted by literal key)
	 */
	protected Map<Literal, Integer> successScore = new HashMap<Literal, Integer>();
	
	/**
	 * Incremented upon success of a goal (denoted by literal key)
	 */
	protected Map<Literal, Integer> failScore = new HashMap<Literal, Integer>();
	
	/**
	 * Logger object used for messages
	 */
	protected Logger logger;

	/**
	 * Ui window; not defined in this class...
	 */
	protected JFrame ui = null;
	
	/**
	 * Convenience method used to get the agent id; put at lowest level of extension
	 * @return getTS().getUserAgArch().getAgName();
	 */
	public final String getId() {	return getTS().getUserAgArch().getAgName();	}

	protected final DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss:SS");
	
	private AgentHeartbeat watcher;
	
	/**
	 * Starts up the agent; simply sets up the logger level and adds self as GoalListener to the TS
	 */
	public void initAg() {
		super.initAg(); 
		util = new AgentUtils(this);
		logger = Logger.getLogger(getClass().getSimpleName() + ":" + getId());
		logger.setLevel(UniversalConstants.AGENT_LOGGER_LEVEL);
		getTS().getC().addEventListener(this);
		getTS().addGoalListener(this);
		if(System.getProperty("os.name").toLowerCase().indexOf("win") >= 0)	{	
			os = operatingSystem.Windows;	
		}
		else 	{	//assumptative bit... not testing outside of win/ubuntu linux, so use as default case
			os = operatingSystem.Linux;	
		}
		watcher = new AgentHeartbeat(this);
		watcher.start();
	}
	
	@Override
	public void stopAg() {
		super.stopAg();
		watcher.kill();
	}

	/**
	 * Returns operating system as either windows or linux
	 * @return
	 */
	public final operatingSystem getOsType()	{	return os;	}
	
	@Override
	public Option selectOption(List<Option> options)	{
		Option  o = super.selectOption(options);
		if(o == null)	{
			logger.severe("No option found!\n" + getTS().getC());
			bbDump(getBB(), Level.INFO);
		}
		return o;
	}
	
	/**
	 * Return successes 
	 */
	public Map<Literal, Integer> getSuccesses()	{	return successScore;	}
	public Map<Literal, Integer> getFailures()	{	return failScore;		}

	/**
	 * Used to avoid duplicate results; presumes actions cannot be duplicated without interim changes
	 */
	private Literal last = null;
	
	/**
	 * Return true if this is a repeat; only holds if g is also for a primitive (which can
	 * be for an obligation, i.e. where the trigger intention == action literal)
	 * @param g
	 * @return
	 */
	protected boolean repeat(Literal g)	{
		if(last == null)	{	return false;	}
		g = g.copy();
		boolean capCheck = true;
		if(this instanceof CapabilityAwareAgent)	{
			capCheck = (((CapabilityAwareAgent)this).hasCapability(g.getFunctor()) && 
					((CapabilityAwareAgent)this).getCapability(g.getFunctor()).isPrimitive());
		}
		boolean result = capCheck && last.equals(g);
		if(result)	{
			logger.warning("Repeated result " + g + "==" + last + " || " +
					"hasC=" + ((CapabilityAwareAgent)this).hasCapability(g.getFunctor()));
		}
		return result;
	}
	
	/**
	 * Records success of a goal or action
	 * @param goal Literal
	 */
	public void recordSuccess(Literal g) {
		logger.severe("RecordSuccess(" + g + ")");
		if(!g.isGround() || repeat(g))	{	return;	}//skip out unground		
		int count = 1;
		last = g.copy();
		if(successScore.containsKey(g))	{	count = successScore.get(g) + 1;	}
		if(UniversalConstants.SHOW_ALL_ACTION_RESULTS)	{
			JOptionPane.showMessageDialog(getUi(), 
					"Success:" + g, "recordSuccess:" + this.getId(), 
					JOptionPane.INFORMATION_MESSAGE);
		}
		
		successScore.put(g, new Integer(count));
	}
	
	/**
	 * Records failure of a goal or action
	 * @param goal Literal
	 */
	public void recordFailure(Literal g) {
		logger.severe("RecordFailure(" + g + ")");
		if(!g.isGround()|| repeat(g))	{	return;		}//skip out unground
		
		int count = 1;
		last = g.copy();
		if(failScore.containsKey(g))	{	count = failScore.get(g) + 1;	}
		if(UniversalConstants.SHOW_ALL_ACTION_RESULTS)	{
			JOptionPane.showMessageDialog(getUi(), 
					"Failure:" + g, "recordFailure:" + this.getId(), 
					JOptionPane.INFORMATION_MESSAGE);
		}
		
		if(logger.isLoggable(Level.FINER))	{
			String pls = "";
			for(Plan p: getPL().getPlans())	{	pls = pls + "\n" + p;	}
			logger.finer(g + " Failure - Plan Lib;" + pls);
		}

		failScore.put(g, new Integer(count));
	}
	
	/**
	 * Default behaviour; does nothing
	 */
	@Override
	public void goalStarted(Event goal) {	}

	/**
	 * Records the success of the goal, based on goal.getLiteral
	 */
	@Override
	public void goalFinished(Trigger goal) {
		Literal g = goal.getLiteral();
		if(g.getFunctor().startsWith("kqml"))	{	return;	}
		logger.info("!!!!!!!!!!!!! " +
				"Goal success - " + g.toString() + 
				" !!!!!!!!!!!!!");
		StatsRecorder.getInstance().recordGoalResult(
				getId(), g, true, !g.getAnnots(UniversalConstants.ANNOT_MAINTAINED).isEmpty());
		recordSuccess(g);
	}
	
	/**
	 * Records the failure of the goal, based on goal.getLiteral
	 * 
	 * NB: Recording is now handled by arch calling recordFailure, as this method doesn't capture
	 * the entire set of intention triggers - only the failed action and it's trigger.
	 */
	@Override
	public void goalFailed(Trigger goal) {
		Literal g = goal.getLiteral();
		if(g.getFunctor().startsWith("kqml"))	{	return;	} //don't care!
		
		String stack = "";
		for(StackTraceElement s: new Exception().getStackTrace())	{	stack = stack + "\n" + s;	}
		 
		logger.info("!!!!!!!!!!!!!(GSA): Goal failure - " + g.toString() + "!!!!!!!!!!!!!");
		
		recordFailure(g);
		
		StatsRecorder.getInstance().recordGoalResult(
				getId(), g, false, !g.getAnnots(UniversalConstants.ANNOT_MAINTAINED).isEmpty());
		
		/*
		 * Need to try and get a handle on the intention in order to generate failure info for 
		 * obligation handling...
		 * 
		 * First try the Selected Intention
		 */
		Intention in = getTS().getC().getSelectedIntention();
		if(in!=null)	{
			LinkedList<IntendedMeans> imQ = util.intentionIntoIMsQueue(in);
			logger.finer("SI = " + in + ", trigger=" + imQ.getFirst().getTrigger());
			//find trigger for goal
			if(imQ.getFirst().getTrigger().getLiteral().equals(g) && !imQ.getFirst().equals(imQ.getLast()))	{
				logger.finer("Matched " + imQ.getFirst() + " to goal failure trigger... \n" +
						"record top failure for " + imQ.getLast().getTrigger());
				recordFailure(imQ.getLast().getTrigger().getLiteral());
			}
			else	{	in = null;	}
		}
		
		//check selected events now; need to match trigger event literal with the goal literal passed in and, if we match,
		//extract the intention and record failure for the top-level literal trigger (which may be an obligation).
		if(in==null && getTS().getC().getSelectedEvent()!=null)	{
			Event e = getTS().getC().getSelectedEvent();
			if(e.getTrigger().getLiteral().equals(g) & e.getIntention()!=null)	{
				logger.finer("Event " + e + "; matches goal trigger");
				//get top intention...
				in = e.getIntention();
				Literal topImTrig = util.intentionIntoIMsQueue(in).getLast().getTrigger().getLiteral();
				if(!topImTrig.equals(goal))	{
					logger.finer("FAIL Top trigger = " + topImTrig);
					recordFailure(topImTrig);
				}
			}
		}
	}

	/**
	 * Does nothing except logger output at FINEST level
	 */
	@Override
	public void goalSuspended(Trigger goal, String reason) {
		logger.finest(reason + "; Goal suspended - " + goal.toString() + ", CIs=" + getTS().getC().getIntentions());
	}

	/**
	 * Does nothing except logger output at FINEST level
	 */
	@Override
	public void goalResumed(Trigger goal) {
		logger.finest("Goal resumed - " + goal.toString() + ", CIs=" + getTS().getC().getIntentions());
	}
	
	/*=====================================================================================================
	 *  Circumstance listener
	 *====================================================================================================*/
	@Override
	public void eventAdded(Event e)	{	logger.finer("Event added: " + e.toString());	} 

	/**
	 * Responds to the addition of an intention by attempting to form contracts for it.
	 */
	@Override
	public void intentionAdded(Intention i)	{
		if(i==null)	{	return;	}
		
		//debug stuff at finer and above levels of detail
		if(logger.isLoggable(Level.FINER))	{
			logger.finer("CL:Intention added " + i + ", there are " + 
					getTS().getC().getIntentions().size() + " current agent intentions;");
			Queue<Intention> inSet = new LinkedList<Intention>();
			inSet.addAll( this.getTS().getC().getIntentions());
			for(Intention intention:inSet)	{	logger.finer(intention.toString());	}

			if(i.isSuspended())	{	logger.finer("CL:Added Intention suspended " + i.getId());	}
			if(i.isFinished())	{	logger.finer("CL:Added Intention finished " + i.getId());	}
		}
		
		moveToNextIntentionAction(i);
	}

	@Override
	public void intentionDropped(Intention i)	{	
		if(i==null)	{	return;	}
		logger.severe("CL:Intention " + i + " dropped");
		//cancel if pending
		if(getTS().getC().getPendingIntentions()!=null)	{
			Map<String, Intention> pi = getTS().getC().getPendingIntentions();
			if(pi.values().contains(i))	{
				logger.severe("Dropped intention is in pending i's; " + i);
			}
		}
		//invalid?
		if(getTS().getC().getPendingActions()!=null)	{
			Map<Integer, ActionExec> pa = getTS().getC().getPendingActions();
			if(pa.containsKey(i.getId()))	{
				logger.severe("Dropped intention " + i.getId() + " has pending action " + pa.get(i.getId()) + ", being removed!");
				//pa.remove(i.getId());
			}
		}
	}

	@Override
	public void intentionSuspended(Intention i, String reason)	{	
		logger.finer("CL:Intention suspended " + i + "(" + reason + ")");
	}

	@Override
	public void intentionResumed(Intention i)	{	logger.finer("CL:Intention resumed " + i);	}	
	
	/**
	 * Handles moving to the next action in an intention; moved into a submethod to allow extension.
	 * @param i
	 */
	protected abstract void moveToNextIntentionAction(Intention i);
	
	/**
	 * Utility method to dump the contents of a belief base with the agent logger, each literal split onto
	 * a new line
	 */
	public void bbDump(BeliefBase bb, Level level) {
		synchronized (bb) {
			if (!logger.isLoggable(level)) {
				return;
			} 
			String belS = "";
			Iterator<Literal> beliefs = bb.iterator();
			while (beliefs.hasNext()) {
				belS = belS + "\n\t" + beliefs.next();
			}
			logger.log(level, "Beliefs;" + ((WorldBeliefBase) bb).getId() + belS);
		}
	}
	/**
	 * Used to get a UI instance for the agent; may return null under certain circumstances, i.e. no uis visible or
	 * no ui implemented
	 */
	public JFrame getUi()	{	return ui;	}


	/**
	 * Used for sending heartbeats
	 */
	protected abstract void heartbeat(String src);
}
