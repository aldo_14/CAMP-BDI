package agent.type;

import static agent.type.MessageTypes.IL_OB_FAILED;
import static agent.type.MessageTypes.IL_OB_SUCCEED;
import static agent.AgentUtils.formUnifierFromArgs;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Stack;
import java.util.Vector;
import java.util.logging.Level;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;

import agent.AgentUtils;
import agent.model.capability.Capability;
import agent.model.capability.CompositeCapability;
import agent.model.capability.CompositeCapabilityManager;
import agent.model.capability.ExternalCapability;
import agent.model.capability.ExternalCapabilityService;
import agent.model.capability.PrimitiveCapability;
import agent.model.capability.factory.PrimitiveCapabilityFactory;
import agent.model.goal.factory.TaskFactory;
import agent.type.arch.MultiagentArch;
import common.UniversalConstants;
import jason.JasonException;
import jason.asSemantics.ActionExec;
import jason.asSemantics.Agent;
import jason.asSemantics.IntendedMeans;
import jason.asSemantics.Intention;
import jason.asSemantics.Message;
import jason.asSemantics.Option;
import jason.asSemantics.Unifier;
import jason.asSyntax.ASSyntax;
import jason.asSyntax.ListTerm;
import jason.asSyntax.Literal;
import jason.asSyntax.NumberTerm;
import jason.asSyntax.Plan;
import jason.asSyntax.PlanBody;
import jason.asSyntax.PlanBodyImpl;
import jason.asSyntax.Pred;
import jason.asSyntax.Structure;
import jason.asSyntax.Term;
import jason.asSyntax.parser.ParseException;
import jason.bb.BeliefBase;
import jason.runtime.Settings;

/**
 * @author Alan White
 *</br></br>
 * Capability aware agent; has a set of defined primitive and composite capabilities.  These objects
 * are used to define detailed (agent) local knowledge of the semantics and specifics of any 
 * actions the agent may perform, including confidence estimation based on some combination of
 * past execution history and/or current environmental conditions.
 * </br></br>
 * (Implemented as an abstract class, to provide capability management functions to parent class)
 * </br></br>
 * In the latter case, it is assumed that the agent will have specific local environmental knowledge
 * that allows it to make a more accurate estimation than an external agent.
 * </br></br>
 * Extends GoalScoringAgent, applying action/goal results to update capability success results.
 */
public abstract class CapabilityAwareAgent extends GoalScoringAgent {
	/**
	 * literal for capability advertisement / update messages;</BR><code>
	 * hasCapability( holder, task sig, precondition, effect, confidence, cost)</code></br> sent for each precondition:effect pair
	 */
	public static final String HAS_CAPABILITY = "hasCapability";
	
	/**
	 * Agent task factory; used to create GoalTask objects
	 */
	private TaskFactory goalTaskFactory;

	/**
	 * Primitive capabilities - correspond to the effectors available to this action
	 */
	private Map<String, PrimitiveCapability> primitiveCapabilities = new HashMap<String, PrimitiveCapability>();
	
	/**
	 * Factory acts as a manager of the agent capabilities, allowing addition/removal of plans during operations
	 */
	protected CompositeCapabilityManager manager;

	/**
	 * If this is NOT empty, we can only receive capability info from agents listed in it
	 */
	private Collection<String> knownAgents;
	
	/**
	 * The set of capabilities which are advertised (broadcast) to other agents in the MAS; idenitified by name
	 */
	protected Collection<String> advertisedCapabilities = new Vector<String>();

	/**
	 * Defines which external capabilities this agent is interested in, and will handle advertisement messages for
	 */
	private Collection<String> recognisedCapabilities = new Vector<String>();

	/**
	 * Get a 'dummy' agent purely for use in setting a bb and testing logical formulas (singleton pattern)
	 * @return
	 */
	public static Agent getFutureAgent(final BeliefBase bb){
		final Agent future = new Agent();
		future.setBB(bb);
		return future;
	}
	
	public void initAg() {
		super.initAg();
		Settings settings = getTS().getSettings();
		
		String domain = settings.getUserParameter("domain");
		logger.fine("Domain is;" + domain);
		goalTaskFactory = TaskFactory.getInstance(domain);
		logger.finer("Created task factory? " + (goalTaskFactory!=null));
		
		knownAgents = new Vector<String>();
		//which agents can we see the capabilities of?
		if(settings.getUserParameters().containsKey("knownAgents"))	{
			String agentIds = settings.getUserParameter("knownAgents");
			if(agentIds!=null && agentIds.length()>0)	{
				for(String tName: agentIds.split(","))	{
					knownAgents.add(tName.trim());
				}
			}
		}
		logger.finest("Known agents = " + knownAgents);
		
		//what goals do we know?
		if(settings.getUserParameters().containsKey("taskKnowledge"))	{
			String capString = settings.getUserParameter("taskKnowledge");
			if(capString!=null && capString.length()>0)	{
				for(String tName: capString.split(","))	{
					recognisedCapabilities.add(tName.trim());
				}
			}
		}
		logger.finest("Recognises capabilities = " + recognisedCapabilities);
		
		//initialise PCs based on mas2j config
		primitiveCapabilities = new HashMap<String, PrimitiveCapability>();
		if(settings.getUserParameters().containsKey("capabilities"))	{
			String capString = settings.getUserParameter("capabilities");
			if(capString!=null && capString.length()>0)	{
				PrimitiveCapabilityFactory factory = PrimitiveCapabilityFactory.getFactory(domain);
				for(String tName: capString.split(","))	{
					tName = tName.trim();
					primitiveCapabilities.put(tName, factory.getCapability(tName, this));
				}
			}
		}
		logger.fine("Primitive capabilities = " + primitiveCapabilities);
		
		if(settings.getUserParameters().containsKey("advertised"))	{
			String capString = settings.getUserParameter("advertised");
			for(String name: capString.split(","))	{
				advertisedCapabilities.add(name.trim());
			}
		}		
	}

	public final TaskFactory getTaskFactory()	{	return goalTaskFactory;	}
	
	/**
	 * This is extended here to allow inference of composite capabilities after the plan library has 
	 * been loaded (in super.load).  Both capabilities are inferred here
	 */
	@Override
	public void load(String asSrc) throws JasonException {
		super.load(asSrc);
		//plan indexes
		for(Plan p: this.getPL().getPlans())	{
			if(!p.getLabel().toString().startsWith("kqml"))	{ 
				//filters out the kqml plans for comms handling
				logger.finer("Setting indexes for for actions in " + p.toASString());
				agentUtils().setPlanActionIndexAnnots(p);
				logger.finer("Set indexes;" + p.toASString());
			}
		}
		
		initCompositeCapabilities();

		logger.fine(getId() + ":Has Primitive capabilities: " + primitiveCapabilities);

		if(logger.isLoggable(Level.FINE)){
			logger.fine(getId() + ":Has Composite capabilities: ");
			for(Capability c: getCompositeCapabilities().values())	{	logger.fine(c.toString());	}
		}
		//debug display output
		if(UniversalConstants.SHOW_AGENT_UI)	{	createDebugUi();	}
		
		//all composites done at this point - now can broadcast capability info
		setExternalCapabilities();//initial
	}

	
	public void setExternalCapabilities() {
		final Collection<ExternalCapability> ecs = getEcsToAdvertise();
		ExternalCapabilityService.getInstance().registerExternalCapabilities(getId(),ecs);
		logger.fine("Completed capability setup for " + ecs);
	}
	
	/**
	 * Create this agents set of external capabilities
	 * @return
	 */
	protected Collection<ExternalCapability> getEcsToAdvertise()	{
		final Collection<ExternalCapability> set = new Vector<ExternalCapability>();
		for(final String cn : advertisedCapabilities)	{
			if(!hasCapability(cn))		{
				logger.warning("Advertising a non-existent capability? " + cn);
			}
			else	{
				Capability c = getCapability(cn);
				set.addAll(c.isPrimitive() ? getEcsForPrimitiveCapabilities(c) : getEcsForCompositeCapabilities((CompositeCapability)c));
			}//end main else
		}
		return set;
	}
	
	protected abstract Collection<ExternalCapability> getEcsForPrimitiveCapabilities(final Capability c);
	protected abstract Collection<? extends ExternalCapability> getEcsForCompositeCapabilities(final CompositeCapability cc);

	/**
	 * Calculate confidence for an unground plan; use calc(planbody) type for the ground plans
	 * @param p
	 * @param bb
	 * @return
	 */
	protected final float calculateUngroundPlanConfidence(Plan p, BeliefBase bb) {
		if(Capability.CONF_STRATEGY.equals(Capability.confidenceStrategy.min))	{
			return calculateUngroundPlanConfidenceMin(p, bb);
		}
		else if(Capability.CONF_STRATEGY.equals(Capability.confidenceStrategy.average))	{
			return calculateUngroundPlanConfidenceAvg(p, bb);
		}
		else if(Capability.CONF_STRATEGY.equals(Capability.confidenceStrategy.weightedAverage))	{
			return calculateUngroundPlanConfidenceWeightedAvg(p, bb);
		}
		else	{
			throw new UnsupportedOperationException("Not implemented handling yet!");
		}
	}
	
	/**
	 * Calculate using a weighted average based on plan length n, where early activites are higher weight
	 * @param p
	 * @param bb
	 * @return
	 */
	private float calculateUngroundPlanConfidenceWeightedAvg(Plan p, BeliefBase bb) {
		if(p==null || p.getBody()==null)	{	return 1.15f;	}
		
		//get plan length for weiughting factor
		List<PlanBody> pbs = new AgentUtils(this).planIntoBodyList(p.getBody());
		float n = pbs.size();
		PlanBody pb = p.getBody();
		if(pb.isInternalAction()) {
			logger.info("top pb " + pb + "\n is internal action");
			pb = pb.getBodyNext();
			n--;
		}
		
		float sum = 0;
		float sumWeight = 0;
		float i = 0;
		while(pb!=null)	{
			float weight = (n - i)/(++i);
			if(	pb.getBodyType().equals(PlanBodyImpl.BodyType.achieve)		||
					pb.getBodyType().equals(PlanBodyImpl.BodyType.achieveNF)	||
					pb.getBodyType().equals(PlanBodyImpl.BodyType.action)		)	{
				Literal action = (Literal)pb.getBodyTerm();

				if(hasCapability(pb))	{
					float conf = action.isGround() ? getCapability(pb).getSpecificConfidence(bb, action) : getCapability(pb).getGeneralConfidence(bb);
					logger.info(action + " conf from C = " + conf);
					sum += conf*weight;
				}
				else if(!getExtCapability(action).isEmpty())	{
					Collection<ExternalCapability> cs = getExtCapability(action);
					float localMax = -1.1f;
					//if > 1 matching; assume we'd use the higher
					for(ExternalCapability ec: cs)	{
						float ecConf = action.isGround() ? ec.getSpecificConfidence(bb, action) : ec.getGeneralConfidence(bb);
						if(ecConf > localMax)	{localMax = ecConf;	}
					}
					logger.info(action + " conf from ECs = " + localMax);
					sum += localMax*weight;
				}
				sumWeight+=weight;
			}
			pb = pb.getBodyNext();
		}
		logger.info("Returning unground confidence " + sum + "/" + sumWeight + "=" + (sum/sumWeight) + " for " + p);
		return sumWeight==0? 0 : (sum / sumWeight);
	}

	/**
	 * Calculate as an average
	 * @param p
	 * @param bb
	 * @return
	 */
	private float calculateUngroundPlanConfidenceAvg(Plan p, BeliefBase bb) {
		if(p==null || p.getBody()==null)	{	return 1.15f;	}
		PlanBody pb = p.getBody();
		float sum = 0;
		int len = 0;
		while(pb!=null)	{
			len++;
			if(	pb.getBodyType().equals(PlanBodyImpl.BodyType.achieve)		||
					pb.getBodyType().equals(PlanBodyImpl.BodyType.achieveNF)	||
					pb.getBodyType().equals(PlanBodyImpl.BodyType.action)		)	{
				Literal action = (Literal)pb.getBodyTerm();

				if(hasCapability(pb))	{
					float conf = getCapability(pb).getGeneralConfidence(bb);
					logger.info(action + " conf from C = " + conf);
					sum += conf;
				}
				else if(!getExtCapability(action).isEmpty())	{
					Collection<ExternalCapability> cs = getExtCapability(action);
					float localMax = -1.1f;
					//if > 1 matching; assume we'd use the higher
					for(ExternalCapability ec: cs)	{
						float ecConf = ec.getSpecificConfidence(bb, action);
						if(ecConf > localMax)	{localMax = ecConf;	}
					}
					logger.info(action + " conf from ECs = " + localMax);
					sum += localMax;
				}
			}
			pb = pb.getBodyNext();
		}
		logger.info("Returning unground confidence " + sum + "//" + len + "=" + (sum/len) + " for " + p);
		return sum / len;
	}

	/**
	 * General case implementation for unground plan
	 * @param p
	 * @param bb
	 * @return float; the min of general confidences
	 */
	protected float calculateUngroundPlanConfidenceMin(Plan p, BeliefBase bb)	{
		if(p==null || p.getBody()==null)	{	return 1.15f;	}
		PlanBody pb = p.getBody();
		float min = 1.1f;
		while(pb!=null)	{
			if(	pb.getBodyType().equals(PlanBodyImpl.BodyType.achieve)		||
					pb.getBodyType().equals(PlanBodyImpl.BodyType.achieveNF)	||
					pb.getBodyType().equals(PlanBodyImpl.BodyType.action)		)	{
				Literal action = (Literal)pb.getBodyTerm();

				if(hasCapability(pb))	{
					float conf = getCapability(pb).getGeneralConfidence(bb);
					logger.info(action + " conf from C = " + conf);
					if(conf < min)	{	min = conf;	}
				}
				else if(!getExtCapability(action).isEmpty())	{
					Collection<ExternalCapability> cs = getExtCapability(action);
					float localMax = -1.1f;
					//if > 1 matching; assume we'd use the higher
					for(ExternalCapability ec: cs)	{
						float ecConf = ec.getSpecificConfidence(bb, action);
						if(ecConf > localMax)	{localMax = ecConf;	}
					}
					logger.info(action + " conf from ECs = " + localMax);
					if(localMax!=-1.1f && localMax < min)	{	min = localMax;	}
				}
			}
			pb = pb.getBodyNext();
		}
		if(min == 1.1)	{	min = -1;	}
		logger.info("Returning unground confidence " + min + " for " + p);
		return min;
	}

	@Override
	public Option selectOption(List<Option> options) {
		if(options.isEmpty()) {
			logger.severe("No options found!\n" + getTS().toString());
		}
		return super.selectOption(options);
	}
	
	/**
	 * Extension of selectMessage to internally handle the removal and processing of capability messages
	 */
	@Override
	public Message selectMessage(Queue<Message> messages) {
		// make sure the selected Message is removed from 'messages'
		if(messages.isEmpty())	{	return super.selectMessage(messages);	} //quickstep
		synchronized(messages)	{
			Object[] message = messages.toArray();
			for(Object o: message){
				Message m = (Message)o;
				logger.finer("MSG: " + m);

				// handle receipt of delegation messages by scheduling and invoking a plan with feedback message sending
				//check args to determine 'role' (implicit coordination); if first arg, do the actual action, if not then
				//block until the primary agent completes.
				if(m.getIlForce().equals("do"))	{
					//don't accept 'do' messages if we have existing intentions
					if(!getTS().getC().getIntentions().isEmpty()){
						logger.warning("Contains an intention generated by self");
						rejectDoMessage(m);
						messages.remove(o);
						return selectMessage(messages); //recursion!
					}
					else if(!getWaitingForReply().isEmpty() && !isChildOfWaitingDependency((Literal)m.getPropCont()))	{
						//subcheck for existing intentions - may be redundant?
						//check the intentions we're waiting for, and see if we're exec-ing a sub-activity
						logger.warning("do " + m + "\nWaiting for other intention execution \n" + getWaitingForReply() +"\n... abort exec");
						throw new RuntimeException("SEVERE FAILURE - check sent messages were received? \nMB=" + getTS().getC().getMailBox().toString());
						/*Message reply = m.clone();
						reply.setIlForce(IL_OB_FAILED);
						try {
							getTS().getUserAgArch().sendMsg(reply);
							messages.remove(o);
						} catch (Exception e) {
							logger.severe(e.toString());
							for(StackTraceElement s:e.getStackTrace()){logger.severe(s.toString());}
						}*/
					}
					else	{
						//else - we have no current intentions, so can accept a 'do' depending on the 'wait' state
						//first agent in args
						String firstAgent = ((Literal)m.getPropCont()).getTerm(0).toString().replace("\"", "");

						//determine if we can 'accept' this delegation message, depending on existing intentions etc
						boolean canHandle = canPerformObligation((Literal)m.getPropCont());
						logger.info("CanHandle " + m.toString() + "=" + canHandle);

						if(canHandle)	{	
							handleDo(messages, o, m, firstAgent);	
						}
						else	{
							rejectDoMessage(m);	
							messages.remove(o);
						}
					}

				}//end do
				else if(	m.getIlForce().equals(IL_OB_SUCCEED) ||  
							m.getIlForce().equals(IL_OB_FAILED)	)	{
					logger.info(m.getSender() + " -> Result received : " + m.toString());
					handleResult(m);
					messages.remove(o);
				}//end handle fail/succeed messages
			}
		logger.severe("Calling super-method with " + messages);
		return super.selectMessage(messages); //use default select behaviour
		}
	}

	/**
	 * Checks if the given literal (activity) is a descendent of a current action, waiting on
	 * obligant execution
	 * @param propCont
	 * @return
	 */
	private boolean isChildOfWaitingDependency(Literal activity) {
		for(ActionExec ae:getWaitingForReply().keySet())	{
			Literal l = (Literal)ae.getActionTerm();
			if(agentUtils().isSubChildOf(activity, l))	{
				logger.severe("Activity " + activity + " is related to suspended intention " + l);
				return true;
			}
		}
		//no hits found!
		return false;
	}

	/**
	 * Handle a fail/succeed message
	 * @param m
	 */
	protected void handleResult(Message m) {
		Literal action = (Literal)m.getPropCont();
		//, seperated list of names / intentions
		List<Term> dId = 
				((Literal)action.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT).get(0)).getTerms();
		List<Term> iId = 
				((Literal)action.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT_INTENTION).get(0)).getTerms();
		logger.info("Task dId = " + dId);
		String targetDep = dId.get(dId.size()-1).toString().replaceAll("\"", "");
		List<String> actors = MultiagentArch.getInvolvedAgents(action, this);
		
		if(targetDep.equals(getId()))	{
			logger.info("Result Message for " + action + " is relevant...");
			int intentionId = Integer.parseInt(iId.get(iId.size()-1).toString().replaceAll("\"", ""));
			logger.severe("Received result: " + m.toString());
			recordResult(action, m.getIlForce().equals(IL_OB_SUCCEED), m.getSender(), intentionId);
		}
		else if(actors.contains(getId()))	{
			if(UniversalConstants.DEBUG && UniversalConstants.DEMO_MODE)	{
			JOptionPane.showMessageDialog(
					getUi(), new String[]{
						"RCV Team result;",
						"Type:" + m.getIlForce(),
						"Sndr:" + m.getSender(),
						"Rcvr:" + m.getReceiver(),
						"Body:" + m.getPropCont()
					}, getId(), JOptionPane.INFORMATION_MESSAGE);
			}
			m.setSender(getId());
			if(!m.getSender().equals(targetDep))	{
				m.setReceiver(targetDep);
				try {
					getTS().getUserAgArch().sendMsg(m);
				} catch (Exception e) {
					logger.severe(e.toString());
					for(StackTraceElement s:e.getStackTrace()){logger.severe(s.toString());}
				}
			}
			}
		else	{
			logger.info("Result Message for " + action + " is not for me!");
			logger.info(m.toString());
			//note; this is important enough an error to directly highlight, as it can screw up contract formation
			//and joint action
			JOptionPane.showMessageDialog(
					getUi(), new String[]{
						"Received a result message not intended for self;",
						"Type:" + m.getIlForce(),
						"Sndr:" + m.getSender(),
						"Rcvr:" + m.getReceiver(),
						"Body:" + m.getPropCont()
					}, getId(), JOptionPane.ERROR_MESSAGE);
		}

		/*
		 * If we are waiting on another agent to complete (i.e. obliged to wait), we check here to
		 * see if it can be removed and appropriate messaging performed 
		 * 
		 * if we are not primary actor, then don't record success/failure in capability confidence;
		 * we'll instead that conf value from the primary actors capability advertisement
		 */
		logger.info("Checking obligations stack");
		for(Literal ob:getWaitObligations())	{
			if(ob.equals(action))	{
				logger.info("Got result for obligation; " + action);
				removeWaitObligation(action);

				if(m.getIlForce().equals(IL_OB_SUCCEED))	{
					logger.info("JA succeed: " + action);
					recordSuccess(action);
				}
				else if(m.getIlForce().equals(IL_OB_FAILED))	{
					logger.info("JA fail: " + action);
					recordFailure(action);
				}
				List<Term> dependants = 
						((Literal)action.getAnnots("dependant").get(0)).getTerms();
				String dependant = 
						dependants.get(dependants.size()-1).toString().replaceAll("\"", "");
				
				//inform dependent of result(?)
				//need to check message is from the primary executory agent; i.e. first in args
				if(m.getSender().equals(ob.getTerm(0).toString().replaceAll("\"", "")))	{
					Message result = m.clone();
					result.setSender(getId());
					result.setReceiver(dependant);
					try {
						this.getTS().getUserAgArch().sendMsg(result);
					} catch (Exception e) {
						logger.severe(e.toString());
						for(StackTraceElement s: e.getStackTrace()){logger.severe(s.toString());}
					}
				}//if m.sender==dependant
			}//ob.equals(action)
		}//for all wait Obligations
	}

	/**
	 * @param messages
	 * @param o
	 * @param m
	 * @param firstAgent
	 */
	private void handleDo(Queue<Message> messages, Object o, Message m,
			String firstAgent) {
		if(firstAgent.equals(getId()))	{ //primary is me!
			if(UniversalConstants.DEMO_MODE){
				JOptionPane.showMessageDialog(
						ui, "Do obligation: " + m.getPropCont(), 
						this.getId(), JOptionPane.PLAIN_MESSAGE);
			}
			messages.remove(o);
			doObligation(m);
			logger.info("Handled 'do' message; " + m.toString());
		}
		else	{
			if(UniversalConstants.DEMO_MODE){
				JOptionPane.showMessageDialog(
						ui, "Assist obligation: " + m.getPropCont(), 
						this.getId(), JOptionPane.PLAIN_MESSAGE);
			}
			messages.remove(o);
			doAssistObligation(m);
		}
	}

	/**
	 * Rejects a 'do' message; sends the appropriate failure message to the original sender
	 * @param m
	 */
	protected void rejectDoMessage(Message m) {
		logger.info("Not handling 'do' message; " + m.toString());
		//fail!
		Message reply = m.clone();
		reply.setIlForce(IL_OB_FAILED);
		reply.setSender(this.getId());
		reply.setReceiver(m.getSender());
		try {
			this.getTS().getUserAgArch().sendMsg(reply);
		} catch (Exception e) {
			logger.severe(e.toString());
			for(StackTraceElement s:e.getStackTrace()){logger.severe(s.toString());}
		}
	}

	protected void doObligation(Message m) {
		Literal task = (Literal) m.getPropCont();
		
		logger.info("Performing obligation task " + task);
		setExecuting(task);
		
		if(hasCapability(task.getFunctor()) && getCapability(task.getFunctor()).isPrimitive())	{
			//wrap the execution call with a plan addition and invocation
			//get IID and dependentID
			String iId = 
					((Literal)task.getAnnots("dependantIntention").get(0)).getTerm(0).toString();
			iId = iId.replaceAll("\"", "");
			String dId = ((Literal)task.getAnnots("dependant").get(0)).getTerm(0).toString();
			dId = dId.replaceAll("\"", "");

			//form plan
			long t = System.currentTimeMillis();
			String uuid			= "handlePlan" + t;
			String trigger 		= "+!handleDo(I, S)";
			String context 		= "I=" + iId + " & S=\"" + dId + "\"";
			String taskString 	= task.toString(); //primitive call
			String actions 		= taskString + ";.remove_plan(" + uuid + ").";
			
			//create then add plan
			Plan p = Plan.parse("@" + uuid + " " + trigger + ":" + context + "<-" + actions);
			try {//skip capability inference part for composite caps (?)
				getPL().add(p);
				logger.info("Added plan....\nP=" + p.toASString());
			} catch (JasonException e) {
				logger.severe("ERROR adding wrapper plan; " + e.toString());
				for(StackTraceElement ste: e.getStackTrace())	{ logger.severe(ste.toString());	}
			}

			//add annotations to trigger from source
			p.getTrigger().getLiteral().addAnnots(task.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT));
			p.getTrigger().getLiteral().addAnnots(task.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT_INTENTION));
			
			//unifier...
			Unifier u = new Unifier();
			u.bind(ASSyntax.createVar("I"), ASSyntax.parseNumber(iId));
			u.bind(ASSyntax.createVar("S"), ASSyntax.createString(dId));
			p.getTrigger().getLiteral().apply(u);

			//fire off trigger for the new plan as a new event
			getTS().getC().addAchvGoal( p.getTrigger().getLiteral(), Intention.EmptyInt);
		}
		else	{
			//easier, already have a plan to invoke
			//fire trigger event for subplan
			logger.info("firing trigger for composite capability task " + task);
			getTS().getC().addAchvGoal(task, Intention.EmptyInt);
		}
	}
	
	/**
	 * Assist another agent in performing some action; equates to waiting until a failure or
	 * success message is received.  The purpose of this is to simulate the agent assisting or,
	 * specifically, being 'idle' for the action duration.
	 * @param m
	 */
	protected void doAssistObligation(Message m){
		Literal task = (Literal)m.getPropCont();
		String actorName = task.getTerm(0).toString().replace("\"", "");
		logger.fine("Waiting for result message from " + actorName + " for joint-action " + task);
		pushWaitObligation(task);
	}

	/**
	 * Show an agent-context sensitive ui display.  Default is simply an error message.  Extend
	 * with specifics - should be conditional on DEBUG being true.
	 */
	protected void createDebugUi()	{	ui  = new UiWindow();	}

	/**
	 * Initialize composite capabilities based upon the pre-existing primitive capabilities and the 
	 * current plan library. May be extended by superclass.
	 * @see agent.type.CapabilityAwareAgent#initCompositeCapabilities()
	 */
	protected Map<String, CompositeCapability> initCompositeCapabilities() {
		manager = new CompositeCapabilityManager(this);
		List<Plan> plans = pl.getPlans();
		for(Plan p: plans){
			if(!p.getLabel().toString().startsWith("kqml")){ //filter out the inbuilt kqml plans for comms
				logger.finest("CCgen> Plan label: " + 	p.getLabel().toString()	); 
				logger.finest("CCgen> Plan trigger: " + p.getTrigger()			);
				logger.finest("CCgen> Plan context: " + p.getContext()			);
				logger.finest("CCgen> Plan body: " + 	p.getBody()				);
				manager.factory(p);//add plan / CC into factories store
			}
		}
		logger.finer("Initialised cc set; " + manager.getFormedCapabilities());
		return manager.getFormedCapabilities();
	}

	public Map<String, PrimitiveCapability> getPrimitiveCapabilities() {	return primitiveCapabilities;			}
	public Map<String, CompositeCapability> getCompositeCapabilities() {	return manager.getFormedCapabilities();	}

	/**
	 * Returns the named composite or primitive capability object, or null if the named capability does not exist.
	 * Presumes one match per string id.
	 * @return CapabilityInf or null
	 */
	public Capability getCapability(String s){
		logger.finest("getCapabilityByFunctor: PC list = " + this.primitiveCapabilities.keySet());
		logger.finest("getCapabilityByFunctor: CC list= " + this.manager.getFormedCapabilities().keySet());
		//effectors check
		if(primitiveCapabilities.containsKey(s))			{	return primitiveCapabilities.get(s);			}
		else												{	logger.finest("!Not a prim capability:" + s);	}
		//plans check
		if(getCompositeCapabilities().containsKey(s))		{	return getCompositeCapabilities().get(s);		}
		else												{	logger.finest("!Not a comp	 capability:" + s);	}
		//nothing found
		return null;	
	}

	public boolean hasCapability(PlanBody pb)	{
		String f = ((Literal)pb.getBodyTerm()).getFunctor();
		if(pb.getBodyType().equals(PlanBody.BodyType.achieve) || pb.getBodyType().equals(PlanBody.BodyType.achieveNF))	{
			return getCompositeCapabilities().containsKey(f);
		}
		else	{	return primitiveCapabilities.containsKey(f);	}
	}
	
	/**
	 * Gets a capability, differentiating by the type - add goal or otherwise
	 * @param pb
	 * @return
	 */
	public Capability getCapability(PlanBody pb)	{
		String f = ((Literal)pb.getBodyTerm()).getFunctor();
		if(pb.getBodyType().equals(PlanBody.BodyType.achieve) || pb.getBodyType().equals(PlanBody.BodyType.achieveNF))	{
			return getCompositeCapabilities().get(f);
		}
		else	{	return primitiveCapabilities.get(f);	}
	}
	
	/**
	 * Check capability based on literal functor
	 * @param f - functor
	 * @return boolean if have primitive/composite capability corresponding to f
	 */
	public boolean hasCapability(String f) {
		return (primitiveCapabilities.containsKey(f) || getCompositeCapabilities().containsKey(f));
	}
	
	/**
	 * Returns true if we know an agent that can perfom the capability identified by f, and that
	 * agent is NOT ourself. 
	 * @param f - name of capability
	 * @return true if can find at least one hasCapability entry with a signature functor corresponding
	 * to f.
	 */
	public boolean knowsProviderForExtCapability(String f)	{	return !getExtCapability(f).isEmpty();	}
	
	/**
	 * Get the external capability information for a given action (literal); if ground will restrict by
	 * holder (first arg)
	 * @param l
	 * @return ExternalCapability
	 */
	public Collection<ExternalCapability> getExtCapability(Literal l)	{
		if(l.isGround() && !l.isInternalAction())	{
			LinkedList<String> involvedAgents = MultiagentArch.getInvolvedAgents(l, this);
			if(involvedAgents == null || involvedAgents.isEmpty())	{
				logger.severe("No involved agents found for  " + l + "\n" + getExtCapabilities());
				return new Vector<ExternalCapability>();
			}
			String actor = involvedAgents.getFirst();
			return getExtCapability(l.getFunctor(), actor);
		}
		return getExtCapability(l.getFunctor());
	}
	
	/**
	 * Get external capability object so we can identify provider set
	 * @param f - functor for the capability signature
	 */
	public Collection<ExternalCapability> getExtCapability(final String f)	{
		Collection<String> looking = new Vector<String>();
		looking.add(f);
		Collection<ExternalCapability> all = 
				ExternalCapabilityService.getInstance().getEcs(getId(), knownAgents, looking);
		if(logger.isLoggable(Level.FINEST))	{
			String ec = "";
			for(ExternalCapability s : all)	{
				ec = ec + "\n" + s;
				ec = ec + "\n\t" + all;
			}
			logger.finest("Returning ec set for " + f + "; " + ec);
		}
		return all;
	}
	
	public Collection<ExternalCapability> getExtCapability(final String functor, final String actor)	{
		final Vector<String> known = new Vector<>();
		if(actor!=null)		{	known.add(actor);	}
		final Vector<String> taskKnowledge = new Vector<>();
		if(functor!=null)	{	taskKnowledge.add(functor);		}
		final Collection<ExternalCapability> ecs = 
				ExternalCapabilityService.getInstance().getEcs(getId(), known, taskKnowledge);
		return ecs;
	}

	/**
	 * Attempts to find the 'best fit' external capability for the given action,
	 * executing in the given context, excluding dependency contracts
	 */
	public Capability getBestFitExtCapability(final BeliefBase context, final Literal action) {
		Collection<ExternalCapability> caps = getExtCapability(action);
		if (caps.isEmpty()) {
			logger.warning("Unable to find external capability;" + action);
			return null;
		} else if (caps.size() == 1) {
			return caps.iterator().next();
		} else {
			return getBestConfidenceEC(context, action, caps);
		}
	}

	private Capability getBestConfidenceEC(final BeliefBase context, final Literal action,
			final Collection<ExternalCapability> caps) {
		float conf = -1;
		Capability selected = null;
		for (final ExternalCapability ec : caps) {
			final float ecConf = ec.getSpecificConfidence(context, action, conf);
			if (ecConf > conf) {
				selected = ec;
				conf = ecConf;
			}
		}
		if (selected == null) {
			throw new RuntimeException("Severe EC discovery error!");
		}
		return selected;
	}

	/**
	 * Get complete external capability set
	 */
	public Collection<ExternalCapability> getExtCapabilities()	{
		Collection<ExternalCapability> all = 
				ExternalCapabilityService.getInstance().getEcs(getId(), knownAgents, recognisedCapabilities);
		
		//filter out CC capabilities if
		//1) there is a PC (external) for a cap
		//2) there is a local capability (PC or CC) for the cap
		Collection<ExternalCapability> rem = new Vector<ExternalCapability>();
		Collection<String> pcNames = new Vector<String>();
		for(ExternalCapability ec: all)	{ //need to id pc set in advance
			if(ec.isPrimitive())	{	pcNames.add(ec.achieves().getSignature().getFunctor());	}
		}
		
		for(ExternalCapability ec: all)	{
			if(!ec.isPrimitive())	{
				if(pcNames.contains(ec.achieves().getSignature().getFunctor()))	{
					logger.severe("Filtering out CC matching Prim EC: " + ec.getSignature());
					rem.add(ec);
				}
				else if(	hasCapability(ec.achieves().getSignature().getFunctor()) && 
							getCapability(ec.achieves().getSignature().getFunctor()).isPrimitive())	{
					//note; second condition is because we don't represent (replicate) local CCs in plans,
					//we assume these are either forwarding, or replicable by planning using PCs and ECs
					logger.severe("Filtering out CC EC as we have the capability: " + ec.getSignature());
					rem.add(ec);
				}
			}
			//else, can keep it
		}
		
		if(!rem.isEmpty())	{	logger.severe("Remove ECs from getSet; " + rem);	}
		all.removeAll(rem);
		
		if(logger.isLoggable(Level.FINE))	{
			String ec = "";
			for(ExternalCapability s : all)	{
				ec = ec + "\n" + s.getHolderName() + ":" + s.getSignature();
			}
			logger.fine("Returning complete ec set; " + ec);
		}
		return all;
	}
	
	/**
	 * Checks whether the preconditions for executing the specified action hold in the given belief base
	 * @param context
	 * @param action
	 * @return
	 */
	public boolean checkPreconditions(BeliefBase context, Literal action){
		String type = action.getFunctor();

		Capability c = this.getCapability(type);
		if(c==null)	{	return false;	}//no action, not executable
		else		{	return c.preconditionsHold(context, action);	}
	}


	/**
	 * Method that takes the given BB, clones it, updates the clone with the postcondition
	 * effects of a specified capability and instantiation, and returns the  - expected - post
	 * execution BB.  Intended for use in detecting causal link errors.
	 * @param bb1 - the pre-execution beliefbase
	 * @param action - literal denoting action - functor (name) and terms
	 * @return BeliefBase object - a modified bb with the relevent posteffect changes
	 * @see agent.capability.Capability.getPostEffects()
	 */
	public BeliefBase estimatePostExecutionBB(BeliefBase b, Literal action, Capability relevant){
		logger.finest("Estimating post-exec BB for " + action.toString() + "\n" + b);
		//no capability; no way to update!
		BeliefBase estBB = null;
		if(relevant == null) {
			logger.finer("No relevant capability, returning unmodified BB");
			return b; //no c, no action, no change!
		}
		// NB: simplified by removing existance checks, etc
		// We clone the bb later for similar reasons.
		else	{
			estBB =  b.clone(); //clone to avoid any pass-by-ref problems
			action = action.copy();
			Literal actsig = relevant.getSignature();
			Literal goalSig = relevant.achieves().getSignature();
			Unifier u = formUnifierFromArgs(goalSig, actsig);
			logger.finer("Formed unifier from " + goalSig + ":" + actsig + ";" + u);
			actsig.apply(u);
			u = formUnifierFromArgs(goalSig, action);
			Unifier u2 = formUnifierFromArgs(actsig, action);
			logger.finer("Formed unifier from " + goalSig + ":" + action + ";u=" + u  + "|u2=" + u2);
			String[] pe = relevant.getPostEffects();
			//iterate and add/remove based on the particular effect
			//i.e. -atJ; remove atJ, +atJ; add atJ.  Assume that explicit negation includes a removal statement
			//for the positive statement.
			for(String post: pe)	{
				try	{
					if(post.startsWith("-"))	{
						/*
						 * Search for literal in BB matching the removal lit... this is to 
						 */
						Structure removeMe = ASSyntax.parseStructure(post.substring(1));
						logger.finer(action + ": about to remove literal; " + removeMe);
						removeMe.apply(u);
						if(!removeMe.isGround())	{	removeMe.apply(u2);	}
						logger.finer(action + ": after " + u + "; " + removeMe);
						Vector<Literal> removeSet = new Vector<Literal>();
						Iterator<Literal> remIt = estBB.getCandidateBeliefs(removeMe, null);
						if(!removeMe.isGround())	{
							logger.severe("Removing unground literal " + removeMe);
						}
						while(remIt!=null && remIt.hasNext())	{
							Literal next = remIt.next().copy();
							if(next.equalsAsStructure(removeMe))	{	removeSet.add(removeMe);	}
						}
						
						/*
						 * Move removes to here in order to avoid concurrentModificationException that occurs
						 * using bb.remove(removeMe) above (as bb is the source of the iterator remIt arises from)
						 */
						for(Literal l: removeSet)	{
							if(estBB.contains(l)!=null)	{
								l = estBB.contains(l);//to handle annotation-related differences
								boolean result = estBB.remove(l);
								if(!result && (estBB.contains(l)!=null))	{	
									logger.warning("Failed to remove " + l + " post-exec");	
								}
								else	{	logger.finer("Removed " + l + " post-exec");	}
							}
						}
						
					}
					else if(post.startsWith("+"))	{
						Structure addMe = ASSyntax.parseStructure(post.substring(1));
						addMe.apply(u);
						if(!addMe.isGround())	{	addMe.apply(u2);	}
						logger.finer(action + ": Add literal; " + addMe);
						if(!addMe.isGround())	{
							logger.severe("Adding an unground literal " + addMe);
						}
						boolean result = estBB.add(addMe);	
						
						//error chec - add failed and literal not in BB
						if(!result && estBB.contains(addMe)==null)	{
							logger.warning("Failed to add " + addMe + " in BB post-exec;" +
									" contains = " + estBB.contains(addMe));
						}//end add
						else	{
							logger.finer("Added " + addMe + " to post-exec");
							
							/*
							 * Successful addition; now remove any contradictory (inverse) beliefs.  I.e if
							 * we add a busy(truck1), we need to make sure ~busy(truck1) is not present in
							 * the bb simultaneously
							 */
							Vector<Literal> removeSet = new Vector<Literal>();
							Iterator<Literal> remIt = estBB.getCandidateBeliefs(addMe, null);
							while(remIt!=null && remIt.hasNext())	{
								Literal next = remIt.next();
								if(next.equalsAsStructure(addMe) & (next.negated()!=addMe.negated()))	{
									removeSet.add(next);
								}
							}
							
							//do removes seperate from using the bb-derived iterator
							for(Literal l: removeSet)	{
								result = estBB.remove(l);
								if(!result)	{	logger.warning("Failed to remove contradictory " + addMe.negated() + " " + addMe );	}
								else		{	logger.finest("Removed contradictory " + addMe.negated() + " " + addMe );			}
							}
						}//end else !add result
					}
					else	{	logger.severe(action + ": Unknown effect type; " + post);	}
				}
				catch(ParseException e)	{
					logger.severe("Error parsing posteffect (" + post + ") for " + action + ";" + e.toString());
					for(StackTraceElement s: e.getStackTrace())	{	logger.severe(s.toString());	}
				}
			}//end for pe
		}//end else

		if(logger.isLoggable(Level.FINER)){//in case bb.toString is costly...
			String[] pe = relevant.getPostEffects();
			Vector<String> eff = new Vector<String>();
			for(String pef: pe)	{
				eff.add(pef);
			}
			logger.finer("Returning Post-exec BB for " + action.toString() + ", pe-s were " + eff + 
					" (" + relevant.getClass().getCanonicalName() + ")");
		}
		return estBB;
	}

	/**
	 * Adds a plan into the plan library, also updating the capabilities of the agent with respect to the task
	 * achieved by that plan.
	 * @param p
	 * @throws JasonException
	 */
	public void addPlan(Plan p) throws JasonException{
		manager.factory(p);
		logger.fine(	"Added plan " + p + ", capabilities now= " + 
				this.getCompositeCapabilities().keySet().toString());
		//set plan action index annotations
		agentUtils().setPlanActionIndexAnnots(p);
		getPL().add(p);
	}

	/**
	 * Removes a plan from the plan library, whilst updating the capability set.  Should be used 
	 * instead of getPL().remove(Plan)... reference by plan label
	 */
	public Plan removePlan(Pred label, Term sourceAnnotation) {
		logger.fine("Removing plan: " + label.toString());
		Plan p = getPL().get(label.toString());
		if(p!=null)	{
			manager.removal(p);
			getPL().remove(label, sourceAnnotation);
			if(getPL().get(label.toString())!=null)	{
				logger.severe("Still contains plan :" + p.toASString());
			}
		}
		else	{	logger.fine("No plan to remove!");	}
		return p;
	}
	
	/**
	 * Remove plan by label
	 */
	public Plan removePlan(String label)	{
		logger.fine("Removing plan: " + label);
		Plan p = getPL().get(label);
		if(p!=null)	{
			manager.removal(p);
			getPL().remove(label);
			if(getPL().get(label.toString())!=null)	{
				logger.severe("Still contains plan :" + p.toASString());
			}
		}
		else	{	logger.fine("No plan to remove!");	}
		return p;
	}
	
	/** Extended to update capability stats */
	@Override
	public void recordSuccess(Literal g) {
		//is this an obligation? 
		if(isExecuting(g))	{
			sendObResult(IL_OB_SUCCEED, g);
			logger.info("Completed execution " + g);
			completedExecution(g);
		}
		
		g = (Literal)g.clone();
		g.clearAnnots();//for logging
		
		boolean ja = MultiagentArch.isJointActivity(g, this);
		if(!ja && hasCapability(g.getFunctor()))	{
			logger.finer("Not JA, treat as normal and record success");
			Capability c = this.getCapability(g.getFunctor());
			if(c!=null){	c.addResult(true, g.getTerms());	}
			super.recordSuccess(g);
		}
		else if(ja && (MultiagentArch.getInvolvedAgents(g, this).get(0).equals(getId()) || hasCapability(g.getFunctor())))	{
			logger.finer("Primary actor for " + g + "; record result");
			super.recordSuccess(g);
		}
		else if(hasCapability(g.getFunctor()))	{
			logger.finer("Capable actor for " + g + "; record result");
			super.recordSuccess(g);
		}
		else	{	logger.finer("Not the actor/not capable for " + g + "; don't record result");	}
	}

	/**
	 * Extended to update capability stats
	 */
	@Override
	public void recordFailure(Literal g){
		logger.severe("Failure " + g + ", obligations=" + currentlyExecuting());
		bbDump(getBB(), Level.FINE);
		
		if(g.isGround() && !repeat(g) && hasCapability(g.getFunctor()))	{	
			if(!g.getAnnots(UniversalConstants.ANNOT_MAINTAINED).isEmpty())	{	
				logger.severe(g + " was maintained " + 
						(g.getAnnots(UniversalConstants.ANNOT_MAINTAINED)) + " before failing");	
			}
			else	{
				logger.severe(g + " was NOT maintained " + 
						(g.getAnnots(UniversalConstants.ANNOT_MAINTAINED)) + " before failing");
			}
		}
		
		if(isExecuting(g))	{
			logger.severe(g + " is a failed obligation!");
			sendObResult(IL_OB_FAILED, g);
			completedExecution(g);
		}

		g = (Literal)g.clone();
		g.clearAnnots();
		boolean ja = MultiagentArch.isJointActivity(g, this);
		
		if(!ja && hasCapability(g.getFunctor()))	{ //only record if we have corresponding cap-entry
			logger.severe("Not JA, treat failure as normal");
			Capability c = getCapability(g.getFunctor());
			if(c!=null){	c.addResult(true, g.getTerms());	}
			super.recordFailure(g);
		}
		//record if we are the first actor OR we have the capability
		else if(ja && (MultiagentArch.getInvolvedAgents(g, this).get(0).equals(getId()) || hasCapability(g.getFunctor())))	{
			logger.severe("Primary actor for " + g + "; record result");
			super.recordFailure(g);
		}
		else	{	logger.severe("Not the actor for " + g + "; don't record result");	}
	}

	/**
	 * Sends results for an obligation; replaces previous in-plan calls to broadcast a result
	 * @param result - fail or succeed string
	 * @param g - action literal
	 */
	private void sendObResult(String result, Literal g)	{
		//get dependant from annotations
		List<Term> dependantAnnotList = ((Literal)g.getAnnots("dependant").get(0)).getTerms();
		String dId = dependantAnnotList.get(dependantAnnotList.size()-1).toString();
		dId = dId.replaceAll("\"", "");
		
		//get any other actors from the arguments
		LinkedList<String> actors = MultiagentArch.getInvolvedAgents(g, this);
		
		//form send list
		actors.remove(getId());
		actors.add(dId);
					
		//form and send messages
		for(String id: actors)	{
			try {
				getTS().getUserAgArch().sendMsg(new Message(result, getId(), id, g));
				logger.finer("Sent result message to " + id + " for " + result + " - " + g);
			} catch (Exception e) {
				String sts = "";
				for(StackTraceElement s: e.getStackTrace())	{	sts = sts + "\n" + s;	}
				logger.severe("ERROR sending action result message!; " + e.toString() + sts);
			}
		}
	}

	/*==========================================================================================
	 * 
	 *  Methods and objects related to performing multiagent/delegated activities.  These relate
	 *  to handling delegation to obligants
	 *
	 *==========================================================================================*/	
	/**
	 * Stores suspended intentions and waiting actions
	 */
	private Map<Integer, ActionExec> waitingList = new HashMap<Integer, ActionExec>();

	/**
	 * Stores the list of agents to return a result for the key ActionExec
	 */
	private Map<ActionExec, List<String>> waitingForReply = new HashMap<ActionExec, List<String>>();

	/**
	 * Stores the result so far for the given actionExec - once falsified, remains so
	 */
	private Map<ActionExec, Boolean> waitResults = new HashMap<ActionExec, Boolean>();

	/**
	 * Stores the failure message for the key actionExec, should be an empty string if no obligants
	 * have returned a 'failure' message
	 */
	protected Map<ActionExec, String> failMsg = new HashMap<ActionExec, String>();

	/**
	 * Record a result message and determine if the associated intention can be unsuspended
	 * @param action
	 * @param pass
	 */
	protected void recordResult(Literal action, boolean pass, String source, int intId)	{
		ActionExec ae = getWaitingList().get(intId);
		
		if(ae==null)	{
			logger.severe(	"No ae identified for result " + action + " " + pass + 
							" from " + source + "(int " + intId + ")");
			return;
		}
		
		logger.info("Handling result for action " + action + ", WL=" + getWaitingList() + " pass = " + pass);

		assert (ae!=null): "Couldn't find waiting action for " + intId;
		assert (ae.getActionTerm().equals(action)): "aeAction " + ae.getActionTerm() + "does not match " + action;
		assert (getWaitingForReply().containsKey(ae)):	"ActionExec does not have a waitList! " + ae;

		List<String> waitList = getWaitingForReply().get(ae);

		assert waitList.contains(source): "Got result from " + source + " - not in " + waitList;

		if(waitList!=null)	{	waitList.remove(source);	}
		
		//msg; used for human readable information
		if(!pass)	{
			String failString = " (Failed - " + source + ") ";
			if(failMsg.containsKey(ae))	{	failString = failMsg.get(ae) + failString;	}
			failMsg.put(ae, failString);
		}
		else	{
			Intention i = ae.getIntention();
			IntendedMeans top = agentUtils().intentionIntoIMsQueue(i).getLast();
			Literal trigger = top.getTrigger().getLiteral();
			//reduces the maint count to 1, if set...
			//counter-op on the thrashing guard value
			if(	trigger.getAnnots(UniversalConstants.ANNOT_MAINTAINED)!=null && 
				!trigger.getAnnots(UniversalConstants.ANNOT_MAINTAINED).isEmpty())	{
				LinkedList<IntendedMeans> q = agentUtils().intentionIntoIMsQueue(i);
				for(IntendedMeans im: q)	{
					Literal tL = im.getTrigger().getLiteral();
					ListTerm annots = tL.getAnnots(UniversalConstants.ANNOT_MAINTAINED);
					if(!annots.isEmpty())	{
						double totalMtCount = ((NumberTerm)((Literal)annots.get(0)).getTerm(1)).solve();
						tL.getAnnots().removeAll(annots); //remove old
						tL.addAnnot(ASSyntax.createLiteral(
								UniversalConstants.ANNOT_MAINTAINED, 
								ASSyntax.createNumber(0), 
								ASSyntax.createNumber(totalMtCount)));
						im.getTrigger().setLiteral(tL);
					}	
					logger.info("Updated maintCount trigger " + tL);
				}
				logger.finer("Updated plan-trigger annots \n" + i.toString());
			}
		}

		//add result info
		if(getWaitResults().containsKey(ae))		{	pass = pass && getWaitResults().get(ae);	}
		getWaitResults().put(ae, pass);
		logger.info("Wait results:" + getWaitResults().toString());

		if(waitList.isEmpty()){
			logger.info("Waitlist empty, re-enabling intention");
			getWaitingForReply().remove(ae);
			getWaitingList().remove(intId);
			
			//record result
			if(pass)	{	completedDependency(ae);} //was recordSuccess
			else		{	failedDependency(ae);	}
			
			unsuspendPendingAction(ae);
		}
		else	{
			logger.info("Waitlist reduced; " + waitList);
			getWaitingForReply().put(ae, waitList);
		}
	}

	
	/**
	 * Handles any tidy-up for dependencies
	 * @param ae
	 */
	protected abstract void completedDependency(ActionExec ae);
	
	/**
	 * Allows override of failure handling.  We also 'fail' the ims between the top and bottom,
	 * as these don't seem to be properly handled otherwise.
	 * @param ae
	 */
	protected void failedDependency(ActionExec ae)	{
		recordFailure(ae.getActionTerm());
	}

	/**
	 * Resume the pending action in response to receipt of an obligation result
	 * @param action  - actionExec with result field set
	 */
	protected void unsuspendPendingAction(ActionExec action){
		logger.fine("Unsuspending " + action);

		//set action result before adding to feedback...
		action.setResult(getWaitResults().remove(action));

		logger.severe(action.getActionTerm() + " result1 " + action.getResult());
		
		//append info msg if failed and have an entry...
		if(!action.getResult() && failMsg.containsKey(action)){
			action.setFailureReason(null, failMsg.remove(action));
		}
		logger.severe(action.getActionTerm() + " result2 " + action.getResult());
		
		action.getIntention().setSuspended(false);

		forLoop:
		for(Intention i: getTS().getC().getIntentions())	{
			if(i.getId() == action.getIntention().getId())	{
				i.setSuspended(false);
				break forLoop;
			}
		}
		
		getTS().getC().addFeedbackAction(action); //allow next reasoning cycle to resume intention from point of failure
		IntendedMeans im = action.getIntention().peek();
		if(im==null)	{	logger.severe("Null im!" + action + "\n" + getTS().getC());	}
		else if(im != null && im.getCurrentStep() == null)	{	
			action.getIntention().pop(); 
			logger.severe(	"Unsuspend pending action; Null current step; " + action + 
							", im=" + im + "\n" + getTS().getC());	
			//add a failure event for the parent?
		}			
		else	{
			logger.info(	"Unsuspend pending action; Insert as next step; " + im.getCurrentStep().getBodyNext());
			im.insertAsNextStep(im.getCurrentStep().getBodyNext());
		}
		logger.finer("End TS.C=" + this.getTS().getC());
	}

	/**
	 * Called when we have suspended the action-intention until the passed in agents have responded with 
	 * success or failure messages
	 * @param action
	 * @param waitingOn
	 */
	public void addWaitForResponse(ActionExec action, List<String> waitingOn) {
		synchronized(getWaitingForReply())	{
			//response will be; success/fail message, with action literal, and annotations
			//to indicate the intention id and dependant
			logger.info("Waiting for " + action.getActionTerm() + " responses from " + waitingOn);
			getWaitingList().put(action.getIntention().getId(), action);
			getWaitingForReply().put(action, waitingOn);
			failMsg.put(action, "");
		}
	}

	/*==========================================================================================
	 *
	 * Methods which are used by obligants acting as a secondary; these obligants will 'block' 
	 * any further intentions until the principle actor completes, in order to provide implict
	 * co-ordination.  However, if the agent recieves an intention request (a 'do') which is 
	 * a 'child' of the current blocking action (i.e. where the principal makes an additional
	 * activity request when executing a handling plan), then it will accept this.
	 * 
	 * i.e. if A and B handle test(A, B)
	 * and A has the plan do1(A); do2(B); do3(C)
	 * Then B will not accept any 'do' requests EXCEPT do2(B), as this is from the parent
	 *
	 *==========================================================================================*/

	/**
	 * Stack used to hold obligation tasks in the order received.  An intention can be accepted if
	 * and only if its dependency annotations indicate it is generated through the process of meeting
	 * the <i>top</i> intention literal held in this stack.
	 * 
	 * This should NOT be accessed by name; instead use the synchronized methods to get clones, add
	 * entries, remove entries etc 
	 */
	private Stack<Literal> waitOns = new Stack<Literal>();

	/** Returns true if we have an obligation to wait on the result of one or more action*/
	protected boolean hasWaitObligations()			{	
		synchronized(getWaitOns())	{	return !getWaitOns().isEmpty();		}
	}

	/** Used to synch access to secondaryObs stack*/
	protected void pushWaitObligation(Literal newOb)	{	
		synchronized(getWaitOns())	{	getWaitOns().push(newOb);	}
	}

	/**Synchs access to 'remove'*/
	protected void removeWaitObligation(Literal ob)	{	
		synchronized(getWaitOns())	{	getWaitOns().remove(ob);	}
	}

	/**
	 * Get a <b>cloned</b> copy of the wait obligations for safe iteration
	 */
	@SuppressWarnings("unchecked")
	protected Stack<Literal> getWaitObligations()	{	
		synchronized(getWaitOns())	{	return (Stack<Literal>) getWaitOns().clone();	}
	}

	/**
	 * Checks if the given action (task) can be accepted and <b>immediately</b> executed as an intention, 
	 * depending on whether or not we are a) executing an action already or b) waiting on another action 
	 * to be completed by a teammate
	 * @param task
	 * @return boolean - if true, perform this task!
	 */
	protected boolean canPerformObligation(Literal task)	{
		//if we don't have any secondary obligations we can accept, on the proviso
		//we aren't executing any intentions as pricipal obligants
		if(!getWaitingList().isEmpty() && !isChildOfWaitingDependency(task))	{	
			logger.info("Already executing actions and waiting on feedbacks: " + getWaitingList().toString());
			return false;	
		}
		else	{
			//no wait obligations, no executing actions - agent is 'free'
			if(!hasWaitObligations())	{	//repetition?
				logger.info("We have no wait obligations, so can participate in execution of " + task);
				return true;	
			}
			//check if this obligation 'fits' as a child of the intention we are waiting upon
			else	{
				//we can just do a 'toString' based comparison of dependant and intention annotations...
				for(Literal ob: getWaitObligations())	{
					if(	agentUtils().isSubChildOf(task, ob) || agentUtils().sharedParent(task, ob) || 
						agentUtils().isInSamePlan(task, ob)) 	{	
						return true;	
					}
				}
				logger.info("Actions in the stack 'block' acceptance; " + getWaitObligations());
				return false;
			}
		}
	}	

	protected abstract void setExecuting(Literal action);
	
	protected abstract void completedExecution(Literal action);

	protected abstract boolean isExecuting(Literal action);
	
	protected abstract List<Literal> currentlyExecuting();

	public Map<Integer, ActionExec> getWaitingList() 			{	
		synchronized(waitingList){
			return waitingList;
		}		
	}
	
	/*==========================================================================================
	 *
	 * UI related stuff...
	 *
	 *==========================================================================================*/
	public Map<ActionExec, Boolean> getWaitResults() 			{	return waitResults;		}
	public Map<ActionExec, List<String>> getWaitingForReply() 	{	return waitingForReply;	}
	public Stack<Literal> getWaitOns() 							{	return waitOns;			}

	/**
	 * 
	 * @author Alan White
	 * 
	 * Simple internal ui class to show waitList and intention state
	 */
	class UiWindow extends JFrame implements WindowListener	{
		private static final long serialVersionUID = -2286416473274345000L;
		/**
		 * Create new window...
		 */
		protected UiWindow(){
			super();
			this.addWindowListener(this);
			this.setTitle(getId());
			this.setSize(500, 300);
			this.setContentPane(doContentPane());
			this.setVisible(true);
			//setup constant refresh...
			new Thread(new Runnable(){
				@Override
				public void run() {
					while(isVisible())	{
						try {	Thread.sleep(1000);} //ms
						catch (InterruptedException e) 	{
							logger.severe(e.toString());
							for(StackTraceElement s: e.getStackTrace()){logger.severe(s.toString());}
						}
						repaint();
					}
				} //run
			}).start();
		}

		/**
		 * Builds the content pane ready for display
		 * @return
		 */
		private Container doContentPane() {
			try	{
				Container display = new Container();
				display.setSize(this.getContentPane().getSize());

				//current intention
				JLabel title1 = new JLabel("Current intentions");
				String[] colNamesI = new String[]	{
						"ID", "Current task", "Suspended"
				};
				LinkedList<Intention> is = new LinkedList<Intention>();
				is.addAll(getTS().getC().getIntentions());
				Object[][] dataI = new Object[is.size()][colNamesI.length];
				for(int i=0; i<is.size(); i++){
					Intention curr = is.get(i);
					if(curr!=null)	{
						dataI[i][0] = Integer.toString(curr.getId());
						if(curr.peek()!=null)	{	dataI[i][1] = curr.peek().getCurrentStep().getBodyTerm();	}	
						else					{ 	dataI[i][1] = "N/A";										}
						dataI[i][2] = Boolean.toString(curr.isSuspended());
					}
					else	{
						dataI[i][0] = "Null id";
						dataI[i][1] = "Null term";
						dataI[i][2] = "--";
					}
					
				}

				//table
				JTable tableI = new JTable(dataI, colNamesI);
				JScrollPane intention = new JScrollPane(tableI);
				tableI.setFillsViewportHeight(true);

				//primary role; waits
				JLabel title2 = new JLabel("Dependencies on others");
				String[] colNamesD = new String[]	{
						"ID", "Task Literal", "Waiting for", "Result"
				};
				Object[][] dataD = new Object[getWaitingList().keySet().size()][colNamesD.length];
				int i = 0;
				for(Integer id: getWaitingList().keySet()){
					ActionExec ae = getWaitingList().get(id);
					dataD[i][0] = id.toString();
					dataD[i][1] = ae.getActionTerm().toString();
					dataD[i][2] = getWaitingForReply().get(ae).toString();
					if(getWaitResults().containsKey(id))	{
						dataD[i][3] = getWaitResults().get(ae).toString();
					}
					else	{
						dataD[i][3] = "-";
					}
					i++;
				}

				JTable tableD = new JTable(dataD, colNamesD);
				JScrollPane delegated = new JScrollPane(tableD);
				tableD.setFillsViewportHeight(true);

				//secondary role; waiting on returns
				JLabel title3 = new JLabel("Obligations waiting for lead agent to complete");
				title3.setPreferredSize(new Dimension(getSize().width, 50));

				String waitString = "";
				for(Literal l:getWaitOns()){
					waitString = waitString + l.toString() + "\n";
				}
				JTextArea waitForPrimary = new JTextArea(waitString);

				JScrollPane wait = new JScrollPane(waitForPrimary);

				title1.setPreferredSize		(new Dimension(getSize().width, 20));
				intention.setPreferredSize	(new Dimension(getSize().width, 120));
				title2.setPreferredSize		(new Dimension(getSize().width, 20));
				delegated.setPreferredSize	(new Dimension(getSize().width, 120));
				title3.setPreferredSize		(new Dimension(getSize().width, 20));
				wait.setPreferredSize		(new Dimension(getSize().width, 100));

				display.setLayout(new BoxLayout(display, BoxLayout.Y_AXIS));
				display.add(title1);
				display.add(intention);
				display.add(title2);
				display.add(delegated);
				display.add(title3);
				display.add(wait);
				return display;
			}
			catch(Exception e){
				logger.severe(e.toString());
				for(StackTraceElement ste: e.getStackTrace())	{	logger.severe(ste.toString());	}
				return new JLabel(e.toString());
			}
		}

		public void paint(Graphics g)	{
			this.setContentPane(doContentPane());
			super.paint(g);
		}

		@Override
		public void windowActivated(WindowEvent arg0) {}
		@Override
		public void windowClosed(WindowEvent arg0) {
			this.setVisible(false);
			this.dispose();
		}
		@Override
		public void windowClosing(WindowEvent arg0) {}
		@Override
		public void windowDeactivated(WindowEvent arg0) {}
		@Override
		public void windowDeiconified(WindowEvent arg0) {}
		@Override
		public void windowIconified(WindowEvent arg0) {}
		@Override
		public void windowOpened(WindowEvent arg0) {}

	}
}
