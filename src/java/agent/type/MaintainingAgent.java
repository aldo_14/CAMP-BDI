package agent.type;

import static agent.AgentUtils.formUnifierFromArgs;
import static agent.type.MessageTypes.IL_DEP_MAINTAINED;
import static agent.type.MessageTypes.IL_OB_MAINTAINED;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Vector;
import java.util.logging.Level;

import javax.swing.JOptionPane;

import agent.AgentUtils;
import agent.maintenance.MaintenancePolicy;
import agent.maintenance.MaintenanceTask;
import agent.maintenance.MaintenanceTask.Type;
import agent.model.capability.Capability;
import agent.model.capability.CompositeCapability;
import agent.model.capability.ConfidencePrecondition;
import agent.model.capability.ExternalCapability;
import agent.model.capability.StubForComposite;
import agent.model.dependency.Contract;
import agent.model.goal.GoalTask;
import agent.planning.PlanResult;
import agent.planning.PlannerInf;
import agent.planning.graphplan.AbsGPModule;
import agent.planning.graphplan.LGP.Mode;
import agent.type.arch.MultiagentArch;
import common.Analytics;
import common.Analytics.AnalyticsOperations;
import common.StatsRecorder;
import common.UniversalConstants;
import jason.asSemantics.ActionExec;
import jason.asSemantics.IntendedMeans;
import jason.asSemantics.Intention;
import jason.asSemantics.Message;
import jason.asSemantics.Option;
import jason.asSemantics.Unifier;
import jason.asSyntax.ASSyntax;
import jason.asSyntax.ListTerm;
import jason.asSyntax.ListTermImpl;
import jason.asSyntax.Literal;
import jason.asSyntax.LiteralImpl;
import jason.asSyntax.LogExpr;
import jason.asSyntax.LogicalFormula;
import jason.asSyntax.NumberTerm;
import jason.asSyntax.Plan;
import jason.asSyntax.PlanBody;
import jason.asSyntax.PlanBody.BodyType;
import jason.asSyntax.Term;
import jason.asSyntax.Trigger;
import jason.asSyntax.Trigger.TEOperator;
import jason.asSyntax.Trigger.TEType;
import jason.asSyntax.parser.ParseException;
import jason.bb.BeliefBase;

public abstract class MaintainingAgent extends ContractFormerAgent {
	private static final int MAINTAIN_CAP = 3;
	private static final int PLAN_CAP = 3;
	private static final int TOTAL_MAINT_CAP = 30;
	public static final int REPLAN_CAP = 18;
	private static final int REUSE_DEPTH_LIMIT = 7;// 7
	private static final int MAX_DEPTH_LIMIT = 12;// 12;
	private static final Term MAINT_CAP_ANNOT = ASSyntax.createString("MaintenanceCapExceeded");
	private static final int DEFAULT_LOOKAHEAD = 4;
	private int pmMaxCost = Integer.MAX_VALUE;
	private int emMaxCost = Integer.MAX_VALUE;
	private boolean maintain = false;
	private boolean replan = false;
	private boolean confConstrained = false;
	private boolean continuousPlanning = false, continuousMaintenance = false;

	/**
	 * How many plan steps in advance to check - defaults to Integer.MAX_VALUE
	 * (check all steps), if not specified in args (using "lookahead" param)
	 * 
	 * Reducing this may improve performance at selectIntention stage, by
	 * reducing the bb cloning and logexpr checking required, but will reduce
	 * the (potential) efficacy of maintenance
	 */
	private int maintainLookahead;

	/**
	 * List of literals - intention triggers - currently being maintained
	 */
	private List<Literal> currentlyMaintaining = new Vector<Literal>();

	/**
	 * If true, will schedule plans to restore PMs, if the only plan is below
	 * maintenance threshold and is the next executing activity
	 */
	private boolean allowLowConfPlansForUnhandledPMs = true;

	private PlannerInf planner;

	/**
	 * Extends initAg to setup the planner.
	 */
	@Override
	public void initAg() {
		super.initAg(); // initializes capabilities...

		if (getTS().getSettings().getUserParameters().containsKey("doMaintain")) {
			maintain = Boolean.parseBoolean(getTS().getSettings().getUserParameter("doMaintain"));
		}

		sendUpdateMessages = maintain;

		if (getTS().getSettings().getUserParameters().containsKey("doReplan")) {
			replan = Boolean.parseBoolean(getTS().getSettings().getUserParameter("doReplan"));
		}

		if (getTS().getSettings().getUserParameters().containsKey("continuous")) {
			continuousPlanning = Boolean.parseBoolean(getTS().getSettings().getUserParameter("continuous"));
		}

		if (getTS().getSettings().getUserParameters().containsKey("continuousMaintenance")) {
			continuousMaintenance = Boolean
					.parseBoolean(getTS().getSettings().getUserParameter("continuousMaintenance"));
		}

		if (getTS().getSettings().getUserParameters().containsKey("lookahead")) {
			maintainLookahead = Integer.parseInt(getTS().getSettings().getUserParameter("lookahead"));
			intentionConfidenceLookahead = Integer.parseInt(getTS().getSettings().getUserParameter("lookahead"));
		} else {
			maintainLookahead = DEFAULT_LOOKAHEAD;
			intentionConfidenceLookahead = maintainLookahead;
		}

		if (getTS().getSettings().getUserParameters().containsKey("confConstraint")) {
			confConstrained = Boolean.parseBoolean(getTS().getSettings().getUserParameter("confConstraint"));
		}

		if (getTS().getSettings().getUserParameters().containsKey("pmMaxCost")) {
			pmMaxCost = Integer.parseInt(getTS().getSettings().getUserParameter("pmMaxCost"));
		}

		if (getTS().getSettings().getUserParameters().containsKey("allowConfRelaxationForPMTs")) {
			allowLowConfPlansForUnhandledPMs = Boolean
					.parseBoolean(getTS().getSettings().getUserParameter("allowConfRelaxationForPMTs"));
		}

		if (maintain) {
			logger.info(this.getId() + "; Maintenance enabled, planning utils created");
		} else {
			logger.warning(this.getId() + "; Maintenance disabled");
		}

		final String pModeParameter = getTS().getSettings().getUserParameter("planningMode");
		planner = AbsGPModule.factory(this, pModeParameter != null ? Mode.getMode(pModeParameter) : Mode.speed);
	}

	public void setMaintaining(final boolean m) {
		maintain = m;
	}

	public boolean isMaintaining() {
		return maintain;
	}

	public void setReplanning(boolean r) {
		replan = r;
	}

	public boolean isReplanning() {
		return replan;
	}

	public boolean isContinual() {
		return continuousPlanning;
	}

	@Override
	public boolean isContinualMaintenance() {
		return continuousMaintenance;
	}

	public boolean planConfConst() {
		return confConstrained;
	}

	public void setConfConst(boolean r) {
		confConstrained = r;
	}

	@Override
	public Option selectOption(List<Option> options) {
		if (options.isEmpty()) {
			StatsRecorder.getInstance()
					.recordString(dateFormat.format(new Date()) + ":" + getId() + " has no options to select from!");
		}
		Option o = super.selectOption(options);
		if (o == null) {
			StatsRecorder.getInstance()
					.recordString(dateFormat.format(new Date()) + ":" + getId() + " selected no option!");
		}
		return o;
	}

	@Override
	public Intention selectIntention(Queue<Intention> is) {
		if (is.isEmpty() && !getObligations().isEmpty()) {
			for (final Literal obligation : getObligations()) {
				logger.severe("Maintaining obligation - " + obligation);
				Intention obI;
				if ((obI = getTS().getC().getPendingIntentions().get(obligation.toString())) != null) {
					logger.severe("Maintaining obligation intention\n" + obI.toString());
					maintain(obI, true);
				}
			}
		}

		return super.selectIntention(is);
	}

	/**
	 * Used for tracking post-maintenance wait periods... i.e. if we receive an
	 * update for obligation <i>key</i>, send notifications to all associated
	 * set members then remove. i.e. on milhq <secure("j1"):
	 * <moveTo(apc1,j1),secure(apc1,j1)>>
	 */
	private Map<Literal, Collection<Literal>> depWaits = new HashMap<Literal, Collection<Literal>>();

	/**
	 * Special case handling for dependency confidence update messages
	 * 
	 * We want to prevent erroneously maintaining an intention containing a
	 * dependency, <i>if</i> the obligation has still to attempt local
	 * modification. As such, we handle all confidence update messages (etc) to
	 * ensure confidence and contract are updated but <i>only</i> try and
	 * maintain if we receive a "obligationMaintained" update from the obligant.
	 */
	@Override
	public Message selectMessage(Queue<Message> messages) {
		synchronized (messages) {
			buf(ts.getUserAgArch().perceive(), false, "SelectMessage for \n" + messages);
			Map<Literal, Message> senderOblMap = new HashMap<Literal, Message>();

			// get maintenance task intentions here
			// do not maintain until super.selectMessage call has ran, as
			// this updates contract info...
			Queue<Literal> handleLater = new LinkedList<Literal>();
			Queue<Literal> depResponses = new LinkedList<Literal>();
			for (Message m : messages) {
				if (m.getIlForce().equals(IL_OB_MAINTAINED)) {
					logger.info("Received confirmation of maintenance by obligant \n" + m);
					Contract c = (Contract) m.getPropCont();
					if (hasDependencyContract(c.getActivity())) {
						setDependency(c.getActivity(), c);
						handleLater.add(c.getActivity());
						senderOblMap.put(c.getActivity(), m.clone());
					} else {
						logger.warning("We do not hold this dependency any longer...\n" + c.getActivity() + " !in "
								+ getDependencies());
						cancelDependency(c.getActivity(), false);
					}
				} else if (m.getIlForce().equals(IL_DEP_MAINTAINED)) {
					logger.info("Received confirmation of maintenance by dependant \n" + m);
					depResponses = handleDepMaintainedMessageReciept(depResponses, m);
				}
			}

			// conf updates
			final Message rt = super.selectMessage(messages);

			// check which dependencies are being updated and whether we need to
			// exec maintenance for them
			final Vector<Literal> toHandle = new Vector<>();
			final Vector<Integer> maintList = new Vector<>();

			// maintain if we don't have any candidate intentions
			if (getTS().getC().getIntentions().isEmpty()) {
				for (final Literal l : handleLater) {
					// remove duplicates for the same intention
					if (hasDependencyContract(l)) {
						int intentionId = intentionForConfUpdate(l).getId();
						if (!maintList.contains(intentionId)) {
							toHandle.add(l);
							maintList.add(intentionId);
						} else {
							logger.info("Duplicate maint call for intention id " + intentionId + " for " + l);
						}
					}
				}

				for (Literal l : toHandle) {
					handleUpdateToDependency(l);
				}
			}
			/*
			 * Check if we can resume our execution wrt dependencies
			 */
			unsupendWaitingDependantIntentions(depResponses);
			return rt;
		}
	}

	private void unsupendWaitingDependantIntentions(final Queue<Literal> dependenciesWhichHaveBeenMaintained) {
		for (final Literal l : dependenciesWhichHaveBeenMaintained) {
			if (hasObligationContract(l) && getTS().getC().getPendingIntentions().containsKey(l.toString())) {
				logger.fine("Dependency maint received; Resume pending intention for " + l + "\n" + ts.getC());
				StatsRecorder.getInstance().recordString(dateFormat.format(new Date()) + ":Resuming int for " + l);
				slipInPending(l);
			}

			// check for associated
			if (depWaits.containsKey(l)) {
				Collection<Literal> waitingDeps = depWaits.get(l);
				logger.fine("Dependency responses; Can also update dependencies for " + l + "; " + waitingDeps);
				for (final Literal wl : waitingDeps) {
					if (hasDependencyContract(wl)) {
						sendDepMaintDoneMsg(wl);
					}
				}
				depWaits.remove(l);
			}
		}
	}

	private Queue<Literal> handleDepMaintainedMessageReciept(final Queue<Literal> depResponses, final Message m) {
		final Literal dep = (Literal) m.getPropCont();
		logger.warning("Dependent " + m.getSender() + " completed maint for " + dep);
		if (ts.getC().getPendingIntentions().containsKey(dep.toString())) {
			depResponses.add(dep);
		}
		return depResponses;
	}

	private void handleUpdateToDependency(final Literal l) {
		if (!hasDependencyContract(l)) {
			return;
		}
		final Contract c = getDependencyContract(l);
		float conf = c.getExternalCapability().getSpecificConfidence(bb, l);
		final float threshold = c.getMp().maintenanceAcceptanceThreshold();
		final Intention i = intentionForConfUpdate(c.getActivity());
		final Literal trigger = agentUtils().intentionIntoIMsQueue(i).getLast().getTrigger().getLiteral();

		if ((conf < threshold) && (i != null) && !intentionsEqual(lastMaint, i)) {
			logger.severe("maintain call for intention (selectMessage)" + "\n\t" + i.peek() + "\nLF=" + "\n\t"
					+ lastMaint.peek());
			StatsRecorder.getInstance().recordString(
					dateFormat.format(new Date()) + ":" + getId() + " start maintaining Ob intention " + i.getId());
			boolean result = maintain(i, true);
			StatsRecorder.getInstance().recordString(
					dateFormat.format(new Date()) + ":" + getId() + " finish maintaining Ob intention " + i.getId());
			lastMaint = i.clone();

			if (result) {
				StatsRecorder.getInstance().recordString(dateFormat.format(new Date()) + ":" + getId()
						+ " modified intention due to ob change " + i.getId());

				logger.severe("Next activit mt passed; Clearing " + contractRejectImmediatelyPriorToExec);
				contractRejectImmediatelyPriorToExec.clear();
			}
			logger.severe("Conf triggered maintenance: " + i.getId() + " result=" + result);

			// obligation handling...
			if (hasObligationContract(trigger) && result) {
				if (nonSuspendedIntentionHasInsufficientConfidence(i, trigger)) {
					logger.severe("1) Post maintenance; setting as pending; " + trigger + " " + i.getId() + "\n" + i);
					StatsRecorder.getInstance()
							.recordString(dateFormat.format(new Date()) + ":(1)Pending int for " + l);
					getTS().getC().removeIntention(i);
					scheduleAsPendingIntention(trigger, i);
				}
				sendPostMaintUpdateToDependant(i, result);
			}

			lastMaint = i.clone();
		}

		if (hasObligationContract(trigger) && !getTS().getC().getPendingActions().containsKey(i.getId())) {
			logger.severe("2) Post maintenance; setting as pending; " + trigger + " " + i.getId() + "\n" + i);
			StatsRecorder.getInstance().recordString(dateFormat.format(new Date()) + ":(2)Pending int for " + l);

			getTS().getC().removeIntention(i);
			scheduleAsPendingIntention(trigger, i);// wait for reply
			final Collection<Literal> set = new Vector<Literal>();
			set.addAll(depWaits.get(trigger));
			set.add(l);
			depWaits.put(trigger, set);
		} else {
			logger.info("Inform obligant for triggering activity " + l);
			sendDepMaintDoneMsg(l);
		}
	}

	private boolean nonSuspendedIntentionHasInsufficientConfidence(Intention i, Literal trigger) {
		return (getObligationContract(trigger).getExternalCapability().getSpecificConfidence(bb,
				trigger) < getObligationContract(trigger).getMp().maintenanceAcceptanceThreshold()) && !i.isSuspended()
				&& !getWaitingList().containsKey(i.getId());
	}

	/**
	 * Adds intention mapped to l into the intentions queue, for picking up in
	 * selectIntention
	 * 
	 * @param l
	 */
	private void slipInPending(Literal l) {
		if (ts.getC().getPendingIntentions().containsKey(l.toString())) {
			Intention i = ts.getC().getPendingIntentions().remove(l.toString());
			i.setSuspended(false);
			/*
			 * Queue<Intention> old = ts.getC().getIntentions();
			 * ts.getC().getIntentions().clear();
			 * ts.getC().getIntentions().add(i); for(Intention iq: old) {
			 * ts.getC().getIntentions().add(iq); }
			 */
			ts.getC().addIntention(i);
			lastMaint = i.clone();
			logger.info("Updated intentions queue in " + ts.getC());
		} else {
			logger.severe("Pending not found for " + l + " in " + ts.getC());
		}
	}

	/**
	 * Sends a IL_DEP_MAINTAINED message to the actors identified in wl
	 * 
	 * @param wl
	 */
	private void sendDepMaintDoneMsg(final Literal wl) {
		final Message m = new Message(IL_DEP_MAINTAINED, getId(), IL_DEP_MAINTAINED, wl);
		final LinkedList<String> recip = getInvolvedAgents(wl);
		// set all recipients listed in wl
		for (final String id : recip) {
			if (!getId().equals(id)) {
				m.setReceiver(id);
				try {
					ts.getUserAgArch().sendMsg(m);
				} catch (Exception e) {
					logger.severe(e.toString());
					throw new RuntimeException("SendDepMaintDoneMsg error; " + e.toString());
				}
			}
		}
	}

	/**
	 * Notes the intention relevant to a confidence update message, so we can
	 * maintain it later. Presumes that we have already tested and confirmed the
	 * given obligation is a dependency.
	 */
	protected Intention intentionForConfUpdate(Literal obl) {
		Contract c = getDependencyContract(obl);
		int depIntId = (int) ((NumberTerm) c.getIntentionTrace().get(c.getIntentionTrace().size() - 1)).solve();
		logger.fine("Maintaining intention " + depIntId + " with updated confidence:\n" + getTS().getC());

		// get pending action, i.e. where dependency is executing
		Map<Integer, ActionExec> pa = getTS().getC().getPendingActions();
		if (!pa.isEmpty() && pa.containsKey(depIntId)) {
			logger.finer("Got intention to maintain from PAs: " + pa.get(depIntId));
			return pa.get(depIntId).getIntention();
		}

		// case where not PA, i.e. where waiting on other agent

		// check executing actions
		ActionExec action = getTS().getC().getAction();
		if (action != null && action.getIntention().getId() == depIntId) {
			logger.finer("Got intention to maintain from selected action: " + action);
			// return action.getIntention();
		}

		// check pending intentions?
		Map<String, Intention> pi = getTS().getC().getPendingIntentions();
		for (Intention in : pi.values()) {
			if (in.getId() == depIntId) {
				logger.finer("Got intention to maintain from PIs: " + in);
				return in;
			}
		}
		// if(i!=null) { maintainIntention(i); }

		logger.finer("Intention was not found " + depIntId);
		return null;
	}

	/**
	 * Informative counter thing
	 */
	private int maintCount = 0;

	/**
	 * Performs maintenance task formation and handling for the given intention
	 * i. Focuses on the ground and concrete/specific levels of the intention
	 * IMs; i.e. once it hits an unground action (at any level) or a subgoal
	 * addition, will stop trying to maintain. We only try to maintain actions
	 * with matching capabilities.
	 * 
	 * @param i
	 * @param fromObUpdate
	 *            - if true, update arose due to an obligation and ergo
	 *            maint-cap-before-execution limitation does not apply
	 * @return true <b>if changed IMs</b>
	 */
	public boolean maintain(final Intention i, final boolean fromObUpdate) {
		if (i.isFinished()) {
			logger.finer("Intention " + i.getId() + " complete; skip maintain step");
			return false;
		}

		final LinkedList<IntendedMeans> imsQ = agentUtils().intentionIntoIMsQueue(i);
		final IntendedMeans top = imsQ.getLast();
		final IntendedMeans bottom = imsQ.getFirst();

		if (bottom == null || bottom.getCurrentStep() == null) {
			logger.finer("bottom IM==null; wait for next pop of " + i);
			return false;
		}

		Trigger topTrig = top.getTrigger();
		Literal trigger = topTrig.getLiteral();

		long start = System.currentTimeMillis();
		if (!isMaintaining()) {
			return false;
		}
		String append = " from intention progression";
		if (fromObUpdate) {
			append = " from dependency update";
		}
		logger.info("Maintenance checking " + i + "(trigger=" + trigger + ")" + append + "\n" + ts.getC());

		// 'skip' conditions, if intentions next step is to be removed (IMs are
		// empty)
		if (shouldSkipMaintenance(i, top, bottom)) {
			logger.severe("shouldSkipConditions met, terminating maint of " + i.getId());
			return false;
		}

		// check complete; true if top == null, and all ims below have only one
		// activity
		if (intentionIsCompleted(i, imsQ, bottom)) {
			logger.severe("All done, so terminating maint of " + i.getId());
			return false;
		}

		// skipping failure handling plans
		if (topTrig.getType().equals(TEType.achieve) && topTrig.getOperator().equals(TEOperator.del)) {
			logger.severe("Failure handling plan, don't maint " + i.getId());
			return false;
		}

		if (hasObligationContract(trigger) && currentlyMaintaining.contains(trigger)) {
			logger.severe("Don't maintain a currently maintaining obligation; " + i.getId());
			return false;
		}

		if (maintainedTooManyTimes(fromObUpdate, trigger)) {
			logger.severe("Maintained too many times; " + i.getId());
			return false;
		}

		// this goes here because, otherwise, buf will force advertisement of
		// un-maintained intention
		if (hasObligationContract(trigger)) {
			logger.finer(i + "(trigger=" + trigger + ") is an obligation");
			currentlyMaintaining.add(trigger);
		}

		logger.severe("About to form agenda; " + (System.currentTimeMillis() - start) + "ms elapsed");
		start = System.currentTimeMillis();

		buf(ts.getUserAgArch().perceive(), false, "Start of maintaintence for \n" + i);
		heartbeat("Starting maintain for " + i);
		final Queue<MaintenanceTask> agenda = formAgenda(i, fromObUpdate);

		logger.severe("Formed agenda; " + (System.currentTimeMillis() - start) + "ms elapsed");

		boolean result = false;

		// try and solve tasks from queue. We end if run out of tasks without
		// success, or succeed in one task
		if (!agenda.isEmpty()) {
			heartbeat("Handling tasks in agenda " + agenda);
			start = System.currentTimeMillis();
			maintCount++;
			String ob = "";// debug op string...
			if (hasObligationContract(trigger)) {
				ob = " obligation";
			}
			logger.info(agenda.size() + " maintenance tasks for " + ob + "\n" + i);

			result = handleAgenda(agenda, result);

			logger.info("Handled maintenance tasks for " + i.getId() + ", result = " + result + ", "
					+ (start - System.currentTimeMillis()) + "ms elapsed");

			if (fromObUpdate) {
				if (result) {
					maintenanceSucceededForObligation(i, imsQ);
				} else { // !result
					maintenanceFailedForObligation(i, imsQ);
				}
			}
			currentlyMaintaining.remove(trigger);
		} else {
			emptyAgendaHandling(i, trigger);
			return false;
		}
		heartbeat("Ending maintenance " + agenda);
		return result;
	}// end maint method

	private boolean intentionIsCompleted(Intention i, LinkedList<IntendedMeans> imsQ, IntendedMeans bottom) {
		if (bottom.getCurrentStep() == null) {
			for (IntendedMeans im : imsQ) {
				if ((im.getCurrentStep() != null) && (im.getCurrentStep().getBodyNext() != null)) {
					return false;
				}
			}
			logger.info("No need to maintain a completed intention; " + i);
			return true;
		}
		return false;
		/*
		 * if(bottom.getCurrentStep() == null) { boolean allDone = true;
		 * imCheck: for(IntendedMeans im: imsQ) { if((im.getCurrentStep() !=
		 * null) && (im.getCurrentStep().getBodyNext() != null)){ allDone =
		 * false; break imCheck; } }
		 * 
		 * if(allDone) { logger.info(
		 * "No need to maintain a completed intention; " + i); return true; } }
		 * return false;
		 */
	}

	private boolean handleAgenda(Queue<MaintenanceTask> agenda, boolean result) {
		while (!agenda.isEmpty() && !result) {
			MaintenanceTask mt = agenda.remove();// head = top priority element
			result = handleMaintenanceTask(mt);
		}
		return result;
	}

	private void maintenanceSucceededForObligation(Intention i, LinkedList<IntendedMeans> imsQ) {
		// update the maintainted annotation - args are (A,B) where
		// a=changes to intention since last execution
		// b=total change count for intention
		logger.info("set maintained annotation");

		LinkedList<IntendedMeans> q = imsQ;
		for (IntendedMeans im : q) {
			Literal tL = im.getTrigger().getLiteral();
			ListTerm annots = tL.getAnnots(UniversalConstants.ANNOT_MAINTAINED);
			Term maintAnnot;
			int count = 1;
			int total = 1;
			if (!annots.isEmpty()) {
				if (annots.size() > 1) {
					logger.severe(
							tL + " has too many '" + UniversalConstants.ANNOT_MAINTAINED + "' annotations! " + annots);
				}
				count += (int) ((NumberTerm) ((Literal) annots.get(0)).getTerm(0)).solve();
				total += (int) ((NumberTerm) ((Literal) annots.get(0)).getTerm(1)).solve();
				tL.getAnnots().removeAll(annots); // remove old
			}
			maintAnnot = ASSyntax.createLiteral(UniversalConstants.ANNOT_MAINTAINED, ASSyntax.createNumber(count),
					ASSyntax.createNumber(total));
			tL.addAnnot(maintAnnot);
			im.getTrigger().setLiteral(tL);
			logger.severe("Updated maintCount trigger " + tL);
		}
		logger.finer("Updated plan-trigger annots \n" + i.toString());
	}

	private void maintenanceFailedForObligation(Intention i, LinkedList<IntendedMeans> imsQ) {
		logger.info("set maint failed annotation");
		LinkedList<IntendedMeans> q = imsQ;
		for (IntendedMeans im : q) {
			Literal tL = im.getTrigger().getLiteral();
			ListTerm annots = tL.getAnnots(UniversalConstants.ANNOT_MAINTAIN_ATTMPT);
			Term maintAnnot;
			int count = 1;
			if (!annots.isEmpty()) {
				if (annots.size() > 1) {
					throw new RuntimeException(tL + " has too many '" + UniversalConstants.ANNOT_MAINTAIN_ATTMPT + "' annotations! "
							+ annots);
				}
				count += (int) ((NumberTerm) ((Literal) annots.get(0)).getTerm(0)).solve();
				tL.getAnnots().removeAll(annots); // remove old
			}
			maintAnnot = ASSyntax.createLiteral(UniversalConstants.ANNOT_MAINTAIN_ATTMPT, ASSyntax.createNumber(count));
			tL.addAnnot(maintAnnot);
			im.getTrigger().setLiteral(tL);
			logger.severe("Updated " + UniversalConstants.ANNOT_MAINTAIN_ATTMPT + " on " + tL);
		}
		logger.finer("Updated plan-trigger annots \n" + i.toString());
	}

	private void emptyAgendaHandling(Intention i, Literal trigger) {
		logger.info("No maintenance tasks for " + i.getId());
		heartbeat("Ending maintenance with empty agenda");
		if (hasObligationContract(trigger)) {
			logger.info("Completed maintenance for " + trigger);
			currentlyMaintaining.remove(trigger);
			float updatedConfidence = calculateIntentionConfidence(i.clone(), getBB().clone());

			if (updatedConfidence != getObligationContract(trigger).getExternalCapability().getGeneralConfidence(bb)) {
				logger.info("Completed maintenance for " + trigger + "\nNo changes, update conf =" + updatedConfidence);
				// sendConfidenceUpdateMessage(getObligationContract(trigger),
				// updatedConfidence);
				this.getObligationContract(trigger).getExternalCapability().setConf(updatedConfidence);
			} else {
				logger.info("No confidence change between contract and modified for " + trigger);
			}
		}
	}

	private boolean maintainedTooManyTimes(boolean fromObUpdate, Literal trigger) {
		if (trigger.getAnnots(UniversalConstants.ANNOT_MAINTAINED) != null
				&& !trigger.getAnnots(UniversalConstants.ANNOT_MAINTAINED).isEmpty()) {
			int maintCount = (int) ((NumberTerm) ((Literal) trigger.getAnnots(UniversalConstants.ANNOT_MAINTAINED)
					.get(0)).getTerm(0)).solve();
			int totalMaintCount = (int) ((NumberTerm) ((Literal) trigger.getAnnots(UniversalConstants.ANNOT_MAINTAINED)
					.get(0)).getTerm(1)).solve();
			if (fromObUpdate && maintCount > MAINTAIN_CAP) {
				logger.severe(trigger + " has exceeded maintenance cap " + maintCount + ", so failing!");

				if (!trigger.hasAnnot(MAINT_CAP_ANNOT)) {
					trigger.addAnnot(MAINT_CAP_ANNOT);
				}

				StatsRecorder.getInstance().recordString(dateFormat.format(new Date()) + "; " + trigger
						+ " exceeded maint cap (" + MAINTAIN_CAP + ") without executions");
				return true;
			}

			if (fromObUpdate && totalMaintCount > TOTAL_MAINT_CAP) {
				logger.severe(trigger + " has exceeded maximum maintenance changes, so failing!");
				StatsRecorder.getInstance().recordString(dateFormat.format(new Date()) + "; " + trigger
						+ " exceeded total maint cap (" + TOTAL_MAINT_CAP + ")");
				return true;
			} else {
				logger.finer("Maintaining " + trigger + ", maint count=" + (maintCount + 1));
			}
		}

		if (trigger.getAnnots(UniversalConstants.ANNOT_MAINTAIN_ATTMPT) != null
				&& !trigger.getAnnots(UniversalConstants.ANNOT_MAINTAIN_ATTMPT).isEmpty()) {
			int plCount = (int) ((NumberTerm) ((Literal) trigger.getAnnots(UniversalConstants.ANNOT_MAINTAIN_ATTMPT)
					.get(0)).getTerm(0)).solve();
			if (plCount > TOTAL_MAINT_CAP) {
				logger.severe(trigger + " has exceeded maintenance cap, so failing!");
				StatsRecorder.getInstance().recordString(dateFormat.format(new Date()) + "; " + trigger
						+ " exceeded plan attempt cap (" + PLAN_CAP + ")");
				return true;
			}
		}
		return false;
	}

	private boolean shouldSkipMaintenance(Intention i, IntendedMeans top, IntendedMeans bottom) {
		PlanBody topCurrStep = top.getCurrentStep();
		PlanBody nextStep = bottom.getCurrentStep();
		if (topCurrStep == null) {
			logger.info("Top IM==null; about to complete " + i);
			return true;
		} else if (i.isSuspended()) {
			// logger.info("Suspended, do not maintain " + i.getId() + "\n" +
			// ts.getC());
			// return true;
		} else if (!groundPlan(top.getPlan(), top.getUnif())) {
			logger.info("Top plan is not ground ;" + top.getPlan());
			return true;
		} else if (nextStep.isInternalAction()) {
			logger.info("Next action " + nextStep.getBodyTerm() + " is internal, wait for decomposition of " + i);
			return true;
		} else if (nextStep.getBodyTerm().toString().replaceAll("\"", "").equals(".fail")) {
			// note; should be caught by IA check
			logger.info("Top IM - next activity " + nextStep.getBodyTerm() + "is a forced fail!; skip maint");
			return true;
		}
		return false;
	}

	/**
	 * Return true if all literals in the plan are ground
	 * 
	 * @param plan
	 * @param unifier
	 * @return
	 */
	private boolean groundPlan(Plan plan, Unifier unifier) {
		PlanBody pb = plan.getBody();
		while (pb != null) {
			pb = pb.clonePB();
			pb.apply(unifier);
			if (!pb.isGround()) {
				return false;
			}
			pb = pb.getBodyNext();
		}
		return true;
	}

	protected void sendPostMaintUpdateToDependant(final Intention i, final boolean result) {
		final float updatedConfidence = calculateIntentionConfidence(i.clone(), getBB().clone());
		final Literal trigger = agentUtils().intentionIntoIMsQueue(i).getLast().getTrigger().getLiteral();
		final Contract obligationContract = getObligationContract(trigger);
		final ExternalCapability oldEc = obligationContract.getExternalCapability();
		final GoalTask goal = oldEc.achieves();
		final List<String> missingPreconditions = AgentUtils.getExtraPrecondSetForPlan(this,
				AgentUtils.extractOrderedLeafActivityList(i.clone()), getBB());
		logger.severe("Got " + missingPreconditions.size() + " precond set");
		if(!missingPreconditions.isEmpty()) {
			logger.severe("Got non-empty missing precond set; " + missingPreconditions); 
			final int iterationLength = goal.getPreconditions().length;
			final String[] preconditions = new String[goal.getPreconditions().length * missingPreconditions.size()];
			final Unifier u = AgentUtils.formUnifierFromArgs(oldEc.getSignature(), obligationContract.getActivity());
			for (int j = 0; j < iterationLength;j++) {
				logger.severe("j=" + j + "<" + iterationLength);
				for (int k = 0; k < missingPreconditions.size(); k++) {
					logger.severe("k=" + k + "<" + missingPreconditions.size());
					preconditions[j + k] = combinePreconditions(
							applyUnifierToLfString(goal.getPreconditions()[j], u),
							missingPreconditions.get(k));
					logger.severe("Set precondition[" + (j+k) + "] to " + preconditions[j+k]);
				}
				logger.severe("Combined " + goal.getPreconditions()[j] + " with " + missingPreconditions + "(j="
						+ (j + missingPreconditions.size()) + ")");
			}
			oldEc.setPreconditions(preconditions);
		}
		else {
			logger.severe("empty missing precond set");
		}
		
		oldEc.setConf(updatedConfidence);
		logger.severe("Set conf to "+ updatedConfidence);
		obligationContract.setExternalCapability(oldEc);
		logger.info("Obligation maintenance complete for " + trigger);
		sendOblMaintainedMessage(obligationContract);
	}

	private String applyUnifierToLfString(final String s, final Unifier u) {
		final LogicalFormula lf = LogExpr.parseExpr(s);
		lf.apply(u);
		return lf.toString();
	}

	private void sendOblMaintainedMessage(final Contract obligationContract) {
		// String ilf, String s, String r, Object c
		Message m = new Message(IL_OB_MAINTAINED, getId(), obligationContract.getDependentId(), obligationContract);
		try {
			getTS().getUserAgArch().sendMsg(m);
		} catch (final Exception e) {
			String s = "Exception sending message " + m + "\n" + e.toString();
			for (StackTraceElement st : e.getStackTrace()) {
				s = s + "\n\t" + st;
			}
			logger.severe(s);
		}
	}

	private String combinePreconditions(final String prepend, final String missingPreconditions) {
		logger.severe("Combine preconditions; " + prepend + " and " + missingPreconditions);
		final Collection<String> mushed = addIfNotPresent(
				addIfNotPresent(new ArrayList<String>(), Arrays.asList(prepend.split("&"))),
				Arrays.asList(missingPreconditions.split("&")));

		String precondition = "";
		final Iterator<String> preId = mushed.iterator();
		while(preId.hasNext()) {
			precondition = precondition + preId.next() + (preId.hasNext()?" & ":"");
		}
		
		return LogExpr.parseExpr(precondition).toString();
	}

	private Collection<String> addIfNotPresent(final Collection<String> mushed, final Collection<String> addMe) {
		logger.info("Add if not present\n" + mushed + "\n+\n" + addMe);
		final Iterator<String> atoms = addMe.iterator();
		while (atoms.hasNext()) {
			final String atom = normalizeAtom(atoms.next());
			if (!mushed.contains(atom)) {
				mushed.add(atom);
			}
		}
		return mushed;
	}
	
	private String normalizeAtom(final String atom) {
		String normalizedAtom = atom.trim();
		if(normalizedAtom.startsWith("(")) {
			normalizedAtom = normalizedAtom.substring(1);
		}
		while(charCount('(', normalizedAtom) > charCount(')', normalizedAtom)) {
			normalizedAtom = normalizedAtom + ")";
		}
		if(charCount('(', normalizedAtom) < charCount(')', normalizedAtom)) {
			normalizedAtom = normalizedAtom.substring(0, normalizedAtom.length()-1);
		}
		while(normalizedAtom.startsWith("(") && normalizedAtom.endsWith(")")) {
			normalizedAtom = normalizedAtom.substring(1, normalizedAtom.length()-1);
		}
		return LogExpr.parseExpr(normalizedAtom).toString();
	}
	
	private int charCount(final char counted, final String s) {
		int i = 0;
		final char[] sC = s.toCharArray();
		for(char c:sC) {
			if(c == counted) { i++; }
		}
		return i;
	}

	/**
	 * Determines if there are any maintenance activities required (to be
	 * performed) for the given intention, using step as the start point and
	 * unif as the variable assignment
	 * 
	 * @param i
	 *            - Intention being maintained
	 * @param fromObUpdate
	 *            - if true, we've just received an obligation update message
	 * @return queue of priority ordered maintenance tasks
	 */
	private Queue<MaintenanceTask> formAgenda(final Intention i, final boolean fromObUpdate) {
		heartbeat("Starting formAgenda for " + i);
		long agendaFormationTime = System.nanoTime();

		// execution context; cloned here to avoid any access concurrency
		// issues, and progressed with action effects...
		BeliefBase context = getBB().clone();
		PriorityQueue<MaintenanceTask> agenda = new PriorityQueue<MaintenanceTask>(); // will
																						// be
																						// returned
		// get a queue of means; this is due to remove of direct stack object
		// access in the Intentions API
		LinkedList<IntendedMeans> planMeans = agentUtils().intentionIntoIMsQueue(i);

		// check top-level trigger for intention vs obligations and update EEC
		// if required
		// this only applies for non-executing intentions - as once executing,
		// the current context is the current BB...
		Literal trigger = planMeans.peekLast().getTrigger().getLiteral();
		if (hasObligationContract(trigger) && !isExecuting(trigger)) {
			logger.info("Non-executing Intention; stems from obligation for " + trigger + "\n" + i);
			context = insertChangeSetIntoBB(getObligationContract(trigger).providedCLStates(), context);
			logger.finer("Updated context;" + context);
		}

		// the most ground, i.e. currently executing, intended means (plan)
		IntendedMeans groundLevel = planMeans.peek();

		int count = 0;

		topLoop: while (!planMeans.isEmpty() & (count < maintainLookahead)) {
			IntendedMeans im = planMeans.remove();
			Unifier u = im.getUnif();
			PlanBody step = im.getCurrentStep();

			// if !at most ground level, skip the current step
			if (im.equals(groundLevel)) {
				logger.finer("At most ground level..." + im + " (tasks? " + agenda.size() + ")\n" + getTS().getC());
			} else {
				logger.finer("At a more abstract IM level..." + im + "\n" + agenda.size() + " tasks so far");
				if (step != null) {
					step = step.getBodyNext();
				} // note, may now == null
			}

			// iterate through steps
			logger.finer("Beginning iteration at this level of IMs...");

			/*
			 * This gets a little complicated... first, we only handle steps
			 * which are actions with capabilities. That includes subgoal
			 * addition and internal actions but not - for example - belief
			 * checks. We also stop at the first unground step, as we can't
			 * replan to fix these anyway and presume we can't correctly
			 * estimate the post-state for subsequent actions
			 */
			stepLoop: while (step != null) {
				++count; // update counter restriction
				Term bodyTerm = step.getBodyTerm();
				if (bodyTerm == null) {
					logger.finer("Null body");
					break stepLoop;
				} else if (!bodyTerm.isGround()) {
					logger.finer(bodyTerm + " is unground - ending!");
					break topLoop;
				}
				typeCheck: if (!bodyTerm.isLiteral() || bodyTerm.isInternalAction()) {
					logger.finer("Not literal term or Atomic action, no maintain ops: " + bodyTerm);
					// if the IA is .fail, then the ims are predefined to fail
					// -no maint is required, because
					// the end state is failure anyway.
					if (isFailActivity(bodyTerm)) {
						logger.severe(bodyTerm + " is a fail; skip agenda formation!");
						return new PriorityQueue<>();
					}
				} else if (bodyTerm.isLiteral() && !bodyTerm.isInternalAction()
						&& maintainableStep(step.getBodyType())) {
					String annot = ""; // annotation, set for (some) mts
					Literal action = (Literal) bodyTerm.clone();
					if (!action.isGround()) {
						action.apply(u);
					}
					logger.info("Applied " + u + ", Try maintain ops: " + action);
					if (!action.isGround()) {
						logger.finer(action + " is an unground action - ending!");
						break topLoop; // end!
					}

					// fetch the MP. if we have one for a specific action use
					// that; else take the
					// one for the trigger literal (parent action of the
					// composite / plan)

					final MaintenancePolicy mp = hasObligationContract(trigger) ? getObligationContract(trigger).getMp()
							: getMaintenancePolicy(action);

					// check if we have the requisite knowledge to perform
					// maintenance ops on this action
					Capability c = null;

					// if currently executing, preconditions hold; so skip
					// precond checks and
					// effects check...
					final ActionExec aSet = getTS().getC().getPendingActions().get(i.getId());
					boolean executing = (aSet != null && aSet.getActionTerm().equalsAsStructure(action));
					boolean preHold = executing;
					PlanBody nextStepForDelegation = executing ? step.getBodyNext() : step;

					boolean delegated = isDelegatedActivity(step);

					logger.finer(
							action + " is delegated? " + delegated + "(pcaps=" + getPrimitiveCapabilities().keySet()
									+ ", ccaps=" + getCompositeCapabilities().keySet() + ")");

					/*
					 * We want to a) get the required capability and b) check
					 * the preconditions. B) is done here because of some issues
					 * with matching the likely external capabilities to the
					 * actions.
					 * 
					 * IF we have the local capability, this is relatively
					 * simple - we check the preconditions. IF the capability is
					 * composite (multiple plans), we run an additional check to
					 * see if a plan exists
					 * 
					 * IF the capability is for a delegated action, there are
					 * two sources. Firstly, if a relationship has been formed,
					 * it can be taken straight from the contract. If no
					 * dependency contract exists (yet), the percept literals
					 * for external capabilities are scoured and the 'best fit'
					 * selected (i.e. with highest confidence and preconds
					 * holding)
					 */
					if (hasCapability(step) && !delegated) {
						logger.finer("Performing maintenance for agent-held action" + action
								+ "; Fetching capability from local cs'");
						c = getCapability(step);
						logger.info(action + " matched to capability " + c.getClass().getCanonicalName());
						if (!executing) {
							preHold = c.preconditionsHold(context, action);

							if (logger.isLoggable(Level.INFO)) {
								String preset = "\n";
								for (String s : c.getPreconditions()) {
									preset = preset + s + "\n";
								}
								logger.info(action + "; prehold for local action=" + preHold + "{" + preset + "}");
							}

							// composite capabilities have extra checks, to see
							// if a plan can be selected for them
							if (!c.isPrimitive()) {
								CompositeCapability cc = (CompositeCapability) c;
								// next exec - more refined check
								if (im.equals(groundLevel) && im.getCurrentStep().equals(step)) {
									preHold = cc.preconditionsHoldAndPlanExists(context, action);
									logger.info("Special plan check for " + action + "; plan can be selected? = "
											+ preHold);
								}
							}
						} else {
							logger.warning("Skipping preconditions check for executing action " + step);
						}
					}
					/*
					 * This condition is to cover a special case, where a plan
					 * is inserted into the plan lib by some action; we use the
					 * task knowledge to determine if we can create a 'stand in'
					 * stub capability object.
					 */
					else if (!delegated && !hasCapability(step) && getTaskFactory().factory(action) != null) {
						logger.fine("Invoking special case for stub actions ");
						c = new StubForComposite(this, getTaskFactory().factory(action));
						if (!executing) {
							preHold = c.preconditionsHold(context, action);
							logger.finer("Not executing: " + getTS().getC());
						}
						logger.finer("Stub capability = " + c + "\nPreHold=" + preHold);
						if (!preHold) {
							bbDump(context, Level.FINER);
						}
					} else if (hasDependencyContract(action)
							&& getDependencyContract(action).getExternalCapability() != null) {
						logger.finer(action + "; Getting capability from associated dependency contract");
						c = getDependencyContract(action).getExternalCapability();
						preHold = executing ? true : c.preconditionsHold(context, action);
						// I think this is always the case, as we don't recalc
						// pre's even post-maint

						logger.finer("External capability = " + c + "\nPreHold=" + preHold);
						if (!preHold) {
							bbDump(context, Level.FINER);
						}
					}
					// Special check for lack of contract and failure to form
					// prev contract
					// if delegated, no dependency contract, have prev failed
					// and about to exec, generate EMt
					// Note that does not prohibit the same plan being formed;
					// assumes that failures to accept are reflected in the EC
					// adverts, so we don't get them in the inferred domain
					else if (delegated && !hasDependencyContract(action)) {
						if (action.equals(nextStepForDelegation.getBodyTerm())
								&& contractRejectImmediatelyPriorToExec.contains(normalize(action))) {
							// form new effects maintenance action and exit
							// confidence = 0 as no contract in prior task - no
							// exec
							logger.warning("Lack of contract for nextStep activity, Deps;\n" + getDependencies());
						}
						// delegated task, which we have no contract and haven't
						// tried/failed yet
						else if (knowsProviderForExtCapability(action.getFunctor())
								&& !contractRejectImmediatelyPriorToExec.contains(normalize(action))) {
							c = getBestFitExtCapability(context, action);
							logger.warning("Performing maintenance for outside-held action " + action
									+ "(not found in contracts " + getDependencies() + ")" + "\nBest guess match;\n"
									+ c);
							if (c != null && !executing) {
								preHold = c.preconditionsHold(context, action);
							} else if (c == null) {
								logger.info("No best-fit EC found for " + action + ", adding an effects task");
								agenda.add(new MaintenanceTask(Type.effects, i, im, step, context, 0, mp, c,
										"Delegated activity without a capability match"));
							}
						}
					} else {
						logger.warning("Unknown capability;" + action + "(delegated=" + delegated + ")"
								+ "\nhasDependency=" + hasDependencyContract(action) + ", skipForm="
								+ contractRejectImmediatelyPriorToExec + "\nhasCapability=" + hasCapability(step)
								+ ", has knowledge=" + getTaskFactory().factory(action) + "\nknows ext = "
								+ knowsProviderForExtCapability(action.getFunctor()));
						break typeCheck;
					}

					// get confidence using method, now we have the capability
					heartbeat("Starting confidence calculation for " + action);
					float confidence = (c == null) ? 0 : calculateConfidence(context, action, c);
					heartbeat("Finished confidence calculation for " + action);

					// now, determine which maintenance tasks should be
					// generated - if any - versus the confidence value
					// we don't care about this if !preHold...
					boolean effects = preHold ? confidence < mp.maintenanceTriggerThreshold() : false;
					if (effects) {
						annot += "(conf=" + confidence + ") triggered by threshold; "
								+ mp.maintenanceTriggerThreshold();
					}

					// note debug info
					logger.info(action + " preconditions hold?=" + preHold + ", (confidence=" + confidence
							+ "), effects triggered?=" + effects);

					// create MTs
					if (!preHold) {
						agenda.add(generatePreconditionsTask(i, context, im, step, annot, action, mp, c));
					} else if (effects && (!executing || executing && hasDependencyContract(action))) {
						// if executing, then we don't maintain...
						// form new task for the same goal
						agenda.add(new MaintenanceTask(Type.effects, i, im, step, context, confidence, mp, c, annot));
						logger.warning("Effects MT added " + step.toString() + " " + annot);
					}

					// update BB with action postEffects, if we have capability
					// information
					logger.finer("Done maint check for " + action + "; updating BB");
					if (c != null) {
						context = estimatePostExecutionBB(context, action, c);
						logger.finer("Updated the BB with effects of held capability " + action);
					}
					// note; last step in IM will pass on modified bb to next-up
					// IM, so we propagate the
					// bb states
				}

				step = step.getBodyNext();// iterate
			} // end stepLoop (iteration through current IM level)

			/*
			 * Merging; if we have more than one task, and not all tasks are
			 * precond tasks, and we can do so, we merge these into a single
			 * effects MT that will try and replace the IM at fault.
			 */
			agenda = consolidateTasks(i, context, agenda, planMeans);

			heartbeat("Next part of while loop...");
		} // move to next IM
		agendaFormationTime = System.nanoTime() - agendaFormationTime;

		Analytics.getInstance().logTime(AnalyticsOperations.agendaFormation, agendaFormationTime);

		logger.fine("Form maint tasks completed (" + agenda + ")- " + (agendaFormationTime / 1000) + "ms elapsed");
		return agenda;
	}

	private boolean isDelegatedActivity(final PlanBody planBody) {
		final Literal action = (Literal) planBody.getBodyTerm();
		return MultiagentArch.isJointActivity(action, this)
				&& !MultiagentArch.getInvolvedAgents(action, this).get(0).equals(getId()) && !hasCapability(planBody);
	}

	private boolean isFailActivity(Term bodyTerm) {
		return bodyTerm.isInternalAction() && bodyTerm.toString().replaceAll("\"", "").equals(".fail");
	}

	private MaintenanceTask generatePreconditionsTask(Intention i, BeliefBase context, IntendedMeans im, PlanBody step,
			String annot, Literal action, MaintenancePolicy mp, Capability c) {
		/*
		 * We use precondition maintenance to preserve plan dependency
		 * relationships (if they exist); it is assumed that any larger-scale
		 * replanning will preserve the desired effects, anyway.
		 */
		boolean dependenciesInPlan = areDependenciesFormed(step);

		// insert plan to restore preconditions
		if (dependenciesInPlan) {
			if (dependenciesInPlan) {
				annot = annot + " (Has dependency contracts associated with itself, or " + "subsequent plan actions)";
			}

			// Note; conf == 0
			final MaintenanceTask mt = new MaintenanceTask(Type.precondition, i, im, step, context, 0, mp, c, annot);
			logger.warning("Precondition MT added " + step.toString() + " " + annot);
			bbDump(context, Level.FINER);
			return mt;
		} else {
			annot = "Added to replace a PM task for a non-dependency action";
			final MaintenanceTask mt = new MaintenanceTask(Type.effects_for_pre, i, im, step, context, 0, mp, c, annot);
			logger.warning("Effects replacement of Precondition MT added " + step.toString() + " " + annot);
			return mt;
		}
	}

	/**
	 * @param i
	 * @param context
	 * @param agenda
	 * @param planMeans
	 * @return
	 */
	private PriorityQueue<MaintenanceTask> consolidateTasks(Intention i, BeliefBase context,
			PriorityQueue<MaintenanceTask> agenda, LinkedList<IntendedMeans> planMeans) {
		if (agenda.isEmpty()) {
			return agenda;
		}
		// if the first activity has a dependency / ANY activity following has a
		// dependency, exit
		if (areDependenciesFormed(agenda.iterator().next().getMaintainedActionStart())) {
			return agenda;
		}

		if ((agenda.size() > 1) && (planMeans.peek() != null) && planMeans.peek().getCurrentStep() != null) {
			IntendedMeans pim = planMeans.peek();
			if (hasCapability(pim.getCurrentStep())) {
				for (MaintenanceTask mt : agenda) {
					// precond task check
					if (mt.getType().equals(Type.precondition)) {
						return agenda; // early termination
					}
				}
				Literal stepTask = (Literal) pim.getCurrentStep().getBodyTerm();
				float minConf = 1;
				for (MaintenanceTask mt : agenda) {
					if (mt.getConf() < minConf) {
						minConf = mt.getConf();
					}
				}
				MaintenanceTask imTask;
				if (minConf == 0) {
					imTask = new MaintenanceTask(MaintenanceTask.Type.effects_for_pre, i, pim, pim.getCurrentStep(),
							context, minConf, getMaintenancePolicy(stepTask), getCapability(pim.getCurrentStep()),
							"Merged from multiple IM agenda tasks");
				} else {
					imTask = new MaintenanceTask(MaintenanceTask.Type.effects, i, pim, pim.getCurrentStep(), context,
							minConf, getMaintenancePolicy(stepTask), getCapability(pim.getCurrentStep()),
							"Merged from multiple IM agenda tasks");
				}
				logger.severe("Multiple tasks on the lower IM\n" + agenda + "\nMerging into a single task;\n" + imTask);
				agenda.clear();
				agenda.add(imTask);
			} else if (agenda.size() > 1) {
				logger.severe("No consolidation... " + agenda);
			}
		}
		return agenda;
	}

	protected int effectsTasks(PriorityQueue<MaintenanceTask> agenda) {
		int agendaEffCount = 0;
		for (MaintenanceTask mt : agenda) {
			if (mt.getType().equals(Type.effects)) {
				agendaEffCount++;
			}
		}
		return agendaEffCount;
	}

	protected int preTasks(PriorityQueue<MaintenanceTask> agenda) {
		int agendaEffCount = 0;
		for (MaintenanceTask mt : agenda) {
			if (mt.getType().equals(Type.precondition) || mt.getType().equals(Type.effects_for_pre)) {
				agendaEffCount++;
			}
		}
		return agendaEffCount;
	}

	/**
	 * Return true if step, or its successors, require a dependency to be formed
	 * <b>and</b> that dependency has been formed (contract exists). Returns
	 * false if there are no joint actions in the plan (from step inclusive), or
	 * if there ARE joint actions but no contract has been formed. </br>
	 * </br>
	 * Will only continue as far as there are ground actions.
	 * 
	 * @param step
	 * @return boolean
	 */
	private boolean areDependenciesFormed(PlanBody step) {
		logger.finer("areDependenciesFormed: Checking dependencies; " + step);
		stepLoop: while (step != null) {
			Literal action = ((Literal) step.getBodyTerm()).copy();
			if (!action.isGround()) {
				logger.warning("Skipping out from unground step: " + action);
				break stepLoop;
			}
			if (MultiagentArch.isJointActivity(action, this) && hasDependencyContract(action)) {
				logger.finer("areDependenciesFormed: " + action + " is a dependency and has a contract; return true");
				return true;
			}
			step = step.getBodyNext();
		}
		logger.finer(
				"areDependenciesFormed: No dependencies found / formed.  Current dependencies = " + getDependencies());
		return false;
	}

	/**
	 * Calculates the confidence of action, associated with Capability c, when
	 * executed with belief base estExecContext.
	 * 
	 * FIXME - only return general conf or 1 if we know this is a dependency and
	 * the obligant is currently performing maintenance?
	 * 
	 * @param estExecContext
	 * @param action
	 * @param c
	 * @return
	 */
	private float calculateConfidence(final BeliefBase estExecContext, Literal action, final Capability c) {
		action = action.copy();
		logger.info("Calculating confidence; " + action + "; c= " + c);
		/*
		 * Special case if the action is delegated... 1/ if the performer has
		 * sent a specific confidence value, use that 2/ else, take the general
		 * confidences from the ExternalCapability entry and select the lowest
		 * to return
		 */
		if (c.isExternal()) {
			logger.info("External capability for " + action);
			// if we have specific info, return the confidence sent by the
			// primary executing obligant
			if (hasDependencyContract(action)) {
				return getDependencyContract(action).getExternalCapability().getSpecificConfidence(estExecContext,
						action);
			} else if (action.isGround()) {
				return getLowestConfidenceFromActorsInInternalActivity(estExecContext, action);
			} else {
				return c.getGeneralConfidence(estExecContext);
			}
		} else {
			return doInternalActivityConfidenceEstimation(estExecContext, action, c);
		}
	}

	private float doInternalActivityConfidenceEstimation(BeliefBase estExecContext, Literal action, Capability c) {
		logger.info("Internal confidence estimation for " + action);
		// (implicit) else, internal
		if (c.isPrimitive() & action.isGround()) {
			logger.fine("Confidence for ground prim action " + action);
			return c.getSpecificConfidence(estExecContext, action);
		} else if (c.isPrimitive() & !action.isGround()) {
			logger.fine("Confidence for unground prim action " + action);
			return c.getGeneralConfidence(estExecContext);
		} else if (!c.isPrimitive() && action.isGround()) {
			logger.fine("Confidence for ground composite " + action);
			CompositeCapability cc = ((CompositeCapability) c);
			// plan/composite; can get a better confidence measurement if
			// ground; i.e. all vars unified to a value...
			Unifier u = formUnifierFromArgs(c.getSignature(), action);
			logger.finer("Note; action is ground; u = " + u);
			action.apply(u);
			return cc.getSpecificConfidence(estExecContext, action);// ,u);
		} else { // composite & !ground
			logger.fine("Confidence for unground composite " + action);
			return c.getGeneralConfidence(estExecContext);
		}
	}

	private float getLowestConfidenceFromActorsInInternalActivity(BeliefBase estExecContext, Literal action) {
		LinkedList<String> actors = MultiagentArch.getInvolvedAgents(action, this);
		if (actors.contains(getId())) {
			actors.remove(getId());
		}

		float lowest = 1;
		String lowestActor = "N/A";
		for (String actor : actors) {
			// average out the general confidence of this actor for all HOLDING
			// preconds
			Collection<ExternalCapability> cSet = getExtCapability(action.getFunctor(), actor);
			float conf = 0;
			int cSize = 0;
			if (!cSet.isEmpty() && actor.equals(actors.getFirst())) {
				for (ExternalCapability ec : cSet) {
					if (ec.preconditionsHold(estExecContext, action)) {
						conf += ec.getGeneralConfidence(estExecContext);
						cSize++;
					}
				}
				conf = conf / cSize;
				logger.info("Averaged confidence " + conf + " from " + cSet + " for " + actor);
			} else if (!actor.equals(actors.getFirst())) {
				logger.severe("Empty external capability set for " + action + ", " + actor);
				conf = 1.1f; // for check loop
			}
			// else, no worries - we don't need confidence for subordinate...

			if (lowest > conf) {
				lowest = conf;
				lowestActor = actor;
				logger.info("Set new lowest; " + actor + ", conf=" + conf);
			}
		}
		logger.fine(action + ", lowest confidence actor= " + lowestActor + "(" + lowest + ")");
		return lowest;
	}

	/**
	 * Checks if a planBody is of a type that can be maintained; we only
	 * maintain actions or subgoals, so things like belief changes are ignored)
	 * 
	 * @param type
	 *            - step type
	 * @return true if we want to check this step for maintenance tasks
	 */
	private boolean maintainableStep(BodyType type) {
		return !(type.equals(PlanBody.BodyType.achieveNF) || type.equals(PlanBody.BodyType.addBel)
				|| type.equals(PlanBody.BodyType.delBel) || type.equals(PlanBody.BodyType.delAddBel));
	}

	/**
	 * Attempts to modify an intention wrt a specified maintenance task
	 * (problem)
	 * 
	 * @param t
	 *            - a maintenance task... effects or precondition type
	 * @return true if t was succesfully handled and changes made to the
	 *         intention
	 */
	private boolean handleMaintenanceTask(MaintenanceTask t) {
		// handle drop (Intention abortion)
		if (t.getType().equals(MaintenanceTask.Type.drop)) {
			return handleDrop(t);
		}

		Literal maintAct = (Literal) t.getMaintainedActionStart().getBodyTerm();
		maintAct = maintAct.copy();
		maintAct.apply(t.getIm().getUnif());

		if (!maintAct.isGround()) {
			logger.warning("mt " + maintAct + " exit - unground task;\n" + t.toString());
			return false;
		}

		// check the effects...
		cCheck:
		/*
		 * We are trying to catch the case where a precondition does not hold,
		 * but that precondition actually corresponds to a desired effect - i.e.
		 * the goal has become true at some point after the plan was formed, and
		 * this activity is not required in the plan any more.
		 */
		if (t.getType().equals(MaintenanceTask.Type.precondition) && (t.getCapability() != null)) {
			Capability c = t.getCapability();
			for (String l : c.achieves().getAddEffects()) {
				Literal pe = LiteralImpl.parseLiteral(l);
				if (t.getContext().contains(pe) == null) {
					break cCheck;
				}
			}
			for (String l : c.achieves().getDeleteEffects()) {
				Literal pe = LiteralImpl.parseLiteral(l);
				if (t.getContext().contains(pe) != null) {
					break cCheck;
				}
			}
			logger.warning("Maintenance of " + maintAct
					+ " found that ec contains required effects; skipping for parent task");
			return false;
		} else if (t.getType().equals(MaintenanceTask.Type.precondition)) {
			logger.warning("Null capability for " + t.toString());
		}

		/*
		 * If we get a 'true' result, it means the intention has been modified.
		 * Ergo we need to consider stuff like dropping dependencies (forming
		 * new ones should happen anyway), messaging obligation confidence
		 * updates, etc
		 */
		if (t.getType().equals(MaintenanceTask.Type.precondition)) {
			boolean handledPre = handlePre(t);
			StatsRecorder.getInstance().recordMaintResult(getId(), handledPre);
			if (handledPre) { // scheduled PM task
				logger.severe(t + " succeeded!\n" + getTS());
				StatsRecorder.getInstance().recordString(dateFormat.format(new Date()) + ":" + getId() + " handled "
						+ t.getType() + " task " + t.getMaintainedActionStart().getBodyTerm() + " result = " + true);
				return true;
			} else {
				/*
				 * If we can't establish preconditions, we then try and form a
				 * new action that achieves the goal in some other manner.
				 */
				logger.severe(t + " failed; Adding equivalent effects maintenance task");
				StatsRecorder.getInstance().recordString(
						dateFormat.format(new Date()) + ":" + getId() + " NOT handled " + t.getType() + " task "
								+ t.getMaintainedActionStart().getBodyTerm() + ", trying effects equivalent");

				// special case type, for handling purposes
				t = new MaintenanceTask(MaintenanceTask.Type.effects_for_pre, t.getIntention(), t.getIm(),
						t.getMaintainedActionStart(), t.getMaintainedActionStart(), t.getContext(), 0,
						getMaintenancePolicy(maintAct), t.getCapability());
			}

		}

		// handle effects mt; may originate from failed Precondition maint. task
		if (t.getType().equals(MaintenanceTask.Type.effects) || t.getType().equals(MaintenanceTask.Type.effects_for_pre)
				|| t.getType().equals(MaintenanceTask.Type.replan)) {
			boolean result = handleEff(t);
			logger.severe("Succeeded?=" + result + " for " + t + "\n" + getTS());
			StatsRecorder.getInstance().recordMaintResult(getId(), result);
			StatsRecorder.getInstance().recordString(dateFormat.format(new Date()) + ":" + getId() + " handled "
					+ t.getType() + " task " + t.getMaintainedActionStart().getBodyTerm() + " result = " + result);
			return result;
		}

		return false; // failure case - not handled
	}

	/** NOT CURRENTLY IMPLEMENTED. */
	private boolean handleDrop(MaintenanceTask t) {
		return false;
	}

	/**
	 * Handles precondition maintenance by inserting a new plan to enable
	 * precondition states, at the current IM and preceding the specified
	 * action. </br>
	 * </br>
	 * To keep indexes accurate (and ordered) in the original IM, replace the
	 * offending step with a subgoal addition, leading into a new plan (for this
	 * intention execution only) which performs restorative actions and then the
	 * original, source literal (keep annotations the same here, to preserve
	 * existing obligation contracts)
	 * 
	 * @param t
	 * @return boolean true if intention successfully updated
	 */
	private boolean handlePre(final MaintenanceTask t) {
		// (new code for lpg usage)
		final Intention intention = t.getIntention();
		final Literal topTrigger = agentUtils().intentionIntoIMsQueue(intention).peekLast().getTrigger().getLiteral();
		final Literal action = (Literal) t.getMaintainedActionStart().getBodyTerm();
		final Literal testAction = ASSyntax.createLiteral("test", ASSyntax.createString("testArgument"));
		((MultiagentArch) getTS().getUserAgArch()).setActionAnnotations(getId(), intention.getId(), topTrigger,
				testAction);

		final Capability c = findActionCapability(t.getMaintainedActionStart(), t.getContext());
		BeliefBase initState = t.getContext().clone();

		// create capability sets
		final Collection<Capability> myCaps = new Vector<Capability>();
		final Collection<ExternalCapability> extCaps = new Vector<ExternalCapability>();
		setupPlanningOperatorCapabilities(topTrigger, myCaps, extCaps, initState);// edits-by-reference...

		// NB: paddler handles ternary/closed world assumption (if not
		// explicitly true, regard as false)
		final float confidenceConstraint = getMaintenancePolicy(action).maintenanceAcceptanceThreshold();
		String[] preconds = !c.isExternal() ? c.getPreconditionsByConfConstraint(confidenceConstraint)
				: getAllEcPreconds(action, confidenceConstraint);

		final long start = System.currentTimeMillis();

		// NB: no disjunction, so run preconds.length sequential tasks...
		float conf = 0f;
		Plan selected = null;
		List<List<Literal>> preSet = new Vector<List<Literal>>();
		for (String goalFormula : preconds) {
			logger.finer("Extracting goal state from " + goalFormula);
			List<Literal> goal = new Vector<Literal>();
			String[] pre = goalFormula.split("&");
			for (String cdtn : pre) {
				cdtn = cdtn.trim();
				// tidy up brackets, ternary negation
				if (cdtn.startsWith("(")) {
					cdtn = cdtn.substring(1);
				}
				while (cdtn.endsWith("))")) {
					cdtn = cdtn.substring(0, cdtn.length() - 1);
				}
				// 2 possible cases of ternary negation...
				cdtn = cdtn.startsWith("not (") ? cdtn.replaceFirst("not \\(", "~")
						: ((cdtn.startsWith("not ")) ? cdtn.replaceFirst("not ", "~") : cdtn);
				final Literal goalL = Literal.parseLiteral(cdtn);
				goalL.apply(formUnifierFromArgs(c.getSignature(), action));
				goal.add(goalL);
			}
			preSet.add(goal);
		}

		heartbeat("PM Plan, pre(1)");
		final PlanResult pr = planner.getPlan(getId() + "_pm_mt" + maintCount, action, initState.clone(), preSet,
				myCaps, extCaps, getMaintenancePolicy(action).maintenanceAcceptanceThreshold());
		selected = pr.getPlan();
		heartbeat("PM Plan, pre(2)");

		StatsRecorder.getInstance().recordPlanningResult(getId(), pr);

		logger.info("Planning completed in " + (System.currentTimeMillis() - start) + "ms elapsed");

		if (selected != null) {
			logger.severe("Trying conf check for " + selected.toASString());
			int cost = calculatePlanCost(selected, initState);
			if (cost > pmMaxCost) {
				logger.severe("Cost " + cost + " > " + pmMaxCost + "!");
				selected = null;
			}
		}

		if (selected == null) {
			logger.warning("PM: no suitable plan was found for task " + t.toString());
			if (UniversalConstants.DEBUG) {
				JOptionPane.showMessageDialog(getUi(),
						new String[] { "No enabling plan found",
								"for task " + t.getMaintainedActionStart().getBodyTerm() },
						"Precondition maintenance; " + getId(), JOptionPane.WARNING_MESSAGE);
			}
			return false;
		} else {
			// set indexes in the new plan based on the values of indexes in the
			// old/current one
			// (remember, indexes are to uid an action within the plan - not
			// identify the position)
			int indexOffset = findHighestIndexValue(t.getIm().getPlan());
			agentUtils().setPlanActionIndexAnnots(selected, indexOffset);

			logger.info("PM: Plan was found for task " + t.toString() + ";\n\t" + selected.toASString()
					+ "\n\tconfidence = " + conf);

			// set the annotations for the new plan
			PlanBody pb = selected.getBody();
			while (pb != null && !topTrigger.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT).isEmpty()
					&& !topTrigger.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT_INTENTION).isEmpty()) {
				((MultiagentArch) getTS().getUserAgArch()).setActionAnnotations(getId(), intention.getId(), topTrigger,
						(Literal) pb.getBodyTerm());
				pb = pb.getBodyNext();
			}

			// insertion!
			// first up; set this plan to lead into the maintained PB
			List<PlanBody> bodyList = agentUtils().planIntoBodyList(selected.getBody());
			bodyList.get(bodyList.size() - 1).setBodyNext(t.getMaintainedActionStart().clonePB());

			// ok. Now need to set the IM to execute the plan actions next
			logger.finer("PM: Updating task IM; " + t.getIm());
			PlanBody predecessor = null;

			// t.getIM.getcurrent will break if im is null, but IM should never
			// legally be null in this call...

			// find predecessor to maintained body
			if (t.getIm().getCurrentStep().getBodyTerm().equals(action)) {
				predecessor = null;
			} else {
				// iterate until we find...
				PlanBody predecessorCheck = t.getIm().getCurrentStep();
				prediterator: while (predecessor == null && predecessorCheck.getBodyNext() != null) {
					if (predecessorCheck.getBodyNext().getBodyTerm().equals(action)) {
						logger.finer("Found predecessor in im! " + t.getIm());
						predecessor = predecessorCheck;
						break prediterator;
					}
					predecessorCheck = predecessorCheck.getBodyNext();
				}
			}

			if (predecessor == null) {
				// if no predessor, replaces entire plan...
				t.getIm().insertAsNextStep(selected.getBody());
				t.getIm().removeCurrentStep();
				logger.info("Inserted new plan; " + predecessor + " as start of IM...");
			} else {
				// insert as next step to predecessor
				predecessor.setBodyNext(selected.getBody());
				logger.info("Inserted new plan; " + predecessor + " (PM plan; " + selected + ")");
			}
			logger.info("PM: Inserted new enabling plan;\n\t" + selected.toASString() + "\nfor task "
					+ t.getMaintainedActionStart().getBodyTerm());
			return true;
		}
	}

	private String[] getAllEcPreconds(final Literal action, final float con) {
		final Collection<ExternalCapability> caps = getExtCapability(action);
		final String[] pre = new String[caps.size()];
		for (int i = 0; i < pre.length; i++) {
			pre[i] = ((Capability) caps.toArray()[i]).getPreconditionsByConfConstraint(con)[0];
		}
		return pre;

	}

	private boolean handleEff(MaintenanceTask t) {
		return handleEff(t, true);
	}

	/**
	 * Performs planning for effects maintenance; supercedes old code as we now
	 * have a far, far faster planning solution. </br>
	 * </br>
	 * Want to check IM levels in turn (abstracting 'up') until we find a
	 * satisfactory new plan.
	 * 
	 * @param t
	 * @param considerSub
	 *            - if true, consider directly replacing task literal with new
	 *            subplan. If false, skip this step
	 * @return true if inserted new plan succesfully
	 */
	private boolean handleEff(MaintenanceTask t, boolean considerSub) {
		// need to get all IMs above t.getIm; we'll try and solve each in turn,
		// abstracting until we
		// get a passable (confidence) result or run out of ims
		Intention intention = t.getIntention();
		LinkedList<IntendedMeans> ims = agentUtils().intentionIntoIMsQueue(intention);
		Literal topTrigger = ims.peekLast().getTrigger().getLiteral();

		Literal testAction = ASSyntax.createLiteral("test", ASSyntax.createString("testArgument"));// i.e.
																									// is
																									// ground
		((MultiagentArch) getTS().getUserAgArch()).setActionAnnotations(getId(), intention.getId(), topTrigger,
				testAction);

		logger.info("EM: handle task\n" + t);

		// strip everything more ground than t.getIms...
		IntendedMeans tim = t.getIm();
		while (!ims.peek().equals(tim)) {
			ims.remove();
		}
		logger.info("IMs for t=" + t + " are;\n" + ims);

		final boolean isReplanning = (isReplanning() && t.getType().equals(MaintenanceTask.Type.replan));
		final float maintenanceAcceptanceThreshold = isReplanning ? 0
				: getMaintenancePolicy(topTrigger).maintenanceAcceptanceThreshold();
		logger.info("MODES: " + (isReplanning() ? "Replanning " : " ") + "/" + (isMaintaining() ? "Maintaining " : ""));

		float drop = getMaintenancePolicy(topTrigger).dropConfidenceThreshold();
		logger.info("EM: Confidence threshold for top level trigger " + topTrigger + " maint="
				+ maintenanceAcceptanceThreshold + ", drop=" + drop + "\nIntention;\n" + tim);

		Plan plan = null;
		BeliefBase init = t.getContext().clone();
		logger.info("Got planner, context setup");

		/*
		 * Two approaches taken for the first step... 1) if there are
		 * dependencies formed in the plan, we try and replace the action 2) if
		 * not, we replan from the point of the action
		 */
		// current step; executing next!
		boolean isFirst = t.getMaintainedActionStart().equals(t.getIm().getCurrentStep());
		// true if we have dependencies for this or later activity
		boolean hasDependencies = taskHasFollowingDependencies(t);// end
																	// iteration...

		Literal maintainedBody = (Literal) t.getMaintainedActionStart().getBodyTerm();
		logger.finer("EM: " + maintainedBody + " has dependencies?= " + hasDependencies + "\n\t" + tim);

		Collection<Capability> myCaps = new Vector<Capability>();
		Collection<ExternalCapability> extCaps = new Vector<ExternalCapability>();

		if (t.getType().equals(Type.effects)) {
			setupPlanningOperatorCapabilities(maintainedBody, testAction, myCaps, extCaps, init);
		} else if (t.getType().equals(Type.effects_for_pre)) {
			setupPlanningOperatorCapabilities(testAction, myCaps, extCaps, init);
		} else if (t.getType().equals(Type.replan)) {
			if (MultiagentArch.isJointActivity(maintainedBody, this)) {
				setupPlanningOperatorCapabilities(maintainedBody, testAction, myCaps, extCaps, init);
			} else {
				setupPlanningOperatorCapabilities(testAction, myCaps, extCaps, init);
			}
		} else if (t.getType().equals(Type.continual)) {
			setupPlanningOperatorCapabilities(testAction, myCaps, extCaps, init);
		} else {
			throw new RuntimeException("WTF is this argument type? " + t);
		}
		logger.finer("Setup PCs for " + t.getType());

		logger.info("Capabilities assembled, preparing to plan for " + t.getType() + " " + maintainedBody);
		if (hasDependencies && considerSub) {
			try {
				logger.finer("EM: task has dependencies (" + hasDependencies + "), so trying substitution");
				List<Literal> goal = getGoalLiteralsFromCapability(t.getMaintainedActionStart(), init);

				// try find plan
				heartbeat("EM Plan(1); pre");
				List<List<Literal>> goalSet = new Vector<List<Literal>>();
				goalSet.add(goal);

				PlanResult pr = planner.getPlan(getId() + "_" + t.getType() + maintCount, maintainedBody, init, goalSet,
						myCaps, extCaps, maintenanceAcceptanceThreshold);
				plan = pr.getPlan();
				heartbeat("EM Plan(1); post");

				StatsRecorder.getInstance().recordPlanningResult(getId(), pr);

				if (plan != null) {
					// check acceptability and schedule if this is ok
					float conf = 0;
					float cost = 0;
					if (t.getType().equals(Type.effects_for_pre)) {
						// only check first activity
						Plan pFirst = plan.cloneOnlyBody();
						pFirst.getBody().setBodyNext(null);
						conf = calculatePlanConfidence(pFirst, init);
						cost = calculatePlanCost(pFirst, init);
						logger.severe(
								"Only checking conf of first activity " + pFirst + "; " + conf + ", cost " + cost);
					} else {
						conf = calculatePlanConfidence(plan, init);
					}
					logger.info("EM: (1) got plan for " + maintainedBody + ": \n\t" + plan.toASString()
							+ "\nConfidence = " + conf + ", old plan conf=" + t.getConf() + ", maint="
							+ maintenanceAcceptanceThreshold + ", drop threshold = " + drop);

					// special check for replan; avoid plans which simply repeat
					// the failed external activity (to handle duplicate
					// contract formation)
					if (t.getType().equals(Type.replan)) {
						Literal oldA = ((Literal) t.getMaintainedActionStart().getBodyTerm()).copy();
						oldA.clearAnnots();
						Literal newA = ((Literal) plan.getBody().getBodyTerm()).copy();
						newA.clearAnnots();
						if (MultiagentArch.isJointActivity(newA, this)
								&& (!plan.getBody().getBodyType().equals(BodyType.achieve))
								&& oldA.equalsAsStructure(newA)) {
							logger.severe("New plan " + plan + " consists of activity equal to failed " + oldA);
							plan = null;
						}
					}

					if (conf <= maintenanceAcceptanceThreshold && maintenanceAcceptanceThreshold > 0
							&& cost > emMaxCost) {
						logger.warning("EM: rejected " + maintainedBody + " plan due to low confidence... " + conf
								+ " is below trigger " + maintenanceAcceptanceThreshold);
						plan = null;
					} else {
						// schedule and return - we do this a bit differently
						// because we're replacing an a
						// action with a sequence, rather than pushing onto the
						// IMs stack
						int indexOffset = findHighestIndexValue(tim.getPlan());
						agentUtils().setPlanActionIndexAnnots(plan, indexOffset);

						boolean check = insertReplacementPlan(t, plan, true);
						if (check) {
							logger.info("EM: succesfully inserted replacement plan (conf=" + conf + ") for "
									+ maintainedBody + "\n\t" + plan.toASString());
							return true;
						} else {
							logger.info("EM: unable to insert replacement plan for " + maintainedBody + "\n\t"
									+ plan.toASString());
							plan = null;
						}
					}
				} else {
					logger.severe("EM: No plan for " + t);
				}
			} catch (ParseException e) {
				String st = "EM: Exception in action plan formation " + e.toString();
				for (StackTraceElement s : e.getStackTrace()) {
					st = st + "\n\t" + s.toString();
				}
				logger.severe(st);
			}
		}

		// try and form plan from point-of-failure; need to iter to preceding of
		// action and insert
		// new plan as the successor (replacing action)
		// plan should be implictly null here - if we succeeded, we returned.
		// context is still the same, goal is im goal
		if (!isFirst && taskHasPrecedingDependencies(t)) {
			Literal goalTrigger = tim.getTrigger().getLiteral();
			logger.finer("EM: trying to plan from point-of-failure " + maintainedBody + " for " + goalTrigger);
			List<Literal> goal = getGoalLiteralsFromGoalTask(goalTrigger);
			if (goal == null || goal.isEmpty()) {
				logger.finer("EM: could not get goal for " + goalTrigger + ", for im " + tim);
			} else {
				logger.finer("EM: goals for " + goalTrigger + "=" + goal);

				heartbeat("EM Plan(2); pre");
				List<List<Literal>> goalSet = new Vector<List<Literal>>();
				goalSet.add(goal);
				PlanResult pr = planner.getPlan(getId() + "_" + t.getType() + maintCount, goalTrigger, init, goalSet,
						myCaps, extCaps, maintenanceAcceptanceThreshold);
				plan = pr.getPlan();
				heartbeat("EM Plan(2); post");

				StatsRecorder.getInstance().recordPlanningResult(getId(), pr);

				if (plan != null) {
					float conf = calculatePlanConfidence(plan, init);
					logger.fine("EM: got plan for insertion over " + maintainedBody + ": \n\t" + plan.toASString()
							+ "\nConfidence = " + conf + ", maint=" + maintenanceAcceptanceThreshold + ", drop = "
							+ drop + ", old=" + t.getConf());

					// if not ok, set to null...
					if (conf <= maintenanceAcceptanceThreshold || conf <= t.getConf()) {
						logger.fine("EM: rejected " + maintainedBody + " plan due to low confidence... " + "conf="
								+ conf + ", old=" + t.getConf() + ", maint=" + maintenanceAcceptanceThreshold
								+ ", drop=" + drop);
						plan = null;
					}
					// insertion
					if (plan != null) {
						logger.fine("EM: accepted plan");
						int indexOffset = findHighestIndexValue(tim.getPlan());
						agentUtils().setPlanActionIndexAnnots(plan, indexOffset);

						boolean check = insertReplacementPlan(t, plan, false);
						if (check) {
							logger.info("EM: succesfully inserted replacement plan for " + maintainedBody + "\n\t"
									+ plan.toASString());
							return true;
						} else {
							logger.info("EM: unable to insert replacement plan for " + maintainedBody + "\n\t"
									+ plan.toASString());
							plan = null;
						}
					}
				}
			}
		}

		// if no plan found...
		// iterate through IM levels, trying to replace whole-plan
		logger.finer("EM: trying iteration through IMs...");
		init = getBB().clone(); // because we're now planning from 1st IM
								// action...

		// we can get the capability set here, and use for all further planning
		// tasks because the bb is the same
		myCaps.clear();
		extCaps.clear();

		if (t.getType().equals(Type.replan)) {
			// still excluding prev tried, as cap must have been exhausted...
			if (MultiagentArch.isJointActivity(maintainedBody, this)) {
				setupPlanningOperatorCapabilities(maintainedBody, testAction, myCaps, extCaps, init);
			} else {
				setupPlanningOperatorCapabilities(testAction, myCaps, extCaps, init);
			}
		} else if (t.getType().equals(Type.effects_for_pre) || t.getType().equals(Type.continual)) {
			// planning accounts for the preconditions...
			setupPlanningOperatorCapabilities(testAction, myCaps, extCaps, init);
			logger.finer("Setup PCs for " + t.getType());
		} else { // eff
			setupPlanningOperatorCapabilities(maintainedBody, testAction, myCaps, extCaps, init);
		}

		// for replanning only! Focus on top level intention replan ahead of
		// iterative/hierarchical repair
		if (t.getType().equals(Type.replan) || t.getType().equals(Type.continual)) {
			while (ims.size() > 1) {
				ims.remove();
			}
		}

		/* prevent pointless repeats */
		List<Literal> lastGoal = new Vector<Literal>();
		// look for a plan; stop when we run out of ims, or when return true is
		// hit (plan found and scheduled)
		while (!ims.isEmpty()) {
			IntendedMeans mostGround = ims.peek();
			ims.remove();
			final boolean lastIm = ims.isEmpty();

			Literal trigger = mostGround.getTrigger().getLiteral();
			logger.finer("EM: trying to find replacement for IM (" + trigger + ")\n\t" + mostGround);

			// capability must be local, and in this case we just use the goal
			// task
			List<Literal> goal = getGoalLiteralsFromGoalTask(trigger);
			/*
			 * If we are running the same goal as last time (which implicitly
			 * failed, or we'd have returned, we can just abort the planner call
			 * entirely
			 */
			logger.severe("Check for duplicate call; goal=" + goal + ", lastGoal=" + lastGoal);

			if (goal != null && !goal.isEmpty() && !goalsHaveEqualSet(lastGoal, goal)) {
				heartbeat("EM Plan (Ims) - pre");
				List<List<Literal>> goalSet = new Vector<List<Literal>>();
				goalSet.add(goal);
				PlanResult pr = planner.getPlan(getId() + "_" + t.getType() + maintCount, trigger, init, goalSet,
						myCaps, extCaps, maintenanceAcceptanceThreshold);
				plan = pr.getPlan();
				// update last goal info - avoids repetitition of planner calls
				// for the same goal set
				lastGoal.clear();
				for (Literal l : goal) {
					lastGoal.add(l.copy());
				}
				heartbeat("EM Plan (Ims) - post");

				StatsRecorder.getInstance().recordPlanningResult(getId(), pr);

				if (plan != null) {
					// check acceptability and schedule if this is ok
					logger.finer("EM: Calculating plan confidence...");
					float conf = calculatePlanConfidence(plan, init);
					logger.info("EM: (2) got plan for IM (" + trigger + "): \n\t" + plan.toASString()
							+ "\nConfidence = " + conf + "\nold plan conf=" + t.getConf() + ", maint="
							+ maintenanceAcceptanceThreshold + ", drop threshold = " + drop);

					if (((conf > maintenanceAcceptanceThreshold)
							|| (conf > t.getConf())) /* && (!lastIm) */) {
						logger.fine("EM: attempting to insert plan for IM (" + trigger + ")\n\tparent IM=" + mostGround
								+ "\n\tPlan=" + plan.toASString());
						int indexOffset = findHighestIndexValue(mostGround.getPlan());
						agentUtils().setPlanActionIndexAnnots(plan, indexOffset);

						boolean result = insertNewPlanForIm(t.getIntention(), mostGround, plan);
						if (result) {
							logger.info("EM: succesfully inserted new IM for " + trigger + "\n\t" + plan.toASString());
							return true;
						} else {
							logger.info("EM: unable to insert new IM for " + trigger + "\n\t" + plan.toASString());
							plan = null; // unacceptable!
						}
					} else {
						Collection<Literal> underList = getActivitiesUnderThreshold(plan.getBody(), init,
								maintenanceAcceptanceThreshold);
						logger.info("EM: rejected IM (" + trigger + ")\nConf=" + conf + ", maint="
								+ maintenanceAcceptanceThreshold + ", drop=" + drop + ", old=" + t.getConf()
								+ "\nUnderlist=" + underList + ", lastIM " + lastIm);
						contractRejectImmediatelyPriorToExec.addAll(underList);
						plan = null;
					}
				}
			}
			// if we've not progressed, to next iter...
			heartbeat("Post IM removal in EM");

			// hail mary code; we try and find a confidence UNCONSTRAINED plan,
			// if we'd fail regardless
			// no checks for cost here... this only applies if we have no
			// further ims to progress up through
			if (unconstrainedMaintenancePlanningCaseApplies(t, ims)) {
				logger.warning("Entering hail mary check for " + t);
				if (goal != null && !goal.isEmpty() && isFirst && !myCaps.isEmpty()) {
					logger.warning(
							"No plans found under confidence constraints, trying to find a generic lower conf replacement");
					heartbeat("Last gasp plan - pre");
					// reform caps without confidence constraint
					List<List<Literal>> goalSet = new Vector<List<Literal>>();
					goalSet.add(goal);
					PlanResult pr = planner.getPlan(getId() + "_hm_" + t.getType() + maintCount, trigger, init, goalSet,
							myCaps, extCaps, t.getConf());
					plan = pr.getPlan();
					heartbeat("Last gasp plan - post");
					StatsRecorder.getInstance().recordPlanningResult(getId(), pr);

					if (plan != null) {
						// check acceptability and schedule if this is ok
						// just using first non-internal activity, so try a
						// 1-action plan...
						Plan testPlan = plan.cloneOnlyBody();
						testPlan.getBody().setBodyNext(null);

						logger.finer("EM (3): Calculating plan confidence...");
						float conf = calculatePlanConfidence(testPlan, init);
						logger.fine("EM: (3) got plan for IM (" + trigger + "): \n\t" + plan.toASString()
								+ "\nConfidence = " + conf + ", old plan conf=" + t.getConf() + ", maint="
								+ maintenanceAcceptanceThreshold + ", drop threshold = " + drop);

						if (conf > t.getConf()) { // i.e. > 0
							logger.fine("EM: attempting to insert plan for IM (" + trigger + ")" + "\n\tparent IM="
									+ ims.peek() + "\n\tPlan=" + plan.toASString());
							int indexOffset = findHighestIndexValue(mostGround.getPlan());
							// use max here, just in case we duplicate...
							agentUtils().setPlanActionIndexAnnots(plan, indexOffset);
							if (insertNewPlanForIm(t.getIntention(), mostGround, plan)) {
								logger.info("EM (3): succesfully inserted new relaxed conf IM for " + trigger + "\n\t"
										+ plan.toASString());
								return true;
							} else {
								logger.info("EM (3): unable to insert new relaxed conf IM for " + trigger + "\n\t"
										+ plan.toASString());
								plan = null; // unacceptable!
							}
						}
					}
				}
			}

		} // end ims loop

		// no plans found or scheduled
		logger.severe("EM: no plans were found/scheduled for task\n\t" + t.toString());
		return false;
	}

	private boolean unconstrainedMaintenancePlanningCaseApplies(MaintenanceTask t, LinkedList<IntendedMeans> ims) {
		return (!t.getType().equals(Type.replan)) && allowLowConfPlansForUnhandledPMs
				&& ((t.getConf() == 0) || t.getType().equals(Type.effects_for_pre)) && (ims.peek() == null);
	}

	private boolean goalsHaveEqualSet(final List<Literal> lastGoal, final List<Literal> goal) {
		return (lastGoal.size() == goal.size() && lastGoal.containsAll(goal) && goal.containsAll(lastGoal));
	}

	private boolean taskHasPrecedingDependencies(final MaintenanceTask mt) {
		final PlanBody action = mt.getMaintainedActionStart();
		final LinkedList<IntendedMeans> ims = agentUtils().intentionIntoIMsQueue(mt.getIntention());
		while (!ims.isEmpty()) {
			final IntendedMeans im = ims.pop();
			final List<PlanBody> bodyList = agentUtils().planIntoBodyList(im.getCurrentStep());
			for (int i = 0; i < bodyList.size(); i++) {
				// return false if hit action with no prior return
				if (bodyList.get(i).equals(action)) {
					return false;
				}
				// exit on first dependency found before action
				if (getDependencies().contains(((Literal) bodyList.get(i).getBodyTerm()).toString())) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean taskHasFollowingDependencies(MaintenanceTask t) {
		boolean hasDependencies = false;
		PlanBody pb = t.getMaintainedActionEnd();
		checkForDependencies: while (pb != null) {
			Literal l = (Literal) pb.getBodyTerm();
			if (hasDependencyContract(l)) {
				hasDependencies = true;
				break checkForDependencies;
			}
			pb = pb.getBodyNext();
		}
		return hasDependencies;
	}

	/**
	 * Set when we are replanning a current activity
	 */
	private Literal replanActivity = null;

	public boolean obligantIsExecuting(Literal action) {
		if (isReplanning() && replanActivity != null && replanActivity.equalsAsStructure(action)) {
			return false;
		}
		return super.obligantIsExecuting(action);
	}

	private void setupPlanningOperatorCapabilities(Literal testAction, Collection<Capability> myCaps,
			Collection<ExternalCapability> extCaps, BeliefBase initState) {
		setupPlanningOperatorCapabilities(null, testAction, myCaps, extCaps, initState);
	}

	/**
	 * Takes the empty collections and initial state (by reference) and setups
	 * up the capability set to be passed into the planner module, plus updates
	 * the initial state with required identity entries. Does NOT filter by
	 * confidence at this stage, but does filter out external capabilities that
	 * are obligation constrained (i.e. where the holder would not be able to
	 * accept a request stemming from task, due to preexisting capabilities)
	 * 
	 * @param skipAction
	 *            - if NOT null, then this action is used to remove
	 *            corresponding caps - preventing reuse of the failed
	 *            capability, based on the activity and performer details This
	 *            only applies to <i>composites</i>, as these correspond to
	 *            plans with non-specific preconditions and where we can make
	 *            more generalized assumptions wrt the confidence of specific
	 *            activities.
	 * @param testAction
	 *            - a literal, content and functor irrelevant, which has
	 *            annotations identical to those which would be added to any
	 *            <b>scheduled</b> dependency.
	 * @param myCaps
	 * @param extCaps
	 * @param initState
	 * @param t
	 */
	private void setupPlanningOperatorCapabilities(final Literal skipAction, final Literal testAction,
			final Collection<Capability> myCaps, final Collection<ExternalCapability> extCaps,
			final BeliefBase initState) {
		myCaps.clear();
		logger.info("Get pcs; " + this.getPrimitiveCapabilities());
		for (Capability pc : getPrimitiveCapabilities().values()) {
			logger.severe("Get PC: " + pc);
			if (pc.getPostEffects().length > 0) {
				myCaps.add(pc); // i.e. don't add no-ops...
			}
		}

		/*
		 * We don't use composite as operators; this is because, generally
		 * speaking, the preconditions convey selection restraints ahead of
		 * validity criteria. Plan generation is also more likely to give an
		 * optimal result than re-use of existing (hand-written) plans, or - if
		 * not - simply reproduce them.
		 */

		/*
		 * add all external capabilities known; there can be two sources. One is
		 * unconstrained, the other only relevant to obligation holders. In the
		 * latter, only the dependant(s) upstream can (re)use the capability...
		 */
		extCaps.clear();

		Collection<ExternalCapability> ecs = getExtCapabilities();
		LinkedList<String> skipProviders = new LinkedList<String>();

		if (skipAction != null) {
			skipProviders = MultiagentArch.getInvolvedAgents(skipAction, this);
		}

		boolean allowEcs = true;
		boolean filterExistingProvidersOut = false;
		Vector<String> depSet = new Vector<String>();
		// depth check using topTrigger
		if (!testAction.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT).isEmpty()) {
			List<Term> annotD = ((Literal) testAction.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT).get(0).clone())
					.getTerms();
			allowEcs = (annotD.size() <= MAX_DEPTH_LIMIT);
			filterExistingProvidersOut = (annotD.size() > REUSE_DEPTH_LIMIT);
			if (filterExistingProvidersOut) {
				Iterator<Term> ads = annotD.iterator();
				while (ads.hasNext()) {
					String dep = ads.next().toString().replaceAll("\"", "");
					if (!depSet.contains(dep)) {
						depSet.add(dep);
					}
				}
				logger.severe("Filter out all ECS from " + depSet + " for planning of " + testAction
						+ " due to depth limit (" + annotD.size() + ">" + REUSE_DEPTH_LIMIT + ")");
			}
		} // end depth checks

		logger.severe("Form caps; skipform = " + contractRejectImmediatelyPriorToExec + ", skipAction = " + skipAction);

		// if we can, add ecs in....
		if (allowEcs) {
			for (final ExternalCapability ec : ecs) {
				// check signature...
				Literal sig = ec.getSignature();

				// check sig against skip list - don't add capabilities if it'd
				// mean calling the same obligant for the same task
				// this handles cases where the specific conf for the dep. is
				// less than the abstract estimate adertised and
				// used in the ECs
				// if FALSE we skip to provider; implicitly reject ECs from
				// operator set unless all checks here are passed
				// if no skip action OR ( (activity is not in skip set) AND
				// holder is not in provider skips)
				boolean keepEcInOperators = (skipAction == null) && ec.getGeneralConfidence(initState) > 0;
				if (skipAction != null) {
					keepEcInOperators = ec.isPrimitive();
					if (!ec.isPrimitive()) {
						keepEcInOperators = !(sig.getFunctor().equals(skipAction.getFunctor())
								&& skipProviders.contains(ec.getHolderName()));
						logger.severe("Keep non-prim ec? " + keepEcInOperators);
					}

				}
				logger.severe("Initial skip check for " + sig + " is " + keepEcInOperators + " (true=keepAction), "
						+ "skipProviders=" + skipProviders + ", skipA=" + skipAction + ", prim ec=" + ec.isPrimitive());

				if (!filterExistingProvidersOut && depSet.contains(ec.getHolderName())) {
					logger.severe("Filtering out due to recursion limit; " + ec);
					keepEcInOperators = false;
				}

				// check sig against current skipForm
				// don't skip prims - we assume advertised preconds are accurate
				if (keepEcInOperators & skipAction != null & !ec.isPrimitive()) {
					skipCheck: for (Literal skipped : contractRejectImmediatelyPriorToExec) {
						LinkedList<String> involved = MultiagentArch.getInvolvedAgents(skipped, this);
						logger.fine("Check skip EC " + ec.getSignature() + "(" + ec.getHolderName() + ") vs " + skipped
								+ ", involved ag=" + involved);
						if (sig.getFunctor().equals(skipped.getFunctor()) && involved.contains(ec.getHolderName())) {
							logger.severe("Don't add EC " + ec.getSignature() + "(" + ec.getHolderName()
									+ ") ->  failed in dep form; " + skipped);
							keepEcInOperators = false;
							break skipCheck;
						}
					}
				} // endif

				// check sig PEs >0 i.e. not a no-op...
				if (keepEcInOperators && ec.getPostEffects().length > 0) {
					// if no obligation constraints, no checking required
					if (sig.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT).isEmpty()
							&& sig.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT_INTENTION).isEmpty()
							&& !extCaps.contains(ec)) {
						logger.finer("No ob constraints, adding ec; signature " + sig + "(" + ec.getHolderName() + ")");
						extCaps.add(ec);
					} else {
						// can have multiple entries, i.e. listTerm of
						// [dependant(1,2), dependant(1,2,4)]
						ListTerm dAnnots = sig.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT);
						ListTerm iAnnots = sig.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT_INTENTION);
						Literal advertised = sig.copy();
						boolean wouldAccept = false;
						checkLoop: for (int i = 0; i < dAnnots.size(); i++) {
							advertised.clearAnnots();
							// assuming both are the same length...
							Literal da = (Literal) dAnnots.get(i);
							Literal ia = (Literal) iAnnots.get(i);
							advertised.addAnnots(da, ia);
							// similar to code for checking obligation handling
							// during contract formation...

							// if same plan, ok
							wouldAccept = agentUtils().isInSamePlan(testAction, advertised);
							if (wouldAccept) {
								break checkLoop;
							}

							// shares a common root
							wouldAccept = agentUtils().sharedParent(testAction, advertised);
							if (wouldAccept) {
								break checkLoop;
							}

							// tl is a sub/child of sigClone withing the task
							// tree
							wouldAccept = agentUtils().isSubChildOf(testAction, advertised);
							if (wouldAccept) {
								break checkLoop;
							}
						}
						if (wouldAccept) {
							logger.finer("holder of " + sig + " would accept " + testAction + "\n" + ec);

							// remove annots from sig
							ec.getSignature().clearAnnots();
							extCaps.add(ec);
						}
					}
				}
			}
		} else {
			logger.severe("Exceeded depth cap  (" + testAction + ">" + MAX_DEPTH_LIMIT + ", no ECs permitted");
		}
	}

	/**
	 * Submethod which inserts a plan as a direct replacement for the maintained
	 * task specified in t. Includes any obligation/dependency related messaging
	 * processes
	 * 
	 * @return true if successful
	 */
	private boolean insertReplacementPlan(MaintenanceTask t, Plan p, boolean keepFollowingActions) {
		if (!(t.getType().equals(Type.effects) || t.getType().equals(Type.effects_for_pre))) {
			logger.severe(t + "\nis not an EM / E4P task! - Not inserting new\n" + p);
			return false;
		}

		Literal action = (Literal) t.getMaintainedActionStart().getBodyTerm();

		// find highest index in current im plan
		int highestIndex = findHighestIndexValue(t.getIm().getPlan());

		// add in indexes, using offset based on highest im index.
		agentUtils().setPlanActionIndexAnnots(p, highestIndex);

		// phew. Can look at insertion now...
		Intention i = t.getIntention();
		IntendedMeans im = t.getIm();
		List<PlanBody> currentImPBs = agentUtils().planIntoBodyList(im.getCurrentStep());

		// get last step of selected, and set the body after t.maintainedAction
		// as the next step of it
		if (keepFollowingActions) {
			PlanBody pb = p.getBody();
			while (pb.getBodyNext() != null) {
				pb = pb.getBodyNext();
			}
			pb.setBodyNext(t.getMaintainedActionEnd().getBodyNext());
		}
		// if false, we don't retain actions...

		// get the action before t.maintainedAction, then replace t.ma with the
		// first step of selected
		if (currentImPBs.get(0).equals(t.getMaintainedActionStart())) {
			// first action, no preceding
			logger.finer("Next action will be executing the new plan " + p);
			logger.finer("Current I= " + i);
			im.getCurrentStep().setBodyNext(p.getBody());
			im.removeCurrentStep(); // remove deprecated, replace...
			logger.finer("Updated I= " + i);
		} else {
			// we need to search for the action before the maintained one, so we
			// can replace maintained with the
			// new plan...
			PlanBody preceding = null;
			searchLoop: for (PlanBody planbody : currentImPBs) {
				if (planbody.getBodyNext() != null && planbody.getBodyNext().equals(t.getMaintainedActionStart())) {
					logger.severe("Found preceding action");
					preceding = planbody; // set
					break searchLoop;
				}
			}

			if (preceding == null) {
				JOptionPane.showMessageDialog(getUi(), "Preceding not found!");
				logger.severe("Preceding for maintained task " + t.getMaintainedActionStart() + "\nnot found in\n"
						+ currentImPBs);
				return false;
			} else if (!keepFollowingActions) {
				// cancel dependencies
				// this includes ones after start and before end...
				PlanBody check = t.getMaintainedActionStart();
				logger.severe("Cancelling subsequent dependencies for " + check + " in im");
				while (check != null) {
					if (hasDependencyContract((Literal) check.getBodyTerm())) {
						logger.severe(
								"Cancel old dependency for " + getDependencyContract((Literal) check.getBodyTerm()));
						cancelDependency((Literal) check.getBodyTerm(), false);
					}
					check = check.getBodyNext();
				} // end while
			}
			preceding.setBodyNext(p.getBody());
			logger.fine("Updated intention by inserting the new plan;\n " + t.getIntention());
		}

		// done!

		logger.finer("Updated intention;\n" + t.getIntention());

		// implicit success, otherwise we would return false earlier...
		// cancel any dependency for maintained task
		if (hasDependencyContract(action)) {
			logger.fine("Cancel dependency for " + action + "; " + getDependencyContract(action));
			cancelDependency(action, false);
			// NB: subsequent formContract call will update the bb changes
		}

		if (!keepFollowingActions) {
			// try other ims in i
			Queue<IntendedMeans> imQ = agentUtils().intentionIntoIMsQueue(i);
			// need to 'cut' levels until t.getIm + 1
			while (!imQ.peek().equals(im)) {
				imQ.remove();
			}

			if (imQ.isEmpty()) {
				logger.severe("Unable to find im in " + i);
			} else if (imQ.size() > 1) {
				// check all ims below the maintained one
				while (!imQ.isEmpty()) {
					IntendedMeans current = imQ.remove();// get top
					PlanBody imPb = current.getCurrentStep();
					if (imPb != null) {
						imPb = imPb.getBodyNext();
					}
					while (imPb != null) {
						logger.severe("rem dep check; " + imPb);
						Literal ca = (Literal) imPb.getBodyTerm();
						if (hasDependencyContract(ca)) {
							logger.severe("Cancel sub-im dependency for " + ca + "; " + getDependencyContract(ca)
									+ " in " + current);
							cancelDependency(ca, false);
						}
						imPb = imPb.getBodyNext();
					} // end while
					logger.severe("Checked dependency removal for " + current + ", onto next in " + imQ);
				}
			}

		} // end if
		return true; // end
	}

	/**
	 * Takes a given plan and replace the existing one, for a given IM. note
	 * that this may involve further subgoal addition and refinement as the
	 * agent executes. Also performs messaging for dropping dependencies for the
	 * current IM, and informing obligants of the root task
	 * 
	 * @param im
	 *            - intended means; this represents the level for insertion. If
	 *            the first step is executing, then plan is set as the next
	 *            action
	 * @param p
	 *            - plan to insert as the new IM
	 * @return true if succeeded in inserting the new plan
	 */
	private boolean insertNewPlanForIm(Intention i, IntendedMeans im, Plan p) {
		/*
		 * TODO - need to test insertion when we have an executing
		 * dependency/action; don't want to remove existing whilst they execute.
		 * This really applies to dependencies; we have to receive (a) result to
		 * progress to the next action
		 */
		logger.info("Insert new plan for " + i.getId() + "; \ni=" + i + "\nim=" + im + "\np=" + p + "\nC="
				+ getTS().getC());

		// cancel dependencies
		PlanBody pb = im.getCurrentStep();
		while (pb != null) {
			Literal a = (Literal) pb.getBodyTerm();
			if (hasDependencyContract(a) && !isExecuting(a)) {
				logger.info("Cancelling dependency for " + a + " due to new plan insertion");
				cancelDependency(a, false);
			} else if (hasDependencyContract(a) && isExecuting(a)) {
				logger.info("dependency " + a + " is being executed!");
				cancelDependency(a, false);
			}
			pb = pb.getBodyNext();
		}

		boolean rmCurrent = true;
		// get list of ims 'below' im, in order to cancel any dependencies below
		// this; also remove the ims
		logger.info("Pre insertion intention = " + i);
		List<Literal> cancelList = new Vector<Literal>();
		while ((i.peek() != null) && (!i.peek().equals(im))) {
			final IntendedMeans popped = i.pop();
			logger.fine("Removing IM: " + popped);

			// remove dependencies
			PlanBody remove = popped.getCurrentStep();
			while (remove != null) {
				Literal activity = (Literal) remove.getBodyTerm();
				if (hasDependencyContract(activity)) {
					cancelList.add(activity);
				} else {
					logger.info("Don't cancel dependency; " + activity);
				}
				remove = remove.getBodyNext();
			}

			pb = popped.getCurrentStep();
			if (pb == null || pb.getBodyTerm() == null) {
				logger.warning("(1) Null 1st action " + pb);
				rmCurrent = false; // nothing to remove
			} else if (isExecuting((Literal) pb.getBodyTerm())) {
				logger.warning("(2) Executing 1st action " + pb);
				pb = pb.getBodyNext();
				rmCurrent = false;
			} else if (getTS().getC().getPendingActions().containsKey(i.getId()) && getTS().getC().getPendingActions()
					.get(i.getId()).getActionTerm().equalsAsStructure(im.getCurrentStep().getBodyTerm())) {
				logger.warning("(1) Executing action in PAs " + pb);
				pb = pb.getBodyNext();
				rmCurrent = false;
			}

		}

		logger.fine("Insert " + p.getBody() + "\nAs next step in " + im + "->" + rmCurrent);

		// remove current step dependencies
		PlanBody temp = im.getCurrentStep();
		while (temp != null) {
			Literal l = (Literal) temp.getBodyTerm();
			if (hasDependencyContract(l)) {
				cancelList.add(l);
			} else {
				logger.info("Not a dependency; " + l);
			}
			temp = temp.getBodyNext();
		}

		// removal call(s)
		if (!cancelList.isEmpty()) {
			logger.info("Cancel im dependencies; " + cancelList + "\nFrom " + getDependencies());
			for (Literal l : cancelList) {
				cancelDependency(l, false);
			}
		} else {
			logger.info("No dependencies to cancel for i!\n" + i + "\n" + getDependencies());
		}

		if (im.getCurrentStep() == null) {
			logger.severe("Note; inserting next step into null Im... need new im " + p + " in\n" + i);
			Option pop = new Option(p, new Unifier());
			IntendedMeans newIm = new IntendedMeans(pop, null);
			i.pop(); // remove top 'null' im
			i.push(newIm);
			logger.severe("Note; inserted " + newIm + "\n" + i);
		} else {
			im.insertAsNextStep(p.getBody());

			// if the current step is executing, don't remove...
			// only remove if NOT currently executing
			if (rmCurrent) {
				im.removeCurrentStep();
			}
		}
		logger.fine("Added " + p + "\nPost insertion intention = " + i);
		return true;
	}

	/**
	 * Gets goal literals from a capability, including sideeffects.
	 * 
	 * @param action
	 * @return list of literals - positive or negated
	 * @throws ParseException
	 */
	private List<Literal> getGoalLiteralsFromCapability(PlanBody action, BeliefBase executionContext)
			throws ParseException {
		Literal l = (Literal) action.getBodyTerm();
		Capability c = findActionCapability(action, executionContext);
		if (c == null || !l.isGround()) {
			logger.severe(((c == null) ? "Unable to find capability" : "unground ") + "for " + l);
			Capability c2 = findActionCapability(action, executionContext);
			throw new RuntimeException("SEVERE error!; search 2 is - " + c2);
		}

		Literal sig = c.getSignature();
		Unifier u = formUnifierFromArgs(sig, l);
		logger.finer("Unifier for " + action + "=" + u + " (sig=" + sig + ")");

		String[] effects = c.getPostEffects();
		List<Literal> goal = getGoalLiteralFromEffects(effects, u);
		// if no capabilitiy effects, then use the associated goalTask...
		if (goal.isEmpty()) {
			logger.severe("Empty goal set for " + action + "\nC was;" + c + "\nTrying to get from goal object "
					+ c.achieves());
			u = formUnifierFromArgs(c.achieves().getSignature(), l);
			logger.info("Set unifier for achieves; " + u + "(" + c.achieves().getSignature() + ", " + action + ")");
			goal = getGoalLiteralFromEffects(c.achieves().getGoalEffects(), u);
		}
		if (goal.isEmpty()) {
			logger.severe("Could not set ANY goal states for " + action);
		} else {
			logger.finer("Effects for " + action + "=" + goal);
		}

		return goal;
	}

	/**
	 * @param goal
	 * @param u
	 * @param effects
	 * @throws ParseException
	 */
	private List<Literal> getGoalLiteralFromEffects(String[] effects, Unifier u) throws ParseException {
		List<Literal> goal = new Vector<Literal>();
		for (String p : effects) {
			boolean negated = false;
			if (p.startsWith("+")) {
				p = p.substring(1);
				negated = p.startsWith("~");
			} else { // - -> add a negation
				p = p.substring(1);
				negated = !p.startsWith("~");
			}
			Literal condition = ASSyntax.parseLiteral(p);
			condition.apply(u);
			condition.setNegated(!negated);
			if (!condition.isGround()) {
				logger.severe("Unground goal literal; " + p + "- " + condition + "(" + u + ")");
			}
			goal.add(condition);
		}
		return goal;
	}

	/**
	 * Gets the goal task for an action and forms a set of goal literals for use
	 * in planning. Forms a unifier using the action literal and the capability
	 * signature; returns an empty set if cannot identify the appropriate
	 * capability for the action.
	 * 
	 * @param action
	 *            - the activity we want the goal condition for
	 * @return List of literals - positive or negated goal states
	 */
	private List<Literal> getGoalLiteralsFromGoalTask(Literal action) {
		List<Literal> goal = new Vector<Literal>();
		GoalTask task = getTaskFactory().factory(action.getFunctor());

		if (task == null) {
			logger.warning("Can't find a goal task for " + action);
			return null;// test for this at the callee point
		}

		Literal sig = task.getSignature();
		Unifier u = formUnifierFromArgs(sig, action);
		logger.finer("Unifier for " + action + "=" + u + " (sig=" + sig + ")");

		String[] taskEff = task.getGoalEffects();
		for (String p : taskEff) {
			boolean negated = false;
			if (p.startsWith("+")) {
				p = p.substring(1);
				negated = p.startsWith("~");
			} else { // - -> add a negation
				p = p.substring(1);
				negated = !p.startsWith("~");
			}

			// parse and add literal to goal states
			try {
				Literal condition = ASSyntax.parseLiteral(p);
				condition.apply(u);
				condition.setNegated(!negated);
				goal.add(condition);
			} catch (ParseException e) {
				String st = "Parse problem; goal extraction for " + p + "\n" + e.toString();
				for (StackTraceElement s : e.getStackTrace()) {
					st = st + "\n\t" + s.toString();
				}
				logger.severe(st);
				return null;
			}
		}

		logger.finer("Goal for " + action + "=" + goal);
		return goal;
	}

	/**
	 * Finds the highest index annotation in the passed in plan
	 * 
	 * @param plan
	 * @return
	 */
	private int findHighestIndexValue(Plan plan) {
		PlanBody imStart = plan.getBody();
		int highestAnnotValue = 0;
		while (imStart != null) {
			if (imStart.getBodyTerm().isLiteral()) {
				Literal l = (Literal) imStart.getBodyTerm();
				if (!l.getAnnots(UniversalConstants.ANNOTATION_ID_IN_PLAN).isEmpty()) {
					int annot = (int) ((NumberTerm) ((Literal) l.getAnnots(UniversalConstants.ANNOTATION_ID_IN_PLAN)
							.get(0)).getTerm(0)).solve();
					if (annot > highestAnnotValue) {
						highestAnnotValue = annot;
					}
				}
			}
			imStart = imStart.getBodyNext();
		}

		return highestAnnotValue;
	}

	/**
	 * Failure handling override... forces a replan attempt for the current im
	 * 
	 * @param ae
	 */
	@Override
	protected void failedDependency(ActionExec ae) {
		// in case dependency was cancelled and replaced through maintenance
		if (!hasDependencyContract((Literal) ae.getActionTerm())) {
			logger.severe("Failed dependency (" + ae.getActionTerm()
					+ ") does not correspond to any existing contract;\n" + getDependencies() + "\nIntention="
					+ ae.getIntention() + "\nCircumstance=" + getTS().getC());
			ae.setResult(true); // so we can ignore it and progress...
			// remove pa?
			return;
		}

		logger.severe("Failed dependency (" + ae.getActionTerm() + ")");
		boolean replanResult = false;
		if (isReplanning()) {
			replanActivity = ae.getActionTerm();
			replanResult = replanForCurrentStep(ae.getIntention(), true);
			replanActivity = null;
		}

		if (replanResult) {
			logger.severe("Successful replan for failed " + ae + "!\n" + getTS().getC());
			getWaitResults().put(ae, true); // fool unsuspension...
			ae.setResult(true);
		} else {
			// if not replanning, then we fail the entire intention for ae...
			// note that this will break functionality if ae itself has been
			// cancelled as a
			// dependency... ergo the check above.
			logger.severe("Failed/Didn't replan failed " + ae.getActionTerm() + "\n" + ae.getIntention()
					+ "(replanning=" + isReplanning() + ", replan result=" + replanResult);
			LinkedList<IntendedMeans> im = agentUtils().intentionIntoIMsQueue(ae.getIntention());
			// pop top and end
			IntendedMeans imF = im.pollFirst();
			logger.severe("Pop first; " + imF);
			// IntendedMeans imL = im.pollLast();
			// logger.severe("Pop last; " + imL);
			Iterator<IntendedMeans> it = im.iterator();
			while (it.hasNext()) {
				IntendedMeans curr = it.next();
				logger.severe("Failed trigger; " + curr.getTrigger().getLiteral());
				if (!curr.getTrigger().equalsAsStructure(ae.getActionTerm())) {
					super.recordFailure(curr.getTrigger().getLiteral());
				}
			}
			recordFailure(ae.getActionTerm());
		}
	}

	/**
	 * Attemtps to form a new plan when the current step - im.peek.getCurrent -
	 * has failed
	 * 
	 * @param intention
	 * @return true if found a new plan and inserted it
	 */
	public boolean replanForCurrentStep(final Intention intention, final boolean failed) {
		IntendedMeans top = agentUtils().intentionIntoIMsQueue(intention).getLast();
		boolean replanOk = true;
		int replanCount = 0;
		if (hasReplannedAlready(top)) {
			final Literal repLit = (Literal) top.getTrigger().getLiteral().getAnnots(UniversalConstants.ANNOT_REPLANNED)
					.get(0);
			replanCount = (int) ((NumberTerm) repLit.getTerm(0)).solve();
			if (replanCount > MaintainingAgent.REPLAN_CAP && failed) {
				logger.severe("Replan cap met for trigger parent " + top.getTrigger());
				replanOk = false;
			}
		}
		logger.severe("Replan count " + replanCount + " for trigger " + top.getTrigger());

		if (!replanOk) {
			logger.severe("Abort; Too many replans! " + top.getTrigger());
			return false;
		}

		IntendedMeans im = intention.peek();
		PlanBody body = im.getCurrentStep();
		logger.info("Buffering update for failed action " + body);
		List<Literal> perceive = getTS().getUserAgArch().perceive();
		if (perceive != null) {
			buf(perceive);
		}
		BeliefBase bbClone;
		synchronized (bb) {
			bbClone = getBB().clone();
		}

		// add in tell messages from other agents
		Queue<Message> mb = this.getTS().getC().getMailBox();
		logger.fine("Messages received (replan)=" + mb);
		for (Message m : mb) {
			if (m.getIlForce().equals("tell")) {
				logger.info("Manual 'tell' update - add " + m.getPropCont());
				bbClone.add((Literal) m.getPropCont());
			} else if (m.getIlForce().equals("untell")) {
				logger.info("Manual 'untell' update - rem " + m.getPropCont());
				bbClone.remove((Literal) m.getPropCont());
			}
		}

		// get capability...
		Capability c = null;
		final Literal bodyTerm = ((Literal) body.getBodyTerm()).copy();
		if (!bodyTerm.isGround() && im.getUnif() != null) {
			bodyTerm.apply(im.getUnif());
		}

		if (hasCapability(body)) {
			c = getCapability(body);
		} else if (hasDependencyContract(bodyTerm)) {
			c = getDependencyContract(bodyTerm).getExternalCapability();
		} else if (knowsProviderForExtCapability((bodyTerm).getFunctor())) {
			c = getBestFitExtCapability(bbClone, bodyTerm);
		} else {
			logger.severe("Unable to find capability for replanning the action " + bodyTerm);
			return false;
		}
		logger.info("Get capability for " + bodyTerm + "=" + c);
		maintCount++; // counter...
		logger.info("Buffered and updated for failed action " + body);
		MaintenancePolicy policyInfo = getMaintenancePolicy(bodyTerm);
		final MaintenanceTask.Type type = failed
				? (planConfConst() ? MaintenanceTask.Type.replan_conf_const : MaintenanceTask.Type.replan)
				: MaintenanceTask.Type.continual;

		MaintenanceTask efT = new MaintenanceTask(type, intention, im, body, body, bbClone, 0, policyInfo, c);
		logger.info("Created " + type.name() + " task for failed action " + body);
		boolean result = handleEff(efT, false);

		if (!result && type == MaintenanceTask.Type.replan_conf_const) {
			efT = new MaintenanceTask(MaintenanceTask.Type.replan, intention, im, body, body, bbClone, 0, policyInfo,
					c);
			logger.info("Created retry (conf relaxed) replan task for failed action " + body);
			result = handleEff(efT, false);
		}

		if (result) {
			// do annots!
			Literal newAnnot = ASSyntax.createLiteral(UniversalConstants.ANNOT_REPLANNED,
					ASSyntax.createNumber(++replanCount));
			// set annots
			if (hasReplannedAlready(top)) {
				Literal repLit = (Literal) top.getTrigger().getLiteral().getAnnots(UniversalConstants.ANNOT_REPLANNED)
						.get(0);
				top.getTrigger().getLiteral().delAnnot(repLit);
			}
			top.getTrigger().getLiteral().addAnnot(newAnnot);
			logger.severe("EM based Replan suceeded for " + bodyTerm + "\nUpdated intention = \n" + intention);
		}
		return result;
	}

	private boolean hasReplannedAlready(IntendedMeans top) {
		return !top.getTrigger().getLiteral().getAnnots(UniversalConstants.ANNOT_REPLANNED).isEmpty();
	}

	/**
	 * Avoid infinite looping on maintenance planning
	 */
	private Intention lastMaint = null;

	/**
	 * Extends intention contract formation code to perform maintenance for
	 * unfinished intentions, when 'maintain' variable is set to true. Formed
	 * such that maintenance related changes are made before contract formation.
	 */
	@Override
	protected void moveToNextIntentionAction(final Intention i) {
		logger.info("Maintenance call for\n" + i + "\nlast call was\n" + lastMaint);
		if (maintain && !i.isFinished() && !i.isSuspended()) {
			final PlanBody currentStep = i.peek().getCurrentStep();
			if (intentionsEqual(lastMaint, i)) {
				logger.severe("Not repeating maintain call for intention" + "\n\t" + i.peek() + "\nLF=" + "\n\t"
						+ lastMaint.peek());
			} else {
				final Literal trigger = agentUtils().intentionIntoIMsQueue(i).getLast().getTrigger().getLiteral();
				logger.severe("maintain call for intention\n" + i + "\nTrigger=" + trigger + ", ob? "
						+ hasObligationContract(trigger) + "\nCurrentStep = " + currentStep + "\nLast maint = "
						+ lastMaint);
				StatsRecorder.getInstance().recordString(
						dateFormat.format(new Date()) + ":" + getId() + " start maintaining intention " + i.getId());
				boolean result = maintain(i, false);
				lastMaint = i.clone();
				StatsRecorder.getInstance().recordString(
						dateFormat.format(new Date()) + ":" + getId() + " finish maintaining intention " + i.getId());

				logger.severe("Finished maintenance of " + i.getId());

				if (result) {
					logger.severe(getId() + " modified intention " + i.getId() + " " + trigger + " ob?"
							+ hasObligationContract(trigger));
					StatsRecorder.getInstance().recordString(
							dateFormat.format(new Date()) + ":" + getId() + " modified intention " + i.getId());

					// we check for existing entries in the intention and allow
					// contract formation to re-attempt
					logger.severe("Next activit mt passed; Clearing " + contractRejectImmediatelyPriorToExec);
					contractRejectImmediatelyPriorToExec.clear();
				}

				/*
				 * If the confidence is below the trigger value, then we have to
				 * wait for our parent agent to check it is ok before we can
				 * proceed; this is to stop the dependant assuming a particular
				 * state, where said state is violated by the immediate
				 * execution of activities after this call...
				 * 
				 * //IF is an obligation //AND current step (next to exec) is
				 * not internal OR null OR an abstract subgoal //AND confidence
				 * is below mp threshold
				 */
				if (hasObligationContract(trigger) & i.peek() != null && currentStep != null) {
					final boolean confidenceUnderThreshold = getObligationContract(trigger).getExternalCapability()
							.getSpecificConfidence(bb, trigger) < getObligationContract(trigger).getMp()
									.maintenanceAcceptanceThreshold();

					if (!currentStep.getBodyTerm().isInternalAction()
							&& !currentStep.getBodyType().equals(BodyType.achieve)
							&& !currentStep.getBodyType().equals(BodyType.achieveNF) && confidenceUnderThreshold) {
						logger.severe("3) Post maintenance; setting as pending; " + trigger + " " + i.getId() + "\n" + i);
						StatsRecorder.getInstance().recordString(
								dateFormat.format(new Date()) + ":(post)Pending int for " + trigger + ":" + i.getId());
						getTS().getC().removeIntention(i);
						scheduleAsPendingIntention(trigger, i);// wait for reply
						sendPostMaintUpdateToDependant(i, result);
					}
				}
			}
		}
		super.moveToNextIntentionAction(i);
	}

	/**
	 * Checks whether i1 and i2 are the same, because stupid jason equals only
	 * checks id, not content
	 */
	protected boolean intentionsEqual(Intention i1, Intention i2) {
		// simple checks first
		if (i1 == null && i2 == null) {
			return true;
		}
		if ((i1 != null && i2 == null) || (i1 == null && i2 != null)) {
			return false;
		}

		if (i1.peek() == null && i2.peek() == null) {
			return true;
		}
		if ((i1.peek() != null && i2.peek() == null) || (i1.peek() == null && i2.peek() != null)) {
			return false;
		}
		final IntendedMeans im1 = (IntendedMeans) agentUtils().intentionIntoIMsQueue(i1.clone()).peek().clone();
		final IntendedMeans im2 = (IntendedMeans) agentUtils().intentionIntoIMsQueue(i2.clone()).peek().clone();
		if (im1.getCurrentStep() == null && im2.getCurrentStep() == null) {
			return true;
		}
		if ((im1.getCurrentStep() != null && im2.getCurrentStep() == null)
				|| (im1.getCurrentStep() == null && im2.getCurrentStep() != null)) {
			return false;
		}

		final PlanBody cs1 = im1.getCurrentStep().clonePB();
		final PlanBody cs2 = im2.getCurrentStep().clonePB();

		final List<PlanBody> bodySet1 = agentUtils().planIntoBodyList(cs1, false);
		final List<PlanBody> bodySet2 = agentUtils().planIntoBodyList(cs2, false);

		if (bodySet1.size() != bodySet2.size()) {
			return false;
		}

		// else, compare term by term
		for (int i = 0; i < bodySet1.size(); i++) {
			PlanBody bd1 = bodySet1.get(i);
			PlanBody bd2 = bodySet2.get(i);
			Literal bdt1 = (Literal) bd1.getBodyTerm();
			bdt1.setAnnots(new ListTermImpl());
			Literal bdt2 = (Literal) bd2.getBodyTerm();
			bdt2.setAnnots(new ListTermImpl());
			if (!bdt2.toString().equals(bdt1.toString())) {
				return false;
			}
		}
		return true;
	}

	protected Collection<? extends ExternalCapability> getEcsForCompositeCapabilities(final CompositeCapability cc) {
		if (planner.getPlanningMode().equals(Mode.speed)) {
			return cc.getConfidenceBasedPreconditionEcs(getBB(),
					getMaintenancePolicy(cc.getSignature().getFunctor()).maintenanceAcceptanceThreshold());
		} else {
			return cc.getConfidenceBasedPreconditionEcs(getBB());
		}
	}

	protected Collection<ExternalCapability> getEcsForPrimitiveCapabilities(final Capability c) {
		final Collection<ExternalCapability> set = new Vector<>();
		final Literal sig = c.getSignature();
		final GoalTask goal = c.achieves();
		final String[] eff = c.getPostEffects();
		final float cost = c.getGeneralCostEstimate();
		if (planner.getPlanningMode().equals(Mode.speed)) {
			final float confConstraint = getMaintenancePolicy(c.getSignature().getFunctor())
					.maintenanceAcceptanceThreshold();
			for (final String pre : c.getPreconditionsByConfConstraint(confConstraint)) {
				set.add(new ExternalCapability(sig, goal, this,
						new ConfidencePrecondition(getId(), c.getSignature(), pre, 1f), eff, cost, true));
			}
		} else {
			for (final ConfidencePrecondition pre : c.getConfidenceBasedPreconditions(getBB())) {
				set.add(new ExternalCapability(sig, goal, this, pre, eff, cost, true));
			}
		}
		return set;
	}
}
