package agent.type;

import java.util.Map;

import jason.asSemantics.Circumstance;
import jason.asSemantics.Intention;
import truckworld.env.Cargoworld;

public class AgentHeartbeat extends Thread {

	private static final long TIMEOUT = Cargoworld.INACTIVITY_TIMEOUT_PERIOD/2;
	private final GoalScoringAgent watched;
	private boolean alive=false;
	public AgentHeartbeat(final GoalScoringAgent agent) {
		this.watched = agent;
	}
	
	@Override
	public void start() {
		super.start();
		this.alive = true;
	}
	
	public void kill() {
		this.alive = false;
	}
	
	@Override
	public void run() {
		while(alive) {
			final Circumstance current = watched.getTS().getC();
			if(current.getSelectedIntention()!=null) {
				watched.heartbeat("Active intention\n" + current.getSelectedIntention());
			}
			else if (!wrapIntoString(current).isEmpty()){
				watched.heartbeat("Pending intentions exist" + wrapIntoString(current));
			}
			try {
				sleep(TIMEOUT);
			} catch (final InterruptedException e) {}
		}
	}

	private String wrapIntoString(final Circumstance current) {
		final Map<String, Intention> pis = current.getPendingIntentions();
		String piString = "";
		for(final String key: pis.keySet()) {
			piString = piString + "\n" + key + "=" + pis.get(key);
		}
		return piString;
	}

}
