package agent.type;

public class MessageTypes {
	public static final String IL_OB_SUCCEED = "succeed";
	public static final String IL_OB_FAILED = "fail";
	public static final String IL_DEPENDENCY_REQ = "dependencyReq";
	public static final String IL_DEPENDENCY_CANCEL = "dependencyCancel";
	public static final String IL_OBLIGANT_REFUSE = "obligantRefuse";
	public static final String IL_OBLIGANT_ACCEPT = "obligantAccept";
	public static final String IL_CONFIRM_OBL_CONTRACT = "confirmContractWithObligant";

	/**
	 * The set of BB changes provided by the dependant, to the obligant, have
	 * changed
	 */
	public static final String IL_CONTRACT_CHANGED = "updatedContract";

	/**
	 * For transmitting confidence of a specific obligation post-maintenance,
	 * regardless of result
	 */
	protected static final String IL_OB_MAINTAINED = "obligationMaintained";
	protected static final String IL_DEP_MAINTAINED = "dependencyMaintained";
}
