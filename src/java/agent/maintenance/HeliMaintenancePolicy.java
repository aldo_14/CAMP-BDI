package agent.maintenance;

/**
 * 
 * @author alanwhite
 *
 *Helicopter policy with added drop conditions
 */
public class HeliMaintenancePolicy extends DefaultMaintenancePolicy {

	public HeliMaintenancePolicy()	{
		super();
		dropConditions.add("windy");
	}
}
