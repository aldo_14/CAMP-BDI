package agent.maintenance;


/**
 * @author Alan White
 *
 * Default implementation of a maintenance policy object; parse xml and fill in the fields of this.
 * Otherwise, fields are left as defaults.
 */
public class NoHandleMaintenancePolicy extends DefaultMaintenancePolicy	{
	/**
	 * no args; uses default settings with no drop/maintain states
	 */
	public NoHandleMaintenancePolicy()	{
		super();
		this.setMaintenancePlanMinThreshold(0f); 
		this.setMaintenanceTriggerThreshold(0f);
	}
}