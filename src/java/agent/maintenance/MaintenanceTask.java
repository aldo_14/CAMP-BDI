package agent.maintenance;

import java.util.logging.Logger;

import agent.maintenance.MaintenancePolicy.Priority;
import agent.model.capability.Capability;
import common.UniversalConstants;
import jason.asSemantics.IntendedMeans;
import jason.asSemantics.Intention;
import jason.asSyntax.PlanBody;
import jason.bb.BeliefBase;

/**
 * 
 * @author Alan White
 *
 */
public class MaintenanceTask implements Comparable<MaintenanceTask> {
	/**
	 * The typeof task; to insert new predecessor actions to enable the 
	 * desired precondition, to replace with a new action or actions for the
	 * same goal (but with improved confidence), or to drop the task and therefore
	 * abort the successor plan parts.
	 */
	public enum Type { precondition, effects, drop, effects_for_pre, replan, replan_conf_const, replan_for_unformed_dependency, continual }

	private final PlanBody startAction, endAction;
	private final BeliefBase context;
	private final Type type;
	private Priority priority;
	private final float conf;
	
	/**
	 * Intention where the task derives from, i.e. that requires maintenance
	 */
	private final Intention src;
	
	/**
	 * Used for handy annotations (for the user/reader), for debug/logging usage
	 */
	private String annoString;
	
	/**
	 * Direct reference to the intended means level this task relates to - i.e. whether it is
	 * at the most ground or a more abstract plan level
	 */
	private IntendedMeans imSrc;

	private Capability cap;
	
	private Logger logger;
	
	private String UUID;
	
	/**
	 * 
	 * @param type
	 * @param in
	 * @param im
	 * @param action
	 * @param context
	 * @param c
	 * @param policy
	 * @param cap
	 */
	public MaintenanceTask(Type type, Intention in, IntendedMeans im, 
			PlanBody first, PlanBody last, BeliefBase context, float c,
			MaintenancePolicy policy, Capability cap){
		this(type, in, im, first, last, context, c, policy, cap, "");
	}
	
	/**
	 * Creates a new task; default priority is low
	 * @param type
	 * @param in
	 * @param action
	 * @param context
	 */
	public MaintenanceTask(	Type type, Intention in, IntendedMeans im, 
			PlanBody first, PlanBody last, BeliefBase context, float confidence,
			MaintenancePolicy policy, Capability cap, String annoString){
		this.logger = Logger.getLogger(this.getClass().getName());
		this.logger.setLevel(UniversalConstants.MT_LOGGING_LEVEL);
		this.startAction = first.clonePB();
		this.endAction = last.clonePB();
		this.context = context;//TODO assume have used .clone() otherwise pass-by-ref screws everything up
		this.conf = confidence;
		this.src = in;
		this.imSrc = im;
		this.type = type;
		this.priority = Priority.low;//default to low
		this.cap = cap;
		this.annoString = annoString;//annotated information for user/developer
		if(	type.equals(Type.effects) && (confidence > policy.maintenanceTriggerThreshold()) && policy.maintenanceTriggerThreshold()>0)	{
			throw new RuntimeException("Severe confidence error in MP; conf=" + confidence + ", threshold=" + policy.maintenanceTriggerThreshold());
		}
		UUID = "T" + System.currentTimeMillis();
	}

	/**
	 * Creates a new task with specified priority
	 * @param type
	 * @param in
	 * @param action
	 * @param context
	 * @param p
	 */
	public MaintenanceTask(Type type, Intention in,  IntendedMeans im, 
			PlanBody first, PlanBody last, BeliefBase context, float conf,
			Priority p, MaintenancePolicy policy, Capability cap, String annoString){
		this(type, in, im, first, last, context, conf, policy, cap, annoString);
		this.priority = p;
	}


	public MaintenanceTask(Type effects, Intention i, IntendedMeans im,
			PlanBody step, BeliefBase context, float confidence,
			MaintenancePolicy mp, Capability c, String annot) {
		this(effects, i, im, step, step, context, confidence, mp, c, annot);
	}

	/**
	 * Gets the type of task based on the Type enum
	 * @return Type; pre, eff or drop
	 */
	public Type getType()	{ return type; }

	/**
	 * Get the source intention
	 * @return
	 */
	public Intention getIntention(){ return src;	}

	/**
	 * Get the source IMs
	 * @return
	 */
	public IntendedMeans getIm()	{	return imSrc;	}
	
	/**
	 * Returns the task priority as per the Priority enum
	 * @return Priority; see enum - high, low
	 */
	public Priority getPriority()	{ return priority;	}

	/**
	 * Returns the plan part <b>to be</b> maintained
	 * @return PlanBody
	 */
	public PlanBody getMaintainedActionStart()	{	return startAction;	}
	public PlanBody getMaintainedActionEnd()	{	return endAction;	}

	/**
	 * Returns the assumed execution context, passed in when the task was created.
	 * @return BeliefBase
	 */
	public BeliefBase getContext()	{	return context;	}

	/**
	 * Returns string representation
	 */
	public String toString()	{
		return 	getClass().getSimpleName() + ":" + getType().name() + "[" + UUID + "]" +  
				" for " + getMaintainedActionStart().getBodyTerm() + " to " + getMaintainedActionEnd().getBodyTerm() +
				"(Unifier " + imSrc.getUnif() + ", Priority " + priority + ", intention " + src.getId() + 
				", (prior)confidence=" + conf + "\nAnnots=" + annoString + ")";
	}

	/**
	 * Compares for priority against another task.  Intended to facilitate default priorityQueue behaviour
	 * such that tasks are ordered by priority first, then by relative urgency
	 * @param t MaintenanceTask
	 * @return int; 0==identical, 1==this>t, -1==t>this
	 */
	@Override
	public int compareTo(MaintenanceTask t) {
		Priority tp = t.getPriority();
		if(tp.equals(priority))													{	return 0;	}
		else if(tp.equals(Priority.low) && priority.equals(Priority.high))		{	return 1;	}
		else /*if(tp.equals(Priority.high) && priority.equals(Priority.low))*/	{	return -1;	}
	}
	
	/**
	 * Return information string for this task (user specified info)
	 * @return
	 */
	public String getAnnotationString() {	return annoString;	}

	/**
	 * Last generated confidence; 0 if is for precondition meintenance
	 * @return
	 */
	public float getConf() {	return conf;	}
	
	public Capability getCapability()	{	return cap;	}

	//public void setType(Type t) {	type = t;	}
}
