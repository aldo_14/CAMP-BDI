package agent.maintenance;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

import jason.asSemantics.Unifier;
import jason.asSyntax.ASSyntax;

/**
 * @author Alan White
 *
 *         Default implementation of a maintenance policy object; parse xml and
 *         fill in the fields of this. Otherwise, fields are left as
 *         defaults. </BR>
 *         </BR>
 *         nb; DROP conditions refer to dropping effects maintenance tasks; i.e.
 *         define if the task is to be viewed as intractable and thus avoided by
 *         the agent.
 */
public class DefaultMaintenancePolicy implements MaintenancePolicy {
	/**
	 * Constants for xml parsing; relates to the fields in the mp object
	 */
	public static final String HEAD_TAG = "maintenancePolicy", APPLIED_TO = "appliedTo", AGENT_ID_TAG = "agent",
			CAPABILITY_TAG = "capability", PRIORITY_TAG = "priority", DROP_CONDITIONS_TAG = "dropConditions",
			DROP_THRESHOLD_TAG = "threshold", DROP_STATE_TAG = "state", REPAIR_CONDITIONS_TAG = "repair",
			REPAIR_THRESHOLD_TAG = "threshold", REPAIR_CONSTRAINT_TAG = "constraint",
			REPAIR_CONSTRAINT_CONDITION_TAG = "condition", REPAIR_CONSTRAINT_RESTRICTION_TAG = "restriction",
			REPAIR_RELAXATION_TAG = "relaxation", RELAXATION_CONDITION_TAG = "condition",
			RELAXATION_REMOVE_CONSTRAINT_TAG = "remove";

	/**
	 * Constant identifier for agent id in state specs; i.e. damaged(AGENT_ID)
	 * can be mapped such that aid = AGENT_ID in unifier
	 */
	private static final String AGENT_ID = "AGENTID";

	/**
	 * Wildcard strings used for agent:any or capability:any pairs
	 */
	private static final String AGENT_ID_WILDCARD = "*", CAPABILITY_ID_WILDCARD = "*", SEPARATOR = "::";

	private Map<String, String> constraints = new HashMap<String, String>();
	private Map<String, String> relaxations = new HashMap<String, String>();
	protected List<String> dropConditions = new Vector<String>();
	private float dropThreshold, repairTrigThreshold, repairMinThreshold;

	/**
	 * Maps the a-c restrictions and priorities </br>
	 * </br>
	 * key format is agent_id:capability_name</br>
	 * values is a Priorirty enum object
	 */
	private Map<String, Priority> priorityMap = new ConcurrentHashMap<String, Priority>();

	/**
	 * no args; uses default settings with no drop/maintain states
	 */
	public DefaultMaintenancePolicy() {
		// add a default MP...
		priorityMap.put(AGENT_ID_WILDCARD + SEPARATOR + CAPABILITY_ID_WILDCARD, Priority.low);
		this.repairTrigThreshold = 0.95f; // was 0.85
		this.repairMinThreshold = 0.95f;
		this.dropThreshold = 0.0f;
		dropConditions.add("damaged(" + AGENT_ID + ")");
		dropConditions.add("resting(" + AGENT_ID + ")");
		dropConditions.add("mortal(" + AGENT_ID + ")");
	}

	// SETTERS
	public void setConstraints(Map<String, String> constraints) {
		this.constraints = constraints;
	}

	public void setConstraint(String key, String constraint) {
		this.constraints.put(key, constraint);
	}

	public void removeConstraint(String key) {
		this.constraints.remove(key);
	}

	public void setRelaxations(Map<String, String> relaxations) {
		this.relaxations = relaxations;
	}

	public void setRelaxation(String key, String relaxation) {
		this.relaxations.put(key, relaxation);
	}

	public void removeRelaxation(String key) {
		this.relaxations.remove(key);
	}

	public void addDropCondition(String cond) {
		this.dropConditions.add(cond);
	}

	public void removeDropCondition(String cond) {
		this.dropConditions.remove(cond);
	}

	public void setDropConditions(List<String> dropConditions) {
		this.dropConditions = dropConditions;
	}

	public void setDropThreshold(float dropThreshold) {
		this.dropThreshold = dropThreshold;
	}

	public void setMaintenanceTriggerThreshold(final float thrsh) {
		this.repairTrigThreshold = thrsh;
	}

	public void setMaintenancePlanMinThreshold(final float thrsh) {
		this.repairMinThreshold = thrsh;
	}

	// GETTERS - from interface
	@Override
	public List<String> dropConditions() {
		return dropConditions;
	}

	@Override
	public float dropConfidenceThreshold() {
		return dropThreshold;
	}

	@Override
	public float maintenanceTriggerThreshold() {
		return repairTrigThreshold;
	}

	@Override
	public float maintenanceAcceptanceThreshold() {
		return repairMinThreshold;
	}

	@Override
	public Map<String, String> getConstraints() {
		return constraints;
	}

	@Override
	public Map<String, String> getRelaxations() {
		return relaxations;
	}

	/**
	 * Merges together two maintenance policies and returns the result. Presumes
	 * a dependency relationship, where 'this' MP is for the obligant
	 * capability/agent
	 */
	@Override
	public MaintenancePolicy merge(MaintenancePolicy dependant) {
		// create new and then modify contents appropriately
		DefaultMaintenancePolicy merged = new DefaultMaintenancePolicy();

		// set priority - take highest
		Map<String, Priority> pm2 = merged.getApplicableAndPriorityMap();

		// merge priorities
		for (String key : priorityMap.keySet()) {
			if (pm2.containsKey(key) && pm2.get(key).equals(Priority.high)) {
				merged.addApplicable(key, Priority.high);
			} else {
				merged.addApplicable(key, priorityMap.get(key));
			}
		}

		// add in any leftovers...
		for (String key : pm2.keySet()) {
			if (!merged.getApplicableAndPriorityMap().containsKey(key)) {
				merged.addApplicable(key, pm2.get(key));
			}
		}

		// set maintain threshold - take highest value
		merged.setMaintenancePlanMinThreshold(
				(dependant.maintenanceAcceptanceThreshold() > this.maintenanceAcceptanceThreshold())
						? dependant.maintenanceAcceptanceThreshold() : this.maintenanceAcceptanceThreshold());

		merged.setMaintenanceTriggerThreshold(
				(dependant.maintenanceTriggerThreshold() > this.maintenanceTriggerThreshold())
						? dependant.maintenanceTriggerThreshold() : this.maintenanceTriggerThreshold());

		// set drop threshold (values) - take highest value
		merged.setDropThreshold((dependant.dropConfidenceThreshold() > dropConfidenceThreshold())
				? dependant.dropConfidenceThreshold() : dropConfidenceThreshold());

		// set constraints; take from obligant (this)
		for (String s : getConstraints().keySet()) {
			merged.setConstraint(s, getConstraints().get(s));
		}

		// set relaxations - take from dependant
		for (String s : dependant.getRelaxations().keySet()) {
			merged.setRelaxation(s, dependant.getRelaxations().get(s));
		}

		// set drop conditions
		List<String> newDrop = new Vector<String>();
		newDrop.addAll(dropConditions());
		newDrop.addAll(dependant.dropConditions());
		merged.setDropConditions(newDrop);

		return merged;
	}

	/**
	 * Set that this MP applies to the agent/capability set defined by 'key' and
	 * has priority p; i.e. truck1:*, high
	 * 
	 * @param key
	 * @param p
	 */
	@Override
	public void addApplicable(String key, Priority p) {
		priorityMap.put(key, p);
	}

	@Override
	public Map<String, Priority> getApplicableAndPriorityMap() {
		return priorityMap;
	}

	@Override
	public boolean applicableToAgent(String name) {
		for (String s : priorityMap.keySet()) {
			if (s.startsWith(name) || s.startsWith(AGENT_ID_WILDCARD)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean applicableToCapability(String name) {
		for (String s : priorityMap.keySet()) {
			if (s.endsWith(name) || s.endsWith(CAPABILITY_ID_WILDCARD)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Return priority associated with this agent and capability - null if not
	 * set here. Checks wildcards first, then concrete (i.e. priority
	 * order;*::*, aid::*, *::cid, aid:cid)
	 */
	@Override
	public Priority getPriority(String agentId, String capabilityId) {
		// try *::*
		String test = AGENT_ID_WILDCARD + SEPARATOR + CAPABILITY_ID_WILDCARD;
		if (priorityMap.containsKey(test)) {
			return priorityMap.get(test);
		}

		// try aid::*
		test = agentId + SEPARATOR + CAPABILITY_ID_WILDCARD;
		if (priorityMap.containsKey(test)) {
			return priorityMap.get(test);
		}

		// try *::cid
		test = AGENT_ID_WILDCARD + SEPARATOR + capabilityId;
		if (priorityMap.containsKey(test)) {
			return priorityMap.get(test);
		}

		// try aid:cid
		test = agentId + SEPARATOR + capabilityId;
		if (priorityMap.containsKey(test)) {
			return priorityMap.get(test);
		}

		// else - nothing?
		throw new RuntimeException("Unknown priority");
	}

	/**
	 * Does this apply to the agent / capability pair?
	 */
	@Override
	public boolean appliesTo(String holder, String c) {
		String t1 = AGENT_ID_WILDCARD + SEPARATOR + CAPABILITY_ID_WILDCARD,
				t2 = holder + SEPARATOR + CAPABILITY_ID_WILDCARD, t3 = AGENT_ID_WILDCARD + SEPARATOR + c,
				t4 = holder + SEPARATOR + c;
		return this.priorityMap.containsKey(t1) || this.priorityMap.containsKey(t2) || this.priorityMap.containsKey(t3)
				|| this.priorityMap.containsKey(t4);
	}

	@Override
	public String toString() {
		return "Maintenance Policy (" + getClass().getCanonicalName() + ")" + "\n\tPriority map\n\t\t" + priorityMap
				+ "\n\tConfidence Thresholds for repair<=" + repairTrigThreshold + ", plan acceptance>="
				+ repairMinThreshold + ", drop<=" + dropThreshold + "\n\tConstraints=\n\t\t" + constraints
				+ "\n\tRelaxations=\n\t\t" + relaxations + "\n\tDrop conditions=\n\t\t" + dropConditions;
	}

	@Override
	public Unifier getAgentIdUnifier(String aid) {
		Unifier u = new Unifier();
		u.unifies(ASSyntax.createVar(AGENT_ID), ASSyntax.createString(aid));
		return u;
	}

}
