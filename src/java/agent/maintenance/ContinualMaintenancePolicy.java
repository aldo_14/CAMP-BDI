package agent.maintenance;

public class ContinualMaintenancePolicy extends DefaultMaintenancePolicy	{
	public ContinualMaintenancePolicy()	{
		super();
		this.setMaintenancePlanMinThreshold(0.90f); //i.e. accept as better
		this.setMaintenanceTriggerThreshold(1.001f);
	}
}