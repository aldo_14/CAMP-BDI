package agent.maintenance;


/**
 * @author Alan White
 *
 * Default implementation of a maintenance policy object; parse xml and fill in the fields of this.
 * Otherwise, fields are left as defaults.
 */
public class ReplanMaintenancePolicy extends DefaultMaintenancePolicy	{
	/**
	 * no args; uses default settings with no drop/maintain states
	 */
	public ReplanMaintenancePolicy()	{
		//add a default MP...
		super();
		this.setMaintenancePlanMinThreshold(0.001f); 
		this.setMaintenanceTriggerThreshold(0.001f);
	}
}