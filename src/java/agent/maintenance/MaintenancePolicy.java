/**
 * 
 */
package agent.maintenance;

import jason.asSemantics.Unifier;

import java.util.List;
import java.util.Map;

/**
 * @author Alan White
 * Java interface representing a maintenance policy.  This can be associated with any number of agents and capabilities, and
 * is used to define effects maintenance behaviour.  Policies have generic and specific components; the latter are ignored if
 * reused for an agent/capability pair which lacks the specific knowledge.
 */
public interface MaintenancePolicy {
	
	/**
	 * Capabilities can be given a priority used to arbitrate when ordering / prioritizing maintenance tasks; the default is
	 * 'low'. Intended behaviour is that priority is used to select tasks to be handled out of execution order - that 'high' priority
	 * tasks are subordered by urgency and executed before low priority.
	 */
	public enum Priority{high, low};
	
	//drop condition/threshold
	/**
	 * If the agent BB holds the specified literals, then the action/intention associated is dropped.  This returns a list of Strings; each
	 * string is parseable to a LogicalFormula.  Each string denotes a condition that, if it holds, means the agent should drop the 
	 * intention to perform the given action.
	 * @return a list of String (may be empty)
	 */
	public List<String> dropConditions();
	
	/**
	 * If the confidence of the given action is <= this value, then the action is dropped (i.e. not performed nor maintained)
	 * @return float 1>value>=0
	 */
	public float dropConfidenceThreshold();
	
	//repair condition/threshold
	/**
	 * If the confidece of the action is <= this value, and >= the drop value (and condition !true), an effects maintenance task is
	 * added to the agenda
	 * @return float 1>value>=0
	 */
	public float maintenanceTriggerThreshold();
	
	/**
	 * Minimum value for accepting a formed maint. plan
	 */
	public float maintenanceAcceptanceThreshold();
	
	//constraints upon repair plans
	/**
	 * Defines any additional constraints upon the execution of maintenance tasks.  Maps a string condition with a string constraint; if the 
	 * condition holds in the BB, the constraint is applied.  More than one condition may apply (depending on how conditions are defined by the
	 * programmer).
	 * @return Map<String, String>
	 */
	public Map<String, String> getConstraints();
	
	//relaxations permitted
	/**
	 * Converse of getConstraints; provides a map of trigger conditions and associated relaxations (i.e. constraints that can be removed), which
	 * are intended to be parsed from string form
	 * @return Map<String, String>
	 */
	public Map<String, String> getRelaxations();
	
	/**
	 * Set that this mp is applicable to agent:capability pair identified by 'key',
	 * with priority p
	 * @param key
	 * @param p
	 */
	public void addApplicable(String key, Priority p);
	
	/**
	 * Get the full map of applicable agent:capability pairs and their relative priorities
	 * @return Map - string key to priority
	 */
	public Map<String, Priority> getApplicableAndPriorityMap();
	
	/**
	 * Tests whether the MP *may* be applied to named agent; does not check for capability
	 * constraints.
	 * @param name
	 * @return boolean; true if we have a applicable priority with the first part being either
	 * the name or a wildcard
	 */
	public boolean applicableToAgent(String name);	
	
	
	/**
	 * Tests whether the MP *may* be applied to named capability; does not check for agent
	 * constraints.
	 * @param name
	 * @return boolean; true if we have a applicable priority with the second part being either
	 * the name or a wildcard
	 */
	public boolean applicableToCapability(String name);	
	
	/**
	 * Get priority for this agent and capability - or null if not set
	 */
	public Priority getPriority(String agentId, String capabilityId);
	
	/**
	 * Returns true if this MP applies to the given capability, held by agent 'holder'.
	 * @return
	 */
	public boolean appliesTo(String holder, String c);

	//utility - merge method
	/**
	 * Creates a new maintenance policy by merging this with another.  This method pertains to dependency formation; the 'merge' operation is
	 * performed on the obligant (action performer / delegation acceptor) using a MaintenancePolicy from the dependant (assigning the task)
	 * @param dependant MaintenancePolicy
	 * @return new, merged MP
	 */
	public MaintenancePolicy merge(MaintenancePolicy dependant);

	/**
	 * Provides a standard unifier for agent id to terms in the mp xml
	 * @param aid Agent instance identifier (i.e. Truck1)
	 * @return
	 */
	public Unifier getAgentIdUnifier(String aid);
}
