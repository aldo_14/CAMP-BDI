package agent;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import agent.model.capability.Capability;
import agent.model.capability.ExternalCapability;
import agent.model.dependency.Contract;
import agent.model.goal.GoalTask;
import agent.type.CapabilityAwareAgent;
import agent.type.ContractFormerAgent;
import agent.type.arch.MultiagentArch;
import common.UniversalConstants;
import jason.asSemantics.Agent;
import jason.asSemantics.IntendedMeans;
import jason.asSemantics.Intention;
import jason.asSemantics.Unifier;
import jason.asSyntax.ASSyntax;
import jason.asSyntax.ListTerm;
import jason.asSyntax.Literal;
import jason.asSyntax.LogExpr;
import jason.asSyntax.LogicalFormula;
import jason.asSyntax.Plan;
import jason.asSyntax.PlanBody;
import jason.asSyntax.PlanBodyImpl;
import jason.asSyntax.Term;
import jason.asSyntax.UnnamedVar;
import jason.asSyntax.VarTerm;
import jason.asSyntax.parser.ParseException;
import jason.bb.BeliefBase;

public class AgentUtils {

	private String holderId;

	/*
	 * *************************************************************************
	 * ************************** UTILITY METHODS
	 *****************************************************************************************************/
	public AgentUtils() {
		holderId = "";
	}

	public AgentUtils(Object holder) {
		if (holder == null) {
			holderId = "";
		} else if (holder instanceof Agent) {
			holderId = ((Agent) holder).getTS().getUserAgArch().getAgName();
		} else if (holder instanceof Capability) {
			holderId = ((Capability) holder).getSignature().getFunctor();
		} else {
			holderId = "?AgentUils";
		}

	}

	/**
	 * Returns true if l1 and l2 are tasks within the same team; i.e. that at
	 * some point in their dependency trace (annotations), they share the same
	 * intention / agent parent. i.e.</br>
	 * <code>
	 * dA = st, ta1, op2 / dI = 1, 3, 5 - matches dA = st / dI = 1 </br>
	 * dA = st, ta1 / dI = 1, 3 - matches dA = st, ta3 / dI = 1, 5 </br>
	 * but not dA = st, ta2 / dI = 2, 4  or st / 3 etc
	 * </code></br>
	 * Just need to check entry at term 0 matches for both literal annotations.
	 * 
	 * @param l1
	 * @param l2
	 * @return
	 */
	public synchronized boolean onSameTeam(Literal l1, Literal l2) {
		boolean hasAnnots1 = !l1.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT).isEmpty()
				&& !l1.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT_INTENTION).isEmpty();
		boolean hasAnnots2 = !l2.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT).isEmpty()
				&& !l2.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT_INTENTION).isEmpty();

		// not obs for either, so true
		if (!hasAnnots1 && !hasAnnots2) {
			return true;
		}
		// obs for one but not the other, return false; one has obligations and
		// is restricted
		if ((hasAnnots1 && !hasAnnots2) || (hasAnnots2 && !hasAnnots1)) {
			return false;
		}
		// implicit else...

		ListTerm dep1 = l1.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT);
		ListTerm dep2 = l2.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT);
		ListTerm int1 = l1.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT_INTENTION);
		ListTerm int2 = l2.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT_INTENTION);

		// presume lengths are equal...
		for (int i = 0; i < dep1.size(); i++) {
			for (int j = 0; j < dep2.size(); j++) {

				String dependantL1 = ((Literal) dep1.get(i)).getTerm(0).toString();
				String dependantL2 = ((Literal) dep2.get(j)).getTerm(0).toString();
				String intentionL1 = ((Literal) int1.get(i)).getTerm(0).toString();
				String intentionL2 = ((Literal) int2.get(j)).getTerm(0).toString();
				if (dependantL1.equals(dependantL2) && intentionL1.equals(intentionL2)) {
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * Return true if task equates to a dependency for the intention intId,
	 * intended by agent aId
	 * 
	 * @param task
	 * @param intId
	 * @return
	 */
	public synchronized boolean dependencyForIntention(Literal task, String aId, int intId) {
		Logger logger = Logger.getLogger(holderId + ":isInSamePlan");
		logger.setLevel(UniversalConstants.UTILS_LOGGER_LEVEL);
		ListTerm dAnnots = task.getAnnots("dependant");
		ListTerm iAnnots = task.getAnnots("dependantIntention");
		if (dAnnots.isEmpty() || iAnnots.isEmpty()) {
			logger.severe("No annotations, ergo not a dep??" + task + "(for " + aId + ":" + intId);
			return false;
		} else {
			List<Term> ial = ((Literal) iAnnots.get(iAnnots.size() - 1)).getTerms();
			List<Term> aal = ((Literal) dAnnots.get(dAnnots.size() - 1)).getTerms();
			int relId = Integer.parseInt(ial.get(ial.size() - 1).toString().replaceAll("\"", ""));
			String relA = aal.get(aal.size() - 1).toString().replaceAll("\"", "");
			if ((relId == intId) && relA.equals(aId)) {
				logger.severe(task + " dep of " + aId + ":" + intId);
				return true;
			} else {
				return false;
			}
		}

	}

	/**
	 * Checks if the task 'other' is in the same intention as 'task', and comes
	 * afterwards. We can accept obligations for this, as we know 'task' will
	 * not be executed until 'other' has completed and been popped from
	 * execution.
	 * 
	 * @param task
	 * @param other
	 * @return
	 */
	public synchronized boolean isInSamePlan(Literal task, Literal other) {
		Logger logger = Logger.getLogger(holderId + ":isInSamePlan");
		logger.setLevel(UniversalConstants.UTILS_LOGGER_LEVEL);
		boolean sameDep = task.getAnnots("dependant").toString().equals(other.getAnnots("dependant").toString());
		boolean sameInt = task.getAnnots("dependantIntention").toString()
				.equals(other.getAnnots("dependantIntention").toString());

		// assume index>(????)
		logger.finer(task + " follow " + other + "; same dependency? " + sameDep + ", same intention? " + sameInt);
		return sameDep && sameInt;
	}

	/**
	 * Checkes if task and other have a shared ancestor i.e. 1,2,3,4,5 returns
	 * true for 1,2,3,8 Can just check for a common root...
	 * 
	 * @param task
	 * @param other
	 * @return true if the tasks share the same common root
	 */
	public synchronized boolean sharedParent(Literal task, Literal other) {
		Logger logger = Logger.getLogger(holderId + ":startsWith");
		logger.setLevel(UniversalConstants.UTILS_LOGGER_LEVEL);

		// check annots are present
		if (task.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT).isEmpty()
				|| task.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT_INTENTION).isEmpty()) {
			return false;
		}
		if (other.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT).isEmpty()
				|| other.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT_INTENTION).isEmpty()) {
			return false;
		}

		List<Term> annotDT = ((Literal) task.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT).get(0).clone())
				.getTerms();
		List<Term> annotIT = ((Literal) task.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT_INTENTION).get(0)
				.clone()).getTerms();
		List<Term> annotDO = ((Literal) task.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT).get(0).clone())
				.getTerms();
		List<Term> annotIO = ((Literal) task.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT_INTENTION).get(0)
				.clone()).getTerms();
		boolean commonDepRoot = annotDT.get(0).toString().equals(annotDO.get(0).toString());
		boolean commonIntRoot = annotIT.get(0).toString().equals(annotIO.get(0).toString());
		logger.info("AnnotDt = " + annotDT + ", AnnotDO = " + annotDO + ", annotIT = " + annotIT + ", annotIO = "
				+ annotIO + ", common root - dep? " + commonDepRoot + ", int? " + commonIntRoot);
		return commonDepRoot && commonIntRoot;
	}

	/**
	 * Return true if literal 'task' is an action that is a <i>direct</i>
	 * refinement of the task represented by 'other'
	 * 
	 * @param task
	 * @param other
	 */
	public synchronized boolean isSubChildOf(Literal task, Literal other) {
		Logger logger = Logger.getLogger(holderId + ":subChildTest");
		logger.setLevel(UniversalConstants.UTILS_LOGGER_LEVEL);
		logger.info("Checking obl " + other + " against " + task);

		/*
		 * Hack for the TOP trigger; i.e. the initial triggering event with no
		 * annots such as cargoNeeded(blah)
		 */
		if (task.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT).isEmpty()
				&& task.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT_INTENTION).isEmpty()) {
			return true;
		}

		try {
			// get dIds and aIds for the task
			// task should have annots ala dependant(2,2) and
			// dependantIntention(truck1, driver)
			// looking for a obligation task with annots ala dependant(2) and
			// dependantIntention(truck1)...
			List<Term> annotD = ((Literal) task.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT).get(0).clone())
					.getTerms();
			List<Term> annotI = ((Literal) task.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT_INTENTION).get(0)
					.clone()).getTerms();

			if (annotD.size() != annotI.size()) {
				// this really, really shouldn't be allowed to happen
				logger.severe("Mismatch in annotations for " + task.toString());
			}

			if (other.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT_INTENTION).isEmpty()) {
				// cannot create a new intention when we already have an
				// entirely different obligation
				return false;
			}

			// get annots from task we are comparing against
			String lAnnotsD = ((Literal) other.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT).get(0)).getTerms()
					.toString();
			String lAnnotsI = ((Literal) other.getAnnots(UniversalConstants.ANNOTATION_DEPENDANT_INTENTION).get(0))
					.getTerms().toString();
			logger.info("Check dep: " + lAnnotsD + " vs " + annotD + " equals=" + lAnnotsD.equals(annotD.toString()));
			logger.info("Check int: " + lAnnotsI + " vs " + annotI + " equals=" + lAnnotsI.equals(annotI.toString()));

			/*
			 * First (top) entry of the dependent annotations is always going to
			 * be that of a new intention; we want to find if there is a common
			 * starting point, which may be as limited as only the first
			 * agent:intention id combination.
			 */
			while (!annotD.isEmpty()) {
				// remove last added entry
				logger.info(task + " - " + annotD + ":" + annotI);
				annotD.remove(annotD.size() - 1);
				annotI.remove(annotI.size() - 1);
				logger.info(task + " - " + annotD + ":" + annotI);
				if (lAnnotsD.startsWith(annotD.toString()) && lAnnotsI.startsWith(annotI.toString())) {
					logger.info(other + " parent for " + task);
					return true;
				}
			}
			return false;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Exception " + e.toString());
			String ec = "isSubChildOf(" + task + ", " + other + "):! " + e.toString();
			for (StackTraceElement st : e.getStackTrace()) {
				ec = ec + "\n" + st.toString();
			}
			logger.severe(ec);
			return false;// default
		}
	}

	/**
	 * Forms a unifier from the given arguments; maps the arguments in
	 * 'actionCall' to the arguments in 'signature'; such as to generate a
	 * unifier for ground pre and post conditions. </br>
	 * NB: if wanting to get a goal state for a plan, then map goal (
	 * <code>achieves().getSignature</code> as the literal passed in for
	 * signature)
	 * 
	 * @param signature
	 *            - the signature (i.e. move(A,B), move(C,D), etc) used to
	 *            create the unifier, terms arity should be same length as the
	 *            args list. Assumed to be unground.
	 * @param actionCall
	 *            - the literal corresponding to the activity, i.e.
	 *            move("truck1", "Glasgow", "Edinburgh"). Assumed to be at least
	 *            partially ground (untested)
	 * @return Unifier matching the vars to signature; A=truck1, O=Glasgow,
	 *         J=Edinburgh, etc. May involve referencing unbound vars. Returns
	 *         an empty unifier if the arguments are insufficient (i.e. length
	 *         mismatch).
	 */
	public synchronized final static Unifier formUnifierFromArgs(Literal sig, Literal action) {
		sig = sig.copy(); // deref
		action = action.copy();
		Logger logger = Logger.getLogger("formUnifierFromArgs(" + sig + "," + action + ")");
		logger.setLevel(UniversalConstants.UTILS_LOGGER_LEVEL);
		Unifier u = new Unifier();
		List<Term> args = action.getTerms();
		logger.finer("form unifier; sig=" + sig.toString() + ", action=" + action + ", args =" + args);
		if (args.size() == sig.getTerms().size()) {
			for (int i = 0; i < args.size(); i++) {
				Term arg = args.get(i);

				logger.finer(i + ") form unifier; arg=" + arg);

				if (sig.getTerm(i).equals(arg)) {
					logger.finest(sig.getTerm(i) + " is unbound, equal terms; no change");
				} else {
					VarTerm vt = new VarTerm(sig.getTerm(i).toString());
					logger.finest("Binding vt = " + vt + " to " + arg + ", !t cyclic=" + !arg.isCyclicTerm()
							+ ", t hasVar=" + arg.hasVar(vt, u) + ", t isVar=" + arg.isVar());
					if (arg.isGround()) {
						u.bind(vt, arg.clone());
					} else {
						u.bind(vt, ASSyntax.createAtom(arg.toString().replaceAll("\"", "")));
					}

					// this can cause infinite loop if result of get matches an
					// existing var name
					// i.e. if we've already formed O==J
					// then J == O can cause an infinite loop...
					logger.finest("Check vt = " + u.get(vt));
				}
			}
		}
		logger.finer("Sig; " + sig + "- Unifier for " + action + "=" + u);
		return u;
	}

	public synchronized List<PlanBody> planIntoBodyList(PlanBody body) {
		return planIntoBodyList(body, true);
	}

	/**
	 * Takes a given plan body and forms a List of the planbodies therein
	 */
	public synchronized List<PlanBody> planIntoBodyList(PlanBody body, boolean includeInternalActions) {
		List<PlanBody> returned = new Vector<PlanBody>();
		while (body != null) {
			if (includeInternalActions) {
				returned.add(body);
			} else if (!includeInternalActions && !body.getBodyTerm().isInternalAction()) {
				returned.add(body);
			}
			body = body.getBodyNext();
		}
		return returned;
	}

	/**
	 * Converts a list of literals into a list of their functors (strings)
	 */
	public synchronized List<String> literalListIntoFunctorStrings(List<Literal> in) {
		List<String> out = new Vector<String>();
		for (Literal l : in) {
			out.add(l.getFunctor());
		}
		return out;
	}

	/**
	 * Counts the number of actions in p; assumes p is a linear plan, and counts
	 * subgoals as one action
	 * 
	 * @param p
	 * @param countInternal;
	 *            if true, include internal actions in the count
	 * @return
	 */
	public synchronized int getActionCount(PlanBody pb, boolean cIn) {
		int count = 0;
		while (pb != null) {
			if (!cIn || (cIn && pb.getBodyTerm().isInternalAction())) {
				count++;
			}
			pb = pb.getBodyNext();
		}
		return count;
	}

	/**
	 * Forms a queue of intended means to mirror that contained in the intention
	 * (directly references...); this copies previous API functionality that
	 * used to allow direct access to the intentions IM stack. Peek() gives the
	 * same result as intention.peek, i.e. the most ground intended means.
	 * 
	 * @param i
	 *            Intention
	 * @return Stack of IntendedMeans; all contents referenced rather than
	 *         cloned
	 */
	public synchronized LinkedList<IntendedMeans> intentionIntoIMsQueue(Intention i) {
		Logger logger = Logger.getLogger(holderId + ":intentionIntoIMsQueue(" + i.getId() + ")");
		logger.setLevel(UniversalConstants.UTILS_LOGGER_LEVEL);
		LinkedList<IntendedMeans> rt = new LinkedList<IntendedMeans>();
		Iterator<IntendedMeans> ims = i.iterator();
		while (ims.hasNext()) {
			rt.add(ims.next());
		}
		logger.finer("Created ims stack; \n" + rt);
		return rt;
	}

	public static synchronized LinkedList<Literal> extractOrderedLeafActivityList(final Intention original) {
		final Intention i = original.clone();
		final LinkedList<Literal> activities = new LinkedList<Literal>();
		boolean atTop = true;
		while (i.peek() != null) {
			final IntendedMeans im = i.pop();
			PlanBody pb = im.getCurrentStep();
			if (!atTop) {
				pb = pb.getBodyNext();
			}
			atTop = false;
			while (pb != null) {
				final Literal activity = (Literal) pb.clonePB().getBodyTerm();
				if(!activity.isInternalAction()) {
					activity.apply(im.getUnif());
					activities.add(activity);
				}
				pb = pb.getBodyNext();
			}
		}
		return activities;
	}
	
	public static List<String> getExtraPrecondSetForPlan(
			final ContractFormerAgent agent, final List<Literal> activities, final BeliefBase eec) {
		final List<String> newList = new LinkedList<String>();
		newList.addAll(getExtraPrecondSetForPlan(agent, activities, eec, true));
		return newList;
	}

	public static Set<String> getExtraPrecondSetForPlan(
			final ContractFormerAgent agent, final List<Literal> activities, final BeliefBase eec, final boolean debug) {
		final Logger logger = Logger.getLogger(agent.getId() + ":getExtraPrecondSetForPlan");
		logger.setLevel(Level.ALL);
		logger.info("Activity set\n"+activities);
		BeliefBase bb = eec.clone();
		Set<String> precondSet = new LinkedHashSet<>();
		if (!activities.isEmpty()) {
			logger.info("Begin looping");
			for (int i = 0; i < activities.size(); i++) {
				logger.info("Begin looping; i=" + i + "<" + activities.size());
				final Literal activity = activities.get(i);
				final Capability c = findCapability(agent, bb, activity) ;
				logger.info("Got capability, let's check preconds...!");
				if (!c.preconditionsHold(bb, activity)) {
					logger.info("Preconds aren't holding for " + activity);
					final String[] pre = c.getPreconditions();
					final LinkedList<String> precondNotHoldSet = getOrEdPreconds(bb, pre, formUnifierFromArgs(c.getSignature(),activity));
					final List<List<String>> dnfSet = cloneAndDuplicate(precondSet, precondNotHoldSet.size());
					for(int j=0;j<precondNotHoldSet.size();j++) {
						if(!dnfSet.get(j).contains(precondNotHoldSet.get(j).trim())) {
							dnfSet.get(j).add(precondNotHoldSet.get(j).trim());
						}
					}
					precondSet = mergeIntoDisjunctiveSets(dnfSet);
				}
				logger.info("Estimate next EEC");
				bb = agent.estimatePostExecutionBB(bb, activity, c);
				logger.info("Estimated!");
			}
		}
		logger.severe("Returning precond set for " + activities + " as " + precondSet);
		return precondSet;
	}

	private static Capability findCapability(final ContractFormerAgent agent, final BeliefBase eec,
			final Literal activity) {
		return (!MultiagentArch.isJointActivity(activity, agent))? agent.getCapability(activity.getFunctor()):
			(agent.hasDependencyContract(activity) ? agent.getDependencyContract(activity).getExternalCapability() : agent.getBestFitExtCapability(eec, activity));
	}

	/**
	 * Returns a CNF specification of non-holding preconditions, where the returned collection stirngs
	 * are disjunctive specifications convertable to (ground) logical formulas
	 */
	private static LinkedList<String> getOrEdPreconds(final BeliefBase eec, final String[] pre, final Unifier u) {
		final Logger logger = Logger.getLogger(":getOrEdPreconds");
		logger.setLevel(Level.INFO);
		final LinkedList<String> precondNotHoldSet = new LinkedList<>();
		for (final String precond : pre) {
			final List<String> notHoldingForThis = getNonHoldingPreconditions(precond, eec, u);
			String andList = "";
			final Iterator<String> iterator = notHoldingForThis.iterator();
			while(iterator.hasNext()) {
				final String next = iterator.next();
				if(!andList.contains(next)) {
					andList = andList + next + (iterator.hasNext()? " & " : "");
				}
			}
			if(andList.endsWith("& ")) {
				andList = andList.substring(0, andList.length()-2);
			}
			logger.info("And-ed set of preconds is " + andList);
			precondNotHoldSet.add(andList);
		}
		return precondNotHoldSet;
	}

	private static List<String> getNonHoldingPreconditions(final String precondition, final BeliefBase bb, final Unifier u) {
		final Logger logger = Logger.getLogger(":getNonHoldingPreconditions");
		logger.setLevel(Level.ALL);
		final List<String> unbelieved = new LinkedList<String>();
		final String[] preLits = precondition.split("&");
		for (String formula : preLits) {
			formula = formula.trim();
			formula = formula.replaceFirst("not \\(", "not ");
			while (formula.startsWith("(")) {
				formula = formula.replaceFirst("\\(", "");
			}
			while (formula.endsWith("))")) {
				formula = formula.replaceAll("\\)\\)", ")");
			}
			logger.info("Testing; " + formula + ", u=" + u);
			final LogicalFormula lf = LogExpr.parseExpr(formula);
			lf.apply(u);
			Agent test = CapabilityAwareAgent.getFutureAgent(bb);
			if (!test.believes(lf, new Unifier())) {
				logger.info("Not found " + lf);
				if(!unbelieved.contains(lf.toString())) {
					unbelieved.add(lf.toString());
				}
			}
		}
		return unbelieved;
	}
	
	public static List<List<String>> cloneAndDuplicate(final Set<String> cloneMe, final int times) {
		final List<List<String>> cloned = new ArrayList<>(times);
		for(int i=0; i<times; i++) {
			final List<String> newList = new ArrayList<>();
			for(final String entry:cloneMe) {
				if(!newList.contains(entry)) {
					newList.add(entry);
				}
			}			
			cloned.add(newList);
		}
		return cloned;
	}
	
	private static Set<String> mergeIntoDisjunctiveSets(final List<List<String>> dnfSet) {
		final Set<String> cnfPrecond = new LinkedHashSet<>(dnfSet.size());
		for(List<String> specification: dnfSet) {
			final String anded = andEd(specification);
			if(!cnfPrecond.contains(anded)) {
				cnfPrecond.add(anded);
			}
		}
		return cnfPrecond;
	}

	public static String andEd(final Collection<String> specification) {
		String lf = "";
		final Iterator<String> it = specification.iterator();
		while(it.hasNext()) {
			final String pre = it.next();
			if(!lf.contains(pre)) {
				lf = lf + pre + (it.hasNext() ? " & " : "");
			}
		}
		lf = lf.trim();
		return lf.endsWith("&")? lf.substring(0, lf.length()-1) : lf;
	}

	/**
	 * See setPlanActionIndexAnnots; assumes indexOffset == 0
	 * 
	 * @param p
	 * @param indexStart
	 */
	public synchronized void setPlanActionIndexAnnots(Plan p) {
		setPlanActionIndexAnnots(p, 1);
	}

	/**
	 * Adds an annotation to each plan action literal, giving its numerical
	 * place in the plan (i.e. index(1) is annotated to the first action,
	 * index(3) to the third, etc) based on some start value (indexStart). This
	 * is used to allow unique identification of actions within the plan, i.e.
	 * so if two move(A,B) actions were present, they could be distinguished
	 * using the index. </br>
	 * It is <b>not</b> inteded to provided definitive placement information, as
	 * newly inserted plan actions (i.e. from maintenance) may have indexes
	 * larger than their plan position.
	 * 
	 * @param p
	 * @param indexOffset
	 *            - actions will be annotated index(their position in plan +
	 *            indexOffset)
	 */
	public synchronized void setPlanActionIndexAnnots(Plan p, int indexOffset) {
		Logger logger = Logger.getLogger(holderId + ":setPlanActionIndexAnnots");
		logger.setLevel(UniversalConstants.UTILS_LOGGER_LEVEL);
		if (p == null) {
			logger.severe("Null plan passed in for annotation!");
			return;
		}

		List<PlanBody> actions = planIntoBodyList(p.getBody());

		for (int i = 0; i < actions.size(); i++) {
			try {
				logger.finest("Set annot: " + actions.get(i));
				// NB: internal actions do not support annotations...
				// we also don't annotate things like tests or belief addition -
				// only actions or plan triggers/goal adds
				if (actions.get(i).getBodyTerm().isLiteral() && !actions.get(i).getBodyTerm().isInternalAction()
						&& (actions.get(i).getBodyType().equals(PlanBodyImpl.BodyType.achieve)
								|| actions.get(i).getBodyType().equals(PlanBodyImpl.BodyType.achieveNF)
								|| actions.get(i).getBodyType().equals(PlanBodyImpl.BodyType.action))) {
					// check that we've not already annotated this plan... if
					// so, we return immediately
					Literal action = (Literal) actions.get(i).getBodyTerm();
					// set index (uuid int) label
					if (action.hasAnnot(ASSyntax.parseTerm(UniversalConstants.ANNOTATION_ID_IN_PLAN + "(A)"))) {
						logger.warning("Already annotated plan; returning with no further changes.  " + p.toASString());
						return;
					} else {
						action.addAnnot(ASSyntax.createLiteral(UniversalConstants.ANNOTATION_ID_IN_PLAN,
								ASSyntax.createNumber((i + 1) + indexOffset)));
					}

					// append plan label
					if (action.hasAnnot(ASSyntax.parseTerm(UniversalConstants.ANNOTATION_PLAN_LABEL + "(A)"))) {
						logger.warning("Already annotated plan; returning with no further changes.  " + p.toASString());
						return;
					} else {
						action.addAnnot(ASSyntax.createLiteral(UniversalConstants.ANNOTATION_PLAN_LABEL,
								ASSyntax.createString(p.getLabel().getFunctor())));
					}
				}
			} catch (Exception e) {
				String st = "";
				for (StackTraceElement s : e.getStackTrace()) {
					st = st + "\n" + s;
				}
				logger.severe("Error annotating action " + i + " from set " + actions + st);
			}
		}

	}

	/**
	 * Places the effects of a given action into a collection denoting belief
	 * base changes arising post-execution; the passed in collection should be
	 * the cumulative changes of any preceding action(s) in the plan.
	 * 
	 * @param sig
	 *            - signature used to map ground terms to effects
	 * @param eff
	 *            - string of effects
	 * @param bbChanges
	 *            - previous bb changes; add to this
	 * @param action
	 *            - the action being handled, used to ground terms accordingly
	 * @return Collection<String> of BB changes
	 */
	public synchronized Collection<String> putEffectsIntoBBChanges(Literal sig, String[] eff, Literal action,
			Collection<String> existingChanges) {
		Logger logger = Logger.getLogger(holderId + ":putInBBChanges(sig/action=" + sig + "/" + action + ")");
		logger.setLevel(UniversalConstants.UTILS_LOGGER_LEVEL);
		Vector<String> bbChanges = new Vector<String>();

		if (!action.isGround()) {
			logger.warning("Unground action; " + action + ", prev changes were\n" + existingChanges);
		}

		// deref
		for (String s : existingChanges) {
			bbChanges.add(new String(s));
		}

		// debug output
		if (logger.isLoggable(Level.FINER)) {
			String s = "Posteffects of " + action + "[";
			for (String pe : eff) {
				s = s + " " + pe;
			}
			s = s + " ]";
			logger.finer(s);
		}

		Unifier u = formUnifierFromArgs(sig, action);
		logger.finer("Unifier " + sig + ":" + action + "=" + u);
		for (String s : eff) {
			try {
				// + = add, - = remove
				boolean addLit = s.startsWith("+");
				Literal l = ASSyntax.parseLiteral(s.substring(1));
				logger.finer(l + ": about to apply  " + u);
				boolean result = l.apply(u);
				logger.finer(result + "; Formed & applied " + u + " to " + l);

				// try manual
				for (int i = 0; i < l.getTerms().size(); i++) {
					Term t = l.getTerm(i);
					logger.finer(t + " is var? " + t.isVar() + ", is ground? " + t.isGround());
					if (!t.isGround() && t.isVar()) {
						if (u.get(t.toString()) != null) { // can get mapping
															// for t
							logger.finer("Manually unifying the unground var... " + t.toString() + " to "
									+ u.get(t.toString()) + " from " + u.toString());
							l.setTerm(i, u.get(t.toString()));
						} else {
							logger.finer("Updating var references; " + l.toString());
							/*
							 * If a value is unground, unification will simply
							 * set it to null; so we just want to match up
							 * var-to-var so capability signature and the
							 * action/plan signature match. The default impl of
							 * unifier means we need to do a bit of hackery
							 * here, firstly to get the var-var mappings from
							 * the unifier, then to strip out the auto-formed
							 * UUID groundings (i.e. in the getAsTerm, an
							 * unground mapping J=CDEST would see the unground J
							 * replaced by a generated uuid varterm - ala _20J)
							 */
							// we need to remap to the original non-uuid; remove
							// the _INT from the functor/name
							// i.e. _27J should be plain J
							ListTerm ult = (ListTerm) u.getAsTerm();
							for (int j = 0; j < ult.size(); j++) {
								Literal mapEntry = (Literal) ult.get(j);
								logger.finest("Found mapping for " + t + "; " + mapEntry);
								UnnamedVar uv = (UnnamedVar) mapEntry.getTerm(0);
								String nonUuid = uv.getFunctor();
								nonUuid = nonUuid.replaceFirst("_[0-9]+", "");
								logger.finest(
										"Unifier entry; " + mapEntry + "(" + mapEntry.getTerm(1) + "=" + nonUuid + ")");
								if (nonUuid.equals(t.toString())) {
									logger.finest("Map entry " + nonUuid + " matches " + t + ", set to to "
											+ mapEntry.getTerm(1));
									l.setTerm(i, mapEntry.getTerm(1));
								} else {
									logger.finest("Map entry " + nonUuid + " !matches " + t);
								}
							}
						}
						logger.finer("Updated; " + l);
					}
				}

				// for adding/removing from bb change set
				String add = "+" + l.toString();
				String rem = "-" + l.toString();

				// now check vis-a-vis changes from last action
				// if addLit, remove any '-' prefixed entries
				if (addLit) {
					if (bbChanges.contains(rem)) {
						bbChanges.remove(rem);
					}
					if (!bbChanges.contains(add)) {
						bbChanges.add(add);
					}
				}
				// if !addLit (remove), remove any existing '+' prefixed entries
				else {
					if (bbChanges.contains(add)) {
						bbChanges.remove(add);
					}
					if (!bbChanges.contains(rem)) {
						bbChanges.add(rem);
					}
				}
			} catch (ParseException e) {
				logger.severe("Error parsing posteffect (" + s + ");" + e.toString());
				for (StackTraceElement st : e.getStackTrace()) {
					logger.severe(st.toString());
				}
			}
		}

		logger.finer(action + " -> effects to BB  = " + bbChanges);
		return bbChanges;
	}

	/**
	 * Takes a plan (starting body) and assembles a set of the posteffects of
	 * those plan actions, using the passed in agent object to get capability
	 * info.
	 * 
	 * @param p
	 *            PlanBody for start of plan
	 * @return Collection of string changes; prepended with + for state
	 *         addition, - for deletion
	 */
	public synchronized Collection<String> getPostEffectsOfPlan(final CapabilityAwareAgent ca, PlanBody p) {
		final Logger logger = Logger.getLogger(holderId + ":getPostEffectsOfPlan");
		logger.setLevel(UniversalConstants.UTILS_LOGGER_LEVEL);
		Collection<String> changes = new Vector<String>();
		while (p != null) {
			final Literal l = (Literal) p.getBodyTerm();
			if (ca.hasCapability(p)) {
				Capability c = ca.getCapability(p);
				logger.finer(l + " effects addition\nTaking from held capability... sig=" + c);
				changes = putEffectsIntoBBChanges(c.getSignature(), c.getPostEffects(), l, changes);
			} else if (ca instanceof ContractFormerAgent && ((ContractFormerAgent) ca).hasDependencyContract(l)) {
				logger.finer(l + " effects; Taking from contract...");
				Contract c = ((ContractFormerAgent) ca).getDependencyContract(l);
				ExternalCapability ec = c.getExternalCapability();
				if (ec == null) {
					logger.finer(l + " effects; no ec, from task...");
					final GoalTask g = ca.getTaskFactory().factory(l.getFunctor());
					if (g != null) {
						changes = putEffectsIntoBBChanges(g.getSignature(), g.getGoalEffects(), l, changes);
					} else {
						logger.warning("No ec or goal for " + l);
					}
				} else {
					changes = putEffectsIntoBBChanges(ec.getSignature(), ec.getPostEffects(), l, changes);
				}
			} else {
				// we just use the goal information, as too early to identify
				// what sort of precise sideffects there are
				final GoalTask g = ca.getTaskFactory().factory(l.getFunctor());
				if (g != null) {
					// do variable mapping from plan sig vs goal sig
					logger.finer(l + " getting effects from goal task info; " + g);
					changes = putEffectsIntoBBChanges(g.getSignature(), g.getGoalEffects(), l, changes);
				}
			}
			p = p.getBodyNext(); // next step...
		}
		return changes;
	}

}
