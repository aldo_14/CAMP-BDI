package agent.planning;

import java.util.Collection;
import java.util.List;

import agent.model.capability.Capability;
import agent.model.capability.ExternalCapability;
import agent.planning.graphplan.LGP.Mode;
import jason.asSyntax.Literal;
import jason.bb.BeliefBase;

public interface PlannerInf {
	/**
	 * Generate a plan
	 * @param agid - agent identifier
	 * @param trigger - trigger activity for PDDL file naming
	 * @param init - initial state belief base
	 * @param goal - list of list of literals; defines disjunctive sets of goal literals
	 * @param caps - set of local/internal capabilities used to form operators
	 * @param ext - set of external caps used to form operators
	 * @param minConf - confidence restriction for plan operator generation, if applicable
	 * @return Plan if found, null if not
	 */
	PlanResult getPlan(	String agid, Literal trigger, BeliefBase init, List<List<Literal>> goal, 
								Collection<Capability> caps, Collection<ExternalCapability> ext, float minConf);
	
	Mode getPlanningMode();
}
