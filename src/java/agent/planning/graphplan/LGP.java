package agent.planning.graphplan;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.logging.Logger;

import truckworld.env.Cargoworld;
import common.UniversalConstants;

/** Singleton used to provide synchronized access to the LGP executable */
public class LGP {
	public enum Mode {quality("-quality"), speed("-speed");
		private final String arg;
		Mode(final String arg) {
			this.arg = arg;
		}
		public String getArg() { 
			return arg;
		}
		public static Mode getMode(final String arg) {
			return arg.equals(quality.getArg())? quality:speed;
		}
	};
	
	private static final float CPU_TIME_BOUND = 0.5f;
	private static LGP _instance = null;
	private Logger logger;
	private String id = "";
	private final Mode mode;
	
	public LGP(final String id, final Mode mode){
		logger = Logger.getLogger(getClass().getSimpleName());
		logger.setLevel(UniversalConstants.PLANNER_LOGGER_LEVEL);
		this.id =  id;
		this.mode = mode;
		_instance = this;
	}

	public boolean callLpgTd(final String opFile, final String problemFile, final String outputFile)	{
		synchronized(_instance)	{ //try and avoid overlapping calls by using the singleton as semaphore
			final int exitValue;
			//lpg-td-1.0 -o operators.pddl -f problem.pddl -out output.plan -quality
			String planner = "\"" + LPGPlanningUtils.PLANNING_DIR + "\\" + id + "\\lpg-td-1.0.exe\"";
			try	{
				//NOTE: needs cygwin in the PATH; if not present, will fail with no message or result(s)
				final Process process = createProcessBuilder(opFile, problemFile, outputFile, planner).start();
				Pacemaker heartBeat = getHeartbeatThread(opFile + "(1)", process);
				heartBeat.start();
				createGobblers(process);
				exitValue = process.waitFor();
				heartBeat.kill();
				closeTheStreams(process);
				process.destroy();
				System.gc();}
			catch(Exception e)	{
				String eS = "Planner exception!  " + e.toString() + "\nArgs: -o " + opFile + ", -f " + problemFile + ", -out " + outputFile;
				for(StackTraceElement st: e.getStackTrace())	{	eS = eS + "\n\t" + st.toString();	}
				logger.severe(eS);
				return false;
			}
			return exitValue == 0;
		}
	}

	private ProcessBuilder createProcessBuilder(final String opFile, final String problemFile, final String outputFile, final String planner) {
			return 	mode.equals(Mode.quality) ?
					getProcessForQualityMode(opFile, problemFile, outputFile, planner):
					getProcessForSpeedMode(opFile, problemFile, outputFile, planner);
	}

	private ProcessBuilder getProcessForSpeedMode(final String opFile, final String problemFile,
			final String outputFile, final String planner) {
		return new ProcessBuilder(
				new String[]{planner, "-o", opFile, "-f", problemFile,"-out", outputFile, mode.getArg(), "-seed", "2014", "-onlybestfirst", "-lowmemory"});
	}

	private ProcessBuilder getProcessForQualityMode(final String opFile, final String problemFile,
			final String outputFile, final String planner) {
		return new ProcessBuilder(
				new String[]{planner, "-o", opFile, "-f", problemFile,"-out", outputFile, 
				"-n", "20", "-seed", "2014", "-cputime", Float.toString(CPU_TIME_BOUND)});
	}

	private void createGobblers(final Process process) {
		new StreamGobbler(process.getInputStream(), "InputStream(1)").start();
		new StreamGobbler(process.getErrorStream(), "ErrorStream(1)").start();
	}

	private void closeTheStreams(final Process process) throws IOException {
		process.getErrorStream().close();
		process.getInputStream().close();
		process.getOutputStream().close();
	}


	private Pacemaker getHeartbeatThread(final String id, final Process process) {
		return new Pacemaker(process, id);
	}


	private final class Pacemaker extends Thread {
		private final Process process;
		private final String id;
		private boolean isAlive = true;

		private Pacemaker(final Process process, final String id) {
			this.process = process; 
			this.id = id;
		}

		public final void kill() {
			isAlive = false;
		}
		
		public final boolean alive() {
			return isAlive;
		}

		public void run()	{
			final long timeout = System.currentTimeMillis() + new Float(CPU_TIME_BOUND*1000).longValue() +150;
			while(alive())	{
				Cargoworld.getInstance().heartbeat("Planner: " + id );
				if((System.currentTimeMillis()) > timeout)	{
					logger.severe("Timeout " + System.currentTimeMillis() + ">" + timeout + "; killing; " + id);
					process.destroy();
					this.kill();
				}
				try {	Thread.sleep(50);	} 
				catch (InterruptedException e){
					logger.severe(e.toString());
					this.kill();
				}
			}
		}
	}


	class StreamGobbler extends Thread	{
		final InputStream is;
		final String type;

		StreamGobbler(final InputStream is, final String type)   {
			this.is = is;
			this.type = type;
		}

		public void run()  {
			try    {
				InputStreamReader isr = new InputStreamReader(is);
				BufferedReader br = new BufferedReader(isr);
				String line=null;
				while ( (line = br.readLine()) != null)	{   logger.info(type + ">" + line);    }
				br.close();
				isr.close();
			}
			catch (IOException ioe)   {  logger.severe("Gobbling error: " + ioe);    }
		}
	}
}
