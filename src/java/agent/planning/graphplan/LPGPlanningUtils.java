package agent.planning.graphplan;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import agent.AgentUtils;
import agent.model.capability.Capability;
import agent.model.capability.CompositeCapability;
import agent.model.capability.ConfidencePrecondition;
import agent.model.capability.ExternalCapability;
import agent.planning.PlanningUtils;
import agent.type.CapabilityAwareAgent;
import common.UniversalConstants;
import jason.asSyntax.ASSyntax;
import jason.asSyntax.Literal;
import jason.asSyntax.LogExpr;
import jason.asSyntax.Plan;
import jason.asSyntax.Term;
import jason.bb.BeliefBase;
import jason.bb.DefaultBeliefBase;

public class LPGPlanningUtils extends PlanningUtils {
	/**
	 * Defines fluents so we can use a cost-based metric
	 */
	private static final String DOMAIN_REQUIREMENTS = "(:requirements :adl :fluents)";
	private static final String ACTION_COST = "total-cost";
	
	/**
	 * Insert this before actions to define fluents
	 */
	private static final String COST_FUNCTION = "(:functions (" + ACTION_COST+ "))";// (" + CONF_COST +"))";
	/**
	 * Insert cost metric at the end, after actions; minimize action cost
	 */
	private static final String COST_METRIC = "(:metric minimize (" + ACTION_COST+ "))";

	/**
	 * Insert into init part of problem to intialize the cost metric(s)
	 */
	private static final String INIT_COST_METRIC = "  (= (" + ACTION_COST + ") 0)";

	private final boolean qualityMode;
	
	public LPGPlanningUtils(final boolean qualityMode) {
		this.qualityMode = qualityMode;
	}
	
	/* NOTE: example PDDL for wumpus world;
	 (define (problem wumpus-b-1)
  (:domain wumpus-b)
  (:objects sq-1-1 sq-1-2 sq-1-3
	    sq-2-1 sq-2-2 sq-2-3
	    the-gold the-arrow
	    agent wumpus)
  (:init (adj sq-1-1 sq-1-2) (adj sq-1-2 sq-1-1)
	 (adj sq-1-2 sq-1-3) (adj sq-1-3 sq-1-2)
	 (adj sq-2-1 sq-2-2) (adj sq-2-2 sq-2-1)
	 (adj sq-2-2 sq-2-3) (adj sq-2-3 sq-2-2)
	 (adj sq-1-1 sq-2-1) (adj sq-2-1 sq-1-1)
	 (adj sq-1-2 sq-2-2) (adj sq-2-2 sq-1-2)
	 (adj sq-1-3 sq-2-3) (adj sq-2-3 sq-1-3)
	 (pit sq-1-2)
	 (at the-gold sq-1-3)
	 (is-gold the-gold)
	 (takeable the-gold)
	 (at agent sq-1-1)
	 (alive agent)
	 (have agent the-arrow)
	 (is-arrow the-arrow)
	 (takeable the-arrow)
	 (at wumpus sq-2-3)
	 (alive wumpus))
  (:goal (and (have agent the-gold)
              (at agent sq-1-1)
          ))
  )
	 */
	private int count;
	
	/**
	 * Form the domain file; goals are passed in, as we may have predicates there that are NOT
	 * referenced in any capabilities (but which required identification - this applies
	 * particularly to 'not' conditions, i.e. a not onR goal for a helicopter agent)
	 */
	public String assemblePddlDomain(final String dom,
			final Collection<Capability> caps, final Collection<ExternalCapability> extcaps, 
			final List<List<Literal>> goal, final BeliefBase init, final float minConf) {
		String operators = "";
		count = 1;
		for(Capability c:caps)	{
			operators = operators + getPddlOperator(c, init, minConf) + "\n";
			count++;
		}
		for(Capability c:extcaps)	{
			operators = operators + getPddlOperator(c, init, minConf) + "\n";
			count++;
		}
		
		List<Capability> allC = new Vector<>();
		allC.addAll(caps);
		allC.addAll(extcaps);
		return assembleOperatorsDefinition(createPddlDomainString(dom), formPredicates(allC, init, goal, minConf), operators);
	}

	/**
	 * Assembles a pddl operators string (for a file) when given the indicated string params
	 * @return
	 */
	private String assembleOperatorsDefinition(final String dom, final String pred, final String ops)	{
		logger.setLevel(UniversalConstants.PLANNER_LOGGER_LEVEL);
		return "(define " + dom + "\n" + pred +"\n" + (qualityMode?COST_FUNCTION + "\n":"") + ops + "\n)";
	}

	/* i.e.
	 (define (problem trivial1)
  (:domain dock-worker-robot)

  (:objects
   r1 - robot
   l1 l2 - location
   k1 k2 - crane
   p1 q1 p2 q2 - pile
   ca - container)

  (:init
   (adjacent l1 l2)
   (adjacent l2 l1)

   (attached p1 l1)
   (attached q1 l1)
   (attached p2 l2)
   (attached q2 l2)

   (belong k1 l1)
   (belong k2 l2)

   (in ca p1)

   (on ca pallet)

   (top ca p1)
   (top pallet q1)
   (top pallet p2)
   (top pallet q2)

   (at r1 l1)
   (unloaded r1)
   (occupied l1)

   (empty k1)
   (empty k2))

;; the task:
  (:goal
    (and (in ca p1))))
	 */
	/**
	 * Create string for a facts file
	 * @param problemName
	 * @param domainName
	 * @param initState
	 * @param goal
	 * @return
	 */
	public String assembleFactsString(String problemName, String domainName, 
			BeliefBase initState, List<List<Literal>> goal)	{
		String[] init = beliefBaseToInitialStateString(initState);
		//String types = init[0]; - not supporting typing
		String objects = init[1];
		String state = init[2];
		return "(define (problem " + problemName + ")\n(:domain " + domainName + ")\n" + objects +"\n" + state + 
				"\n" + createPddlGoalString(goal) + (qualityMode?"\n" + COST_METRIC:"") + "\n)";
	}

	/**
	 * (Utility method) 
	 * Takes domName and returns a formatted pddl domain name part; i.e. 'wumpus' in returns '(:domain wumpus)'.
	 * </br><br>
	 * Includes requirements (:Strips)
	 * @param domName
	 * @return String
	 */
	public String createPddlDomainString(String domName)	{
		return "(domain " + domName + ")\n" + DOMAIN_REQUIREMENTS; 
	}

	/**
	 * Get a goal string from a set of goal propositions
	 * i.e. <code>  (:goal (and (have agent the-gold) (at agent sq-1-1) ))</code>
	 * @param goal2
	 * @return
	 */
	public String createPddlGoalString(List<List<Literal>> goalDisjSet)	{
		logger.setLevel(UniversalConstants.PLANNER_LOGGER_LEVEL);
		final List<String> goalStatement = new Vector<>();
		for(List<Literal> goalCond: goalDisjSet)	{
			logger.finer(goalCond + "; generating goal states for disjunction");
			final String goal = formGoalFromStateList(goalCond);
			goalStatement.add(goalCond.size()>1? "(and " + goal + ")" :goal);
		}
		return "(:goal\n\t" + formGoal(goalStatement) + "\n)";		
	}

	private String formGoalFromStateList(final List<Literal> goalCond) {
		StringBuilder goal = new StringBuilder();
		Iterator<Literal> gs = goalCond.iterator();
		while(gs.hasNext())	{
			Literal state = gs.next();
			
			String stateString = state.getFunctor() + " ";
			Iterator<Term> args = state.getTerms().iterator();
			while(args.hasNext())	{
				stateString = stateString + " " + args.next().toString().replaceAll("\"", "");
			}
			goal.append("(" + (state.negated() ? "not (" + stateString + ")" : stateString) + ")");
			if(gs.hasNext())	{	goal.append(" ");	}
		}
		return goal.toString();
	}

	private String formGoal(final List<String> goalStatement) {
		String goal = "";
		for(String gs: goalStatement)	{	
			goal = goal + "\n\t\t" + gs;	
		}
		return (goalStatement.size()==1)? goal : "(or" + goal + "\n\t)";
	}

	/**
	 * Takes a set of operators and adds the predicate definitions based upon preconditions and effects
	 * of those operators, then adds in any other predicates from literals in the BB</br>
	 * <code>(:predicates 
	 			(PREDICATE_1_NAME [?A1 ?A2 ... ?AN])
               	(PREDICATE_2_NAME [?A1 ?A2 ... ?AN])
	       ...)</code>
	 * @param o
	 * @param confLimit - used to get predicates with policy based constraints
	 **/
	public String formPredicates(final List<Capability> c, final BeliefBase bb, final List<List<Literal>> goals, final float confLimit)	{
		//store which ones are recorded already....
		final Vector<String> predicateNames = new Vector<String>();
		
		//superset of all unique proposition types, to be translated to pred Strings
		final Vector<Literal> predicateSrcLiterals = new Vector<Literal>();

		//for all capabilities
		for(Capability cap: c)	{
			//get all literals in precondition(s)
			final Vector<String> precondV = new Vector<>();
			for(String s:  cap.getPreconditions())	{
				if(!precondV.contains(s)) {	
					precondV.add(s);	
				}
			}
			for(final String s: cap.getPreconditionsByConfConstraint(confLimit))	{
				if(!precondV.contains(s)) {	
					precondV.add(s);	
				}
			}
			for(final ConfidencePrecondition cp: cap.getConfidenceBasedPreconditions(bb)) {
				if(!precondV.contains(cp.getPreconditionsForPlanning())) {
					precondV.add(cp.getPreconditionsForPlanning());
				}
			}
			
			for(String s: precondV)	{
				if(s.trim().equalsIgnoreCase("true"))	{} //ignore
				else if(s.trim().isEmpty())				{}
				else	{ //got stuff...
					//need to split precond into literal bits
					String[] pre = s.split("&");
					for(String ls: pre)	{
						ls = ls.trim();
						if(ls.startsWith("("))			{	ls = ls.substring(1);					}
						while(ls.endsWith("))"))		{	ls = ls.substring(0, ls.length()-1);	}
						if(ls.startsWith("not ("))		{	ls = ls.replaceFirst("not \\(", "~");	}
						else if(ls.startsWith("not "))	{	ls = ls.replaceFirst("not ", "~");		}
						Literal lit = Literal.parseLiteral(ls);
						if(!predicateNames.contains(lit.getFunctor()) && 
								!lit.getFunctor().equals(CapabilityAwareAgent.HAS_CAPABILITY))	{
							predicateSrcLiterals.add(lit);
							predicateNames.add(lit.getFunctor());
						}
					}//end iterating through precondition literals
				}//end iterating through preconditions
			}//end handling precond literals
			
			//get all literals in effects
			String[] addE = cap.getAddEffects();
			String[] remE = cap.getDeleteEffects();
			for(String s: addE)	{
				Literal l = Literal.parseLiteral(s);
				if(!predicateNames.contains(l.getFunctor()))	{
					predicateSrcLiterals.add(l);
					predicateNames.add(l.getFunctor());
				}
			}
			for(String s: remE)	{
				Literal l = Literal.parseLiteral(s);
				if(!predicateNames.contains(l.getFunctor()))	{
					predicateSrcLiterals.add(l);
					predicateNames.add(l.getFunctor());
				}
			}
		}
		
		//do BB literals
		Iterator<Literal> bbIt = bb.iterator();
		while(bbIt.hasNext())	{
			Literal belief = bbIt.next();
			if(	!predicateNames.contains(belief.getFunctor()) && 
					!belief.getFunctor().equals(CapabilityAwareAgent.HAS_CAPABILITY) )	{
				predicateSrcLiterals.add(belief);
				predicateNames.add(belief.getFunctor());
			}
		}
		
		//goal literals
		for(List<Literal>ll: goals)	{
			for(Literal l:ll)	{
				if(	!predicateNames.contains(l.getFunctor()) && 
						!l.getFunctor().equals(CapabilityAwareAgent.HAS_CAPABILITY) )	{
					predicateSrcLiterals.add(l);
					predicateNames.add(l.getFunctor());
				}
			}
		}
		
		StringBuilder pString = new StringBuilder();
		//take the big props set and form our predicates!
		//got all preds from BB
		for(Literal predicate: predicateSrcLiterals)	{
			String pr = predicate.getFunctor();
			int tCount = predicate.getTerms().size(); //name doesn't matter...
			for(int i=0; i<tCount; i++)	{	pr = pr + " ?B" + i;	}
			//i.e. atJ(AGENT, A) should become atJ ?B0 ?B2
			//B# is purely so we can visually check source of preds, makes no practical impact
			pString.append("\n\t(" + pr + ")"); 
		}
		logger.finer("Generated predicates; \n" + pString);
		return "(:predicates " + pString.toString() + "\n)";
	}

	/**
	 * Method which takes the literals in a given BB and converts to an initial state.  Returns an array
	 * of three strings; objects, objects with types, init state.  
	 * </br>
	 * Note that literals containing numeric terms are omitted.
	 * </br></br><b>(index 0)</b> (objects taken using the [object] annotation for bb literals):</br>
	 * <code>
	  (:objects sq-1-1 sq-1-2 sq-1-3
		    sq-2-1 sq-2-2 sq-2-3
		    the-gold the-arrow
		    agent wumpus)
	 * </code>
	 * </br></br><b>(index 1):</b> (objects with types, i.e. thing(car)[objectType] becomes car - thing)</br>
	 * <code>
	 * ( no type support )
	 * </code>
	 * </br></br><b>(index 2):</b></br>
	 * <code>
	  (:init (adj sq-1-1 sq-1-2) (adj sq-1-2 sq-1-1)
		 (adj sq-1-2 sq-1-3) (adj sq-1-3 sq-1-2)
		 (adj sq-2-1 sq-2-2) (adj sq-2-2 sq-2-1)
		 (adj sq-2-2 sq-2-3) (adj sq-2-3 sq-2-2)
		 (adj sq-1-1 sq-2-1) (adj sq-2-1 sq-1-1)
		 (adj sq-1-2 sq-2-2) (adj sq-2-2 sq-1-2)
		 (adj sq-1-3 sq-2-3) (adj sq-2-3 sq-1-3)
		 (pit sq-1-2)
		 (at the-gold sq-1-3)
		 (is-gold the-gold)
		 (takeable the-gold)
		 (at agent sq-1-1)
		 (alive agent)
		 (have agent the-arrow)
		 (is-arrow the-arrow)
		 (takeable the-arrow)
		 (at wumpus sq-2-3)
		 (alive wumpus))
	 * </code>
	 * @param bb
	 * @return
	 * 
	 * 		while(beliefs.hasNext())	{
			Literal belief = beliefs.next();

			if(!belief.getFunctor().equals("hasCapability") && !hasNumeric(belief.getTerms()) &&
					belief.isGround() && !belief.isRule())	{  //must be ground and not a rule
				//skip hasCap as it has a weird structure and relates to ECs
				//convert to format
				String belString = belief.getFunctor();
				Iterator<Term> args = belief.getTerms().iterator();
				while(args.hasNext())	{
					belString = belString + " " + args.next().toString().replaceAll("\"", "");
				}

				if(belief.negated())	{	belString = "not (" + belString + ")";	}

				belString = "(" + belString + ")";

				if(!belief.negated())	{	init = init + "\n\t " + belString;	}

				//even if negated, still check for types of object....
				if(belief.hasAnnot(UniversalConstants.OBJECT_DEF_ANNOT))	{
					logger.finest("Belief " + belief.toString() + ", is oType");
					//is an object type.... add to objects bit
					String objName = belief.getTerm(0).toString().replaceAll("\"", "");
					if(!objectsCheck.contains(objName))	{
						objects = objects + "\n\t " + objName;
						objectsCheck.add(objName); //to stop duplication
					}

					//todo need to tidy up properly; need to have multiple objects per type
					//i.e. car - ford mercedes suzuki bmw
					//rather than car - ford, car - mercedes etc entries
					types = types + "\n\t" + objName + " - " + belief.getFunctor();
				}

			}
		}
	 */
	public String[] beliefBaseToInitialStateString(final BeliefBase bb)	{
		logger.setLevel(UniversalConstants.PLANNER_LOGGER_LEVEL);
		StringBuilder objects = new StringBuilder();
		Vector<String> objectsCheck = new Vector<String>();
		StringBuilder init = new StringBuilder();
		StringBuilder types = new StringBuilder();
		Iterator<Literal> beliefs = bb.iterator();
		while(beliefs.hasNext())	{
			Literal belief = beliefs.next();

			if(!belief.getFunctor().equals("hasCapability") && !hasNumeric(belief.getTerms()) &&
					belief.isGround() && !belief.isRule())	{  //must be ground and not a rule
				//skip hasCap as it has a weird structure and relates to ECs
				//convert to format
				String belString = belief.getFunctor();
				Iterator<Term> args = belief.getTerms().iterator();
				while(args.hasNext())	{
					belString = belString + " " + args.next().toString().replaceAll("\"", "");
				}

				if(belief.negated())	{	belString = "not (" + belString + ")";	}

				belString = "(" + belString + ")";

				if(!belief.negated())	{	init.append("\n\t " + belString);	}

				//even if negated, still check for types of object....
				if(belief.hasAnnot(UniversalConstants.OBJECT_DEF_ANNOT))	{
					logger.finest("Belief " + belief.toString() + ", is oType");
					//is an object type.... add to objects bit
					String objName = belief.getTerm(0).toString().replaceAll("\"", "");
					if(!objectsCheck.contains(objName))	{
						objects.append("\n\t " + objName);
						objectsCheck.add(objName); //to stop duplication
					}

					//todo need to tidy up properly; need to have multiple objects per type
					//i.e. car - ford mercedes suzuki bmw
					//rather than car - ford, car - mercedes etc entries
					types.append("\n\t" + objName + " - " + belief.getFunctor());
				}

			}
		}
		final String typeString = (types.length()>0?"(:objects " + types.toString() + "\n)":"");
		final String objectString = (objects.length()>0 ?  "(:objects " + objects.toString() + "\n)" : "");
		final String initString = "(:init " + (qualityMode?INIT_COST_METRIC:"") + "\n" + init.toString() + "\n)";
		logger.finer("Generated initial state/domain info; \n" + objects + "\n" + init);
		return new String[]{typeString, objectString, initString}; 
	}

	private final String getPddlOperator(final Capability c, final BeliefBase init, final float minConf)	{
		logger.setLevel(UniversalConstants.PLANNER_LOGGER_LEVEL);
		final Literal sig = c.getSignature();
		final String holderId = c.isExternal() ? ((ExternalCapability)c).getHolderName() : c.getHolder().getId();
		Literal holderPrecond = ASSyntax.createLiteral( holderId, sig.getTerm(0));
		
		if(c.isPrimitive() || c.isExternal())	{			
			String[] preconditions;
			if(minConf <= 0) 	{	preconditions = c.getPreconditions();							}
			else				{	preconditions = c.getPreconditionsByConfConstraint(minConf);	}
			String[] pSub = new String[preconditions.length];
			for(int i=0; i<preconditions.length;i++)	{
				pSub[i] = holderPrecond.toString() + "&" + preconditions[i];
				pSub[i] = LogExpr.parseExpr(pSub[i]).toString();
				logger.finer(sig + ": PC/EC Precondition set (conf limit;" + minConf + ") " + pSub[i]);
			}
			
			ConfidencePrecondition[] confP = c.getConfidenceBasedPreconditions(init);
			final Vector<String> delEff = parseDeleteEffects(c, sig);		
			final Vector<String> addEff = parseAddEffects(c, sig);
			
			String[] add = new String[addEff.size()];
			for(int i=0; i<add.length; i++)	{	add[i] = addEff.get(i);	}
			String[] del = new String[delEff.size()];
			for(int i=0; i<del.length; i++)	{	del[i] = delEff.get(i);	}			
			return (qualityMode ? getPddlOperatorWithConfScore(sig, confP, del, add): getPddlOperatorStatic(sig, pSub, del, add, 1f));
		}
		else	{
			/*
			 * Because of the ASL structure, it is possible to have effects that don't have matching arguments; we
			 * don't represent these in the operator, as it will cause a failure
			 */
			String returned = ""; //set of all pddl ops
			//composite capability
			CompositeCapability cc = (CompositeCapability)c;
			Map<String, Vector<Plan>> means = cc.getPossibleMeans();
			for(String preKey : means.keySet())	{
				Vector<Plan> associated = means.get(preKey);
				
				//precondition - with added holder constraint
				String pre = holderPrecond.toString() + "&" + preKey.toString();
				pre = LogExpr.parseExpr(pre).toString();
				logger.finer(sig + ": PC/EC Precondition set " + pre);
				
				AgentUtils agentUtils = new AgentUtils(c.getHolder());
				
				//do for each plan / set of side-effects
				for(final Plan plan: associated)	{
					Plan p = (Plan) plan.clone();
					Collection<String> pe = agentUtils.getPostEffectsOfPlan(cc.getHolder(), p.getBody());
					Collection<String> remove = new Vector<String>();
					
					//remove effects with arguments that are NOT present in the signature
					for(String eff:pe)	{
						String prepend = eff.substring(0, 1);
						String lit = eff.substring(1);
						Literal literal = Literal.parseLiteral(lit);
						for(Term t: literal.getTerms())	{
							if(!sig.getTerms().contains(t))	{
								logger.warning("Effect " + prepend + " " + lit + 
										" term (" + t + ") not in signature " + sig);
								remove.add(eff);
							}
							else	{
								logger.fine("Effect " + prepend + " " + lit + 
										" term (" + t + ") in signature " + sig);
							}
						}
					}
					pe.removeAll(remove);
					
					//pe into set of add and delete statements....
					//figure out add/deletes...
					Vector<String> add = new Vector<String>();
					Vector<String> del = new Vector<String>();
					for(String s: pe)	{
						if(s.startsWith("+"))		{	add.add(s.substring(1));	}
						else if(s.startsWith("-"))	{	del.add(s.substring(1));	}
					}
					String[] addEffects = new String[add.size()];
					String[] delEffects = new String[del.size()];
					for(int i=0;i<addEffects.length; i++)	{	addEffects[i] = add.get(i);	}
					for(int i=0;i<delEffects.length; i++)	{	delEffects[i] = del.get(i);	}
					
					//TODO need to handle confPre properly
					//Do we need to care about CC case?
					//add pddl string result for this plan
					final float conf = c.getGeneralConfidence(init);
					returned  = returned + getPddlOperatorWithConfScore(sig, new ConfidencePrecondition[]{
							new ConfidencePrecondition(holderId, sig, pre, conf)}, delEffects, addEffects) + "\n";
				}
			}
			return returned.trim();
		}
	}

	private Vector<String> parseDeleteEffects(final Capability c, final Literal sig) {
		Vector<String> delEff = new Vector<>();
		
		//remove effects with arguments that NOT present in the signature, such as may apply
		//for ECs advertising composites
		for(String e: c.getDeleteEffects())	{
			Literal literal = Literal.parseLiteral(e);
			boolean ok = true;
			checkT:
			for(Term t: literal.getTerms())	{
				if(!sig.getTerms().contains(t))	{
					logger.warning("Delete " + literal + " term (" + t + ") not in " + sig);
					ok = false;
					break checkT;
				}
			}
			if(ok)	{		
				logger.fine("Delete " + literal + " is ok for " + sig);
				delEff.add(e);	
			}
		}
		return delEff;
	}

	private Vector<String> parseAddEffects(Capability c, Literal sig) {
		final Vector<String> addEff = new Vector<>();
		for(String e: c.getAddEffects())	{
			Literal literal = Literal.parseLiteral(e);
			boolean ok = true;
			checkT:
			for(Term t: literal.getTerms())	{
				if(!sig.getTerms().contains(t))	{
					logger.warning("Add " + literal + " term (" + t + ") not in " + sig);
					ok = false;
					break checkT;
				}
			}
			if(ok)	{
				logger.fine("Add " + literal + " effect is ok for" + sig);
				addEff.add(e);
			}
		}
		return addEff;
	}
	
	/**
	 * Gets a pddl action definition, i.e.
	 * <code>
	 * (:action move :parameters (?x ?y)
	 *	:precondition (and (ROOM ?x) (ROOM ?y) (at-robby ?x))
	 *	:effect (and (at-robby ?y)	(not (at-robby ?x))))
	 * </code>
	 * Note that we support ternary preconditions in this situation; explicit (~functor(terms...) and implicit
	 * (not functor(terms...)) negated literals are both represented in pddl with the 'not' keyword and closed-world
	 * assumption.
	 * </br></br><B>Does not check effects for contradiction</b>
	 * @param count 
	 */
	public String getPddlOperatorWithConfScore(	final Literal signature, final ConfidencePrecondition[] confPre, 
									final String[] deletes, final String[] adds)	{
		String rt = "";
		for(final ConfidencePrecondition pre: confPre)	{
			if(pre.getConfidence()>0) {
				rt = rt + "\n" + getPddlOperatorSub(signature, pre.getPreconditionsForPlanning(), deletes, adds, pre.getConfidenceMetricValue());
			count++;
			}
		}
		return rt;
	}

	public String getPddlOperatorStatic(	final Literal signature, final String[] pres, 
											final String[] deletes, final String[] adds, final float conf)	{
		String rt = "";
		for (final String pre : pres) {
			rt = rt + "\n" + getPddlOperatorSub(signature, pre, deletes, adds, new BigDecimal(conf));
			count++;
		}
		return rt;
	}
	
	//this is a hack because of lpg-td bugs associated with employing disjunction...
	private String getPddlOperatorSub(final Literal signature, final String pre, 
			final String[] deletes, final String[] adds, final BigDecimal confidenceScore)	{
		logger.setLevel(UniversalConstants.PLANNER_LOGGER_LEVEL);
		logger.info("Get PDDL for " + signature + "(" + pre + ") with " + deletes.length + " deletes, " + 
				adds.length + " additions, cost " + confidenceScore);
		
		String functor = "OP_" + count + "_" + signature.getFunctor();
		String params = "(" + termsToPddl(signature.getTerms()) + ")";
		String preconds = "";
		String[] literals = pre.split("&");
		String ps = "";
		//handle lits...

		for(String ls: literals)	{
			logger.severe("Handle " + ls + "\n" + pre);
			ls = ls.trim();
			//tidy up brackets, ternary negation
			if(ls.startsWith("("))		{	ls = ls.substring(1);	}
			while(ls.endsWith("))"))	{	ls = ls.substring(0, ls.length()-1);	}
			if(ls.startsWith("not ("))	{	ls = ls.replaceFirst("not \\(", "~");	}
			logger.severe("Parse " + ls);
			Literal lit = Literal.parseLiteral(ls);
			ps = ps + " " + literalToPddl(lit);
		}

		//done; add as an '(and ())' set if we have multiples
		if(literals.length>0)	{
			preconds = preconds + "\n\t\t\t(and " + ps.trim() + ")"; 
		}
		else	{
			preconds = preconds +"(" + ps.trim() + ")";
		}


		//we don't support disjunction in effects...
		String effects = "";
		Vector<Literal> effL = setupEffectLiteralSet(deletes, adds);
		
		for(Literal l: effL)	{	
			effects = effects + " " + literalToPddl(l);
		}
		
		effects = "(and " + effects + (qualityMode && confidenceScore.floatValue()!=0 ? " (increase (" + ACTION_COST + ") " + confidenceScore.toPlainString() + ") )":" )");	
		return "(:action " + functor + " :parameters " + params + "\n\t:precondition " + preconds + "\n\t:effect " + effects + "\n)";
	}

	private Vector<Literal> setupEffectLiteralSet(final String[] deletes, final String[] adds) {
		Vector<Literal> effL = new Vector<>();
		
		//effects are add(+) or delete(-) literal statements ala strips
		//4 types - +thing, +~thing, -thing, -~thing; equates to; thing, ~thing, ~thing, thing
		//adds; fairly simple
		for(String addEff: adds)	{
			Literal l = Literal.parseLiteral(addEff);
			if(!effL.contains(l))	{	effL.add(l);	}
			
			Literal lNeg = l.copy();
			lNeg.setNegated(l.negated()); //this is a very counter-intuitive API...
			//validity check
			if(effL.contains(l) && effL.contains(lNeg))	{
				throw new RuntimeException("Effects contains both " + l + " and " + lNeg);
			}
		}
		
		//removes
		for(String remEff: deletes)	{
			if(remEff.startsWith("~"))	{	remEff = remEff.substring(1);	}
			else						{	remEff = "~" + remEff;	}
			Literal l = Literal.parseLiteral(remEff);
			if(!effL.contains(l))	{	effL.add(l);	}
			
			Literal lNeg = l.copy();
			lNeg.setNegated(l.negated());
			//validity check
			if(effL.contains(l) && effL.contains(lNeg))	{
				throw new RuntimeException("Effects contains both " + l + " and " + lNeg);
			}
		}
		return effL;
	}

	/*
	; Time 1.53
	; Search time 0.02
	; Parsing time 1.52
	; Mutex time 0.00
	; Quality 3
	 */
	public final float extractQualityOfLpgPlanFile(final File in) throws IOException	{
		return extractFloat(in, "; Quality");
	}
	public final float extractSearchTimeForLpgPlan(final File in) throws IOException	{
		return extractFloat(in, "; Search time ");
	}

	public final float extractParsingTimeForLpgPlan(final File in)	throws IOException	{
		return extractFloat(in, "; Parsing time ");
	}
	
	public final float extractMutexTimeForLpgPlan(final File in)	throws IOException	{
		return extractFloat(in, "; Mutex time");
	}
	
			
	private float extractFloat(final File in, final String prepend) throws FileNotFoundException, IOException {
		final BufferedReader reader  = new BufferedReader(new FileReader(in));
		String line = reader.readLine();
		while(line!=null)	{
			if(line.trim().startsWith(prepend)) {
				reader.close();
				return Float.parseFloat(line.replaceAll(prepend, "").trim());
			}
			reader.readLine();
		}
		reader.close();
		return 0;
	}

	/**
	 * Takes an LPG generated file and converts it into a set of asl action statements
	 * i.e</br>
	 * <code>
	 * Time 0.01
	 *	
	 *0:   (OP_2_MOVE TRUCK1 J5_J3 J3 J5) [1]
	 *1:   (OP_2_MOVE TRUCK1 J1_J5 J5 J1) [1]
	 *2:   (OP_2_MOVE TRUCK1 J1_J5 J1 J5) [1]
	 *3:   (OP_2_MOVE TRUCK1 J1_J5 J5 J1) [1]
	 *4:   (OP_0_LOAD TRUCK1 J1 CARGO1) [1]
	 *5:   (OP_2_MOVE TRUCK1 J1_J5 J1 J5) [1]
	 *6:   (OP_2_MOVE TRUCK1 J5_J3 J5 J3) [1]
	 *7:   (OP_2_MOVE TRUCK1 J3_J4 J3 J4) [1]
	 *8:   (OP_0_UNLOAD TRUCK1 J4 CARGO1) [1]
	 * </code>
	 * </br></br>
	 * Should become</br>
	 * <code>move(truck1, j5_j4, j3, j5)...etc<code>
	 * @param in
	 * @return
	 */
	public final String[] lpgPlanFileToActions(File in, Collection<String> capabilityNames)	{
		logger.setLevel(UniversalConstants.PLANNER_LOGGER_LEVEL);
		try{
			Vector<String> s = new Vector<String>();
			//tokenization
			BufferedReader reader  = new BufferedReader(new FileReader(in));
			String line = reader.readLine();
			while(line!=null)	{
				line = line.trim();
				if(line.startsWith("Time"))	{	logger.info("Plan " + line);	}
				//empty plan file
				else if(line.startsWith("no solution"))	{
					logger.severe("No solution found; " + in.getAbsolutePath());
					reader.close();
					return new String[0];
				}
				else if(!line.startsWith(";") && line.length()>0 && line.contains(":"))	{
					//format -> 0:   (OP_13BULLDOZER1_MOVEANDCLEAR BULLDOZER1 J1_J3 J3 J1) [1]
					//pos: (functor args...) [cost]
					String[] colonSplit = line.split(":");
					
					if(colonSplit.length!=2)	{
						logger.severe(	"Serious planning error for line " + line + 
										", does not have 2 parts for split-colon?!");
					}
					
					//need to map the functor to correct case and lowerCase all args
					String action = colonSplit[1].split("\\[")[0].trim().substring(1); 
					//removed start and end bits for index and cost
					action = action.substring(0, action.length()-1); 
					//removed bracketing
					//tokenization-type-stuff
					String[] tokens = action.split(" "); //easy peasy...
					String handled = "";
					String functor = stripOperator(tokens[0]);
					normalizeFunctorCase:
						for(String cName: capabilityNames)	{
							if(cName.equalsIgnoreCase(functor))	{	
								functor = cName;	
								break normalizeFunctorCase;
							}
						}
					//all following tokens should be ground arguments
					for(int i=1; i<tokens.length; i++)	{
						handled = handled + "\"" + tokens[i].toLowerCase() + "\"";
						if(i<tokens.length-1)	{	handled = handled + ", ";	}
					}
					handled = functor + "(" + handled + ")";
					s.add(handled);
				}
				line = reader.readLine();

			}
			reader.close();
			
			//in no-op plans, returns an empty string
			String[] body = new String[s.size()];
			for(int i=0; i<body.length; i++)	{	body[i] = s.get(i);	}
			return body;
		}
		catch(Exception e)	{
			String trace = e.toString();
			for(StackTraceElement s: e.getStackTrace())	{	
				trace = trace + "\n\t" + s.toString();	
			}
			logger.severe(trace);
			return new String[]{};
		}
	}
	
	/**
	 * Removes the stuff prepended to a functor in order to allow multiple preconditions;
	 * i.e. put in op_0_move, get move back; put op_holder_move, get move back
	 * @param functor
	 * @return
	 */
	public String stripOperator(final String functor)	{
		logger.setLevel(UniversalConstants.PLANNER_LOGGER_LEVEL);
		String f = functor;
		logger.finest("Handle functor " + functor + "...");
		try	{
			if( 	(f.startsWith("op_")||f.startsWith("OP_")	) &&
					f.split("_").length==3)	{
				f = f.substring(3); //trim op_ at start
				f = f.split("_")[1]; //last section is the functor name
			}
		}	catch(NumberFormatException n){ 
			logger.warning(n.toString());
			return functor; //no change!
		}//thrown if 2nd arg is not a number
		logger.finest("stripOperator: in= " + functor + "\n\t\tout= " + f);
		return f;
	}

	public static void main(String[] args) throws Exception	{
		Literal thing1 = ASSyntax.createLiteral("thing", ASSyntax.createString("car"));
		Literal thing2 = ASSyntax.createLiteral("thing", ASSyntax.createString("aeroplane"));
		Literal thing3 = ASSyntax.createLiteral("vehicle", ASSyntax.createString("car"));
		thing1.addAnnot(UniversalConstants.OBJECT_DEF_ANNOT);
		thing1.addAnnot(ASSyntax.createLiteral("source", ASSyntax.createString("percept")));
		thing2.addAnnot(UniversalConstants.OBJECT_DEF_ANNOT);
		thing2.addAnnot(ASSyntax.createLiteral("source", ASSyntax.createString("percept")));
		thing3.addAnnot(UniversalConstants.OBJECT_DEF_ANNOT);
		thing3.addAnnot(ASSyntax.createLiteral("source", ASSyntax.createString("percept")));
		System.out.println(thing1 + " has annot?(1) " + thing1.hasAnnot(UniversalConstants.OBJECT_DEF_ANNOT));
		System.out.println(thing1 + " has annot?(2) " + thing1.getAnnots(UniversalConstants.OBJECT_DEF_ANNOT.getFunctor()));
		System.out.println(thing1 + " has annot?(3) " + thing1.getAnnots().contains(UniversalConstants.OBJECT_DEF_ANNOT));
		Literal test1 = ASSyntax.createLiteral("testFunctor2", ASSyntax.createString("arg1"));
		Literal test2 = ASSyntax.createLiteral("testFunctor3", ASSyntax.createString("arg13"),ASSyntax.createString("arg23"));
		Literal test3 = ASSyntax.createLiteral("testFunctor4");
		BeliefBase bb = new DefaultBeliefBase();
		bb.add(thing1);
		bb.add(thing2);
		bb.add(thing3);
		bb.add(test1);
		bb.add(test2);
		bb.add(test3);
	}
}
