package agent.planning.graphplan;

import java.io.File;
import java.io.FileWriter;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import agent.model.capability.Capability;
import agent.model.capability.ExternalCapability;
import agent.planning.PlanResult;
import agent.planning.PlannerInf;
import agent.planning.graphplan.LGP.Mode;
import agent.type.MaintainingAgent;
import common.Analytics;
import common.Analytics.AnalyticsOperations;
import common.StatsRecorder;
import common.UniversalConstants;
import jason.asSyntax.ASSyntax;
import jason.asSyntax.Literal;
import jason.asSyntax.parser.ParseException;
import jason.bb.BeliefBase;

/**
 * @author alan white
 *
 * Module used to provide access to a LGP-TD planner.  Heavily uses methods within PlanningUtils; only
 * performs deterministic planning without probabalistic configuration, so strips out any capabilities
 * with suboptimal confidence values.
 */
public class LGPModule implements PlannerInf {

	/**
	 * Used for unique identification
	 */
	private static int BASE_COUNTER = 0;

	/**
	 * Holder agent; used to ref capabilities
	 */
	private MaintainingAgent agent;

	/**
	 * Logger object: agentName:className
	 */
	private Logger logger;
	
	/**
	 * LGP instance
	 */
	private LGP myLgp;

	/**
	 * Planner mode
	 */
	private Mode mode = Mode.speed;
	
	private final LPGPlanningUtils utils;
	
	/**
	 * Creates a module that we can use for planning
	 * @param agent
	 * @param mode 
	 */
	public LGPModule(final MaintainingAgent agent, Mode mode)	{				
		logger = Logger.getLogger(agent.getLogger().getName() + ":" + getClass().getSimpleName());
		logger.setLevel(UniversalConstants.PLANNER_LOGGER_LEVEL);	
		this.agent = agent;
		this.mode = mode;
		myLgp = new LGP(agent.getId(), mode);
		utils = new LPGPlanningUtils(mode.equals(Mode.quality));
	}
	
	@Override
	public PlanResult getPlan(String agid, Literal trigger, BeliefBase init, List<List<Literal>> goal, 
			Collection<Capability> caps, 
			Collection<ExternalCapability> ext, float minConf)	{
		Literal tClone = trigger.copy();
		tClone.clearAnnots();
		agid = agid + "_" + tClone.toString().replaceAll("\"","").trim();
		
		long elapsed = System.nanoTime();
		PlanResult result = formPlan(agid, trigger, init, goal, caps, ext, minConf, Integer.MAX_VALUE);
		elapsed = System.nanoTime() - elapsed;
		if(result == null || result.getPlan() == null)	{
			Analytics.getInstance().logTime(AnalyticsOperations.failedPlanning, elapsed);
		}
		else	{
			Analytics.getInstance().logTime(AnalyticsOperations.successPlanning, elapsed);
		}
		return result;
	}

	private final Object synch = new Object();

	/**
	 * Returns a generated plan, wrapper in a PlanResult object with stats
	 */
	private PlanResult formPlan(final String callerid, final Literal trigger, final BeliefBase init, final List<List<Literal>> goal, 
			final Collection<Capability> caps, final Collection<ExternalCapability> ext, final float minConf, final int levelLimit)	{
		if(goal == null)	{	
			throw new RuntimeException("Null goal given in planner call");
		} //error case!
		synchronized(synch)	{
			PlanResult p = null;
			//check if goal already holds in BB...
			if(goal.isEmpty() || alreadyHoldsInBB(init, goal))	{
				return formEmptyPlan(trigger, goal);
			}
			
			try	{
				final int COUNTER = BASE_COUNTER++;
				long start = System.currentTimeMillis();
				//init = init.clone();

				//add agent identifier literals into inital context bb...
				//this is required to tie external capability operators to their actors
				Set<String> agNames = agent.getTS().getUserAgArch().getRuntimeServices().getAgentsNames();
				for(String name: agNames)	{
					if(!agNames.equals(agent.getTS().getUserAgArch().getAgName()))	{
						Literal identifier = ASSyntax.createLiteral(name, ASSyntax.createString(name));
						identifier.addAnnot(UniversalConstants.OBJECT_DEF_ANNOT); //required for adding object def
						if(init.contains(identifier) == null)	{	
							logger.info("Adding identified: " + identifier +  " to initial state");
							init.add(identifier);	
						}	
					}
				}

				String id = agent.getId() + "_GeneratedPlan_" + COUNTER ;
				id = id.replaceAll("\"", "");
				logger.info("Starting getPlan...");

				logger.fine(COUNTER + " getPlan;  Domain creation done;");

				//pddl output here (temp)
				Collection<Capability> cInto = new Vector<Capability>();
				Collection<ExternalCapability> ecInto = new Vector<ExternalCapability>();

				/*
				 * Filtering.  We filter out capabilities below the 'drop' threshold from being considered, and
				 * we drop external capabilities if we have a local equivalent.
				 */
				
				Vector<String> capFunctors = new Vector<String>();
				for(Capability c: caps)	{
					if((c.getGeneralConfidence(init) > minConf || mode.equals(Mode.quality)) && !ecInto.contains(c))	{	
						cInto.add(c);	
						String functor = c.getSignature().getFunctor();
						if(!capFunctors.contains(functor))	{	capFunctors.add(functor);	}
					}
				}
				for(ExternalCapability c: ext)	{
					if((c.getGeneralConfidence(init) > minConf || mode.equals(Mode.quality)) && !ecInto.contains(c) && agNames.contains(c.getHolderName()))	{	
						ecInto.add(c);	
						String functor = c.getSignature().getFunctor();
						//trim out functors for external versions of local c's
						if(!capFunctors.contains(functor))	{	capFunctors.add(functor);	}
					}
					else if(!agNames.contains(c.getHolderName()))	{
						throw new RuntimeException("EC with unknown holder; " + c.toString());
					}
				}
				logger.severe("PDDL will have " + cInto.size() + " Cs and " + ecInto.size() + " ECs\n\t" + ecInto);

				long fileFormTime = System.nanoTime();
				String operatorsPddl = utils.assemblePddlDomain("dynamic", cInto, ecInto, goal, init, minConf);
				String problemPddl = utils.assembleFactsString("planningProblem", "dynamic", init, goal);
				logger.fine("\nPDDL operators file contents;\n\n" + operatorsPddl);
				logger.fine("\nPDDL problem file contents;\n\n" + problemPddl);

				String pFile = LPGPlanningUtils.PLANNING_DIR + "\\" + COUNTER + "_problem" + "_" + callerid +".pddl";
				String oFile = LPGPlanningUtils.PLANNING_DIR + "\\" + COUNTER + "_operators" + "_" + callerid + ".pddl";
				FileWriter out = new FileWriter(pFile);
				logger.fine("Writing problem file: " + pFile);
				out.write(problemPddl);
				out.close();
				logger.fine("Writing operators file: " + oFile);
				out = new FileWriter(oFile);
				out.write(operatorsPddl);
				out.close();
				fileFormTime = System.nanoTime() - fileFormTime;
				
				Analytics.getInstance().logTime(AnalyticsOperations.planFileForming, fileFormTime);
				
				File output = new File(LPGPlanningUtils.PLANNING_DIR + "\\" + COUNTER + "_plan_" + callerid);
				final String outFile = output.getAbsolutePath();

				long elapsedT = System.nanoTime();
				boolean result = myLgp.callLpgTd(oFile, pFile, outFile);
				elapsedT = System.nanoTime() - elapsedT;
				
				logger.info(	trigger + " | Calling planner; -o " + oFile + " -p " + pFile + " -out " + outFile + 
								"\nPlanning took " + ((System.currentTimeMillis()-start)/1000) + "s/ " + elapsedT + "ns");
				
				Analytics.getInstance().logTime(AnalyticsOperations.formingPlans, elapsedT);
				
				//parse result
				if(result)	{
					long fileReadTime = System.nanoTime();
					File dir = new File(LPGPlanningUtils.PLANNING_DIR);
					File[] plans = dir.listFiles(new java.io.FileFilter(){
						@Override
						public boolean accept(File f) {
							return f.getName().startsWith(COUNTER + "_plan_" + callerid); 
						}
					});

					if(plans.length==0)	{	
						//get dir
						String fst = "";
						for(File f: dir.listFiles())	{	
							fst = fst + "\n>>" + f.getName() + "(" + f.getName().startsWith(COUNTER + "_plan_" + callerid) + ")";	
						}
						fst = fst.trim();
						logger.severe(callerid + "No plans found in " + dir.getAbsolutePath() + fst  + 
								"\nFilter " + output);
						if(!LPGPlanningUtils.PERSIST_FILES)	{
							new File(oFile).delete();
							new File(pFile).delete();
							new File(outFile).delete();
						}
						return new PlanResult(elapsedT);
					}
					else	{	logger.info("Got " + plans.length + " plan results");	}

					output = plans[plans.length-1]; //want the last file if >1, for the case of 'n' mode
					String[] pActions = utils.lpgPlanFileToActions(output, capFunctors);
					
					fileReadTime = System.nanoTime() - fileReadTime;
					Analytics.getInstance().logTime(AnalyticsOperations.planFileReading, fileReadTime);
					
					if(pActions.length==0)	{
						logger.severe("Got an empty body array!");
						if(!LPGPlanningUtils.PERSIST_FILES)	{
							new File(oFile).delete();
							new File(pFile).delete();
						}
						new File(outFile).delete();
						return new PlanResult(elapsedT);
					}
					
					//trigger
					String planTriggerString = "+!" + trigger;

					//don't care about preconds for plan, as not adding to PL
					//precondition checks should thus only occur at the im level, on a per-action basis...
					String planContextString = "true";
					//action set
					String planBodyString = "";
					for(int i=0; i<pActions.length;i++)	{
						if(i!=0)	{	planBodyString = planBodyString + "; ";	}
						planBodyString = planBodyString + pActions[i];
					}
					planBodyString = ".print(\"Starting generated plan " + id + "\");" + planBodyString + ".";

					String asl = "@" + id + "\n" + 
							planTriggerString + ":" + planContextString + 
							"\n\t\t<-" + planBodyString;
					logger.finer("Parsing plan:\n" + asl); 

					p = new PlanResult( ASSyntax.parsePlan(asl), elapsedT);			
					logger.info(pFile + " > Plan result: " + output.getName() + "\n" + p);
				}
				else	{
					logger.warning(pFile + " > No result found for task " + goal);
					p = new PlanResult(elapsedT);
				}

				if(!LPGPlanningUtils.PERSIST_FILES)	{
					new File(oFile).delete();
					new File(pFile).delete();
					new File(outFile).delete();
				}

				return p;
			}
			catch(Exception e)	{
				String eS = BASE_COUNTER + " planning exception!  " + e.toString();
				for(StackTraceElement st: e.getStackTrace())	{	eS = eS + "\n\t" + st.toString();	}
				logger.severe(eS);
				return p; //may be null, might be set - depends on failure point
			}
		}
	}

	private PlanResult formEmptyPlan(Literal trigger, List<List<Literal>> goal) {
		try {
			String id = agent.getId() + "_noop_" + ++BASE_COUNTER;
			id = id.replaceAll("\"", "");
			String asl = "@" + id + "\n+!" + trigger + ":true\n\t\t<-.print(\"no-op plan\").";
			logger.severe("All goal literals " + goal + " hold in the BB!\n; using no-op plan\n" + asl);
			StatsRecorder.getInstance().recordString(goal + " for planning task " + trigger + " already true, return no-op");
			return new PlanResult(ASSyntax.parsePlan(asl), 0);
		} catch (ParseException e) {
			Level old = logger.getLevel();
			logger.setLevel(Level.SEVERE);
			String es = "";
			for(StackTraceElement s: e.getStackTrace()) {	es = es + "\n" + s;	}
			logger.severe(e + es);
			logger.setLevel(old);
			return null;
		}
	}

	private boolean alreadyHoldsInBB(BeliefBase init, List<List<Literal>> goal) {
		boolean holds = false;
		if(goal.size()==1)	{
			holds = true;
			for (Literal l : goal.get(0)) {
				String ls = l.toString();
				if (ls.startsWith("~")) {
					Literal lc = Literal.parseLiteral(ls.substring(1));
					if (init.contains(lc) != null) {
						logger.severe("Negated " + lc + " is in the BB");
						return false;
					}
				} else {
					if (init.contains(l) == null) {
						logger.severe("Non-Negated " + l + " is in the BB");
						return false;
					}
				}
			}
		}
		return holds;
	}

	@Override
	public Mode getPlanningMode() {
		return this.mode;
	}
}
