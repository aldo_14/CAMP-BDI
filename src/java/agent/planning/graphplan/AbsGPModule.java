package agent.planning.graphplan;

import jason.asSyntax.Literal;
import jason.bb.BeliefBase;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.logging.Logger;

import agent.planning.PlannerInf;
import agent.planning.graphplan.LGP.Mode;
import agent.type.MaintainingAgent;

import common.UniversalConstants;

public abstract class AbsGPModule {
	/** Logger object for debug; uses PLANNER_LOGGER_LEVEL */
	protected Logger logger;
	
	/**
	 * Holder agent; used for some data referencing, but not directly modified.  Plan
	 * insertation, triggering, etc should be performed by the method/class calling the 
	 * planner.
	 */
	protected MaintainingAgent agent;
	
	/**
	 * Creates a module that we can use for planning
	 * @param agent
	 */
	public AbsGPModule(MaintainingAgent agent)	{	
		logger = Logger.getLogger(agent.getLogger().getName() + ":" + getClass().getSimpleName());
		logger.setLevel(UniversalConstants.PLANNER_LOGGER_LEVEL);	
		this.agent = agent;
	}

	/**
	 * Gets a belief base that can be used by the planner
	 * @return List of literals that can be converted into an initial state
	 */
	protected List<Literal> getBeliefBaseForPlanner(BeliefBase bb)	{
		List<Literal> beliefs = new Vector<Literal>();
		
		Iterator<Literal> it = bb.iterator();
		while(it.hasNext())	{	beliefs.add(it.next().copy());	}
		return beliefs;
	}
	
	
	public static final PlannerInf factory(MaintainingAgent ag, Mode planningMode)	{
		return new LGPModule(ag, planningMode);
	}
}
