package agent.planning;

import jason.asSyntax.Plan;
import jason.asSyntax.PlanBody;

/**
 * 
 * @author Alan
 * Wraps a plan and generation time for stats purposes
 */
public class PlanResult {
	private final Plan plan;
	private final long genTime;
	private final int length;
	
	/**
	 * Plan found
	 * @param p - Plan
	 * @param t - generation time
	 */
	public PlanResult(Plan p, long t)	{
		plan = p;
		genTime = t;
		length = calcPlanLength();
	}
	
	/**
	 * No plan found
	 * @param t - generation time
	 */
	public PlanResult(long t)	{
		plan = null;
		length = 0;
		genTime = t;
	}
	
	public Plan	getPlan()			{	return plan;	}
	public long getGenerationTime()	{	return genTime;	}
	private int calcPlanLength()		{
		int i = 0;
		if(plan == null)	{	return i;	} //sanity check
		else	{
			PlanBody p = plan.getBody();
			while(p!=null)	{ 
				//don't count internal actions...
				if(!p.isInternalAction())	{	i++;	}
				p = p.getBodyNext();
			}
			return i;
		}
	}
	
	public int getPlanLength()	{	return length;	}
	
	public String toString()	{
		if(plan!=null)	{
			return "Plan found (" + genTime + "ns, " + length + " actions);\n" + plan.toASString();
		}
		else	{
			return "No plan found (" + genTime + "ns)";
		}
	}
}
