package agent.planning;

import jason.asSyntax.Literal;
import jason.asSyntax.Term;
import jason.bb.BeliefBase;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.logging.Logger;

import agent.model.capability.Capability;
import agent.type.CapabilityAwareAgent;

import common.UniversalConstants;

/**
 * 
 * @author Alan White
 * Generic (i.e. not bound to planner implementation) utilitis for planning
 */
public class PlanningUtils {
	protected static Logger logger = Logger.getLogger("Planning Utilities");

	/**
	 * Where to store temp domains/problems, output results to, and look for the planner exe(s)
	 */
	public static final String PLANNING_DIR = System.getProperty("user.dir") + "\\planner";
	
	/**
	 * If true, don't delete the pddl and result files after each planner execution.
	 */
	public static final boolean PERSIST_FILES = true;
	
	
	/**
	 * Return true if ANY term in terms is numeric. </br>
	 * i.e. thing(A,B) returns false, thing(A,1) returns true
	 */
	protected static boolean hasNumeric(final List<Term> terms) {
		for(final Term t: terms)	{
			if(t.isArithExpr() || t.isNumeric())	{	return true;	}
		}
		return false;
	}
	
	/**
	 * Returns a collection of all the functors agent <i>a</i> can use in planning domains; takes all
	 * functors of all capability signatures.
	 * @param a - a CapabilityAwareAgent
	 * @return Collection of strings
	 */
	protected Collection<String> getCapabilityFunctorSet(CapabilityAwareAgent a)	{
		Vector<String> rt = new Vector<String>();
		Vector<Capability> allCs = new Vector<Capability>();
		allCs.addAll(a.getPrimitiveCapabilities().values());
		allCs.addAll(a.getCompositeCapabilities().values());
		allCs.addAll(a.getExtCapabilities());
		
		for(Capability c: allCs)	{
			String functor = c.getSignature().getFunctor();
			if(!rt.contains(functor))	{	rt.add(functor);	}
		}
	
		return rt;
	}
	
	/**
	 * Returns goal literals NOT in the current beliefs
	 * @param goals
	 * @param bb
	 * @return
	 */
	protected static List<Literal> removeExistingStatesFromList(final List<Literal> goals, 
																final BeliefBase bb)	{
		final List<Literal> returned = new ArrayList<>();
		for(Literal goal: goals)	{
			if(bb.contains(goal)==null)	{
				returned.add(goal.copy());
			}
		}
		return returned;
	}
	
	/**
	 * Returns true if we cannot achieve ANY passed in goal state set
	 * @param goals goal state set
	 * @param caps agent capability set
	 * @param beliefs current beliefs
	 */
	public static boolean isGoalUnachievable(
			List<List<Literal>> goals, Collection<Capability> caps, BeliefBase beliefs)	{
		Collection<String> achievableStateFunctors = getAchievableEffectNames(caps);
		for(List<Literal> goalLiterals: goals)	{
			goalLiterals = removeExistingStatesFromList(goalLiterals, beliefs);
			if(!cannotAchieve(goalLiterals, achievableStateFunctors))	{
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Goes through all the effects defined in caps and returns a set of the corresponding
	 * predicate names; to be used for loose intractability heuristics.  Does not discriminate
	 * between addition/deletion and/or negation, just the name of the predicate.
	 * @param caps
	 * @return  collection of strings, i.e. {atJ, onR, mortal} 
	 */
	protected static Collection<String> getAchievableEffectNames(Collection<Capability> caps)	{
		ArrayList<String> effLitNames = new ArrayList<>();
		for(Capability c: caps)	{
			for(String eff: c.getAddEffects())	{
				Literal effectLiteral = Literal.parseLiteral(eff);
				if(!effLitNames.contains(effectLiteral.getFunctor()))	{
					effLitNames.add(effectLiteral.getFunctor());
				}
			}
			for(String eff: c.getDeleteEffects())	{
				Literal effectLiteral = Literal.parseLiteral(eff);
				if(!effLitNames.contains(effectLiteral.getFunctor()))	{
					effLitNames.add(effectLiteral.getFunctor());
				}
			}
		}
		return effLitNames;
	}
	
	/**
	 * Returns true if definitely CANNOT achieve a given goal
	 * @param goal, a list of goal state literals (intersection/disjunctive)
	 * @param capSet names of agent capabilities, compared against functors
	 */
	protected static boolean cannotAchieve(List<Literal> goal, Collection<String> capSet)	{
		logger.setLevel(UniversalConstants.PLANNER_LOGGER_LEVEL);
		for(Literal l: goal)	{
			if(!capSet.contains(l.getFunctor()))	{
				logger.severe("cannotAchieve; " + l + " not in " + capSet);
				return true;
			}
		}
		return false; //not disproven!
	}
	
	/**
	 * Converts a jason literal to PDDL rep, with the semantics depending on whether the lit is
	 * ground or not.  If unground, terms are replaced by ?A#, where # is a number 0+ to distinguish
	 * between terms.  Negation is represented by appending a not (i.e. not (functor ?a0 ?a1)).  The
	 * return value is bracketed.
	 * </bR></br>
	 * NB: this method does not handle any typing!
	 * @param in
	 * @return string for pddl rep of literal, i.e. atJ(Truck1, B) becomes (atJ Truck1 b).
	 */
	public static String literalToPddl(final Literal in)	{
		logger.setLevel(UniversalConstants.PLANNER_LOGGER_LEVEL);
		String pddl = (in.negated()?"(not ":"") + "(" + in.getFunctor() + " " + termsToPddl(in.getTerms()) + ")" + (in.negated()?")":"");
		logger.finest(in.toString() + " converted to " + pddl);
		return pddl;
	}

	/**
	 * Converts a list of Jason Term objects into a pddl string, with
	 * unground terms prepended by '?'
	 */
	protected static String termsToPddl(final List<Term> termList) {
		final StringBuilder pddlBuilder = new StringBuilder();
		final Iterator<Term> terms = termList.iterator();
		while(terms.hasNext())	{
			final Term curr = terms.next();
			pddlBuilder.append((curr.isGround()?" ":" ?") + curr.toString());
		}
		return  pddlBuilder.toString().replaceAll("\"", "").trim();
	}
}
