package agent.model.capability;

import java.util.HashMap;
import java.util.logging.Logger;

import agent.model.goal.GoalTask;
import agent.model.goal.factory.TaskFactory;
import agent.type.CapabilityAwareAgent;
import common.UniversalConstants;
import common.UniversalConstants.WorldAction;
import jason.asSyntax.ListTerm;
import jason.asSyntax.Plan;
import jason.asSyntax.Trigger.TEOperator;
import jason.asSyntax.Trigger.TEType;
import vehicle.HeliAgent;
import vehicle.TruckworldAgent;
import vehicle.VehicleAgent;
import vehicle.capabilities.DecontaminatePlanCapability;
import vehicle.capabilities.DoRouteCapability;
import vehicle.capabilities.MHQSecureCapability;
import vehicle.capabilities.MoveToCapability;
import vehicle.capabilities.UnblockPlanCapability;
import vehicle.capabilities.heli.HeliMoveTo;

public class CompositeCapabilityManager {
	protected Logger logger;
	protected CapabilityAwareAgent agent;
	
	/**
	 * This is used to temporarily hold a 'record' of factory created capabilities; as plans are fed into the factory, then
	 * they may be added as context-sensitive options to capabilities for the same triggering label (if a capability for
	 * that level - i.e. an entry in that string key - exists)
	 */
	private HashMap<String, CompositeCapability> existing	=	new HashMap<String, CompositeCapability>();
	private HashMap<String, CompositeCapability> repair		=	new HashMap<String, CompositeCapability>();

	public CompositeCapabilityManager(CapabilityAwareAgent agent) {
		this.agent = agent;
		logger = Logger.getLogger(agent.getId() + ":" + getClass().getSimpleName());
		logger.setLevel(UniversalConstants.CAPABILITY_LOGGER_LEVEL);
	}

	/**
	 * Creates and returns a composite capability object from the given information
	 * @param agent The agent that will hold the capability
	 * @param p The plan that will be used to infer the composite->subcapability relationship(s)
	 * @return CompositeCapability
	 */
	public Capability factory(Plan p) {
		logger.info("Capability manager; Adding plan " + p);
		
		TaskFactory factory = agent.getTaskFactory();
		
		ListTerm annots = p.getLabel().getAnnots();
		
		logger.info("Annots: " + annots);
		
		if(p.getTrigger()==null) return null;

		String name = p.getTrigger().getLiteral().getFunctor();
		
		//if true, this is a plan to recover from a goal failure
		boolean repairPlan = 	p.getTrigger().getOperator().equals(TEOperator.del) && 
				p.getTrigger().getType().equals(TEType.achieve) &&
				!p.getTrigger().getType().equals(TEType.belief);
		logger.finer("Capability " + name + " repair? = " + repairPlan + 
				", trigger=" + p.getTrigger().toString());

		//we don't handle capabilities not directly related to a goal addition
		if(p.getTrigger().getType().equals(TEType.belief)
			|| p.getTrigger().getType().equals(TEType.test)) 	{
			logger.finer("Not appropriate for a c-capability: " + p.getTrigger().toString());
			return null;
		}

		logger.finest("Creating composite capability object: " + name);

		boolean alreadyExists = false;
		if(repairPlan)	{	alreadyExists = repair.containsKey(name);	}
		else			{	alreadyExists = existing.containsKey(name);	}

		/*
		 * Modify/create a composite capability to represent this plan know-how.  If the
		 * plan is to repair - i.e. respond to goal failure - it is placed in a seperate 
		 * hashmap. 
		 */
		CompositeCapability c = null;
		if(alreadyExists){//modify existing plan
			logger.finer("Modifying existing capability; " + name);
			if(!repairPlan)	{	
				c = existing.get(name);	
				c.addMeans(p);	}
			else			{	
				c = repair.get(name);	
				c.addMeans(p);	}
		}
		else	{
			GoalTask t = factory.factory(p.getTrigger().getLiteral());
			
			if(t == null){
				t = new GoalTask(
						p.getTrigger().getLiteral(),
						new String[]{"true"},//i.e. - don't fail on first preC check before adding/parsing plan
						new String[]{});
				logger.warning("Goal task for " + name + " is null! - not adding composite and returning null result");
				return null;
			}
			
			//check because we can't guarantee what vars are used by the trigger instance
			//so we want to know if arity varies, because it'll screw everything up...
			if(t.getSignature().getTerms().size() != p.getTrigger().getLiteral().getTerms().size()){
				logger.severe("Arity mismatch between goal task " + t.getSignature() + 
						"and plan trigger " + p.getTrigger().getLiteral());
			}
			// this is a hack code for specialist ccs
			if(t.getSignature().getFunctor().equals("moveTo"))	{
				c = (agent instanceof HeliAgent)? new HeliMoveTo(agent, t, p) : new MoveToCapability(agent, t, p);
			}
			else if(t.getSignature().getFunctor().equals(WorldAction.unblock.name()))	{
				c = new UnblockPlanCapability(agent, t, p);
			}
			else if(t.getSignature().getFunctor().toLowerCase().contains("decontaminate"))	{
				c = new DecontaminatePlanCapability(agent, t, p);
			}
			else if(t.getSignature().getFunctor().equals("doRoute") && (agent instanceof VehicleAgent))	{
				c = new DoRouteCapability(agent, t, p);
			}
			else if(	t.getSignature().getFunctor().equals("secure") && (agent instanceof TruckworldAgent) && 
						agent.getTS().getUserAgArch().getAgName().equalsIgnoreCase("militaryhq"))	{
				c = new MHQSecureCapability(agent, t, p);
			}
			else	{
				c = new CompositeCapability(agent, t, p);
			}
		}
		//back into internal store...
		///c.getSignature().toString()
		if(!repairPlan)	{	existing.put(name, c);	}
		else			{	repair.put(name, c);	}

		logger.finer("Capabilities (composite): " + existing.keySet().toString() + 
				"\n(repair): " + repair.keySet().toString() +
				"\n Added c: " + c.toString());

		//now return!
		return c;
	}
	
	/**
	 * Adds a composite
	 * @param c
	 */
	public void addAsExisting(CompositeCapability c)	{
		existing.put(c.getSignature().getFunctor(), c);
	}

	/**
	 * Removes the named plan from composite capabilities.  If the CC has no known means as a result, it
	 * is then itself removed
	 * @param p Plan object
	 */
	public void removal(Plan p) {
		logger.finer("Removing plan " + p);
		String name = p.getTrigger().getLiteral().getFunctor().toString();
		if(existing.containsKey(name))	{
			CompositeCapability c = existing.get(name);
			boolean empty = c.removeMeans(p);
			if(empty)	{
				logger.finer("Removing composite capability : " + c.toString());
				existing.remove(c.getSignature().toString());
			}
		}
		else if(repair.containsKey(name))	{
			CompositeCapability c = repair.get(name);
			boolean empty = c.removeMeans(p);
			if(empty)	{
				logger.finer("Removing repair composite capability : " + c.toString());
				repair.remove(c.getSignature().toString());
			}
		}
	}

	/**
	 * Returns all the capabilities created by this factory so far, indexed by label name/capability name
	 * @return
	 */
	public HashMap<String, CompositeCapability> getFormedCapabilities()				{	return existing;	}

	public HashMap<String, CompositeCapability> getFormedRepairPlanCapabilities()	{	return repair;		}



}
