/**
 * 
 */
package agent.model.capability;

import jason.asSyntax.Literal;
import jason.asSyntax.Term;
import jason.bb.BeliefBase;

import java.util.Collection;
import java.util.List;

import agent.model.goal.GoalTask;
import agent.type.CapabilityAwareAgent;

/**
 * @author Alan white
 * Defines a capability (polymorphic) info model
 */
public interface Capability {
	/**
	 * Defines the method used to get plan confidence
	 */
	public enum confidenceStrategy{ min, average, weightedAverage };
	
	/**
	 * Static defining the confidence estimation approach
	 */
	public static final confidenceStrategy CONF_STRATEGY = confidenceStrategy.weightedAverage;
	
	/**
	 * Returns capability signature - name (functor) and unground variables/args
	 * @return Literal
	 */
	public Literal getSignature();
	
	/**
	 * Returns the task this capability represents 'know how' for
	 */
	public GoalTask achieves();

	/**
	 * Tests if the preconditions hold for this specific instance and belief base
	 * i.e. holds for terms a,b (move a to b), if at(a), and road a->b and can travel that
	 * road, etc.  Also should check terms are valid wrt the action
	 * @param bb  BeliefBase - set of literals to evaluate
	 * @param arg Literal - concrete action
	 * @return boolean if preconditions hold - i.e. can execute
	 */
	public boolean preconditionsHold(BeliefBase bb, Literal action);
	
	/**
	 * Return the LF string set for all preconditions holding
	 * @param bb
	 * @param action
	 * @return Collection of strings, each parsable to a logical expression/formula
	 */
	public Collection<String> getHoldingPreconds(BeliefBase bb, Literal action);

	/**
	 * Returns a confidence [0..1; 0->100%] for this capability in the <i>general</i> case,
	 * given belief set defined by bb (plus any other information, such as agent internal data or
	 * stored results)</br></br>
	 * Used to arbitrate between potential task performers, before the full semantics are known (i.e
	 * as a heuristic for planning/task allocation considerations, or for plans)
	 * @param bb
	 * @return float 0..1
	 */
	public float getGeneralConfidence(BeliefBase bb);
	
	/**
	 * As getGeneral, but returns IMMEDIATELY after any conf value<min is returned
	 * @param bb
	 * @param min
	 * @return
	 */
	public float getGeneralConfidence(BeliefBase bb, float min);
	
	/**
	 * Returns a confidence [0..1; 0->100%] for this capability in the <i>specific</i> case,
	 * using args to define the concrete instance of the capability.</br></br>
	 * Used to handle confidence in planned tasks, and anticipate failure.  Requires a list
	 * of literal actions to be passed in, to support confidence estimations of plans (composite
	 * capabilities) as well as primitive actions.  In the latter case, the list should be simply
	 * a single literal; any after that are anticipated to be ignored.
	 * @param bb
	 * @param args Literal - should be a ground action
	 * @return float 0..1
	 */
	public float getSpecificConfidence(BeliefBase bb, Literal action);

	/**
	 * As getSpecificConfidence, but returns IMMEDIATELY after any conf value<min is returned
	 * @param bb
	 * @param min
	 * @return
	 */
	public float getSpecificConfidence(BeliefBase bb, Literal action, float min);
	
	/**
	 * Stores the result for a specific execution of this capability, used in order to record history and
	 * derive a general confidence (accounting for unknown states affecting execution)
	 * @param succeed if true, the action was successful
	 * @param args the Terms used as arguments; may be used for constraint based reasoning?
	 */
	public void addResult(boolean succeed, List<Term> args);
	
	/**
	 * Returnssuccess ratio; i.e. (1=suceed, 0=fail)
	 * @return float 0-1 
	 */
	public float getSuccessRate();

	/**
	 * Returns an array of strings, which can be parsed and unified in order to determine the 
	 * BB changes after execution completes; i.e. {-atJ(A,B), +onR(A,B,C)}
	 * @return String[]
	 */
	public String[] getPostEffects();
	
	/**
	 * Get states deleted as posteffects
	 * @return
	 */
	public String[] getDeleteEffects();
	
	/**
	 * Get literals added as Posteffects
	 * @return
	 */
	public String[] getAddEffects();
	
	/**
	 * Check if this is a primitive capability or not.  Useful for casting etc.
	 * @return true if primitive, false if composite capability
	 */
	public boolean isPrimitive();

	/**
	 * Returns the set of preconditions; an action may have multiple PC statements.  Preconditions are strings 
	 * parsable to logical expressions that be checked against a BB.  The evaluation can be viewed as the result
	 * of OR-ing all string results.
	 * @return String array
	 */
	public String[] getPreconditions();
	
	
	public ConfidencePrecondition[] getConfidenceBasedPreconditions(final BeliefBase context);
	
	/**
	 * Returns a set of preconditions intended to prevent low-confidence entries.  For example, in CargoWorld will
	 * append 'not damaged' to agent preconds if conf >= 0.8.  Hacky way of confidence filtering
	 * @param confValue
	 * @return string array of preconds
	 */
	public String[] getPreconditionsByConfConstraint(float confValue);

	/**
	 * Return true if this capability is held by another agent just being referenced here; false if the holder is the executor.
	 * Set to true for externalCapability, false for others
	 * @return boolean
	 */
	public boolean isExternal();
	
	/**
	 * Returns an int cost estimate, equating to the average number of actions required in a plan to execute this capability
	 */
	public float getGeneralCostEstimate();
	
	public float getCostEstimate(Literal action, BeliefBase context);

	public CapabilityAwareAgent getHolder();
}
