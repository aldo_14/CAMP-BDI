package agent.model.capability.factory;

import agent.model.capability.PrimitiveCapability;
import agent.type.CapabilityAwareAgent;
import common.UniversalConstants.WorldAction;
import vehicle.ApcAgent;
import vehicle.BulldozerAgent;
import vehicle.HazmatAgent;
import vehicle.HeliAgent;
import vehicle.VehicleAgent;
import vehicle.capabilities.ConsumeCapability;
import vehicle.capabilities.FreeAgentCapability;
import vehicle.capabilities.apc.ApcMoveRoad;
import vehicle.capabilities.apc.SecureArea;
import vehicle.capabilities.bulldozer.BulldozerMoveRoad;
import vehicle.capabilities.bulldozer.UnblockRoad;
import vehicle.capabilities.hazmat.Decontaminate;
import vehicle.capabilities.hazmat.HazmatMoveRoad;
import vehicle.capabilities.heli.Fly;
import vehicle.capabilities.heli.HeliLoad;
import vehicle.capabilities.heli.HeliUnload;
import vehicle.capabilities.heli.Land;
import vehicle.capabilities.heli.Takeoff;
import vehicle.capabilities.truck.Load;
import vehicle.capabilities.truck.MoveRoad;
import vehicle.capabilities.truck.Unload;

public class TruckworldPCFactory extends PrimitiveCapabilityFactory {
	
	@Override
	public PrimitiveCapability getCapability(final String name, final CapabilityAwareAgent holder) {
		
		if(name.equals(WorldAction.move.name()))	{	
			return getTypeAppropriateMoveCapability(holder);
		}	
		else if(name.equals(WorldAction.fly.name()) && (holder instanceof HeliAgent))	{	
			return new Fly((HeliAgent) holder);	
		}
		else if(name.equals(WorldAction.load.name())) 	{	
			return (holder instanceof HeliAgent) ? new HeliLoad((VehicleAgent) holder) : new Load((VehicleAgent) holder);	
		}
		else if(name.equals(WorldAction.unload.name()))			{	
			return (holder instanceof HeliAgent) ? new HeliUnload((VehicleAgent) holder) : new Unload((VehicleAgent) holder);		
		}
		else if(name.equals(WorldAction.unblock.name()))		{	
			return new UnblockRoad((VehicleAgent) holder);		
		}
		else if(name.equals(WorldAction.decontaminate.name()))	{	
			return new Decontaminate((VehicleAgent) holder);	
		}
		else if(name.equals(WorldAction.land.name()))			{	
			return new Land((VehicleAgent) holder);			
		}
		else if(name.equals(WorldAction.takeOff.name()))		{	
			return new Takeoff((VehicleAgent) holder);			
		}
		else if(name.equals(WorldAction.consume.name()))			{	
			return new ConsumeCapability(holder);
		}
		else if(name.equals(WorldAction.secureArea.name()) && (holder instanceof ApcAgent))		{	
			return new SecureArea((VehicleAgent) holder);			
		}
		else if(name.equals(WorldAction.free.name()))			{	
			return new FreeAgentCapability(holder);
		}
		else {	logger.severe("Primitive C " + name + " not found, returning null!");	}
		return null;
	}

	private PrimitiveCapability getTypeAppropriateMoveCapability(final CapabilityAwareAgent holder) {
		if(holder instanceof ApcAgent)			{	
			return new ApcMoveRoad((VehicleAgent) holder);		
		}
		else if(holder instanceof HazmatAgent)	{	
			return new HazmatMoveRoad((HazmatAgent) holder);	
		}
		else if(holder instanceof BulldozerAgent)	{	
			return new BulldozerMoveRoad((BulldozerAgent) holder);
		}
		else									{	
			return new MoveRoad((VehicleAgent) holder);			
		}
	}

}
