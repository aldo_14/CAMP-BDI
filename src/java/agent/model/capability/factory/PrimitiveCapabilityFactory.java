package agent.model.capability.factory;

import java.util.logging.Logger;

import agent.model.capability.PrimitiveCapability;
import agent.type.CapabilityAwareAgent;

import common.UniversalConstants;

public abstract class PrimitiveCapabilityFactory {
	protected Logger logger;
	
	
	public static PrimitiveCapabilityFactory getFactory(String env)	{
		if(env.equals("truckworld"))	{	return new TruckworldPCFactory();	}
		else	{
			return new PrimitiveCapabilityFactory()	{
				@Override
				public PrimitiveCapability getCapability(String name,
						CapabilityAwareAgent holder) {
					return null;
				}
			};
		}
	}	
	
	public PrimitiveCapabilityFactory()	{
		logger = Logger.getLogger(getClass().getSimpleName());
		logger.setLevel(UniversalConstants.FACTORY_LOGGER_LEVEL);
	}
	
	/**
	 * Get the named capability, held by the named agent.
	 * @param name
	 * @param holder
	 * @return PrimitiveCapability
	 */
	public abstract PrimitiveCapability getCapability(String name, CapabilityAwareAgent holder);
}
