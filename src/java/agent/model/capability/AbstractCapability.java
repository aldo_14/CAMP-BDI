package agent.model.capability;

import java.util.Vector;
import java.util.logging.Logger;

import agent.AgentUtils;
import agent.model.goal.GoalTask;
import agent.type.CapabilityAwareAgent;
import common.UniversalConstants;
import jason.asSyntax.Literal;
import jason.asSyntax.LiteralImpl;
import jason.asSyntax.LogExpr;
import jason.bb.BeliefBase;

/**
 * 
 * @author Alan White
 *
 * Abstract capability class - provides some common functionality for both primitive and composite (sub) types
 * 
 * Preconditions are a set of intersections of terms; for purposes of casual link formation and simplicity, disjunctions
 * are not supported here.  Instead multiple equivalent intersection statements should be used, i.e. A & B, B & C instaed of
 * (A | C)  B.
 * 
 * Effects are lists (intersection statement) of terms that are made true or negated upon execution.  We assume an open
 * world - i.e. ~at(B) is used to assume the <i>removal</i> of at(B) rather than its explicit negation in the BB.
 */
public abstract class AbstractCapability extends GoalRecorderCapability {

	/**
	 * Holding agent.  Primarily provided for use by subclasses of this one.
	 */
	protected CapabilityAwareAgent holder;

	/**
	 * Signature; corresponds to calling the capability as an action, or invoking a subgoal
	 * that calls this capability
	 */
	protected final String signature;
	
	/**
	 * Signature in literal form
	 */
	protected final Literal sigLit;
	
	/** Relevant task which this represents 'know how' for */
	protected GoalTask task;

	/**
	 * Strings to define unground pre/post conditions.
	 */
	protected String[] 	preconditions 	= new String[0], 
						effects 		= new String[0], 
						addEffects 		= new String[0], 
						delEffects 		= new String[0];
	
	protected final AgentUtils agentUtils;
	
	/**
	 * Constructor for a capability, for a given task, held by a given agent
	 * @param holder
	 * @param task
	 */
	protected AbstractCapability(	
			CapabilityAwareAgent holder, GoalTask task, Literal signature, String[] pre, String post[]){
		super();
		agentUtils = new AgentUtils(holder);
		logger = Logger.getLogger(holder.getLogger().getName() + ":" + signature.toString());
		logger.setLevel(UniversalConstants.CAPABILITY_LOGGER_LEVEL);
		this.holder = holder;
		this.task = task;
		this.signature = signature.toString();
		this.sigLit = signature;

		if(pre.length==0)	{	
			//fill in with an empty string equiv to 'always holds'...
			this.preconditions = new String[]{"true"};	
		}
		else				{					
			for(String s: pre)	{ //check validity
				if(s.contains("|"))	{
					logger.severe(signature.toString() + " was given an unsupported 'or' precondition; " + s);
				}
				try	{	LogExpr.parseExpr(s);	}
				catch(Exception e)	{
					logger.severe(e.toString());
					logger.severe(signature.toString() + " was given an unparsable precondition; " + s);
				}
			}
			this.preconditions = pre;	//do anyways!
		}

		//figure out add/deletes...
		setupEffectsArrays(post);
	}

	private void setupEffectsArrays(final String[] post) {
		this.effects = post;
		Vector<String> add = new Vector<String>();
		Vector<String> del = new Vector<String>();
		for(String s: this.effects)	{
			if(s.startsWith("+"))		{	add.add(s.substring(1));	}
			else if(s.startsWith("-"))	{	del.add(s.substring(1));	}
		}
		addEffects = new String[add.size()];
		delEffects = new String[del.size()];
		for(int i=0;i<addEffects.length; i++)	{	addEffects[i] = add.get(i);	}
		for(int i=0;i<delEffects.length; i++)	{	delEffects[i] = del.get(i);	}

		//revise effects ordering; delete then add
		if(effects.length > 0)	{
			effects = new String[effects.length];
			for(int i=0; i<delEffects.length; i++)	{
				effects[i] = "-" + delEffects[i];
			}
			for(int i=delEffects.length; i<effects.length; i++)	{
				effects[i] = "+" + addEffects[i-(delEffects.length)];
			}
		}
	}
	
	/**
	 * Default, added for use in externalCapability...
	 */
	public AbstractCapability(final String sig) { 
		signature = sig; 
		sigLit = LiteralImpl.parseLiteral(sig);
		agentUtils = new AgentUtils(this);
	}

	/**
	 * Get signature (acts as UUID)
	 */
	@Override
	public final Literal getSignature() {	
		return sigLit;
	}

	/**
	 * Returns the agent possessing this capability instance
	 * @return a CapabilityAwareAgent
	 */
	@Override
	public final CapabilityAwareAgent getHolder()	{	return holder;	}

	/**
	 * A capability represents the know-how to achieve some goal, either as a single effector action or
	 * through a sequence of sub-tasks.  This method returns a representative of the goal this
	 * capability is for.
	 */
	@Override
	public final GoalTask achieves()		{	return task;	}

	/**
	 * Gets the precondtions for this action; treat each array entry as part of an 'OR'; i.e. 
	 * <code>["a & b", "c", "a & c"]</code> equates to <code>(A & b) || c || (A & C)</code>
	 * @return String[]
	 */
	@Override
	public String[] getPreconditions()	{	return preconditions;	}
	
	/**
	 * Default implementation of get by conf constraint... does nothing...
	 */
	@Override
	public String[] getPreconditionsByConfConstraint(float confValue) {
		return getPreconditions();
	}
	
	/**
	 * Get unground posteffects string; should be '&' seperated list of terms
	 */
	@Override
	public final String[] getPostEffects() 	{	return effects;	}
	
	/**
	 * Get states deleted as posteffects
	 * @return
	 */
	@Override
	public String[] getDeleteEffects()	{	return delEffects;	}
	
	/**
	 * Get literals added as Posteffects
	 * @return
	 */
	@Override
	public String[] getAddEffects()		 {	return addEffects;	}
	
	/**
	 * Default implementation is to return 1.  Can return the success rate 
	 * [0...1, equating to 0..100%] under certain circumstances, where failure is
	 *  inherently related to the acting agent in some manner.
	 */
	@Override
	public float getGeneralConfidence(BeliefBase bb) {
		return 1;//getSuccessRate();//default
	}
	
	public float getGeneralConfidence(BeliefBase bb, float threshold)	{
		return getGeneralConfidence(bb);
	}
	
	/**
	 * Default implementation just returns the general confidence; see getGeneralConfidence
	 */
	@Override
	public float getSpecificConfidence(BeliefBase bb, Literal planAction) {
		return getGeneralConfidence(bb); //default implementation
	}
	
	/**
	 * Suggested method for supporting A-B pruning mechanisms
	 */
	public float getSpecificConfidence(BeliefBase bb, Literal action, float min)	{
		return getSpecificConfidence(bb, action);
	}

	public boolean isExternal()	{	return false;	}

}