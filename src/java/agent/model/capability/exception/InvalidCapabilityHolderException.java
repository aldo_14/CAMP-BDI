/**
 * 
 */
package agent.model.capability.exception;

import jason.asSemantics.Agent;

/**
 * @author Alan White
 * Exception thrown when a CapabilityFactory is unable to create a capability instance for the
 * given agent/type.
 */
public class InvalidCapabilityHolderException extends Exception {
	private static final long serialVersionUID = 5368519708677810183L;
	private Agent attemptedHolder;
	private String capability;

	public InvalidCapabilityHolderException(Agent holder, String capabilityType) {
		this.attemptedHolder = holder;
		this.capability = capabilityType;
	}
	public Agent getAttemptedHolder(){return attemptedHolder;}
	public String getCapability(){return capability;}
	
	public String toString(){
		return 	"Capability " + capability + " cannot be held by " + attemptedHolder.getTS().getUserAgArch().getAgName() + 
				", of type " + attemptedHolder.getClass().getCanonicalName();
	}
}
