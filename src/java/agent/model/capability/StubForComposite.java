package agent.model.capability;

import agent.model.goal.GoalTask;
import agent.type.CapabilityAwareAgent;
import jason.asSyntax.Literal;
import jason.bb.BeliefBase;

/**
 *
 * @author Alan
 * Special case variety of capability, used to fill in for plans we add during runtime; takes the 
 * goal task for the plan and uses it as a basis.
 */
public class StubForComposite extends PrimitiveCapability {
	private final ConfidencePrecondition[] confPre;
	/**
	 * Uses goalTask as an argument
	 * @param holder
	 * @param task
	 */
	public StubForComposite(CapabilityAwareAgent holder, GoalTask task)	{
		this(holder, task, task.getSignature(), task.getPreconditions(), task.getGoalEffects());
	}
	
	//hidden constructor
	private StubForComposite(CapabilityAwareAgent holder, GoalTask task,
			Literal signature, String[] pre, String[] post) {
		super(holder, task, signature, pre, post);
		confPre = new ConfidencePrecondition[preconditions.length];
		for(int i=0; i<confPre.length;i++) {
			confPre[i] = new ConfidencePrecondition(holder.getId(), signature, preconditions[i], 1);
		}
	}

	@Override
	public float getSpecificConfidence(BeliefBase bb, Literal action) {
		return getGeneralConfidence(bb); //stub, assume we succeed until we have a plan to fill in here
	}

	@Override
	public ConfidencePrecondition[] getConfidenceBasedPreconditions(BeliefBase context) {
		return confPre;
	}
}
