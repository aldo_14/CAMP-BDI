package agent.model.capability;
import static agent.AgentUtils.formUnifierFromArgs;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;

import agent.model.goal.GoalTask;
import agent.type.CapabilityAwareAgent;
import agent.type.ContractFormerAgent;
import jason.asSemantics.Agent;
import jason.asSemantics.Unifier;
import jason.asSyntax.ASSyntax;
import jason.asSyntax.Literal;
import jason.asSyntax.LogicalFormula;
import jason.asSyntax.Plan;
import jason.asSyntax.PlanBody;
import jason.asSyntax.PlanBodyImpl;
import jason.asSyntax.Term;
import jason.bb.BeliefBase;

/**
 * @author Alan White
 * 
 * Base implementation of a generic capability; these represent a plan to achieve some task.  The contents can be 
 * inferred from the plan / plan actions; concretes specify the critical preconditions and effects (i.e. the latter
 * referring to the planning problem solved).
 * 
 * Each plan may be executable in a number of contexts; ergo a plan with the same label may have multiple action-sets
 * and preconditions for achieving the same end task.
 */
public class CompositeCapability extends AbstractCapability {

	/**
	 * Context-defined plans; i.e. have set preconditions for use.  Multiple plans may exist for the 
	 * same preconditions - in this case the agent is assumed to have an arbitration mechanism for selecting 
	 * between them.
	 */
	private Map<String, Vector<Plan>> possibleMeans;

	/**
	 * Generalized plans; i.e. context is always 'true'
	 */
	private Vector<Plan> universalMeans;

	/**
	 * Used for a specific circumstance where an initially blank CC is created
	 */
	private boolean stub;

	/**
	 * Create a new composite capability, held by an agent, representing at least one known plan to perform a specified 
	 * task
	 * @param agent
	 * @param task
	 * @param plan
	 */
	public CompositeCapability(CapabilityAwareAgent agent, GoalTask task, Plan plan){
		this(agent, task);
		this.addMeans(plan);
		stub = false;
	}

	public CompositeCapability(CapabilityAwareAgent agent, GoalTask task){
		super(agent, task, task.getSignature(), task.getPreconditions(), task.getGoalEffects());
		possibleMeans = new HashMap<String, Vector<Plan>>();
		universalMeans = new Vector<Plan>();
		stub = true;
	}

	/**
	 * Adds a plan associated with this capability - i.e. 'know how' to perform a task, 
	 * differentiated by the context it can be used in. 
	 * @param p The plan 'means'.
	 */
	public void addMeans(Plan p) {
		p = (Plan)p.clone();
		logger.info("Adding means " + p);
		
		stub = false;

		if(p.getContext()!=null){
			String context = p.getContext().toString();
			if(!possibleMeans.containsKey(context))	{
				logger.info("Adding new entry set for context " + context);
				Vector<Plan> pbs = new Vector<Plan>();
				pbs.add(p);
				possibleMeans.put(context, pbs);
			}
			else	{
				logger.info(possibleMeans.keySet() + "\n contains key \n" + context);
				Vector<Plan> pbs = possibleMeans.get(context);
				pbs.add(p);
				possibleMeans.put(context, pbs);
			}
			logger.fine(p.toASString() + " added");
			logger.fine(context + " entries = " + possibleMeans.get(context));
		}
		else	{
			if(!universalMeans.contains(p))	{
				logger.fine("Add UM plan [" + p.toASString() + "]");
				universalMeans.add(p);
			}
			else	{
				logger.fine("Already have plan [" + p.toASString() + "]");
			}
		}
	}

	/**
	 * Removes a plan from this capability
	 * @param p
	 * @return true if there are no  more means in this capability - i.e. it can be removed
	 */
	public boolean removeMeans(Plan p) {
		logger.finer("Removing means; " + p);
		String context = p.getContext().toString();
		if(context == null && universalMeans.contains(p)){
			//remove from universal means?
			universalMeans.remove(p);
		}
		else	{
			//remove from possible means
			Vector<Plan> opts = possibleMeans.get(context);
			if(opts == null)	{
				//nothing to remove, no update/change performed
				logger.warning("Null result for opts when getting by context " + context +
						"\nPMs=\n" + possibleMeans + 
						"\nHaskey=" + possibleMeans.containsKey(context));
				for(String lf: possibleMeans.keySet())	{
					logger.finer("Compare string; key==context;" + lf.toString().equals(context.toString()));
					if(lf.equals(context.toString())){
						logger.finer("Compare entry set with removal; content=" + possibleMeans.get(lf));
					}
				}
			}
			else	{
				opts.removeElement(p);
				logger.finer("Removed plan from means, options= " + opts);
				//do we have other options?  if not, remove entry entirely...
				if(opts.isEmpty())	{	possibleMeans.remove(context);		}
				else				{	possibleMeans.put(context, opts);	}
			}
		}

		return universalMeans.isEmpty() && possibleMeans.isEmpty();
	}

	/**
	 * Returns the complete set of plans encapsulated in the capability - i.e. the methods of 'know how' held
	 * by the agent to achieve the associated goal.
	 * @return maps a logical formula (precondition) to a set of PlanBodies - each of which equates to the first
	 * action in a plan
	 */
	public final Map<String, Vector<Plan>> getPossibleMeans() {	
		Map<String,  Vector<Plan>> rt = new HashMap<String,  Vector<Plan>>();
		//ums (unconstrained)
		rt.put("true", universalMeans);
		//pms
		for(String f: possibleMeans.keySet())	{	
			Vector<Plan> pms = new Vector<Plan>();
			for(Plan p: possibleMeans.get(f))	{	pms.add((Plan) p.clone());	}
			rt.put(f, pms);	
		}
		
		return rt;	
	}
	
	/**
	 * Tests whether this precondition holds, based upon the root task definition.  Does not
	 * test for executable plans being present, as we cannot know whether these will be dynamically
	 * added into the BB
	 */
	@Override
	public boolean preconditionsHold(BeliefBase bb, Literal action)	{
		return !getHoldingPreconds(bb, action).isEmpty();
	}

	/**
	 * Returns the holding task preconditions
	 */
	@Override
	public Collection<String> getHoldingPreconds(BeliefBase bb, Literal action)	{
		Collection<String> holds = new Vector<String>();
		//do task preconditions hold?
		Agent temp = CapabilityAwareAgent.getFutureAgent(bb);
		Unifier u = formUnifierFromArgs(achieves().getSignature(), action);
		LogicalFormula[] preconds = achieves().getPreconditions(u);
		
		for(LogicalFormula p: preconds){
			logger.finer("Supertask preconditions = " + p.toString());
			Iterator<Unifier> returnIt = p.logicalConsequence(temp, u);
			boolean result = (returnIt!=null) && returnIt.hasNext();
			logger.finer("Task " + p + " holds? " + result);
			if(result)	{		holds.add(p.toString());		}
		}
		
		logger.fine(holds.size() + " precondition hits;" + holds);
		return holds;
	}

	/**
	 * Tests whether this precondition holds, based on the bb and plan trigger formula
	 * 
	 * We will return false if there are NO plans executable in the given context; this should only be used
	 * if and when we can be sure that any plan must have been inserted...
	 * @param bb
	 * @param arg
	 * @return
	 */
	public boolean preconditionsHoldAndPlanExists(BeliefBase bb, Literal action)	{
		//do task preconditions hold?
		Agent temp = CapabilityAwareAgent.getFutureAgent(bb);
		//use this unifier everywhere
		Unifier u = formUnifierFromArgs(achieves().getSignature(), action);
		logger.finer("Checking if plans exist for action " + action + ", signature " + signature);
		if(!universalMeans.isEmpty()){
			logger.finer(achieves().getSignature() + "; Capability has universally applicable plan(s)");
			return true;
		}
		
		/*
		 * If we have no ums, and no pms, then we assume *some* means will be added later and leave checking
		 * until that point.
		 */
		if(possibleMeans.isEmpty()){
			logger.warning("No possible means, doing hackery-pokery return thing...");
			return true;
		}

		logger.fine("Possible means; " + possibleMeans);
		for(String lfs : possibleMeans.keySet())	{
			try	{
				LogicalFormula lf = ASSyntax.parseFormula(lfs);
				logger.fine("checking set; " + possibleMeans);
				Plan p = possibleMeans.get(lfs).firstElement(); //assume same sig for plans with same preconds
				Unifier unif = formUnifierFromArgs(p.getTrigger().getLiteral(), action);
				lf.apply(unif);
				boolean hit = lf.logicalConsequence(temp, unif).hasNext();
				if(hit){
					logger.fine("Precondition for pm holds; " + lf.toString());
					return true;
				}
				else	{
					logger.warning("plan precond " + lf + " does not hold in bb");
				}
			}
			catch(Exception e)	{
				String st = "EM: Exception precond check " + lfs + "\n" + e.toString();
				for(StackTraceElement s: e.getStackTrace())	{	st = st +"\n\t" + s.toString();	}
				logger.severe(st);
			}
		}

		//debug
		if(logger.isLoggable(Level.WARNING))	{
			String bbS = "";
			Iterator<Literal> iterator = bb.iterator();
			while(iterator.hasNext()) { bbS = bbS + "\n" + iterator.next();	}
			logger.warning("No plan preconds hold (" + u + "): " + possibleMeans.toString() + bbS);
		}
		return false;
	}


	@Override
	public ConfidencePrecondition[] getConfidenceBasedPreconditions(final BeliefBase context) {
		final ConfidencePrecondition[] returnSet = new ConfidencePrecondition[this.preconditions.length];
		final float generalConfidence = this.getGeneralConfidence(context);
		for(int i=0; i<returnSet.length ; i++) {
			returnSet[i] = new ConfidencePrecondition(holder.getId(), getSignature(), preconditions[i], generalConfidence);
		}
		return returnSet;
	}

	@Override
	public float getGeneralConfidence(BeliefBase bb) {
		return getGeneralConfidence(bb, -0.1f);
	}
	
	/**
	 * Provides a general confidence value based on prior success.  As we are dealing with an unground 
	 * plan, and do not know the full params for the selection of plans, have to just use this
	 * general success rate. 
	 */
	@Override
	public float getGeneralConfidence(BeliefBase bb, float threshold) {
		Vector<Plan> planSet = new Vector<Plan>();
		
		/*
		 * If we have unconstrained plans (i.e. context="true"), we can take the highest confidence value 
		 */
		if(!universalMeans.isEmpty())	{
			planSet.addAll(universalMeans);
			float highestPlanConf = 0;
			for(Plan p: planSet)	{
				float highestActionConf = getUngroundConfidence(p, bb, threshold);
				logger.finer("Plan conf = " + highestActionConf + " for " + p.toASString());
				//i.e. no capabilities were found atall!
				if(highestActionConf == Integer.MAX_VALUE)	{	highestActionConf = getSuccessRate();	}
				//set highest overall confidence based on this...
				if(highestActionConf > highestPlanConf)	{		highestPlanConf = highestActionConf;	}
			}
			
			logger.finer("Highest confidence for UMs= " + highestPlanConf);
			return highestPlanConf;
		}
		else	{
			/*
			 * If we have constrained plans, then we calculate the confidence for each
			 * 
			 * Return the highest confidence OR the generalized success rate - whichever of the two is lower
			 * 
			 * Purpose of this code is to try and pick up state-induced failures; i.e. if an agent is mortal, then confidence
			 * would be 0 regardless of operational semantics
			 */
			for(Collection<Plan> coll: possibleMeans.values())	{
				planSet.addAll(coll);
			}

			float highest = -1;
			for(Plan p: planSet)	{
				float planConf = getUngroundConfidence(p, bb, threshold);
				logger.finer("Confidence " + planConf + " for " + p.toASString());
				if(planConf > highest)	{	highest = planConf;	}
			}
			
			//all done
			//pick lowest
			float rt = -1;
			if(getSuccessRate() < highest)	{	rt = getSuccessRate();	}
			else							{	rt = highest;			}
			logger.finer("Highest possible means confidence was " + highest + ", gen estimate of success rate=" + 
					getSuccessRate() + "(" + (getSuccessRate()<highest) + "), return " + rt);
			return rt;
		}
	}

	/**
	 * Gets the confidence for an unground plan, assuming all required capabilities are present in the 'holder' agent.  Uses only
	 * <i>unground</i> literals and the GeneralConfidence methods.  Does not recurse if a plan references its own trigger - only
	 * uses the general success rate.
	 * @param p - Plan to check
	 * @param bb - belief base; we can do some general checks against this
	 * @float thresholds - return immediately if conf<threshold...
	 * @return float - the lowest action confidence (pessimistic approach) or Integer.MAX_VALUE if no estimation could be made.  If
	 * all activities are internal actions, will return a 1.
	 */
	private float getUngroundConfidence(Plan p, BeliefBase context, float threshold) {
		float lowestActionConfidence = Integer.MAX_VALUE;
		p = (Plan)p.clone();
		List<PlanBody> actions = agentUtils.planIntoBodyList(p.getBody());
		
		//are all the activities internal actions?  Applies to return value
		boolean allInternal = true;
		
		for(PlanBody pb:actions)	{
			logger.info("Confidence checking the activity " + pb.getBodyTerm());
			
			if(allInternal)	{ //only need to check once
				if(!pb.getBodyTerm().isInternalAction())	{	allInternal = false; }
			}
			
			if(pb.getBodyTerm().isLiteral() && (
					pb.getBodyType().equals(PlanBodyImpl.BodyType.achieve)	||
					pb.getBodyType().equals(PlanBodyImpl.BodyType.achieveNF)||
					pb.getBodyType().equals(PlanBodyImpl.BodyType.action)	
					))	{ 
				//note; ignore IAs as we presume 100% correctness, 
				//and concentrate on environment changing acts/goals
				Literal l = (Literal)pb.getBodyTerm();

				Capability c = getHolder().getCapability(pb);
				if(c==null){
					if(!l.isInternalAction())	{	
						logger.info("Non internal action; no local capability; " + l.toString());
						
						//try external
						if(holder.knowsProviderForExtCapability(l.getFunctor())){
							logger.finest("Fetching external providers for: " + l.toString());
							Collection<ExternalCapability> cSet = holder.getExtCapability(l);
							logger.finest("Got " + cSet.size() + " external providers for: " + l.toString() + "\n" + cSet);
							
							float highestEcConf = -1;
							//select highest confidence rep
							for(ExternalCapability ec:cSet)	{
								if(ec.getGeneralConfidence(context) > highestEcConf)	{	
									highestEcConf = ec.getGeneralConfidence(context);	
								}
							}
							logger.finest("The highest EC confidence for " + l + " was " + highestEcConf);

							if(highestEcConf < lowestActionConfidence) {	lowestActionConfidence = highestEcConf;	}
							else	{	logger.finest("No change in lowest; " + highestEcConf + " < " + lowestActionConfidence);		}
						}
						else	{	
							logger.warning("No capability (internal or external) known for " + l.toString() + "\n" +
								"HolderECs=\n" + holder.getExtCapabilities());	
						}
					}
					else	{
						logger.fine("Assume internal action succeeds");
						if(lowestActionConfidence==Integer.MAX_VALUE){
							lowestActionConfidence = 1f;//assume internal action always succeeds
						}
					}
					//presume bb is unchanged
				}
				else if(c==this && (pb.getBodyType().equals(PlanBody.BodyType.achieve) || 
						pb.getBodyType().equals(PlanBody.BodyType.achieveNF)) )	{
					// want to avoid an infinite loop... ignore this factor in estimation - it can be legit case where a plan 
					// adds itself as a goal, i.e. for example adding a duplicate goal with different args.
					//so we use the success rate alone for it and avoid repeat recursive calls 'down the rabbithole'
					logger.warning("Note; Self reference in plan! " + p.toString());
					float conf = c.getSuccessRate();
					logger.fine("SR based Conf: " + l.toString() + " = " + conf);
					if(conf < lowestActionConfidence) {	lowestActionConfidence = conf;	}
				}
				else if(c==this)	{
					logger.finer("Note; delegation of " + l + " in this plan...");
					//check externals...  presume the highest confidence EC will be used
					//if no ECs exist, ignore
					Collection<ExternalCapability> ext = holder.getExtCapability(l);
					logger.finer("Got " + ext.size() + " candidates...");
					float mc = -1;
					if(!ext.isEmpty())	{
						for(Capability ec:ext)	{
							float ecConf = ec.getGeneralConfidence(context);
							logger.finer("Candidate ec " + ecConf + ">?" + mc);
							if(ecConf > mc)	{	mc = ecConf;	}
						}
						logger.finer(	"lowest ac > Lowest conf for ec? " +
										lowestActionConfidence + ">" + mc);
						if(mc < lowestActionConfidence && mc!=-1) {	
							lowestActionConfidence = mc;	
							logger.finer("lowest ac set" + lowestActionConfidence + " (from" + mc + ")");
						}
						else	{	logger.warning("No ec offering a confidence value; " + mc + "<" + lowestActionConfidence);	}
					}
					else	{	logger.warning("No candidate ecs for " + l + ", recursion?");	}
					
				}
				else	{ //capability found!
					logger.fine("Scoring confidence for " + l.toString());
					float conf;
					if(c.isPrimitive() || !l.isGround()){ //latter handles unground subplans
						conf = c.getGeneralConfidence(context);
					}
					else	{	//ground sub-plan...
						CompositeCapability cc = (CompositeCapability)c;
						conf = cc.getGeneralConfidence(context);
					}

					logger.fine("Conf: " + l.toString() + " = " + conf);
					if(conf < lowestActionConfidence) {	lowestActionConfidence = conf;	}
				}
			}
			//else not a literal, don't worry about it
			
			//terminator threshold check
			if(lowestActionConfidence < threshold)	{	return lowestActionConfidence;	}
		}//end of actions...

		if(lowestActionConfidence == Integer.MAX_VALUE && !allInternal)	{
			logger.fine(lowestActionConfidence + " - max, => No confidence determined, returning 0");
			return 0;
		}
		else if(lowestActionConfidence == Integer.MAX_VALUE && allInternal)	{
			logger.fine(lowestActionConfidence + " - max, => No confidence determined, only internals, return 1");
			return 1;
		}
		else	{
			logger.fine("Confidence estimate (min val); " + lowestActionConfidence);
			return lowestActionConfidence;
		}
	}

	/**
	 * Returns a confidence metric, based on the lowest confidence of the passed in plan actions.  
	 * Any planActions not represented by capability are ignored; if no actions are represented, then
	 * 1 is returned (optimistic assumption).  Internal actions are assumed to be 1 as well.  An anticipated
	 * BB is used to predict the execution context of actions.  If this capability is reference here, then
	 * 0 is returned - as this indicates an infinite recursion/loop when the plan is enacted.
	 * 
	 * It is worth noting that this requires a specific plan to have been selected, which is passed in with the
	 * body as a list of Literals
	 * @param bb - execution context
	 * @param planAction List of literals denoting plan actions
	 */
	@Override
	public float getSpecificConfidence(BeliefBase bb, Literal trigger, float minConf) {
		logger.finer("(CC) Specific Confidence estimation");
		BeliefBase context = bb.clone();
		
		if(!trigger.isGround())	{
			logger.severe("Unground trigger! " + trigger);
			return getGeneralConfidence(bb);
		}
		
		//id plans possibe
		List<Plan> pl = getMeans(trigger, bb);
		if(pl.isEmpty())	{	
			logger.warning("No plans found for " + trigger);
			return 0;	
		}
		String plDump = "";
		for(Plan poss: pl)	{	plDump = plDump + "\n" + poss.toASString();	}
		logger.finer(pl.size() + " possible plans for  " + trigger + plDump);
		
		//work out the best confidence in plans - we return this
		float bestPlanConf = -1;
		for(final Plan poss: pl)	{
			//check all the possible plans for confidence
			final Plan poss2 = (Plan) poss.clone();
			final Unifier plU = formUnifierFromArgs(poss2.getTrigger().getLiteral(), trigger);	
			//List<Literal> argTemp = new ArrayList<Literal>();
			//PlanBody pb = poss.cloneOnlyBody().getBody();
			poss2.apply(plU);
			logger.info("Estimating confidence for possible plan " + poss2.toString());
			//assume we pick the best plan
			final float tc = getPlanConfidence(context, poss2, minConf);
			if(tc > bestPlanConf)	{
				logger.finer("New best plan; " + poss2.toASString());
				bestPlanConf = tc;
			}
		}
		return bestPlanConf == -1? 0 : bestPlanConf;
	}

	public float getSpecificConfidence(BeliefBase bb, Literal trigger)	{
		return getSpecificConfidence(bb, trigger, -0.1f); //no min val
	}
	
	/**
	 * Gets plan confidence using the defined strategy; will throw an exception if strategy not supported
	 * @param context
	 * @param planAction
	 * @param threshold
	 */
	private float getPlanConfidence(BeliefBase context, Plan p, float threshold) {
		return ((ContractFormerAgent)holder).calculatePlanConfidence(p, context);
	}

	/**
	 * Returns the set of plans which can be executed in the given belief base
	 * @return Vector of PlanBody objects
	 */
	protected Vector<Plan> getPossiblePlans(Unifier u, BeliefBase context) {
		Vector<Plan> possible = new Vector<Plan>();
		logger.finer("Have " + universalMeans.size() + " universal means");
		possible.addAll(universalMeans); //these are universally applicable... 

		//get agent
		Agent test = CapabilityAwareAgent.getFutureAgent(context);
		if(logger.isLoggable(Level.FINEST))	{
			String cs = "";
			Iterator<Literal> bbit = test.getBB().iterator();
			while(bbit.hasNext())	{
				cs = cs +"\n\t" + bbit.next();
			}
			logger.finest("Searching for plans under context" + cs);
		}
		
		//check through plans whose preconditions hold in this currrent BB context
		int p = 1;
		for(String pre:possibleMeans.keySet())	{
			try	{
				LogicalFormula preTest = ASSyntax.parseFormula(pre);
				preTest.apply(u);
				Iterator<Unifier> it = preTest.logicalConsequence(test, u);
				if(it!=null && it.hasNext())	{
					logger.info(p++ + ":Plan is usable; Trigger=" + preTest.toString() + ":" + it.next());
					possible.addAll(possibleMeans.get(pre));
				}
				else	{
					logger.info(p++ + ":Disregarding plan as unusable; precond=" + preTest.toString() + ", u=" + u);
				}
			}	
			catch (Exception e)	{
				String st = "EM: Exception in possible means search " + pre + "\n" + e.toString();
				for(StackTraceElement s: e.getStackTrace())	{	st = st +"\n\t" + s.toString();	}
				logger.severe(st);
			}

		}
		logger.info("Returning possible plans; " + possible);
		return possible;
	}

	@Override
	public boolean isPrimitive() {	return false;	}

	@Override
	public String toString(){
		String toString = getSignature() + 
				"\n==>UMeans: " + universalMeans.toString() +
				"\n==>Qualified means: ";
		int i = 1;
		for(String lf:possibleMeans.keySet()){
			Vector<Plan> pbs = possibleMeans.get(lf);
			toString = toString + "\n\tPrecondition " + i++ + " [ " + lf.toString() + " ]: ";
			int j=1;
			for(Plan pb:pbs)	{	
				toString = toString +"\t\t\n(" + j++ + ")" + pb.toASString();	
			}
		}

		return toString + "\nSuccess rate=" + getSuccessRate();
	}

	/**
	 * Returns a list of all literals used / required by this composite, to determine the
	 * required capabilities.  Aggregates all associated plans.
	 * i.e. a plan move, move, lift, drop would return [move, lift, drop]
	 * @return
	 */
	public List<String> getRequiredCapabilities() {
		List<String> rtList = new Vector<String>();

		//ums first
		for(Plan um: universalMeans){
			List<String> sub = this.getRequiredCapabilitiesForPlanBody(um);
			for(String s: sub){
				if(holder.getCapability(s) == null	&& !rtList.contains(s))	{	rtList.add(s);	}
			}
		}

		//pms
		for(Vector<Plan> um: possibleMeans.values()){
			for(Plan pb:um){
				List<String> sub = getRequiredCapabilitiesForPlanBody(pb);
				for(String s: sub){
					if((holder.getCapability(s) == null) && !rtList.contains(s))	{	rtList.add(s);	}
				}
			}
		}

		logger.info("Composite capability " + getSignature() + " requires capabilities: " + rtList);

		return rtList;
	}

	/**
	 * Get a list of functors in the plan body
	 * @param pb Planbody for the start of the plan
	 * @return List of functors (string action names)
	 */
	private List<String> getRequiredCapabilitiesForPlanBody(Plan p)	{
		List<String> rtList = new Vector<String>();
		PlanBody pb = p.getBody();
		while(pb.getBodyNext()!=null)	{
			Term current = pb.getBodyTerm();
			if(!current.isInternalAction())	{	rtList.add(((Literal)current).getFunctor());	}
			//ignore internals...
			pb = pb.getBodyNext();
		}

		return rtList;
	}

	/** @return boolean;  true if empty 'stub' composite capability */
	public boolean isStub()	{	return stub;	}

	/**
	 * Cost estimate is the average length of plans associated with this CC
	 * (unless overridden)
	 */
	public float getGeneralCostEstimate()	{
		float count = 0; //total number of actions in all plans (for average)
		int size = 0; //toal number of plans
		//for universal means
		Vector<Plan> pSet = new Vector<Plan>();
		pSet.addAll(universalMeans);
		
		for(Vector<Plan> vp: possibleMeans.values())	{
			pSet.addAll(vp);
		}
		
		//note: only works if we don't have infinite loops ala a sub-capability
		//calling this one
		for(Plan p : pSet)	{
			//count += AgentUtils.getActionCount(p.getBody(), false);//don't count IAs...
			List<PlanBody> pbs = agentUtils.planIntoBodyList(p.getBody());
			int recurseCount = 0;
			for(PlanBody pb: pbs)	{
				Literal l = (Literal)pb.getBodyTerm();
				Collection<ExternalCapability> exts = holder.getExtCapability(l);
				if(l.getFunctor().equals(getSignature().getFunctor()))	{
					//try external
					if(!exts.isEmpty())	{
						//average the cost...
						float cost = 0;
						for(ExternalCapability ec: exts)	{	cost+=ec.getGeneralCostEstimate();	}
						count += (cost/exts.size());
					}
					else	{
						logger.severe("Recursion detected!");
						recurseCount++;
					}
				}
				else if(holder.hasCapability(pb))	{
						Capability c = holder.getCapability(pb);
						count += c.getGeneralCostEstimate();
				}
				else if(!holder.getExtCapability(l).isEmpty())	{
					//average the cost...
					float cost = 0;
					for(ExternalCapability ec: exts)	{	cost+=ec.getGeneralCostEstimate();	}
					count += (cost/exts.size());
				}
			}
			//trying to account for self-referencing without infinite looping
			count += (recurseCount*count); 
			size++;
		}
		
		if(count==0)	{	count = 1;			}
		else			{	count = count/size;	}
		logger.finer("Average plan length; " + count);
		return count;
	}
	

	@Override
	public float getCostEstimate(Literal action, BeliefBase context) {
		List<Plan> possibleSet = getMeans(action, context);
		if(possibleSet.isEmpty())	{	return getGeneralCostEstimate();	}
		else	{
			//TODO for plan length - don't worry about cost justnow, we don't use it
		}
		return getGeneralCostEstimate();
	}
	

	public List<Plan> getMeans(Literal action, BeliefBase context) {
		Unifier u = formUnifierFromArgs(getSignature(), action);
		return getPossiblePlans(u, context);
	}

	public Collection<ExternalCapability> getConfidenceBasedPreconditionEcs(final BeliefBase bb) {
		final Collection<ExternalCapability> set = new Vector<>();
		GoalTask goal = achieves();
		float conf = getGeneralConfidence(bb);
		Map<String, Vector<Plan>> means = getPossibleMeans();
		for (final String pre : means.keySet()) {
			final Vector<Plan> plans = means.get(pre);
			for (final Plan plan : plans) {
				Collection<String> effSet = holder.agentUtils().getPostEffectsOfPlan(holder, plan.getBody());
				String[] eff = new String[effSet.size()];
				String es = "";
				for (int i = 0; i < eff.length; i++) {
					eff[i] = (String) effSet.toArray()[i];
					es = es + " " + eff[i];
				}
				logger.finer(plan.getTrigger().getLiteral() + " Composite posteffects = " + es + ", for plan\n" + plan.toASString());
				float cost = holder.agentUtils().getActionCount(plan.getBody(), false);
				set.add(new ExternalCapability(plan.getTrigger().getLiteral(), goal, holder,
						new ConfidencePrecondition(holder.getId(), getSignature(), pre, conf), eff, cost, false));
			}// end forplans
		} // end forpre
		return set;
	}

	public Collection<? extends ExternalCapability> getConfidenceBasedPreconditionEcs(final BeliefBase bb, final float minConf) {
		final Collection<ExternalCapability> set = new Vector<>();
		final Collection<ExternalCapability> defaultSet = this.getConfidenceBasedPreconditionEcs(bb);
		
		for(final ExternalCapability ec: defaultSet) {
			if(ec.getConfidenceBasedPreconditions(bb)[0].getConfidence()>minConf) {
				set.add(ec);
			}
		}
		return set;
	}
}
