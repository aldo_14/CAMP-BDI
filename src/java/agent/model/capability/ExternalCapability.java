package agent.model.capability;
import static agent.AgentUtils.formUnifierFromArgs;
import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import agent.model.goal.GoalTask;
import agent.type.CapabilityAwareAgent;
import common.UniversalConstants;
import jason.asSemantics.Agent;
import jason.asSemantics.Unifier;
import jason.asSyntax.ASSyntax;
import jason.asSyntax.Literal;
import jason.asSyntax.LogExpr;
import jason.asSyntax.LogicalFormula;
import jason.asSyntax.Term;
import jason.asSyntax.parser.ParseException;
import jason.bb.BeliefBase;

public class ExternalCapability extends AbstractCapability {
	private float conf, cost;
	private final boolean primitive;
	private final ConfidencePrecondition confPre; 
	public ExternalCapability(Literal sig, GoalTask task, CapabilityAwareAgent holder, 
			ConfidencePrecondition pre, String[] postEffects, float cost, boolean prim) {
		super(sig.toString());
		logger = Logger.getLogger(holder + " holding " + sig.toString());
		logger.setLevel(UniversalConstants.CAPABILITY_LOGGER_LEVEL);
		this.task = task;
		this.preconditions = new String[]{pre.getPrecondition()};
		this.confPre = pre;
		this.effects = postEffects; 
		
		//strip post-effects which are NOT in the signature
		Vector<String> effCol = new Vector<String>();
		for(int i=0;i<effects.length;i++)	{	
			String e = effects[i].trim();
			String prepend = effects[i].substring(0, 1);
			e = e.substring(1);

			Literal l = Literal.parseLiteral(e);
			boolean ok = true;
			checkT:
				for(final Term t: l.getTerms())	{
					//note; external capabilities associated with obligaitons/dependencies can have ground terms
					if(!t.isGround() && t.isVar() && !sig.getTerms().contains(t))	{
						ok = false;
						logger.fine("Capability " + sig + " discarding pe with unknown term " + t +"; " + 
								effects[i] + "(!is ground? " + !t.isGround() + " && isVar " + t.isVar() + " !" + 
								sig.getTerms() + " contains " + sig.getTerms().contains(t) + ")");
						break checkT;
					}
				}
			if(ok)	{	effCol.add(prepend + e);	}
		}
		
		logger.finer("Capability " + sig + " effects=" + effCol);
		effects = new String[effCol.size()];
		for(int i=0;i<effects.length;i++)	{	effects[i] = effCol.get(i);	}
		this.conf 	= pre.getConfidence();
		this.cost 	= cost;
		this.holder = holder;
		
		//figure out add/deletes...
		Vector<String> add = new Vector<String>();
		Vector<String> del = new Vector<String>();
		for(String s: this.effects)	{
			if(s.startsWith("+"))		{	add.add(s.substring(1));	}
			else if(s.startsWith("-"))	{	del.add(s.substring(1));	}
		}
		addEffects = new String[add.size()];
		delEffects = new String[del.size()];
		for(int i=0;i<addEffects.length; i++)	{	addEffects[i] = add.get(i);	}
		for(int i=0;i<delEffects.length; i++)	{	delEffects[i] = del.get(i);	}
		
		this.primitive = prim;
	}

	public String getHolderName()	{	return holder.getId();	}
	
	public void setPreconditions(final String[] precond) { 
		this.preconditions = precond;
	}
	
	public String toString(){
		String eff = "";
		for(int i=0; i<effects.length; i++)	{	eff = eff + " & " + effects[i];	}
		eff  = eff.replaceFirst(" &", "");
		String type = "";
		if(isPrimitive())	{	type = "Primitive";	}
		else				{	type = "Composite"; 	}
		return holder  + " holding: " + type + " " + getSignature() + " pre:" + getPreconditions()[0] + 
				", eff:" + eff + " ( cost = " + cost + ", conf = " + conf + ")";
	}
	
	/**
	 * Update the confidence associated with this EC
	 * @param conf
	 */
	public void setConf(float conf)	{	this.conf = conf;	}
	
	@Override
	public float getGeneralConfidence(BeliefBase bb) {	return conf;	}
	@Override
	public float getSpecificConfidence(BeliefBase bb, Literal action) {	
		try	{
		Capability capability = getHolder().getCapability(action.getFunctor());
		float precise = capability.getSpecificConfidence(bb, action);
		return precise;
		}
		catch(Exception e)	{
			String st = "";
			for(StackTraceElement s : e.getStackTrace())	{
				st = st + "\n\t" + s;
			}
			logger.severe(e.toString() + st);
		}
		return conf;	
	}
	@Override
	public float getGeneralCostEstimate()	{	return cost;	}
	

	
	@Override
	public ConfidencePrecondition[] getConfidenceBasedPreconditions(final BeliefBase context) {
		return new ConfidencePrecondition[]{confPre};
	}
	
	@Override
	public final boolean isExternal()	{	return true;	}

	@Override
	public boolean preconditionsHold(final BeliefBase bb, final Literal action) {
		final Agent test = CapabilityAwareAgent.getFutureAgent(bb);
		final Unifier u = formUnifierFromArgs(getSignature(), action);
		// does 'a' precondition hold?
		// preconditions.length should always == 1...
		for (final String precondition : preconditions) {
			if (preconditions.equals("true") || preconditions.equals("")) {
				return true;
			} else if (preconditions != null) {
				boolean preHolds = false;
				try {
					final LogicalFormula pre = ASSyntax.parseFormula(precondition);
					pre.apply(u);
					preHolds = test.believes(pre, u);
				} catch (ParseException e) {
					throw new RuntimeException("Invalid precondition " + precondition, e);
				}
				// we check for contradictions
				if (!preHolds || logger.isLoggable(Level.FINER)) {
					String res = "";
					String[] preLits = getPreconditions()[0].split("&");
					for (String formula : preLits) {
						formula = formula.trim();
						logger.finest("Check lf; " + formula);
						formula = formula.replaceFirst("not \\(", "not ");
						while (formula.startsWith("(")) {
							formula = formula.replaceFirst("\\(", "");
						}
						while (formula.endsWith("))")) {
							formula = formula.replaceAll("\\)\\)", ")");
						}
						LogicalFormula lf = LogExpr.parseExpr(formula);
						lf.apply(u);
						res = res + lf.toString() + "(holds?=" + test.believes(lf, u) + ") & ";
					}
					logger.log(logger.getLevel(),
							"Testing precondition for " + action + "(U=" + u + "); " + precondition + "= " + preHolds
									+ "\n\t" + res.substring(0, res.length() - 2) + "\n(" + preconditions.length
									+ " preconds total)");
				} // end isLoggable check
				if (preHolds) {
					return true;
				}
			}
		}
		return false; // no precondition held, no true return, must be false
	}

	@Override
	public boolean isPrimitive() {	return primitive;	}

	@Override
	public Collection<String> getHoldingPreconds(BeliefBase bb, Literal action) {
		//from primitive capability code...
		Collection<String> holding = new Vector<String>();
		Unifier u = formUnifierFromArgs(getSignature(), action);
		for(String pre: preconditions)	{
			LogicalFormula lf = LogExpr.parseExpr(pre);

			lf.apply(u);
		//	lf = LogExpr.parseExpr(lf.toString()); //hack for poss. bug with unifier usage(?)
			logger.finer(action + "/" + signature + ": Check preconditions; " + lf.toString());

			Agent a = CapabilityAwareAgent.getFutureAgent(bb);

			Iterator<Unifier> it = lf.logicalConsequence(a, u);
			boolean result = (it!=null) && it.hasNext();
			logger.finer("PC check: lf was; " + lf.toString() + ", result=" + result );

			if(result && logger.isLoggable(Level.FINER))	{
				while(it.hasNext()){logger.finer("Result of preC it; " + it.next().toString());}
			}

			if(result)	{	holding.add(lf.toString());	}
		}
		logger.finer("Precondition hits; " + holding);
		return holding;
	}

	@Override
	public float getCostEstimate(Literal action, BeliefBase context) {
		Capability c = holder.getCapability(action.getFunctor());
		if(c==null)		{	
			logger.severe("Can't find holder C for " + action + "?");
			return getGeneralCostEstimate();	
		}
		else	{	return c.getCostEstimate(action, context);	}
	}

}
