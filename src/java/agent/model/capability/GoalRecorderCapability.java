package agent.model.capability;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.logging.Logger;

import common.UniversalConstants;
import jason.asSyntax.Term;

/**
 * 
 * @author Alan White
 * 
 * Abstract class created purely to help divide up and simplify the default implementation
 * of capability; this contains the methods and objects used to record goal success.
 */
public abstract class GoalRecorderCapability implements Capability {
	/** Logger object */
	protected Logger logger;
	
	/**
	 * A fixed size FIFO of previous results; pairs the time with the result.  Remove the
	 * top entry when we hit MAX_RESULTS
	 */
	private Queue<Map<Long, Boolean>> resultsQueue;
	
	/** Defines a maximum number of historical results to be held; default is 50 */
	public static final int MAX_RESULTS = 50;
	
	/** If we haven't received a result for this many ms, reset records to zero */
	public static final int TEND_BACK_TO_ONE_DELAY = 60000;

	/** Last result time - start at zero */
	private long lastResult = 0;
	
	protected GoalRecorderCapability(){
		logger = Logger.getLogger(getClass().getSimpleName());
		logger.setLevel(UniversalConstants.CAPABILITY_LOGGER_LEVEL);
		this.resultsQueue = new LinkedList<Map<Long, Boolean>>();
	}

	/**
	 * Adds a result to the recorded history; in effect increments the resultsHeld integer
	 * and, if succeed=true, the successes integer
	 */
	@Override
	public final void addResult(boolean succeed, List<Term> args) {
		while(resultsQueue.size() >= GoalRecorderCapability.MAX_RESULTS)	{
			resultsQueue.poll(); //remove top
		}
		Map<Long, Boolean> entry = new HashMap<Long, Boolean>();
		lastResult = System.currentTimeMillis();
		entry.put(lastResult, succeed);
		resultsQueue.add(entry);
	}
	
	/**
	 * Calculates success rate based on past history, performing any historical
	 * weighting.  
	 * @return float; 0 = never succeeds, 1=always succeeds
	 */
	@Override
	public final float getSuccessRate() {	return 1;}
}
