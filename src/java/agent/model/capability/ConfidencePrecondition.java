package agent.model.capability;

import static jason.asSyntax.ASSyntax.createLiteral;

import java.math.BigDecimal;

import jason.asSyntax.Literal;
import jason.asSyntax.LogExpr;

/**
 * Represents a specific precondition and confidence score; i.e. that x & y gives z quality
 */
public class ConfidencePrecondition {
	private final String precondition,precondForPlanning;
	private final float confidence;
	
	public ConfidencePrecondition(final String holderName, final Literal signature, final String pre, final float conf) {
		this.precondition = pre;
		this.confidence = conf;
		this.precondForPlanning = LogExpr.parseExpr((signature.getTerms().isEmpty() ? 
				"" : createLiteral( holderName, signature.getTerm(0)).toString() + " &") + precondition).toString();
	}

	public String getPrecondition() {
		return LogExpr.parseExpr(precondition).toString();
	}

	public String getPreconditionsForPlanning() {
		return precondForPlanning;
	}
	
	public BigDecimal getConfidenceMetricValue() {
		//need to set to some v.low value otherwise plan length is not used as a metric
		return (confidence==1) ? new BigDecimal(0.01) : new BigDecimal(10*(-Math.log(confidence)));
	}
	
	public float getConfidence() {
		return confidence;
	}
	
	public static void main(String[] args) {
		System.out.println(new BigDecimal(10*(-Math.log(0))).toPlainString());
	}
}
