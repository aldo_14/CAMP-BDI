package agent.model.capability;

import static agent.AgentUtils.formUnifierFromArgs;

import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Level;

import agent.model.goal.GoalTask;
import agent.type.CapabilityAwareAgent;
import jason.asSemantics.Agent;
import jason.asSemantics.Unifier;
import jason.asSyntax.Literal;
import jason.asSyntax.LogExpr;
import jason.asSyntax.LogicalFormula;
import jason.bb.BeliefBase;
/**
 * @author Alawhite
 * Generic abstract implementation of a primitive (effector) capability, to provide a very basic historical record function.  
 * Keeps an average of all results/successes, does not consider historical weighting or specific instantiations.
 * </br>
 * May have multiple preconditions (representing disjunction, which is not necessarily supported by the dynamic planner); when 
 * converted to a planning operator, will have multiple operators - one for each precondition state.  Preconditions are strings
 * which can be parsed into sets of positive or negative literals and assessed against a given beliefbase.
 * </br></br>
 * Primitive capabilities also have a unique characteristic in providing a method to obtain an <i>operator</i> for use in
 * classical planning; due to restrictions on the selected planner, only binary logic is supported for preconditions.
 */
public abstract class PrimitiveCapability extends AbstractCapability {

	protected PrimitiveCapability(CapabilityAwareAgent holder, GoalTask task,
			Literal signature, String[] pre, String[] post) {
		super(holder, task, signature, pre, post);
	}
	
	@Override
	public final boolean isPrimitive() {	return true;	}


	@Override
	public boolean preconditionsHold(BeliefBase bb, Literal action) {
		return !getHoldingPreconds(bb, action).isEmpty();
	}
	
	@Override
	public Collection<String> getHoldingPreconds(BeliefBase bb, Literal action) {
		Collection<String> holding = new Vector<String>();
		Unifier u = formUnifierFromArgs(getSignature(), action);
		for(String pre: preconditions)	{
			LogicalFormula lf = LogExpr.parseExpr(pre);

			lf.apply(u);
			logger.finer(action + "/" + signature + ": Check preconditions; " + lf.toString());

			Agent a = CapabilityAwareAgent.getFutureAgent(bb);

			Iterator<Unifier> it = lf.logicalConsequence(a, u);
			boolean result = (it!=null) && it.hasNext();
			logger.finer("PC check: lf was; " + lf.toString() + ", result=" + result );

			if(result && logger.isLoggable(Level.FINER))	{
				while(it.hasNext()){logger.finer("Result of preC it; " + it.next().toString());}
			}

			if(result)	{	holding.add(lf.toString());	}
		}
		
		logger.finer(holding.size() + " preconditions holding; " + holding);
		return holding;
	}
	
	/**
	 * Generalized cost estimate; normally return 1 for prims
	 * @return 1
	 */
	@Override
	public float getGeneralCostEstimate()	{	return 1;	}
	
	@Override
	public float getCostEstimate(Literal action, BeliefBase context)	{
		return getGeneralCostEstimate(); //simple model
	}
	
	public String toString(){
		return this.getSignature() + ": current confidence=" + this.getGeneralConfidence(holder.getBB()) + 
				", success rate=" + this.getSuccessRate();
	}
}
