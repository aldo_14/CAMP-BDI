package agent.model.capability;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import common.UniversalConstants;


/**
 * 
 * @author alanwhite
 *
 * A singleton class standing in for a MAS service to advertise, etc agent
 * capabilities.  Implemented due to issues with Jason message handling performance.
 */
public class ExternalCapabilityService {

	private final static ExternalCapabilityService ecs = new ExternalCapabilityService();

	private Logger logger;

	public static final ExternalCapabilityService getInstance()	{
		return ecs;
	}

	/**
	 * Maps agent id to an EC set.
	 */
	private Map<String, Collection<ExternalCapability>> ecSet;

	private ExternalCapabilityService()	{
		ecSet = new HashMap<String, Collection<ExternalCapability>>();
		logger = Logger.getLogger(getClass().getSimpleName());
		logger.setLevel(UniversalConstants.CAP_SRVC_LOGGER_LEVEL);
	}

	/**
	 * Fetches all conformant ecs.  Collections define restrictions upon task
	 * and agent awareness; if they are empty, then return all
	 * </br></br>
	 * For example, {truck1, truck2} {move} will return the ECS for the move
	 * action on both truck1 and truck2.
	 * @param knownAgents
	 * @param taskKnowledge
	 * @return
	 */
	public Collection<ExternalCapability> getEcs(
			String aid, Collection<String> knownAgents, Collection<String> taskKnowledge)	{
		synchronized(ecSet)	{
			logger.info(aid + ":Servicing request; KA=" + knownAgents + ", task=" + taskKnowledge);
			if(logger.isLoggable(Level.FINER))	{
				String mapString = "";
				for(String key: ecSet.keySet())	{
					Collection<ExternalCapability> colEx = ecSet.get(key);
					if(logger.isLoggable(Level.FINER))	{
						for(ExternalCapability e: colEx)	{	mapString = mapString + "\n\t" + e.toString();	}
					}
				}
				logger.finer("My map;" + mapString );
			}
			return filterReturnedEcsByFunctor(filterReturnedEcsByConf(filterEcsByHolder(knownAgents)), taskKnowledge);
		}
	}

	private Collection<ExternalCapability> filterEcsByHolder(final Collection<String> knownAgents) {
		final Collection<ExternalCapability> returnSet = new Vector<>();
		if(knownAgents.isEmpty())	{
			for(final Collection<ExternalCapability> val: ecSet.values())	{	
				returnSet.addAll(val);	
			}
		}
		else	{
			for(final String id: knownAgents)	{	
				if(ecSet.containsKey(id))	{	
					returnSet.addAll(ecSet.get(id));	
				}
			}
		}
		return returnSet;
	}
	
	private Collection<ExternalCapability> filterReturnedEcsByConf(final Collection<ExternalCapability> ecs) {
		final Collection<ExternalCapability> returnSet = new Vector<>();
		for(final ExternalCapability ec: ecs) {
			if(ec.getGeneralConfidence(ec.getHolder().getBB())>0) {
				returnSet.add(ec);
			}
		}
		return returnSet;
	}

	private Collection<ExternalCapability> filterReturnedEcsByFunctor(final Collection<ExternalCapability> ecs, final Collection<String> knownFunctors) {
		final Collection<ExternalCapability> returnSet = new Vector<>();
		for(final ExternalCapability ec: ecs) {
			if(knownFunctors.isEmpty() || knownFunctors.contains(ec.getSignature().getFunctor())) {
				returnSet.add(ec);
			}
		}
		
		return returnSet;
	}
	
	/**
	 * Adds the given external capability to the data store; gets the agent
	 * id from the capability holder field
	 * @param ec
	 */
	public void registerExternalCapabilities(String holder, Collection<ExternalCapability> ec)	{
		synchronized(ecSet)	{
			if(ecSet.containsKey(holder))	{	
				ecSet.remove(holder);	
				}
			ecSet.put(holder, ec);
		}
	}
}
