package agent.model.goal.factory;

import jason.asSyntax.Literal;

import java.util.logging.Logger;

import agent.model.goal.GoalTask;

import common.UniversalConstants;

public class TestTaskFactory extends TaskFactory {
	public TestTaskFactory()	{
		logger = Logger.getLogger(this.getClass().getSimpleName());
		logger.setLevel(UniversalConstants.CAPABILITY_LOGGER_LEVEL);
	}

	@Override
	public GoalTask factory(String tName) {
		GoalTask g = null;
		if(tName.equals("test"))	{
			g = new GoalTask(Literal.parseLiteral("test(A, B, C)"), new String[]{"true"},new String[]{});
		}
		if(tName.equals("subTest"))	{
			g = new GoalTask(Literal.parseLiteral("subTest(A, B)"), new String[]{"true"},new String[]{});
		}
		else if(tName.equals("test1"))	{
			g = new GoalTask(Literal.parseLiteral("test1(A)"), new String[]{"true"}, new String[]{});
		}
		else if(tName.equals("test2"))	{
			g = new GoalTask(Literal.parseLiteral("test2(A)"), new String[]{"true"}, new String[]{});
		}
		else if(tName.equals("test3"))	{
			g = new GoalTask(Literal.parseLiteral("test3(A)"), new String[]{"true"}, new String[]{});
		}
		
		if(g==null){	logger.severe("Returning a null task for " + tName);	}
		
		return g;
	}
	
	/**
	 * Intended for composite capabilities (inferred from plans), where l corresponds to the triggering literal
	 * @param l
	 * @return
	 */
	public GoalTask factory(Literal l){
		GoalTask g = factory(l.getFunctor());
		/*
		 * note; this should check for GOALS, not event triggers.  We don't model these...
		 */
		if(g == null){
			//do signature based factory
		}
		return g;
	}
	
}
