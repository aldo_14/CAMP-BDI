package agent.model.goal.factory;

import jason.asSyntax.Literal;

import java.util.logging.Logger;

import agent.model.goal.GoalTask;


/**
 * 
 * @author Alan White
 * Creates tasks.  These define goals with preconditions and a set signature.  This factory is
 * used to allow a common definition of goals in the MAS, i.e. all agents share the same meaning for goals
 * (although their capabilities for achieving said goals may vary in terms of preconditions or side-effects).
 * </br></br>
 * Note that agents all access this factory BUT their agent descriptions will define which tasks they have
 * knowledge of - i.e. agents don't have universal planning knowledge.
 */
public abstract class TaskFactory {
	protected Logger logger;
	
	public final static TaskFactory getInstance(String domain){
		if(domain.equals("truckworld"))	{	return new CargoworldTaskFactory();	}	
		else if(domain.equals("test"))	{	return new TestTaskFactory();		}
		else	{
			return new TaskFactory(){
				@Override
				public GoalTask factory(String tName) 	{	return null;	}
				@Override
				public GoalTask factory(Literal l) 		{	return null;	}
			};
		}
	}

	/**
	 * Returns a task instance if exists
	 * @param tName id name key referenced by capability
	 * @return GoalTask, null if unable to create
	 */
	public abstract GoalTask factory(String tName);
	
	/**
	 * Intended for composite capabilities (inferred from plans), where l corresponds to the triggering literal
	 * @param l
	 * @return
	 */
	public abstract GoalTask factory(Literal l);
}
