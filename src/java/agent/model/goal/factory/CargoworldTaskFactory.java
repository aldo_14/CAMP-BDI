package agent.model.goal.factory;

import jason.asSemantics.Unifier;
import jason.asSyntax.Literal;
import jason.asSyntax.LogicalFormula;

import java.util.logging.Logger;

import agent.AgentUtils;
import agent.model.goal.GoalTask;

import common.UniversalConstants;

public class CargoworldTaskFactory extends TaskFactory {
	public CargoworldTaskFactory()	{
		logger = Logger.getLogger(this.getClass().getSimpleName());
		logger.setLevel(UniversalConstants.FACTORY_LOGGER_LEVEL);
	}

	@Override
	public GoalTask factory(String tName) {
		GoalTask g = null;
		/*
		 * NB: bear in mind that tasks are adopted by composite capabilities to describe themselves; this is relevant for
		 * 'stubs' like doRoute, where the capability is created and 'filled in' with plans later
		 */
		if(tName.equals("moveTo")){ //plan to form and follow a route
			g = new GoalTask(Literal.parseLiteral("moveTo(AGENT, O, J)"), 
					new String[]{
							"location(O) & location(J) & not mortal(AGENT) & atJ(AGENT, O)", 
							"connection(O) & location(J) & not mortal(AGENT) & onR(AGENT, O)"},
					new String[]{"-onR(AGENT, O)", "-atJ(AGENT,O)", "-overJ(AGENT,O)", "+atJ(AGENT, J)"});
		}
		if(tName.equals("moveAndSecure")){ 
			//plan to form and follow a route; applies to moveAgent capability which includes 
			//clearing dangerzones if required
			g = new GoalTask(Literal.parseLiteral("moveAndSecure(AGENT, J1, J2)"), 
					new String[]{
							"location(J1) & location(J2) & not mortal(AGENT) & atJ(AGENT, J1)", 
							"connection(J1) & location(J2) & not mortal(AGENT) & onR(AGENT, J1)"},
					new String[]{"-onR(AGENT, J1)", "-atJ(AGENT, J1)", "-dangerZone(J1)", "-dangerZone(J2)", "+atJ(AGENT, J2)"});
		}
		else if(tName.equals("move"))	{
			g = new GoalTask(
					Literal.parseLiteral("move(AGENT, RID, O, J)"),
					new String[]{"atJ(AGENT, O)", "onR(AGENT, RID)"},
					new String[]{"-onR(AGENT, O)", "-atJ(AGENT,O)", "+atJ(AGENT, J)"}
					);
		}
		else if(tName.equals("unblock"))	{
			g = new GoalTask(
					Literal.parseLiteral("unblock(AGENT, RID, UO, UJ)"),
					new String[]{"blocked(UO, UJ) & road(RID, UO, UJ)"},
					new String[]{"-blocked(UO, UJ)", "-blocked(UJ, UO)"}
					);
		}
		//goUnblock(BDAGENT, ALOC, RID, R1, R2)
		//composite for sending a bulldozer...
		else if(tName.equals("goUnblock"))	{
			g = new GoalTask(
					Literal.parseLiteral("goUnblock(AGENT, AO, RID, UO, UJ)"),
					new String[]{"blocked(UO, UJ) & road(RID, UO, UJ)"},
					new String[]{"-blocked(UO, UJ)", "-blocked(UJ, UO)"}
					);
		}
		//secureEnds(R1, R2)
		//composite for securing endpoints if either (or both) are dangerous
		else if(tName.equals("secureEnds"))	{
			g = new GoalTask(
					Literal.parseLiteral("secureEnds(R1, R2)"),
					new String[]{	"dangerZone(R1) & dangerZone(R2)", 
									"not dangerZone(R1) & dangerZone(R2)",
									"dangerZone(R1) & not dangerZone(R2)"},
					new String[]{"-dangerZone(R1)", "-dangerZone(R2)"}
					);
		}
		else if(tName.equals("load"))	{
			g = new GoalTask(
					Literal.parseLiteral("load(AGENT, LJ, LC)"),
					new String[]{"not mortal(AGENT) & atJ(AGENT, LJ) & cargoAt(LC, LJ) & not carryingCargo(AGENT)"},
					new String[]{"-cargoAt(LC,LJ)", "+loaded(AGENT, LC)", "+carryingCargo(AGENT)"}
					);
		}
		else if(tName.equals("unload")){
			g = new GoalTask(
					Literal.parseLiteral("unload(AGENT, LJ, LC)"),
					new String[]{"not mortal(AGENT) & atJ(AGENT, LJ) & loaded(AGENT, LC) & carryingCargo(AGENT)"},
					new String[]{"+cargoAt(LC, LJ)", "-loaded(AGENT, LC)", "-carryingCargo(AGENT)"}//, "-cargoNeeded(LJ)"}
					);
		}
		else if(tName.equals("consume")){
			g = new GoalTask(
					Literal.parseLiteral("consume(AGENT, J, C)"),
					new String[]{"cargo(C) & location(J) & cargoAt(C, J) & cargoNeeded(J)"},
					new String[]{"-cargo(C)", "-cargoAt(C,J)", "-cargoNeeded(LJ)"}
					);
		}
		else if(tName.equals("register"))	{ //internal only - stub
			g = new GoalTask(
					Literal.parseLiteral("register(J)"), //J= initial location or random val
					new String[]{"true"},
					new String[]{});
		}
		else if(tName.equals("vehicle.capabilities.PlanRoute"))	{ //INTERNAL ONLY - stub
			g = new GoalTask(
					Literal.parseLiteral("vehicle.capabilities.PlanRoute(O, J)"),
					new String[]{"true"}, //always true!
					new String[]{});//no changes
		}
		else if(tName.equals("doRoute"))	{//plan to doRoute, move
			g = new GoalTask(
					Literal.parseLiteral("doRoute(AGENT, O, J)"),  //similar to move op
					new String[]{"atJ(AGENT, O)", /*OR*/ "onR(AGENT, O)"},
					new String[]{"-atJ(AGENT, O)", "-onR(AGENT, O)", "+atJ(AGENT, J)"});
		}
		else if(tName.equals("supply"))	{
			//supply cargo to area...
			g = new GoalTask(Literal.parseLiteral("supply(J)"), new String[]{"cargoNeeded(J)"}, new String[]{"-cargoNeeded(J)"});
		}
		//else if(tName.equals("start"))	{//REDUNDANT?
		//	g = new GoalTask(Literal.parseLiteral("start"),
		//			new String[]{"not registered"},
		//			new String[]{"registered"});
		//s}
		else if(tName.equals("moveCargo"))	{ //agent, agent loc, cargo, source Junction, dest Junction
			g = new GoalTask(Literal.parseLiteral("moveCargo(AGENT, O, C, K, J)"),
					new String[]{
					"location(O) & location(K) & cargoAt(C, K) & not cargoAt(C, J) & atJ(AGENT, O)",
					"connection(O) & location(K) & cargoAt(C, K) & not cargoAt(C, J) & onR(AGENT, O)"}, //c is at K
					new String[]{"-cargoAt(C, K)", "+cargoAt(C, J)"}); //note; goal is move, consume is later...
		}
		/*
		 +!moveAndClear(AGENT, RID, START, END):	registered(AGENT) & not busy(AGENT) & blocked(RID) & not mortal(AGENT) & 
									not atJ(START) & atJ(O)
		 */
		else if(tName.equals("moveAndClear"))	{ //agent, road (id) to clear, agent loc, start junction, end junction
			g = new GoalTask(Literal.parseLiteral("moveAndClear(AGENT, RID, O, J1, J2)"),
					new String[]{"blocked(J1, J2) & blocked(J2, J1) & road(RID, J1, J2) & location(O) & location(J1) & location(J2)"},
					//semi-redundant, as assuming bidirectional roads..
					new String[]{"-blocked(J1, J2)", "-blocked(J2, J1)"	});
		}
		/*
		 * APC goals 
		 */
		else if(tName.equals("secureArea"))	{
			g = new GoalTask(Literal.parseLiteral("secureArea(AGENT, O)"),
					new String[]{	"atJ(AGENT,O) & dangerZone(O)"},
					new String[]{	"-dangerZone(O)"	});
		}
		
		/*
		 * Heli goals
		 * takeoff, land, fly
		 */
		else if(tName.equals("takeOff"))	{
			g = new GoalTask(Literal.parseLiteral("takeOff(AGENT, TO)"),
					new String[]{	"atJ(AGENT, TO) & not flying(AGENT)"},
					new String[]{	"-atJ(AGENT,TO)", "+flying(AGENT)", "+overJ(AGENT,LOC)"}); //latter constrains solution length...
		}

		else if(tName.equals("land"))	{
			g = new GoalTask(Literal.parseLiteral("land(AGENT, LOC)"),
					new String[]{	"atJ(AGENT,LOC) & flying(AGENT)"},
					new String[]{	"-overJ(AGENT,LOC)","-flying(AGENT)", "+atJ(AGENT,LOC)"	});//latter constrains solution length...
		}

		else if(tName.equals("fly"))	{
			g = new GoalTask(Literal.parseLiteral("fly(AGENT, END)"),
					new String[]{	"flying(AGENT)"		},
					new String[]{	"+overJ(AGENT,END)", "+atJ(AGENT, END)"	});
		}
		/*
		 * Hazmat goals
		 */
		else if(tName.equals("decontaminate"))	{
				g = new GoalTask(Literal.parseLiteral("decontaminate(AGENT, ROAD, START, END)"),
						new String[]{	"atJ(AGENT,START) & toxic(START, END)"},
						new String[]{	"-toxic(START,END)", "-toxic(END, START)", "+atJ(AGENT, END)"	});
		}/*
		 * Mil Hq (composite) goals
		 */
		//send a hazmat unit stationed at AGST and clear the road from RoadSt to RoadEnd
		else if(tName.equals("sendHazmat"))	{
			g = new GoalTask(Literal.parseLiteral("sendHazmat(AGENT, AGST, ROAD, ROADST, ROADEND)"),
					new String[]{	"toxic(ROADST, ROADEND)"},
					new String[]{	"-toxic(ROADST, ROADEND)", "-toxic(ROADEND, ROADST)", "+atJ(AGENT, ROADEND)"	});
		}
		//+!decontaminateRoad(SELF, RID, R1, R1) - clear a road from R1 to R2; agent is undefined / is holder
		else if(tName.equals("decontaminateRoad"))	{
			g = new GoalTask(Literal.parseLiteral("decontaminateRoad(SELF, RID, ROADST, ROADEND)"),
					new String[]{	"toxic(ROADST, ROADEND) & toxic(ROADEND, ROADST)"},
					new String[]{	"-toxic(ROADST, ROADEND)", "-toxic(ROADEND, ROADST)"});
		}
		//secure(AGENT, loc) - agent = milHQ, loc=location to secure
		else if(tName.equals("secure"))	{
			g = new GoalTask(Literal.parseLiteral("secure(SELF, LOC)"),
					new String[]{	"dangerZone(LOC)"},
					new String[]{	"-dangerZone(LOC)"});
		}
		//+!sendToSecure(AGENT, LOC, SECURE)
		else if(tName.equals("sendToSecure"))	{
			g = new GoalTask(Literal.parseLiteral("sendToSecure(AGENT, LOC, SECURE)"),
					new String[]{	"dangerZone(SECURE) & atJ(AGENT, LOC)"},
					new String[]{	"-dangerZone(SECURE)", "-atJ(AGENT,LOC)", "+atJ(AGENT,SECURE)"});
		}
		//unstick(A, RID, R1, R2)
		//rescue agent A stuck due to changes - does not apply to post nd failure cooldown effects
		else if(tName.equals("unstick"))	{
			g = new GoalTask(Literal.parseLiteral("unstick(HOLDER, A, RID, R1, R2)"),
					new String[]{	"not resting(A) & stuck(A) & not mortal(A) & onR(RID) & road(RID, R1, R2) & " +
									"blocked(R1, R2) & not toxic(R1, R2)",
									"not resting(A) & stuck(A) & not mortal(A) & onR(RID) & road(RID, R1, R2) & " +
									"toxic(R1, R2) & not blocked(R1,R2)",},
					new String[]{	"-stuck(A)", "-blocked(R1, R2)", "-blocked(R2, R1)", "-toxic(R1, R2)", "-toxic(R2, R1)"});
		}
		//free(A, R, R1, R2)
		//state transition to unstuck agent
		else if(tName.equals("free"))	{
			g = new GoalTask(Literal.parseLiteral("unstick(A, RID, R1, R2)"),
					new String[]{	"not resting(A) & stuck(A) & not mortal(A) & onR(RID) & road(RID, R1, R2)",
									"not resting(A) & stuck(A) & not mortal(A) & onR(RID) & road(RID, R1, R2)"},
					new String[]{	"-stuck(A)"});
		}
		
		if(g==null){	
			logger.warning("Unknown task for " + tName);	
		}
		
		return g;
	}
	
	/**
	 * Intended for composite capabilities (inferred from plans), where l corresponds to the triggering literal
	 * @param l
	 * @return
	 */
	public GoalTask factory(Literal l)	{	
		if(l.isInternalAction())	{
			//we still check these, but can get a 'null' warning if no model.  
			//This message is for debug clarification.
			logger.warning(l + " represents an atomic action");
		}
		GoalTask goal =  factory(l.getFunctor());	
		
		if(goal == null)	{	return goal;	}
		//signature normalization against l
		Literal gSig = goal.getSignature();
		if(gSig.equalsAsStructure(l)){
			logger.info("Task equality for sig; " + gSig + ":" + l);
			return goal;
		}
		
		Unifier u = AgentUtils.formUnifierFromArgs(gSig, l);
		logger.info( gSig + ":" + l + "; unifier = " + u);
		String[] eff = goal.getGoalEffects();
		String[] uEff = new String[eff.length];
		for(int i=0; i<uEff.length; i++)	{
			String pre = eff[i].substring(0,1);
			String lit = eff[i].substring(1);
			Literal modL = Literal.parseLiteral(lit);
			logger.info("old effect literal = " + pre + lit);
			modL.apply(u);
			logger.info("new effect literal = " + pre + modL);
			uEff[i] = pre + modL.toString();
		}
		
		LogicalFormula[] lf = goal.getPreconditions(u);
		String[] uPre = new String[lf.length];
		for(int i=0;i<lf.length;i++)	{	uPre[i] = lf[i].toString();	}
		
		goal = new GoalTask(l, uPre, uEff);
		logger.info("Goal = " + goal);
		
		return goal;
	}
	
}
