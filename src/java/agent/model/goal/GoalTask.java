package agent.model.goal;

import jason.asSemantics.Unifier;
import jason.asSyntax.ASSyntax;
import jason.asSyntax.Literal;
import jason.asSyntax.LiteralImpl;
import jason.asSyntax.LogExpr;
import jason.asSyntax.LogicalFormula;
import jason.asSyntax.Term;
import jason.asSyntax.VarTerm;
import jason.asSyntax.parser.ParseException;

import java.util.List;
import java.util.Vector;
import java.util.logging.Logger;

import common.UniversalConstants;


/**
 * 
 * @author Alan White
 *
 * Describes a goal, that can be achieved by a capability.  Capability descriptors
 * extend this basic description with the addition of further pre or post-conditions
 * describing further constraints or side-effects respectively.  
 * 
 * The information in the GoalTask object can be viewed as representing that held within
 * a planning problem description; the information in the capability as representing an 
 * operator or plan description, which may include side-effects not relevant to the goal.
 * 
 * For example, a 'moveTo' Goal is satisfied when the given agent is at the given location;
 * the moveTo capability will have sideffects on top of this, including fuel reduction or
 * wear-and-tear type damage.  The capability will also have constraints on when it can be
 * used (relevant to the executing agent, but not the root task).
 */
public class GoalTask {
	
	/**
	 * Logger object
	 */
	public Logger logger = Logger.getLogger(this.getClass().getSimpleName());
	
	private final String signature;

	private String[] precondition, effects, addEff, deleteEff;
	
	/**
	 * Forms a new goal descriptor; the terms in the signature should match the terms in a capability.
	 */
	public GoalTask(Literal sig, String[] pre, String[] goal)	{
		logger.setLevel(UniversalConstants.CAPABILITY_LOGGER_LEVEL);
		this.signature = sig.toString();
		this.precondition = pre;
		this.effects = goal;
		//figure out add/deletes...
		Vector<String> add = new Vector<String>();
		Vector<String> del = new Vector<String>();
		for(String s: this.effects)	{
			if(s.startsWith("+"))		{	add.add(s.substring(1));	}
			else if(s.startsWith("-"))	{	del.add(s.substring(1));	}
		}
		addEff 		= new String[add.size()];
		deleteEff 	= new String[del.size()];
		for(int i=0;i<addEff.length; i++)		{	addEff[i] = add.get(i);		}
		for(int i=0;i<deleteEff.length; i++)	{	deleteEff[i] = del.get(i);	}
	}

	/**
	 * The task name/descriptor.  This should serve as a UUID, and correspond to a capability functor.
	 * @return literal - functor and unground arguments
	 */
	public final Literal getSignature()	{ return LiteralImpl.parseLiteral(signature);	}
	
	/**
	 * Forms a unifier, given the set of args in order, based on the standard variable
	 * names defined for getPreString and getPostString
	 * @param args
	 * @return empty unifier if args mismatch
	 */
	public final Unifier formUnifier(List<Term> args)	{
		Unifier u = new Unifier();
		if(args.size() != getSignature().getTerms().size()){
			logger.warning("Mismatch between signature (" + signature.toString() + ") " +
					"and args (" + args.toString() + ")");
			return u;
		}
		else	{
			for(int i=0;i<getSignature().getTerms().size();i++){
				try {
					VarTerm t = ASSyntax.parseVar(getSignature().getTerm(i).toString());
					Term vl = args.get(i);
					u.bind(t, vl);
				} catch (ParseException e) {
					logger.severe("Parse error at term " + i + "; " + e.toString());
					e.printStackTrace();
					return null;
				}
			}
			return u;
		}
	}

	/**
	 * Preconditions for the <i> validity</i> of this task being a goal; i.e. if these do not hold, then
	 * it is inferred the task can be dropped without proceeding to the point of selecting a plan, etc.  This
	 * mostly applies to composite capabilities, which involve selection of a context-dependent plan.  This
	 * is a LogicalFormula.  Args may be specified or null; the former refers to testing concrete(ground) instances, 
	 * and will have the LF returned with specific values.
	 * </br></br>
	 * final method; modify getPreString methods to return the appropriate string for parsing to a LogicalFormula object
	 * @param arg
	 * @return
	 */
	public final LogicalFormula[] getPreconditions(List<Term> arg){	return getPreconditions(formUnifier(arg));	}
	
	/**
	 * Return preconditions using the given unifier
	 * @param u
	 * @return
	 */
	public final LogicalFormula[] getPreconditions(Unifier u){
		LogicalFormula[] ret = new LogicalFormula[precondition.length];

		for(int i=0; i<precondition.length; i++)	{
			ret[i] = LogExpr.parseExpr(precondition[i]);
			ret[i].apply(u);
			/*
			 * for some reason, preconditions aren't evaluating correcly unless re-string and 
			 * re-parse them after being unified?  This works, regardless...
			 */
			ret[i] = LogExpr.parseExpr(ret[i].toString());
		}
		return ret;
	}
	
	public final String[] getPreconditions() {	return precondition;	}
	
	public final String[] getGoalEffects() 	{	return effects;	}	
	
	
	/**
	 * Get states deleted as posteffects
	 * @return
	 */
	public String[] getDeleteEffects()	{	return deleteEff;	}
	
	/**
	 * Get literals added as Posteffects
	 * @return
	 */
	public String[] getAddEffects()	{	return addEff;	}
	
	@Override
	public String toString(){
		String pre = " ", eff = " ";
		for(String s: precondition)	{	pre = pre + " " + s;	}
		for(String s: effects)		{	eff = eff + " " + s;	}
		return getSignature() + ", PRE[" + pre.trim() + "], EFF/GOAL[" + eff.trim() + "]";
	}
}
