/**
 * 
 */
package agent.beliefBase;

import static truckworld.env.Percepts.BLOCKED;
import static truckworld.env.Percepts.BLOCKED_ROAD;
import static truckworld.env.Percepts.DANGERZONE;
import static truckworld.env.Percepts.TOXIC;
import static truckworld.env.Percepts.TOXIC_ROAD;

import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

import common.Analytics;
import common.Analytics.AnalyticsOperations;
import common.UniversalConstants;
import common.UniversalConstants.RoadCondition;
import common.UniversalConstants.RoadType;
import jason.asSemantics.Unifier;
import jason.asSyntax.ASSyntax;
import jason.asSyntax.Literal;
import jason.asSyntax.Term;
import jason.bb.BeliefBase;
import jason.bb.DefaultBeliefBase;
import truckworld.exceptions.ConnectionExistsException;
import truckworld.world.Junction;
import truckworld.world.Road;
import truckworld.world.interfaces.JunctionInf;
import truckworld.world.interfaces.RoadInf;
/**
 * @author Alan White
 * 
 * A default belief base, extended such that a (Java) <i>World</i> object is 
 * created and used to store beliefs about the environment.  Roads, junctions, etc are
 * added to this world object in response to the appropriate percepts.
 * </br></br>
 * Used by any agent that maintains a map of the geography.  Does NOT contain that
 * agents percepts about its own location, status, etc - used simply to allow additional
 * java based representation of geography.  Percepts are held as per standard in the
 * default BB superclass.
 * </br></br>
 * <B>Note: Sub-classes will need to implement their own clone() method in order to override
 *  the default BB behaviour, due to the superclass-enforced return type.</b>
 * 
 * @see truckworld.world.Junction
 * @see truckworld.world.interfaces.JunctionInf
 * @see truckworld.world.Road
 */
public class WorldBeliefBase extends DefaultBeliefBase	{
	protected static int COUNTER = 1; //used for UUID-ing loggers
	protected Logger logger;
	protected Map<String, JunctionInf> junctions;
	protected Map<String, RoadInf> roads;

	/**
	 * Maps a 2-entry array to a string identifier.  Used to ensure roads are created with the same reference ID as
	 * used by the environment object
	 */
	private Map<String, String> roadIds;

	/**
	 * Identifier strings for cities, villages, etc - corresponds to a junction id
	 */
	protected Collection<String> cities, villages, airports, seaports, refuelStations, repairStations;

	/*
	 *These are used to temporarily hold values for road information, until
	 *enough percepts are received to 'build' the road object 
	 */
	private Map<String, RoadCondition> condition;
	private Map<String, Boolean> blocked;
	private Map<String, RoadType> type;
	private Collection<String> dangerZones;

	public WorldBeliefBase(){
		super();
		//set logger level
		setLogger(getClass().getSimpleName() + ":" + ++COUNTER);
		logger.setLevel(UniversalConstants.BB_LOGGER_LEVEL);
		//init objects
		junctions	= 	new HashMap<String, JunctionInf>();
		roads 		= 	new HashMap<String, RoadInf>();
		condition 	= 	new ConcurrentHashMap<String, RoadCondition>(8,0.9f,1);
		blocked 	= 	new ConcurrentHashMap<String, Boolean>(8,0.9f,1);
		type 		= 	new HashMap<String, RoadType>();
		roadIds 	= 	new HashMap<String, String>();
		cities		= 	new Vector<String>();
		villages	= 	new Vector<String>(); 
		airports	= 	new Vector<String>(); 
		seaports	= 	new Vector<String>(); 
		refuelStations	= 	new Vector<String>();
		repairStations	= 	new Vector<String>();
		dangerZones	= 	new Vector<String>();
	}

	protected void setLogger(String lid)	{	logger = Logger.getLogger(lid);	}

	/**
	 * Extends the default BB add implementation through adding a 'custom' add method used to parse
	 * beliefs into local java objects - i.e. to build the world map.  
	 * @see customAdd(Literal) in this class
	 */
	@Override
	protected boolean add(Literal l, boolean addInEnd) {
		synchronized(this) {
			final Literal bl = contains(l);
			final boolean exists = (bl != null && !bl.isRule()); // check if in BB
			final boolean added = super.add(l, addInEnd); // add to BB using super-method
			if (added && !exists) {
				customAdd(l);
			}
			return added;
		}
	}

	@Override
	public boolean remove(Literal l)	{
		synchronized(this) {
			final boolean result = super.remove(l);
			customRemove(l);
			return result;
		}
	}

	/**
	 * 'Add' used by clone(), which skips parsing through literals to add new road objects,
	 * as these will be built already and copied directly.  Thereforce, just calls the supper
	 * method for DefaultBB implementation.
	 * @param l
	 * @param addInEnd
	 * @return
	 * @see super.add
	 */
	protected boolean addForClone(Literal l)	{	
		return super.add(l, true);
	}

	/**
	 * This method performs additional actions on top of the default
	 * belief base 'add' method.  This is called in the event that the literal addition has
	 * succeeded (super.add returns true), and that the literal did not previously exist.
	 * </br></br>
	 * It is moved to a new method in order to support synchronization up internal model
	 * updates, i.e. to avoid multiple adds having unexpected behaviour wrt the java objects - 
	 * such as resulting in over-addition of entries to roadId.
	 * @param newLiteral
	 */
	protected void customAdd(Literal newLiteral) {
		synchronized (this) {
			//added the belief; update data objects as and if necessary
			String name = newLiteral.getFunctor();
			List<Term> terms = newLiteral.getTerms();
			boolean negated = newLiteral.negated();
			String tryRGen = null;//false;
			logger.fine("Handle percept " + newLiteral);
			//definition of a new road id and connected junctions
			if (name.equals("road")) {
				String id = parseTerm(terms.get(0));
				String j1Id = parseTerm(terms.get(1));
				String j2Id = parseTerm(terms.get(2));
				roadIds.put(j1Id + UniversalConstants.ROAD_ID_SEPARATOR_CHAR + j2Id, id);
				roadIds.put(j2Id + UniversalConstants.ROAD_ID_SEPARATOR_CHAR + j1Id, id);
				tryRGen = id;
			}
			//status changes
			else if (name.equals(RoadCondition.slippery.name())) {
				String fr = parseTerm(terms.get(0));
				String to = parseTerm(terms.get(1));
				String id = roadIds.get(fr + UniversalConstants.ROAD_ID_SEPARATOR_CHAR + to);
				if (roads.containsKey(id) && !negated) {
					roads.get(id).setCondition(RoadCondition.slippery);
					logger.fine("Slippery road! " + id);
					return;
				} else {
					if (!negated) {
						condition.put(fr + UniversalConstants.ROAD_ID_SEPARATOR_CHAR + to, RoadCondition.slippery);
						if (id == null) {
							logger.warning("Null id, " + fr + ", " + to + " rID=" + roadIds);
						}
						tryRGen = id;
					}
				}
			} else if (name.equals(RoadCondition.flooded.name())) {
				String fr = parseTerm(terms.get(0));
				String to = parseTerm(terms.get(1));
				String id = roadIds.get(fr + UniversalConstants.ROAD_ID_SEPARATOR_CHAR + to);
				if (roads.containsKey(id) && !negated) {
					roads.get(id).setCondition(RoadCondition.flooded);
					logger.fine("Flooded road! " + id);
					return;
				} else {
					if (!negated) {
						condition.put(fr + UniversalConstants.ROAD_ID_SEPARATOR_CHAR + to, RoadCondition.flooded);
						if (id == null) {
							logger.warning("Null id, " + fr + ", " + to + " rID=" + roadIds);
						}
						tryRGen = id;
					}
				}
			} else if (name.equals(RoadCondition.dry.name())) {
				String fr = parseTerm(terms.get(0));
				String to = parseTerm(terms.get(1));
				String id = roadIds.get(fr + UniversalConstants.ROAD_ID_SEPARATOR_CHAR + to);
				if (roads.containsKey(id) && !negated) {
					roads.get(id).setCondition(RoadCondition.dry);
					return;
				} else {
					if (!negated) {
						condition.put(fr + UniversalConstants.ROAD_ID_SEPARATOR_CHAR + to, RoadCondition.dry);
						if (id == null) {
							logger.warning("Null id, " + fr + ", " + to + " rID=" + roadIds);
						}
						tryRGen = id;
					}
				}
			} else if (name.equals(BLOCKED) && terms.size() == 2) {
				String fr = parseTerm(terms.get(0));
				String to = parseTerm(terms.get(1));
				String id = roadIds.get(fr + UniversalConstants.ROAD_ID_SEPARATOR_CHAR + to);
				logger.finer("roadIDs get (" + (fr + UniversalConstants.ROAD_ID_SEPARATOR_CHAR + to) + ")= " + id
						+ ", roadIDs=" + roadIds + " roads=" + roads);
				if (roads.containsKey(id)) {
					roads.get(id).setBlocked(!negated);
					return;
				} else {
					blocked.put(fr + UniversalConstants.ROAD_ID_SEPARATOR_CHAR + to, !negated);
					if (id == null) {
						logger.warning("Null id, " + fr + ", " + to + " rID=" + roadIds);
					}
					tryRGen = id;
				}
			} else if (name.equals(BLOCKED_ROAD) && terms.size() == 1) {
				String id = parseTerm(terms.get(0));
				if (roads.containsKey(id)) {
					roads.get(id).setBlocked(!negated);
					return;
				} else {
					blocked.put(id, !negated);
					if (id == null) {
						logger.warning("Null id, " + id + " rID=" + roadIds);
					}
					tryRGen = id;
				}
			}
			//surface percept - mud or tarmac
			else if (name.equals(RoadType.mud.name())) {
				//terms = from - to
				String from = parseTerm(terms.get(0));
				String to = parseTerm(terms.get(1));
				String id = roadIds.get(from + UniversalConstants.ROAD_ID_SEPARATOR_CHAR + to);

				if (roads.containsKey(id)) {
					logger.finest("Already have road " + id);
					return;
				} else {
					type.put(from + UniversalConstants.ROAD_ID_SEPARATOR_CHAR + to, RoadType.mud);
					if (id == null) {
						logger.warning("Null id, " + from + ", " + to + " rID=" + roadIds);
					}
					tryRGen = id;
				}
			} else if (name.endsWith(RoadType.tarmac.name())) {
				//terms = from - to
				String from = parseTerm(terms.get(0));
				String to = parseTerm(terms.get(1));
				String id = roadIds.get(from + UniversalConstants.ROAD_ID_SEPARATOR_CHAR + to);

				if (roads.containsKey(id)) {
					logger.finest("Already have road " + id);
					return;
				} else {
					type.put(from + UniversalConstants.ROAD_ID_SEPARATOR_CHAR + to, RoadType.tarmac);
					if (id == null) {
						logger.warning("Null id, " + from + ", " + to + " rID=" + roadIds);
					}
					tryRGen = id;
				}
			}
			//a new junction - co-ords; we can do this immediately!
			else if (name.equals("junction")) {
				String id = parseTerm(terms.get(0));

				if (junctions.containsKey(id)) {
					logger.finest("Duplicate co-ordinate percept; " + terms + ", existing entry = "
							+ junctions.get(id).toString());
					return;
				} else {
					double x = Double.parseDouble(parseTerm(terms.get(1)));
					double y = Double.parseDouble(parseTerm(terms.get(2)));
					Junction j = new Junction(id, x, y, 1); //Note; we don't model elevation...
					logger.finest("Got a new junction; " + j.toString());
					junctions.put(id, j);

					for (JunctionInf j2 : junctions.values()) {
						String id2 = j2.getId();
						if (roadIds.containsKey(new String[] { id, id2 })) {
							tryRGen = roadIds.get(new String[] { id, id2 });
						} else if (roadIds.containsKey(new String[] { id2, id })) {
							tryRGen = roadIds.get(new String[] { id2, id });
						}
					}
				}
			} else if (name.equals("city")) {
				String id = parseTerm(terms.get(0));
				this.cities.add(id);
			} else if (name.equals("village")) {
				String id = parseTerm(terms.get(0));
				this.villages.add(id);
			} else if (name.equals("airport")) {
				String id = parseTerm(terms.get(0));
				this.airports.add(id);
			} else if (name.equals("seaport")) {
				String id = parseTerm(terms.get(0));
				this.seaports.add(id);
			} else if (name.equals("repairStation")) {
				String id = parseTerm(terms.get(0));
				this.repairStations.add(id);
			} else if (name.equals("refuelStation")) {
				String id = parseTerm(terms.get(0));
				this.refuelStations.add(id);
			} else if (name.equals(TOXIC) & terms.size() == 2) {
				String fr = parseTerm(terms.get(0));
				String to = parseTerm(terms.get(1));
				String id = roadIds.get(fr + UniversalConstants.ROAD_ID_SEPARATOR_CHAR + to);
				RoadInf r = this.getRoad(id);

				if (r == null) {//don't start with toxic roads...
					logger.severe("null road for " + newLiteral);
				} else {
					r.setToxic(true);
				}
			} else if (name.equals(TOXIC_ROAD) & terms.size() == 1) {
				String id = parseTerm(terms.get(0));
				RoadInf r = this.getRoad(id);

				if (r == null) {//don't start with toxic roads...
					logger.severe("null road for " + newLiteral);
				} else {
					r.setToxic(true);
				}
			} else if (name.equals(DANGERZONE)) {
				String loc = parseTerm(terms.get(0));
				if (!dangerZones.contains(loc)) {
					dangerZones.add(loc);
				}
				if (junctions.containsKey(loc)) {
					junctions.get(loc).setDangerous(true);
				} else {
					logger.severe(loc + "; DZ junction object not present in " + junctions);
				}

				for (RoadInf r : roads.values()) {
					if (r.getEndIds()[0].equals(loc)) {
						r.getEnds()[0].setDangerous(true);
					}
					if (r.getEndIds()[1].equals(loc)) {
						r.getEnds()[1].setDangerous(true);
					}
				}
			}
			//other percepts; ignore
			else {
				logger.finest("WorldBB not performing any extra handling " + "for percept " + name);
				return;
			}
			//check to see if can create any new roads with the gather percept info
			logger.fine(newLiteral + ", Try create new roads: " + tryRGen);
			if (tryRGen != null) {
				for (String j : roadIds.keySet()) {
					if (roadIds.get(j).equals(tryRGen)) {
						tryCreateNewRoad(tryRGen, j.split(UniversalConstants.ROAD_ID_SEPARATOR_CHAR));
						return;
					}
				}
			}
		}

	}

	/**
	 * Tries to create a new road object for the given String id.  Require that junction objects be
	 * present in the WBB, as well as road surface type and condition into
	 * @param id Identifier for the road
	 * @param js Junction ids for both ends
	 */
	private void tryCreateNewRoad(String id, String[] js) {
		//need entries for all data
		if(roads.containsKey(id))	{
			logger.fine(id + " already exists! - no need to try and create...");
			return;
		}
		String jsTempID = js[0]+UniversalConstants.ROAD_ID_SEPARATOR_CHAR+js[1];

		logger.fine("Create new road? id = " + id + ", j=" + js[0] +", " + js[1]);
		if(		junctions.containsKey(js[0])		&&
				junctions.containsKey(js[1])		&&
				type.containsKey(jsTempID) 			&&
				condition.containsKey(jsTempID)			)	{
			logger.finest("Building road : " + id);
			try {
				//assume not blocked! - can always set later
				if(!blocked.containsKey(jsTempID))	{	blocked.put(jsTempID, false);	}
				RoadInf r = new Road(
						id, //roadIds.get(jsTempID),
						type.remove(jsTempID), 
						junctions.get(js[0]), 
						junctions.get(js[1]),
						condition.get(jsTempID),
						blocked.remove(jsTempID));
				logger.finest("Adding road to agent model;" + r.getId());
				roads.put(r.getId(), r);
				logger.fine("Built road : " + r.getId() + ", roads=" + roads);

				//removals to decrease future iteration time...
				condition.remove(jsTempID);
				condition.remove(jsTempID);
				blocked.remove(jsTempID);
				blocked.remove(jsTempID);
				type.remove(jsTempID);
				type.remove(jsTempID);
			} catch (ConnectionExistsException e) {
				logger.finest(	"Removing duplicate bi-directional road ID from " +
						"consideration; " + id);
				roads.remove(id);
				//note; this is ok, because creating bi-directional percepts
				//logger.severe("Duplicate junction connection (expected) exception; " + id);//e.toString());
				//e.printStackTrace();
			}
		}	
		else	{
			logger.fine("Cannot create " + id + ", j1=" + junctions.containsKey(js[0]) + 
					", j2=" + junctions.containsKey(js[1])	+
					", type=" + type.containsKey(jsTempID)  +
					", condition=" + condition.containsKey(jsTempID));
			logger.finer("cond: " + condition);
			logger.finer("type: " + type);
		}//endif
	}

	/**
	 * Removes quotation marks from terms
	 * @param in
	 * @return
	 */
	protected String parseTerm(Term in)			{	return in.toString().replace("\"", "");	}

	/**
	 * Fetches a junction from java beliefs
	 * @param id String key id
	 * @return JunctionInf
	 */
	public JunctionInf getJunction(String id) 	{	return junctions.get(id);	}

	/**
	 * Fetches and returns a road by id key
	 * @return RoadInf
	 */
	public RoadInf getRoad(String id)			{	return roads.get(id);	}


	/**
	 * Fetches and returns a road by endpoints
	 * @return RoadInf; null if no road found for endpoints
	 */
	public RoadInf getRoad(String j1, String j2)	{	
		logger.finer("Get road: " + j1 + UniversalConstants.ROAD_ID_SEPARATOR_CHAR + j2 + ", Roads=" + roads.keySet() + ", RoadIds=" + roadIds);
		RoadInf 		r = getRoad(roadIds.get(j1 + UniversalConstants.ROAD_ID_SEPARATOR_CHAR + j2));
		if(r==null)	{	r = getRoad(roadIds.get(j2 + UniversalConstants.ROAD_ID_SEPARATOR_CHAR + j1));	}

		if(r==null){
			logger.warning(j1 + UniversalConstants.ROAD_ID_SEPARATOR_CHAR + j2 + ", Road not found; roadIDs = " + roadIds.toString() + 
					": " + roadIds.containsKey(j1+UniversalConstants.ROAD_ID_SEPARATOR_CHAR+j2)  + "|"  + roadIds.containsKey(j2+UniversalConstants.ROAD_ID_SEPARATOR_CHAR+j1));
			logger.warning(j1 + UniversalConstants.ROAD_ID_SEPARATOR_CHAR + j2 + ", Road not found; roads = " + roads.keySet().toString());
		}

		return r;	
	}

	/**
	 * Adapted from default belief base...
	 */
	public synchronized BeliefBase clone(){
		return geoCopy(new WorldBeliefBase());
	}
	
	private WorldBeliefBase geoCopy(final WorldBeliefBase bb) {
		return geoCopy(bb, 1);
	}
	/**
	 * Used for world BB clone operations, so we can move it outside the clone() method and extend
	 * clone() elsewhere without having to rewrite this bit...
	 * @param bb
	 * @return
	 */
	private WorldBeliefBase geoCopy(final WorldBeliefBase bb, final int count) {
		try {
			synchronized (this) {
				long start = System.nanoTime();
				// now setup the roads... copy roads, copy junctions...
				try {
					// first, setup a new junction map...
					Map<String, JunctionInf> jCopy = new ConcurrentHashMap<String, JunctionInf>();
					for (String key : getJunctions().keySet()) {
						jCopy.put(key, (JunctionInf) getJunctions().get(key).clone());
					}

					// then, setup a new road map using jCopy
					Map<String, RoadInf> rCopy = new ConcurrentHashMap<String, RoadInf>();
					for (String key : getRoads().keySet()) {
						rCopy.put(key, (RoadInf) getRoads().get(key).clone());
					}

					// copy id mapping
					Map<String, String> ridCopy = new ConcurrentHashMap<String, String>();
					for (String key : roadIds.keySet()) {
						ridCopy.put(key, roadIds.get(key));
					}

					// copy refuel, etc...
					Collection<String> citiesCopy = new Vector<String>();
					Collection<String> villagesCopy = new Vector<String>();
					Collection<String> airportsCopy = new Vector<String>();
					Collection<String> seaportsCopy = new Vector<String>();
					Collection<String> refuelStationsCopy = new Vector<String>();
					Collection<String> repairStationsCopy = new Vector<String>();
					citiesCopy.addAll(this.cities);
					villagesCopy.addAll(this.villages);
					airportsCopy.addAll(this.airports);
					seaportsCopy.addAll(this.seaports);
					refuelStationsCopy.addAll(this.refuelStations);
					repairStationsCopy.addAll(this.repairStations);

					// finally, set!
					bb.setJunctions(junctions);
					bb.setRoadIds(ridCopy);
					bb.setRoads(rCopy);
					bb.setAirports(airportsCopy);
					bb.setSeaports(seaportsCopy);
					bb.setRefuelStations(refuelStationsCopy);
					bb.setRepairStations(repairStationsCopy);
					bb.setVillages(villagesCopy);
					bb.setCities(citiesCopy);
					for (String dz : getDangerZones()) {
						bb.customAdd(ASSyntax.createLiteral("dangerZone", ASSyntax.createString(dz)));
					}
				} catch (CloneNotSupportedException e) {
					logger.severe(e.toString());
					e.printStackTrace();
				}

				long cloneLit = System.nanoTime();
				Iterator<Literal> all = iterator();
				while (all.hasNext()) {
					Literal b = all.next();
					Literal bCopy = b.copy();
					if (b.hasAnnot()) {
						for (Term annot : b.getAnnots()) {
							bCopy.addAnnot(annot.clone());
						}
					}
					bb.addForClone(bCopy);
				}
				long nanoTime = System.nanoTime();
				Analytics.getInstance().logTime(AnalyticsOperations.cloneBBLiterals, nanoTime - cloneLit);
				Analytics.getInstance().logTime(AnalyticsOperations.cloneBeliefBase, nanoTime - start);
				return bb;
			}
		} catch (ConcurrentModificationException e) {
			if (count < 4) {
				return this.geoCopy(bb, count + 1);
			} else {
				throw new RuntimeException(e);
			}
		}
	}

	//get/set methods intended for cloning...
	public Map<String, JunctionInf> getJunctions() 			{	return junctions;			}	
	public void setJunctions(Map<String, JunctionInf> j) 	{	this.junctions = j;			}
	public Map<String, RoadInf> getRoads() 					{return roads;					}
	public void setRoads(Map<String, RoadInf> r) 			{	this.roads = r;				}
	public void setRoadIds(Map<String, String> r) 			{	this.roadIds = r;			}
	public Collection<String> getCities() 					{	return cities;				}
	public void setCities(Collection<String> cities) 		{	this.cities = cities;		}
	public Collection<String> getVillages() 				{	return villages;			}
	public void setVillages(Collection<String> villages) 	{	this.villages = villages;	}
	public Collection<String> getAirports() 				{	return airports;			}
	public void setAirports(Collection<String> airports) 	{	this.airports = airports;	}
	public Collection<String> getSeaports() 				{	return seaports;			}
	public void setSeaports(Collection<String> seaports) 	{	this.seaports = seaports;	}
	public Collection<String> getRefuelStations() 			{	return refuelStations;		}
	public void setRefuelStations(Collection<String> rs) 	{	this.refuelStations = rs;	}
	public Collection<String> getRepairStations() 			{	return repairStations;		}
	public void setRepairStations(Collection<String> rs) 	{	this.repairStations = rs;	}	
	public Collection<String> getDangerZones() 				{	return dangerZones;			}

	public void customRemove(final Literal l) {
		synchronized (this) {
			logger.fine("Custom remove " + l + "(" + l.negated() + ")");

			final List<Term> terms = l.getTerms();
			// blocked
			if (l.getFunctor().equals(BLOCKED) & l.getTerms().size() == 2) {
				final String fr = parseTerm(terms.get(0));
				final String to = parseTerm(terms.get(1));
				final String id = roadIds.get(fr + UniversalConstants.ROAD_ID_SEPARATOR_CHAR + to);
				logger.finer("roadIDs get (" + (fr + UniversalConstants.ROAD_ID_SEPARATOR_CHAR + to) + ")= " + id
						+ ", roadIDs=" + roadIds + " roads=" + roads);
				if (roads.containsKey(id)) {
					roads.get(id).setBlocked(l.negated());
					return;
				}
			} else if (l.getFunctor().equals(BLOCKED_ROAD) & l.getTerms().size() == 1) {
				final String id = parseTerm(terms.get(0));
				if (roads.containsKey(id)) {
					roads.get(id).setBlocked(l.negated());
					return;
				}
			} else if (l.getFunctor().equals(TOXIC) & l.getTerms().size() == 2) {
				final String fr = parseTerm(terms.get(0));
				final String to = parseTerm(terms.get(1));
				final String id = roadIds.get(fr + UniversalConstants.ROAD_ID_SEPARATOR_CHAR + to);
				logger.finer("roadIDs get (" + (fr + UniversalConstants.ROAD_ID_SEPARATOR_CHAR + to) + ")= " + id
						+ ", roadIDs=" + roadIds + " roads=" + roads);
				if (roads.containsKey(id)) {
					roads.get(id).setToxic(l.negated());
					return;
				}
			} else if (l.getFunctor().equals(TOXIC_ROAD) & l.getTerms().size() == 1) {
				final String id = parseTerm(terms.get(0));
				if (roads.containsKey(id)) {
					roads.get(id).setToxic(l.negated());
					return;
				}
			} else if (l.getFunctor().equals(DANGERZONE)) {
				final String loc = parseTerm(terms.get(0));
				if (dangerZones.contains(loc)) {
					dangerZones.remove(loc);
				}

				if (junctions.containsKey(loc)) {
					junctions.get(loc).setDangerous(false);
				}

				for (RoadInf r : roads.values()) {
					if (r.getEndIds()[0].equals(loc)) {
						r.getEnds()[0].setDangerous(false);
					}
					if (r.getEndIds()[1].equals(loc)) {
						r.getEnds()[1].setDangerous(false);
					}
				}
			}
		}
	}
	
	@Override
    public Iterator<Literal> getCandidateBeliefs(Literal l, Unifier u) {
		synchronized(this) {
			return super.getCandidateBeliefs(l, u);
		}
	}

	public String getId() {	return logger.getName();	}
}