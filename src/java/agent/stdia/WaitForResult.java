package agent.stdia;

import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.Message;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.Literal;
import jason.asSyntax.Term;

import java.util.Queue;
import java.util.logging.Logger;

import common.UniversalConstants;

/**
 * 
 * @author Alan
 * @deprecated no longer used
 */
public class WaitForResult extends DefaultInternalAction {
	private static final long serialVersionUID = 3180496923643996760L;

	public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
		Logger logger = Logger.getLogger(ts.getUserAgArch().getAgName() + ":waitForResult");
		logger.setLevel(UniversalConstants.INTERNAL_ACTION_LOGGER_LEVEL);

		logger.finer("Begin, args = " + args);

		assert args.length==1: "Wrong args length! " + args.length;

		//get the action we are looking for
		Literal action = (Literal)args[0];
		String dependant = action.getAnnots("dependant").toString().replaceAll("\"", "");
		logger.info("Literal to look for = " + action);

		/*
		 * Note; this checks the mailbox but does not remove messages; it is expected selectMessage will
		 * do so.  As we are explicitly halting the agent, we do not expect this to be a problem wrt
		 * to timing. 
		 */
 		mailChecking:
			while(true)	{
				ts.getUserAgArch().getArchInfraTier().checkMail();
				Queue<Message> mail = ts.getC().getMailBox();
				logger.finer("Checking mail = " + mail.size() + " messages");

				if(!mail.isEmpty()){
					for(Message current: mail){
						logger.finer("Message = " + current.toString());
						//check body is == action
						if(current.getPropCont() instanceof Literal)	{
							Literal cont = (Literal)current.getPropCont();
							if(cont.equals(action))	{
								if(current.getIlForce().equals("succeed"))	{
									logger.fine("Received result! " + current.toString());
									current.setReceiver(dependant);
									ts.getUserAgArch().sendMsg(current); //inform dependant
									return true;
								}
								else if(current.getIlForce().equals("failed"))	{
									logger.fine("Received result! " + current.toString());
									current.setReceiver(dependant);
									ts.getUserAgArch().sendMsg(current); //inform dependant
									return true;
								}

								//do we have the desired message?
								break mailChecking;
							}
						}
						//else - ignore and move on!
					}
				}
				else	{	Thread.sleep(1);	}
			}

		return false; //should never hit this; should exit with return true in loop
	}
}
