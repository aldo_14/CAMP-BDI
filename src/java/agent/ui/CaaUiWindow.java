package agent.ui;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import agent.model.capability.Capability;
import agent.model.capability.CompositeCapability;
import agent.model.capability.ExternalCapability;
import agent.model.capability.PrimitiveCapability;
import agent.model.dependency.Contract;
import agent.type.ContractFormerAgent;
import agent.type.MaintainingAgent;
import jason.asSemantics.ActionExec;
import jason.asSemantics.IntendedMeans;
import jason.asSemantics.Intention;
import jason.asSyntax.Literal;


public class CaaUiWindow extends JFrame implements WindowListener {

	/**
	 * Used for cascading purposes
	 */
	public static int WINCOUNT = -1;
	
	private static final long serialVersionUID = -5460745741163568596L;

	private Logger logger;
	
	//display objects... using tables
	//current intention set
	private JLabel ciTitle = new JLabel("Current intentions");
	private JTable currentIntentions;
	private String[] ciCols = new String[]	{"ID", "Current task", "Suspended", "Trigger"};

	//dependencies (active)
	private JLabel adTitle = new JLabel("Currently executing dependencies");
	private JTable activeDependencies;
	private String[] adCols = new String[]	{"ID", "Task Literal", "Waiting for", "Result"};

	//waitlist for dependencies
	private JLabel wdTitle = new JLabel("Waitlist for dependency formation");
	private JTable waitListDependencies;
	private String[] wlCols = new String[]	{"Task", "Waiting on..."};

	//dependencies (contracts)
	private JLabel dcTitle = new JLabel("Formed dependency contracts");
	private JTable dependencyContracts;
	private String[] dcCols = new String[]	{"Task", "Obligants", "Confidence"};

	//obligations (active)
	private JLabel wrTitle = new JLabel("Obligations: waiting on primary completion");
	private JTable waitRoleObligations;
	private String[] wroCols = new String[]	{"Joint Activity"};

	//obligations (contracts)
	private JLabel ocTitle = new JLabel("Obligation contracts held");
	private JTable obligationContracts;
	private String[] ocCols = new String[]	{"Task", "Dependant"};

	private ContractFormerAgent a;

	public CaaUiWindow(final ContractFormerAgent a)	{
		logger = a.getLogger();
		this.a = a;
		if(a instanceof MaintainingAgent)	{
			Vector<String> info = new Vector<String>();
			if(((MaintainingAgent)a).isReplanning())	{	info.add("Replanning");	}
			if(((MaintainingAgent)a).isMaintaining())	{	info.add("Maintaining");}
			if(!info.isEmpty())	{
				setTitle(a.getId() + ": " + getClass().getSimpleName() + " " + info);
			}
			else	{	setTitle(a.getId() + ": " + getClass().getSimpleName());	}
		}
		else		{	setTitle(a.getId() + ":" + getClass().getSimpleName());		}
		this.addWindowListener(this);
		this.setSize(500, 500);
		this.setLocation((++WINCOUNT * 100), (++WINCOUNT * 20));

		//to sort out tiling of many many screens
		if((WINCOUNT * 100 > 1600)	||	(WINCOUNT * 20 > 900))	{	WINCOUNT = 0;	}
		
		
		//jmenu stuff
		JMenuBar menubar = new JMenuBar();
		JMenu menu = new JMenu("Agent info");
		JMenuItem shoCir = new JMenuItem("Show Agent Circumstance");
		shoCir.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent a) {	showCircumstance();	}
		});
		JMenuItem shoCap = new JMenuItem("Show Agent Capabilities");
		shoCap.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent a) {	showCapabilities();	}
		});
		JMenuItem dumpBB = new JMenuItem("Dump BB to log");
		dumpBB.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent a) {	dumpBB();	}
		});
		JMenuItem dumpCap = new JMenuItem("Dump Capabilities to log");
		dumpCap.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent a) {	dumpCap();	}
		});
		menu.add(shoCir);
		menu.add(shoCap);
		menu.add(dumpBB);
		menu.add(dumpCap);
		menubar.add(menu);
		setJMenuBar(menubar);

		//panel for each table/label combo - need to set in flowing layout
		Container cp = this.getContentPane();

		//create tables
		currentIntentions 	= new JTable(1, ciCols.length);
		activeDependencies 	= new JTable(1, adCols.length);
		waitListDependencies= new JTable(1, wlCols.length);
		dependencyContracts = new JTable(1, dcCols.length);
		waitRoleObligations = new JTable(1, wroCols.length);
		obligationContracts = new JTable(1, ocCols.length);

		//set data
		activeDependencies.setModel(
				new DefaultTableModel(this.getActiveDependenciesData(), adCols));
		currentIntentions.setModel(
				new DefaultTableModel(this.getCurrentIntentionData(), ciCols));
		dependencyContracts.setModel(
				new DefaultTableModel(this.getDependencyContractData(), dcCols));
		obligationContracts.setModel(
				new DefaultTableModel(this.getObligationContractData(), ocCols));
		waitRoleObligations.setModel(
				new DefaultTableModel(this.getSecondaryRoleObligationsData(), wroCols));
		waitListDependencies.setModel(
				new DefaultTableModel(this.getDependencyWaitList(), wlCols));

		//set size
		setElementPrefSize();

		//add tables to scrollpane
		JScrollPane ciScroller = new JScrollPane(currentIntentions);
		currentIntentions.setFillsViewportHeight(true);

		JScrollPane adScroller = new JScrollPane(activeDependencies);
		activeDependencies.setFillsViewportHeight(true);

		JScrollPane wlScroller = new JScrollPane(waitListDependencies);
		waitListDependencies.setFillsViewportHeight(true);

		JScrollPane dcScroller = new JScrollPane(dependencyContracts);
		dependencyContracts.setFillsViewportHeight(true);

		JScrollPane wroScroller = new JScrollPane(waitRoleObligations);
		waitRoleObligations.setFillsViewportHeight(true);

		JScrollPane ocScroller = new JScrollPane(obligationContracts);
		obligationContracts.setFillsViewportHeight(true);

		//add everything to content pane
		cp.setLayout(new BoxLayout(cp, BoxLayout.Y_AXIS));
		cp.add(ciTitle);
		cp.add(ciScroller);
		cp.add(adTitle);
		cp.add(adScroller);
		cp.add(wdTitle);
		cp.add(wlScroller);
		cp.add(dcTitle);
		cp.add(dcScroller);
		cp.add(wrTitle);
		cp.add(wroScroller);
		cp.add(ocTitle);
		cp.add(ocScroller);

		setContentPane(cp);

		setVisible(true);
		//setup constant refresh while visible...
		new Thread(new Runnable(){
			@Override
			public void run() {
				while(isVisible())	{
					try {	Thread.sleep(20);} //ms
					catch (InterruptedException e) 	{
						logger.severe(e.toString());
						for(StackTraceElement s: e.getStackTrace()){logger.severe(s.toString());}
					}
					repaint();
				}
			} //end run method
		}).start();
	}

	protected void dumpCap() {
		Collection<PrimitiveCapability> pSet = a.getPrimitiveCapabilities().values();
		String ps = "";
		for(PrimitiveCapability c:pSet)	{	ps = ps + "\n" + c.toString();	}
		
		Collection<CompositeCapability> cSet = a.getCompositeCapabilities().values();
		String cs = "";
		for(CompositeCapability c:cSet)	{	cs = cs + "\n" + c.toString();	}
		
		Collection<ExternalCapability> ecSet = a.getExtCapabilities();
		String ecs = "";
		for(ExternalCapability c:ecSet)	{	ecs = ecs + "\n" + c.toString();	}
		
		String cap = "Capabilities for " + a.getId() + "\nPrimitive;" + ps + "\nComposite;" + cs + "\nExternal;" + ecs;
		Logger logger = Logger.getLogger("CapDump:"+a.getId());
		logger.setLevel(Level.SEVERE);
		logger.severe(cap);
		
		JOptionPane.showMessageDialog(
				this, new String[]{"Capability info written to log"}, 
				a.getId() + " Caps dumped", 
				JOptionPane.INFORMATION_MESSAGE);
	}

	protected void dumpBB() {
		Logger logger = Logger.getLogger("BBDump:"+a.getId());
		logger.setLevel(Level.INFO);
		Iterator<Literal> beliefs = a.getBB().iterator();
		String t = 	"==============BBDUMP==============";
		while(beliefs.hasNext())	{	t = t + "\n\t" + beliefs.next();	}
		t = t +	"\n==================================";
		logger.info(t);

		JOptionPane.showMessageDialog(
				this, new String[]{"BeliefBase contents written to log"}, 
				a.getId() + " BB dumped", 
				JOptionPane.INFORMATION_MESSAGE);
	}

	/**
	 * Create circumstance info dialog
	 */
	protected void showCircumstance() {
		String circ = a.getTS().getC().toString();
		JOptionPane.showMessageDialog(
				this, circ, a.getId() + " circumstance", JOptionPane.INFORMATION_MESSAGE);
	}


	/**
	 * Show internal and external capabilities in a simple dialog box
	 */
	protected void showCapabilities() {
		List<String> capabilities = new Vector<String>();
		capabilities.add("Primitive (internal);");
		for(String cap: a.getPrimitiveCapabilities().keySet())	{
			capabilities.add(cap + ":" + a.getPrimitiveCapabilities().get(cap));
		}
		capabilities.add("Composite (internal);");
		for(Capability cap: a.getCompositeCapabilities().values())	{
			capabilities.add("" + cap);
		}
		capabilities.add("Externally held;");
		for(ExternalCapability cap: a.getExtCapabilities())	{
			capabilities.add("" + cap);
		}
		JOptionPane.showMessageDialog(this, capabilities.toArray(),
				a.getId() + " capabilities", JOptionPane.INFORMATION_MESSAGE);
	}
	
	/**
	 * Repaint; updates the data contents of objects on display
	 */
	@Override
	public void repaint()	{
		try	{
			//set pref size in case ui window changed size
			setElementPrefSize();
			updateTableContents(activeDependencies, getActiveDependenciesData());
			updateTableContents(currentIntentions, getCurrentIntentionData());
			updateTableContents(dependencyContracts, getDependencyContractData());
			updateTableContents(obligationContracts, getObligationContractData());
			updateTableContents(waitRoleObligations, getSecondaryRoleObligationsData());
			updateTableContents(waitListDependencies, getDependencyWaitList());
		}
		catch(Exception e)	{
			//	logger.severe("Repaint error " + e.toString());
			//	for(StackTraceElement s: e.getStackTrace())	{	logger.severe(s.toString());	}
		}
		super.repaint();
	}

	/**
	 * Update table contents
	 * @param table to edit
	 * @param data[row][col]
	 */
	private void updateTableContents(JTable table,	String[][] data) {
		DefaultTableModel model = ((DefaultTableModel)table.getModel());
		if(model.getRowCount()>0)	{
			for(int i=(model.getRowCount()-1); i>=0; i--)	{	
				if(i>=0)	{	model.removeRow(i);		}
			}
		}
		for(int i=0; i<data.length; i++)	{	model.addRow(data[i]);	}
	}


	private void setElementPrefSize()	{
		ciTitle.setPreferredSize(new Dimension(getWidth(), 20));
		currentIntentions.setPreferredSize(new Dimension(getWidth(), 40));
		adTitle.setPreferredSize(new Dimension(getWidth(), 20));
		activeDependencies.setPreferredSize(new Dimension(getWidth(), 40));
		wdTitle.setPreferredSize(new Dimension(getWidth(), 20));
		waitListDependencies.setPreferredSize(new Dimension(getWidth(), 40));
		dcTitle.setPreferredSize(new Dimension(getWidth(), 20));
		dependencyContracts.setPreferredSize(new Dimension(getWidth(), 100));
		wrTitle.setPreferredSize(new Dimension(getWidth(), 20));
		waitRoleObligations.setPreferredSize(new Dimension(getWidth(), 40));
		ocTitle.setPreferredSize(new Dimension(getWidth(), 20));
		obligationContracts.setPreferredSize(new Dimension(getWidth(), 100));
	}

	private String[][] getCurrentIntentionData() {
		try {
			//"ID", "Current task", "Suspended"
			LinkedList<Intention> is = new LinkedList<Intention>();
			Object[] io = a.getTS().getC().getIntentions().toArray();
			
			for(Object o: io)	{
				is.add(((Intention)o).clone());
			}
			
			io = a.getTS().getC().getPendingIntentions().values().toArray();
			
			for(Object o: io)	{
				is.add(((Intention)o).clone());
			}
			
			io = a.getTS().getC().getPendingActions().values().toArray();
			
			for(Object o:io)	{
				ActionExec a = (ActionExec)o;
				if(!is.contains(a.getIntention()))	{	is.add(a.getIntention().clone());	}
			}
			
			if(!is.isEmpty())	{
				String[][] data = new String[is.size()][ciCols.length];
				for(int i=0; i<is.size(); i++){
					Intention curr = is.get(i).clone();
					if(curr!=null & curr.peek()!=null)	{
						Literal top  = curr.peek().getTrigger().clone(); //avoid shallow ref
						top.apply(curr.peek().getUnif());
						data[i][0] = Integer.toString(curr.getId()) +":";

						if(curr.peek()!=null && curr.peek().getCurrentStep()!=null)	{	
							Literal task = (Literal) curr.peek().getCurrentStep().getBodyTerm().clone();
							task.apply(curr.peek().getUnif());
							data[i][0] = data[i][0] + curr.peek().getTrigger().toString();
							data[i][1] = task.toString();	
							if(	curr.peek().getCurrentStep().getBodyTerm().isLiteral() &&
									a.hasObligationContract(task))	{
								data[i][1] = "O:" + data[i][1];
							}
						}	
						else	{ 	data[i][1] = "N/A";		}

						//intention booleans
						data[i][2] = Boolean.toString(curr.isSuspended());
						
						//get parent trigger
						Iterator<IntendedMeans> ims = curr.iterator();
						while(ims.hasNext())	{
							data[i][3] = ims.next().getTrigger().toString();
						}
					}
					else	{
						data[i][0] = "Null id";
						data[i][1] = "Null term";
						data[i][2] = "--";
						data[i][3] = "--";
					}
				}
				return data;
			}
			else	{
				String[][] data = new String[1][ciCols.length];
				data[0][0] = "-";
				data[0][1] = "-";
				data[0][2] = "-";
				data[0][3] = "-";
				return data;
			}
		} catch (Exception e) {
			return new String[0][0];
		}
	}

	private String[][] getActiveDependenciesData() {
		//"ID", "Task Literal", "Waiting for", "Result"
		if(!a.getWaitingList().isEmpty())	{
			String[][] data = new String[a.getWaitingList().keySet().size()][adCols.length];
			int i = 0;
			Object[] wl = a.getWaitingList().keySet().toArray();
			for(Object o:wl){
				Integer id = (Integer)o;
				ActionExec ae = a.getWaitingList().get(id);
				data[i][0] = id.toString();
				data[i][1] = ""+ae.getActionTerm();
				data[i][2] = ""+a.getWaitingForReply().get(ae);
				if(a.getWaitResults().containsKey(id))	{
					data[i][3] =a.getWaitResults().get(ae).toString();
				}
				else	{
					data[i][3] = "-";
				}
				i++;
			}
			return data;
		}
		else	{
			String[][] data = new String[1][adCols.length];
			data[0][0] = "-";
			data[0][1] = "-";
			data[0][2] = "-";
			data[0][3] = "-";
			return data;
		}
	}

	private String[][] getDependencyContractData() {
		//"Task", "Obligants"
		if(a.getDependencies().isEmpty())	{
			String[][] rt = new String[1][dcCols.length];
			rt[0][0] = "-";
			rt[0][1] = "-";
			rt[0][2] = "-";
			return rt;	
		}
		else	{
			String[][] rt = new String[a.getDependencies().size()][dcCols.length];
			int i=0;
			Object[] deps = a.getDependencies().toArray();
			for(Object o: deps)	{
				Literal cl = (Literal)o;
				//this can occur if the dep. contract is called during update...
				Contract c = a.getDependencyContract(cl);
				if(c==null)	{
					logger.severe("no contract found for " + cl + " held by " + a.getId());
					rt[i][0] = cl.getFunctor() + " " + cl.getTerms();
					rt[i][1] = "Null contract...";
					rt[i][2] = "...";
				}
				else	{
					rt[i][0] = c.getActivity().getFunctor() + " " + c.getActivity().getTerms();
					rt[i][1] = a.getInvolvedAgents(c.getActivity()).toString();
					rt[i][2] = "" + c.getExternalCapability().getGeneralConfidence(a.getBB());
				}
				
				i++;
			}
			return rt;
		}
	}

	/**
	 * List of forming dependencies
	 * @return
	 */
	private String[][] getDependencyWaitList() {
		//"Obligation", "Waiting on..."
		if(a.contractWaitList().isEmpty())	{
			String[][] data = new String[1][wlCols.length];
			data[0][0] = "-";
			data[0][1] = "-";
			return data;
		}
		else	{
			String[][] data = new String[a.contractWaitList().keySet().size()][wlCols.length];
			int i=0;
			Object[] o = a.contractWaitList().keySet().toArray();
			for(Object ol:o)	{
				Literal l = (Literal)ol;
				data[i][0] = l.toString();
				data[i][1] = a.actionWaitList(l).toString();
				i++;
			}
			return data;
		}
	}

	private String[][] getSecondaryRoleObligationsData() {
		//task
		if(!a.getWaitOns().isEmpty())	{
			Object[] wo = a.getWaitOns().toArray();
			String[][] data = new String[a.getWaitOns().size()][wroCols.length];
			int i=0;
			for(Object o: wo)	{
				Literal l = (Literal)o;
				data[i][0] = l.toString();
				i++;
			}
			return data;
		}
		else	{
			String[][] data = new String[1][wroCols.length];
			data[0][0] = "-";
			return data;
		}
	}

	private String[][] getObligationContractData() {
		//Task", "Dependant"
		if(a.getObligations().isEmpty())	{
			String[][] rt = new String[1][ocCols.length];
			rt[0][0] = "-";
			rt[0][1] = "-";
			return rt;
		}
		else	{
			Object[] wo = a.getObligations().toArray();
			String[][] rt = new String[a.getObligations().size()][ocCols.length];
			int i = 0;
			for(Object o: wo)	{
				Literal ob = (Literal)o;
				Contract c = a.getObligationContract(ob);
				if(c!=null)	{
				rt[i][0] = c.getActivity().getFunctor() + " " + c.getActivity().getTerms(); //task
				rt[i][1] = 	c.getDependantTrace().get(c.getDependantTrace().size()-1) + ":int=" + 
						c.getIntentionTrace().get(c.getIntentionTrace().size()-1); //dependant
				i++;
				}
				else	{
					rt[i][0] = ob.getFunctor() + " " + ob.getTerms();
					rt[i][1] = "no contract";
				}
			}
			return rt;
		}
	}

	@Override
	public void windowActivated(WindowEvent arg0) {}
	@Override
	public void windowClosed(WindowEvent arg0) {
		this.setVisible(false);
		this.dispose();
	}
	@Override
	public void windowClosing(WindowEvent arg0) {}
	@Override
	public void windowDeactivated(WindowEvent arg0) {}
	@Override
	public void windowDeiconified(WindowEvent arg0) {}
	@Override
	public void windowIconified(WindowEvent arg0) {}
	@Override
	public void windowOpened(WindowEvent arg0) {}
}
