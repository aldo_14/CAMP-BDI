package vehicle;

import truckworld.vehicle.Vehicle;
import truckworld.vehicle.type.Bulldozer;

public class BulldozerAgent extends TruckAgent {
	@Override
	public Vehicle getVehicleObject() {	
		final Bulldozer bd = new Bulldozer(getId());
		bd.setHealth(getHealth());
		return bd;
	}
}
