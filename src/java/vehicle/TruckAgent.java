package vehicle;

import truckworld.vehicle.Vehicle;
import truckworld.vehicle.type.Truck;

public class TruckAgent extends VehicleAgent {
	@Override
	public Vehicle getVehicleObject() {	
		final Truck truck = new Truck(getId());
		truck.setHealth(getHealth());
		return truck;
	}
}
