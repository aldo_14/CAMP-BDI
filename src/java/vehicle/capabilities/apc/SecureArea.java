package vehicle.capabilities.apc;

import jason.asSyntax.Literal;
import jason.bb.BeliefBase;
import vehicle.VehicleAgent;
import vehicle.capabilities.VehicleCapability;
import agent.model.capability.ConfidencePrecondition;
import common.UniversalConstants.WorldAction;

/**
 * APC secures the identified junction, removing a danger zone
 */
public class SecureArea extends VehicleCapability {

	private static String[] defPre = new String[]{
		"location(O) & apc(AGENT) & atJ(AGENT, O) & dangerZone(O) & not mortal(AGENT)"};
	private static String[] defEff = new String[]{"-dangerZone(O)"}; 
	//NB possibility of damage; not represented in ca, but in env code
	private final ConfidencePrecondition[] confPre;

	public SecureArea(VehicleAgent holder) {	
		super(	holder, 
				holder.getTaskFactory().factory(WorldAction.secureArea.name()), //goal
				Literal.parseLiteral("secureArea(AGENT, O)"), //signature - agent, location
				defPre, //preconditions
				defEff);//effects	
		confPre = new ConfidencePrecondition[]{
			new ConfidencePrecondition(holder.getId(), sigLit, "location(O) & apc(AGENT) & atJ(AGENT, O) & dangerZone(O) & healthy(AGENT)", 1),
			new ConfidencePrecondition(holder.getId(), sigLit, "location(O) & apc(AGENT) & atJ(AGENT, O) & dangerZone(O) & damaged(AGENT)", 0.8f)
		};
	}

	@Override
	public float getSpecificConfidence(BeliefBase bb, Literal action) {
		return getGeneralConfidence(bb);
	}

	public String[] getPreconditionsByConfConstraint(float minConf) {
		if(minConf > DAMAGE_AGENT_MODIFIER)	{
			return new String[]{ 	defPre[0].replaceAll("not mortal", "healthy")	};
		}
		else	{	return defPre;	}
	}

	@Override
	public ConfidencePrecondition[] getConfidenceBasedPreconditions(BeliefBase context) {
		return confPre;
	}

}
