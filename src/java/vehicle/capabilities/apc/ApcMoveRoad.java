/**
 * 
 */
package vehicle.capabilities.apc;

import java.util.List;

import agent.beliefBase.WorldBeliefBase;
import agent.model.capability.ConfidencePrecondition;
import common.UniversalConstants.WorldAction;
import jason.asSyntax.Literal;
import jason.asSyntax.StringTerm;
import jason.asSyntax.Term;
import jason.bb.BeliefBase;
import truckworld.world.interfaces.RoadInf;
import vehicle.VehicleAgent;
import vehicle.capabilities.truck.MoveRoad;

/**
 * @author Alan White
 * 
 * Task description for a capability to move from one position to another, with the start being
 * either a road or junction on the map
 * 
 * Extends moveRoad - APCs aren't bothered by flooding or toxic environments.
 */
public class ApcMoveRoad extends MoveRoad {

	/**
	 * NB: this is a bit overcomplex because we support movement starting from either a junction or on a road in this code...
	 * Signature is move(Agent, A, b)
	 * Preconditions; 
	 * A is a road OR at junction (first arg)
	 * B is a junction
	 * types are correct for agent, road, junction (Vehicle, connection, location types)
	 * road connects to B
	 * agent is !mortal (health state=(healthy|damaged))
	 * road is !blocked 
	 * road is !flooded; APCs can use slippery mud, unlike other types
	 */
	private final static String[] defPre = {	
			"apc(AGENT) & connection(RID) & location(J) & location(O) & road(RID, O, J) & not mortal(AGENT) & not blocked(O, J) & not flooded(O, J) & not stuck(AGENT) & onR(AGENT, RID)",	
			"apc(AGENT) & connection(RID) & location(J) & location(O) & road(RID, O, J) & not mortal(AGENT) & not blocked(O, J) & not flooded(O, J) & not stuck(AGENT) & atJ(AGENT, O) & not dangerZone(O)"	};

	/*
	 * Effects; agent is at B, is not on the road at O or at the junction O (can remove both regardless of start case)
	 */
	private final static String[]  defEff = {"-onR(AGENT, RID)", "-atJ(AGENT, O)", "+atJ(AGENT, J)"};
	
	private final ConfidencePrecondition[] confPre;
	
	/**
	 * Default constructor
	 * @param holder
	 */
	public ApcMoveRoad(VehicleAgent holder) {	
		super(	holder, 
				holder.getTaskFactory().factory(WorldAction.move.name()), //goal
				Literal.parseLiteral("move(AGENT, RID, O, J)"), //signature - agent, road, origin, destination
				defPre, //preconditions
				defEff);//effects	
		confPre = new ConfidencePrecondition[] {
				//healthy, not slippery, dry = 1
				new ConfidencePrecondition(holder.getId(), sigLit, "apc(AGENT) & connection(RID) & location(J) & location(O) & road(RID, O, J) & healthy(AGENT) &"
						+ " not blocked(O, J) & dry(O, J) & not stuck(AGENT) & onR(AGENT, RID)", 1),
				new ConfidencePrecondition(holder.getId(), sigLit,"apc(AGENT) & connection(RID) & location(J) & location(O) & road(RID, O, J) & healthy(AGENT) &"
						+ " not blocked(O, J) & dry(O, J) & not stuck(AGENT) & atJ(AGENT, O) & not dangerZone(O)", 1),
				//healthy, slippery tarmac = 0.8
				new ConfidencePrecondition(holder.getId(), sigLit,"apc(AGENT) & connection(RID) & location(J) & location(O) & road(RID, O, J) & healthy(AGENT) &"
					+ " not blocked(O, J) & slippery(O, J) & not stuck(AGENT) & onR(AGENT, RID)", SLIPPERY_MODIFIER),
				new ConfidencePrecondition(holder.getId(), sigLit, "apc(AGENT) & connection(RID) & location(J) & location(O) & road(RID, O, J) & healthy(AGENT) &"
					+ " not blocked(O, J) & slippery(O, J) & not stuck(AGENT) & atJ(AGENT, O) & not dangerZone(O)", SLIPPERY_MODIFIER),
				//damaged, not slippery, not flooded = 0.8
				new ConfidencePrecondition(holder.getId(), sigLit, "apc(AGENT) & connection(RID) & location(J) & location(O) & road(RID, O, J) & damaged(AGENT) &"
					+ " not blocked(O, J) & dry(O, J) & not stuck(AGENT) & onR(AGENT, RID)", DAMAGE_AGENT_MODIFIER),
				new ConfidencePrecondition(holder.getId(), sigLit, "apc(AGENT) & connection(RID) & location(J) & location(O) & road(RID, O, J) & damaged(AGENT) &"
					+ " not blocked(O, J) & dry(O, J) & not stuck(AGENT) & atJ(AGENT, O) & not dangerZone(O)", DAMAGE_AGENT_MODIFIER),			
				//damaged, slippery tarmac = 0.8*0.8
				new ConfidencePrecondition(holder.getId(), sigLit, "apc(AGENT) & connection(RID) & location(J) & location(O) & road(RID, O, J) & damaged(AGENT) &"
					+ " not blocked(O, J) & slippery(O, J) & not stuck(AGENT) & onR(AGENT, RID)", (SLIPPERY_MODIFIER*DAMAGE_AGENT_MODIFIER)),
				new ConfidencePrecondition(holder.getId(), sigLit, "apc(AGENT) & connection(RID) & location(J) & location(O) & road(RID, O, J) & damaged(AGENT) &"
					+ " not blocked(O, J) & slippery(O, J) & not stuck(AGENT) & atJ(AGENT, O) & not dangerZone(O)", (SLIPPERY_MODIFIER*DAMAGE_AGENT_MODIFIER)),
		};
	}

	/**
	 * Extends to consider slippy roads if confValue > 0.8*0.8...
	 */
	@Override
	public String[] getPreconditionsByConfConstraint(float confValue) {
		float slipDamConf = 1*(DAMAGE_AGENT_MODIFIER * MoveRoad.SLIPPERY_MODIFIER);

		//damaged or slippery is bad
		if(confValue <= MoveRoad.SLIPPERY_MODIFIER && confValue <= DAMAGE_AGENT_MODIFIER && confValue > slipDamConf )	{
			return new String[]{
					defPre[0].replaceAll("not mortal", "healthy"), //not mortal
					defPre[1].replaceAll("not mortal", "healthy"),
					defPre[0].replaceAll("not flooded", "dry"), //not damaged
					defPre[1].replaceAll("not flooded", "dry")
			};
		}

		//damaged and slippery is bad, damaged OR slippery is ok
		if(confValue > MoveRoad.SLIPPERY_MODIFIER || confValue > DAMAGE_AGENT_MODIFIER)	{
			return new String[]{
					defPre[0].replaceAll("not mortal", "healthy").replaceAll("not flooded", "dry"), 
					defPre[1].replaceAll("not mortal", "healthy").replaceAll("not flooded", "dry")	
			};
		}
		//implicit else
		return defPre;
	}


	@Override
	public float getSpecificConfidence(BeliefBase bb, Literal action) {
		//move (0=agent, 1=road, 2=a,3=b) ; ROAD: a->b
		List<Term> args = action.getTerms();
		String from	= ((StringTerm)args.get(2)).getString();
		String to	= ((StringTerm)args.get(3)).getString();
		
		WorldBeliefBase wbb = (WorldBeliefBase)bb;
		
		if(wbb.getRoads().isEmpty())	{
			logger.severe("No Roads!; " + wbb.getRoads().keySet());
			return 0;
		}
		else	{
			logger.info("Roads: " + wbb.getRoads().keySet());
		}
		RoadInf r = wbb.getRoad(from, to);
		
		if(r==null){
			//road does not exist!
			logger.warning(	"Confidence function cannot determine road being used to travel; " +
							"from=" + from +", to=" + to);
			logger.warning("Road list in BB: " + wbb.getRoads().keySet().toString());
			return 0f;
		}
		
		//need to handle specific road types when judging confidence...?
		//include rainfall knowledge for slippery tarmac / dry mud?
		float base = getGeneralConfidence(bb);
		
		logger.info("Estimation general conf=" + base);
		if(r.isBlocked())		{
			logger.warning(r.getId() + " is blocked (" + r.isBlocked() + ")/flooded(" + r.isFlooded() + 
					"), return 0 confidence");
			return 0f;
		}
		else if(r.isFlooded())	return base*0f;	
		else if(r.isSlippery())	return base*MoveRoad.SLIPPERY_MODIFIER; //modulate with weather knowledge?
		else					return base;
	}
	
	@Override
	public ConfidencePrecondition[] getConfidenceBasedPreconditions(BeliefBase context) {
		return confPre;
	}

}
