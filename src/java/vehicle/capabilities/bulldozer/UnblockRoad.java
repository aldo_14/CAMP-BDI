/**
 * 
 */
package vehicle.capabilities.bulldozer;

import static vehicle.capabilities.truck.MoveRoad.SLIPPERY_MODIFIER;

import agent.beliefBase.WorldBeliefBase;
import agent.model.capability.ConfidencePrecondition;
import common.UniversalConstants.RoadType;
import common.UniversalConstants.WorldAction;
import jason.asSyntax.Literal;
import jason.asSyntax.StringTerm;
import jason.bb.BeliefBase;
import truckworld.world.interfaces.RoadInf;
import vehicle.VehicleAgent;
import vehicle.capabilities.VehicleCapability;
import vehicle.capabilities.truck.MoveRoad;

/**
 * @author Alan White </br>
 *         Task description for a capability to unblock a road from either end;
 *         agent starts at A of A:B, ends at B. Road must be blocked;
 *         restrictions on roads are such that flooded roads cannot be
 *         unblocked. There is no restriction on road surface type. </bR>
 *         </br>
 *         This is very similar to a 'move' capability in effect, except for the
 *         unblocked effect.
 */
public class UnblockRoad extends VehicleCapability {
	private static final String[] defPre = {
			"connection(RID) & location(J) & location(O) & road(RID, O, J) & not mortal(AGENT) & blocked(O, J) & not toxic(O,J) & not dangerZone(O) & "
			+ "not dangerZone(J) & not stuck(AGENT) & atJ(AGENT, O) & dry(O, J)",
			"connection(RID) & location(J) & location(O) & road(RID, O, J) & not mortal(AGENT) & blocked(O, J) & not toxic(O,J) & not dangerZone(O) & "
			+ "not dangerZone(J) & not stuck(AGENT) & atJ(AGENT, O) & tarmac(O, J) & slippery(O, J)" 
			};

	private static String[] defEff = { "-blocked(O, J)", "-blocked(J, O)", "-atJ(AGENT, O)", "+atJ(AGENT, J)" };

	private final ConfidencePrecondition[] confPre;

	public UnblockRoad(VehicleAgent holder) {
		super(holder, holder.getTaskFactory().factory(WorldAction.unblock.name()), 
				Literal.parseLiteral("unblock(AGENT, RID, O, J)"), defPre, defEff);
		confPre = new ConfidencePrecondition[] {
				new ConfidencePrecondition(holder.getId(), sigLit,
						"connection(RID) & location(J) & location(O) & road(RID, O, J) & healthy(AGENT) & blocked(O, J) & not toxic(O,J) "
						+ "& not dangerZone(O) & not dangerZone(J) & not stuck(AGENT) & atJ(AGENT, O) & dry(O, J)",
						1),
				// damage = 0.8
				new ConfidencePrecondition(holder.getId(), sigLit,
						"connection(RID) & location(J) & location(O) & road(RID, O, J) & damaged(AGENT) & blocked(O, J) & not toxic(O,J) "
						+ "& not dangerZone(O) & not dangerZone(J) & not stuck(AGENT) & atJ(AGENT, O) & dry(O, J)",
						DAMAGE_AGENT_MODIFIER),
				// slippery = 0.8
				new ConfidencePrecondition(holder.getId(), sigLit,
						"connection(RID) & location(J) & location(O) & road(RID, O, J) & healthy(AGENT) & blocked(O, J) & not toxic(O,J) "
						+ "& not dangerZone(O) & not dangerZone(J) & not stuck(AGENT) & tarmac(O,J) & atJ(AGENT, O) & slippery(O, J)",
						SLIPPERY_MODIFIER),

				// slippery AND damaged = 0.8*0.8
				new ConfidencePrecondition(holder.getId(), sigLit,
						"connection(RID) & location(J) & location(O) & road(RID, O, J) & damaged(AGENT) & blocked(O, J) & not toxic(O,J) "
						+ "& not dangerZone(O) & not dangerZone(J) & not stuck(AGENT) & tarmac(O,J) & atJ(AGENT, O) & slippery(O, J)",
						SLIPPERY_MODIFIER * DAMAGE_AGENT_MODIFIER) };
	}

	@Override
	public String[] getPreconditionsByConfConstraint(float confValue) {
		// slippery AND damaged
		float slipDamConf = 1 * (DAMAGE_AGENT_MODIFIER * SLIPPERY_MODIFIER);

		// damaged OR slippery is bad - must be dry AND healthy
		if (confValue > MoveRoad.SLIPPERY_MODIFIER || confValue > DAMAGE_AGENT_MODIFIER) {
			return new String[] {
			"connection(RID) & location(J) & location(O) & road(RID, O, J) & healthy(AGENT) & blocked(O, J) & not toxic(O,J) & not dangerZone(O) & "
			+ "not dangerZone(J) & not stuck(AGENT) & atJ(AGENT, O) & dry(O, J)"
			};
		}

		// both damaged and slippery is bad, damaged OR slippery is ok
		if (confValue <= MoveRoad.SLIPPERY_MODIFIER && confValue <= DAMAGE_AGENT_MODIFIER && confValue > slipDamConf) {
			return new String[] {
					"connection(RID) & location(J) & location(O) & road(RID, O, J) & not mortal(AGENT) & blocked(O, J) & not toxic(O,J) & not dangerZone(O) & "
					+ "not dangerZone(J) & not stuck(AGENT) & atJ(AGENT, O) & dry(O, J)",
					"connection(RID) & location(J) & location(O) & road(RID, O, J) & healthy(AGENT) & blocked(O, J) & not toxic(O,J) & not dangerZone(O) & "
					+ "not dangerZone(J) & not stuck(AGENT) & atJ(AGENT, O) & tarmac(O, J) & slippery(O, J)" 
			};
		}
		return defPre;
	}

	@Override
	public ConfidencePrecondition[] getConfidenceBasedPreconditions(final BeliefBase context) {
		return confPre;
	}
	
	@Override
	public float getSpecificConfidence(BeliefBase bb, Literal action) {
		//move (0=agent, 1=road, 2=a,3=b) ; ROAD: a->b
		final String from = ((StringTerm) action.getTerms().get(2)).getString();
		final String to	= ((StringTerm) action.getTerms().get(3)).getString();
		
		final WorldBeliefBase wbb = (WorldBeliefBase)bb;
		//from is either a junction or road id..
		final RoadInf r = (wbb.getRoads().containsKey(from))? wbb.getRoad(from): wbb.getRoad(from, to);	
		
		if(r==null){
			logger.warning(	"Confidence function cannot determine road being used to travel; " +
							"from=" + from +", to=" + to + "\nRoad list in BB: " + wbb.getRoads().keySet().toString());
			return 0f;
		}
		if(!r.isBlocked()) {
			return 1f; 
		}
		float base = getGeneralConfidence(bb);
		logger.finer("Estimation general conf=" + base);
		if(r.isFlooded() || r.isToxic() )		{
			logger.warning(r.getId() + " bad return 0 confidence");
			return 0f;
		}
		else if(r.isSlippery())	{	
			return r.getRoadType().equals(RoadType.tarmac) ? (base*SLIPPERY_MODIFIER) : 0f; 
		}
		else	{	
			return base;	
		}
	}
}
