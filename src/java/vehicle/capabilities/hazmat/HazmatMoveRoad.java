/**
 * 
 */
package vehicle.capabilities.hazmat;

import jason.asSyntax.Literal;
import jason.asSyntax.StringTerm;
import jason.asSyntax.Term;
import jason.bb.BeliefBase;

import java.util.List;

import truckworld.world.interfaces.RoadInf;
import vehicle.HazmatAgent;
import vehicle.capabilities.truck.MoveRoad;
import agent.beliefBase.WorldBeliefBase;
import agent.model.capability.ConfidencePrecondition;
import common.UniversalConstants.RoadType;
import common.UniversalConstants.WorldAction;

/**
 * @author Alan White
 * 
 * Task description for a capability to move from one position to another, with the start being
 * either a road or junction on the map
 */
public class HazmatMoveRoad extends MoveRoad {

	/**
	 * Note; we preclude movement to or from a danger-zone...
	 */
	private static String common = 
			"hazmat(AGENT) & connection(RID) & location(J) & location(O) & road(RID, O, J) & not mortal(AGENT) & " +
			"not blocked(O, J) & not dangerZone(O) & not dangerZone(J) & not stuck(AGENT)";
	/**
	 * NB: this is a bit overcomplex because we support movement starting from either a junction or on a road in this code...
	 * Signature is move(Agent, A, b)
	 * Preconditions; 
	 * A is a road OR at junction (first arg)
	 * B is a junction
	 * types are correct for agent, road, junction (Vehicle, connection, location types)
	 * road connects to B
	 * agent is !mortal (health state=(healthy|damaged))
	 * road is !blocked 
	 * road is !flooded OR road is dry & mud (can travel on slippery tarmac, or any dry road)
	 * agent is !stuck
	 */
	private static String[] defPre = {
		common + " & onR(AGENT, RID) & dry(O, J)",
		common + " & onR(AGENT, RID) & tarmac(O, J) & slippery(O, J)",
		common + " & atJ(AGENT, O) & dry(O, J) & not toxic(O,J) ",
		common + " & atJ(AGENT, O) & tarmac(O, J) & slippery(O, J) & not toxic(O,J) "
		};

	/*
	 * Effects; agent is at B, is not on the road at O or at the junction O (can remove both regardless of start case)
	 */
	private static String[]  defEff = {"-onR(AGENT, RID)", "-atJ(AGENT, O)", "+atJ(AGENT, J)"};
	
	private final ConfidencePrecondition[] confPre;
	
	/**
	 * Default constructor
	 * @param holder
	 */
	public HazmatMoveRoad(HazmatAgent holder) {	
		super(	holder, 
				holder.getTaskFactory().factory(WorldAction.move.name()), //goal
				Literal.parseLiteral("move(AGENT, RID, O, J)"), //signature - agent, road, origin, destination
				defPre, //preconditions
				defEff);//effects	
		confPre = new ConfidencePrecondition[]{
				//dry, healthy = 1
				new ConfidencePrecondition(holder.getId(), sigLit,
						"hazmat(AGENT) & connection(RID) & location(J) & location(O) & road(RID, O, J) & healthy(AGENT) & " +
						"not blocked(O, J) & not dangerZone(O) & not dangerZone(J) & not stuck(AGENT) & onR(AGENT, RID) & dry(O, J)",
						1f),
				new ConfidencePrecondition(holder.getId(), sigLit,
						"hazmat(AGENT) & connection(RID) & location(J) & location(O) & road(RID, O, J) & healthy(AGENT) & " +
						"not blocked(O, J) & not dangerZone(O) & not dangerZone(J) & not stuck(AGENT) & atJ(AGENT, O) & dry(O, J) & not toxic(O,J)",
						1f),
				//slippery tarmac, healthy = 0.8
				new ConfidencePrecondition(holder.getId(), sigLit,
						"hazmat(AGENT) & connection(RID) & location(J) & location(O) & road(RID, O, J) & healthy(AGENT) & " +
						"not blocked(O, J) & not dangerZone(O) & not dangerZone(J) & not stuck(AGENT) & onR(AGENT, RID) & " +
						"slippery(O, J) & tarmac(O,J)",
						SLIPPERY_MODIFIER),
				new ConfidencePrecondition(holder.getId(), sigLit,
						"hazmat(AGENT) & connection(RID) & location(J) & location(O) & road(RID, O, J) & healthy(AGENT) & " +
						"not blocked(O, J) & not dangerZone(O) & not dangerZone(J) & not stuck(AGENT) & atJ(AGENT, O) & not toxic(O,J) & " +
						"slippery(O, J) & tarmac(O,J)",
						SLIPPERY_MODIFIER),
				//dry, damaged = 0.8
				new ConfidencePrecondition(holder.getId(), sigLit,
						"hazmat(AGENT) & connection(RID) & location(J) & location(O) & road(RID, O, J) & damaged(AGENT) & " +
						"not blocked(O, J) & not dangerZone(O) & not dangerZone(J) & not stuck(AGENT) & onR(AGENT, RID) & dry(O, J)",
						DAMAGE_AGENT_MODIFIER),
				new ConfidencePrecondition(holder.getId(), sigLit,
						"hazmat(AGENT) & connection(RID) & location(J) & location(O) & road(RID, O, J) & damaged(AGENT) & " +
						"not blocked(O, J) & not dangerZone(O) & not dangerZone(J) & not stuck(AGENT) & atJ(AGENT, O) & dry(O, J) & not toxic(O,J)",
						DAMAGE_AGENT_MODIFIER),
				//slippery tarmac, damaged = 0.8*0.8
				new ConfidencePrecondition(holder.getId(), sigLit,
						"hazmat(AGENT) & connection(RID) & location(J) & location(O) & road(RID, O, J) & damaged(AGENT) & " +
						"not blocked(O, J) & not dangerZone(O) & not dangerZone(J) & not stuck(AGENT) & onR(AGENT, RID) & " +
						"slippery(O, J) & tarmac(O,J)",
						DAMAGE_AGENT_MODIFIER*SLIPPERY_MODIFIER),
				new ConfidencePrecondition(holder.getId(), sigLit,
						"hazmat(AGENT) & connection(RID) & location(J) & location(O) & road(RID, O, J) & damaged(AGENT) & " +
						"not blocked(O, J) & not dangerZone(O) & not dangerZone(J) & not stuck(AGENT) & atJ(AGENT, O) & not toxic(O,J) & " +
						"slippery(O, J) & tarmac(O,J)",
						DAMAGE_AGENT_MODIFIER*SLIPPERY_MODIFIER)
		};
	}
		
	
	@Override
	public float getSpecificConfidence(BeliefBase bb, Literal action) {
		//move (0=agent, 1=road, 2=a,3=b) ; ROAD: a->b
		List<Term> args = action.getTerms();
		String from	= ((StringTerm)args.get(2)).getString();
		String to	= ((StringTerm)args.get(3)).getString();
		
		WorldBeliefBase wbb = (WorldBeliefBase)bb;
		
		if(wbb.getRoads().isEmpty())	{
			logger.severe("No Roads!; " + wbb.getRoads().keySet());
			return 0;
		}
		else	{	logger.info("Roads: " + wbb.getRoads().keySet());	}
		
		//from is either a junction or road id..
		RoadInf r = null;
		if(wbb.getRoads().containsKey(from))	{	r = wbb.getRoad(from);		}
		else									{	r = wbb.getRoad(from, to);	}
		
		if(r==null){
			//road does not exist!
			logger.warning(	"Confidence function cannot determine road being used to travel; " +
							"from=" + from +", to=" + to);
			logger.warning("Road list in BB: " + wbb.getRoads().keySet().toString());
			return 0f;
		}
		
		//need to handle specific road types when judging confidence...?
		//include rainfall knowledge for slippery tarmac / dry mud?
		float base = getGeneralConfidence(bb);
		
		logger.info("Estimation general conf=" + base);

		if(r.isFlooded() || r.isBlocked() )		{
			logger.warning(r.getId() + " is blocked (" + r.isBlocked() + ")/flooded(" + r.isFlooded() + "), return 0 confidence");
			return 0f;
		}
		else if(r.isSlippery() && r.getRoadType().equals(RoadType.mud))		{	return 0f;	}
		else if(r.isSlippery() && r.getRoadType().equals(RoadType.tarmac))	{	return base*SLIPPERY_MODIFIER; }
		else	{	return base;	}
	}


	@Override
	public ConfidencePrecondition[] getConfidenceBasedPreconditions(BeliefBase context) {
		return confPre;
	}
}
