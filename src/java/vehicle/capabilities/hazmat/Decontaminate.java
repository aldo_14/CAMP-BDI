package vehicle.capabilities.hazmat;

import static vehicle.capabilities.truck.MoveRoad.SLIPPERY_MODIFIER;

import agent.beliefBase.WorldBeliefBase;
import agent.model.capability.ConfidencePrecondition;
import common.UniversalConstants.RoadType;
import common.UniversalConstants.WorldAction;
import jason.asSyntax.Literal;
import jason.asSyntax.StringTerm;
import jason.bb.BeliefBase;
import truckworld.world.interfaces.RoadInf;
import vehicle.VehicleAgent;
import vehicle.capabilities.VehicleCapability;

/**
 * 
 * @author Alan
 *
 *         Decontaminate the given road, travelling from start to end.
 */
public class Decontaminate extends VehicleCapability {
	private static String signature = "decontaminate(AGENT, RID, O, J)";
	private static String[] defPre = {
			"atJ(AGENT, O) & hazmat(AGENT) & connection(RID) & location(J) & location(O) & "
					+ "road(RID, O, J) & not mortal(AGENT) & toxic(O, J) & not flooded(O, J) & tarmac(O,J) & "
					+ "not blocked(O,J) & not dangerZone(O) & not dangerZone(J) & not stuck(AGENT)",
			"atJ(AGENT, O) & hazmat(AGENT) & connection(RID) & location(J) & location(O) & "
					+ "road(RID, O, J) & not mortal(AGENT) & toxic(O, J) & dry(O, J) & "
					+ "not blocked(O,J) & not dangerZone(O) & not dangerZone(J) & not stuck(AGENT)" };
	private static String[] defEff = { "-toxic(O, J)", "-toxic(J,O)", "+atJ(AGENT, O)" };

	private final ConfidencePrecondition[] confPre;

	public Decontaminate(VehicleAgent holder) {
		super(holder, holder.getTaskFactory().factory(WorldAction.decontaminate.name()), // goal
				Literal.parseLiteral(signature), // signature - agent, road,loc
				defPre, // preconditions
				defEff);// effects
		confPre = new ConfidencePrecondition[] {
				// dry, healthy = 1
				new ConfidencePrecondition(holder.getId(), sigLit,
						"atJ(AGENT, O) & hazmat(AGENT) & connection(RID) & location(J) & location(O) & "
								+ "road(RID, O, J) & healthy(AGENT) & toxic(O, J) & dry(O, J) & "
								+ "not blocked(O,J) & not dangerZone(O) & not dangerZone(J) & not stuck(AGENT)",
						1f),
				// slippery tarmac, healthy = 0.8
				new ConfidencePrecondition(holder.getId(), sigLit,
						"atJ(AGENT, O) & hazmat(AGENT) & connection(RID) & location(J) & location(O) & "
								+ "road(RID, O, J) & healthy(AGENT) & toxic(O, J) & slippery(O, J) & tarmac(O,J) & "
								+ "not blocked(O,J) & not dangerZone(O) & not dangerZone(J) & not stuck(AGENT)",
						SLIPPERY_MODIFIER),
				// dry, damaged = 0.8
				new ConfidencePrecondition(holder.getId(), sigLit,
						"atJ(AGENT, O) & hazmat(AGENT) & connection(RID) & location(J) & location(O) & "
								+ "road(RID, O, J) & damaged(AGENT) & toxic(O, J) & dry(O, J) & "
								+ "not blocked(O,J) & not dangerZone(O) & not dangerZone(J) & not stuck(AGENT)",
						DAMAGE_AGENT_MODIFIER),
				// slippery tarmac, damaged =0.8*0.8
				new ConfidencePrecondition(holder.getId(), sigLit,
						"atJ(AGENT, O) & hazmat(AGENT) & connection(RID) & location(J) & location(O) & "
								+ "road(RID, O, J) & damaged(AGENT) & toxic(O, J) & slippery(O, J) & tarmac(O,J) &"
								+ "not blocked(O,J) & not dangerZone(O) & not dangerZone(J) & not stuck(AGENT)",
						SLIPPERY_MODIFIER * DAMAGE_AGENT_MODIFIER) };
	}

	@Override
	public float getSpecificConfidence(final BeliefBase bb, final Literal action) {
		final String from = ((StringTerm)action.getTerms().get(2)).getString();
		final String to	= ((StringTerm)action.getTerms().get(3)).getString();
		final WorldBeliefBase wbb = (WorldBeliefBase)bb;
		final RoadInf r = wbb.getRoads().containsKey(from)? wbb.getRoad(from): wbb.getRoad(from, to);
		if(r==null){
			logger.warning(	"Confidence function cannot determine road being used to travel; " +
							"from=" + from +", to=" + to + "\nRoad list in BB: " + wbb.getRoads().keySet().toString());
			return 0f;
		}
		if(!r.isToxic()) {
			return 1f; //can't fail!
		}
		if(r.isFlooded() || r.isBlocked() )		{
			logger.warning(r.getId() + " bad return 0 confidence");
			return 0f;
		}
		else if(r.isSlippery())	{	
			return r.getRoadType().equals(RoadType.tarmac) ? (getGeneralConfidence(bb)*SLIPPERY_MODIFIER) : 0f; 
		}
		else	{	
			return getGeneralConfidence(bb);	
		}
	}

	/**
	 * Extends to consider slippy roads if confValue > 0.8*0.8...
	 */
	@Override
	public String[] getPreconditionsByConfConstraint(float confValue) {
		float slipDamConf = 1 * (DAMAGE_AGENT_MODIFIER * SLIPPERY_MODIFIER);
		final String[] pre = getPreconditions();

		if (confValue > DAMAGE_AGENT_MODIFIER) {
			for (int i = 0; i < pre.length; i++) {
				pre[i] = pre[i].replaceAll("not mortal", "healthy");
			}
		}

		if (confValue > slipDamConf) {
			String[] ok = new String[pre.length];
			for (int i = 0; i < pre.length; i++) {
				ok[i] = pre[i] + " & not slippery(O,J)";
			}
			return ok;
		} else {
			return pre;
		}
	}

	@Override
	public ConfidencePrecondition[] getConfidenceBasedPreconditions(BeliefBase context) {
		return confPre;
	}

}
