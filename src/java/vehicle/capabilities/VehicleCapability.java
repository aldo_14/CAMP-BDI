package vehicle.capabilities;

import agent.model.capability.PrimitiveCapability;
import agent.model.goal.GoalTask;
import common.UniversalConstants.HealthState;
import jason.asSyntax.ASSyntax;
import jason.asSyntax.Literal;
import jason.asSyntax.parser.ParseException;
import jason.bb.BeliefBase;
import truckworld.env.Percepts;
import vehicle.VehicleAgent;

/**
 * Some shared capability method implementations, for effectors used by vehicles
 */
public abstract class VehicleCapability extends PrimitiveCapability	{
	
	public static final float DAMAGE_AGENT_MODIFIER = 0.7f;

	protected VehicleCapability(VehicleAgent holder, GoalTask task,
			Literal signature, String[] pre, String[] post) {
		super(holder, task, signature, pre, post);
	}

	/**
	 * Default implementation of get by conf constraint... if value > 0.8, we 
	 * cannot use damaged agents, so append.
	 */
	@Override
	public abstract String[]  getPreconditionsByConfConstraint(float minConf);
	
	/**
	 * Uses success rate, modified by health - i.e. 'mortal' agents can never do anything, 
	 * 'damaged' ones have half chance of succeeding compared to a perfectly healthy agent.
	 */
	@Override
	public float getGeneralConfidence(final BeliefBase bb) {
		//stuck/resting agents can't do ANYTHING
		Literal stuck = ASSyntax.createLiteral(Percepts.RESTING, ASSyntax.createString(holder.getId()));
		if(bb.contains(stuck)!=null)	{
			logger.warning("Stuck, confidence is zero!");
			return 0;
		}
		
		String mortal = HealthState.mortal.name() + "(\"" + holder.getId() + "\")";
		String damaged = HealthState.damaged.name() + "(\"" + holder.getId() + "\")";
		float base = 1;
		try {
			if(bb.contains(ASSyntax.parseLiteral(mortal)) != null)	{
				logger.finer("Confidence=" + base + " for dead agent");
				return 0;
			}
			else if(bb.contains(ASSyntax.parseLiteral(damaged)) != null)	{
				base = DAMAGE_AGENT_MODIFIER;
				logger.finer("Confidence=" + base + " for damaged agent");
			}
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
		
		return base * getSuccessRate();
	}
}
