/**
 * 
 */
package vehicle.capabilities.truck;

import jason.asSyntax.ASSyntax;
import jason.asSyntax.Literal;
import jason.asSyntax.StringTerm;
import jason.asSyntax.Term;
import jason.bb.BeliefBase;

import java.util.Collection;
import java.util.List;
import java.util.Vector;

import truckworld.env.Percepts;
import truckworld.world.interfaces.JunctionInf;
import truckworld.world.interfaces.RoadInf;
import vehicle.VehicleAgent;
import vehicle.capabilities.VehicleCapability;
import agent.beliefBase.WorldBeliefBase;
import agent.model.capability.ConfidencePrecondition;
import agent.model.goal.GoalTask;

import common.UniversalConstants.RoadType;
import common.UniversalConstants.WorldAction;

/**
 * @author Alan White
 * 
 * Task description for a capability to move from one position to another, with the start being
 * either a road or junction on the map
 */
public class MoveRoad extends VehicleCapability {
	
	public static final float SLIPPERY_MODIFIER = 0.7f;
	/**
	 * Note; we preclude movement to or from a danger-zone...
	 */
	private static String common = 
			"connection(RID) & location(J) & location(O) & road(RID, O, J) & not mortal(AGENT) & " +
			"not blocked(O, J) & not toxic(O,J) & not dangerZone(O) & not dangerZone(J) & not stuck(AGENT)";
	/**
	 * NB: this is a bit overcomplex because we support movement starting from either a junction or on a road in this code...
	 * Signature is move(Agent, A, b)
	 * Preconditions; 
	 * A is a road OR at junction (first arg)
	 * B is a junction
	 * types are correct for agent, road, junction (Vehicle, connection, location types)
	 * road connects to B
	 * agent is !mortal (health state=(healthy|damaged))
	 * road is !blocked 
	 * road is !flooded OR road is dry & mud (can travel on slippery tarmac, or any dry road)
	 * agent is !stuck
	 */
	private static final String[] defPre = {
		common + " & onR(AGENT, RID) & dry(O, J)", //mud OR tarmac
		common + " & onR(AGENT, RID) & tarmac(O, J) & slippery(O, J)",
		common + " & atJ(AGENT, O) & dry(O, J)",
		common + " & atJ(AGENT, O) & tarmac(O, J) & slippery(O, J)"
		};

	/*
	 * Effects; agent is at B, is not on the road at O or at the junction O (can remove both regardless of start case)
	 */
	private static final String[]  defEff = {"-onR(AGENT, RID)", "-atJ(AGENT, O)", "+atJ(AGENT, J)"};

	private final ConfidencePrecondition[] confPre;
	
	/**
	 * Default constructor
	 * @param holder
	 */
	public MoveRoad(VehicleAgent holder) {	
		this(	holder, 
				holder.getTaskFactory().factory(WorldAction.move.name()), //goal
				Literal.parseLiteral("move(AGENT, RID, O, J)"), //signature - agent, road, origin, destination
				defPre, //preconditions
				defEff);//effects	
	}
	
	public MoveRoad(VehicleAgent truckAgent, GoalTask task,
			Literal parseLiteral, String[] defPre2, String[] defEff2) {
		super(truckAgent, task, parseLiteral, defPre2, defEff2);
		confPre = new ConfidencePrecondition[]{
				//healthy,dry
				new ConfidencePrecondition(holder.getId(), sigLit,
						"connection(RID) & location(J) & location(O) & road(RID, O, J) & healthy(AGENT) & " +
						"not blocked(O, J) & not toxic(O,J) & not dangerZone(O) & not dangerZone(J) & " +
						"not stuck(AGENT) & onR(AGENT, RID) & dry(O, J)" , 
						1f),
				new ConfidencePrecondition(holder.getId(), sigLit, 
						"connection(RID) & location(J) & location(O) & road(RID, O, J) & healthy(AGENT) & " +
						"not blocked(O, J) & not toxic(O,J) & not dangerZone(O) & not dangerZone(J) & " +
						"not stuck(AGENT) & atJ(AGENT, O) & dry(O, J)" , 
						1f),
				//dry, damaged
				new ConfidencePrecondition(holder.getId(), sigLit, 
						"connection(RID) & location(J) & location(O) & road(RID, O, J) & damaged(AGENT) & " +
						"not blocked(O, J) & not toxic(O,J) & not dangerZone(O) & not dangerZone(J) & " +
						"not stuck(AGENT) & onR(AGENT, RID) & dry(O, J)" , 
						DAMAGE_AGENT_MODIFIER),
				new ConfidencePrecondition(holder.getId(), sigLit,
						"connection(RID) & location(J) & location(O) & road(RID, O, J) & damaged(AGENT) & " +
						"not blocked(O, J) & not toxic(O,J) & not dangerZone(O) & not dangerZone(J) & " +
						"not stuck(AGENT) & atJ(AGENT, O) & dry(O, J)" , 
						DAMAGE_AGENT_MODIFIER),
				//slippery tarmac, healthy
				new ConfidencePrecondition(holder.getId(), sigLit,
						"connection(RID) & location(J) & location(O) & road(RID, O, J) & healthy(AGENT) & " +
						"not blocked(O, J) & not toxic(O,J) & not dangerZone(O) & not dangerZone(J) & " +
						"not stuck(AGENT) & onR(AGENT, RID) & slippery(O, J) & tarmac(O,J)" , 
						SLIPPERY_MODIFIER),
				new ConfidencePrecondition(holder.getId(), sigLit,
						"connection(RID) & location(J) & location(O) & road(RID, O, J) & healthy(AGENT) & " +
						"not blocked(O, J) & not toxic(O,J) & not dangerZone(O) & not dangerZone(J) & " +
						"not stuck(AGENT) & atJ(AGENT, O) & slippery(O, J) & tarmac(O,J)" , 
						SLIPPERY_MODIFIER),
				//slippery tarmac, damaged
				new ConfidencePrecondition(holder.getId(), sigLit,
						"connection(RID) & location(J) & location(O) & road(RID, O, J) & damaged(AGENT) & " +
						"not blocked(O, J) & not toxic(O,J) & not dangerZone(O) & not dangerZone(J) & " +
						"not stuck(AGENT) & onR(AGENT, RID) & slippery(O, J) & tarmac(O,J)" , 
						DAMAGE_AGENT_MODIFIER*SLIPPERY_MODIFIER),
				new ConfidencePrecondition(holder.getId(), sigLit,
						"connection(RID) & location(J) & location(O) & road(RID, O, J) & damaged(AGENT) & " +
						"not blocked(O, J) & not toxic(O,J) & not dangerZone(O) & not dangerZone(J) & " +
						"not stuck(AGENT) & atJ(AGENT, O) & slippery(O, J) & tarmac(O,J)" , 
						DAMAGE_AGENT_MODIFIER*SLIPPERY_MODIFIER)
		};
	}	

	/**
	 * Extends to consider slippy roads if confValue > 0.8*0.8...
	 */
	@Override
	public String[] getPreconditionsByConfConstraint(float confValue) {
		float slipDamConf = 1*(DAMAGE_AGENT_MODIFIER * SLIPPERY_MODIFIER);

		//damaged or slippery is bad
		if(confValue > MoveRoad.SLIPPERY_MODIFIER || confValue > DAMAGE_AGENT_MODIFIER)	{
			return new String[]{
					//strips the 'slippery and tarmac is ok' constraint
					defPre[0].replaceAll("not mortal", "healthy"), //not damaged
					defPre[2].replaceAll("not mortal", "healthy")
			};
		}

		//damaged and slippery is bad, damaged OR slippery is ok
		if(confValue <= MoveRoad.SLIPPERY_MODIFIER && confValue <= DAMAGE_AGENT_MODIFIER && confValue > slipDamConf )	{
			return new String[]{
					defPre[0].replaceAll("not mortal", "healthy"), //not damaged
					defPre[1].replaceAll("not mortal", "healthy"),
					defPre[1].replaceAll("slippery", "dry"),
					defPre[3].replaceAll("slippery", "dry")
			};
		}
		return defPre;
	}
	
	public float getGeneralConfidence(BeliefBase bb)	{
		Literal stuck = ASSyntax.createLiteral(Percepts.RESTING, 
				ASSyntax.createString(holder.getId()));
		if(bb.contains(stuck)!=null)	{
			logger.warning("Stuck, confidence is zero!");
			return 0;
		}
		
		float base = super.getGeneralConfidence(bb);
		
		//if we are in a location with no outbound connections OR all or slippery, modify
		//confidence accordingly
		VehicleAgent v = (VehicleAgent)holder;
		Literal loc = v.getLocLit();
		if(loc==null)	{	return base;	}
		if(bb instanceof WorldBeliefBase)	{
			WorldBeliefBase vbb = (WorldBeliefBase)bb;
			String locId = ((StringTerm)loc.getTerm(1)).getString();
			if(!vbb.getJunctions().isEmpty() && !vbb.getRoads().isEmpty())	{
				if(loc.getFunctor().equals(Percepts.ON_ROAD))	{
					RoadInf r;
					try {
						r = (RoadInf) vbb.getRoad(locId).clone();
					} catch (CloneNotSupportedException e) {
						String st= "";
						for(StackTraceElement ste: e.getStackTrace())	{	st = st + "\n" + ste;	}
						logger.severe(e.toString() + st);
						return 0;
					}
					
					//presume we have agents to secures, decon, etc at this juncture
					if(r.isFlooded())		{	
						logger.severe(holder.getId() + " on a flooded road, zero confidence in moveRoad");
						return 0;	
					}
					else if(r.isSlippery())	{	return base * SLIPPERY_MODIFIER;	}
					else					{	return base;						}
				}
				else	{//adJ
					JunctionInf j = vbb.getJunction(locId);
					Collection<String> outbound = j.getRoads();
					Collection<RoadInf> viable = new Vector<RoadInf>();
					for(String rid: outbound)	{
						RoadInf r = vbb.getRoad(rid);
						//if(v.getVehicleObject().canTravelAlong(r))	{	viable.add(r);	}
						//just check state, assume other things can be inverted...
						if(!r.isFlooded())	{	viable.add(r);	}
					}
					
					if(viable.isEmpty())	{	
						logger.severe(	"No viable (unflooded) roads from " + j + " " + holder.getId() + 
										" has zero confidence in movement");
						return 0;	
					}
					
					for(RoadInf r: viable)	{
						if(!r.isSlippery())	{	return base;	}
					}
					
					logger.severe("All viable roads from " + j + " are slippery!");
					return base * SLIPPERY_MODIFIER;	
				}
			}
		}
		logger.severe("Fall-through case");
		return base;
	}
	
	@Override
	public float getSpecificConfidence(BeliefBase bb, Literal action) {
		//move (0=agent, 1=road, 2=a,3=b) ; ROAD: a->b
		List<Term> args = action.getTerms();
		String from	= ((StringTerm)args.get(2)).getString();
		String to	= ((StringTerm)args.get(3)).getString();
		
		WorldBeliefBase wbb = (WorldBeliefBase)bb;
		
		if(wbb.getRoads().isEmpty())	{
			logger.severe("No Roads!; " + wbb.getRoads().keySet());
			return 0;
		}
		else	{	logger.info("Roads: " + wbb.getRoads().keySet());	}
		
		//from is either a junction or road id..
		RoadInf r = null;
		if(wbb.getRoads().containsKey(from))	{	r = wbb.getRoad(from);		}
		else									{	r = wbb.getRoad(from, to);	}
		
		if(r==null){
			//road does not exist!
			logger.warning(	"Confidence function cannot determine road being used to travel; " +
							"from=" + from +", to=" + to);
			logger.warning("Road list in BB: " + wbb.getRoads().keySet().toString());
			return 0f;
		}
		
		//need to handle specific road types when judging confidence...?
		//include rainfall knowledge for slippery tarmac / dry mud?
		float base = getGeneralConfidence(bb);
		
		logger.info("Estimation general conf=" + base);
		if(r.isFlooded() || r.isBlocked() || r.isToxic())		{
			logger.warning(r.getId() + " is blocked (" + r.isBlocked() + ")/flooded(" + r.isFlooded() + 
					")/toxic(" + r.isToxic() + "), return 0 confidence");
			return 0f;
		}
		else if(r.isSlippery())		{	
			return r.getRoadType().equals(RoadType.mud)?0f: base*SLIPPERY_MODIFIER; 
		}
		else	{	return base;	}
	}

	@Override
	public ConfidencePrecondition[] getConfidenceBasedPreconditions(BeliefBase context) {
		return confPre;
	}

}
