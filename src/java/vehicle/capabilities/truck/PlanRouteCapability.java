package vehicle.capabilities.truck;

import jason.asSyntax.Literal;
import jason.bb.BeliefBase;
import agent.model.capability.ConfidencePrecondition;
import agent.model.capability.PrimitiveCapability;
import agent.type.CapabilityAwareAgent;

/**
 * 
 * @author Alawhite
 *
 * Capability referring to the internal ability to plan a route - DO WE NEED THIS?
 */
public class PlanRouteCapability extends PrimitiveCapability{

	private final ConfidencePrecondition[] confPre;
	
	public PlanRouteCapability(CapabilityAwareAgent holder) {
		super(	holder, 
				holder.getTaskFactory().factory("vehicle.capabilities.PlanRoute"), 
				holder.getTaskFactory().factory("vehicle.capabilities.PlanRoute").getSignature(), 
				holder.getTaskFactory().factory("vehicle.capabilities.PlanRoute").getPreconditions(), 
				holder.getTaskFactory().factory("vehicle.capabilities.PlanRoute").getGoalEffects());
		confPre = new ConfidencePrecondition[preconditions.length];
		for(int i=0; i<confPre.length; i++) {
			confPre[i] = new ConfidencePrecondition(holder.getId(), sigLit, preconditions[i], 1);
		}
	}

	@Override
	public float getSpecificConfidence(BeliefBase bb, Literal action) {
		return getGeneralConfidence(bb);
	}

	@Override
	public ConfidencePrecondition[] getConfidenceBasedPreconditions(BeliefBase context) {
		return confPre;
	}
}
