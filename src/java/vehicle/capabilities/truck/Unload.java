package vehicle.capabilities.truck;

import jason.asSyntax.Literal;
import jason.bb.BeliefBase;
import vehicle.VehicleAgent;
import vehicle.capabilities.VehicleCapability;
import agent.model.capability.ConfidencePrecondition;
import common.UniversalConstants.WorldAction;

public class Unload extends VehicleCapability {

	private static String[]	defPre = {	"truck(AGENT) & location(J) & cargo(C) & not mortal(AGENT) & atJ(AGENT, J) & " +
										"loaded(AGENT, C) &  carryingCargo(AGENT)"};
	private static String[]	defEff = {"+cargoAt(C,J)", "-loaded(AGENT, C)", "-carryingCargo(AGENT)"};//, "-cargoNeeded(J)"};

	private final ConfidencePrecondition[] confPre;
	
	public Unload(VehicleAgent holder) {
		super(	holder, 
				holder.getTaskFactory().factory(WorldAction.unload.name()),
				Literal.parseLiteral("unload(AGENT, J, C)"), //cargo C at J
				defPre, defEff);
		confPre = new ConfidencePrecondition[]{
				new ConfidencePrecondition(holder.getId(), sigLit,
					"truck(AGENT) & location(J) & cargo(C) & healthy(AGENT) & atJ(AGENT, J) & loaded(AGENT, C) &  carryingCargo(AGENT)", 
					1),
				new ConfidencePrecondition(holder.getId(), sigLit,
					"truck(AGENT) & location(J) & cargo(C) & damaged(AGENT) & atJ(AGENT, J) & loaded(AGENT, C) &  carryingCargo(AGENT)", 
					DAMAGE_AGENT_MODIFIER)
		};
	}

	@Override
	public float getSpecificConfidence(BeliefBase bb, Literal action) {
		return getGeneralConfidence(bb);
	}

	public String[] getPreconditionsByConfConstraint(float minConf) {
		if(minConf > DAMAGE_AGENT_MODIFIER)	{
			return new String[]{ 	defPre[0].replaceAll("not mortal", "healthy")	};
		}
		else	{	return defPre;	}
	}

	@Override
	public ConfidencePrecondition[] getConfidenceBasedPreconditions(BeliefBase context) {
		return confPre;
	}
}
