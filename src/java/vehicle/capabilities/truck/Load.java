package vehicle.capabilities.truck;

import jason.asSyntax.Literal;
import jason.bb.BeliefBase;
import vehicle.VehicleAgent;
import vehicle.capabilities.VehicleCapability;
import agent.model.capability.ConfidencePrecondition;
import common.UniversalConstants.WorldAction;

public class Load extends VehicleCapability {

	private static String[] defPre = {
		"truck(AGENT) & cargo(C) & location(J) & atJ(AGENT, J) & cargoAt(C, J) & not mortal(AGENT) & not carryingCargo(AGENT)"}; 
	private static String[]	defEff = {"-cargoAt(C,J)", "+loaded(AGENT, C)", "+carryingCargo(AGENT)"};
	private final ConfidencePrecondition[] confPre;
	
	public Load(VehicleAgent holder) {
		super(	holder, 
				holder.getTaskFactory().factory(WorldAction.load.name()),
				Literal.parseLiteral("load(AGENT, J, C)"), //cargo C at J
				defPre, defEff);
		confPre = new ConfidencePrecondition[]{
				new ConfidencePrecondition(holder.getId(), sigLit,
					"truck(AGENT) & cargo(C) & location(J) & atJ(AGENT, J) & cargoAt(C, J) & healthy(AGENT) & not carryingCargo(AGENT)",
					1),
				new ConfidencePrecondition(holder.getId(), sigLit,
					"truck(AGENT) & cargo(C) & location(J) & atJ(AGENT, J) & cargoAt(C, J) & damaged(AGENT) & not carryingCargo(AGENT)", 
					DAMAGE_AGENT_MODIFIER)
		};
	}

	@Override
	public float getSpecificConfidence(BeliefBase bb, Literal action) {
		return getGeneralConfidence(bb);
	}

	@Override
	public String[] getPreconditionsByConfConstraint(float minConf) {
		if(minConf > DAMAGE_AGENT_MODIFIER)	{
			return new String[]{ 	defPre[0].replaceAll("not mortal", "healthy")	};
		}
		else	{	return defPre;	}
	}


	@Override
	public ConfidencePrecondition[] getConfidenceBasedPreconditions(BeliefBase context) {
		return confPre;
	}
}
