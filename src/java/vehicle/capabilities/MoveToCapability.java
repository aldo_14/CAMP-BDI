package vehicle.capabilities;
import java.util.Collection;
import java.util.Stack;
import java.util.logging.Level;

import agent.beliefBase.WorldBeliefBase;
import agent.model.capability.CompositeCapability;
import agent.model.goal.GoalTask;
import agent.type.CapabilityAwareAgent;
import common.UniversalConstants.HealthState;
import common.UniversalConstants.RoadCondition;
import common.Utils;
import common.Utils.HeuristicsForGScore;
import common.Utils.HeuristicsForHScore;
import jason.asSyntax.ASSyntax;
import jason.asSyntax.Literal;
import jason.asSyntax.Plan;
import jason.asSyntax.StringTerm;
import jason.bb.BeliefBase;
import truckworld.env.Percepts;
import truckworld.world.interfaces.JunctionInf;
import truckworld.world.interfaces.RoadInf;
import vehicle.VehicleAgent;
import vehicle.capabilities.truck.MoveRoad;
import vehicle.ia.getLocOnRoad;

/**
 * 
 * @author alanwhite
 *
 * Special case for composite 'moveTo' plans, as they form the plan on-demand then invoke it,
 * meaning that we do not always expect there to be a plan.  Instead, we use the 'move' primitive
 * capability confidence as a basis for calculation.
 */
public class MoveToCapability extends CompositeCapability {
	
	public MoveToCapability(CapabilityAwareAgent agent, GoalTask task, Plan plan){
		super(agent, task, plan);
		logger.setLevel(Level.FINER);
	}

	public MoveToCapability(CapabilityAwareAgent agent, GoalTask task){
		super(agent, task);
		logger.setLevel(Level.FINER);
	}
	
	//specialist confidence messages
	public float getGeneralConfidence(BeliefBase bb) {
		//could try brute force - average all routes?  Better just to use general confidence.  Better still to try and
		//use moveCapability confidence..
		if(!holder.hasCapability("move") || holder.getCapability("move").getGeneralConfidence(bb)<=0) {
			logger.warning(holder.getId() + " cannot move");
			return 0;
		}
		
		//else
		VehicleAgent v = (VehicleAgent)holder;
		WorldBeliefBase vbb = (WorldBeliefBase)bb;
		float base = this.getSuccessRate();//mc.getGeneralConfidence(bb);
		if(base == 0 )	{	return 0;	}
		
		Literal location = v.getLocLit();
		if(location == null)	{	return base;	}//unknown loc - probably just registered...
		else	{
			String jid = ((StringTerm)location.getTerm(1)).getString();
			if(location.getFunctor().equals(Percepts.AT_JUNCTION))	{
				JunctionInf j = vbb.getJunction(jid);
				if(j!=null)		{
					Collection<String> road = j.getRoads();
					for(String r: road)	{		if(!vbb.getRoad(r).isSlippery())	{	return base;	}	}
					//done - reaching this point implies all poss start roads are slippery...
					return base * MoveRoad.SLIPPERY_MODIFIER;
				}
				else	{	
					logger.severe("Null junction; " +jid + "\nJset:" + vbb.getJunctions().keySet().toString());	
				}
			}
			else	{ //onR
				RoadInf r = vbb.getRoad(jid);
				if(r!=null)	{	
					if(r.isSlippery())	{	return base * MoveRoad.SLIPPERY_MODIFIER;	}	
					else				{	return base;	} //assumes stuck check already made
				}
				else	{	logger.severe("Null road; " + jid);	}
			}
			logger.severe("Can't find location for confidence Gen Est?  Returning base conf " + base + "\n" + getHolder().getBB());
		}
		return base;
	}

	/**
	 * Confidence is derived by estimating the route which will be taken, and employing a rough
	 * mirror of the 'move' capability.
	 */
	public float getSpecificConfidence(BeliefBase bb, Literal action) {
		if(!holder.hasCapability("move"))	{
			logger.warning(holder.getId() + "; Holder cannot move, return 0");
			return 0;
		}

		if(!action.isGround())	{	
			logger.warning(action + " is unground, returning general confidence for moveTo");
			return getGeneralConfidence(bb);	
		}
		
		if(action.getTerm(1).equals(action.getTerm(2)))	{
			logger.info("Same start and end for  " + action + ", so return 1 (no-op)");
			return 1;
		}
		/*
		 * If we can find a route, return the move capability confidence.  If no route exists, then
		 * we return <b>0</b>.
		 */
		if(! (holder instanceof VehicleAgent))	{
			logger.severe("MoveTo held by a non vehicle agent " + holder);
			return 0;
		}
		
		VehicleAgent va = ((VehicleAgent)holder);
		
		if(va.getHealth().equals(HealthState.mortal))	{
			logger.severe("Mortal agent " + va.getId() + ", returning 0");
			return 0;
		}
		
		JunctionInf end  = 
				((WorldBeliefBase)bb).getJunction(((StringTerm)action.getTerm(2)).getString());

		logger.finer("Check BB:");
		holder.bbDump(bb, Level.FINER);
		
		//trickier - are we on a road or a junction?
		boolean onRoad = getLocOnRoad.onRoad(va.getId(), bb);
		JunctionInf start;
		if(onRoad)	{
			RoadInf r = getLocOnRoad.getVehicleRoad(va.getId(), bb);
			//distance...
			JunctionInf j1 =  r.getEnds()[0];
			JunctionInf j2 =  r.getEnds()[1];

			double disJ1 = j1.getCoordinate().distance(end.getCoordinate());
			double disJ2 = j2.getCoordinate().distance(end.getCoordinate());
			if(disJ1 < disJ2)	{	start = j1;	}
			else				{	start = j2;	}
		}
		else	{
			start = getLocOnRoad.getVehicleLocation(va.getId(), bb);
		}
		
		if(start == null || end == null)	{
			logger.warning("Unable to determine start (" + start + ") or end (" + end + ")");
			return 0; //assume invalid arguments
		}
		if (!onRoad && start.getId().equals(end.getId())) {
			logger.info("Already present at " + end);
			return 1;
		}

		//can we find a route from a to b?
		//don't loosen for this...
		Stack<JunctionInf> route = Utils.getRoute(start, end, ((WorldBeliefBase)bb).getRoads(),
					va.getVehicleObject(), HeuristicsForHScore.distanceOnly, HeuristicsForGScore.confidenceBased, false);
		if(route == null) {
			route = Utils.getRoute(start, end, ((WorldBeliefBase)bb).getRoads(), va.getVehicleObject(), 
					HeuristicsForHScore.distanceOnly, HeuristicsForGScore.confidenceBased, true);
		}
		
		if(route == null || route.isEmpty()){
			logger.warning("No route exists! " + action + " (route=" + route + ", vehicle=" + va.getVehicleObject() + ")");
			//debug op
			if(logger.isLoggable(Level.INFO))	{
				String roadInf = "";
				for(RoadInf r: ((WorldBeliefBase)bb).getRoads().values())	{
					roadInf = roadInf + "\n" + r.toString() + " (traversable? " + 
							va.getVehicleObject().canTravelAlong(r) + ")";
				}
				logger.info("Road information;" + roadInf);
			}
			return 0;
		}
		else	{
			return getRouteConfidence(bb, route);
		}
	}

	private float getRouteConfidence(BeliefBase bb, Stack<JunctionInf> route) {
		//TODO different confidence methods?
		float conf =  ((VehicleAgent)holder).getHealth().equals(HealthState.healthy)?1:0;
		logger.info("Found a route, returning conf estimate\n" + route);

		for(int i=0; i<route.size()-1; i++)	{
			JunctionInf st = route.get(i);
			JunctionInf ed = route.get(i+1);
			RoadInf r = ((WorldBeliefBase)bb).getRoad(st.getConnection(ed));
			logger.severe(	"Check route road; " + r + 
							" slippery alt check;" + isSlipperyRoad(st.getId(), ed.getId(), bb) + 
							", holderBB=" + isSlipperyRoad(st.getId(), ed.getId(), holder.getBB()));
			if(r.isSlippery())	{	return conf*MoveRoad.SLIPPERY_MODIFIER;	}
		}
		return conf;
	}
	
	private boolean isSlipperyRoad(String from, String to, BeliefBase bb)	{
		StringTerm toS = ASSyntax.createString(to);
		StringTerm fromS = ASSyntax.createString(from);
		Literal slipLit = ASSyntax.createLiteral(RoadCondition.slippery.name(), toS, fromS);
		return bb.contains(slipLit)!=null;
	}
	
	@Override
	public float getCostEstimate(Literal action, BeliefBase bb)	{
		if(!holder.hasCapability("move"))	{
			logger.warning(holder.getId() + "; Holder cannot move, return 0");
			return getGeneralCostEstimate();
		}

		if(!action.isGround())	{	
			logger.warning(action + " is unground, returning general cost for moveTo");
			return getGeneralCostEstimate();
		}
		
		if(action.getTerm(1).equals(action.getTerm(2)))	{
			logger.info("Same start and end for  " + action + ", so return 1 (no-op)");
			return getGeneralCostEstimate();
		}
		/*
		 * If we can find a route, return the move capability confidence.  If no route exists, then
		 * we return <b>0</b>.
		 */
		if(! (holder instanceof VehicleAgent))	{
			logger.severe("MoveTo held by a non vehicle agent " + holder);
			return Integer.MAX_VALUE;
		}
		
		VehicleAgent va = ((VehicleAgent)holder);
		
		if(va.getHealth().equals(HealthState.mortal))	{
			logger.severe("Mortal agent " + va.getId() + ", returning 0");
			return Integer.MAX_VALUE;
		}
		
		JunctionInf end  = 
				((WorldBeliefBase)bb).getJunction(((StringTerm)action.getTerm(2)).getString());

		//trickier - are we on a road or a junction?
		boolean onRoad = getLocOnRoad.onRoad(va.getId(), bb);
		JunctionInf start;
		if(onRoad)	{
			RoadInf r = getLocOnRoad.getVehicleRoad(va.getId(), bb);
			//distance...
			JunctionInf j1 =  r.getEnds()[0];
			JunctionInf j2 =  r.getEnds()[1];

			double disJ1 = j1.getCoordinate().distance(end.getCoordinate());
			double disJ2 = j2.getCoordinate().distance(end.getCoordinate());
			if(disJ1 < disJ2)	{	start = j1;	}
			else				{	start = j2;	}
		}
		else	{
			start = getLocOnRoad.getVehicleLocation(va.getId(), bb);
		}
		
		if(start == null || end == null)	{
			logger.warning("Cost Unable to determine start (" + start + ") or end (" + end + ")");
			return Integer.MAX_VALUE; //assume invalid arguments
		}
		else	{
			logger.info("Cost; Route search info (Neighbours); st=" + start.getNeighbours() + ", end=" + end.getNeighbours());
		}

		//can we find a route from a to b?
		//don't loosen for this...
		Stack<JunctionInf> route = Utils.getRoute(
						start, end, ((WorldBeliefBase)bb).getRoads(), va.getVehicleObject(), 
						HeuristicsForHScore.distanceOnly,
						HeuristicsForGScore.confidenceBased, false);
		if(route == null) {
			route = Utils.getRoute(
					start, end, ((WorldBeliefBase)bb).getRoads(), va.getVehicleObject(), 
					HeuristicsForHScore.distanceOnly,
					HeuristicsForGScore.confidenceBased, true);
		}

		if(route == null){
			logger.warning("Cost; No route exists! " + action + " (route=" + route + ", vehicle=" + va.getVehicleObject() + ")");
			return Integer.MAX_VALUE;
		}
		else	{	return route.size()-1;	}
	}
	

	public String[] getPreconditionsByConfConstraint(float confValue) {
		if(confValue < VehicleCapability.DAMAGE_AGENT_MODIFIER)	{
			String[] pre = this.getPreconditions();
			String[] rt = new String[pre.length];
			for(int i=0;i<rt.length;i++)	{	rt[i] = pre[i].replaceAll("not mortal", "healthy");	}
			return rt;
		}
		return super.getPreconditionsByConfConstraint(confValue);
	}
}
