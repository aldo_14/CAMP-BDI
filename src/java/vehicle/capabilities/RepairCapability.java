package vehicle.capabilities;

import common.UniversalConstants.WorldAction;

import jason.asSyntax.Literal;
import jason.bb.BeliefBase;
import vehicle.VehicleAgent;
import agent.model.capability.ConfidencePrecondition;
import agent.model.goal.GoalTask;

/** 
 * @author Alan White
 *
 * If we are at a junction with a repair shop, we can fix ourself!
 */
public class RepairCapability extends VehicleCapability {

	private static String[] defPre = new String[]{
	"location(O) & atJ(AGENT, O) & not dangerZone(O) & garage(O) & not healthy(AGENT)"};
	private static String[] defEff = 
			new String[]{"-damaged(AGENT)", "-mortal(AGENT)", "+healthy(AGENT)"}; 
	
	protected RepairCapability(VehicleAgent holder, GoalTask task,
			Literal signature, String[] pre, String[] post) {
		super(	holder, 
				holder.getTaskFactory().factory(WorldAction.repair.name()), //goal
				Literal.parseLiteral("repair(AGENT, O)"), //signature - agent, location
				defPre, //preconditions
				defEff);//effects	
		throw new RuntimeException("Not implemented");
	}

	/**
	 * Shouldn't be any additional preconditions; allow agents to repair even if mortal
	 * and at a location (can't be stuck anyways)
	 */
	@Override
	public String[] getPreconditionsByConfConstraint(float minConf) {
		return this.getPreconditions();
	}

	@Override
	public ConfidencePrecondition[] getConfidenceBasedPreconditions(BeliefBase context) {
		throw new RuntimeException("Not implemented");
	}

}
