package vehicle.capabilities;

import jason.asSyntax.Literal;
import jason.bb.BeliefBase;
import agent.model.capability.ConfidencePrecondition;
import agent.model.capability.PrimitiveCapability;
import agent.type.CapabilityAwareAgent;

import common.UniversalConstants.WorldAction;

/**
 * 
 * @author Alan
 * 
 * Final stage of fulfilling a cargo request
 */
public class ConsumeCapability extends PrimitiveCapability {

	private static String[] defPre = {"cargo(C) & location(J) & cargoAt(C, J) & cargoNeeded(J)"}; 
	private static String[]	defEff = {"-cargoAt(C,J)", "-cargoNeeded(J)", "-cargo(C)"};

	public ConsumeCapability(CapabilityAwareAgent holder) {
		super(	holder, holder.getTaskFactory().factory(WorldAction.consume.name()), 
				Literal.parseLiteral("consume(AGENT, J, C)"), 
				defPre, defEff);
	}
	
	
	/**
	 * Default implementation is to return 1 - hard for this to fail...
	 */
	@Override
	public float getGeneralConfidence(BeliefBase bb) {	return 1;	}
	
	/**
	 * Default implementation just returns the general confidence; see getGeneralConfidence
	 */
	@Override
	public float getSpecificConfidence(BeliefBase bb, Literal planAction) {
		if(preconditionsHold(bb, planAction))	{	return 1;	}
		else									{	return 0;	}
	}

	@Override
	public ConfidencePrecondition[] getConfidenceBasedPreconditions(BeliefBase context) {
		return new ConfidencePrecondition[]{
			new ConfidencePrecondition(holder.getId(), getSignature(), defPre[0], 1f)
		};
	}
}
