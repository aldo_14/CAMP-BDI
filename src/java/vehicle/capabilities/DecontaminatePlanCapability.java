package vehicle.capabilities;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Vector;

import agent.beliefBase.WorldBeliefBase;
import agent.model.capability.CompositeCapability;
import agent.model.capability.ConfidencePrecondition;
import agent.model.capability.ExternalCapability;
import agent.model.goal.GoalTask;
import agent.type.CapabilityAwareAgent;
import common.UniversalConstants.RoadType;
import common.UniversalConstants.WorldAction;
import jason.asSyntax.Literal;
import jason.asSyntax.Plan;
import jason.asSyntax.StringTerm;
import jason.bb.BeliefBase;
import truckworld.world.interfaces.RoadInf;
import vehicle.capabilities.truck.MoveRoad;

public class DecontaminatePlanCapability extends CompositeCapability {
	
	public DecontaminatePlanCapability(CapabilityAwareAgent agent, GoalTask task, Plan plan){
		super(agent, task, plan);
	}

	public DecontaminatePlanCapability(CapabilityAwareAgent agent, GoalTask task){
		super(agent, task);
	}
	
	@Override
	public float getSpecificConfidence(final BeliefBase bb, final Literal action) {
		final String roadId = ((StringTerm) action.getTerm(1)).getString();
		final RoadInf cleanMe = ((WorldBeliefBase)bb).getRoad(roadId);
		if(!cleanMe.isToxic()) {
			return 1;
		}
		if (cleanMe.isFlooded() || (cleanMe.isSlippery() && cleanMe.getRoadType().equals(RoadType.mud)))  {
			return 0;
		} else {
			return super.getSpecificConfidence(bb, action);
		}
	}

	
	//specialist confidence messages
	public float getGeneralConfidence(final BeliefBase bb) {
		//get confidences for 'clear'
		Collection<ExternalCapability> deconSet = holder.getExtCapability(WorldAction.decontaminate.name());
		
		if(!deconSet.isEmpty()) {
			return getConfidenceWhenKnowHazmats(bb, deconSet);
		}
		else {
			return getConfidenceWhenForwarding(bb, holder.getExtCapability("decontaminateRoad"));
		}
	}

	private float getConfidenceWhenForwarding(final BeliefBase bb, final Collection<ExternalCapability> caps) {
		float highConf = 0;
		for(final ExternalCapability ex: caps) {
			final float gc = ex.getGeneralConfidence(bb);
			highConf = (gc>highConf) ? gc : highConf;
		}
		return highConf;
	}

	private float getConfidenceWhenKnowHazmats(final BeliefBase bb, final Collection<ExternalCapability> deconSet) {
		float highestFoundConfidence = 0; //highest conf
		for (ExternalCapability ec : deconSet) {
			float decontaminateConfidence = ec.getGeneralConfidence(bb);
			checkConfidence: 
			if (decontaminateConfidence > highestFoundConfidence) {
				String nm = ec.getHolderName();
				Collection<ExternalCapability> moves = holder.getExtCapability("moveTo", nm);
				if (moves.isEmpty()) {
					if (!ec.isPrimitive()) {
						// assuming is a composite?
						if (decontaminateConfidence > highestFoundConfidence) {
							highestFoundConfidence = decontaminateConfidence;
							logger.info("New highest conf is for " + ec.getHolderName() + "; " + highestFoundConfidence);
						}
					}
					break checkConfidence;
				}

				// find the moveTo confidence for this agent...
				float highestMoveToConfidence = 0;
				for(ExternalCapability move:moves)	{
					float singleMoveGeneralConfidence = move.getGeneralConfidence(bb);
					highestMoveToConfidence = (singleMoveGeneralConfidence > highestMoveToConfidence) ? singleMoveGeneralConfidence : highestMoveToConfidence;
				}
				
				float overallConf = (decontaminateConfidence > highestMoveToConfidence)	? highestMoveToConfidence : decontaminateConfidence;
				
				if(overallConf > highestFoundConfidence)	{
					highestFoundConfidence = overallConf;
					logger.info("New highest conf is for provider " + ec.getHolderName() + "; " + highestFoundConfidence);
				}//end if
			}
		}
		return highestFoundConfidence; //best plan...
	}
	
	@Override
	public ConfidencePrecondition[] getConfidenceBasedPreconditions(final BeliefBase context) {
		final ArrayList<ConfidencePrecondition> preconds = new ArrayList<>();
		final float generalConfidence = this.getGeneralConfidence(context);
		for(final String originalPrecond : this.getPreconditions()) {
			if(originalPrecond.contains("not \\(flooded")) {
				final String pre1 = originalPrecond.replaceAll("not \\(flooded", "(dry");
				final String pre2 = originalPrecond.replaceAll("not \\(flooded", "(slippery");
				preconds.add(new ConfidencePrecondition(holder.getId(), getSignature(), pre1, generalConfidence));
				preconds.add(new ConfidencePrecondition(holder.getId(), getSignature(), pre2, generalConfidence*MoveRoad.SLIPPERY_MODIFIER));	
			}
			else if(originalPrecond.contains("not flooded")) {
				final String pre1 = originalPrecond.replaceAll("not flooded", "dry");
				final String pre2 = originalPrecond.replaceAll("not flooded", "slippery");
				preconds.add(new ConfidencePrecondition(holder.getId(), getSignature(), pre1, generalConfidence));
				preconds.add(new ConfidencePrecondition(holder.getId(), getSignature(), pre2, generalConfidence*MoveRoad.SLIPPERY_MODIFIER));	
			}
			else {
				preconds.add(new ConfidencePrecondition(holder.getId(), getSignature(), originalPrecond, generalConfidence));
			}
		}
		return preconds.toArray(new ConfidencePrecondition[preconds.size()]);
	}
	
	@Override
	public Collection<ExternalCapability> getConfidenceBasedPreconditionEcs(final BeliefBase bb) {
		final Collection<ExternalCapability> set = new Vector<>();
		GoalTask goal = achieves();
		float conf = getGeneralConfidence(bb);
		Map<String, Vector<Plan>> means = getPossibleMeans();
		for (final String pre : means.keySet()) {
			final Vector<Plan> plans = means.get(pre);
			for (final Plan plan : plans) {
				Collection<String> effSet = holder.agentUtils().getPostEffectsOfPlan(holder, plan.getBody());
				String[] eff = new String[effSet.size()];
				String es = "";
				for (int i = 0; i < eff.length; i++) {
					eff[i] = (String) effSet.toArray()[i];
					es = es + " " + eff[i];
				}
				logger.finer(plan.getTrigger().getLiteral() + " Composite posteffects = " + es + ", for plan\n" + plan.toASString());
				final float cost = holder.agentUtils().getActionCount(plan.getBody(), false);
				if(pre.contains("not (flooded")) {
					final String pre1 = pre.replaceAll("not \\(flooded", "(dry");
					set.add(new ExternalCapability(plan.getTrigger().getLiteral(), goal, holder, 
							new ConfidencePrecondition(holder.getId(), getSignature(), pre1, conf),
							eff, cost, false));
					final String pre2 = pre.replaceAll("not \\(flooded", "(slippery");
					set.add(new ExternalCapability(plan.getTrigger().getLiteral(), goal, holder, 
							new ConfidencePrecondition(holder.getId(), getSignature(), pre2, conf*MoveRoad.SLIPPERY_MODIFIER),
							eff, cost, false));
				}
				else if(pre.contains("not flooded")) {
					set.add(new ExternalCapability(plan.getTrigger().getLiteral(), goal, holder, 
							new ConfidencePrecondition(holder.getId(), getSignature(), pre.replaceAll("not flooded", "dry"), conf),
							eff, cost, false));
					set.add(new ExternalCapability(plan.getTrigger().getLiteral(), goal, holder, 
							new ConfidencePrecondition(holder.getId(), getSignature(), pre.replaceAll("not flooded", "slippery"), conf*MoveRoad.SLIPPERY_MODIFIER),
							eff, cost, false));
				}
				else {
					set.add(new ExternalCapability(plan.getTrigger().getLiteral(), goal, holder,
							new ConfidencePrecondition(holder.getId(), getSignature(), pre, conf), eff, cost, false));
				}
			}// end forplans
		} // end forpre
		return set;
	}
}
