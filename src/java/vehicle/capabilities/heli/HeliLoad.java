package vehicle.capabilities.heli;

import jason.asSyntax.Literal;
import jason.bb.BeliefBase;
import vehicle.VehicleAgent;
import vehicle.capabilities.VehicleCapability;
import agent.model.capability.ConfidencePrecondition;
import common.UniversalConstants.WorldAction;

/**
 * 
 * @author alanwhite
 * Load cargo whilst landed at a location
 */
public class HeliLoad extends VehicleCapability {

	private static String[] defPre = {
		"helicopter(AGENT) & cargo(CARGO) & location(LJ) & atJ(AGENT, LJ) & cargoAt(CARGO, LJ) & not mortal(AGENT) & " +
		"not flying(AGENT) & not dangerZone(LJ) & not carryingCargo(AGENT)"}; 
	private static String[]	defEff = {"-cargoAt(CARGO, LJ)", "+loaded(AGENT, CARGO)", "+carryingCargo(AGENT)"};
	
	private final ConfidencePrecondition[] confPre;
	
	public HeliLoad(VehicleAgent holder) {
		super(	holder, 
				holder.getTaskFactory().factory(WorldAction.load.name()),
				Literal.parseLiteral("load(AGENT, LJ, CARGO)"), //cargo C at J
				defPre, defEff);
		confPre = new ConfidencePrecondition[]{
				new ConfidencePrecondition(holder.getId(),  sigLit,
				"helicopter(AGENT) & cargo(CARGO) & location(LJ) & atJ(AGENT, LJ) & cargoAt(CARGO, LJ) & healthy(AGENT) & " +
				"not flying(AGENT) & not dangerZone(LJ) & not carryingCargo(AGENT)", 
				1f),
				new ConfidencePrecondition(holder.getId(), sigLit,
				"helicopter(AGENT) & cargo(CARGO) & location(LJ) & atJ(AGENT, LJ) & cargoAt(CARGO, LJ) & damaged(AGENT) & " +
				"not flying(AGENT) & not dangerZone(LJ) & not carryingCargo(AGENT)", 
				DAMAGE_AGENT_MODIFIER)
		};
	}

	@Override
	public float getSpecificConfidence(BeliefBase bb, Literal action) {
		return getGeneralConfidence(bb);
	}

	public String[] getPreconditionsByConfConstraint(float minConf) {
		if(minConf > DAMAGE_AGENT_MODIFIER)	{
			return new String[]{ 	defPre[0].replaceAll("not mortal", "healthy")	};
		}
		else	{	return defPre;	}
	}

	@Override
	public ConfidencePrecondition[] getConfidenceBasedPreconditions(BeliefBase context) {
		return confPre;
	}
	
	
}
