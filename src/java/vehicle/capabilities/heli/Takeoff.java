package vehicle.capabilities.heli;

import jason.asSyntax.Literal;
import jason.bb.BeliefBase;
import vehicle.VehicleAgent;
import agent.model.capability.ConfidencePrecondition;
import common.UniversalConstants.WorldAction;

/**
 * 
 * @author Alan
 *
 * Become airbourne
 */
public class Takeoff extends GenericHeliCap {
	private static String signature = "takeOff(AGENT, START)";
	private static String[] defPre = {	"helicopter(AGENT) & atJ(AGENT, START) & not mortal(AGENT) & " +
										"not flying(AGENT) & location(START) & not dangerZone(START) & " + 
										"airport(START)"};
	private static String[] defEff = { "+overJ(AGENT, START)","+flying(AGENT)","-atJ(AGENT,START)"};
	private final ConfidencePrecondition[] confPre;
	
	public Takeoff(VehicleAgent holder) {
		super(	holder, 
				holder.getTaskFactory().factory(WorldAction.takeOff.name()), //goal
				Literal.parseLiteral(signature), //signature - agent, road,loc
				defPre, //preconditions
				defEff);//effects
		confPre = new ConfidencePrecondition[]{
				new ConfidencePrecondition(holder.getId(), sigLit,
						"helicopter(AGENT) & atJ(AGENT, START) & healthy(AGENT) & " +
						"not flying(AGENT) & location(START) & not dangerZone(START) & " + 
						"airport(START)", 1f),
				new ConfidencePrecondition(holder.getId(), sigLit,
						"helicopter(AGENT) & atJ(AGENT, START) & damaged(AGENT) & " +
						"not flying(AGENT) & location(START) & not dangerZone(START) & " + 
						"airport(START)", DAMAGE_AGENT_MODIFIER)
		};
	}

	@Override
	public float getSpecificConfidence(BeliefBase bb, Literal action) {
		return getGeneralConfidence(bb);
	}

	@Override
	public ConfidencePrecondition[] getConfidenceBasedPreconditions(BeliefBase context) {
		return confPre;
	}
}
