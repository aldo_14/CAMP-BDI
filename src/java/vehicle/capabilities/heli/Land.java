package vehicle.capabilities.heli;

import jason.asSyntax.Literal;
import jason.bb.BeliefBase;
import vehicle.VehicleAgent;
import agent.model.capability.ConfidencePrecondition;
import common.UniversalConstants.WorldAction;

/**
 * 
 * @author Alan
 *
 * Land at a suitable location
 */
public class Land extends GenericHeliCap {
	private static String signature = "land(AGENT, LOC)";
	private static String[] defPre = {"airport(LOC) & flying(AGENT) & overJ(AGENT, LOC) & not mortal(AGENT) & not dangerZone(LOC) & location(LOC) & helicopter(AGENT)"};
	private static String[] defEff = {"-flying(AGENT)", "+atJ(AGENT, LOC)", "-overJ(AGENT, LOC)"};
	private final ConfidencePrecondition[] confPre;
	
	public Land(VehicleAgent holder) {
		super(	holder, 
				holder.getTaskFactory().factory(WorldAction.land.name()), //goal
				Literal.parseLiteral(signature), //signature - agent, road,loc
				defPre, //preconditions
				defEff);//effects	
		confPre = new ConfidencePrecondition[]{
				new ConfidencePrecondition(holder.getId(), sigLit,
						"airport(LOC) & flying(AGENT) & overJ(AGENT, LOC) & healthy(AGENT) & not dangerZone(LOC) & location(LOC) & helicopter(AGENT)", 1f),
				new ConfidencePrecondition(holder.getId(), sigLit,
						"airport(LOC) & flying(AGENT) & overJ(AGENT, LOC) & damaged(AGENT) & not dangerZone(LOC) & location(LOC) & helicopter(AGENT)", 
						DAMAGE_AGENT_MODIFIER)
		};
	}

	@Override
	public float getSpecificConfidence(BeliefBase bb, Literal action) {
		return getGeneralConfidence(bb);
	}


	@Override
	public ConfidencePrecondition[] getConfidenceBasedPreconditions(BeliefBase context) {
		return confPre;
	}
}
