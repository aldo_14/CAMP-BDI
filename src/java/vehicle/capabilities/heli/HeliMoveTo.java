package vehicle.capabilities.heli;

import java.util.ArrayList;

import agent.beliefBase.WorldBeliefBase;
import agent.model.capability.Capability;
import agent.model.capability.CompositeCapability;
import agent.model.capability.ConfidencePrecondition;
import agent.model.goal.GoalTask;
import agent.type.CapabilityAwareAgent;
import jason.asSyntax.ASSyntax;
import jason.asSyntax.Literal;
import jason.asSyntax.Plan;
import jason.asSyntax.StringTerm;
import jason.bb.BeliefBase;
import truckworld.env.Percepts;

/**
 * 
 * @author alanwhite
 *
 * Special case for composite 'moveTo' plans, as they form the plan on-demand then invoke it,
 * meaning that we do not always expect there to be a plan.  Instead, we use the 'move' primitive
 * capability confidence as a basis for calculation.
 */
public class HeliMoveTo extends CompositeCapability {
	
	private static final Literal SIGNATURE = Literal.parseLiteral("moveTo(AGENT, ST, ED)");
	private static final String[] PRECONDITIONS = new String[]{
			"helicopter(AGENT) & location(ST) & location(ED) &  not busy(AGENT) & not mortal(AGENT) & atJ(AGENT, ST) & not atJ(AGENT, ED) & "
			+ "not flying(AGENT) & airport(ED) & airport(ST)", //only takeoff from airport
			"helicopter(AGENT) & location(ST) & location(ED) &  not busy(AGENT) & not mortal(AGENT) & oveRJ(AGENT, ST) & not atJ(AGENT, ED) & "
			+ "flying(AGENT) & airport(ED)"}; //don't need origin airport if hovering
	private static final String[] EFFECTS = new String[]{
		"-atJ(AGENT,ST)", "-flying(AGENT)", "-overJ(AGENT,ST)", "-overJ(AGENT,ED)", "+atJ(AGENT, ED)"};
	private static final GoalTask GOAL = new GoalTask(SIGNATURE, PRECONDITIONS, EFFECTS);

	public HeliMoveTo(CapabilityAwareAgent agent, GoalTask task, Plan plan){
		super(agent, GOAL, plan);
	}

	public HeliMoveTo(CapabilityAwareAgent agent, GoalTask task){
		super(agent, GOAL);
	}
	
	@Override
	public String[] getPreconditionsByConfConstraint(float confValue) {
		if(confValue <= (GenericHeliCap.WINDY_CONF_LOSS * GenericHeliCap.DAMAGE_AGENT_MODIFIER))	{
			//windy and damaged
			String[] precond = this.getPreconditions();
			String[] rt = new String[precond.length*2];
			for(int i=0; i<precond.length; i++)	{
				rt[i] = precond[i] + " & not damaged(AGENT)";
			}
			for(int i=precond.length; i<rt.length; i++)	{
				rt[i] = precond[i-3] + "& not " + Percepts.WINDY;
			}
			return rt;
		}
		else if( (confValue <= GenericHeliCap.WINDY_CONF_LOSS) || (confValue <= GenericHeliCap.DAMAGE_AGENT_MODIFIER))	{
			//windy OR damaged
			String[] precond = this.getPreconditions();
			String rt[] = new String[precond.length];
			for(int i=0; i<rt.length;i++)	{
				rt[i] = precond[i] + " & not damaged(AGENT) & not " + Percepts.WINDY;
			}
			return rt;
		}
		else	{	return getPreconditions();	}
	}
	
	//specialist confidence messages
	public float getGeneralConfidence(BeliefBase bb) {
		//could try brute force - average all routes?  Better just to use general confidence.  Better still to try and
		//use moveCapability confidence..
		if(!holder.hasCapability("fly"))	{
			logger.warning(holder.getId() + " does not have capability fly!");
			return 0; //cannot move
		}
		if(((WorldBeliefBase)bb).getAirports().isEmpty())	{
			return 0; //nowhere to go!
		}
		//else
		Capability mc = holder.getCapability("fly");
		return mc.getGeneralConfidence(bb);
	}

	/**
	 * Confidence is derived solely from 'move', due to this equating to a plan-on-demand operation.  When a plan is
	 * found, then we can get confidence from the IM layer with the move actions directly.
	 */
	public float getSpecificConfidence(BeliefBase bb, Literal action) {
		if(!holder.hasCapability("fly"))	{
			logger.severe(holder.getId() + "; Holder cannot move, return 0");
		}
		
		//basic precond check on immutable thingies
		final Literal flyingPercept = ASSyntax.createLiteral(Percepts.FLYING, 
				ASSyntax.createString(holder.getId()));
		final String originId = ((StringTerm)action.getTerm(1)).getString();
		final String destinationId = ((StringTerm)action.getTerm(2)).getString();
		if(bb.contains(flyingPercept)==null)	{
			if(!((WorldBeliefBase)bb).getAirports().contains(originId))	{
				return 0;
			}
			if(!((WorldBeliefBase)bb).getAirports().contains(destinationId))	{
				return 0;
			}
		}
		
		//skip clone, just add/remove quickly
		synchronized(bb)	{
			bb.add(flyingPercept);
			action = ASSyntax.createLiteral("fly", action.getTerm(0), action.getTerm(1), action.getTerm(2));
			float conf = holder.getCapability("fly").getSpecificConfidence(bb, action);
			bb.remove(flyingPercept);
			return conf;
		}
	}
	
	public ConfidencePrecondition[] getConfidenceBasedPreconditions(final BeliefBase context) {
		final String[] constP = getPreconditionsByConfConstraint(0.8f);
		final ArrayList<ConfidencePrecondition> newPres = new ArrayList<>();
		final String holder = getHolder().getId();
		final float base = getGeneralConfidence(context);
		//TODO
		//healthy ! windy
		//damaged ! windy
		//healthy windy
		//!damaged windy
		for(final String pre:constP) {
			newPres.add(new ConfidencePrecondition(holder, getSignature(), pre, base));
			newPres.add(new ConfidencePrecondition(holder, getSignature(), pre, base*GenericHeliCap.DAMAGE_AGENT_MODIFIER));
			newPres.add(new ConfidencePrecondition(holder, getSignature(), pre, base*GenericHeliCap.WINDY_CONF_LOSS));
			newPres.add(new ConfidencePrecondition(holder, getSignature(), pre, base*(GenericHeliCap.DAMAGE_AGENT_MODIFIER*GenericHeliCap.WINDY_CONF_LOSS)));
		}
		return newPres.toArray(new ConfidencePrecondition[newPres.size()]);
	}
}
