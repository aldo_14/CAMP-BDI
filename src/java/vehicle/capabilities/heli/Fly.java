package vehicle.capabilities.heli;

import jason.asSyntax.Literal;
import jason.bb.BeliefBase;
import vehicle.HeliAgent;
import agent.model.capability.ConfidencePrecondition;
import common.UniversalConstants.WorldAction;

/**
 * 
 * @author Alan
 *
 * Flies within a straight line from start to end
 * 
 * Note; we can fly between dangerZone areas, but not land there... 
 * 
 * However, we do specify the destination, as this is required to perform a landing action.
 */
public class Fly extends GenericHeliCap {
	private final static String signature = "fly(AGENT, START, END)";
	private final static String[] defPre = {
		"helicopter(AGENT) & overJ(AGENT, START) & not mortal(AGENT) & flying(AGENT) & location(START) & location(END)"};
	private final static String[] defEff = {"-overJ(AGENT, START)", "+overJ(AGENT, END)"};
	private final ConfidencePrecondition[] confPre;
	
	public Fly(HeliAgent holder) {
		super(	holder, 
				holder.getTaskFactory().factory(WorldAction.fly.name()), //goal
				Literal.parseLiteral(signature), //signature - agent, road,loc
				defPre, //preconditions
				defEff);//effects	
		confPre = new ConfidencePrecondition[]{
			new ConfidencePrecondition(holder.getId(), sigLit,
				"helicopter(AGENT) & overJ(AGENT, START) & healthy(AGENT) & not windy & flying(AGENT) & location(START) & location(END)",
				1f),
			//damaged, not windy
			new ConfidencePrecondition(holder.getId(), sigLit,
				"helicopter(AGENT) & overJ(AGENT, START) & damaged(AGENT) & not windy & flying(AGENT) & location(START) & location(END)",
				DAMAGE_AGENT_MODIFIER),	
			//windy
			new ConfidencePrecondition(holder.getId(), sigLit,
				"helicopter(AGENT) & overJ(AGENT, START) & healthy(AGENT) & windy & flying(AGENT) & location(START) & location(END)",
				WINDY_CONF_LOSS),
			//damaged AND windy
			new ConfidencePrecondition(holder.getId(), sigLit,
				"helicopter(AGENT) & overJ(AGENT, START) & damaged(AGENT) & windy & flying(AGENT) & location(START) & location(END)",
				DAMAGE_AGENT_MODIFIER*WINDY_CONF_LOSS)
		};
	}
	

	@Override
	public float getSpecificConfidence(BeliefBase bb, Literal action) {
		return getGeneralConfidence(bb);
	}
	
	/**
	 * Returns a hard-set value for flights, to indicate these are more expensive...
	 */
	@Override
	public float getGeneralCostEstimate()	{
		return 8; //flying is expensive!
	}

	@Override
	public ConfidencePrecondition[] getConfidenceBasedPreconditions(BeliefBase context) {
		return confPre;
	}
	

}
