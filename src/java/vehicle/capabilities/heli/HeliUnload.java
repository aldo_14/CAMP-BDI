package vehicle.capabilities.heli;

import jason.asSyntax.Literal;
import jason.bb.BeliefBase;
import vehicle.VehicleAgent;
import vehicle.capabilities.VehicleCapability;
import agent.model.capability.ConfidencePrecondition;
import common.UniversalConstants.WorldAction;

/**
 * 
 * @author alanwhite
 *
 * Unload cargo whilst loaded at a location
 */
public class HeliUnload extends VehicleCapability {

	private static String[]	defPre = {"helicopter(AGENT) & location(ULJ) & cargo(CARGO) & not mortal(AGENT) & atJ(AGENT, ULJ) & " +
	"loaded(AGENT, CARGO) & not flying(AGENT) & not dangerZone(ULJ) & carryingCargo(AGENT)"};
	private static String[]	defEff = {"+cargoAt(CARGO,ULJ)", "-loaded(AGENT, CARGO)", "-carryingCargo(AGENT)"};//, "-cargoNeeded(ULJ)"};
	
	private final ConfidencePrecondition[] confPre;
	
	public HeliUnload(VehicleAgent holder) {
		super(	holder, 
				holder.getTaskFactory().factory(WorldAction.unload.name()),
				Literal.parseLiteral("unload(AGENT, ULJ, CARGO)"), //cargo C at ULJ
				defPre, defEff);
		confPre = new ConfidencePrecondition[]{
				new ConfidencePrecondition(holder.getId(), sigLit,
						"helicopter(AGENT) & location(ULJ) & cargo(CARGO) & healthy(AGENT) & atJ(AGENT, ULJ) & " +
						"loaded(AGENT, CARGO) & not flying(AGENT) & not dangerZone(ULJ) & carryingCargo(AGENT)",
						1f),
				new ConfidencePrecondition(holder.getId(), sigLit,
						"helicopter(AGENT) & location(ULJ) & cargo(CARGO) & damaged(AGENT) & atJ(AGENT, ULJ) & " +
						"loaded(AGENT, CARGO) & not flying(AGENT) & not dangerZone(ULJ) & carryingCargo(AGENT)",
						DAMAGE_AGENT_MODIFIER)
				};
	}

	@Override
	public float getSpecificConfidence(BeliefBase bb, Literal action) {
		return getGeneralConfidence(bb);
	}

	public String[] getPreconditionsByConfConstraint(float minConf) {
		if(minConf > DAMAGE_AGENT_MODIFIER)	{
			return new String[]{ 	defPre[0].replaceAll("not mortal", "healthy")	};
		}
		else	{	return defPre;	}
	}

	@Override
	public ConfidencePrecondition[] getConfidenceBasedPreconditions(BeliefBase context) {
		return confPre;
	}
}
