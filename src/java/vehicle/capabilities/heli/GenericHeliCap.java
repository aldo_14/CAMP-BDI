package vehicle.capabilities.heli;

import java.util.Iterator;
import java.util.Vector;

import agent.model.goal.GoalTask;
import jason.asSyntax.Literal;
import jason.bb.BeliefBase;
import truckworld.env.Percepts;
import vehicle.VehicleAgent;
import vehicle.capabilities.VehicleCapability;
/**
 * Provides some generic flight stuff...
 * @author alanwhite
 *
 */
public abstract class GenericHeliCap extends VehicleCapability {
	
	protected GenericHeliCap(VehicleAgent holder, GoalTask task,
			Literal signature, String[] pre, String[] post) {
		super(holder, task, signature, pre, post);
	}

	public final static float WINDY_CONF_LOSS = 0.7f;
	
	public String[] getPreconditionsByConfConstraint(float confValue) {
		float damagedAndWindy = 1*DAMAGE_AGENT_MODIFIER*WINDY_CONF_LOSS;
		
		String[] noDam = this.getPreconditions();
		
		if(confValue > DAMAGE_AGENT_MODIFIER)	{
			for(int i=0; i<noDam.length; i++)	{
				noDam[i] = noDam[i].replaceAll("not mortal", "healthy");
			}
		}
		//bars damaged AND windy... i.e. 0.675 < conf 
		if(confValue < damagedAndWindy && confValue > WINDY_CONF_LOSS){
			for(int i=0; i<noDam.length; i++)	{
				//healthy and not windy
				noDam[i] = noDam[i] + " & not " + Percepts.WINDY;
			}
			return noDam;
		}
		
		//0.8 < conf
		//healthy (windy state doesn't matter)
		//OR
		//windy & healthy
		Vector<String> rt = new Vector<String>();
		//else, must be healthy & windy, or not windy & healthy
		if(confValue > damagedAndWindy && confValue < WINDY_CONF_LOSS)	{
			for(String s: noDam)	{
				rt.add(s); //must be healthy (wind state doesn't matter?)
			}
			 for(String s: this.getPreconditions())	{
				 rt.add(s + " & not " + Percepts.WINDY);
			}
			String[] precond = new String[rt.size()];
			for(int i = 0; i<precond.length; i++)	{	precond[i] = rt.get(i);	}
			return precond;
		}
		return noDam;
	}
	
	@Override
	public float getGeneralConfidence(BeliefBase bb) {
		float rt = super.getGeneralConfidence(bb);
		Iterator<Literal> windyChk = 
				bb.getCandidateBeliefs(Literal.parseLiteral(Percepts.WINDY), null);
		if(windyChk != null && windyChk.hasNext())	{	rt *= WINDY_CONF_LOSS;	}
		return rt;
	}
}
