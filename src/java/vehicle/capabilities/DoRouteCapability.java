package vehicle.capabilities;

import jason.asSyntax.Literal;
import jason.asSyntax.Plan;
import jason.bb.BeliefBase;
import agent.model.goal.GoalTask;
import agent.type.CapabilityAwareAgent;

/**
 * Special case for doRoute, as plans are inserted dynamically and thus we can get false-negatives with the
 * generic implementation; so we just use the GoalTask object as a basis for pres and effects, and assess general/
 * specific confidence using the moveTo capabilities checks for route existence.
 */
public class DoRouteCapability extends MoveToCapability {
	
	public DoRouteCapability(CapabilityAwareAgent agent, GoalTask task, Plan plan){
		super(agent, task, plan);
		preconditions = achieves().getPreconditions();
		addEffects = achieves().getAddEffects();
		delEffects = achieves().getDeleteEffects();
	}

	public DoRouteCapability(CapabilityAwareAgent agent, GoalTask task){
		super(agent, task);
		preconditions = achieves().getPreconditions();
		addEffects = achieves().getAddEffects();
		delEffects = achieves().getDeleteEffects();
	}
	
	@Override
	public String[] getPreconditions()	{	return achieves().getPreconditions();	}

	@Override
	public String[] getAddEffects()		{	return achieves().getAddEffects();	}

	@Override
	public String[] getDeleteEffects()	{	return achieves().getDeleteEffects();	}
	

	/**
	 * Special case, because we rely on runtime plan generation
	 */
	@Override
	public boolean preconditionsHold(BeliefBase bb, Literal action)	{
		return getSpecificConfidence(bb, action) > 0;
	}
}
