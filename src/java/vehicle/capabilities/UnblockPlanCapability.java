package vehicle.capabilities;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Vector;

import agent.beliefBase.WorldBeliefBase;
import agent.model.capability.CompositeCapability;
import agent.model.capability.ConfidencePrecondition;
import agent.model.capability.ExternalCapability;
import agent.model.goal.GoalTask;
import agent.type.CapabilityAwareAgent;
import common.UniversalConstants.RoadType;
import common.UniversalConstants.WorldAction;
import jason.asSyntax.Literal;
import jason.asSyntax.Plan;
import jason.asSyntax.StringTerm;
import jason.bb.BeliefBase;
import truckworld.world.interfaces.RoadInf;
import vehicle.capabilities.truck.MoveRoad;

/**
 * Special case for plans for logistics hq to perform bulldozing.  Required as the 
 * plan is unground until the internal action for selection is performed - i.e. until we have a defined bulldozer.
 */
public class UnblockPlanCapability extends CompositeCapability {
	
	public UnblockPlanCapability(CapabilityAwareAgent agent, GoalTask task, Plan plan){
		super(agent, task, plan);
		this.getConfidenceBasedPreconditions(agent.getBB());
	}

	public UnblockPlanCapability(CapabilityAwareAgent agent, GoalTask task){
		super(agent, task);
	}
	
	
	@Override
	public float getSpecificConfidence(final BeliefBase bb, final Literal action) {
		final String roadId = ((StringTerm) action.getTerm(1)).getString();
		final RoadInf unlockMe = ((WorldBeliefBase)bb).getRoad(roadId);
		if (unlockMe.isFlooded() || (unlockMe.getRoadType().equals(RoadType.mud)&&unlockMe.isSlippery())) {
			return 0;
		} else {
			return super.getSpecificConfidence(bb, action);
		}
	}
	
	//specialist confidence messages
	public float getGeneralConfidence(BeliefBase bb) {
		//get bulldozer confidences for 'clear'
		Collection<ExternalCapability> ubSet = holder.getExtCapability(WorldAction.unblock.name());
		
		if(ubSet.isEmpty())	{
			logger.severe("No bulldozers with unblock found; return 0");
			return 0;
		}
		
		float highConf = 0; //highest conf
		for(ExternalCapability unblock: ubSet)	{
			float unconf = unblock.getGeneralConfidence(bb);
			checkConfidence:
			if(unconf > highConf)	{
				if(unblock.getHolderName().equals(getHolder().getId())) {
					logger.severe("Possible recursion!");
					break checkConfidence;
				}
				
				final String nm = unblock.getHolderName();
				Collection<ExternalCapability> moves = holder.getExtCapability("moveTo", nm);
				if(moves.isEmpty() && !unblock.isPrimitive())	{
					logger.finer("agent " + nm + " cannot move... unblock configured as logical");
					if(unconf > highConf)	{
						highConf = unconf;
						logger.info("New highest conf is for log provider " + 
						unblock.getHolderName() + "; " + highConf);
					}
					break checkConfidence;//next cap
				}
				if(moves.isEmpty() && unblock.isPrimitive())	{
					//assuming is a composite?
					logger.severe("agent with PC, " + nm + " cannot move...");
					break checkConfidence;//next cap
				}
				
				//find the moveTo confidence for this agent...
				float highestMvConf = 0;
				for(ExternalCapability move:moves)	{
					float mgc = move.getGeneralConfidence(bb);
					if(mgc > highestMvConf)	{
						highestMvConf = mgc;	
					}
				}
				
				final float overallConf = (unconf > highestMvConf) ? highestMvConf : unconf;
				
				if(overallConf > highConf)	{
					highConf = overallConf;
					logger.info("New highest conf is for provider " + unblock.getHolderName() + "; " + highConf);
				}//end if
				
			}
		}
		return highConf; //best plan...
	}

	@Override
	public ConfidencePrecondition[] getConfidenceBasedPreconditions(final BeliefBase context) {
		final ArrayList<ConfidencePrecondition> preconds = new ArrayList<>();
		final float generalConfidence = this.getGeneralConfidence(context);
		for(final String originalPrecond : this.getPreconditions()) {
			if(originalPrecond.contains("not (flooded")) {
				final String pre1 = originalPrecond.replaceAll("not \\(flooded", "(dry");
				preconds.add(new ConfidencePrecondition(holder.getId(), getSignature(), pre1, generalConfidence));
				final String pre2 = originalPrecond.replaceAll("not \\(flooded", "(slippery");
				preconds.add(new ConfidencePrecondition(holder.getId(), getSignature(), pre2, generalConfidence*MoveRoad.SLIPPERY_MODIFIER));	
			}
			else if(originalPrecond.contains("not flooded")) {
				final String pre1 = originalPrecond.replaceAll("not flooded", "dry");
				preconds.add(new ConfidencePrecondition(holder.getId(), getSignature(), pre1, generalConfidence));
				final String pre2 = originalPrecond.replaceAll("not flooded", "slippery");
				preconds.add(new ConfidencePrecondition(holder.getId(), getSignature(), pre2, generalConfidence*MoveRoad.SLIPPERY_MODIFIER));	
			}
			else {
				preconds.add(new ConfidencePrecondition(holder.getId(), getSignature(), originalPrecond, generalConfidence));
			}
		}
		return preconds.toArray(new ConfidencePrecondition[preconds.size()]);
	}
	
	@Override
	public Collection<ExternalCapability> getConfidenceBasedPreconditionEcs(final BeliefBase bb) {
		final Collection<ExternalCapability> set = new Vector<>();
		GoalTask goal = achieves();
		float conf = getGeneralConfidence(bb);
		Map<String, Vector<Plan>> means = getPossibleMeans();
		for (final String pre : means.keySet()) {
			final Vector<Plan> plans = means.get(pre);
			for (final Plan plan : plans) {
				Collection<String> effSet = holder.agentUtils().getPostEffectsOfPlan(holder, plan.getBody());
				String[] eff = new String[effSet.size()];
				String es = "";
				for (int i = 0; i < eff.length; i++) {
					eff[i] = (String) effSet.toArray()[i];
					es = es + " " + eff[i];
				}
				logger.finer(plan.getTrigger().getLiteral() + " Composite posteffects = " + es + ", for plan\n" + plan.toASString());
				float cost = holder.agentUtils().getActionCount(plan.getBody(), false);
				if(pre.contains("not (flooded")) {
					final String pre1 = pre.replaceAll("not \\(flooded", "(dry");
					set.add(new ExternalCapability(plan.getTrigger().getLiteral(), goal, holder, 
							new ConfidencePrecondition(holder.getId(), getSignature(), pre1, conf),
							eff, cost, false));
					final String pre2 = pre.replaceAll("not \\(flooded", "(slippery");
					set.add(new ExternalCapability(plan.getTrigger().getLiteral(), goal, holder, 
							new ConfidencePrecondition(holder.getId(), getSignature(), pre2, conf*MoveRoad.SLIPPERY_MODIFIER),
							eff, cost, false));
				}
				else if(pre.contains("not flooded")) {
					set.add(new ExternalCapability(plan.getTrigger().getLiteral(), goal, holder, 
							new ConfidencePrecondition(holder.getId(), getSignature(),  
							pre.replaceAll("not flooded", "dry"), conf),
							eff, cost, false));
					set.add(new ExternalCapability(plan.getTrigger().getLiteral(), goal, holder, 
							new ConfidencePrecondition(holder.getId(), getSignature(), 
									pre.replaceAll("not flooded", "slippery"), conf*MoveRoad.SLIPPERY_MODIFIER),
							eff, cost, false));
				}
				else {
					set.add(new ExternalCapability(plan.getTrigger().getLiteral(), goal, holder,
							new ConfidencePrecondition(holder.getId(), getSignature(), pre, conf), eff, cost, false));
				}
			}// end forplans
		} // end forpre
		return set;
	}
}
