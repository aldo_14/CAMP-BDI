package vehicle.capabilities;

import jason.asSyntax.Literal;
import jason.asSyntax.Plan;
import jason.bb.BeliefBase;

import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;

import truckworld.env.Percepts;
import agent.model.capability.CompositeCapability;
import agent.model.capability.ExternalCapability;
import agent.model.goal.GoalTask;
import agent.type.CapabilityAwareAgent;

import common.UniversalConstants.WorldAction;

public class MHQSecureCapability extends CompositeCapability {
	private static final Literal RESTING_LITERAL = Literal.parseLiteral(Percepts.RESTING + "(A)");

	public MHQSecureCapability(CapabilityAwareAgent agent, GoalTask task, Plan plan){
		super(agent, task, plan);
	}
	
	public MHQSecureCapability(CapabilityAwareAgent agent, GoalTask task) {
		super(agent, task);
	}
	

	public float getGeneralConfidence(BeliefBase bb) {
		//check for unstuck (not resting), non-mortal APCs in the BB
		//get APC confidences for 'secureArea'
		Collection<ExternalCapability> ubSet = 
				holder.getExtCapability(WorldAction.secureArea.name());
		if(ubSet.isEmpty())	{
			logger.severe("No APCs with secureArea found; return 0");
			return 0;
		}
		
		//check if have APCs with > 1 confidence
		Vector<String> holders = new Vector<String>();
		for(ExternalCapability c: ubSet)	{
			if(c.getGeneralConfidence(bb)>0)	{		
				holders.add(c.getHolderName());	
			}
		}
		
		if(holders.isEmpty())	{	
			logger.severe("No secureArea capable agents with >0 conf found!");
			return 0;	
		}
		
		//now check the capable agents aren't resting-stuck (just in case this wasn't caught earlier...)
		Iterator<Literal> stuck = bb.getCandidateBeliefs(RESTING_LITERAL, null);
		Vector<String> stuckSet = new Vector<String>();
		if(stuck!=null)	{
			while(stuck.hasNext())	{
				Literal btL = stuck.next();
				stuckSet.add(btL.getTerm(0).toString().replaceAll("\"", ""));
			}
		}
		holders.removeAll(stuckSet);
		
		if(holders.isEmpty())	{
			logger.severe("No non-resting APCs available!");
			return 0;
		}
		return super.getGeneralConfidence(bb);
	}

}
