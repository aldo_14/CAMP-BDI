package vehicle.capabilities;

import jason.asSyntax.Literal;
import jason.bb.BeliefBase;
import agent.model.capability.ConfidencePrecondition;
import agent.model.capability.PrimitiveCapability;
import agent.type.CapabilityAwareAgent;

import common.UniversalConstants.WorldAction;

public class FreeAgentCapability extends PrimitiveCapability {

	private final static Literal signature = Literal.parseLiteral("free(AGENT, RID, R1, R2)");
	
	private final static String[] pre = {	"not resting(AGENT) & stuck(AGENT) & not toxic(R1, R2) & not blocked(R1, R2) & " +
											"onR(AGENT, RID) & road(RID, R1, R2) & location(R1) & location(R2) & " +
											"not mortal(AGENT)"};
	
	private final ConfidencePrecondition[] confPre;
	
	private final static String[] eff = {"-stuck(AGENT)"};
	
	public FreeAgentCapability(CapabilityAwareAgent holder) {
		super(holder, holder.getTaskFactory().factory(WorldAction.free.name()), signature, pre, eff);
		confPre = new ConfidencePrecondition[]{new ConfidencePrecondition(holder.getId(),signature, pre[0], 1)};	
	}
	
	/**
	 * Default implementation is to return 1 - hard for this to fail...
	 */
	@Override
	public float getGeneralConfidence(BeliefBase bb) {	return 1;	}
	
	/**
	 * Default implementation just returns the general confidence; see getGeneralConfidence
	 */
	@Override
	public float getSpecificConfidence(BeliefBase bb, Literal planAction) {
		if(preconditionsHold(bb, planAction))	{	return 1;	}
		else									{	return 0;	}
	}
	@Override
	public ConfidencePrecondition[] getConfidenceBasedPreconditions(BeliefBase context) {
		return confPre;
	}
}
