/**
 * 
 */
package vehicle;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.Map;

import agent.model.capability.CompositeCapability;
import agent.model.goal.GoalTask;
import common.UniversalConstants;
import common.UniversalConstants.HealthState;
import jason.asSyntax.ASSyntax;
import jason.asSyntax.Literal;
import jason.asSyntax.StringTerm;
import jason.asSyntax.VarTerm;
import truckworld.env.Percepts;
import truckworld.vehicle.Vehicle;
import vehicle.capabilities.DoRouteCapability;

/**
 * @author Alan White
 * 
 * 
 * A concrete agent instance for a 'vehicle' type agent.  Serves 2 purposes; to initialize the capabilities required, and
 * to provide some hooks for the WorldBeliefBase
 */
public abstract class VehicleAgent extends TruckworldAgent {

	/**
	 * When implemented, will return a representation of this vehicles 'physical' form for 
	 * use in route finding.
	 * @return new Vehicle with ID set to this agent
	 */
	public abstract Vehicle getVehicleObject();

	@Override
	protected Map<String, CompositeCapability> initCompositeCapabilities() {
		Map<String, CompositeCapability> formed = super.initCompositeCapabilities();
		if(!formed.containsKey("doRoute"))	{
			GoalTask g = getTaskFactory().factory("doRoute");
			DoRouteCapability c = new DoRouteCapability(this, g);
			this.manager.addAsExisting(c);
		}
		return formed;
	}
	
	protected final UniversalConstants.EntityType getVehicleType()	{
		return getVehicleObject().getType();
	}
	
	private boolean reg = false;
	
	/**
	 * Returns the BB literal corresponding to location
	 * @return
	 */
	public Literal getLocLit()	{
		StringTerm id = ASSyntax.createString(getId());
		VarTerm var = ASSyntax.createVar();

		//check registered - only do this until we KNOW the agent is registered, then skip it
		if(!reg)	{
			Iterator<Literal> il = bb.getCandidateBeliefs(
					ASSyntax.createLiteral(Percepts.AG_REGISTERED, id),  null);
			if(il==null)	{
				logger.severe("Not registered, rt null for getLoc");
				return null;
			}
			else	{	reg = true;	}
		}
		
		try {
			return getLocLitSubcall(id, var);
		}
		catch(final ConcurrentModificationException cme) {
			return getLocLitSubcall(id, var);
		}
	}

	private Literal getLocLitSubcall(StringTerm id, VarTerm var) {
		//try atJ first
		Iterator<Literal> il = bb.getCandidateBeliefs(
				ASSyntax.createLiteral(Percepts.AT_JUNCTION, id, var),  null);
		if(il!=null)	{
			while(il.hasNext())	{
				Literal l = il.next();
				String testid = l.getTerm(0).toString().replaceAll("\"", "");
				if(testid.equals(id.getString()))	{	return l;	}
			}
		}
		//try onR
		il = bb.getCandidateBeliefs(
				ASSyntax.createLiteral(Percepts.ON_ROAD, id, var),  null);
		if(il!=null)	{
			while(il.hasNext())	{
				Literal l = il.next();
				String testid = l.getTerm(0).toString().replaceAll("\"", "");
				if(testid.equals(id.getString()))	{	return l;	}
			}
		}
		logger.warning("No location found for " + getId() + " pending events; " + getTS().getC().getEvents());
		return null;
	}
	
	/**
	 * Checks the BB to fetch this agents health state
	 * @return
	 */
	public HealthState getHealth()	{
		StringTerm id = ASSyntax.createString(getId());
		Literal hLit = ASSyntax.createLiteral(HealthState.healthy.name(), id);
		Literal dLit = ASSyntax.createLiteral(HealthState.damaged.name(), id);
		Literal mLit = ASSyntax.createLiteral(HealthState.mortal.name(), id);
		if(getBB().contains(hLit) != null)	{	return HealthState.healthy;	}
		if(getBB().contains(dLit) != null)	{	return HealthState.damaged;	}
		if(getBB().contains(mLit) != null)	{	return HealthState.mortal;	}

		logger.severe("Cannot find my health?!");
		return HealthState.healthy;
	}
}
