package vehicle;

import truckworld.vehicle.Vehicle;
import truckworld.vehicle.type.APC;

public class ApcAgent extends VehicleAgent {

	@Override
	public Vehicle getVehicleObject() 		{	
		APC apc =  new APC(getId());
		apc.setHealth(getHealth());
		return apc;	
	}

}
