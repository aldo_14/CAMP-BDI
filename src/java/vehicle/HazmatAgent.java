package vehicle;

import truckworld.vehicle.Vehicle;
import truckworld.vehicle.type.Hazmat;

public class HazmatAgent extends VehicleAgent {

	@Override
	public Vehicle getVehicleObject() 		{	
		Hazmat hz =  new Hazmat(getId());
		hz.setHealth(getHealth());
		return hz;	
	}

}
