/**
 * 
 */
package vehicle.ia;

import jason.JasonException;
import jason.asSemantics.Agent;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.Atom;
import jason.asSyntax.Literal;
import jason.asSyntax.LogExpr;
import jason.asSyntax.LogicalFormula;
import jason.asSyntax.Plan;
import jason.asSyntax.Term;
import jason.asSyntax.Trigger.TEOperator;

import java.util.List;
import java.util.Stack;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import truckworld.env.Percepts;
import truckworld.world.interfaces.JunctionInf;
import truckworld.world.interfaces.RoadInf;
import vehicle.VehicleAgent;
import agent.beliefBase.WorldBeliefBase;
import agent.type.CapabilityAwareAgent;
import agent.type.MaintainingAgent;

import common.UniversalConstants;
import common.UniversalConstants.WorldAction;
import common.Utils;
import common.Utils.HeuristicsForGScore;
import common.Utils.HeuristicsForHScore;

/**
 * @author Alawhite
 *
 * Internal action to find and schedule a route.
 */
public class planRoute extends getLocOnRoad {

	private static final String DO_ROUTE_FUNCTOR = "doRoute";
	private static final long serialVersionUID = -7815607760683060585L;
	/*
	 * Percept generated for the purposes of linking route plan formation and execution
	 * capabilities... only 'seen' in internal BB estimation, not required as a literal percept
	 * from the environment.
	
	public static final String POST_PERCEPT_NAME = "hasRoutePlan"; */
	private Logger logger = Logger.getLogger(this.getClass().getSimpleName());
	private static int COUNTER = 0;
	private final static String AT_D_LABEL = "@doRouteEmpty";
	private final static Plan AT_DESTINATION = Plan.parse(AT_D_LABEL + "  " +
			"+!doRoute(AGENT, O, J): atJ(AGENT, O) & atJ(AGENT, J) <- " + //is also equiv to O == J
			".print(\"blank move;\", AGENT, \" already at destination\", J).");
	
	/**
	 * When executed, identifies a route to the stated destination and inserts the appropriate move
	 * actions into the agents intention
	 * @throws Exception if no route can be found
	 * @return object; boolean or an Iterator of Unifiers
	 * @see 	jason.asSemantics.DefaultInternalAction#execute(jason.asSemantics.TransitionSystem, 
	 * jason.asSemantics.Unifier, jason.asSyntax.Term[])
	 */
	@Override
	public synchronized Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
		try{
			logger.setLevel(UniversalConstants.INTERNAL_ACTION_LOGGER_LEVEL);
			logger.fine("PlanRoute; " + un.toString());//get base objects
			aId = ts.getUserAgArch().getAgName();
			if(logger.isLoggable(Level.FINER))	{
				for(int i=0; i<args.length; i++){	logger.fine("Arg[" + i + "]=" + args[i]);	}
			}

			if(!(ts.getAg().getBB() instanceof WorldBeliefBase)){
				logger.severe(ts.getUserAgArch().getAgName() + "; Planroute is using incompatible BB type");
				return false;
			}

			//args should be O (unbound), J (bound to a junction id)
			if(logger.isLoggable(Level.FINE))	{
				logger.fine("Unifier: " + un.toString());
				for(int i=0; i<args.length; i++){logger.fine("Arg " + (i+1) + "=" + args[i].toString());}
			}
			if(!args[1].isGround()){
				String argString = "";
				for(Term t: args)	{	argString = argString + " " + t;	}
				logger.severe("Destination argument (" + args[1] + ") is not ground! (args="+argString+")");
				return false;
			}
			
			synchronized(ts.getAg().getBB())	{	
				List<Literal> perceive = ts.getUserAgArch().perceive();
				if(perceive!=null)	{	ts.getAg().buf(perceive);	}
			}
			
			//presume - planRoute(fr, to)
			VehicleAgent v = (VehicleAgent)ts.getAg();
			logger.info("Vehicle = " + v.toString());	

			JunctionInf from;
			boolean onRoad = onRoad(aId, ts.getAg().getBB());
			RoadInf startOnRoad = null;
			WorldBeliefBase wbb = (WorldBeliefBase)v.getBB();
			JunctionInf to = wbb.getJunction(args[1].toString().replace("\"", ""));
			//getJunction(args[0].toString().replace("\"", ""));

			if(to == null)	{
				logger.severe(aId + ": Null 'to' for plan route op " + args[0] + ", " + args[1] );
			}
			
			/*
			 * new bit; if we are on a road, check which end of that road (junction) is closest to the destination
			 * in straight line terms.  Then, insert a move to that junction (i.e. from road to a junction) and use
			 * it as the 'from' point.
			 */
			if(onRoad)	{
				RoadInf r = getVehicleRoad(aId, ts.getAg().getBB());

				if(r == null)	{
					logger.severe(aId + ": Null road 'r' for plan route op " + args[0] + ", " + args[1] );
				}
				startOnRoad = r;
				//insert
				logger.fine("Agent " + ts.getAg().getTS().getUserAgArch().getAgName() + " is on road "+ r.getId());
				//distance...
				JunctionInf j1 =  r.getEnds()[0];
				JunctionInf j2 =  r.getEnds()[1];

				double disJ1 = j1.getCoordinate().distance(to.getCoordinate());
				double disJ2 = j2.getCoordinate().distance(to.getCoordinate());
				if(disJ1 < disJ2)	{	from = j1;	}
				else				{	from = j2;	}
			}
			else	{
				/*
				 * If we are NOT stuck, just use the vehicle location
				 */
				from = getVehicleLocation(aId, v.getBB());
			}
			if(from == null){
				logger.severe("Null error; \"from\"");
				return false;
			}

			if(to == null){
				logger.severe("Null error; \"to\"");
				return false;	
			}

			logger.fine("From = " + from + ", to " + to);

			//form route
			//note; perform each time in case environment conditions have changed...
			Stack<JunctionInf> route = null;
			
			if((ts.getAg() instanceof MaintainingAgent) && 
					(	((MaintainingAgent)ts.getAg()).isMaintaining()	||
						((MaintainingAgent)ts.getAg()).planConfConst()
					))	{
				route = Utils.getRoute(	from, to, wbb.getRoads(),
											v.getVehicleObject(), 
											HeuristicsForHScore.distanceOnly,
											HeuristicsForGScore.confidenceBased, false);//distanceOnly);
				if(route == null) {
					route = Utils.getRoute(	from, to, wbb.getRoads(),
							v.getVehicleObject(), 
							HeuristicsForHScore.distanceOnly,
							HeuristicsForGScore.confidenceBased, true);
				}
			}
			else	{
				route = Utils.getRoute(	from, to, wbb.getRoads(),
										v.getVehicleObject(), 
										HeuristicsForHScore.distanceOnly,
										HeuristicsForGScore.distanceOnly, true);
			}

			//transform into a sequence of 'move' actions
			if(route == null)	{
				//note - we can legally fail this
				String rds = "";
				for(RoadInf r: wbb.getRoads().values())	{	rds = rds + "\n" + r;	}
				logger.severe(wbb.getId() + ";PlanRoute; no route found between " + from + " and " + to  + rds + 
						"\nDZ=" + wbb.getDangerZones().toString());
				return false;
			}
			else if(route.isEmpty()  && startOnRoad==null && from.equals(to)){
				logger.info("PlanRoute; empty route (isEmpty = " + route.isEmpty() + 
						", startOnRoad = " + startOnRoad + ") between " + from + " and " + to);
				//form blank plan / add if required
				if(v.getPL().get(AT_D_LABEL)==null)	{	v.getPL().add(AT_DESTINATION);	}
				//set unifier
				boolean result = un.unifies(args[1], args[0]);
				if(!result)	{	logger.severe("Unification of " + args[1] + " to " + args[0] + " failed!");	}
				return result;
			}
			else	{
				logger.info("Got a route; " + route.toString());

				//take route and parse into a Plan object
				Plan plan = getPlanFromRoute(from, to, route, startOnRoad);

				logger.fine("Got a plan");
				
				logger.fine("Removing old plans...");
				Vector<Plan> remP = new Vector<Plan>();
				for(Plan p: v.getPL().getPlans())	{
					if(p.getTrigger().getLiteral().getFunctor().equals(planRoute.DO_ROUTE_FUNCTOR)
							&& p.getTrigger().getOperator().equals(TEOperator.add))	{ //i.e. not a fail-delete
						remP.add(p);
					}
				}

				for(Plan rm: remP)	{
					String lbl = rm.getLabel().getFunctor();
					if(v instanceof CapabilityAwareAgent)	{	((CapabilityAwareAgent)v).removePlan(lbl);	}
					else	{	v.getPL().remove(lbl);	}
				}
				String pls = "";
				for(Plan libP: v.getPL().getPlans())	{	
					pls = pls + "\n" + libP.toASString() + "(" + libP.getLabel().getFunctor() + ")";	
				}
				logger.fine("Removed old plans " + remP + "\nPLib=" + pls);
				
				//add new plan... can't iterate through list under modification, so need to copy/clone
				//into unmodifiable 'read' list for this... avoid ConcurrentModificationException
				
				logger.finer("Insert " + plan);
				insertIntoPlanLib(plan, ts);
				logger.fine("Inserted the plan");

		//		insertIntoPlanLib(getFailPlan(), ts);
		//		logger.fine("Inserted the failure plan");

				//i.e. plan found, didn't fail; route failures handled by move() actions pushed into plan
				Atom fromVal;
				//create unifier for 'O'
				if(startOnRoad!=null)	{	fromVal = new Atom("\""+startOnRoad.getId()+"\"");	}
				else					{	fromVal = new Atom("\""+from.getId()+"\"");			}
				logger.fine("Unifying " + fromVal + " for " + args[0] + ", uResult="+ un.unifies(fromVal, args[0]));

				logger.finest("Finished planning route");
				
				if(UniversalConstants.DEMO_MODE && UniversalConstants.DEBUG){
					JOptionPane.showMessageDialog(
							v.getUi(), 
							plan.toASString().split(";"), 
							v.getId() + " found route",
							JOptionPane.INFORMATION_MESSAGE);
				}

				return true;
			}//end else
		}catch(Exception e){
			String es = e.toString();
			for(StackTraceElement ste: e.getStackTrace()){	es = es + "\n" + ste;	}
			logger.severe(es);
			throw e;
		}
	}
	
	/**
	 * Takes a stack of junctions and uses these to form a Plan object
	 * @param from Origin junction
	 * @param to Destination junction
	 * @param route Stack of Junctions representing a road-to-road path from origin to destination
	 * @param startOnRoad if the starting position is on a road, this is !=null...
	 * @return a Plan object, ready to be inserted and called
	 */
	public Plan getPlanFromRoute(JunctionInf from, JunctionInf to, Stack<JunctionInf> route, RoadInf startOnRoad) {
		JunctionInf current = null;

		// Handle case where we're starting from a road and where one end is the destination
		if(route.isEmpty() && startOnRoad!=null && (
				startOnRoad.getEnds()[0].getId().equals(to.getId()) || 
				startOnRoad.getEnds()[1].getId().equals(to.getId())))	{
			current = to;
		}
		else	{	current = route.pop();	}

		String planBody= ".print(\"Route plan for " + from.getId() + " to " + to.getId() + "\")";

		/* 
		 * Stuck-on-road handling part
		 * check - are we at the first junction, or are we actually on a road?
		 */
		if(startOnRoad!=null){
			logger.finer("Road starting on road " + startOnRoad.getId() + "... first move to J " + current.getId());
			//insert move-on-road action to planBody
			JunctionInf start = startOnRoad.getEnds()[0];
			if(start.getId().equals(current.getId()))	{	start = startOnRoad.getEnds()[1];	}
			planBody = planBody + ";" + WorldAction.move.name().toLowerCase() + 
					"(\"" + aId + "\", " + 
					"\"" + startOnRoad.getId() + "\", " +
					"\"" + start.getId() + "\", " +//start at road... use other end as directionality indicator (easier to handle)
					"\"" + current.getId() + "\")";
		}

		while(!route.isEmpty())	{
			JunctionInf next = route.pop();
			logger.finest("Adding new move action; " + current.getId() + "->" + next.getId());

			//create term for 'move' action from current to next
			//move(agent id, road, from, to)
			String roadId =	current.getConnection(next);
			String action = WorldAction.move.name().toLowerCase() + 
					"(\"" + aId + "\", \"" + roadId + "\", \"" + current.getId() + "\", \"" + next.getId() + "\")";
			planBody = planBody + ";" + action;
			current = next; //for next loop round
		}
		String id = planRoute.DO_ROUTE_FUNCTOR + ++COUNTER;
		String label = "@" + id;// + "[atomic, source(self)]";

		planBody = 	planBody + ";.print(\"Done Route plan for " + from.getId() + 
				" to " + to.getId() + "\").";//;.remove_plan("+id+")";
				/* + //now remove percept to force replan on next iteration
				"-" + PlanRoute.POST_PERCEPT_NAME + "(\"" + fromP + "\", \"" + to.getId() +"\")";
				 */
		
		//write as full string i.e. +!x : true <- planBody
		String trigger= "+!doRoute(AGENT, O, J)";

		//precondition; at origin (road or junction) and NOT at destination
		String precondition = "";
		if(startOnRoad!=null)	{	
			precondition = Percepts.ON_ROAD + "(\"" + aId + "\", \"" + startOnRoad.getId() + "\")";
		}
		else	{	
			precondition = Percepts.AT_JUNCTION  + "(\"" + aId + "\", \"" + from.getId() + "\")";
		}
		precondition = "not mortal(\"" + aId + "\") & not atJ(\"" + aId + "\", " +
				"\"" + to.getId() + "\") & " + precondition;

		//parsecheck...
		LogicalFormula pre = LogExpr.parseExpr(precondition);

		if(pre == null)	{	logger.severe("Error parsing (" + pre + "), got null back");	}
		else			{	precondition = pre.toString();	}

		//actions
		String planString = label + " " + trigger + ":" + precondition + "<-" + planBody + ".";

		logger.fine("route plan= " + planString);

		//parse into plan lib
		Plan plan = Plan.parse(planString);
		return plan;
	}

	/**
	 * Adds the passed in plan to the agent plan lib (agent extracted from the TS' get method).
	 * @param transitionSystem
	 * @throws JasonException
	 */
	private void insertIntoPlanLib(Plan planToAdd, TransitionSystem transitionSystem )
			throws JasonException {
		Agent agent = transitionSystem.getAg();

		//add plan into the BB for execution
		if(agent instanceof CapabilityAwareAgent)	{	((CapabilityAwareAgent)agent).addPlan(planToAdd);	}
		else										{	agent.getPL().add(planToAdd);						}
		logger.info("Added plan " + planToAdd.toASString());
	}

	@Override
	public boolean suspendIntention(){return false;}//?

}
