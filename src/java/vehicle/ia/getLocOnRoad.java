package vehicle.ia;

import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.Atom;
import jason.asSyntax.Literal;
import jason.asSyntax.Term;
import jason.bb.BeliefBase;

import java.util.Iterator;
import java.util.logging.Logger;

import truckworld.env.Percepts;
import truckworld.world.interfaces.JunctionInf;
import truckworld.world.interfaces.RoadInf;
import agent.beliefBase.WorldBeliefBase;

import common.UniversalConstants;

/**
 * 
 * @author Alan White
 *
 * Determine the current location of the holding agent, either a road or junction
 */
public class getLocOnRoad extends DefaultInternalAction {
	protected String aId;
	private static final long serialVersionUID = -5595407704931311315L;
	private static Logger logger = Logger.getLogger(getLocOnRoad.class.getSimpleName());

	
	/**
	 * Determines the current location of the agent and uses it to set the value of args 0.  Returns false
	 * if agent is not of an appropriate type for this action.
	 */
	public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
		logger.setLevel(UniversalConstants.INTERNAL_ACTION_LOGGER_LEVEL);
		if(!(ts.getAg().getBB() instanceof WorldBeliefBase)){
			logger.severe(ts.getUserAgArch().getAgName() + "; incompatible BB type");
			return false;
		}
		aId = ts.getUserAgArch().getAgName();
		
		WorldBeliefBase wbb = (WorldBeliefBase)ts.getAg().getBB();
		Atom unAtom;
		if(onRoad(aId, wbb))	{
			RoadInf road = getVehicleRoad(aId, wbb);
			unAtom = new Atom("\""+road.getId()+"\"");
		}
		else	{
			JunctionInf jun = getVehicleLocation(aId, wbb);
			unAtom = new Atom("\""+jun.getId()+"\"");
		}
		
        return un.unifies(unAtom, args[0]);
    }
	


	/**
	 * @param bb - should be a WorldBeliefBase, otherwise null is returned
	 * @return JunctionInf
	 */
	public static JunctionInf getVehicleLocation(String aId,BeliefBase bb) {
		if(!(bb instanceof WorldBeliefBase))	{	return null;	}

		Iterator<Literal> il = bb.getCandidateBeliefs(
				Literal.parseLiteral(Percepts.AT_JUNCTION + "(\"" + aId + "\", J)"),  null);
		String jid = "";
		while(il.hasNext())	{
			Literal l = il.next();
			logger.finer("check L: " + l);
			String testid = l.getTerm(0).toString().replaceAll("\"", "");
			if(testid.equals(aId))	{	jid = l.getTerm(1).toString().replaceAll("\"","");	}
		}

		logger.info("id result: " + jid);
		return ((WorldBeliefBase)bb).getJunction(jid);
	}
	
	/**
	 * Returns the road this vehicle is on
	 * @param bb
	 * @param logger
	 * @return
	 */
	public static RoadInf getVehicleRoad(String aId,BeliefBase bb){
		if(!(bb instanceof WorldBeliefBase))	{	return null;	}

		Iterator<Literal> il = bb.getCandidateBeliefs(Literal.parseLiteral("onR(\"" + aId + "\", R)"),  null);
		String jid = "";
		while(il.hasNext())	{
			Literal l = il.next();
			String testid = l.getTerm(0).toString().replaceAll("\"", "");
			if(testid.equals(aId))	{	jid = l.getTerm(1).toString().replaceAll("\"","");	}
		}
		return ((WorldBeliefBase)bb).getRoad(jid);
	}
	
	/**
	 * Returns true if bb shows the agent is currently on a road
	 * @param bb
	 * @return true if the agent is on a road
	 */
	public static boolean onRoad(String aId, BeliefBase bb)	{
		Iterator<Literal> il = bb.getCandidateBeliefs(Literal.parseLiteral("onR(\"" + aId + "\", R)"),  null);
		if(il!=null)	{	
			while(il.hasNext())	{
				Literal l = il.next();
				if(l.getTerm(0).toString().replaceAll("\"", "").equals(aId))	{
					logger.finer("OnRoad true; " + l );
					return true;
				}
			}
		}
		logger.finer("OnRoad false: no matches found ib bb");
		return false;
	}

}
