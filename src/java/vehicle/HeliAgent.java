package vehicle;

import agent.maintenance.HeliMaintenancePolicy;
import agent.maintenance.MaintenancePolicy;
import truckworld.vehicle.Vehicle;
import truckworld.vehicle.type.Helicopter;

public class HeliAgent extends VehicleAgent {

	@Override
	public Vehicle getVehicleObject() {	
		Helicopter h = new Helicopter(getId());	
		h.setHealth(getHealth());
		return h;
	}

	public MaintenancePolicy getMaintenancePolicy(String functor){
		if(	functor.equalsIgnoreCase("fly") 	|| 
			functor.equalsIgnoreCase("takeOff")	||
			functor.equalsIgnoreCase("land")	||
			functor.equalsIgnoreCase("moveTo")		)	{
			return new HeliMaintenancePolicy();
		}
		else 	{
			return super.getMaintenancePolicy(functor);
		}
	}

}
