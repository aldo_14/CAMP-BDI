package vehicle;

import truckworld.vehicle.Vehicle;
import truckworld.vehicle.type.Crane;

import common.UniversalConstants.HealthState;

public class CraneAgent extends VehicleAgent {

	@Override
	public Vehicle getVehicleObject() {
		Crane crane =  new Crane(getId());
		crane.setHealth(HealthState.healthy);
		return crane;
	}
}
