/**
 * 
 */
package truckworld.vehicle;

import truckworld.vehicle.type.APC;
import truckworld.vehicle.type.Bulldozer;
import truckworld.vehicle.type.Crane;
import truckworld.vehicle.type.Hazmat;
import truckworld.vehicle.type.Helicopter;
import truckworld.vehicle.type.Truck;

import common.UniversalConstants.EntityType;

/**
 * @author Alan White
 *
 * Factory class for building vehicles
 */
public class VehicleFactory {
	
	/**
	 * Builds a new vehicle
	 * @param controllerName
	 * @return
	 */
	public Vehicle create(String controllerName){
		if(isOfType(controllerName,EntityType.truck)) {
			return new Truck(controllerName);
		}
		else if(isOfType(controllerName,EntityType.bulldozer)){
			return new Bulldozer(controllerName);
		}
		else if(isOfType(controllerName,EntityType.apc)){
			return new APC(controllerName);
		}
		else if(isOfType(controllerName,EntityType.helicopter)){
			return new Helicopter(controllerName);
		}
		else if(isOfType(controllerName,EntityType.hazmat)){
			return new Hazmat(controllerName);
		}
		else if(isOfType(controllerName, EntityType.crane)){
			return new Crane(controllerName);
		}
		else	{
			throw new RuntimeException("Type not found!! " + controllerName);
		}
	}

	private static boolean isOfType(String controllerName, EntityType type) {
		return controllerName.toLowerCase().startsWith(type.name().toLowerCase());
	}
}
