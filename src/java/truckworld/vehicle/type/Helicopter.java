package truckworld.vehicle.type;

import java.util.Vector;

import truckworld.env.Cargoworld;
import truckworld.vehicle.Vehicle;
import truckworld.world.interfaces.JunctionInf;
import truckworld.world.interfaces.RoadInf;

import common.UniversalConstants.EntityType;
import common.UniversalConstants.HealthState;
import common.UniversalConstants.WorldAction;

public class Helicopter extends Vehicle {
	private Vector<WorldAction> capableOf;
	private boolean flying = false;
	
	public Helicopter(String id) {
		super(id, EntityType.helicopter);
		capableOf = new Vector<WorldAction>();
		capableOf.add(WorldAction.fly);
		capableOf.add(WorldAction.load);
		capableOf.add(WorldAction.land);
		capableOf.add(WorldAction.takeOff);
		capableOf.add(WorldAction.unload);
		capableOf.add(WorldAction.secureArea);
	}

	@Override
	public float maxSpeed(RoadInf road) {
		return 0;
	}

	@Override
	public boolean canTravelAlong(RoadInf road) {
		return false;
	}

	@Override
	public boolean canTravelAlong(RoadInf road, JunctionInf j) {
		return false;
	}
	
	@Override
	public boolean isCapable(WorldAction action) {
		return capableOf.contains(action);
	}

	public void takeOff()		{	flying = true;	}
	public void land()			{	flying = false;	}
	public boolean isFlying()	{	return flying;	}

	public float flightSpeed() {	
		if(getHealth().equals(HealthState.healthy))	{
			return 2 * Cargoworld.SPEED_TIME_SCALAR;
		}
		else if(getHealth().equals(HealthState.damaged))	{
			return 1.5f * Cargoworld.SPEED_TIME_SCALAR;
		}
		else	{	
			//prevent infinite looping error... allow very slow movement 
			//in case we fail to catch this error
			return 0.5f * Cargoworld.SPEED_TIME_SCALAR;
		}
	}

}
