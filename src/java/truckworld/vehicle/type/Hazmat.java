package truckworld.vehicle.type;

import java.util.Vector;

import truckworld.env.Cargoworld;
import truckworld.vehicle.Vehicle;
import truckworld.world.Cargo;
import truckworld.world.interfaces.JunctionInf;
import truckworld.world.interfaces.RoadInf;

import common.UniversalConstants.EntityType;
import common.UniversalConstants.HealthState;
import common.UniversalConstants.RoadCondition;
import common.UniversalConstants.RoadType;
import common.UniversalConstants.WorldAction;

public class Hazmat extends Vehicle {
	private Vector<WorldAction> capableOf;
	
	public Hazmat(String id) {
		super(id, EntityType.hazmat);
		capableOf = new Vector<WorldAction>();
		capableOf.add(WorldAction.decontaminate);
		capableOf.add(WorldAction.move);
	}
	
	@Override
	public float maxSpeed(RoadInf road) {
		//see Truck code
		if(road.isBlocked() || road.isFlooded() || getHealth().equals(HealthState.mortal))	{	
			return 0;	
		}
		RoadType roadType = road.getRoadType();
		RoadCondition condition = road.getConditions();
		float valRet = 1f;
		if(roadType.equals(RoadType.mud))	{
			if(condition.equals(RoadCondition.dry)){
				valRet = 1.5f * Cargoworld.SPEED_TIME_SCALAR;
			}
			else if(condition.equals(RoadCondition.slippery)){
				valRet = 0f;
			}
			else if(condition.equals(RoadCondition.flooded)){
				valRet = 0f;
			}
			else valRet = -1f;
		}//main IF
		else if(roadType.equals(RoadType.tarmac)){
			if(condition.equals(RoadCondition.dry)){
				valRet = 2.10f * Cargoworld.SPEED_TIME_SCALAR;
			}
			else if(condition.equals(RoadCondition.slippery)){
				valRet = 0.4f * Cargoworld.SPEED_TIME_SCALAR;
			}
			else if(condition.equals(RoadCondition.flooded)){
				valRet = 0f;
			}
			else valRet = -1f;
		}//else - !
		else	{	valRet = -1f;	}
		
		//scale by health
		if(getHealth().equals(HealthState.damaged)){	valRet /= 2;	}
		return valRet;
	}

	@Override
	public boolean canTravelAlong(RoadInf road) {
		if( road.getEnds()[0].isDangerous() || road.getEnds()[1].isDangerous())	{
			return false;
		}
		
		return (maxSpeed(road)>0);
	}

	@Override
	public boolean canTravelAlong(RoadInf road, JunctionInf j) {	
		if(getLocationCoordinates().distance(j.getCoordinate())<0.5)	{
			return canTravelAlong(road);
		}
		
		if( j.isDangerous())	{	return false;	}
		return (maxSpeed(road)>0);
	}
	
	@Override
	public boolean canCarryCargo() {	return false;	}

	@Override
	public boolean loadCargo(Cargo c) {	return false;	}

	@Override
	public Cargo unloadCargo(String id) {	return null;	}

	@Override
	public boolean holdingCargo(String id) {	return false;	}

	@Override
	public boolean isCapable(WorldAction action) {	return capableOf.contains(action);	}
}
