package truckworld.vehicle.type;

import java.util.Vector;

import common.UniversalConstants.EntityType;
import common.UniversalConstants.HealthState;
import common.UniversalConstants.RoadCondition;
import common.UniversalConstants.WorldAction;
import truckworld.env.Cargoworld;
import truckworld.vehicle.Vehicle;
import truckworld.world.Cargo;
import truckworld.world.interfaces.JunctionInf;
import truckworld.world.interfaces.RoadInf;
import vehicle.capabilities.truck.MoveRoad;

/**
 * 
 * @author alan white
 * 
 * APCs (armoured personnel carriers) can secure road endpoints.  Travel is limited to
 * non-blocked roads, but can use toxic roads.
 */
public class APC extends Vehicle {
	private Vector<WorldAction> capableOf;
	
	public APC(String id) {
		super(id, EntityType.apc);
		capableOf = new Vector<WorldAction>();
		capableOf.add(WorldAction.move);
		capableOf.add(WorldAction.secureArea);
	}
	
	/**
	 * APCs can travel on any surface, so long as it isn't blocked or flooded
	 */
	@Override
	public float maxSpeed(RoadInf road) {
		if(road.isBlocked() ) 	{	return 0f;	}
		
		RoadCondition condition = road.getConditions();
		if(getHealth().equals(HealthState.mortal)){	return 0;	}

		float valRet = 1f;
		if(condition.equals(RoadCondition.dry)){
			valRet = 1.5f * Cargoworld.SPEED_TIME_SCALAR;
		}
		else if(condition.equals(RoadCondition.slippery)){
			valRet = 1f * Cargoworld.SPEED_TIME_SCALAR;
		}
		else if(condition.equals(RoadCondition.flooded)){
			return 0;
		}
		else valRet = -1f;
		
		//scale by health
		if(getHealth().equals(HealthState.damaged)){	valRet /= 2;	}
		return valRet;
	}

	@Override
	public boolean canTravelAlong(RoadInf road) {
		return maxSpeed(road) > 0; //can travel pretty much anywhere!
	}

	@Override
	public boolean canTravelAlong(RoadInf road, JunctionInf j) {
		//see above; don't worry about dzs at the end, we can handle these!
		return canTravelAlong(road);
	}
	
	@Override
	public boolean canCarryCargo() {	return false;	}

	@Override
	public boolean loadCargo(Cargo c) {	return false;	}

	@Override
	public Cargo unloadCargo(String id) {	return null;	}

	@Override
	public boolean holdingCargo(String id) {	return false;	}

	@Override
	public boolean isCapable(WorldAction action) {	return capableOf.contains(action);	}

	@Override
	public float confidenceScore(final RoadInf r)  {
		return r.isFlooded() ? 0 : (r.isSlippery() ? (1-MoveRoad.SLIPPERY_MODIFIER) : 1);
	}

	
}
