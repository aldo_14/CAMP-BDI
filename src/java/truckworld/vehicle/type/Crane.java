package truckworld.vehicle.type;

import java.util.Vector;

import common.UniversalConstants.EntityType;
import common.UniversalConstants.WorldAction;
import truckworld.vehicle.Vehicle;
import truckworld.world.interfaces.JunctionInf;
import truckworld.world.interfaces.RoadInf;

public class Crane extends Vehicle {

	private Vector<WorldAction> capableOf;
	
	public Crane(String id){
		super(id, EntityType.crane);
		capableOf = new Vector<WorldAction>();
		capableOf.add(WorldAction.load);
		capableOf.add(WorldAction.loadOnto);
		capableOf.add(WorldAction.unload);
		capableOf.add(WorldAction.unloadFrom );
	}
	
	@Override
	public float maxSpeed(RoadInf road) {	return 0;	}

	@Override
	public boolean canTravelAlong(RoadInf road) {	return false;	}

	@Override
	public boolean canTravelAlong(RoadInf road, JunctionInf j) {	return false;	}

	@Override
	public boolean isCapable(WorldAction action) {
		return capableOf.contains(action);
	}

}
