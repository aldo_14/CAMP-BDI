package truckworld.vehicle.type;

import java.util.Vector;

import truckworld.env.Cargoworld;
import truckworld.vehicle.Vehicle;
import truckworld.world.interfaces.JunctionInf;
import truckworld.world.interfaces.RoadInf;

import common.UniversalConstants.EntityType;
import common.UniversalConstants.HealthState;
import common.UniversalConstants.RoadCondition;
import common.UniversalConstants.RoadType;
import common.UniversalConstants.WorldAction;

public class Truck extends Vehicle {

	private Vector<WorldAction> capableOf;
	public Truck(String id){
		super(id, EntityType.truck);
		capableOf = new Vector<WorldAction>();
		capableOf.add(WorldAction.load);
		capableOf.add(WorldAction.unload);
		capableOf.add(WorldAction.move);
	}

	/**
	 * @see truckworld.vehicle.Vehicle#isCapable(common.UniversalConstants.WorldAction)
	 */
	@Override
	public boolean isCapable(WorldAction action) {	return capableOf.contains(action);	}

	/**
	 * Returns maximum speed in distance units per second; simulator
	 * should scale down accordingly
	 */
	private float maxSpeed(RoadType roadType, RoadCondition condition) {
		if(getHealth().equals(HealthState.mortal) || condition.equals(RoadCondition.flooded))	{	
			return 0;	
		}
		
		float valRet = 1f;
		if(condition.equals(RoadCondition.dry)) {
			valRet = Cargoworld.SPEED_TIME_SCALAR * (roadType.equals(RoadType.tarmac)? 2f : 1.5f);
		}
		else if(condition.equals(RoadCondition.slippery)) {
			valRet = roadType.equals(RoadType.tarmac)? (0.4f * Cargoworld.SPEED_TIME_SCALAR) : 0;
		}
		
		return getHealth().equals(HealthState.damaged) ? valRet/2 : valRet;
	}

	/**
	 * @see truckworld.vehicle.Vehicle#canTravelAlong(truckworld.world.interfaces.RoadInf)
	 */
	@Override
	public boolean canTravelAlong(RoadInf road) {
		if(	road.isBlocked() || road.isToxic() || road.getConditions().equals(RoadCondition.flooded) ||
			road.getEnds()[0].isDangerous() || road.getEnds()[1].isDangerous())	{	
			logger.info(road.getId() + " is impassable: blocked? " + road.isBlocked() + ", toxic? " + road.isToxic() + 
					", DZ at " + road.getEndIds()[0] + "? " + road.getEnds()[0].isDangerous() + 
					", DZ at " + road.getEndIds()[1] + "? " + road.getEnds()[1].isDangerous());
			return false;	
		}
		else	{
			return (maxSpeed(road)>0);
		}
	}

	@Override
	public boolean canTravelAlong(RoadInf road, JunctionInf j) {	
		return	(getLocationCoordinates().distance(j.getCoordinate())<0.00001) ? 
				canTravelAlong(road) :
				(!road.isBlocked() && !road.isToxic() && (maxSpeed(road)>0) && !j.isDangerous());
	}
	
	/**
	 * @see truckworld.vehicle.Vehicle#maxSpeed(truckworld.world.interfaces.RoadInf)
	 */
	@Override
	public float maxSpeed(RoadInf road) {
		return (road.isBlocked() || road.isToxic())? 0f :  maxSpeed(road.getRoadType(), road.getConditions());
	}

}
