/**
 * 
 */
package truckworld.vehicle.type;

import java.util.Vector;

import truckworld.env.Cargoworld;
import truckworld.vehicle.Vehicle;
import truckworld.world.Cargo;
import truckworld.world.interfaces.JunctionInf;
import truckworld.world.interfaces.RoadInf;

import common.UniversalConstants.EntityType;
import common.UniversalConstants.HealthState;
import common.UniversalConstants.RoadCondition;
import common.UniversalConstants.RoadType;
import common.UniversalConstants.WorldAction;

/**
 * @author Alawhite
 *
 */
public class Bulldozer extends Vehicle {

	private Vector<WorldAction> capableOf;
	
	public Bulldozer(String id){
		super(id, EntityType.bulldozer);
		capableOf = new Vector<WorldAction>();
		capableOf.add(WorldAction.unblock);
		capableOf.add(WorldAction.move);
	}

	/**
	 * @see truckworld.vehicle.Vehicle#isCapable(common.UniversalConstants.WorldAction)
	 */
	@Override
	public boolean isCapable(WorldAction action) {	return capableOf.contains(action);	}

	/**
	 * Returns maximum speed in distance units per second; simulator
	 * should scale down accordingly
	 * @param roadType
	 * @param condition
	 * @return
	 */
	protected float maxSpeed(RoadType roadType, RoadCondition condition) {
		if(getHealth().equals(HealthState.mortal)){	return 0;	}

		float valRet = (float) (Cargoworld.SPEED_TIME_SCALAR * 0.5); //bulldozers are slow...
		if(roadType.equals(RoadType.mud))	{
			if(condition.equals(RoadCondition.dry)){
				valRet = 1.5f * valRet;
			}
			else if(condition.equals(RoadCondition.slippery)){
				valRet = 0f;
			}
			else if(condition.equals(RoadCondition.flooded)){
				valRet = 0f;
			}
			else valRet = -1f;
		}//main IF
		else if(roadType.equals(RoadType.tarmac)){
			if(condition.equals(RoadCondition.dry)){
				valRet = 2.10f * valRet;
			}
			else if(condition.equals(RoadCondition.slippery)){
				valRet = 0.4f * valRet;
			}
			else if(condition.equals(RoadCondition.flooded)){
				valRet = 0f;
			}
			else valRet = -1f;
		}//else - !
		else	{	valRet = -1f;	}
		
		//scale by health
		if(getHealth().equals(HealthState.damaged)){	valRet /= 2;	}
		return valRet;
	}

	/**
	 * @see truckworld.vehicle.Vehicle#canTravelAlong(truckworld.world.interfaces.RoadInf)
	 * Note; we can't use roads without clearing them first
	 */
	@Override
	public boolean canTravelAlong(RoadInf road) {	
		return( road.isToxic() || road.getEnds()[0].isDangerous() || road.getEnds()[1].isDangerous() || road.isBlocked()) ? 
				false : maxSpeed(road)>0;	
	}

	@Override
	public boolean canTravelAlong(RoadInf road, JunctionInf j) {	
		return(getLocationCoordinates().distance(j.getCoordinate())<0.5) ? canTravelAlong(road) : 
		 !( road.isToxic() || j.isDangerous() || (maxSpeed(road)<=0) || road.isBlocked());
	}	
	
	/**
	 * @see truckworld.vehicle.Vehicle#maxSpeed(truckworld.world.interfaces.RoadInf)
	 */
	@Override
	public float maxSpeed(RoadInf road) {	
		if(road.isToxic())	{	return 0f;	}
		return maxSpeed(road.getRoadType(), road.getConditions());	
	}


	@Override
	public boolean canCarryCargo() 			{	return false;	}
	@Override
	public boolean loadCargo(Cargo c) 		{	return false;	}
	@Override
	public Cargo unloadCargo(String id) 	{	return null;	}
	@Override
	public boolean holdingCargo(String id) 	{	return false;	}

}
