package truckworld.vehicle;

import java.awt.geom.Point2D;
import java.util.logging.Level;

import truckworld.world.Cargo;
import truckworld.world.interfaces.Actor;
import truckworld.world.interfaces.JunctionInf;
import truckworld.world.interfaces.RoadInf;
import vehicle.capabilities.truck.MoveRoad;
import common.UniversalConstants.EntityType;
import common.UniversalConstants.HealthState;
import common.UniversalConstants.RoadType;

/**
 * @author Alan White
 * 
 * Defines a vehicle that travels along roads
 */
public abstract class Vehicle extends Actor {	
	
	/**
	 * Current world location, can be correlated against roads and junctions
	 */
	private Point2D currentWorldLocation;
	
	/**
	 * Health state
	 */
	private HealthState health;
	
	/**
	 * Cargo object
	 */
	private Cargo c;
	
	/**
	 * Default constructor
	 */
	public Vehicle(String id, EntityType type)	{
		super(id, type);
		this.health = HealthState.healthy;
		c = null;
	}
	
	/**
	 * Returns the current <i>exact</i> location of the vehicle in 2d space co-ords
	 * @return point2D
	 */
	public Point2D getLocationCoordinates()	{	return currentWorldLocation;	}
	
	/**
	 * Sets the precise location of this vehicle.  Note; does not check if this is on
	 * a road, etc.
	 * @param location
	 * @return
	 */
	public void setLocation(Point2D location){		
		logger.finer("Set location: " + location);
		StackTraceElement[] stack = new Exception().getStackTrace();
		
		if(logger.isLoggable(Level.FINEST))	{
			for(int i=0; i<stack.length; i++){	logger.finest(stack[i].toString());}
		}
		
		this.currentWorldLocation = location;	
	}
	
	/**
	 * Sets the agent health state
	 * @param hs
	 * @see HealthState
	 */
	public void setHealth(HealthState hs) { health = hs; }
	
	/**
	 * Returns a health state based on the HealthState enum
	 * @return
	 */
	public HealthState getHealth() { return health; }
	
	/** 
	 * Returns the speed the vehicle can travel along a road.  A zero value
	 * indicates the vehicle cannot travel along that road - for example, a 
	 * heavy truck cannot use a flooded tarmac road or a mud road that is not 
	 * dry.
	 * @return float speed value
	 */
	public abstract float maxSpeed(RoadInf road);
	
	/**
	 * Return true if agent can use this specific road.  Relates to max speed 
	 * calculation, but also includes beliefs about whether the road is clear,
	 * etc.  Note that a road may change condition during transit, potentially
	 * stranding vehicles.
	 * @param road
	 * @return boolean (maxSpeed > 0) && !isBlocked etc
	 */
	public abstract boolean canTravelAlong(RoadInf road);
	
	/**
	 * Return true if we can use this specific road, in the case where we are already
	 * travelling down it, towards the end j.  To handle cases where changes at the 
	 * origin junction prevent travel from there, but not travel when already present
	 * @param road
	 * @param endJ
	 * @return
	 */
	public abstract boolean canTravelAlong(RoadInf road, JunctionInf j);
	
	/**
	 * Return true if we can carry this piece of cargo
	 * @param c
	 * @return
	 */
	public boolean canCarryCargo()	{	return c==null;	}
	
	/**
	 * Load piece of cargo
	 * @return false if unable to carry
	 */
	public boolean loadCargo(Cargo newC)	{
		if(c!=null)	{	return false;	}
		else	{
			c = newC;
			return true;
		}
	}
	
	/**
	 * Unload Cargo with the given ID at this location; used in case we allow
	 * vehicle to carry multiple pieces of cargo.
	 * @param id
	 * @return unloaded cargo object...
	 */
	public Cargo unloadCargo(String id)	{
		if(!holdingCargo(id))	{	return null;	}
		else	{
			Cargo rt = c;
			c=null;
			return rt;
		}
	}

	/**
	 * Checks if this agent is carrying cargo with the given id
	 * @param id
	 * @return
	 */
	public boolean holdingCargo(String id)	{
		return (c!=null) && (c.getName().equals(id));
	}
	
	/**
	 * Get all cargo held by this vehicle
	 * @return
	 */
	public Cargo getCargo()	{	return c;	}
	
	public String toString(){
		return this.getId() + "@" + getLocationCoordinates() + ":" + this.getHealth().name();
	}

	public float confidenceScore(final RoadInf r) {
		//check conditions / surface combo - we don't count blocked/dz/toxic conditions as these
		//are (theoretically) preconditions that can be restored by other activities
		if(r.isFlooded()) {
			return 0;	
		}
		else if(r.isSlippery()) {
			return r.getRoadType().equals(RoadType.tarmac) ? 1-MoveRoad.SLIPPERY_MODIFIER : 0;
		}
		return 1;
	}


}
