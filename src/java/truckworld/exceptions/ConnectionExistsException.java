/**
 * 
 */
package truckworld.exceptions;

import truckworld.world.interfaces.RoadInf;


/**
 * @author Alan White
 * This exception is thrown if a road is connected to a junction that already has
 * a connection to the road endpoint, and the properties are not identical - i.e.
 *  if two <i>different</i> roads exist for the same node-node connection.
 */
public class ConnectionExistsException extends Exception {
	private static final long serialVersionUID = 3352704173833256066L;
	private String source;
	private String existing;
	private RoadInf added;

	public ConnectionExistsException(String source, String string, RoadInf added){
		super();
		this.source = source;
		this.existing = string;
		this.added = added;
	}
	
	public String getExisting() {
		return existing;
	}

	public RoadInf getAdded() {
		return added;
	}
	
	public String toString(){
		return source + " Had [" + existing.toString() + "], tried to add [" + added.toString() + "]";
	}
}
