/**
 * 
 */
package truckworld.exceptions;

/**
 * @author Alan White
 * Thrown if an error occurs during generation of a truckworld
 */
public class WorldGenException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2308462167685155503L;

	/**
	 * @param string
	 */
	public WorldGenException(String string) {
		super(string);
	}

}
