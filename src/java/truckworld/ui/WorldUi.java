package truckworld.ui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;

import common.UniversalConstants;
import common.UniversalConstants.EntityType;
import common.UniversalConstants.HealthState;
import common.UniversalConstants.RoadType;
import common.UniversalConstants.WorldAction;
import jason.asSyntax.Literal;
import truckworld.env.Cargoworld;
import truckworld.vehicle.Vehicle;
import truckworld.world.Cargo;
import truckworld.world.interfaces.JunctionInf;
import truckworld.world.interfaces.RoadInf;
import truckworld.world.interfaces.SettlementInf;
import truckworld.world.simulatedConditions.DangerZone;
import truckworld.world.simulatedConditions.EnvStepper;
import truckworld.world.simulatedConditions.Watchdog;
import truckworld.world.simulatedConditions.rain.RainController;
import truckworld.world.simulatedConditions.rain.RainStorm;

/**
 * @author Alan White
 *
 *         Displays the world geography et al
 */
public class WorldUi extends JFrame implements WindowListener, ActionListener {
	/**
	 * Real pixel draw width for vehicles
	 */
	private static final int VEHICLE_DRAW_WIDTH = 12;

	/**
	 * Real pixel draw width for nodes
	 */
	private static final int NODE_DRAW_WIDTH = 15;

	/**
	 * Real pixel draw width for settlement aiport, seaport etc icons
	 */
	private static final int DECORATOR_DRAW_WIDTH = 20;

	/**
	 * Road line thickness in real pixels
	 */
	protected static final float LINE_THICKNESS = 2.5f;

	private static final long serialVersionUID = 6120697386199322516L;

	private final BasicStroke dash = new BasicStroke(1.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f,
			new float[] { 3.0f }, 0.0f);
	private final BasicStroke dash2 = new BasicStroke(1.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND, 10.0f,
			new float[] { 2.0f, 2.0f }, 20.0f);

	/**
	 * Controls repaint rate (frames per second/ redraw ever 1000/FPS ms)
	 */
	protected static final int FPS = 100;

	/**
	 * Draw colours
	 */
	protected Color VEHICLE_COLOUR = new Color(255, 255, 255), VEHICLE_HEALTHY_COLOUR = Color.GREEN,
			VEHICLE_DAMAGED_COLOUR = Color.ORANGE, VEHICLE_MORTAL_COLOUR = Color.RED,
			CARGO_HZ_COLOUR = new Color(0, 255, 30), CARGO_COLOUR = new Color(150, 0, 30), BLOCKED_COLOUR = Color.RED,
			DRY_TARMAC_COLOUR = Color.GRAY, SLIPPERY_TARMAC_COLOUR = new Color(120, 120, 255),
			FLOODED_TARMAC_COLOUR = new Color(0, 0, 150), DRY_MUD_COLOUR = new Color(240, 170, 30),
			SLIPPERY_MUD_COLOUR = new Color(170, 120, 85), FLOODED_MUD_COLOUR = new Color(60, 40, 150),
			RAINSTORM_COLOUR_1 = new Color(160, 225, 225, 100), DEBUG_BB_COLOUR = new Color(255, 0, 0, 100),
			DEBUG_LINE_COLOUR = new Color(0, 255, 0, 100), DEBUG_LINE_COLOUR_HIGHLIGHT_1 = new Color(255, 20, 20),
			DEBUG_LINE_COLOUR_HIGHLIGHT_2 = new Color(200, 50, 50), NODE_COLOUR = Color.YELLOW,
			CLOSED_TEXT_COLOUR = new Color(100, 0, 0), TOXIC_TEXT_COLOUR = new Color(220, 30, 170),
			TEXT_COLOUR = new Color(105, 20, 20), TEXT_BG_COLOUR = new Color(250, 250, 250, 200),
			TEXT_COLOUR_2 = new Color(0, 50, 0), TEXT_BG_COLOUR_2 = new Color(200, 200, 200, 200),
			NODE_TEXT_COLOUR = new Color(10, 10, 50), DZ_COLOUR = new Color(220, 0, 0, 60),
			DZ_NODE_COLOUR = new Color(220, 0, 0), ATTACK_COLOUR = new Color(255, 190, 0, 150),
			TOXIC_COLOUR = new Color(220, 30, 170), STORM_TEXT_COLOUR = Color.BLACK,
			BACKGROUND = new Color(255, 255, 255), DEBUG_ATTACK_BB_COLOUR = Color.red,
			DEBUG_ATTACK_TXT_COLOUR = Color.black, VEHICLE_TEXT_COLOUR = Color.black,
			VEHICLE_TEXT_BG_COLOUR = new Color(240, 240, 240, 180), HELI_FLIGHTPATH_COLOUR = new Color(0, 0, 50),
			LAST_DELIVER = Color.black, CARGO_TARGET = new Color(0, 150, 0);

	private static BufferedImage AIRPORT_IMG, REPAIR_IMG;

	/**
	 * When set to true, show additional information regarding the environment,
	 * such as tracking which roads are affected by storms
	 */
	protected boolean debug = false;
	private boolean showKey = false; // show color key for road labels?

	private Cargoworld env;

	/**
	 * Sub-window to show key for color meaning
	 */
	private JDialog keyWin;

	// menu items
	private JMenuItem envDebugToggle, agentDebugToggle, resultToggle, detailObToggle, keyToggle, demoToggle, kickoffSim,
			shutdown, reset, locStringToggle, vehStringToggle, infoStringToggle;

	private Vector<String> knownV = new Vector<String>();

	private JMenu agents, addCargo, addCargoRequest, addDz,
			removeDz/* , macros */;

	private boolean drawLocationString = true, drawVehicleString = true, drawInfoString = true;

	// decimal formatting - 1 place only...
	private DecimalFormat df = new DecimalFormat("#.#");

	private long start = 0;

	public WorldUi(final Cargoworld e, final String title) {
		super();

		loadImageIcons(e);

		env = e;
		if(UniversalConstants.AUTO_START_SIM) {
			createAutoStartThread();
		}
		
		final JMenuBar mb = new JMenuBar();
		JMenu options = new JMenu("Debug/options");

		kickoffSim = new JMenuItem("Start environmental conditions sim");
		kickoffSim.addActionListener(this);
		kickoffSim.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK));
		options.add(kickoffSim);

		envDebugToggle = new JMenuItem("Toggle Environment Debug Display");
		envDebugToggle.addActionListener(this);
		options.add(envDebugToggle);

		keyToggle = new JMenuItem("Toggle road colour key");
		keyToggle.addActionListener(this);
		options.add(keyToggle);

		agentDebugToggle = new JMenuItem("Toggle DEBUG mode");
		agentDebugToggle.addActionListener(this);
		agentDebugToggle.setAccelerator(KeyStroke.getKeyStroke("F1"));
		options.add(agentDebugToggle);

		demoToggle = new JMenuItem("Toggle DEMO mode");
		demoToggle.addActionListener(this);
		demoToggle.setAccelerator(KeyStroke.getKeyStroke("F2"));
		options.add(demoToggle);

		resultToggle = new JMenuItem("Toggle highlight results");
		resultToggle.addActionListener(this);
		resultToggle.setAccelerator(KeyStroke.getKeyStroke("F3"));
		options.add(resultToggle);

		detailObToggle = new JMenuItem("Toggle Detailed Obligation dialog");
		detailObToggle.addActionListener(this);
		detailObToggle.setAccelerator(KeyStroke.getKeyStroke("F4"));
		options.add(detailObToggle);

		locStringToggle = new JMenuItem("Toggle location strings");
		locStringToggle.addActionListener(this);
		locStringToggle.setAccelerator(KeyStroke.getKeyStroke("F10"));
		options.add(locStringToggle);

		vehStringToggle = new JMenuItem("Toggle vehicle strings");
		vehStringToggle.addActionListener(this);
		vehStringToggle.setAccelerator(KeyStroke.getKeyStroke("F11"));
		options.add(vehStringToggle);

		infoStringToggle = new JMenuItem("Toggle info strings");
		infoStringToggle.addActionListener(this);
		infoStringToggle.setAccelerator(KeyStroke.getKeyStroke("F12"));
		options.add(infoStringToggle);

		reset = new JMenuItem("Reset environment to optimum state");
		reset.addActionListener(this);
		reset.setAccelerator(KeyStroke.getKeyStroke("ctrl R"));
		options.add(reset);

		shutdown = new JMenuItem("Shutdown MAS");
		shutdown.addActionListener(this);
		shutdown.setAccelerator(KeyStroke.getKeyStroke("alt F4"));
		options.add(shutdown);

		mb.add(options);

		agents = new JMenu("Vehicles");
		mb.add(agents);

		// add existing vehicles
		for (String vName : env.getVehicles().keySet()) {
			addVehicle(vName);
		}

		addCargo = new JMenu("Add cargo");
		addCargoRequest = new JMenu("Add cargo demand");
		List<JunctionInf> js = new Vector<JunctionInf>();
		js.addAll(env.getWorld().getAllJunctions());
		// sort
		Collections.sort(js, new Comparator<JunctionInf>() {
			@Override
			public int compare(JunctionInf o1, JunctionInf o2) {
				int index1 = Integer.parseInt(o1.getId().substring(1));
				int index2 = Integer.parseInt(o2.getId().substring(1));
				if (index1 > index2) {
					return 1;
				} else {
					return -1;
				}
			}
		});

		for (final JunctionInf j : js) {
			JMenu cargoAdd = new JMenu(j.getId());
			JMenuItem addSafeCargoToJ = new JMenuItem("Safe");
			addSafeCargoToJ.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					env.receiveCargo(j, new Cargo(Cargo.Type.Safe));
				}
			});
			cargoAdd.add(addSafeCargoToJ);

			JMenuItem addHazardousCargoToJ2 = new JMenuItem("Hazard");
			addHazardousCargoToJ2.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					env.receiveCargo(j, new Cargo(Cargo.Type.Hazardous));
				}
			});
			cargoAdd.add(addHazardousCargoToJ2);

			addCargo.add(cargoAdd);

			JMenuItem needCargoJ = new JMenuItem("For " + j.getId());
			needCargoJ.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					env.needsCargo(j);
				}
			});
			addCargoRequest.add(needCargoJ);
		}
		JMenu cargo = new JMenu("Cargo");
		cargo.add(addCargo);
		cargo.add(addCargoRequest);
		mb.add(cargo);

		addDz = new JMenu("Add danger zone");
		removeDz = new JMenu("Remove danger zone");
		js.clear();
		js.addAll(env.getWorld().getAllJunctions());
		// sort
		Collections.sort(js, new Comparator<JunctionInf>() {
			@Override
			public int compare(JunctionInf o1, JunctionInf o2) {
				int index1 = Integer.parseInt(o1.getId().substring(1));
				int index2 = Integer.parseInt(o2.getId().substring(1));
				if (index1 > index2) {
					return 1;
				} else {
					return -1;
				}
			}
		});

		for (final JunctionInf j : js) {
			JMenuItem addDzToJ = new JMenuItem(j.getId());
			addDzToJ.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if (!env.getDangerZones().containsKey(j.getId())) {
						env.addDangerZone(j.getId());
					}
				}
			});
			JMenuItem remDzFromJ = new JMenuItem(j.getId());
			remDzFromJ.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if (env.getDangerZones().containsKey(j.getId())) {
						env.removeDangerZone(j.getId());
					}
				}
			});

			addDz.add(addDzToJ);
			removeDz.add(remDzFromJ);
		}
		JMenu dangerZone = new JMenu("Danger Zones");
		dangerZone.add(addDz);
		dangerZone.add(removeDz);
		mb.add(dangerZone);

		this.setJMenuBar(mb);
		this.setSize(env.getWorld().getDimensionsX() + 100, env.getWorld().getDimensionsY() + 100);
		this.setMaximumSize(this.getSize());
		this.setPreferredSize(getSize());
		this.setResizable(true);
		this.addWindowListener(this);

		createKeyWindow();
		JScrollPane content = new JScrollPane();
		JPanel map = new JPanel() {
			private static final long serialVersionUID = -6269783206650359003L;

			public void paint(Graphics g) {
				if(!isVisible()) {
					return;
				}
				try {

					int pct = -1;
					if (env.getGenDemands() > 0) {
						pct = (int) (100 * ((float) env.getSatisfied() / (float) env.getGenDemands()));
					}

					setTitle(title + "; " + env.getSatisfied() + " / " + env.getGenDemands() + " (~" + pct + "%)"
							+ ((start > 0) ? ", t=" + Watchdog.calculateElapsedTimeSince(start) : "") + "(step "
							+ e.getStep() + ")");
					int drawSizeX = getSize().width;
					int drawSizeY = getSize().height;
					if (getSize().width > drawSizeX) {
						drawSizeX = this.getSize().width;
					}
					if (getSize().height > drawSizeY) {
						drawSizeY = this.getSize().height;
					}

					BufferedImage bi = (BufferedImage) createImage(drawSizeX, drawSizeY);
					Graphics2D big = (Graphics2D) bi.getGraphics();
					big.setStroke(new BasicStroke(LINE_THICKNESS));
					big.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
					big.setColor(BACKGROUND);

					// draw danger zones
					Vector<DangerZone> dz = new Vector<DangerZone>();
					synchronized (env.getDangerZones()) { // avoid cme...
						for (DangerZone s : env.getDangerZones().values()) {
							dz.add(new DangerZone(s.getSource(), s.getShape()));
						}
					}
					for (DangerZone s : dz) {
						drawDZ(big, s);
					}

					// draw storms
					Object[] o = env.getStorms().toArray();
					for (Object ob : o) {
						drawStorm(big, (RainStorm) ob);
					}

					// draw roads
					Vector<RoadInf> roads = env.getWorld().getAllRoads();
					for (RoadInf r : roads) {
						drawRoad(big, r);
					}

					// draw all junctions
					synchronized (env.getDangerZones()) { // avoid cme...
						for (JunctionInf j : env.getWorld().getAllJunctions()) {
							drawJunction(big, j, env.getDangerZones().containsKey(j.getId()));
						}
					}
					// now vehicles and string labels
					o = env.getVehicles().keySet().toArray();
					Vector<String> key = new Vector<String>();
					for (Object ob : o) {
						key.add((String) ob);
					}

					if (key.size() > 0) { // to avoid div/0 for reductor
						Color vc = VEHICLE_COLOUR;
						int reductor = 255 / key.size();
						for (String k : key) {
							Vehicle v = env.getVehicles().get(k);
							if (v != null) {
								drawVehicle(big, v, vc);
								// update color
								int vg = vc.getGreen() - reductor;
								if (vg < 0) {
									vg = 0;
								}
								vc = new Color(vc.getRed(), vg, vc.getBlue());
							}
						}
					}

					if (drawInfoString) {
						big.setColor(TEXT_COLOUR);

						int xTextPos = getWidth() - 160;
						if (UniversalConstants.DEBUG) {
							big.drawString("DEBUG MODE ON", xTextPos, 20);
						}
						if (UniversalConstants.DEMO_MODE) {
							big.drawString("DEMO MODE ON", xTextPos, 40);
						}
						if (UniversalConstants.SHOW_ALL_ACTION_RESULTS) {
							big.drawString("DIALOG FOR RESULTS", xTextPos, 60);
						}
						if (UniversalConstants.DETAILED_CONTRACT_FORMATION) {
							big.drawString("DIALOG FOR CONTRACTS", xTextPos, 80);
						}

						int reqCount = env.sumCargoRequests();
						int carCount = 0;
						for (String k : env.getCargoCounts().keySet()) {
							carCount += env.getCargoCounts().get(k).size();
						}

						String goalStats = "Cargo:  = " + carCount;
						if (env.cargoLoaded()) {
							goalStats = goalStats + "(loaded)";
						}

						// + "(ld? " + env.cargoLoaded() + ") " +
						goalStats = goalStats + "/ req = " + reqCount + "  Dlvrd: " + env.getSatisfied() + " / "
								+ env.getGenDemands() + " (~" + pct + "%)";
						big.drawString(goalStats, 5, 15);

						Vector<String> vS = new Vector<String>();
						for (String agKey : env.getVehicles().keySet()) {
							Vehicle ag = (Vehicle) env.getVehicles().get(agKey);
							if(ag == null) {
								vS.add(agKey + "(not existing!)");
							}
							else {
								vS.add(ag.getId() + (ag.getHealth().equals(HealthState.damaged)?"(D)":
									(ag.getHealth().equals(HealthState.mortal)?"(M)":"")));
							}
						}
						Collections.sort(vS);
						big.drawString(vS.size() + " Phys agents; " + vS.toString(), 5, 30);

						String stuck = "Stuck; " + env.getStuckList().toString();
						if (env.isWindy()) {
							stuck = stuck + "   WINDY!!";
						}
						big.drawString(stuck, 5, 45);

						// draw action states - give generalized %age
						pct = -1;
						if (env.getActionsTried() > 0) {
							pct = (int) (100 * ((float) env.getActionsPass() / (float) env.getActionsTried()));
						}

						String actions = "Act succ:" + env.getActionsPass() + " / " + env.getActionsTried() + " (~"
								+ pct + "%); Fail arg/pre/exo/nd: " + "(" + env.getActionsArgFail() + "/"
								+ env.getActionsPreFail() + "/" + env.getActionsExoFail() + "/ "
								+ env.getActionsRandFail() + ")";
						String currentActions = "Current: " + env.getCurrentAction();
						String prevAction = "Previous: " + env.getLastActionLiteral() + ":" + env.getLastActionResult();

						long ito = Cargoworld.INACTIVITY_TIMEOUT_PERIOD
								- (System.currentTimeMillis() - env.getLastActionTime());
						String tim = "Avg time acting: " + df.format(env.avgActingTime()) + " ms (total: "
								+ df.format(env.totalActingTime()) + " ms) / " + " timeout: " + ito + "ms";
						// draw string info
						big.drawString(actions, 5, 60);
						big.drawString(currentActions, 5, 75);
						big.drawString(prevAction, 5, 90);
						big.drawString(tim, 5, 105);

						FontMetrics fm = big.getFontMetrics(new Font(big.getFont().toString(), 0, 12));
						int h = fm.getHeight();
						String resultString = "";
						for (String rs : env.getResultStrings()) {
							resultString = resultString + "         " + rs;
						}
						resultString = resultString.trim();
						// int w = fm.stringWidth(resultString);
						big.setColor(TEXT_BG_COLOUR_2);
						Rectangle2D cargoBox = new Rectangle2D.Double(5, getHeight() - 20, getWidth(), h + 5);
						big.fill(cargoBox);
						big.setColor(TEXT_COLOUR_2);
						big.drawString(resultString, 5, getHeight() - 5);
					}
					g.drawImage(bi, 0, 0, this); // double buffering!
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			
			private void drawDZ(Graphics2D big, DangerZone s) {
				big.setPaint(DZ_COLOUR);
				Shape ds = s.getShape();
				big.fill(ds);
			}

			private void drawStorm(Graphics2D big, RainStorm r) {
				big.setPaint(RAINSTORM_COLOUR_1);
				final double x = r.getCentre().getX();
				final double y = r.getCentre().getY();
				double width = (r.getRadius() * 3);
				double height = (r.getRadius() * 3);
				Ellipse2D storm = new Ellipse2D.Double(x - (width / 2), y - (height / 2), width, height);
				big.fill(storm);

				if (debug) {
					debugDrawStorm(big, r, x, y, storm);
				} // end debug stuff
			}

			private void debugDrawStorm(Graphics2D big, RainStorm r, final double x, final double y, Ellipse2D storm) {
				big.setPaint(DEBUG_BB_COLOUR);
				big.draw(storm.getBounds2D());
				for (JunctionInf j : env.getWorld().getAllJunctions()) {
					// draw connection...
					Point2D p1 = new Point2D.Double(j.getCoordinate().getX(), j.getCoordinate().getY());
					Point2D p2 = new Point2D.Double(x, y);
					Stroke old = big.getStroke();
					final BasicStroke dash = new BasicStroke(1.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f,
							new float[] { 3.0f }, 0.0f);
					big.setStroke(dash);

					if (j.getCoordinate().distance(r.getCentre()) <= r.getRadius()) {
						big.setPaint(DEBUG_LINE_COLOUR_HIGHLIGHT_1);
						big.draw(new Line2D.Double(p1, p2));
					} else {
						big.setPaint(DEBUG_LINE_COLOUR);
					}
					big.setStroke(old);
				}
				// text info
				big.setColor(STORM_TEXT_COLOUR);
				big.drawString("Storm (" + (int) r.getStormArea().getCenterX() + ", "
						+ (int) r.getStormArea().getCenterY() + ") life: " + r.getLifespan(), (int) storm.getCenterX(),
						(int) storm.getCenterY());
			}

			private void drawRoad(Graphics2D big, RoadInf r) {
				int x1 = (int) (r.getEnds()[0].getCoordinate().getX());
				int x2 = (int) (r.getEnds()[1].getCoordinate().getX());
				int y1 = (int) (r.getEnds()[0].getCoordinate().getY());
				int y2 = (int) (r.getEnds()[1].getCoordinate().getY());

				if (r.isToxic()) {
					big.setColor(TOXIC_COLOUR);
				} else if (r.isBlocked()) {
					big.setColor(BLOCKED_COLOUR);
				} else {
					if (r.isFlooded()) {
						if (r.getRoadType().equals(RoadType.mud)) {
							big.setColor(FLOODED_MUD_COLOUR);
						} else
							big.setColor(FLOODED_TARMAC_COLOUR);
					} else if (r.isSlippery()) {
						if (r.getRoadType().equals(RoadType.mud)) {
							big.setColor(SLIPPERY_MUD_COLOUR);
						} else
							big.setColor(SLIPPERY_TARMAC_COLOUR);
					} else {
						if (r.getRoadType().equals(RoadType.mud)) {
							big.setColor(DRY_MUD_COLOUR);
						} else
							big.setColor(DRY_TARMAC_COLOUR);
					}
				}

				big.drawLine(x1, y1, x2, y2);

				if (r.isBlocked() && drawLocationString) {
					String st = "";
					if (r.isSlippery()) {
						st = " (s)";
					} else if (r.isFlooded()) {
						st = " (f)";
					}
					big.setColor(CLOSED_TEXT_COLOUR);
					big.drawString(r.getId() + " CLS" + st, ((x1 + x2) / 2) - 14, ((y1 + y2) / 2) - 3);
				}
				if (r.isToxic() && drawLocationString) {
					big.setColor(TOXIC_TEXT_COLOUR);
					String st = "";
					if (r.isSlippery()) {
						st = " (s)";
					} else if (r.isFlooded()) {
						st = " (f)";
					}
					big.drawString(r.getId() + " TOX" + st, ((x1 + x2) / 2) - 10, ((y1 + y2) / 2) + 10);
				}

				if (debug && env.getStepper() instanceof EnvStepper) {
					big.setColor(TEXT_COLOUR_2);
					final RainController rc = ((EnvStepper) env.getStepper()).getRain();
					big.drawString(rc.getAccumulation(r.getId()) + "(" + rc.getSlipperyLimit(r.getId()) + "->"
							+ rc.getFloodLimit(r.getId()) + ")", ((x1 + x2) / 2) - 10, ((y1 + y2) / 2) + 15);
				}
			}

			private void drawJunction(Graphics2D big, JunctionInf j, boolean danger) {
				if (danger) {
					big.setColor(DZ_NODE_COLOUR);
				} else {
					big.setColor(NODE_COLOUR);
				}
				int x = (int) (j.getCoordinate().getX());
				int y = (int) (j.getCoordinate().getY());
				x -= (NODE_DRAW_WIDTH / 2);
				y -= (NODE_DRAW_WIDTH / 2);
				big.fillOval(x, y, NODE_DRAW_WIDTH, NODE_DRAW_WIDTH);

				// drawBB for last gen
				int centreY = y + (NODE_DRAW_WIDTH / 2);
				drawBoundingForLastGen(big, j);

				// draw junction info text
				big.setBackground(new Color(0, 0, 0, 0));
				drawDecoratorsForJunction(big, x, y, j.getId());
				drawLocationStrings(big, j, x, centreY);
			}

			private void drawBoundingForLastGen(final Graphics2D big, final JunctionInf j) {
				if (j.getId().equals(env.getLastGenerated())) {
					big.setColor(LAST_DELIVER);
					Stroke old = big.getStroke();

					big.setStroke(dash2);
					targetBox((int) j.getCoordinate().getX(), (int) j.getCoordinate().getY(), big);

					big.setStroke(old);
				}
			}

			private void drawLocationStrings(final Graphics2D big, final JunctionInf j, final int x,
					final int centreY) {
				if (drawLocationString) {
					String st1 = j.getId() + " " + j.getElevation() + "m";

					FontMetrics fm = big.getFontMetrics(new Font(big.getFont().toString(), 0, 12));

					Rectangle2D rect = fm.getStringBounds(st1, big);
					int yDraw = centreY - 12;
					big.setColor(TEXT_BG_COLOUR);
					big.fillRect(x + (NODE_DRAW_WIDTH), yDraw, (int) rect.getWidth(), (int) rect.getHeight());
					big.setColor(NODE_TEXT_COLOUR);
					big.drawString(st1, x + (NODE_DRAW_WIDTH), centreY);

					if (env.getCargoCounts().get(j.getId()).size() > 0) {
						big.setColor(CARGO_TARGET);
						Stroke old = big.getStroke();

						big.setStroke(dash);
						targetBox((int) j.getCoordinate().getX(), (int) j.getCoordinate().getY(), big);
						big.setStroke(old);
					}
					if (env.getCargoRequests().get(j.getId()) > 0) {
						String st3 = " W:" + env.getCargoRequests().get(j.getId());
						rect = fm.getStringBounds(st3, big);
						yDraw = centreY;
						big.setColor(TEXT_BG_COLOUR);
						big.fillRect(x + (NODE_DRAW_WIDTH), yDraw, (int) rect.getWidth(), (int) rect.getHeight());
						big.setColor(NODE_TEXT_COLOUR);
						big.drawString(st3, x + (NODE_DRAW_WIDTH), centreY + 12);
					}

				}
			}

			private void drawDecoratorsForJunction(final Graphics2D big, final int x, final int y,
					final String junctionId) {
				if (env.getWorld().getSettlementNames().contains(junctionId)) {
					final SettlementInf set = env.getWorld().getSettlement(junctionId);
					// draw airport etc...
					if (set.hasAirport()) {
						final int dx = x - (DECORATOR_DRAW_WIDTH / 2) - 5;
						final int dy = y - DECORATOR_DRAW_WIDTH;
						big.fillRect(dx, dy, DECORATOR_DRAW_WIDTH, DECORATOR_DRAW_WIDTH);
						big.drawImage(WorldUi.AIRPORT_IMG, dx, dy, DECORATOR_DRAW_WIDTH, DECORATOR_DRAW_WIDTH, null);
					}
					if (set.hasRepairStation()) {
						final int dx = x + (DECORATOR_DRAW_WIDTH / 2) + 5;
						final int dy = y - DECORATOR_DRAW_WIDTH;
						big.fillRect(dx, dy, DECORATOR_DRAW_WIDTH, DECORATOR_DRAW_WIDTH);
						big.drawImage(WorldUi.REPAIR_IMG, dx, dy, DECORATOR_DRAW_WIDTH, DECORATOR_DRAW_WIDTH, null);
					}
				}
			}

			/**
			 * Draw a box 40 x 40 centred at x,y with the current graphics
			 * settings
			 * 
			 * @param x
			 * @param y
			 */
			private void targetBox(final int x, final int y, final Graphics2D g) {
				g.drawLine(x, y - 200, x, y + 200);
				g.drawLine(x - 200, y, x + 200, y);
				g.drawRect(x - 15, y - 15, 30, 30);
			}

			private void drawVehicle(final Graphics2D big, final Vehicle v, final Color vc) {
				big.setColor(vc);
				big.setPaint(vc);
				int width = VEHICLE_DRAW_WIDTH;// env.getWorld().getCellDimensions();
				double x = v.getLocationCoordinates().getX() - (width / 2);
				double y = v.getLocationCoordinates().getY() - (width / 2);
				Ellipse2D drawn = new Ellipse2D.Double(x, y, width, width);
				big.setPaint(vc);
				big.draw(drawn);

				if (v.getCargo() != null) {
					Cargo c = v.getCargo();
					Rectangle2D cargo = new Rectangle2D.Double(x, y, width, width);
					if (c.getType().equals(Cargo.Type.Hazardous)) {
						big.setPaint(CARGO_HZ_COLOUR);
					} else {
						big.setPaint(CARGO_COLOUR);
					}
					big.fill(cargo);

					big.setColor(CARGO_TARGET);
					Stroke old = big.getStroke();

					big.setStroke(dash2);
					targetBox((int) v.getLocationCoordinates().getX(), (int) v.getLocationCoordinates().getY(), big);
					big.setStroke(old);
				}

				if (v.getHealth().equals(UniversalConstants.HealthState.healthy)) {
					big.setColor(VEHICLE_HEALTHY_COLOUR);
				} else if (v.getHealth().equals(UniversalConstants.HealthState.damaged)) {
					big.setColor(VEHICLE_DAMAGED_COLOUR);
				} else if (v.getHealth().equals(UniversalConstants.HealthState.mortal)) {
					big.setColor(VEHICLE_MORTAL_COLOUR);
				}
				big.fill(drawn);

				if (drawVehicleString) {
					big.setColor(VEHICLE_TEXT_COLOUR);
					String str = v.getId();
					FontMetrics fm = big.getFontMetrics(new Font(big.getFont().toString(), 0, 12));
					int w = fm.stringWidth(str);
					int h = fm.getHeight();
					big.setColor(VEHICLE_TEXT_BG_COLOUR);
					Rectangle2D bgBox = new Rectangle2D.Double((int) drawn.getCenterX() - w,
							(int) drawn.getCenterY() - fm.getAscent(), w, h);
					big.fill(bgBox);
					big.setColor(VEHICLE_TEXT_COLOUR);
					big.drawString(str, (int) drawn.getCenterX() - w, (int) drawn.getCenterY());

					if (v.isActing() && v.getAction()!=null) {
						Literal act = v.getAction().copy();
						act.clearAnnots();

						if (v.getType().equals(EntityType.helicopter)
								&& act.getFunctor().equals(WorldAction.fly.name())) {
							String dest = act.getTerm(2).toString().replaceAll("\"", "");
							Point2D loc = env.getWorld().getJunction(dest).getCoordinate();
							Stroke old = big.getStroke();
							big.setStroke(dash);
							big.setColor(HELI_FLIGHTPATH_COLOUR);
							// draw line to dest...
							big.drawLine((int) v.getLocationCoordinates().getX(),
									(int) v.getLocationCoordinates().getY(), (int) loc.getX(), (int) loc.getY());

							big.setStroke(old);
						}

						fm = big.getFontMetrics(new Font(big.getFont().toString(), 0, 12));
						h = fm.getHeight();
						w = fm.stringWidth(act.toString());
						big.setColor(VEHICLE_TEXT_BG_COLOUR);
						bgBox = new Rectangle2D.Double((int) drawn.getCenterX() - w, (int) drawn.getCenterY(), w, h);
						big.fill(bgBox);
						big.setColor(VEHICLE_TEXT_COLOUR);
						big.drawString(act.toString(), (int) drawn.getCenterX() - w,
								(int) drawn.getCenterY() + fm.getAscent());
					}
				} // end string drawing
			}
		};
		content.getViewport().add(map);
		content.setPreferredSize(getContentPane().getSize());
		getContentPane().add(content);// , BorderLayout.CENTER);

		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice[] gs = ge.getScreenDevices();
		int xOff = 0;
		int yOff = 0;
		int index = 0; // index of screen to use
		// set offsets to put on the other screen
		if (gs.length > 1) {
			index = gs.length - 1;
			Rectangle gcBounds = gs[index].getDefaultConfiguration().getBounds();
			xOff = gcBounds.x;
			yOff = gcBounds.y;
		}

		setLocation(xOff, yOff);
		setVisible(true);

		new Thread(new Runnable() {

			@Override
			public void run() {
				while (isVisible()) {
					repaint();
					try {
						Thread.sleep(1000 / FPS);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}

		}).start();
	}

	// TODO resource file loader
	private void loadImageIcons(final Cargoworld e) {
		final String imgDir = new File("").getAbsolutePath() + File.separatorChar + "img" + File.separatorChar;

		try {
			ImageIO.setUseCache(true);
			AIRPORT_IMG = ImageIO.read(new File(imgDir + "airport.png"));
			REPAIR_IMG = ImageIO.read(new File(imgDir + "repairstation.png"));
		} catch (IOException ioe) {
			JOptionPane.showMessageDialog(this, ioe.getMessage(), "Error loading image", JOptionPane.ERROR_MESSAGE);
			e.toString();
		}
	}

	/**
	 * Creates a little display to show the colour key for the map
	 */
	private void createKeyWindow() {
		keyWin = new JDialog(this);
		keyWin.setTitle("Key");
		keyWin.setLocation(getX() + getSize().width, getY());
		keyWin.setSize(100, 100);
		keyWin.getContentPane().setLayout(new GridLayout(7, 1));

		JLabel j1 = new JLabel("Dry Tarmac Road");
		j1.setForeground(DRY_TARMAC_COLOUR);
		j1.setFont(Font.getFont(Font.SERIF));

		JLabel j2 = new JLabel("Slippery Tarmac Road");
		j2.setForeground(SLIPPERY_TARMAC_COLOUR);
		j2.setFont(Font.getFont(Font.SERIF));

		JLabel j3 = new JLabel("Flooded Tarmac Road");
		j3.setForeground(FLOODED_TARMAC_COLOUR);
		j3.setFont(Font.getFont(Font.SERIF));

		JLabel j4 = new JLabel("Dry Mud Road");
		j4.setForeground(DRY_MUD_COLOUR);
		j4.setFont(Font.getFont(Font.SERIF));

		JLabel j5 = new JLabel("Slippery Mud Road");
		j5.setForeground(SLIPPERY_MUD_COLOUR);
		j5.setFont(Font.getFont(Font.SERIF));

		JLabel j6 = new JLabel("Flooded Mud Road");
		j6.setForeground(FLOODED_MUD_COLOUR);
		j6.setFont(Font.getFont(Font.SERIF));

		JLabel j7 = new JLabel("Road (either type) blocked");
		j7.setForeground(BLOCKED_COLOUR);
		j7.setFont(Font.getFont(Font.SERIF));

		JLabel j8 = new JLabel("Road (either type) contaminated");
		j7.setForeground(TOXIC_COLOUR);
		j7.setFont(Font.getFont(Font.SERIF));

		keyWin.getContentPane().add(j1);
		keyWin.getContentPane().add(j2);
		keyWin.getContentPane().add(j3);
		keyWin.getContentPane().add(j4);
		keyWin.getContentPane().add(j5);
		keyWin.getContentPane().add(j6);
		keyWin.getContentPane().add(j7);
		keyWin.getContentPane().add(j8);

		keyWin.pack();
		keyWin.setVisible(showKey);
	}

	@Override
	public void setVisible(boolean setting) {
		if (showKey) {
			keyWin.setVisible(setting);
		}
		super.setVisible(setting);
	}

	private void askForShutdown() {
		int result = JOptionPane.showConfirmDialog(this, new String[] { "Do you wish to stop the agent system?" },
				"Confirm shutdown?", JOptionPane.YES_NO_OPTION);
		if (result == 0) {
			shutdown();
		}
	}

	private void shutdown() {
		setVisible(false);
		keyWin.setVisible(false);
		try {
			env.kill();
		} catch (Exception e) {
			String st = "Shutdown call error: " + e.toString();
			for (StackTraceElement s : e.getStackTrace()) {
				st = st + "\n\t" + s;
			}
			env.getLogger().severe(st);
		}
		System.exit(0);
	}
	private void createAutoStartThread() {
		new Thread(new Runnable(){
			long elapsed = 0;
			@Override
			public void run() {
				while(env.getVehicles().entrySet().size() < 10) {
					forceWait();
					if(elapsed > 60000) {
						JOptionPane.showMessageDialog(null, "Waited ages and vehicles were missing?!");
						System.exit(0);
					}
				}
				forceWait();
				
				env.kickOffSim();
			}

			private void forceWait() {
				try {
					Thread.sleep(200);
					elapsed+=200;
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}}).start();
	}


	public void windowActivated(WindowEvent arg0) {
	}

	public void windowClosed(WindowEvent arg0) {
	}

	public void windowClosing(WindowEvent arg0) {
		shutdown();
	}

	public void windowDeactivated(WindowEvent arg0) {
	}

	public void windowDeiconified(WindowEvent arg0) {
	}

	public void windowIconified(WindowEvent arg0) {
	}

	public void windowOpened(WindowEvent arg0) {
	}
	
	@Override
	public void actionPerformed(ActionEvent a) {
		if (a.getSource().equals(envDebugToggle)) {
			debug = !debug;
		} else if (a.getSource().equals(keyToggle)) {
			showKey = !showKey;
			this.keyWin.setVisible(showKey);
		} else if (a.getSource().equals(demoToggle)) {
			UniversalConstants.DEMO_MODE = !UniversalConstants.DEMO_MODE;
		} else if (a.getSource().equals(agentDebugToggle)) {
			UniversalConstants.DEBUG = !UniversalConstants.DEBUG;
		} else if (a.getSource().equals(detailObToggle)) {
			UniversalConstants.DETAILED_CONTRACT_FORMATION = !UniversalConstants.DETAILED_CONTRACT_FORMATION;
		} else if (a.getSource().equals(resultToggle)) {
			UniversalConstants.SHOW_ALL_ACTION_RESULTS = !UniversalConstants.SHOW_ALL_ACTION_RESULTS;
		} else if (a.getSource().equals(locStringToggle)) {
			drawLocationString = !drawLocationString;
		} else if (a.getSource().equals(vehStringToggle)) {
			drawVehicleString = !drawVehicleString;
		} else if (a.getSource().equals(infoStringToggle)) {
			drawInfoString = !drawInfoString;
		} else if (a.getSource().equals(kickoffSim)) {
			start = (start == 0) ? System.currentTimeMillis() : start;
			env.kickOffSim();
		} else if (a.getSource().equals(shutdown)) {
			askForShutdown();
		}
	}

	/**
	 * Add debug menu for vehicle state
	 * 
	 * @param agName
	 */
	public void addVehicle(final String agName) {
		if (knownV.contains(agName)) {
			return;
		} else {
			knownV.add(agName);
		}

		JMenu jm = new JMenu(agName);
		JMenuItem healthy = new JMenuItem("Set healthy");
		healthy.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				env.setVehicleHealth(agName, HealthState.healthy);
			}
		});
		jm.add(healthy);

		JMenuItem damaged = new JMenuItem("Set damaged");
		damaged.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				env.setVehicleHealth(agName, HealthState.damaged);
			}
		});
		jm.add(damaged);

		JMenuItem mortal = new JMenuItem("Set mortal");
		mortal.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				env.setVehicleHealth(agName, HealthState.mortal);
			}
		});
		jm.add(mortal);
		agents.add(jm);
	}
}// end class