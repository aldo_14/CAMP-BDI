package truckworld.env.util;

import java.awt.Polygon;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.util.Random;

import truckworld.world.interfaces.JunctionInf;

public class DzShapeUtil {
	private final Random simRandom;
	private final int maxSizeDangerZone;
	private final int minSizeDangerZone;
	private final float maxOffset;
	
	public DzShapeUtil(final Random seed) {
		simRandom = seed;
		this.maxSizeDangerZone = 40;
		this.minSizeDangerZone = 30;
		this.maxOffset = 0;
	}
	
	/**
	 * @param j
	 * @param minSizeDangerZone 
	 * @return
	 */
	public Shape createDzArea(JunctionInf j) {
		int sides = (int) (4 + simRandom.nextFloat()*4); //less sides -> looks better (with this algorithm)
		int insideRadius = 
				(int) (minSizeDangerZone + 
						(simRandom.nextFloat()*(maxSizeDangerZone - minSizeDangerZone)));
		int outsideRadius = 
				(int)(insideRadius + 
						(simRandom.nextFloat()*(maxSizeDangerZone - insideRadius)));
		Shape dz = generatePolygon(sides, outsideRadius, insideRadius);

		//gen location
		Point2D centre = j.getCoordinate();
		//random shift...
		float shiftX = simRandom.nextFloat()*maxOffset;
		float shiftY = simRandom.nextFloat()*maxOffset;
		if(simRandom.nextBoolean())	{ shiftX *= -1;	}
		if(simRandom.nextBoolean())	{ shiftY *= -1;	}
		centre.setLocation(centre.getX() + shiftX, centre.getY() + shiftY);

		//translate
		AffineTransform trans = AffineTransform.getTranslateInstance(centre.getX(), centre.getY());
		dz = trans.createTransformedShape(dz);
		return dz;
	}
	
	/**
	 * Poly generation util adapted from code at http://www.java2s.com/Code/Java/2D-Graphics-GUI/GeneratePolygon.htm
	 * 
	 * Modified to create non-symmetrical polygons, etc
	 */
	private Shape generatePolygon(int sides, int outsideRadius, int insideRadius) {

		if (sides < 3) { sides = 3; }

		AffineTransform trans = new AffineTransform();
		Polygon poly = new Polygon();
		for (int i = 0; i < sides; i++) {
			trans.rotate(Math.PI * 2 / (float) sides / 2);
			float rad = (float)(insideRadius + (simRandom.nextFloat()*(outsideRadius-insideRadius)));
			Point2D out = 
					trans.transform(new Point2D.Float(0, rad), null);
			poly.addPoint((int) out.getX(), (int) out.getY());

			trans.rotate(Math.PI * 2 / (float) sides / 2);
			if (insideRadius > 0 && (simRandom.nextBoolean()|| poly.npoints<3)) {
				rad = (float)((insideRadius/2) + (simRandom.nextFloat()*(insideRadius/2)));
				Point2D in = trans.transform(
						new Point2D.Float(0, (insideRadius/2) + simRandom.nextInt((insideRadius/2))), 
						null);
				poly.addPoint((int) in.getX(), (int) in.getY());
			}

		}

		return poly;
	}
}
