package truckworld.env.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesHelper {
	private final String propFile;

	public PropertiesHelper(final String propFile) {
		this.propFile = propFile;
	}

	public final Properties loadProperties() {
		File pFile = new File(propFile);
		Properties p = new Properties();
		try {
			if (pFile.exists()) {
				FileInputStream in = new FileInputStream(pFile);
				p.load(in);
				in.close();
				return p;
			} 
			else {
				throw new RuntimeException(propFile + " not found!");
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
