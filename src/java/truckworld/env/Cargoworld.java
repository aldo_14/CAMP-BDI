package truckworld.env;

import static truckworld.env.EnvironmentConstants.VEHICLE_STD_LOAD_TIME;
import static truckworld.env.EnvironmentConstants.VEHICLE_STD_REPAIR_DAMAGED_TIME;
import static truckworld.env.EnvironmentConstants.VEHICLE_STD_REPAIR_MORTAL_TIME;
import static truckworld.env.EnvironmentConstants.VEHICLE_STD_SECURE_TIME;
import static truckworld.env.EnvironmentConstants.VEHICLE_STD_UNLOAD_TIME;
import static truckworld.env.Percepts.AG_REGISTERED;
import static truckworld.env.Percepts.AT_COORD;
import static truckworld.env.Percepts.AT_JUNCTION;
import static truckworld.env.Percepts.BLOCKED;
import static truckworld.env.Percepts.BLOCKED_ROAD;
import static truckworld.env.Percepts.BUSY;
import static truckworld.env.Percepts.CARGO_AT;
import static truckworld.env.Percepts.CARGO_NEEDED;
import static truckworld.env.Percepts.CARGO_TYPE;
import static truckworld.env.Percepts.CARRYING_CARGO;
import static truckworld.env.Percepts.DANGERZONE;
import static truckworld.env.Percepts.FLYING;
import static truckworld.env.Percepts.FLYING_OVER;
import static truckworld.env.Percepts.LOADED;
import static truckworld.env.Percepts.ON_ROAD;
import static truckworld.env.Percepts.RESTING;
import static truckworld.env.Percepts.STUCK;
import static truckworld.env.Percepts.TOXIC;
import static truckworld.env.Percepts.TOXIC_ROAD;
import static truckworld.env.Percepts.WINDY;

import java.awt.geom.Point2D;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Queue;
import java.util.Random;
import java.util.Vector;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import common.Analytics;
import common.Analytics.AnalyticsOperations;
import common.StatsRecorder;
import common.UniversalConstants;
import common.UniversalConstants.EntityType;
import common.UniversalConstants.HealthState;
import common.UniversalConstants.RoadCondition;
import common.UniversalConstants.RoadType;
import common.UniversalConstants.WorldAction;
import common.Utils;
import jason.asSyntax.ASSyntax;
import jason.asSyntax.Literal;
import jason.asSyntax.StringTerm;
import jason.asSyntax.Structure;
import jason.asSyntax.Term;
import jason.environment.Environment;
import truckworld.env.EnvironmentConstants.ActionResult;
import truckworld.env.util.DzShapeUtil;
import truckworld.env.util.PropertiesHelper;
import truckworld.exceptions.ConnectionExistsException;
import truckworld.exceptions.WorldGenException;
import truckworld.ui.WorldUi;
import truckworld.vehicle.Vehicle;
import truckworld.vehicle.VehicleFactory;
import truckworld.vehicle.type.Helicopter;
import truckworld.world.Cargo;
import truckworld.world.Cargo.Type;
import truckworld.world.World;
import truckworld.world.interfaces.JunctionInf;
import truckworld.world.interfaces.RoadInf;
import truckworld.world.interfaces.SettlementInf;
import truckworld.world.simulatedConditions.DangerZone;
import truckworld.world.simulatedConditions.EnvStepper;
import truckworld.world.simulatedConditions.FailureConsequences;
import truckworld.world.simulatedConditions.Stepper;
import truckworld.world.simulatedConditions.Watchdog;
import truckworld.world.simulatedConditions.rain.RainStorm;

public class Cargoworld extends Environment {

	/**
	 * Time scalar; default speed is returned in units per ms, this value is
	 * used to multiply that value. Note that this is independent of the
	 * simulation update speed.
	 * 
	 * So if speed =1 and this = 1, then travelling 1 unit takes 1ms. this = 0.1
	 * then 10ms. 0.01, then 100ms; etc. (lower scalar=slower)
	 */
	public static final float SPEED_TIME_SCALAR = 50f;// 0.05-0.1f for
														// experiment, 0.02 good
														// for debug? Was 0.3

	/**
	 * MS time; if INACTIVITY_TIME has passed since last action, agents are
	 * deemed as idle
	 */
	public static final long INACTIVITY_TIMEOUT_PERIOD = 1000;

	/**
	 * Time last action was attempted/completed
	 */
	private long lastAction = Long.MAX_VALUE; // initial so we don't trigger on
												// first check

	private final static Literal WINDY_PERCEPT = ASSyntax.createLiteral(WINDY);

	private final Map<Literal, ActionResult> lastActionPerformed = new ConcurrentHashMap<>(8,0.9f,1);

	private final Map<Literal, Float> currentAction = new ConcurrentHashMap<>(1,0.9f,1);

	private final VehicleFactory vehicleFactory = new VehicleFactory();

	/**
	 * Logger; shared across sub-class instances etc
	 * 
	 * @see UniversalConstants.LOGGER_ID
	 */
	private Logger logger;

	/** Show user peinterface? */
	private boolean showUi = true;

	/** UI display */
	private WorldUi ui;

	/** The world information */
	private World world;

	/**
	 * Areas of danger from terrorist/rebel/insurgent attack (maps to JID key)
	 */
	private final Map<String, DangerZone> dangerZones = new ConcurrentHashMap<>(30,0.9f,1);

	/**
	 * World vehicles; the String should correspond to a controlling agent.
	 * Vehicles can travel on roads, etc, with the semantics depending on their
	 * exact type.
	 */
	private final Map<String, Vehicle> vehicleAgents = new ConcurrentHashMap<>(10,0.9f,1);

	/**
	 * Initial locations of world vehicles
	 */
	private final Map<String, String> initialVehicleLoc = new ConcurrentHashMap<>(10,0.9f,1);

	/**
	 * Maps agent id to travel distance value
	 */
	private final Map<String, Float> travelledDistance = new ConcurrentHashMap<>(10,0.9f,1);

	/**
	 * Maps agent id to activity cost
	 */
	private final Map<String, Integer> activityCosts = new ConcurrentHashMap<>(10,0.9f,1);

	/**
	 * Maps agent id to time spend acting
	 */
	private final Map<String, Long> actingTime = new ConcurrentHashMap<>(10,0.9f,1);

	/**
	 * Commanders are logical agents that achieve change in the environment
	 * through controlling other agents (i.e. vehicles); this collection is used
	 * for commander-specific percept notification
	 */
	private final Collection<String> commanders = new Vector<>();

	/**
	 * All agents in the environment
	 */
	private final Collection<String> agents = new Vector<>();

	/**
	 * Junctions that need a crane added/don't have a crane added
	 */
	private final Vector<String> cranelessJunctions = new Vector<>();

	/**
	 * Agents that have acted in the current request-cycle. Used for idle-time
	 * healing purposes
	 */
	private final Collection<String> actedThisRequest = new Vector<>();

	/**
	 * Agents requiring to heal over x cycles
	 */
	private final Map<String, Integer> healCountdown = new HashMap<>();

	/**
	 * Cargo objects at fixed junction locations OR held by agents String key
	 * equals either a junction id or an agent id; in the latter, capacity is
	 * initially limited to one (but may amend later to allow larger cargo
	 * carriers...)
	 */
	private final Map<String, Map<String, Cargo>> cargoCount = new HashMap<>();

	/**
	 * Maps string for jid against an integer denoting the number of cargo
	 * deliveries requested - initially set to zero.
	 */
	private final Map<String, Integer> cargoRequests = new HashMap<>();

	/**
	 * Location of the last cargo request generation
	 */
	private String lastCargoRequestLocation = "";

	/**
	 * Seed for random number generator
	 */
	private long simSeed;

	public long getSimSeed() {
		return simSeed;
	}

	/** Local random number generator, used to allow reproducability */
	private Random simRandom;

	/** Simulation params */
	public final static long SIM_STEP_TIME = 1;

	/** Current sim step */
	private int step;

	/** Records agents stuck on a road, by keying road id to agent id */
	private final Map<String, Vector<String>> stuckAgents = new HashMap<>();

	/**
	 * Records agent stuck time and times out as appropriate
	 */
	private final Map<String, Integer> stuckTime = new HashMap<>();

	/** Configuration properties for creating etc the environment */
	private Properties properties;

	/** Object used to determine probability of 'random' failure */
	private FailureChances stochasticiser;

	/** Simulates stepped changes */
	private Stepper stepper;

	/**
	 * Params for failure, etc, debilitation stuff
	 */
	private FailureConsequences consequences;

	/**
	 * DEbugging tool - indicates if we have had an activity since the last
	 * demand was added
	 */
	private boolean actedSinceDemand = true; // init

	/**
	 * Maps agent id to road id; used to record the last road an agent was stuck
	 * on, and employ to prevent an agent becoming 'trapped' by repeat failure
	 * when trying to move off of the fail location. Set when agent becomes
	 * UNSTUCK.
	 */
	// private final Map<String, String> lastStuck = new HashMap<>();

	private final Queue<String> lastMessage = new ArrayBlockingQueue<>(MSG_Q_SIZE);
	private final static int MSG_Q_SIZE = 7;

	/**
	 * Maps the last time a dangerzone was removed to the given time in ms; used
	 * to prevent DZs 'flashing' in and out of existence
	 */
	private final Map<String, Long> lastDzRemoved = new HashMap<>();

	private final static String ANNOT_RESULT_INFO = "resultInfo";

	private static Cargoworld _instance = null;

	private DzShapeUtil dzHelper;

	public static Cargoworld getInstance() {
		return _instance; // may be null...
	}

	public void heartbeat(String id) {
		final long elapsed = System.currentTimeMillis() - lastAction;
		if (elapsed > 1000 || elapsed < 100) {
			logger.warning("Received heartbeat; id= " + id + "\n(long) time elapsed " + elapsed);
		}
		lastAction = System.currentTimeMillis();
	}

	/** Called before the MAS execution with the args informed in .mas2j */
	@Override
	public void init(String[] args) {
		super.init(args);
		for (int i = 0; i < args.length; i++) {
			args[i] = args[i].trim();
		}
		logger = Logger.getLogger(this.getClass().getSimpleName());
		logger.setLevel(UniversalConstants.ENV_LOGGER_LEVEL);

		// load props
		//i.e.
		//"world2-debil0.properties", "nonDetDebilSim5.properties", "1", "maint-quality")
		final String root = new File("").getAbsolutePath();
		final String propFile = root + File.separatorChar + "properties" + File.separatorChar + args[0];
		final String configPropId = args[0].replace(".properties", "");
		final String simPropFile = root + File.separatorChar + "properties" + File.separatorChar + "simulation"
				+ File.separatorChar + args[1].trim();

		StatsRecorder.getInstance(configPropId + "-" + args[1].replace(".properties", "") + File.separatorChar + args[3] + File.separatorChar + args[2]);
		properties = new PropertiesHelper(propFile).loadProperties();
		simRandom = new Random(Long.parseLong(args[2]));

		try {
			// create the environment trucks will be travelling through
			world = new World(properties);

			for (JunctionInf j : world.getAllJunctions()) {
				cranelessJunctions.add(j.getId());
			}

			for (JunctionInf j : world.getAllJunctions()) {
				cargoCount.put(j.getId(), new ConcurrentHashMap<String, Cargo>());
				cargoRequests.put(j.getId(), 0);
				lastDzRemoved.put(j.getId(), new Long(0));
			}

			dzHelper = new DzShapeUtil(simRandom);
			generateInitialWorldPercepts();
			stochasticiser = new FailureChances(properties);
			consequences = new FailureConsequences(new PropertiesHelper(simPropFile).loadProperties());
		} catch (WorldGenException | ConnectionExistsException e) {
			logger.severe(e.toString());
			JOptionPane.showMessageDialog(null, e.toString(), "Error creating world", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
			System.exit(0);
		}

		setupStepper(simPropFile);

		// UI windows - show map and controls for setting road state
		if (showUi) {
			ui = new WorldUi(this, configPropId);
		}
		_instance = this;
	}

	private void setupStepper(final String simPropFile) {
		stepper = new EnvStepper(this, new PropertiesHelper(simPropFile).loadProperties());
		for (int i = 0; i < 500; i++) { // do default conditions
			stepper.stepEnvironment();
		}
	}

	public long getLastActionTime() {
		return lastAction;
	}

	/**
	 * Manually start the simulation...
	 */
	public void kickOffSim() {
		startSlice();
		addDisplayString("Starting simulation...");
		executor.execute(new Watchdog(this, properties));
	}

	/**
	 * Generates and sets the initial percepts about the world geography,
	 * including road conditions and types. Geography information is perceived
	 * by all registered agents.
	 */
	private void generateInitialWorldPercepts() {
		for (JunctionInf j : world.getAllJunctions()) {
			generateJunctionPercept(j);
		}
		for (RoadInf r : world.getAllRoads()) {
			generateRoadPercept(r);
		}
	}

	/**
	 * Generate percept info for a road
	 * 
	 * @param r
	 */
	private void generateRoadPercept(RoadInf r) {
		List<Literal> percepts = new Vector<Literal>();

		// connection id - for planning purposes; defines the object
		addRoadInfoToPercepts(r, percepts);

		// blocked or not?
		if (r.isBlocked()) {
			addRoadBlockagesIntoPercepts(r, percepts);
		}
		percepts = getConditionPercepts(r, percepts);

		logger.finer("Percepts for " + r + "\n" + percepts.toString());

		for (Literal l : percepts) {
			addPercept(l);
		}
	}

	private void addRoadBlockagesIntoPercepts(RoadInf r, List<Literal> percepts) {
		Literal blockedPercept1 = ASSyntax.createLiteral(BLOCKED, ASSyntax.createString(r.getEndIds()[0]),
				ASSyntax.createString(r.getEndIds()[1]));
		Literal blockedPercept2 = ASSyntax.createLiteral(BLOCKED, ASSyntax.createString(r.getEndIds()[1]),
				ASSyntax.createString(r.getEndIds()[0]));
		Literal blockedPerceptRd = ASSyntax.createLiteral(BLOCKED_ROAD, ASSyntax.createString(r.getId()));
		percepts.add(blockedPercept1);
		percepts.add(blockedPercept2);
		percepts.add(blockedPerceptRd);
	}

	private void addRoadConnectivityInfoToPercepts(RoadInf r, List<Literal> percepts) {
		Literal l1 = ASSyntax.createLiteral(r.getRoadType().name().toLowerCase(),
				ASSyntax.createString(r.getEndIds()[0]), ASSyntax.createString(r.getEndIds()[1]));
		Literal l2 = ASSyntax.createLiteral(r.getRoadType().name().toLowerCase(),
				ASSyntax.createString(r.getEndIds()[1]), ASSyntax.createString(r.getEndIds()[0]));
		percepts.add(l1);
		percepts.add(l2);
	}

	private void addRoadInfoToPercepts(RoadInf r, List<Literal> percepts) {
		Literal connection = ASSyntax.createLiteral("connection", ASSyntax.createString(r.getId()));
		connection.addAnnot(UniversalConstants.OBJECT_DEF_ANNOT);
		percepts.add(connection);

		Literal roadId1 = ASSyntax.createLiteral("road",
				ASSyntax.createString(r.getId()), ASSyntax.createString(r.getEndIds()[0]),
				ASSyntax.createString(r.getEndIds()[1]));
		Literal roadId2 = ASSyntax.createLiteral("road", 
				ASSyntax.createString(r.getId()), ASSyntax.createString(r.getEndIds()[1]),
				ASSyntax.createString(r.getEndIds()[0]));
		percepts.add(roadId1);
		percepts.add(roadId2);
		addRoadConnectivityInfoToPercepts(r, percepts);
	}

	/**
	 * Takes in a percept list, adds condition percepts for the given road, then
	 * returns
	 * 
	 * @param r
	 *            - Road
	 * @param percepts
	 *            - Literal set
	 * @return List of Literals
	 */
	private List<Literal> getConditionPercepts(RoadInf r, List<Literal> percepts) {
		percepts.add(ASSyntax.createLiteral(r.getConditions().name().toLowerCase(),
				ASSyntax.createString(r.getEndIds()[1]), ASSyntax.createString(r.getEndIds()[0])));
		percepts.add(ASSyntax.createLiteral(r.getConditions().name().toLowerCase(),
				ASSyntax.createString(r.getEndIds()[0]), ASSyntax.createString(r.getEndIds()[1])));
		return percepts;
	}

	/**
	 * Generate percept info for a junction
	 * 
	 * @param j
	 */
	private void generateJunctionPercept(JunctionInf j) {
		Vector<Literal> percepts = new Vector<Literal>();

		// add junction location, object definition
		Literal locationPercept = ASSyntax.createLiteral("location", ASSyntax.createString(j.getId()));
		locationPercept.addAnnot(UniversalConstants.OBJECT_DEF_ANNOT);
		percepts.add(locationPercept);

		Point2D coord = j.getCoordinate();
		percepts.add(ASSyntax.createLiteral("junction", ASSyntax.createString(j.getId()),
				ASSyntax.createNumber(coord.getX()), ASSyntax.createNumber(coord.getY())));
		// settlements info
		if (world.getSettlementNames().contains(j.getId())) {
			SettlementInf si = world.getSettlement(j.getId());
			if (si.hasAirport()) {
				final Literal airportPercept = ASSyntax.createLiteral("airport", ASSyntax.createString(j.getId()));
				airportPercept.addAnnot(UniversalConstants.OBJECT_DEF_ANNOT);
				percepts.add(airportPercept);
			}

			if (si.hasRepairStation()) {
				final Literal repairCentrePercept = ASSyntax.createLiteral("repairStation", ASSyntax.createString(j.getId()));
				repairCentrePercept.addAnnot(UniversalConstants.OBJECT_DEF_ANNOT);
				percepts.add(repairCentrePercept);
			}
		}
		// note; at the moment assuming everything can see the 'map'
		for (Literal l : percepts) {
			addPercept(l);
		}
	}

	
	/*
	 * Stats info!
	 */
	private int actionsTried = 0, actionsPass = 0, actionsArgFail = 0, actionsPreFail = 0, actionsExoFail = 0,
			actionsRandFail = 0;

	public int getActionsTried() {
		return actionsTried;
	}

	public int getActionsPass() {
		return actionsPass;
	}

	public int getActionsArgFail() {
		return actionsArgFail;
	}

	public int getActionsPreFail() {
		return actionsPreFail;
	}

	public int getActionsExoFail() {
		return actionsExoFail;
	}

	public int getActionsRandFail() {
		return actionsRandFail;
	}

	DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss:SS");

	public void addDisplayString(String s) {
		s = dateFormat.format(new Date()) + "; " + s;
		synchronized (lastMessage) {
			if (lastMessage.size() >= MSG_Q_SIZE) {
				lastMessage.remove(); // remove top
			}
			lastMessage.add(s);
		}
		StatsRecorder.getInstance().recordString(s);
	}

	public Collection<String> getResultStrings() {
		synchronized (lastMessage) {
			return lastMessage;
		}
	}

	/**
	 * Gets a crude cost estimate
	 * 
	 * @param action
	 * @return
	 */
	private int getCost(Structure action) {
		if (action.getFunctor().equalsIgnoreCase("register")) {
			return 0;
		}
		if (action.getFunctor().equalsIgnoreCase(WorldAction.fly.name())) {
			return 8;
		}
		return 1; // default
	}

	/**
	 * Called by the agent infrastructure to schedule an action to be executed
	 * on the environment
	 */
	public void scheduleAction(final String agName, final Structure action, final Object infraData) {
		executor.execute(new ActionRunner(agName, action, infraData, stepper));
	}

	private class ActionRunner implements Runnable {
		private final String agName;
		private final Structure action;
		private final Object infraData;
		private final Stepper stepper;

		public ActionRunner(final String agName, final Structure action, final Object infraData,
				final Stepper stepper) {
			this.agName = agName;
			this.action = action;
			this.infraData = infraData;
			this.stepper = stepper;
		}

		@Override
		public void run() {
			try {
				boolean success = executeAction(agName, action);
				stepper.stepEnvironment();
				getEnvironmentInfraTier().actionExecuted(agName, action, success, infraData);
				if (!success) {
					logger.severe("Sent; Failed " + action + " by " + agName + " unto "
							+ getEnvironmentInfraTier().getClass());
				}

			} catch (Exception ie) {
				if (!(ie instanceof InterruptedException)) {
					logger.log(Level.WARNING, "act error!", ie);
				}
				throw new RuntimeException(ie);
			}
		}

	}

	/**
	 * Execute an action immediately
	 */
	@Override
	public boolean executeAction(String agName, Structure action) {
		lastAction = System.currentTimeMillis();
		logger.info("Execute; " + action);

		Literal actionClone = action.copy();
		actionClone.clearAnnots();

		StringTerm agentNameTerm = ASSyntax.createString(agName);
		if (action.getFunctor().equalsIgnoreCase("register")) {
			boolean registeredOk = registerAgent(agName, action);
			if (registeredOk) {
				addPercept(agName, ASSyntax.createLiteral(AG_REGISTERED, agentNameTerm));
			} else {
				logger.severe("Register failure; " + agName);
			}

			if (registeredOk) {
				addDisplayString("Registered " + agName);
			} else {
				addDisplayString(agName + " registration failed! ");
			}

			return registeredOk; // no log
		}

		if (action.getTerms().isEmpty() || !action.getTerm(0).toString().replaceAll("\"", "").equals(agName)) {
			logger.warning("Actor not specified in action structure as expected...");
		}

		if (action.getFunctor().equals(WorldAction.free.name())) {
			ActionResult result = doFree(agName, action);
			return result.equals(ActionResult.pass);
		}

		// just check we're not already busy....
		if (getVehicles().containsKey(agName)) {
			if (getVehicles().get(agName).isActing()) {
				logger.severe(agName + " is already acting, cannot do " + action + "\nCurrent Activity;"
						+ getVehicles().get(agName).getAction() + "\nHistory;" + lastMessage);
				addDisplayString("Forcewait: already acting, cannot do " + actionClone);
				long timeOut = System.currentTimeMillis() + 10000; // 10s
				while (getVehicles().get(agName).isActing() && System.currentTimeMillis() < timeOut) {
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
					}
				}

				if (getVehicles().get(agName).isActing()) {
					logger.severe(agName + " activity timeout without completion");
					return false;
				}
			}
		}

		// result to be returned
		// ok... add busy percept
		if (getVehicles().containsKey(agName)) {
			getVehicles().get(agName).setActing(action);
		} else if (commanders.contains(agName)) {
			logger.warning("Commander action; " + action);
		} else {
			logger.severe(agName + " not found, cannot do " + action);
			JOptionPane.showMessageDialog(getUi(),
					new String[] { "Unknown vehicle; " + agName, "Cannot perform " + action }, "Unknown vehicle!",
					JOptionPane.ERROR_MESSAGE);
			addDisplayString("Fail: unknown actor, cannot " + actionClone);
			return false; // SEVERE ERROR!!! This should never fire...
		}

		Literal busy = ASSyntax.createLiteral(BUSY, agentNameTerm);
		addPercept(busy);

		long time = System.nanoTime();

		actedSinceDemand = true;

		final ActionResult result = agentDoesAction(agName, action);

		time = System.nanoTime() - time;
		Analytics.getInstance().logTime(AnalyticsOperations.agentActing, time);
		// args - joint?
		Collection<String> involved = getInvolvedAgents(action, agents);
		for (String id : involved) {
			incrementActingTime(id, time / 1000000);
		}

		// record result
		if (getVehicles().containsKey(agName)) {
			getVehicles().get(agName).setActing(null);
		}
		StatsRecorder.getInstance().recordResult(agName, action, result);

		logger.info(result.name() + " " + agName + ":" + action);

		removePercept(busy);
		lastAction = System.currentTimeMillis(); // update again

		updateActivityResultCounters(result);

		// get actors
		Vector<String> actors = getActorsFromAction(action);

		if (!result.equals(ActionResult.pass) && !result.equals(ActionResult.fail_args)) {

			if (result.equals(ActionResult.fail_exogenous_change)
					|| result.equals(ActionResult.fail_nondeterministic)) {
				doDebilitationForFailure(action, actors);
			} // end exo/nd failure handling part
			else if (result.equals(ActionResult.fail_precond)) {
				doDebilitationForPreFail(action, actors);
			}
			// special case for 'load' actions
			if (action.getFunctor().equals(WorldAction.load)) {
				checkIfCargoToBeDestroyedForFailedLoad(action);
			}
		} // end failure result handling action
		else if (result.equals(ActionResult.pass)) {
			checkIfRandomDamageDuringSuccesfulActivity(actors);
		}

		String annot = "";
		if (!action.getAnnots(ANNOT_RESULT_INFO).isEmpty()) {
			annot = ((Literal) action.getAnnots(ANNOT_RESULT_INFO).get(0)).getTerm(0).toString();
		}

		synchronized (currentAction) {
			currentAction.remove(actionClone);
		}
		
		synchronized(lastActionPerformed) {
			lastActionPerformed.clear();
			lastActionPerformed.put(actionClone, result);
			addDisplayString(result + ": " + actionClone + " " + annot);
		}
		// do costing...
		incrementAccruedCost(agName, getCost(action));

		// note acting
		actedThisRequest.add(agName);

		// return true/false result value
		return result.equals(ActionResult.pass);
	}

	private ActionResult agentDoesAction(String agName, Structure action) {
		ActionResult result = ActionResult.fail_args;
		if (action.getFunctor().equalsIgnoreCase(WorldAction.move.name())) {
			result = doMove(agName, action);
		} else if (action.getFunctor().equalsIgnoreCase(WorldAction.load.name())) {
			result = doLoad(agName, action);
		} else if (action.getFunctor().equalsIgnoreCase(WorldAction.unload.name())) {
			result = doUnload(agName, action);
		} else if (action.getFunctor().equalsIgnoreCase(WorldAction.repair.name())) {
			result = doRepair(agName, action);
		} else if (action.getFunctor().equalsIgnoreCase(WorldAction.unblock.name())) {
			result = doUnblock(agName, action);
		} else if (action.getFunctor().equalsIgnoreCase(WorldAction.decontaminate.name())) {
			result = doDecontaminate(agName, action);
		} else if (action.getFunctor().equalsIgnoreCase(WorldAction.secureArea.name())) {
			result = doSecureArea(agName, action);
		} else if (action.getFunctor().equalsIgnoreCase(WorldAction.fly.name())) {
			result = doFly(agName, action);
		} else if (action.getFunctor().equalsIgnoreCase(WorldAction.takeOff.name())) {
			result = doTakeoff(agName, action);
		} else if (action.getFunctor().equalsIgnoreCase(WorldAction.land.name())) {
			result = doLand(agName, action);
		} else if (action.getFunctor().equalsIgnoreCase(WorldAction.loadOnto.name())) {
			result = doLoadOnto(agName, action);
		} else if (action.getFunctor().equalsIgnoreCase(WorldAction.unloadFrom.name())) {
			result = doUnloadFrom(agName, action);
		} else if (action.getFunctor().equalsIgnoreCase(WorldAction.consume.name())) {
			result = doConsume(agName, action);
		}
		return result;
	}

	private void checkIfRandomDamageDuringSuccesfulActivity(Vector<String> actors) {
		for (String s : actors) {
			if (simRandom.nextFloat() < consequences.getRandomDamageChance()) {
				logger.warning("Applying random damage!");
				Vehicle v = vehicleAgents.get(s);
				damageVehicle(v);
				addDisplayString(v.getId() + " was damaged during succesful execution");
			}
		}
	}

	private void doDebilitationForFailure(Structure action, Vector<String> actors) {
		for (String s : actors) {
			Vehicle v = vehicleAgents.get(s);
			if (simRandom.nextFloat() < consequences.getPostFailureDamageChance()) {
				logger.info(s + " damaged by failure of " + action);
				damageVehicle(v);
			}
			if (v.getCargo() != null && shouldDestroyCargoOnFailure()) {
				logger.warning(v.getId() + "; Destroying cargo due to failure " + action);
				destroyCargo(v, v.getCargo().getName());
			}
		}
	}

	private void doDebilitationForPreFail(Structure action, Vector<String> actors) {
		for (String s : actors) {
			if (simRandom.nextFloat() < consequences.getPostFailureDamageChance()) {
				logger.info(s + " damaged by failure of " + action);
				Vehicle v = vehicleAgents.get(s);
				damageVehicle(v);

				// destroy cargo on actors?
				if (v.getCargo() != null) {
					if (shouldDestroyCargoOnFailure()) {
						logger.warning(v.getId() + "; Destroying cargo due to failure " + action);
						destroyCargo(v, v.getCargo().getName());
					}
				} // end cargo null check
			} // end trigger check
		} // end for
	}

	private void checkIfCargoToBeDestroyedForFailedLoad(Structure action) {
		if (shouldDestroyCargoOnFailure()) {
			String locId = action.getTermsArray()[1].toString().replace("\"", "");
			String cId = action.getTermsArray()[2].toString().replace("\"", "");
			logger.warning(cId + "@" + locId + "; Destroying cargo due to failure " + action);
			destroyCargo(locId, cId);
		}
	}

	private synchronized void updateActivityResultCounters(final ActionResult result) {
		actionsTried++;
		if (result.equals(ActionResult.pass)) {
			actionsPass++;
		} else if (result.equals(ActionResult.fail_args)) {
			actionsArgFail++;
		} else if (result.equals(ActionResult.fail_exogenous_change)) {
			actionsExoFail++;
		} else if (result.equals(ActionResult.fail_nondeterministic)) {
			actionsRandFail++;
		} else if (result.equals(ActionResult.fail_precond)) {
			actionsPreFail++;
		}
	}

	private Vector<String> getActorsFromAction(Structure action) {
		Vector<String> actors = new Vector<String>();
		for (Term t : action.getTerms()) {
			String s = ((StringTerm) t).getString();
			if (vehicleAgents.keySet().contains(s)) {
				actors.add(s);
			}
		}
		return actors;
	}

	private void damageVehicle(Vehicle v) {
		HealthState vh = v.getHealth();
		if (vh.equals(HealthState.healthy)) {
			setVehicleHealth(v.getId(), HealthState.damaged);
		} else {
			setVehicleHealth(v.getId(), HealthState.mortal);
		}
	}

	private LinkedList<String> getInvolvedAgents(Structure act, Collection<String> agents) {
		List<Term> terms = act.getTerms();
		LinkedList<String> aNames = new LinkedList<String>();

		if (act.getTerms().isEmpty()) {
			return aNames;
		} // no further handling requireds

		// iterate through terms, setting agents...
		termLoop: for (int index = 0; index < terms.size(); index++) {
			Term t = terms.get(index);
			String tS = t.toString().replaceAll("\"", "");
			if (agents.contains(tS)) {
				aNames.addLast(tS);
			} else
				break termLoop; // done!
		}
		logger.finest("Action " + act + " involves " + aNames);
		if (aNames.isEmpty()) {
			logger.severe("No agents identified in " + act + ", based on set " + agents);
		}
		return aNames;
	}

	/**
	 * Registers a new agent in the environment
	 * 
	 * @param agName
	 * @param action
	 * @return
	 */
	protected boolean registerAgent(String agName, Structure action) {
		if (action.getTerms().size() > 0) {
			// i.e. for a vehicle, which specifies an initial junction location
			String jid = action.getTerm(0).toString().replaceAll("\"", "");

			if (jid.equals("?")) {
				// random
				// try and put helicopters into an airport if we can
				if (agName.startsWith(EntityType.helicopter.name()) && !getWorld().getAirports().isEmpty()) {
					Vector<String> ids = world.getAirports();
					jid = ids.get(simRandom.nextInt((ids.size() - 1)));
				} else if (agName.startsWith(EntityType.helicopter.name()) && getWorld().getAirports().isEmpty()) {
					logger.severe("No airports for heli!");
					return false;
				} else {
					Vector<String> ids = world.getJunctionNames();
					jid = ids.get(simRandom.nextInt((ids.size() - 1)));
				}
			}

			logger.info("Creating new vehicle " + agName + " at " + jid);
			JunctionInf j = world.getJunction(jid);
			if (j == null) {
				logger.warning("Vehicle initial location " + jid + " does not exist!");
				return false;
			}
			return registerVehicleAgent(agName, j);
		} else if (agName.startsWith("crane")) {
			// identify a junction without a crane
			final String id = matchCraneToJunctionId(agName);
			if (id.length() == 0) {
				return false;
			}
			JunctionInf j = world.getJunction(id);
			return registerVehicleAgent(agName, j);
		} else {
			// no terms, means a logical agent - i.e. a commander
			if (!commanders.contains(agName)) {
				commanders.add(agName);
				agents.add(agName);
				registerCommander(agName);
				return true;
			} else {
				logger.severe("Commander " + agName + " is already registered!");
				JOptionPane.showMessageDialog(getUi(), "Already registered commander!: " + agName);
				return false; // already exists
			}
		}
	}

	private String matchCraneToJunctionId(final String craneId) {
		if (this.cranelessJunctions.isEmpty()) {
			return "";
		} else {
			final String craneNum = craneId.substring("crane".length());
			final String jId = "j" + craneNum;
			if (this.cranelessJunctions.contains(jId)) {
				this.cranelessJunctions.removeElement(jId);
				return jId;
			} else {
				return "";
			}
		}
	}

	/**
	 * Setups percept(s) for a commander agent
	 * 
	 * @param agName
	 */
	private void registerCommander(String agName) {
		logger.info("Registered commander " + agName);
	}

	/**
	 * Registers an agent as controlling 'some' vehicle
	 * 
	 * @return
	 */
	private boolean registerVehicleAgent(String agName, JunctionInf j) {
		if (vehicleAgents.containsKey(agName)) {
			logger.warning("Duplicate register: " + agName + ", name taken!");
			return false;
		} else {
			logger.info("Registering " + agName + " as vehicle");
			newVehicle(agName, j);
			if (showUi) {
				getUi().addVehicle(agName);
			}
			return true;
		}
	}

	/**
	 * create a new vehicle and 'assign' to agName
	 * 
	 * @param agName
	 */
	private void newVehicle(String agName, JunctionInf j) {
		// create object
		Vehicle newVeh = vehicleFactory.create(agName);

		// location
		newVeh.setLocation(j.getCoordinate());
		vehicleAgents.put(agName, newVeh);
		agents.add(agName);
		initialVehicleLoc.put(agName, j.getId());

		// create percepts
		generateInitialVehiclePercepts(agName);
		logger.info("Created vehicle: " + newVeh);
	}

	/**
	 * Generate percepts for the named vehicle/agent - called at initialization
	 * 
	 * @param agName
	 */
	private void generateInitialVehiclePercepts(String agName) {
		if (vehicleAgents.containsKey(agName)) {
			logger.finest("Generating percepts for " + agName);
			Vehicle veh = vehicleAgents.get(agName);
			Point2D vLoc = veh.getLocationCoordinates();
			StringTerm agentId = ASSyntax.createString(veh.getId());
			Literal vPercept = ASSyntax.createLiteral("vehicle", agentId);
			vPercept.addAnnot(UniversalConstants.OBJECT_DEF_ANNOT);
			addPercept(vPercept);

			Literal typePercept = ASSyntax.createLiteral(veh.getType().name().toLowerCase(), agentId);
			typePercept.addAnnot(UniversalConstants.OBJECT_DEF_ANNOT);
			addPercept(typePercept);
			// coordinate location percept
			Literal atPercept = ASSyntax.createLiteral(AT_COORD, agentId, ASSyntax.createNumber(vLoc.getX()),
					ASSyntax.createNumber(vLoc.getY()));
			addPercept(atPercept);

			// might get a null pointer exception if allow non-junction
			// locations for
			// vehicle generation, i.e. for boats or helis
			if (Utils.getJunction(vLoc, world.getAllJunctions()) != null) {
				String jid = Utils.getJunction(vLoc, world.getAllJunctions()).getId();
				Literal atJPercept = ASSyntax.createLiteral(AT_JUNCTION, agentId, ASSyntax.createString(jid));
				addPercept(atJPercept);
			}

			// health state percept
			List<Literal> hp = getHealthPercepts(veh);
			for (Literal healthPcpt : hp) {
				addPercept(healthPcpt);
			}
		} else {
			logger.severe("Trying to form percepts for non-existent " + agName);
		}
	}

	protected ActionResult doFree(String agName, Structure action) {
		// name, rid, j1, j2
		String rid = action.getTerm(1).toString().replaceAll("\"", "");
		checkWhetherStuckAgentsCanMove(rid);

		if (stuckAgents.get(rid).contains(agName)) {
			logger.severe("Agent " + agName + " still in " + rid + " stuck list! " + stuckAgents.get(rid));
			return ActionResult.fail_args;
		}

		logger.severe(agName + " unstuck on " + rid);
		return ActionResult.pass;
	}

	/**
	 * Performs the specified move action, including precondition checks Vehicle
	 * travels down a road. Note that start point may be the road itself, in
	 * which case the 'from' denotes the direction
	 * 
	 * Args; agentId, roadId, startJunction, endJunction.
	 */
	protected ActionResult doMove(String agName, Structure action) {
		if (action.getArity() != 4) {
			logger.warning("Wrong action arity for doMove: " + action.getArity());
			action.addAnnot(ASSyntax.createLiteral(ANNOT_RESULT_INFO, ASSyntax.createString("Wrong arity")));
			return ActionResult.fail_args;
		}

		if (isStuck(agName)) {
			logger.severe(agName + " is stuck, fail; " + action);
			return ActionResult.fail_precond;
		}

		final Vehicle agent = getVehicles().get(agName);
		logger.info("doMove; Action = " + action.toString() + ", terms=" + action.getTerms() + ", location="
				+ agent.getLocationCoordinates());

		// get origin and destination; args are
		final String idStart = ((StringTerm) action.getTermsArray()[2]).getString();
		final String idEnd = ((StringTerm) action.getTermsArray()[3]).getString();
		final RoadInf toTravel = world.getRoad(((StringTerm) action.getTermsArray()[1]).getString());
		JunctionInf start = world.getJunction(idStart);
		if (start == null) {
			logger.severe("Move start (" + idStart + ")- " + start + " - does not exist!");
		}
		double distanceToStartJ = agent.getLocationCoordinates().distance(start.getCoordinate());
		boolean startOnR = distanceToStartJ > 0;

		// if we are close but not on the start, teleport!
		if (!(distanceToStartJ > 0) && (distanceToStartJ <= 0.001)) {
			// a short (rounding/safe margin) distance from start; move to
			// correct coords for junction
			logger.warning("Agent location co-ordinates (" + agent.getLocationCoordinates().getX() + ", "
					+ agent.getLocationCoordinates().getY() + ") within margin of error (distance = " + distanceToStartJ
					+ ") for " + start.getCoordinate() + ", so moving onto " + start.getId() + " position");
			agent.setLocation(start.getCoordinate());
		}

		final JunctionInf end = world.getJunction(idEnd);

		synchronized (getDangerZones()) {
			final Collection<String> dzId = getDangerZones().keySet();
			toTravel.getEnds()[0].setDangerous(dzId.contains(toTravel.getEndIds()[0]));
			toTravel.getEnds()[1].setDangerous(dzId.contains(toTravel.getEndIds()[1]));
		}

		// at j, can't use roadf
		if (!startOnR && !agent.canTravelAlong(toTravel)) {
			logger.warning(agName + " " + action + " unable to use road " + toTravel + ", speed=" + agent.maxSpeed(toTravel));
			action.addAnnot(ASSyntax.createLiteral(ANNOT_RESULT_INFO, ASSyntax.createString("Cannot use " + toTravel.getId())));
			return ActionResult.fail_precond;
		}
		// on road, can't travel to dest
		else if (startOnR && !agent.canTravelAlong(toTravel, end)) {
			logger.warning(agName + " " + action + " unable to use road " + toTravel + ", speed=" + agent.maxSpeed(toTravel));
			action.addAnnot(ASSyntax.createLiteral(ANNOT_RESULT_INFO, ASSyntax.createString("Cannot use " + toTravel.getId())));
			return ActionResult.fail_precond;
		}

		// special check - is start J a dz?
		if (agent.getType().equals(EntityType.apc) && !startOnR && start.isDangerous()) {
			logger.severe(agName + "@" + agent.getLocationCoordinates() + " cannot travel from DZ area " + start
					+ ", activity fail;" + action);
			action.addAnnot(ASSyntax.createLiteral(ANNOT_RESULT_INFO, ASSyntax.createString("DZ@" + start)));
			return ActionResult.fail_precond;
		}

		if (agent.getHealth().equals(HealthState.mortal)) {
			logger.warning(agName + " is mortally damaged");
			action.addAnnot(ASSyntax.createLiteral(ANNOT_RESULT_INFO, ASSyntax.createString("Mortal")));
			return ActionResult.fail_precond;
		}

		logger.info("Start moveVehicle");
		final ActionResult rt = moveVehicle(action, agent.getLocationCoordinates(), end.getCoordinate(), agent, toTravel,
				System.currentTimeMillis(), false);
		logger.info("moveVehicle result= " + rt);

		return rt;
	}

	/**
	 * Bulldozes a road from end-to-end (bulldoze(agent, road id, start, end))
	 * 
	 * @param agName
	 * @param action;
	 *            actor, road id, start (origin) junction, end junction
	 * @return
	 */
	protected ActionResult doUnblock(String agName, Structure action) {
		String actor = ((StringTerm) action.getTerm(0)).getString();
		String rId = ((StringTerm) action.getTerm(1)).getString();
		String originJ = ((StringTerm) action.getTerm(2)).getString();
		String endJ = ((StringTerm) action.getTerm(3)).getString();
		RoadInf road;
		JunctionInf origin, end;
		// check args
		if (!actor.equals(agName)) {
			logger.severe("Agent " + agName + " is not the defined actor in " + action);
			return ActionResult.fail_args;
		}

		if (isStuck(agName)) {
			logger.severe(agName + " is stuck, fail; " + action);
			return ActionResult.fail_precond;
		}

		if (!getWorld().getAllRoadsIds().contains(rId)) {
			logger.severe(
					"Road specified in " + action + " does not exist! (roads=" + getWorld().getAllRoadsIds() + ")");
			return ActionResult.fail_args;
		} else {
			// check junctions...
			road = getWorld().getRoad(rId);

			// set junctions / check junctions exist in road...
			JunctionInf[] js = road.getEnds();
			if (js[0].getId().equals(originJ) & js[1].getId().equals(endJ)) {
				origin = js[0];
				end = js[1];
			} else if (js[0].getId().equals(endJ) & js[1].getId().equals(originJ)) {
				origin = js[1];
				end = js[0];
			} else {
				logger.severe("A specified junction(s) in " + action + " not found in " + road);
				return ActionResult.fail_args;
			}

			// road state checks
			if (!road.isBlocked()) {
				logger.info("Trying to unblock a cleared road, so already ok (" + action + ")");
				return ActionResult.pass;
			}

			if(road.isSlippery() && road.getRoadType().equals(RoadType.mud)){ 
				logger.severe("Can't unblock a slippery mud road, " + action + " failed!");
				action.addAnnot(ASSyntax.createLiteral(ANNOT_RESULT_INFO, ASSyntax.createString("Slippery mud road")));
				return ActionResult.fail_precond;
			}
			
			if (road.isFlooded()) {
				logger.severe("Can't unblock a flooded road, " + action + " failed!");
				action.addAnnot(ASSyntax.createLiteral(ANNOT_RESULT_INFO, ASSyntax.createString("Flooded road")));
				return ActionResult.fail_precond;
			}

			if (road.isToxic()) {
				logger.severe("Can't unblock a toxic road, " + action + " failed!");
				action.addAnnot(ASSyntax.createLiteral(ANNOT_RESULT_INFO, ASSyntax.createString("Toxic road")));
				return ActionResult.fail_precond;
			}
		}

		// get agent
		Vehicle agent = this.getVehicles().get(actor);
		if (!agent.isCapable(WorldAction.unblock)) {
			logger.severe("Agent is not capable of action! " + action);
			return ActionResult.fail_args;
		}

		// done args check, got results. now can perform action if we reach this
		// point.
		// long to preserve consistency of currentTimeMillis value
		long startTime = System.currentTimeMillis();
		logger.finer(agent.getId() + " starting move for " + action);
		road.setBlocked(false);
		final ActionResult moveOk = moveVehicle(action, origin.getCoordinate(), end.getCoordinate(), agent, road, startTime, true);
		road.setBlocked(true);
		final String result = (moveOk.equals(ActionResult.pass)) ? "completed" : "failed";
		logger.finer(agent.getId() + " " + result + " move for  " + action);

		// successful - unblock the road at end!
		if (moveOk.equals(ActionResult.pass)) {
			openRoad(road.getId());
			logger.finer("Unblocked road " + road.getId() + " for successful " + action);
		}
		return moveOk;
	}

	/**
	 * Consumes cargo located at a junction
	 * 
	 * @param agName
	 * @param action
	 *            - allocate(loghq, location, cargo)
	 * @return true if able to allocated successfully
	 */
	protected ActionResult doConsume(String agName, Structure action) {
		String cargoId = ((StringTerm) action.getTerm(2)).getString();
		String location = ((StringTerm) action.getTerm(1)).getString();
		JunctionInf j = world.getJunction(location);
		Cargo c = getCargoCounts().get(location).get(cargoId);

		Literal aClone = action.copy();
		aClone.clearAnnots();
		synchronized (currentAction) {
			currentAction.put(aClone, 1f);
		}

		if (j == null || c == null) {
			logger.severe("unable to find cargo or junction " + action);
		} else {
			consume(j, c);
		}

		return ActionResult.pass;
	}

	/**
	 * Decontaminate a road from specified start->end; this is very similar in
	 * effect to the bulldoze action, although preconds and effects differ.
	 * 
	 * @param agName
	 * @param action
	 * @return
	 */
	protected ActionResult doDecontaminate(String agName, Structure action) {
		String actor = ((StringTerm) action.getTerm(0)).getString();
		String rId = ((StringTerm) action.getTerm(1)).getString();
		String originJ = ((StringTerm) action.getTerm(2)).getString();
		String endJ = ((StringTerm) action.getTerm(3)).getString();
		RoadInf road;
		JunctionInf origin, end;
		// check args
		if (!actor.equals(agName)) {
			logger.severe("Agent " + agName + " is not the defined actor in " + action);
			return ActionResult.fail_args;
		}

		if (isStuck(agName)) {
			logger.severe(agName + " is stuck, fail; " + action);
			return ActionResult.fail_precond;
		}

		if (!getWorld().getAllRoadsIds().contains(rId)) {
			logger.severe(
					"Road specified in " + action + " does not exist! (roads=" + getWorld().getAllRoadsIds() + ")");
			return ActionResult.fail_args;
		} else {
			// check junctions...
			road = getWorld().getRoad(rId);

			// set junctions / check junctions exist in road...
			JunctionInf[] js = road.getEnds();
			if (js[0].getId().equals(originJ) & js[1].getId().equals(endJ)) {
				origin = js[0];
				end = js[1];
			} else if (js[0].getId().equals(endJ) & js[1].getId().equals(originJ)) {
				origin = js[1];
				end = js[0];
			} else {
				logger.severe("A specified junction(s) in " + action + " not found in " + road);
				return ActionResult.fail_args;
			}

			// road state checks
			if (!road.isToxic()) {
				logger.info("Trying to decon a clean road, so already ok (" + action + ")");
				action.addAnnot(ASSyntax.createLiteral(ANNOT_RESULT_INFO, ASSyntax.createString("Noop")));
				return ActionResult.pass;
			}

			if (road.isFlooded()) {
				logger.severe("Can't decon a flooded road, " + action + " failed!");
				action.addAnnot(ASSyntax.createLiteral(ANNOT_RESULT_INFO, ASSyntax.createString("Flooded")));
				return ActionResult.fail_precond;
			}
			else if(road.getRoadType().equals(RoadType.mud) && road.isSlippery()) {
				logger.severe("Can't decon a slippery mud road, " + action + " failed!");
				action.addAnnot(ASSyntax.createLiteral(ANNOT_RESULT_INFO, ASSyntax.createString("Slippery Mud")));
				return ActionResult.fail_precond;
			}

			if (road.isBlocked()) {
				logger.severe("Can't clean a blocked road, " + action + " failed!");
				action.addAnnot(ASSyntax.createLiteral(ANNOT_RESULT_INFO, ASSyntax.createString("Blocked")));
				return ActionResult.fail_precond;
			}
		}

		// get agent
		Vehicle agent = this.getVehicles().get(actor);
		if (!agent.isCapable(WorldAction.decontaminate)) {
			logger.severe("Agent is not capable of action! " + action);
			return ActionResult.fail_args;
		}

		// done args check, got results. now can perform action if we reach this point.
		// long to preserve consistency of currentTimeMillis value
		final long startTime = System.currentTimeMillis();
		logger.finer(agent.getId() + " starting move for  " + action);
		road.setToxic(false);
		final ActionResult moveOk = moveVehicle(action, origin.getCoordinate(), end.getCoordinate(), agent, road, startTime, true);
		road.setToxic(true);
		
		final String result= (moveOk.equals(ActionResult.pass))? "completed": "failed";
		logger.finer(agent.getId() + " " + result + " move for  " + action);

		// successful - unblock the road at end!
		if (moveOk.equals(ActionResult.pass)) {
			cleanRoad(road.getId());
			logger.finer("Unblocked road " + road.getId() + " for successful " + action);
		}
		return moveOk;
	}

	/**
	 * Performs the actions of moving a vehicle over time...
	 * 
	 * @param start
	 * @param end
	 * @param agent
	 * @param startTime
	 * @param travelTime
	 */
	private ActionResult moveVehicle(final Structure action, final Point2D start, final Point2D end,
			final Vehicle agent, final RoadInf road, final long startTime, final boolean skipTraversalCheck) {
		long current = 0;

		StringTerm aidL = ASSyntax.createString(agent.getId());
		StringTerm agentId = aidL;
		HealthState initialHealth = agent.getHealth();

		logger.fine(agent + " beginning move " + agent.getLocationCoordinates() + " to " + end);

		// check for random failure
		final boolean randFail = this.isRandomFailure(action, agent);
		
		// set random failure distance...
		final double distanceFail = road.getLength() * simRandom.nextDouble();
		double distanceTravelled = 0;
		agent.setActing(action); // still working!

		JunctionInf startJ = Utils.getJunction(start, world.getAllJunctions());
		JunctionInf endJ = Utils.getJunction(end, world.getAllJunctions());
		// check for start on a road...
		if (startJ == null) {
			logger.info("Start position (" + start + ")is on road " + road.getId() + ": using an endpoint");
			if (road.getEndIds()[0].equals(endJ.getId())) {
				startJ = world.getJunction(road.getEndIds()[1]);
			} else if (road.getEndIds()[1].equals(endJ.getId())) {
				startJ = world.getJunction(road.getEndIds()[0]);
			} else /* 1 = end, start = 0 */ {
				logger.severe("Move error, cannot match id of end " + endJ + " to any junction in " + road);
			}
		}

		boolean rAdded = false;
		final Literal initialAt = ASSyntax.createLiteral(AT_COORD, agentId, ASSyntax.createNumber(start.getX()),
				ASSyntax.createNumber(start.getY()));
		final Literal roadPercept = ASSyntax.createLiteral(ON_ROAD, agentId, ASSyntax.createString(road.getId()));
		final Literal startPercept = ASSyntax.createLiteral(AT_JUNCTION, agentId,
				ASSyntax.createString(startJ.getId()));
		final Literal endPercept = ASSyntax.createLiteral(AT_JUNCTION, agentId, ASSyntax.createString(endJ.getId()));
		
		while (distanceTravelled < road.getLength()) {
			lastAction = System.currentTimeMillis();

			if (distanceTravelled > 0) {
				removePercept(startPercept);
				removePercept(initialAt);
			}
			
			/*
			 * EXOGENOUS CHANGE CASE ONLY
			 */
			// Health state checks - debilitation may occur in transit!
			if (!initialHealth.equals(agent.getHealth())) {
				// update travel time
				if (agent.getHealth().equals(HealthState.mortal)) {
					// consequent failure
					logger.warning("Agent incurred mortal damage, stopped at " + agent.getLocationCoordinates());
					action.addAnnot(ASSyntax.createLiteral(ANNOT_RESULT_INFO, ASSyntax.createString("Mortal damage")));
					agent.setActing(null);

					// NB: need to consider timing of BUSY percept wrt damage
					// percept...
					if (agent.getLocationCoordinates().equals(end)) {
						removePercept(roadPercept);
						addPercept(endPercept);
					} else {// on road
						removePercept(startPercept);
						addPercept(roadPercept);
					}

					// actual position update
					removePercept(initialAt);
					addPercept(ASSyntax.createLiteral(AT_COORD, agentId,
							ASSyntax.createNumber(agent.getLocationCoordinates().getX()),
							ASSyntax.createNumber(agent.getLocationCoordinates().getY())));

					// if carrying hazardous waste, spill it and close the road
					// IF threshold met
					float spillChance = simRandom.nextFloat();
					spillLoop: if ((agent.getCargo() != null)
							&& (spillChance < consequences.getCargoSpillAfterFailureChance())) {
						Cargo c = agent.getCargo();
						if (c.getType().equals(Cargo.Type.Hazardous)) {
							spillCargo(road.getId());
							break spillLoop;
						}
					}

					updateTravelDistance(agent.getId(), distanceTravelled);
					if (agent.getLocationCoordinates().equals(end)) {
						removePercept(roadPercept);
						addPercept(endPercept);
					} else {// on road
						removePercept(startPercept);
						addPercept(roadPercept);
					}
					return ActionResult.fail_exogenous_change; // failure
				}
				/*
				 * Other cases the 'getmaxspeed' method should automatically
				 * return the correct speed and thus the appropriate progress be
				 * calculated
				 */
				else {
					logger.finest("Moving agent is " + agent.getHealth().name());
				}
			}

			// update position
			current += SIM_STEP_TIME;

			// check road is still traversable first!
			// update dzs (hack)
			synchronized (getDangerZones()) {
				Collection<String> dzId = getDangerZones().keySet();
				if (dzId.contains(road.getEndIds()[0]) && !road.getEnds()[0].isDangerous()) {
					road.getEnds()[0].setDangerous(true);
				}
				if (dzId.contains(road.getEndIds()[1]) && !road.getEnds()[1].isDangerous()) {
					road.getEnds()[1].setDangerous(true);
				}
			}

			if (!agent.canTravelAlong(road, endJ) && !skipTraversalCheck) {
				logger.warning(agent.getId() + " is stuck due to conditions change at " + agent.getLocationCoordinates()
						+ " after " + distanceTravelled + " on " + road.toString());
				addDisplayString(agent.getId() + " is stuck due to conditions change at "
						+ agent.getLocationCoordinates() + " on " + road.getId() + ( agent.getCargo()!=null?", cargo? " + agent.getCargo():""));

				synchronized (stuckTime) {
					Vector<String> stuckA = new Vector<String>();
					if (getStuckAgents().containsKey(road.getId())) {
						stuckA = getStuckAgents().get(road.getId());
					}
					stuckA.add(agent.getId());
					getStuckAgents().put(road.getId(), stuckA);
					stuckTime.put(agent.getId(), getGenDemands() + consequences.getUnstickingDelayTime());
				};

				addPercept(ASSyntax.createLiteral(AT_COORD, agentId,
						ASSyntax.createNumber(agent.getLocationCoordinates().getX()),
						ASSyntax.createNumber(agent.getLocationCoordinates().getY())));
				addPercept(ASSyntax.createLiteral(STUCK, aidL));
				if (distanceTravelled > 0) {
					updateTravelDistance(agent.getId(), distanceTravelled);
					action.addAnnot(ASSyntax.createLiteral(ANNOT_RESULT_INFO, ASSyntax.createString("Got stuck")));
					return ActionResult.fail_exogenous_change;
				} else {
					return ActionResult.fail_precond;
				}
			} else {
				logger.finer(agent.getId() + " is still good on " + road + " road length=" + road.getLength());
			}

			float maxSpeed = agent.maxSpeed(road);
			logger.finer(agent.getId() + " has max speed " + maxSpeed);
			
			final float travelDistance = Cargoworld.SPEED_TIME_SCALAR * (SIM_STEP_TIME * maxSpeed);
			
			if(travelDistance > road.getLength()/2) {
				logger.info("Decrement travel distance to half");
				distanceTravelled += road.getLength()/2;
			}
			else {
				distanceTravelled += travelDistance;
			}
			
			if (maxSpeed == 0 && distanceTravelled == 0) {
				logger.finer(agent.getId() + " hit max speed zero error case!");
				// increment so we get stuck on the road properly...
				distanceTravelled = Cargoworld.SPEED_TIME_SCALAR * 0.5;
			}
			
			logger.finer(agent.getId() + " has moved " + distanceTravelled);

			// random failure check!
			if (randFail & distanceTravelled >= distanceFail) {
				performNonDeterministicMoveFailureUpdates(agent, road, aidL, agentId, roadPercept, startPercept);
				updateTravelDistance(agent.getId(), distanceTravelled);
				return ActionResult.fail_nondeterministic;
			}
			// progress = 1 is equivalent to 100% progress, i.e. completion!
			if (distanceTravelled >= road.getLength()) {
				logger.fine(distanceTravelled + ":End of the road (" + road.getId() + ") for " + agent.getId());
				agent.setLocation(end);
			} else if ((distanceTravelled > 0) && (distanceTravelled < road.getLength())) {
				logger.finest(distanceTravelled + ": travelled by " + agent.getId());

				if (!rAdded) {
					rAdded = true;
					addPercept(roadPercept); // add only once!
				}

				// x% along the length of the line... presume a straight line -
				// calculated proportion travelled so far

				double progress = distanceTravelled / road.getLength();
				double newX = start.getX() + (progress * (end.getX() - start.getX()));
				double newY = start.getY() + (progress * (end.getY() - start.getY()));

				// new location / percept add
				agent.setLocation(new Point2D.Double(newX, newY));
				try {
					Thread.sleep(SIM_STEP_TIME);
				} catch (InterruptedException e) {
					logger.warning(agent.getId() + " on " + road.getId() + "-> Move op interrrupted! " + e.toString());
				}
			} else if (distanceTravelled == 0) {
				logger.warning(agent.getId() + " did not move any distance in this iteration");
				throw new RuntimeException("I have no idea why, but agent failed to move!");
			} else { // should never hit here?
				logger.severe(agent.getId() + " unhandled 'else' reached at moveVehicle(?!)");
				throw new RuntimeException("I have no idea why!");
			}
		} // end 'while' used for movement

		// check for error we get with moves of duration=0ms
		if (!agent.getLocationCoordinates().equals(end)) {
			logger.info("Doing bug fix hack...");
			executor.execute(new Runnable() {
				@Override
				public void run() {
					agent.setLocation(end);
				}
			});
		}

		agent.setActing(null);
		removePercept(startPercept); // just in case...
		removePercept(initialAt);
		removePercept(roadPercept);
		addPercept(endPercept);
		addPercept(
				ASSyntax.createLiteral(AT_COORD, agentId, ASSyntax.createNumber(agent.getLocationCoordinates().getX()),
						ASSyntax.createNumber(agent.getLocationCoordinates().getY())));

		logger.fine(
				agent.getId() + " completed move at " + current + " ms, percepts=" + consultPercepts(agent.getId()));
		updateTravelDistance(agent.getId(), distanceTravelled);
		return ActionResult.pass;
	}

	private void performNonDeterministicMoveFailureUpdates(final Vehicle agent, final RoadInf road, StringTerm aidL,
			StringTerm agentId, final Literal roadPercept, final Literal startPercept) {
		agent.setActing(null);
		addPercept(roadPercept);
		removePercept(startPercept);// just in case

		// if carrying hazardous waste, spill it
		if ((agent.getCargo() != null) && (agent.getCargo().getType().equals(Cargo.Type.Hazardous))) {
			spillCargo(road.getId());
		}
		
		if(willGetStuck()) {
			synchronized (stuckTime) {
				Vector<String> stuckA = new Vector<String>();
				if (getStuckAgents().containsKey(road.getId())) {
					stuckA = getStuckAgents().get(road.getId());
				}
				stuckA.add(agent.getId());
				getStuckAgents().put(road.getId(), stuckA);
				// lastStuck.put(agent.getId(), road.getId());
				stuckTime.put(agent.getId(), getGenDemands() + consequences.getUnstickingDelayTime());
			}

			addDisplayString(agent.getId() + " is stuck at " + agent.getLocationCoordinates() + " on "
					+ road.toString() + ", cargo? " + agent.getCargo());
			addPercept(ASSyntax.createLiteral(STUCK, aidL));
			addPercept(ASSyntax.createLiteral(RESTING, aidL));
		}
		addPercept(ASSyntax.createLiteral(AT_COORD, agentId,
				ASSyntax.createNumber(agent.getLocationCoordinates().getX()),
				ASSyntax.createNumber(agent.getLocationCoordinates().getY())));
	}

	private boolean willGetStuck() {
		return this.simRandom.nextFloat()<consequences.getStuckChance();
	}

	/**
	 * Updates (increments) the distance travelled by an agent
	 * 
	 * @param id
	 *            - agent identified
	 * @param dist
	 *            - update value
	 */
	private void updateTravelDistance(String id, double dist) {
		float orig = 0;
		if (travelledDistance.containsKey(id)) {
			orig = travelledDistance.get(id);
		}
		travelledDistance.put(id, (float) (orig + dist));
	}

	private void incrementActingTime(String id, long add) {
		long time = 0;
		if (actingTime.containsKey(id)) {
			time = actingTime.get(id);
		}
		actingTime.put(id, time + add);
	}

	/**
	 * Get distance travelled for a given agent
	 */
	public float getDistanceTravelled(String agentId) {
		if (!travelledDistance.containsKey(agentId)) {
			travelledDistance.put(agentId, 0f);
			return 0;
		}
		return travelledDistance.get(agentId);
	}

	public float avgDistanceTravelled() {
		if (travelledDistance.isEmpty()) {
			return 0;
		}
		float avg = 0;
		for (Float val : travelledDistance.values()) {
			avg += val;
		}
		avg = totalDistanceTravelled() / vehicleAgents.entrySet().size();
		return avg;
	}

	public float totalDistanceTravelled() {
		float tot = 0;
		for (Float val : travelledDistance.values()) {
			tot += val;
		}
		return tot;
	}

	public float avgActingTime() {
		if (actingTime.isEmpty()) {
			return 0;
		}
		float avg = 0;
		for (long val : actingTime.values()) {
			avg += val;
		}
		avg = totalActingTime() / vehicleAgents.entrySet().size();
		return avg;
	}

	/**
	 * Returns the total ms spend acting by agents, including for joint
	 * activities (i.e. if agent a and b act together for 10ms, 20ms is
	 * returned)
	 * 
	 * @return
	 */
	public long totalActingTime() {
		long tot = 0l;
		for (long val : actingTime.values()) {
			tot += val;
		}
		return tot;
	}

	private void incrementAccruedCost(String agId, int accrued) {
		synchronized (activityCosts) {
			int cost = 0;
			if (activityCosts.containsKey(agId)) {
				cost = activityCosts.get(agId);
			}
			activityCosts.put(agId, cost + accrued);
		}
	}

	/**
	 * Average cost of each cargo delivered
	 * 
	 * @return
	 */
	public float averageActivityCost() {
		if (satisfied == 0) {
			return totalActivityCost();
		} else {
			return totalActivityCost() / satisfied;
		}
	}

	public int totalActivityCost() {
		synchronized (activityCosts) {
			int tot = 0;
			for (long val : activityCosts.values()) {
				tot += val;
			}
			return tot;
		}
	}

	/**
	 * create cargo spillage on a road after movement failure
	 * 
	 * @param road
	 */
	public void spillCargo(String roadId) {
		RoadInf road = world.getRoad(roadId);
		if (!road.isToxic() && !road.isBlocked()) {
			StringTerm roadA = ASSyntax.createString(road.getEndIds()[0]);
			StringTerm roadB = ASSyntax.createString(road.getEndIds()[1]);
			StringTerm rid = ASSyntax.createString(road.getId());

			logger.severe("Spilling cargo; " + road.getId());
			road.setToxic(true);
			addPercept(ASSyntax.createLiteral(TOXIC, roadA, roadB));
			addPercept(ASSyntax.createLiteral(TOXIC, roadB, roadA));
			addPercept(ASSyntax.createLiteral(TOXIC_ROAD, rid));

			addDisplayString("Spilled cargo! " + road.getId());
		} else if (road.isBlocked()) {
			logger.finer("Can't spill on blocked " + road.getId());
		}
	}

	public void cleanRoad(String roadId) {
		RoadInf road = world.getRoad(roadId);
		if (road.isToxic() && !road.isBlocked()) {
			StringTerm roadA = ASSyntax.createString(road.getEndIds()[0]);
			StringTerm roadB = ASSyntax.createString(road.getEndIds()[1]);
			StringTerm rid = ASSyntax.createString(road.getId());
			logger.finer("Cleaning up " + road.getId());
			road.setToxic(false);
			checkWhetherStuckAgentsCanMove(roadId);
			removePercept(ASSyntax.createLiteral(TOXIC, roadA, roadB));
			removePercept(ASSyntax.createLiteral(TOXIC, roadB, roadA));
			removePercept(ASSyntax.createLiteral(TOXIC_ROAD, rid));
		}
	}

	/**
	 * Destroy cargo
	 */
	public void destroyCargo(Vehicle carrying, String cargo) {
		logger.severe("Destroying cargo " + cargo + " held by " + carrying);
		removePercept(
				ASSyntax.createLiteral(LOADED, ASSyntax.createString(carrying.getId()), ASSyntax.createString(cargo)));
		removePercept(ASSyntax.createLiteral(CARRYING_CARGO, ASSyntax.createString(carrying.getId())));
		Literal cargoTypePercept = ASSyntax.createLiteral(CARGO_TYPE, ASSyntax.createString(cargo));
		cargoTypePercept.addAnnot(UniversalConstants.OBJECT_DEF_ANNOT);
		removePercept(cargoTypePercept);
		carrying.unloadCargo(cargo);
		addDisplayString("Destroyed " + cargo + " held by " + carrying + ", free " + sumCargoFree());
	}

	/**
	 * Cancel all extant request percepts so we can send a 'new' one
	 */
	public void removeAllRequests() {
		for (JunctionInf j : world.getAllJunctions()) {
			String key = j.getId();
			if (cargoRequests.get(key) > 0) {
				Literal cancel = ASSyntax.createLiteral(CARGO_NEEDED, ASSyntax.createString(key));
				logger.info("Cancelling percept " + cancel + " for " + commanders);
				for (final String id : commanders) {
					removePercept(id, cancel);
					informAgsEnvironmentChanged(id); // force removal so we have
														// a '+' event on next
														// addition...
				}
			}
		}
	}

	public void destroyAllCargo() {
		for (String l : cargoCount.keySet()) {
			destroyAllCargo(l);
		}
	}

	/**
	 * Destroys all cargo at a gven location
	 * 
	 * @param location
	 */
	public void destroyAllCargo(String location) {
		Map<String, Cargo> cSet = cargoCount.get(location);
		Vector<String> delCId = new Vector<String>();
		if (!cSet.isEmpty()) {
			delCId.addAll(cSet.keySet());
			for (String s : delCId) {
				destroyCargo(location, s);
			}
		}
		// else... return, nothing to delete
	}

	/**
	 * Destroys named cargo at a given location
	 * 
	 * @param location
	 * @param cargo
	 */
	public void destroyCargo(String location, String cargo) {
		synchronized (cargoCount) {
			logger.severe("Destroying cargo " + cargo + " at " + location);
			Cargo c = cargoCount.get(location).remove(cargo);
			// delete percept
			Literal cargoTypePercept = ASSyntax.createLiteral(CARGO_TYPE, ASSyntax.createString(cargo));
			cargoTypePercept.addAnnot(UniversalConstants.OBJECT_DEF_ANNOT);
			Literal atPercept = ASSyntax.createLiteral(CARGO_AT, ASSyntax.createString(cargo),
					ASSyntax.createString(location));
			removePercept(cargoTypePercept);
			removePercept(atPercept);
			addDisplayString("Destroyed " + c + "@" + location + ", free " + sumCargoFree());
		}
	}

	private ActionResult doLoadOnto(String agName, Structure action) {
		// "loadOnto(AGENT, J, RECIPIENT, CARGO)"
		if (action.getArity() != 4) {
			logger.warning("Wrong action arity for doLoad: " + action.getArity());
			return ActionResult.fail_args;
		}

		float waitTime = 0;
		final Vehicle crane = vehicleAgents.get(agName);
		// check preconditions hold
		final String jId = action.getTermsArray()[1].toString().replace("\"", ""); // only
																					// entry
		final String recipientId = action.getTermsArray()[2].toString().replace("\"", ""); // only
																							// entry
		final String cargoId = action.getTermsArray()[3].toString().replace("\"", ""); // only
																						// entry
		final JunctionInf j = world.getJunction(jId);
		final Vehicle recipient = vehicleAgents.get(recipientId);

		// check agent health
		if (crane.getHealth().equals(HealthState.mortal)) {
			logger.warning("doLoad failure due to health state of " + agName);
			action.addAnnot(ASSyntax.createLiteral(ANNOT_RESULT_INFO, ASSyntax.createString("Mortal")));
			return ActionResult.fail_precond;
		}

		// type
		if (!(isCrane(crane) && (isHelicopter(recipient) || isTruck(recipient)))) {
			action.addAnnot(ASSyntax.createLiteral(ANNOT_RESULT_INFO, ASSyntax.createString("Wrong agent type")));
			return ActionResult.fail_precond;
		}
		// location
		if (!isAt(crane, j) || !isAt(recipient, j)) {
			action.addAnnot(ASSyntax.createLiteral(ANNOT_RESULT_INFO, ASSyntax.createString("Agent location error")));
			return ActionResult.fail_precond;
		}
		// not dangerzone
		if (j.isDangerous()) {
			action.addAnnot(ASSyntax.createLiteral(ANNOT_RESULT_INFO, ASSyntax.createString(jId + " is dangerous")));
			return ActionResult.fail_precond;
		}
		// crane carrying cargo
		if (!isCarryingThisCargo(cargoId, crane)) {
			action.addAnnot(ASSyntax.createLiteral(ANNOT_RESULT_INFO,
					ASSyntax.createString(agName + " not carrying " + cargoId)));
			return ActionResult.fail_precond;
		}

		// recipient not carrying cargo
		if (!recipient.canCarryCargo()) {
			action.addAnnot(ASSyntax.createLiteral(ANNOT_RESULT_INFO,
					ASSyntax.createString(recipientId + " can't take cargo")));
			return ActionResult.fail_precond;
		}

		boolean randFail = isRandomFailure(action, crane);
		if (randFail) {
			waitTime = waitTime * simRandom.nextFloat();
		}

		try {
			Thread.sleep((long) waitTime);
		} catch (InterruptedException e) {
			logger.warning("Interrupted 'doTime'; " + e.toString());
			e.printStackTrace();
		}

		if (!randFail) {
			final Cargo unloaded = crane.unloadCargo(cargoId);
			recipient.loadCargo(unloaded);
			final Literal originalLoadedPercept = ASSyntax.createLiteral(LOADED, ASSyntax.createString(agName),
					ASSyntax.createString(cargoId));
			final Literal newLoadedPercept = ASSyntax.createLiteral(LOADED, ASSyntax.createString(recipientId),
					ASSyntax.createString(cargoId));
			final Literal originalCarryingPercept = ASSyntax.createLiteral(CARRYING_CARGO,
					ASSyntax.createString(agName));
			final Literal newCarryingPercept = ASSyntax.createLiteral(CARRYING_CARGO,
					ASSyntax.createString(recipientId));
			this.removePercept(originalCarryingPercept);
			this.removePercept(originalLoadedPercept);
			this.addPercept(newCarryingPercept);
			this.addPercept(newLoadedPercept);
			return ActionResult.pass;
		} else { // randFail - cargo remains loaded
			final Type cargoType = crane.getCargo().getType();
			// is the cargo destroyed / spilled?
			if (shouldDestroyCargoOnFailure()) {
				destroyCargo(crane, crane.getCargo().getName());
			}
			doRandomSpill(j, cargoType);
			return ActionResult.fail_nondeterministic;
		}
	}

	private boolean shouldDestroyCargoOnFailure() {
		return simRandom.nextFloat() < consequences.getCargoDestroyAfterFailureChance();
	}

	/**
	 * Vehicle loads an identified container onto itself; load(object)
	 * 
	 * @param agName
	 * @param action
	 * @return
	 */
	private ActionResult doLoad(String agName, Structure action) {
		final Vehicle ag = vehicleAgents.get(agName);

		// params; agent, location of cargo, name (id) of specific cargo object
		if (action.getArity() != 3) {
			logger.warning("Wrong action arity for doLoad: " + action.getArity());
			return ActionResult.fail_args;
		}

		// check agent health
		if (ag.getHealth().equals(HealthState.mortal)) {
			logger.warning("doLoad failure due to health state of " + agName);
			action.addAnnot(ASSyntax.createLiteral(ANNOT_RESULT_INFO, ASSyntax.createString("Mortal")));
			return ActionResult.fail_precond;
		}

		float waitTime = ag.getHealth().equals(HealthState.damaged) ? VEHICLE_STD_LOAD_TIME
				: VEHICLE_STD_LOAD_TIME * 0.7f; // ms

		// extract terms
		final String loadAt = action.getTermsArray()[1].toString().replace("\"", ""); // only
																						// entry
		final String cargoName = action.getTermsArray()[2].toString().replace("\"", ""); // only
																							// entry

		final JunctionInf loadJ = this.world.getJunction(loadAt);

		// if heli, is NOT flying?
		if (ag instanceof Helicopter && ((Helicopter) ag).isFlying()) {
			logger.severe("PC fail: " + agName + " is currently flying! " + action);
			action.addAnnot(ASSyntax.createLiteral(ANNOT_RESULT_INFO, ASSyntax.createString("Flying")));
			return ActionResult.fail_precond;
		}

		// check preconditions hold for action...
		// 1 - cargo at location?
		if (!cargoCount.get(loadJ.getId()).containsKey(cargoName)) {
			logger.severe("PC fail: " + cargoName + " is not at " + loadJ + ", cargo here is "
					+ cargoCount.get(loadJ.getId()));
			action.addAnnot(
					ASSyntax.createLiteral(ANNOT_RESULT_INFO, ASSyntax.createString("Couldn't find " + cargoName)));
			return ActionResult.fail_precond;
		}

		// 2 - vehicle can load cargo?
		if (!ag.canCarryCargo()) {
			logger.severe("PC fail: " + agName + " cannot carry cargo " + cargoName + "; cargo==" + ag.getCargo());
			action.addAnnot(
					ASSyntax.createLiteral(ANNOT_RESULT_INFO, ASSyntax.createString("Can't carry " + cargoName)));
			return ActionResult.fail_precond;
		}

		Type type = cargoCount.get(loadJ.getId()).get(cargoName).getType();

		// 3 - vehicle at location?
		if (!isAt(ag, loadJ)) {
			logger.severe("PC fail: " + agName + " is not at " + loadJ + ", is at " + ag.getLocationCoordinates());
			return ActionResult.fail_args;
		}

		// remove the 'at' percept for the cargo as we try to load it...
		removePercept(ASSyntax.createLiteral(CARGO_AT, ASSyntax.createString(cargoName),
				ASSyntax.createString(loadJ.getId())));

		boolean randFail = isRandomFailure(action, ag);
		if (randFail) {
			waitTime = waitTime * simRandom.nextFloat();
		}

		// now do a wait to simulate the operation time delay
		try {
			Thread.sleep((long) waitTime);
		} catch (InterruptedException e) {
			logger.warning("Interrupted 'doTime'; " + e.toString());
			e.printStackTrace();
		}

		// state change / percept generation
		Map<String, Cargo> cSet = cargoCount.get(loadJ.getId());
		logger.fine("Cargo at " + loadJ.getId() + " = " + cSet.keySet().toString());
		Cargo c = cSet.remove(cargoName);
		cargoCount.put(loadJ.getId(), cSet);
		boolean loadOk = ag.loadCargo(c);

		if (loadOk && !randFail) {
			logger.info(agName + " loaded cargo " + c.getName());
			// NB: have already removed the 'cargoat' percept...

			// add loaded percept for agent-now-holding cargo
			addPercept(ASSyntax.createLiteral(LOADED, ASSyntax.createString(agName), ASSyntax.createString(cargoName)));
			addPercept(ASSyntax.createLiteral(CARRYING_CARGO, ASSyntax.createString(agName)));

			return ActionResult.pass;
		} else {
			logger.severe(agName + " load failed? " + c.getName());

			// is the cargo destroyed / spilled?
			if (shouldDestroyCargoOnFailure()) {
				// add cargo back in!
				cSet.put(cargoName, c);
				cargoCount.put(loadJ.getId(), cSet);
				addPercept(ASSyntax.createLiteral(CARGO_AT, ASSyntax.createString(cargoName),
						ASSyntax.createString(loadJ.getId())));
				// NB: no percepts generated, but ag might have cargo set...
				if (loadOk) {
					ag.unloadCargo(c.getName());
				}
			} else {
				// destroy cargo if carrier...
				logger.severe(c.getName() + " destroyed by activity failure!");
				if (loadOk) {
					destroyCargo(ag, c.getName());
				}
			}

			// check for spills... can occur regardless of destruction...
			this.doRandomSpill(loadJ, type);

			if (randFail) {
				return ActionResult.fail_nondeterministic;
			} else {
				return ActionResult.fail_exogenous_change;
			}
		}
	}

	/**
	 * unloadFrom(AGENT, LOC, FROM, CARGO); crane AGENT takes cargo CARGO from
	 * Truck/Heli FROM at LOC
	 * 
	 * @param agName
	 * @param action
	 * @return
	 */
	private ActionResult doUnloadFrom(String agName, Structure action) {
		if (action.getArity() != 4) {
			logger.warning("Wrong action arity for doLoad: " + action.getArity());
			return ActionResult.fail_args;
		}

		float waitTime = 0;
		final Vehicle crane = vehicleAgents.get(agName);
		// check preconditions hold
		final String loadAtLocation = action.getTermsArray()[1].toString().replace("\"", ""); // only
																								// entry
		final String fromAgent = action.getTermsArray()[2].toString().replace("\"", ""); // only
																							// entry
		final String cargoId = action.getTermsArray()[3].toString().replace("\"", ""); // only
																						// entry
		final JunctionInf loadAtJunction = world.getJunction(loadAtLocation);
		final Vehicle from = vehicleAgents.get(fromAgent);

		// type
		if (!(isCrane(crane) && (isHelicopter(from) || isTruck(from)))) {
			action.addAnnot(ASSyntax.createLiteral(ANNOT_RESULT_INFO, ASSyntax.createString("Wrong agent type")));
			return ActionResult.fail_precond;
		}
		// location
		if (!isAt(crane, loadAtJunction) || !isAt(from, loadAtJunction)) {
			action.addAnnot(ASSyntax.createLiteral(ANNOT_RESULT_INFO, ASSyntax.createString("Agent location error")));
			return ActionResult.fail_precond;
		}
		// not dangerzone
		if (loadAtJunction.isDangerous()) {
			action.addAnnot(
					ASSyntax.createLiteral(ANNOT_RESULT_INFO, ASSyntax.createString(loadAtLocation + " is dangerous")));
			return ActionResult.fail_precond;
		}
		// truck carrying cargo
		if (!isCarryingThisCargo(cargoId, from)) {
			action.addAnnot(ASSyntax.createLiteral(ANNOT_RESULT_INFO,
					ASSyntax.createString(fromAgent + " not carrying " + cargoId)));
			return ActionResult.fail_precond;
		}

		// crane not carrying cargo
		if (!crane.canCarryCargo()) {
			action.addAnnot(
					ASSyntax.createLiteral(ANNOT_RESULT_INFO, ASSyntax.createString(crane + " can't take cargo")));
			return ActionResult.fail_precond;
		}

		if (crane.getHealth().equals(HealthState.mortal)) {
			action.addAnnot(ASSyntax.createLiteral(ANNOT_RESULT_INFO, ASSyntax.createString("Mortal")));
			return ActionResult.fail_precond;
		} else {
			waitTime = crane.getHealth().equals(HealthState.damaged) ? VEHICLE_STD_LOAD_TIME
					: VEHICLE_STD_LOAD_TIME * 0.7f;
		}

		// calculate random failure chance
		boolean randFail = isRandomFailure(action, crane);
		if (randFail) {
			waitTime = waitTime * simRandom.nextFloat();
		}

		try {
			Thread.sleep((long) waitTime);
		} catch (InterruptedException e) {
			logger.warning("Interrupted 'doTime'; " + e.toString());
			e.printStackTrace();
		}

		// if fail; random cargo destruction and spillage info
		if (!randFail) {
			final Cargo unloaded = from.unloadCargo(cargoId);
			crane.loadCargo(unloaded);
			final Literal originalLoadedPercept = ASSyntax.createLiteral(LOADED, ASSyntax.createString(from),
					ASSyntax.createString(cargoId));
			final Literal newLoadedPercept = ASSyntax.createLiteral(LOADED, ASSyntax.createString(agName),
					ASSyntax.createString(cargoId));
			final Literal originalCarryingPercept = ASSyntax.createLiteral(CARRYING_CARGO, ASSyntax.createString(from));
			final Literal newCarryingPercept = ASSyntax.createLiteral(CARRYING_CARGO, ASSyntax.createString(crane));
			this.removePercept(originalCarryingPercept);
			this.removePercept(originalLoadedPercept);
			this.addPercept(newCarryingPercept);
			this.addPercept(newLoadedPercept);
			return ActionResult.pass;
		} else { // randFail - cargo remains loaded
			logger.fine("failed " + action);
			final Type cargoType = from.getCargo().getType();
			// is the cargo destroyed / spilled?
			if (shouldDestroyCargoOnFailure()) {
				logger.severe("Destroy cargo on failure! " + from.getCargo().getName());
				destroyCargo(from, from.getCargo().getName());
			}

			doRandomSpill(loadAtJunction, cargoType);

			return ActionResult.fail_nondeterministic;
		}
	}

	private void doRandomSpill(final JunctionInf loadAtJunction, final Type cargoType) {
		if (cargoType.equals(Type.Hazardous)) {
			if (simRandom.nextFloat() < consequences.getCargoSpillAfterFailureChance()) {
				spillageAtJunction(loadAtJunction);
			}
		}
	}

	private boolean isCarryingThisCargo(final String cargoId, final Vehicle from) {
		return from.getCargo() != null && from.getCargo().getName().equals(cargoId);
	}

	private boolean isCrane(final Vehicle crane) {
		return crane.getType().equals(EntityType.crane);
	}

	private boolean isTruck(final Vehicle from) {
		return from.getType().equals(EntityType.truck);
	}

	private boolean isHelicopter(final Vehicle from) {
		return from.getType().equals(EntityType.helicopter);
	}

	private boolean isRandomFailure(Structure action, final Vehicle... ag) {
		float checkVal = simRandom.nextFloat();
		float probability = stochasticiser.getFailProb(this, action, ag);
		storeReferenceCopyOfActivityFailureProbability(action, probability);
		boolean randFail = checkVal < probability;
		if (randFail) {
			logger.severe("Random failure for " + action + " ( " + ag[0] + "); " + checkVal + "<" + probability);
		}
		return randFail;
	}

	// resultant variable value is only used by the UI
	private void storeReferenceCopyOfActivityFailureProbability(Structure action, float probability) {
		Literal aClone = action.copy();
		aClone.clearAnnots();
		synchronized (currentAction) {
			currentAction.put(aClone, 1 - probability);
		}
	}

	/**
	 * Vehicle unloads an identifier container, placing it in its current
	 * location - but ONLY performable if vehicle is idle (not moving) and at a
	 * junction
	 */
	private ActionResult doUnload(String agName, Structure action) {
		float waitTime = 0;
		Vehicle ag = vehicleAgents.get(agName);

		logger.info("Unload, " + agName + " has cargo " + ag.getCargo());

		// params; actor, name (id) of specific cargo object, location to drop
		if (action.getArity() != 3) {
			logger.warning("Wrong action arity for doUnload: " + action.getArity());
			return ActionResult.fail_args;
		}

		// check agent health
		if (ag.getHealth().equals(HealthState.mortal)) {
			logger.warning("doUnload failure due to health state of " + agName);
			return ActionResult.fail_precond;
		} else {
			waitTime = ag.getHealth().equals(HealthState.damaged) ? VEHICLE_STD_UNLOAD_TIME
					: VEHICLE_STD_UNLOAD_TIME * 0.7f;
		}

		// extract terms
		String unloadAt = action.getTermsArray()[1].toString().replace("\"", "");
		String cargoName = action.getTermsArray()[2].toString().replace("\"", "");

		JunctionInf unloadJ = world.getJunction(unloadAt);

		// precondition check...
		// 1 - cargo held by vehicle?
		if (!ag.holdingCargo(cargoName)) {
			logger.severe("PC fail: " + agName + " is not carrying cargo " + cargoName);
			action.addAnnot(
					ASSyntax.createLiteral(ANNOT_RESULT_INFO, ASSyntax.createString("Not carrying " + cargoName)));
			return ActionResult.fail_precond;
		}

		Type type = ag.getCargo().getType();

		// 2 - vehicle at location?
		if (!isAt(ag, unloadJ)) {
			logger.severe("PC fail: " + agName + " is not at unload location " + unloadJ + ", is at "
					+ ag.getLocationCoordinates());
			return ActionResult.fail_args;
		}

		boolean randFail = isRandomFailure(action, ag);
		if (randFail) {
			waitTime = waitTime * simRandom.nextFloat();
		}

		// now do a wait to simulate the operation time delay
		try {
			Thread.sleep((long) waitTime);
		} catch (InterruptedException e) {
			logger.warning("Interrupted 'doTime'; " + e.toString());
			e.printStackTrace();
		}

		// state change / percept generation
		// agent loses cargo...
		if (ag.getCargo() == null) {
			/*
			 * This should NEVER happen with the precondition checks, etc - so
			 * if it does, will require env debug to correct!
			 */
			logger.severe("Got a null cargo object for unload - serious error!");
			action.addAnnot(ASSyntax.createLiteral(ANNOT_RESULT_INFO, ASSyntax.createString("Null cargo")));
			return ActionResult.fail_precond;
		}

		if (!randFail) { // ok!
			Cargo c = ag.unloadCargo(cargoName);
			// remove agent load percept...
			Literal loadedPercept = ASSyntax.createLiteral(LOADED, ASSyntax.createString(agName),
					ASSyntax.createString(cargoName));
			this.removePercept(loadedPercept);
			removePercept(ASSyntax.createLiteral(CARRYING_CARGO, ASSyntax.createString(agName)));

			// ...location gains cargo!
			// update cargo demand values and percept...
			receiveCargo(unloadJ, c);
			logger.fine("Completed unload operation!");
			return ActionResult.pass;
		} else { // randFail - cargo remains loaded
			logger.fine("failed unload operation!");

			// is the cargo destroyed / spilled?
			if (shouldDestroyCargoOnFailure()) {
				logger.severe("Destroy cargo on failure! " + ag.getCargo().getName());
				destroyCargo(ag, ag.getCargo().getName());
			}

			this.doRandomSpill(unloadJ, type);
			return ActionResult.fail_nondeterministic;
		}
	}

	/**
	 * Spillage event at a junction; at least one connected road becomes
	 * blocked, with other connected roads being evaluated based upon a random
	 * boolean.
	 * 
	 * @param j
	 */
	private void spillageAtJunction(JunctionInf j) {
		int randIndex = simRandom.nextInt(j.getRoads().size());
		int index = 0;
		for (String r : j.getRoads()) {
			if ((index == randIndex) || simRandom.nextBoolean()) {
				spillCargo(world.getRoad(r).getId());
			}
			index++;
		}
	}

	/**
	 * performs a repair action when the vehicle is at a repair station Args;
	 * actor, repair station location
	 * 
	 * @param agName
	 * @param loc(ation)
	 * @param action
	 * @return
	 */
	protected ActionResult doRepair(String agName, Structure action) {
		Vehicle v = getVehicles().get(agName);

		if (action.getTerms().size() != 2) {
			// need aid, location
			logger.severe("doRepair - Invalid action terms " + action);
			return ActionResult.fail_args;
		}

		if (v == null) {
			logger.severe("Vehicle not found!" + agName + ":" + action);
			return ActionResult.fail_args;
		}

		// setup wait time for executing the action
		if (v.getHealth().equals(HealthState.healthy)) {
			return ActionResult.pass;
		}
		long waitTime = v.getHealth().equals(HealthState.mortal) ? VEHICLE_STD_REPAIR_MORTAL_TIME
				: VEHICLE_STD_REPAIR_DAMAGED_TIME;
		// check we are located at a repair station
		String repairStationId = action.getTermsArray()[1].toString().replace("\"", ""); // only
																							// entry

		// type check
		SettlementInf j = world.getSettlement(repairStationId);
		if (j == null) {
			logger.severe("doRepair junction not found " + action);
			return ActionResult.fail_precond;
		}
		if (!j.hasRepairStation()) {
			logger.severe("doRepair location has no station " + action);
			return ActionResult.fail_precond;
		}
		if (!isAt(v, j)) {
			logger.severe(action + " agent is not at specified location; distance =  "
					+ j.getCoordinate().distance(v.getLocationCoordinates()));
			return ActionResult.fail_precond;
		}

		// calculate random failure chance
		boolean randFail = isRandomFailure(action, v);
		if (randFail) {
			waitTime = (long) (waitTime * simRandom.nextFloat());
		}

		// now do a wait to simulate the operation time delay
		try {
			Thread.sleep((long) waitTime);
		} catch (InterruptedException e) {
			logger.warning("repair: Interrupted 'doTime'; " + e.toString());
			e.printStackTrace();
		}

		// set health state
		if (!randFail) {
			setVehicleHealth(v.getId(), HealthState.healthy);
		}

		logger.finer("Done repair of " + v.getId() + ", health state is now " + v.getHealth().name());

		if (randFail) {
			return ActionResult.fail_nondeterministic;
		} else {
			return ActionResult.pass;
		}
	}

	/**
	 * Secure a given danger zone
	 */
	protected ActionResult doSecureArea(String agName, Structure action) {
		String locID = action.getTerm(1).toString().replaceAll("\"", "");

		// precond check
		Vehicle v = getVehicles().get(agName);
		// existance check
		if (v == null) {
			logger.severe(agName + " does not exist for " + action);
			return ActionResult.fail_precond;
		}

		if (!v.isCapable(WorldAction.secureArea)) {
			logger.severe(agName + " not capable of " + action);
			return ActionResult.fail_precond;
		}

		// if is a helicopter, check is FLYING
		if (v instanceof Helicopter && !((Helicopter) v).isFlying()) {
			logger.severe(agName + " is not flying!");
			return ActionResult.fail_precond;
		}

		// loc check
		JunctionInf location = getWorld().getJunction(locID);
		if (!isAt(v, location)) {
			logger.severe(agName + " not at required location (" + location.getCoordinate() + "), but is at "
					+ v.getLocationCoordinates() + ": fail " + action);
			return ActionResult.fail_precond;
		}
		// health check
		if (v.getHealth().equals(HealthState.mortal)) {
			logger.severe(agName + "is mortally wounded for " + action);
			return ActionResult.fail_precond;
		}

		synchronized (getDangerZones()) {
			if (!getDangerZones().containsKey(locID)) {
				logger.info(action + " no change required!");
				return ActionResult.pass;
			}
		}

		long waitTime = VEHICLE_STD_SECURE_TIME;

		boolean randFail = isRandomFailure(action, v);

		if (randFail) {
			waitTime = (long) (waitTime * simRandom.nextFloat());
		}

		// do the action; check 10 times in case of damage from some other
		// source
		// now do a wait to simulate the operation time delay
		for (int i = 0; i < 10; i++) {
			try {
				Thread.sleep((long) (waitTime / 10));
			} catch (InterruptedException e) {
				logger.warning("Interrupted 'doTime'; " + e.toString());
				e.printStackTrace();
			}
			// health check...
			if (v.getHealth().equals(HealthState.mortal)) {
				logger.info(agName + " mortally wounded during cycle " + i + " of " + action);
				return ActionResult.fail_exogenous_change;
			} else {
				logger.info(agName + " survived cycle " + i + " of " + action);
			}
		}

		// calculate whether damaged in process
		boolean damaged = !randFail && (simRandom.nextFloat() < consequences.getSecureAreaDamageChance());
		if (damaged) {
			logger.info(agName + " damaged executing " + action);
			if (v.getHealth().equals(HealthState.healthy)) {
				setVehicleHealth(agName, HealthState.damaged);
			} else {
				setVehicleHealth(agName, HealthState.mortal);
			}
		}

		if (randFail) {
			return ActionResult.fail_nondeterministic;
		}

		// passed!
		removeDangerZone(locID);

		return ActionResult.pass;
	}

	/**
	 * Failure to takeoff leads to the aircraft on the ground, but with possible
	 * damage
	 * 
	 * @param agName
	 * @param action
	 * @return
	 */
	protected ActionResult doTakeoff(String agName, Structure action) {
		// takeoff agent, loc
		String locId = ((StringTerm) action.getTerm(1)).getString();
		JunctionInf j = getWorld().getJunction(locId);
		Vehicle v = getVehicles().get(agName);
		// existance check
		if (v == null) {
			logger.severe(agName + " does not exist for " + action);
			return ActionResult.fail_precond;
		}
		if (j == null) {
			logger.severe("Location " + locId + " not found for " + action);
			return ActionResult.fail_precond;
		}

		if (!isAt(v, j)) {
			logger.severe(agName + " not at  " + j);
			return ActionResult.fail_precond;
		}

		if (!v.isCapable(WorldAction.takeOff)) {
			logger.severe(agName + " not capable of " + action);
			return ActionResult.fail_precond;
		}

		if (!standardFlightPrecondsHold(j, v)) {
			return ActionResult.fail_precond;
		}

		Helicopter h = (Helicopter) v;

		if (isRandomFailure(action, v)) {
			return ActionResult.fail_nondeterministic;
		}
		h.setActing(action);
		h.takeOff();
		addTakeOffPercepts(agName, locId);
		h.setActing(null);
		return ActionResult.pass;
	}

	private void addTakeOffPercepts(String agName, String locId) {
		Literal at = ASSyntax.createLiteral(AT_JUNCTION, ASSyntax.createString(agName), ASSyntax.createString(locId));
		removePercept(at);
		Literal over = ASSyntax.createLiteral(FLYING_OVER, ASSyntax.createString(agName), ASSyntax.createString(locId));
		addPercept(over);
		Literal flying = ASSyntax.createLiteral(FLYING, ASSyntax.createString(agName));
		addPercept(flying);
	}

	/**
	 * Failing to land leads to catastrophic damage... although the aircraft
	 * ends up on the ground!
	 * 
	 * @param agName
	 * @param action
	 * @return
	 */
	protected ActionResult doLand(String agName, Structure action) {
		// takeoff agent, loc
		String locId = ((StringTerm) action.getTerm(1)).getString();
		JunctionInf j = getWorld().getJunction(locId);
		Vehicle v = getVehicles().get(agName);
		// existance check
		if (v == null) {
			logger.severe(agName + " does not exist for " + action);
			return ActionResult.fail_precond;
		}

		if (j == null) {
			logger.severe("Location " + locId + " not found for " + action);
			return ActionResult.fail_precond;
		}

		if (!v.isCapable(WorldAction.land)) {
			logger.severe(agName + " not capable of " + action);
			return ActionResult.fail_precond;
		}

		if (!standardFlightPrecondsHold(j, v)) {
			return ActionResult.fail_precond;
		}

		Helicopter h = (Helicopter) v;

		if (!h.isFlying()) {
			logger.severe(agName + " NOT flying and trying to land!!");
		}

		h.setActing(action);
		// set to landed
		h.land();
		addLandPercepts(agName, locId);
		h.setActing(null);
		if (isRandomFailure(action, v)) {
			return ActionResult.fail_nondeterministic;
		} else {
			return ActionResult.pass;
		}
	}

	private void addLandPercepts(String agName, String locId) {
		Literal at = ASSyntax.createLiteral(AT_JUNCTION, ASSyntax.createString(agName), ASSyntax.createString(locId));
		addPercept(at);
		Literal over = ASSyntax.createLiteral(FLYING_OVER, ASSyntax.createString(agName), ASSyntax.createString(locId));
		removePercept(over);
		Literal flying = ASSyntax.createLiteral(FLYING, ASSyntax.createString(agName));
		removePercept(flying);
	}

	private boolean standardFlightPrecondsHold(final JunctionInf j, final Vehicle v) {
		if (v.getLocationCoordinates().distance(j.getCoordinate()) > 2) {
			logger.severe(v.getId() + " not at  " + j + ", at " + v.getLocationCoordinates());
			return false;
		}

		if (!isHelicopter(v)) {
			logger.severe(v.getId() + " is not a heli!" + v.getType());
			return false;
		}

		// health check
		if (v.getHealth().equals(HealthState.mortal)) {
			logger.severe(v.getId() + "is mortally wounded");
			return false;
		}

		// airport check
		if (!world.getSettlementNames().contains(j.getId())) {
			logger.severe(j.getId() + " Not a settlement");
			return false;
		} else if (!world.getSettlement(j.getId()).hasAirport()) {
			logger.severe("No airport at " + j.getId());
			return false;
		}

		// finally, not a hot dz
		if (isDangerous(j)) {
			logger.severe(j.getId() + " too dangerous");
			return false;
		}

		return true; // Yay!
	}

	/**
	 * Linear move (we don't care about flight paths...). Failure means the
	 * agent is back at the start, but has to travel back there. Also means the
	 * agent lands, with damage (this is a simplification to avoid needed to
	 * have crash co-ordinates, etc)
	 * 
	 * @param agName
	 * @param action
	 * @return
	 */
	protected ActionResult doFly(String agName, Structure action) {
		// args - fly agent, start, end
		Vehicle v = getVehicles().get(agName);

		if (v == null || !isHelicopter(v)) {
			logger.severe("Unknown vehicle " + agName + " for " + action);
			return ActionResult.fail_precond;
		}

		final Helicopter h = (Helicopter) v;

		final String startId = ((StringTerm) action.getTerm(1)).getString();
		final String endId = ((StringTerm) action.getTerm(2)).getString();
		final JunctionInf st = getWorld().getJunction(startId);
		final JunctionInf en = getWorld().getJunction(endId);

		if (st == null || en == null) {
			logger.severe("Unknown junction in " + action + "(" + st + "," + en + ")");
			return ActionResult.fail_precond;
		}

		final Point2D stCo = st.getCoordinate();
		final Point2D enC = en.getCoordinate();

		// check precond state...
		// health
		if(!passesPreFlyChecks(action, h, st)) {
			return ActionResult.fail_precond;
		}

		if (st.equals(en)) {
			logger.info("Already there! " + action);
			return ActionResult.pass;
		}

		if (isRandomFailure(action, h)) {
			return ActionResult.fail_nondeterministic;
		}
		h.setActing(action); // still working!

		StringTerm id = ASSyntax.createString(agName);

		final Literal initialAt = ASSyntax.createLiteral(AT_COORD, id, 
				ASSyntax.createNumber(stCo.getX()), ASSyntax.createNumber(stCo.getY()));
		final Literal initialAtJ = ASSyntax.createLiteral(FLYING_OVER, id, ASSyntax.createString(st.getId()));
		final Literal endAt = ASSyntax.createLiteral(AT_COORD, id, 
				ASSyntax.createNumber(enC.getX()), ASSyntax.createNumber(enC.getY()));
		final Literal endAtJ = ASSyntax.createLiteral(FLYING_OVER, id, ASSyntax.createString(en.getId()));

		double distanceTravelled = 0;
		double travelDistance = stCo.distance(enC);
		
		while (distanceTravelled < travelDistance) {
			if (distanceTravelled > 0) {
				removePercept(initialAtJ);
				removePercept(initialAt);
			}

			// Health state checks - debilitation may occur in transit!
			if (h.getHealth().equals(HealthState.mortal)) {
				// return, land, break
				h.setLocation(stCo);
				logger.severe("Agent mortally damaged!" + st);
				h.land();
				removePercept(ASSyntax.createLiteral(FLYING, id));
				h.setActing(null);
				updateTravelDistance(agName, distanceTravelled);
				action.addAnnot(ASSyntax.createLiteral(ANNOT_RESULT_INFO, ASSyntax.createString("Mortal damage")));
				return ActionResult.fail_exogenous_change;
			}
			// else
			double change = (SIM_STEP_TIME * h.flightSpeed());
			if (change == 0) {
				logger.severe("No change in travel distance for " + h.getId() + "; steptime=" + SIM_STEP_TIME
						+ ", flightSpeed=" + h.flightSpeed());
			}
			distanceTravelled += change;
			// progress = 1 is equivalent to 100% progress, i.e. completion!
			if (distanceTravelled >= travelDistance) {
				logger.fine(distanceTravelled + ":End of the road " + action);
				h.setLocation(enC);
			} else if ((distanceTravelled > 0) && (distanceTravelled < travelDistance)) {
				logger.finest(distanceTravelled + ": travelled by " + h.getId());
				double progress = distanceTravelled / travelDistance;
				double newX = stCo.getX() + (progress * (enC.getX() - stCo.getX()));
				double newY = stCo.getY() + (progress * (enC.getY() - stCo.getY()));

				// new location / percept add
				h.setLocation(new Point2D.Double(newX, newY));

				try {
					Thread.sleep(SIM_STEP_TIME);
				} catch (InterruptedException e) {
					logger.warning(action + "-> Move op interrrupted! " + e.toString());
					e.printStackTrace();
				}
			} else if (distanceTravelled == 0) {
				logger.warning(h.getId() + " did not move any distance in this iteration");
			} 
		}

		// done!
		h.setActing(null);
		h.setLocation(enC);
		removePercept(initialAt);
		removePercept(initialAtJ);
		addPercept(endAt);
		addPercept(endAtJ);
		updateTravelDistance(agName, distanceTravelled);
		return ActionResult.pass;
	}

	private boolean passesPreFlyChecks(final Structure action, final Helicopter h, final JunctionInf st) {
		if (h.getHealth().equals(HealthState.mortal)) {
			logger.severe("Agent is too badly damaged for " + action);
			return false;
		}
		// check flying (in air)
		if (!h.isFlying()) {
			logger.severe("Heli is not airbourne for " + action);
			action.addAnnot(ASSyntax.createLiteral(ANNOT_RESULT_INFO, ASSyntax.createString("Not airbourne")));
			return false;
		}

		if (!isAt(h, st)) {
			logger.severe("Heli for " + action + " is not at " + st + ", Coords=" + h.getLocationCoordinates());
			action.addAnnot(ASSyntax.createLiteral(ANNOT_RESULT_INFO, ASSyntax.createString("Start coords error")));
			return false;
		}
		return true;
	}

	/**
	 * Get a copy of the world, primarily intended for the UI
	 * 
	 * @return
	 */
	public World getWorld() {
		return world;
	}

	/**
	 * Get all vehicles in the world
	 * 
	 * @return
	 */
	public Map<String, Vehicle> getVehicles() {
		return vehicleAgents;
	}

	/**
	 * Sets a junction to need cargo
	 * 
	 * @param loc
	 */
	public void needsCargo(JunctionInf loc) {
		// updated health states
		for (String a : healCountdown.keySet()) {
			if (!actedThisRequest.contains(a)) {
				// reduce countdown
				int cd = (healCountdown.get(a)).intValue();
				cd--;
				if (cd <= 0) {
					healCountdown.put(a, 0); // reset to zero
					heal(a);
				} else {
					healCountdown.put(a, cd);
				}
			}
		}

		// new start
		actedThisRequest.clear();

		Map<String, Cargo> cargoThere = null;
		synchronized (cargoCount) {
			cargoThere = cargoCount.get(loc.getId());
		}

		lastAction = System.currentTimeMillis();

		int r = cargoRequests.get(loc.getId());
		if (!cargoThere.isEmpty() && r > 0) {
			logger.severe(loc + " has " + r + " demands, but has cargo present - " + cargoThere.values());
		}

		if (!cargoThere.isEmpty()) {
			// can we fulfill the request with existing cargo?
			// do nothing, but remove a bit of cargo
			Cargo c = cargoThere.remove(cargoThere.keySet().toArray()[0]);
			synchronized (cargoCount) {
				cargoCount.put(loc.getId(), cargoThere);
			}

			// remove percept (consume cargo)
			Literal atPercept = ASSyntax.createLiteral(CARGO_AT, ASSyntax.createString(c.getName()),
					ASSyntax.createString(loc.getId()));
			this.removePercept(atPercept);
			addDisplayString("Consumed added cargo to meet demand; " + loc.getId());
		} else {
			actedSinceDemand = false;
			synchronized (cargoRequests) {
				cargoRequests.put(loc.getId(), ++r);
			}

			Literal need = ASSyntax.createLiteral(CARGO_NEEDED, ASSyntax.createString(loc.getId()));
			// generate percept(s) for commanders
			for (String s : this.commanders) {
				addPercept(s, need);
			}
			lastCargoRequestLocation = loc.getId();
			logger.warning("Added cargo demand; " + need);
			addDisplayString("Added cargo demand; " + need);// + loc.getId() +
															// ", " +
															// perceptCounter);
		}
	}

	private int satisfied = 0;

	public int getSatisfied() {
		return satisfied;
	}

	private int genDemands = 0, genDemandsSlice = 0;

	public int getGenDemandsSlice() {
		return genDemandsSlice;
	}

	public void setGenDemandsSlice(int gs) {
		genDemandsSlice = gs;
	}

	public int getGenDemands() {
		return genDemands;
	}

	public void setGenDemands(int g) {
		genDemands = g;
		lastAction = System.currentTimeMillis();
	}

	public void consume(JunctionInf loc, Cargo c) {
		synchronized (cargoRequests) {
			int r = cargoRequests.get(loc.getId());
			if (r > 0) {
				if (!c.getName().equals("dummy") && ((satisfied + 1 <= genDemands))) {
					satisfied++;
				}
				// consume cargo to fulfill a request
				logger.info("Cargo " + c + " being consumed by " + loc.getId());
				addDisplayString(c.getName() + "(" + c.getType() + ")" + " delivered to " + loc.getId());

				// destroy all cargo after we make a succesful delivery...
				destroyAllCargo();
				// destroyCargo(loc.getId(), c.getName());

				// remove request percept as cargo is consumed and removed
				Literal need = ASSyntax.createLiteral(CARGO_NEEDED, ASSyntax.createString(loc.getId()));

				logger.info("Remove cargo request percept: " + need);
				for (String s : this.commanders) {
					removePercept(s, need);
				}

				r--;
				if (r < 0) {
					r = 0;
				} // sanity check
				cargoRequests.put(loc.getId(), r); // update internal
			}
		}
	}

	/**
	 * Junction receives cargo.
	 * 
	 * @param loc
	 */
	public void receiveCargo(JunctionInf loc, Cargo c) {
		synchronized (cargoCount) {
			logger.fine("Cargo " + c + " received at " + loc.getId());
			// 'spare' cargo is added to the pile...
			Map<String, Cargo> cargos = cargoCount.get(loc.getId());
			cargos.put(c.getName(), c);
			cargoCount.put(loc.getId(), cargos);
			// setup percepts for location, type
			Literal cargoTypePercept = ASSyntax.createLiteral(CARGO_TYPE, ASSyntax.createString(c.getName()));
			cargoTypePercept.addAnnot(UniversalConstants.OBJECT_DEF_ANNOT);
			addPercept(cargoTypePercept);
			Literal atPercept = ASSyntax.createLiteral(CARGO_AT, ASSyntax.createString(c.getName()),
					ASSyntax.createString(loc.getId()));
			addPercept(atPercept);
			addDisplayString(c.getType() + " " + c.getName() + " added; " + loc.getId());

			logger.fine(loc.getId() + ") Cargo count; " + cargoCount.get(loc.getId()).size());
		}
	}

	// get cargo info, primarily here for the ui...
	public Map<String, Map<String, Cargo>> getCargoCounts() {
		return cargoCount;
	}

	public int sumCargoFree() {
		synchronized (cargoCount) {
			int sum = 0;
			for (JunctionInf j : getWorld().getAllJunctions()) {
				if (cargoCount.containsKey(j.getId())) {
					sum += cargoCount.get(j.getId()).size();
				}
			}
			return sum;
		}
	}

	/**
	 * Return true if unloaded cargo exists
	 * 
	 * @return
	 */
	public boolean cargoExists() {
		return sumCargoFree() > 0;
	}

	/**
	 * Return true if cargo is loaded onto 'a' agent
	 * 
	 * @return
	 */
	public boolean cargoLoaded() {
		for (Vehicle v : vehicleAgents.values()) {
			if (v.getCargo() != null) {
				return true;
			}
		}
		return false;
	}

	public Map<String, Integer> getCargoRequests() {
		synchronized (cargoRequests) {
			return cargoRequests;
		}
	}

	public int sumCargoRequests() {
		int sum = 0;
		for (Integer req : cargoRequests.values()) {
			sum += req;
		}
		return sum;
	}

	/**
	 * Get current rainstorms
	 */
	public Collection<RainStorm> getStorms() {
		return stepper.getStorms();
	}

	public boolean isWindy() {
		return stepper.isWindy();
	}

	public void setWindyPercept(final boolean windy) {
		if (!windy) {
			removePercept(WINDY_PERCEPT);
		} else {
			addPercept(WINDY_PERCEPT);
		}
	}

	/**
	 * @param id
	 * @return
	 */
	public RoadCondition getRoadCondition(String id) {
		return world.getRoad(id).getConditions();
	}

	public void setRoadCondition(final RoadCondition condition, final String roadId) {
		// do this only if the condition has actually changed...
		final RoadInf r = world.getRoad(roadId);
		if (!r.getConditions().equals(condition)) {
			if ((condition.equals(RoadCondition.flooded) && !closeable(r))) {
				logger.finer("Cannot close " + r.getId() + "(" + condition + "), so not setting as flooded");
			} else if ((condition.equals(RoadCondition.slippery) && !slipperable(r))) {
				logger.finer("Cannot setSlip " + r.getId());
			} else {
				// remove old percept(s) for current condition
				List<Literal> remList = getConditionPercepts(r, new Vector<Literal>());
				for (Literal rp : remList) {
					removePercept(rp);
				}

				// set road property & add new percept(s)
				r.setCondition(condition);
				List<Literal> addList = getConditionPercepts(r, new Vector<Literal>());
				for (Literal ap : addList) {
					addPercept(ap);
				}
				logger.info(roadId + " (" + r.getRoadType() + "):set as " + condition
						+ ("slippery=" + r.isSlippery() + ", flooded=" + r.isFlooded() + ")"));
				addDisplayString(roadId + " (" + r.getRoadType() + "):" + condition);
				checkWhetherStuckAgentsCanMove(roadId);
			}
		}
	}

	public int getBlockedCount() {
		return blockedSet().size();
	}

	public Vector<String> blockedSet() {
		final Vector<String> blockedSet = new Vector<>();
		for (final RoadInf r : world.getAllRoads()) {
			if (r.isBlocked()) {
				blockedSet.add(r.getId());
			}
		}
		return blockedSet;
	}

	public void closeRoad(final String id) {
		final RoadInf r = world.getRoad(id);
		if (closeable(r)) {
			r.setBlocked(true);
			world.getRoad(id).setBlocked(true);
			addPerceptsForClosedRoad(id);
			logger.info(id + " (" + r.getRoadType() + "): Blocked");
			addDisplayString(id + " (" + r.getRoadType() + "): Blocked");
		}
	}

	private void addPerceptsForClosedRoad(final String id) {
		StringTerm roadA = ASSyntax.createString(world.getRoad(id).getEndIds()[0]);
		StringTerm roadB = ASSyntax.createString(world.getRoad(id).getEndIds()[1]);
		addPercept(ASSyntax.createLiteral(BLOCKED, roadA, roadB)); // blocked(A,B)
		addPercept(ASSyntax.createLiteral(BLOCKED, roadB, roadA)); // blocked(B,A)
		addPercept(ASSyntax.createLiteral(BLOCKED_ROAD, ASSyntax.createString(world.getRoad(id).getId()))); // blocked(B_A)
	}

	public boolean closeable(RoadInf r) {
		// don't allow toxic landslides
		if (r.isToxic()) {
			return false;
		}

		JunctionInf j1 = r.getEnds()[0];
		JunctionInf j2 = r.getEnds()[1];
		if (j1.getNumNeighbours() == 1 && j2.getNumNeighbours() == 1) {
			logger.finest("Cannot close road " + r.getId());
			return false;
		}

		Collection<String> j1R = new Vector<String>();
		for (String r1 : j1.getRoads()) {
			j1R.add(r1);
		}

		j1R.remove(r.getId());
		boolean j1Accessible = true;
		cLoop: for (String checkId : j1R) {
			RoadInf check = getWorld().getRoad(checkId);
			if (check.isBlocked() || check.isFlooded() || check.isToxic()
					|| (check.getRoadType().equals(RoadType.mud) && check.isSlippery())) {
				j1Accessible = false;
				break cLoop;
			}
		}

		Collection<String> j2R = new Vector<String>();
		for (String r2 : j2.getRoads()) {
			j1R.add(r2);
		}
		j2R.remove(r.getId());

		boolean j2Accessible = true;
		cLoop: for (String checkId : j2R) {
			RoadInf check = getWorld().getRoad(checkId);
			if (check.isBlocked() || check.isFlooded() || check.isToxic()
					|| (check.getRoadType().equals(RoadType.mud) && check.isSlippery())) {
				j2Accessible = false;
				break cLoop;
			}
		}

		if (!(j1Accessible && j2Accessible)) {
			logger.finest("Cannot make inaccessible; j1? " + j1Accessible + ", j2? " + j2Accessible);
			return false;
		} else {
			return true;
		}
	}

	public boolean slipperable(RoadInf r) {
		JunctionInf j1 = r.getEnds()[0];
		JunctionInf j2 = r.getEnds()[1];

		boolean j1ok = false, j2ok = false;
		Collection<String> roads = new Vector<String>();
		roads.addAll(j1.getRoads());
		roads.remove(r.getId());
		loop: for (String rid : roads) {
			RoadInf r1 = world.getRoad(rid);
			if (!r1.isSlippery() && !r1.isFlooded()) {
				j1ok = true;
				break loop;
			}
		}

		roads.clear();
		roads.addAll(j2.getRoads());
		roads.remove(r.getId());
		loop: for (String rid : roads) {
			RoadInf r2 = world.getRoad(rid);
			if (!r2.isSlippery() && !r2.isFlooded()) {
				j2ok = true;
				break loop;
			}
		}

		if (!(j1ok && j2ok)) {
			logger.finest("Cannot make inaccessible; j1? " + j1ok + ", j2? " + j2ok);
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Open road / set road as open
	 */
	public void openRoad(final String id) {
		final RoadInf r = world.getRoad(id);
		if (r.isBlocked()) {
			r.setBlocked(false);
			updatePerceptsForOpenedRoad(id);

			logger.info(id + " (" + r.getRoadType() + "): Unblocked");
			addDisplayString(id + " (" + r.getRoadType() + "): Unblocked");

			checkWhetherStuckAgentsCanMove(id);
		} else {
			throw new RuntimeException("Road not actually closed!" + id);
		}
	}

	private void updatePerceptsForOpenedRoad(final String roadId) {
		final StringTerm roadA = ASSyntax.createString(world.getRoad(roadId).getEndIds()[0]);
		final StringTerm roadB = ASSyntax.createString(world.getRoad(roadId).getEndIds()[1]);
		removePercept(ASSyntax.createLiteral(BLOCKED, roadA, roadB));
		removePercept(ASSyntax.createLiteral(BLOCKED, roadB, roadA));
		removePercept(ASSyntax.createLiteral(BLOCKED_ROAD, ASSyntax.createString(roadId)));
	}

	/**
	 * Checks whether or not agents stuck on this road are still stuck, and if
	 * not removes their 'stuck' percept and updates the appropriate internal
	 * data objects
	 * 
	 * @param roadId
	 *            Road identifier
	 */
	private void checkWhetherStuckAgentsCanMove(String roadId) {
		synchronized (getStuckAgents()) {
			// check for agents stuck on the road...
			if (getStuckAgents().containsKey(roadId)) {
				final Vector<String> stuckA = getStuckAgents().get(roadId);
				final Vector<String> unstuck = new Vector<>();
				for (final String s : stuckA) {
					if (!unstuck.contains(s)) {
						final Vehicle v = vehicleAgents.get(s);
						final RoadInf r = world.getRoad(roadId);
						final boolean stuckTimeoutElapsed = (stuckTime.containsKey(s)
								&& (getGenDemands() >= stuckTime.get(s))) || (!stuckTime.containsKey(s));
						if (stuckTimeoutElapsed) {
							removePercept(ASSyntax.createLiteral(RESTING, ASSyntax.createString(s)));
							final boolean canTravel = v.canTravelAlong(r, r.getEnds()[0]) || v.canTravelAlong(r, r.getEnds()[1]);
							if (canTravel) {
								unstuck.add(s);
							}
						}
					}
				} // stuckA iteration

				for (final String s : unstuck) {
					addDisplayString(s + " is unstuck@" + roadId);
					removePercept(ASSyntax.createLiteral(STUCK, ASSyntax.createString(s)));
					stuckA.remove(s);
					for (final String road : getStuckAgents().keySet()) {
						if (getStuckAgents().get(road).contains(s)) {
							getStuckAgents().get(road).remove(s);
							// lastStuck.put(s, roadId);
						}
					}
					stuckTime.remove(s);
				}
			}
		} // end synch block
	}

	public String getStuckList() {
		synchronized (getStuckAgents()) {
			return stuckTime.keySet().toString();
		}
	}

	/*
	 * =========================================================================
	 * ===== Methods relating to insurgent danger zones, where attacks are more
	 * likely to occur
	 * =========================================================================
	 * =====
	 */
	public void addDangerZone(final String jId) {
		final JunctionInf j = this.getWorld().getJunction(jId);
		DangerZone dz = new DangerZone(j, dzHelper.createDzArea(j));
		addDangerZone(dz);
	}

	public void addDangerZone(final DangerZone dz) {
		synchronized (getDangerZones()) {
			final String id = dz.getSource().getId();
			logger.info("Add dangerZone " + id);
			dangerZones.put(id, dz);
			addPercept(ASSyntax.createLiteral(DANGERZONE, ASSyntax.createString(id)));
			world.getJunction(id).setDangerous(true);
			for (String rid : world.getJunction(id).getRoads()) {
				RoadInf r = world.getRoad(rid);
				if (r.getEndIds()[0].equals(id)) {
					r.getEnds()[0].setDangerous(true);
				}
				if (r.getEndIds()[1].equals(id)) {
					r.getEnds()[1].setDangerous(true);
				}
			}
			addDisplayString("Added dangerZone " + dz.getSource().getId());
		}
	}

	public Map<String, DangerZone> getDangerZones() {
		return dangerZones;
	}

	public boolean isDangerous(JunctionInf j) {
		return dangerZones.containsKey(j.getId()) && (dangerZones.get(j.getId()) != null);
	}

	public void removeDangerZone(String dzId) {
		synchronized (getDangerZones()) {
			if (dangerZones.containsKey(dzId)) {
				logger.info("Remove dangerZone " + dzId);
				dangerZones.remove(dzId);
				lastDzRemoved.put(dzId, System.currentTimeMillis());

				removePercept(ASSyntax.createLiteral(DANGERZONE, ASSyntax.createString(dzId)));
				world.getJunction(dzId).setDangerous(false);
				// roads...
				for (String rid : world.getJunction(dzId).getRoads()) {
					JunctionInf[] ends = world.getRoad(rid).getEnds();
					String[] endIds = world.getRoad(rid).getEndIds();
					if (endIds[0].equals(dzId)) {
						ends[0].setDangerous(false);
					}
					if (endIds[1].equals(dzId)) {
						ends[1].setDangerous(false);
					}
					checkWhetherStuckAgentsCanMove(rid);
				}
				addDisplayString("Removed dangerZone " + dzId);
			}
		}
	}

	public long getLastRemovalTime(String id) {
		return lastDzRemoved.get(id);
	}

	/*
	 * =========================================================================
	 * ===== Methods relating to insurgent attacks, which may damage agents in
	 * radius
	 * 
	 * Attacks damage any agent in their vicinity at the time of occurance ONLY,
	 * but are drawn on screen for a certain amount of time before disappearing.
	 * i.e. we store the 'age' of attacks for ui draw purposes.
	 * =========================================================================
	 * =====
	 */

	private void heal(String s) {
		if (!vehicleAgents.containsKey(s)) {
			logger.warning("Setting health for unknown vehicle; " + s);
			return;
		}
		Vehicle v = this.vehicleAgents.get(s);
		setVehicleHealth(s, v.getHealth().getBetter());
	}

	/**
	 * Sets the vehicle health state and generates percepts accordingly
	 * 
	 * @param s
	 *            agent name
	 * @param HealthState
	 */
	public void setVehicleHealth(String s, HealthState h) {
		if (!vehicleAgents.containsKey(s)) {
			logger.warning("Setting health for unknown vehicle; " + s);
			return;
		}
		Vehicle v = vehicleAgents.get(s);
		if (!v.getHealth().equals(h)) {
			logger.warning("Setting health to " + h.name() + " for vehicle; " + s);
			addDisplayString(v.getType() + " " + v.getId() + ":" + h.name());

			boolean gotWorse = h.getValue() < v.getHealth().getValue();

			// remove old percepts
			List<Literal> rm = getHealthPercepts(v);
			for (Literal p : rm) {
				removePercept(p);
			}

			v.setHealth(h);
			final List<Literal> add = getHealthPercepts(v);
			for (Literal p : add) {
				addPercept(p);
			}

			// add healing request link
			final boolean crashedHeli = isHelicopter(v)
					&& Utils.getJunction(v.getLocationCoordinates(), getWorld().getAllJunctions()) == null;

			// can never heal!
			if (h.equals(HealthState.mortal) && crashedHeli) {
				healCountdown.put(s, Integer.MAX_VALUE);
			} else if (gotWorse) {
				int currCount = 0;
				if (healCountdown.containsKey(s)) {
					currCount = healCountdown.get(s);
				}
				healCountdown.put(s, currCount + consequences.getIdleHealingTime());
			}
		}
	}

	public int getStep() {
		return step;
	}

	/**
	 * Forms a percept set for a vehicle, based upon HealthState; provides
	 * negative percepts for non-applicable states, i.e. !healthy & damaged &
	 * !mortal would be generated for a damaged vehicle. this is to support
	 * binary/ non-ternary logic used by some planners.
	 */
	private List<Literal> getHealthPercepts(Vehicle v) {
		Vector<Literal> rt = new Vector<Literal>();
		StringTerm id = ASSyntax.createString(v.getId());
		HealthState health = v.getHealth();
		rt.add(ASSyntax.createLiteral(health.name().toLowerCase(), id));
		logger.finer(v.getId() + " health= " + health + ", health percepts= " + rt);
		return rt;
	}

	/**
	 * Returns true when env has registered agents..
	 */
	public boolean hasAgents() {
		return !agents.isEmpty();
	}

	/**
	 * Return true if no agents are performing any tasks
	 */
	public boolean nobodyActing() {
		for (Vehicle v : vehicleAgents.values()) {
			if (v.isActing()) {
				return false;
			}
		}
		return true;
	}

	public String getCurrentAction() {
		synchronized (currentAction) {
			return currentAction.toString();
		}
	}

	public Literal getLastActionLiteral() {
		synchronized (lastActionPerformed) {
			return lastActionPerformed.isEmpty() ? ASSyntax.createLiteral("none") : lastActionPerformed.keySet().iterator().next();
		}
	}

	public ActionResult getLastActionResult() {
		synchronized (lastActionPerformed) {
			return lastActionPerformed.isEmpty() ? ActionResult.pass : lastActionPerformed.values().iterator().next();
		}
	}

	/**
	 * Used to record duration of experiment slices
	 */
	private long sliceTime = -1;
	private int sliceCount = 0;
	private int totalCargoReq = 0;

	/**
	 * Called when we start a new time period; does calculations for stats etc
	 */
	private synchronized void startSlice() {
		if (UniversalConstants.DEMO_MODE) {
			JOptionPane.showMessageDialog(getUi(), new String[] { "New slice" }, "New slice starting",
					JOptionPane.INFORMATION_MESSAGE);
		}

		for (String key : vehicleAgents.keySet()) {
			if (!travelledDistance.containsKey(key)) {
				travelledDistance.put(key, 0f);// instantiation of entry
			}
			if (!actingTime.containsKey(key)) {
				actingTime.put(key, 0l);
			}
		}

		if (sliceTime != -1) {
			logger.info("Slice " + sliceCount + " took " + (System.nanoTime() - sliceTime) + "ns");

			final int old = totalCargoReq;

			for (Integer req : getCargoRequests().values()) {
				totalCargoReq += req;
			}

			logger.info("Slice " + sliceCount + " cargo requests = " + totalCargoReq + ", for slice="
					+ (totalCargoReq - old));
		}
		// record start time for this episode
		sliceTime = System.nanoTime();
	}

	/**
	 * Set the current execution cycle time
	 * 
	 * @param cycle
	 */
	public synchronized void setCycle(int i) {
		if (step < i) {
			StatsRecorder.getInstance().heartBeat();
			step = i;
			// record statistics for the current cycle
			// num closed roads / toxic / flooded / slippery
			// num healthy /damaged / mortal agents
			// num failed/passed tasks
			// num cargo demands
			updateStats();

			synchronized (getStuckAgents()) {
				if (!getStuckAgents().isEmpty()) {
					Vector<String> sar = new Vector<String>();
					sar.addAll(getStuckAgents().keySet());
					for (String rid : sar) {
						checkWhetherStuckAgentsCanMove(rid);
					}
				}
			}
		}
	}

	public void updateStats() {
		int hlV = 0, dmgV = 0, morV = 0;
		for (final Vehicle v : getVehicles().values()) {
			final HealthState vh = v.getHealth();
			if (vh.equals(HealthState.healthy)) {
				hlV++;
			} else if (vh.equals(HealthState.damaged)) {
				dmgV++;
			} else {
				morV++;
			}
		}

		int numFlooded = 0, numSlippery = 0, numToxic = 0, numBlocked = 0;
		int numDz = getDangerZones().size();
		for (RoadInf r : this.getWorld().getAllRoads()) {
			// road condition
			if (r.isFlooded()) {
				numFlooded++;
			} else if (r.isSlippery()) {
				numSlippery++;
			}
			// other stats...
			if (r.isBlocked()) {
				numBlocked++;
			}
			if (r.isToxic()) {
				numToxic++;
			}

		}

		StatsRecorder.getInstance().recordStepSummary(getGenDemands(), satisfied, hlV, dmgV, morV,
				world.getAllRoads().size(), numSlippery, numFlooded, numToxic, numBlocked,
				world.getAllJunctions().size(), numDz, getActionsTried(), getActionsPass(), getActionsArgFail(),
				getActionsPreFail(), getActionsExoFail(), getActionsRandFail(), avgDistanceTravelled(),
				totalDistanceTravelled(), avgActingTime(), totalActingTime(), totalActivityCost(),
				averageActivityCost());
	}

	public String getLastGenerated() {
		return lastCargoRequestLocation;
	}

	public WorldUi getUi() {
		return ui;
	}

	/**
	 * Outputs the world state - vehicle and road status. To debug failure
	 * causes.
	 */
	public void dumpState() {
		if (!actedSinceDemand) {
			String dumpString = "";
			for (Vehicle v : getVehicles().values()) {
				dumpString = dumpString + v.getId() + " (" + v.getHealth() + "), stuck? " + isStuck(v.getId()) + "\n";
			}
			for (RoadInf r : world.getAllRoads()) {
				dumpString = dumpString + r.toString() + ", Stuck? " + stuckAgents.get(r.getId()) + "\n";
			}
			logger.severe("==World state info==\n" + dumpString);
		}
	}

	public void kill() throws Exception {
		if(ui.isVisible()) {
			ui.setVisible(false);
		}
		
		logger.severe("INVOKING SHUTDOWN");
		StatsRecorder.getInstance().close();
		getEnvironmentInfraTier().getRuntimeServices().stopMAS();
	}

	public Map<String, Vector<String>> getStuckAgents() {
		synchronized (stuckAgents) {
			return stuckAgents;
		}
	}

	private boolean isStuck(final String aid) {
		Collection<Vector<String>> ags = stuckAgents.values();
		for (Vector<String> st : ags) {
			if (st.contains(aid)) {
				return true;
			}
		}
		return false;
	}

	private static boolean isAt(final Vehicle ag, final JunctionInf loadJ) {
		return ag.getLocationCoordinates().equals(loadJ.getCoordinate());
	}

	public Stepper getStepper() {
		return stepper;
	}

	public FailureConsequences getConsequences() {
		return consequences;
	}
}
