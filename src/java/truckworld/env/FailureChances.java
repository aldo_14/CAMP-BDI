package truckworld.env;

import java.util.Properties;
import java.util.logging.Logger;

import common.UniversalConstants;
import common.UniversalConstants.EntityType;
import common.UniversalConstants.HealthState;
import common.UniversalConstants.RoadType;
import common.UniversalConstants.WorldAction;
import jason.asSyntax.Structure;
import truckworld.vehicle.Vehicle;
import truckworld.world.interfaces.Actor;
import truckworld.world.interfaces.RoadInf;

/**
 * @author Alan White
 * 
 *         Defines chance of failure for an operation. Used to encapsulate the
 *         required info in a handy-dandy class (rather than the
 *         TruckworldEnvironment monolith).
 */
public class FailureChances {
	/**
	 * Modifiers for failure under certain states (inverse)
	 */
	private static final float 	SLIPPERY_FAIL_CHANCE = 0.75f, DAMAGED_FAIL_CHANCE = 0.75f, WINDY_FAIL_CHANCE = 0.75f;

	/**
	 * Logger
	 */
	private Logger logger = Logger.getLogger(this.getClass().getSimpleName());

	/**
	 * Properties file configuration random-failure chance on either a action id
	 * or agent id.actor id basis. Latter takes precedence i.e. truck.move=0 and
	 * move=1; will return 0 for truck's 'move' failure chance.
	 */
	@SuppressWarnings("unused")
	private Properties properties;

	/**
	 * Create a new object; properties file is expected to be the same as used
	 * to init the environment.
	 * 
	 * @param chanceSource
	 *            properties file used to get default chances - code here can
	 *            customize on top of this
	 */
	public FailureChances(final Properties p) {
		logger.setLevel(UniversalConstants.FAILURE_CHANCE_LOGGER_LEVEL);
		this.properties = p;
	}

	public float getFailProb(final Cargoworld env, final Structure op, final Actor... actors) {
		return getFailProb(env, actors[0], op); // TODO multiacotr case
	}

	/**
	 * Fetches the probability of an action failing; this can also be used to
	 * define any agent related constraints, etc. Value pertains to likelihood
	 * of failure, not success... we don't do precond checks here, assume that
	 * env will work those out.
	 * 
	 * @param envState
	 *            - the environment state
	 * @param actor
	 *            - the entity performing the action. Specific subclasses may be
	 *            checked etc.
	 * @param opDetails
	 *            - Structure object containing the action functor and arguments
	 * @return a float value between 0 and 1, where 0 indicates definite success
	 */
	private float getFailProb(final Cargoworld envState, final Actor actor, final Structure opDetails) {
		final String functor = opDetails.getFunctor();
		float fc = 1;// default value

		// everything else is a modification of the default...
		// get health state... standard multiplier
		final Vehicle v = envState.getVehicles().get(actor.getId());
		if (v != null) { // i.e. is a vehicle
			if (v.getHealth().equals(HealthState.damaged)) {
				fc = fc * getDamagedFailChance();
			} else if (v.getHealth().equals(HealthState.mortal)) {
				return 1f;
			}
		}
		logger.info("HS post fc=" + fc);
		
		// if not a heli or hover... move case
		if (isMoveActivity(functor)) {
			final RoadInf r = envState.getWorld().getRoad(getFirstArg(opDetails));
			if (isTruck(v)) {
				return getRoadMoveConfidence(fc, r);
			} 
			else if (isBulldozer(v)) {
				return getBulldozerMoveConfidence(fc, r, liesOnRoad(v, r));
			}
			else if (isHazmat(v)) {
				return getHazmatMoveConfidence(fc, r, liesOnRoad(v, r));
			}
			else if (isApc(v)) {
				return getApcRoadMoveConfidence(fc, r);
			}
		}
		else if(isDecontaminateActivity(functor)) {
			return getHazmatDecontaminateConfidence(fc, envState.getWorld().getRoad(getFirstArg(opDetails)));	
		}
		else if(isUnblockActivity(functor)) {
			return getBulldozerUnblockConfidence(fc, envState.getWorld().getRoad(getFirstArg(opDetails)));
			
		}
		else if (isFlightActivity(functor) && envState.isWindy()) {
			return 1 - (fc * getWindyFailChance());
		}
		logger.fine("Calculated fc for " + opDetails + "=" + fc);
		return (1 - fc); // DEFAULT
	}

	private String getFirstArg(final Structure opDetails) {
		return opDetails.getTerm(1).toString().replaceAll("\"", "");
	}

	private boolean liesOnRoad(final Vehicle v, final RoadInf r) {
		return !v.getLocationCoordinates().equals(r.getEnds()[0].getCoordinate()) && !v.getLocationCoordinates().equals(r.getEnds()[1].getCoordinate());
	}
	
	private boolean isDecontaminateActivity(final String functor) {
		return functor.equals(WorldAction.decontaminate.name());
	}

	private boolean isMoveActivity(final String functor) {
		return functor.equals(WorldAction.move.name());
	}

	private boolean isUnblockActivity(final String functor) {
		return functor.equals(WorldAction.unblock.name());
	}
	
	private boolean isApc(final Vehicle v) {
		return v.getType().equals(EntityType.apc);
	}

	private boolean isHazmat(final Vehicle v) {
		return v.getType().equals(EntityType.hazmat);
	}
	
	private boolean isTruck(final Vehicle v) {
		return v.getType().equals(EntityType.truck);
	}
	
	private boolean isBulldozer(final Vehicle v) {
		return v.getType().equals(EntityType.bulldozer);
	}


	private boolean isFlightActivity(final String functor) {
		return functor.equals(WorldAction.fly.name()) || functor.equals(WorldAction.land.name()) || functor.equals(WorldAction.takeOff.name());
	}

	private float getApcRoadMoveConfidence(final float fc, final RoadInf r) {
		logger.fine("Calculate confidence for apc move; fc=" + fc + ", road=" + r);
		if (r.isBlocked() || r.isFlooded()) {
			logger.fine("road is blocked or flooded");
			return 1;
		} else if (r.isSlippery()) {
			logger.fine("road is slippery, return " + ( 1 - (fc * getSlipperyFailChance())));
			return 1 - (fc * getSlipperyFailChance());
		} else {
			return 1 - fc;
		}
	}

	private float getRoadMoveConfidence(final float fc, final RoadInf r) {
		logger.fine("Calculate confidence for move; fc=" + fc + ", road=" + r);
		if (r.isToxic() || r.isBlocked() || r.isFlooded()) {
			logger.fine("road is blocked or flooded or toxic");
			return 1;
		}
		if (r.isSlippery() && r.getRoadType().equals(RoadType.mud)) {
			logger.fine("road is slippery mud");
			return 1;
		} else if (r.isSlippery() && r.getRoadType().equals(RoadType.tarmac)) {
			logger.fine("road is slippery tarmac; " + getSlipperyFailChance());
			return 1 - (fc * getSlipperyFailChance());
		} else {
			return 1 - (fc);
		}
	}

	private float getHazmatMoveConfidence(final float fc, final RoadInf r, final boolean onR) {
		logger.fine("Calculate confidence for move; fc=" + fc + ", road=" + r);
		if ((r.isToxic() && !onR) || r.isBlocked() || r.isFlooded()) {
			logger.fine("road " + r + (r.isBlocked()?" is blocked":"") + (r.isFlooded()? " flooded":"") + (r.isToxic()?" toxic":""));
			return 1;
		}
		if (r.isSlippery() && r.getRoadType().equals(RoadType.mud)) {
			logger.fine("road is slippery mud");
			return 1;
		} else if (r.isSlippery() && r.getRoadType().equals(RoadType.tarmac)) {
			logger.fine("road is slippery tarmac; " + getSlipperyFailChance());
			return 1 - (fc * getSlipperyFailChance());
		} else {
			return 1 - (fc);
		}
	}

	private float getHazmatDecontaminateConfidence(final float fc, final RoadInf r) {
		logger.fine("Calculate confidence for move; fc=" + fc + ", road=" + r);
		if (r.isBlocked() || r.isFlooded()) {
			logger.fine("road " + r + " is blocked or flooded");
			return 1;
		}
		if (r.isSlippery() && r.getRoadType().equals(RoadType.mud)) {
			logger.fine("road is slippery mud");
			return 1;
		} else if (r.isSlippery() && r.getRoadType().equals(RoadType.tarmac)) {
			logger.fine("road is slippery tarmac; " + getSlipperyFailChance());
			return 1 - (fc * getSlipperyFailChance());
		} else {
			return 1 - (fc);
		}
	}
	
	private float getBulldozerMoveConfidence(final float fc, final RoadInf r, final boolean onR) {
		logger.fine("Calculate confidence for move; fc=" + fc + ", road=" + r);
		if (r.isToxic() || (r.isBlocked() && !onR) || r.isFlooded()) {
			logger.fine("road " + r + " is blocked or flooded or toxic");
			return 1;
		}
		if (r.isSlippery() && r.getRoadType().equals(RoadType.mud)) {
			logger.fine("road is slippery mud");
			return 1;
		} else if (r.isSlippery() && r.getRoadType().equals(RoadType.tarmac)) {
			logger.fine("road is slippery tarmac; " + getSlipperyFailChance());
			return 1 - (fc * getSlipperyFailChance());
		} else {
			return 1 - (fc);
		}
	}


	private float getBulldozerUnblockConfidence(final float fc, final RoadInf r) {
		logger.fine("Calculate confidence for move; fc=" + fc + ", road=" + r);
		if (r.isToxic() || r.isFlooded()) {
			logger.fine("road " + r + " is flooded or toxic");
			return 1;
		}
		if (r.isSlippery() && r.getRoadType().equals(RoadType.mud)) {
			logger.fine("road is slippery mud");
			return 1;
		} else if (r.isSlippery() && r.getRoadType().equals(RoadType.tarmac)) {
			logger.fine("road is slippery tarmac; " + getSlipperyFailChance());
			return 1 - (fc * getSlipperyFailChance());
		} else {
			return 1 - (fc);
		}
	}

	private float getDamagedFailChance() {
		return DAMAGED_FAIL_CHANCE;
	}

	private float getWindyFailChance() {
		return WINDY_FAIL_CHANCE;
	}
	
	private float getSlipperyFailChance() {
		return SLIPPERY_FAIL_CHANCE;
	}
}
