package truckworld.env;

public abstract class Percepts {
	private Percepts() {}
	
	/** Percept functor */
	public static final String
			AG_REGISTERED	= "registered",
			AT_COORD 		= "at", 
			AT_JUNCTION 	= "atJ", 
			FLYING_OVER		= "overJ", 
			ON_ROAD 		= "onR", 
			LOADED 			= "loaded",
			CARRYING_CARGO 	= "carryingCargo",
			CARGO_NEEDED 	= "cargoNeeded",
			CARGO_TYPE 		= "cargo",
			CARGO_AT 		= "cargoAt",
			BUSY			= "busy",
			STUCK 			= "stuck",
			TOXIC 			= "toxic", 
			TOXIC_ROAD		= "toxicRd", 
			DANGERZONE 		= "dangerZone",
			FLYING	 		= "flying",
			WINDY	 		= "windy",
			BLOCKED 		= "blocked",
			BLOCKED_ROAD	= "blockedRd",
			AIRPORT			= "airport",
			RESTING			= "resting";

}
