/**
 * 
 */
package truckworld.env;

/**
 * @author Alan White
 *
 *         Defines constants relating to the simulated environment, such as
 *         property names
 */
public final class EnvironmentConstants {
	/**
	 * Used to charactise not just the result, but the failure reason for an
	 * action execution;</br></br> <B>pass</b>: action successfully
	 * completed</br> <B>fail_precond</b>: failure - preconditions did not hold
	 * - for example, agent is not in a healthy state at start of action, road
	 * is blocked, etc</br> <B>fail_random</b>: failure - due to 'random' event,
	 * i.e. probabilistic failure value. Intended to represent failures that
	 * occur due to some factor(s) that were not, or cannot be,
	 * deterministically modelled in the simulation and/or agent local domain
	 * knowledge</br> <B>fail_during</b>: failure - due to some event (i.e.
	 * exogenous) during execution. For example, if a road floods whilst an
	 * agent is travelling down it</br> <B>fail_args</b>: failure - invalid
	 * arguments, represents programmer error.... </br></br> This is primarily
	 * intended to allow richer statistical recording for results; the agent
	 * itself still receives a true/false result.
	 */
	public enum ActionResult {
		pass, fail_precond, fail_nondeterministic, fail_exogenous_change, fail_args
	};


	/**
	 * Restricts the total number of cargo demand generation instances (not the
	 * total number of current demands, but the number of 'add a demand ops')
	 * before a reset is required. i.e. if = 100, after 100 generation ops have
	 * been performed then no more can be done before a reset is called. Note
	 * that this needs co-ordinated with the need-gen-chance.
	 */
	public static final String PROPERTY_CARGO_DEMAND_GEN_CAP = "CargoDemandCap";

	/**
	 * Sets cargo generation range (how many cargo objects)
	 */
	public static final String PROPERTY_MAX_CARGO_GEN_COUNT = "MaxCargoGen",
			PROPERTY_MIN_CARGO_GEN_COUNT = "MinCargoGen";

	/**
	 * Probability of a wind gust
	 */
	public static final String PROPERTY_WIND_CHANCE = "WindChance";
	public static final String PROPERTY_WIND_DELAY = "WindInterval";
	public static final String PROPERTY_MIN_GUST = "MinGust";
	public static final String PROPERTY_MAX_GUST = "MaxGust";

	/** Standard time for executing a 'load' action */
	public static final int VEHICLE_STD_LOAD_TIME = 1;

	/**
	 * Standard time for executing a 'unload' action
	 */
	public static final int VEHICLE_STD_UNLOAD_TIME = 1;

	/** Standard time for executing a 'repair' action from damaged state */
	public static final int VEHICLE_STD_REPAIR_DAMAGED_TIME = 1;

	/** Standard time for executing a 'repair' action from mortal state */
	public static final int VEHICLE_STD_REPAIR_MORTAL_TIME = 1;

	/**
	 * Time to secure a location
	 */
	public static final int VEHICLE_STD_SECURE_TIME = 1;

	/** Helicopter takeoff time */
	public static final long HELI_STD_TAKEOFF_TIME = 1;
	public static final long HELI_STD_LAND_TIME = 1;

}
