package truckworld.world;

/**
 * 
 * @author Alan White
 *
 * A transportable cargo container
 */ 
public class Cargo implements Cloneable {

	private static int UUID_NUM = 1;
	
	private final static String DEFAULT_NAME = "cargo"; //default
	
	/** UID for cargo object */
	private String name;
	
	/** Type of object */
	private Type type;
	
	/**
	 * Used to denote type if we want to implement hazMat scenarios for
	 * spills, etc
	 */
	public enum Type {Hazardous, Safe};
	
	/**
	 * Constructor which automatically sets the name/UUID
	 */
	public Cargo(Type type)				{	
		this((DEFAULT_NAME + UUID_NUM++).toLowerCase(), type);
	}
	
	/**
	 * Be careful using this - no  uuid guarantee!
	 * @param id
	 * @param type
	 */
	public Cargo(String id, Type type)	{ 
		this.name = id;				
		this.type = type;
	}
	
	public String getName()		{	return name;	}

	public Type getType()	{	return type;	}
	
	/**
	 * Creates a copy of this cargo with the same UUID; intended 
	 * where we want to avoid destruction by dereferencing...
	 */
	public Object clone()		{	return new Cargo(name, type);	}
	
	public String toString()	{ 	return type + " Cargo:" + name;	}
}
