/**
 * 
 */
package truckworld.world;

import java.awt.geom.Point2D;

import truckworld.exceptions.ConnectionExistsException;
import truckworld.world.interfaces.JunctionInf;
import truckworld.world.interfaces.RoadInf;

import common.UniversalConstants;
import common.UniversalConstants.RoadCondition;
import common.UniversalConstants.RoadType;


/**
 * @author Alan White
 *
 * A road connects junction a to junction b
 */
public class Road implements RoadInf {
	/** unique id */
	private String id;
	
	/** ends of bidrectional road */
	private final JunctionInf[] ends;
	
	/** Road length */
	private float length;

	/** If true, something is obstructing the road*/
	private boolean blocked;
	
	/** If true,toxic spill*/
	private boolean toxic;
	
	/** The type of this road, i.e. MUD or TARMAC*/
	private RoadType type;

	/**
	 * Road is dry, slippery or flooded
	 */
	private RoadCondition condition;

	public Road(	String id, RoadType type, JunctionInf end1, JunctionInf end2,
			RoadCondition condition, boolean blocked) 
		throws ConnectionExistsException{
		this.type = type;
		this.id = id.toLowerCase(); //for planner compat
		this.ends = new JunctionInf[]{end1, end2};
		this.condition = condition;
		this.blocked = blocked;
		this.length = (float) end1.getCoordinate().distance(end2.getCoordinate());
		end1.connectRoad(this);
		end2.connectRoad(this);
	}
	
	public Road(	RoadType type, JunctionInf end1, JunctionInf end2,
					RoadCondition condition, boolean blocked) 
	throws ConnectionExistsException{
		this(end1.getId() + UniversalConstants.ROAD_ID_SEPARATOR_CHAR + end2.getId(), 
				type, end1, end2, condition, blocked);
	}
	
	/**
	 * @param type
	 * @param junct start
	 * @param junct end
	 * @param slippery?
	 * @param flooded?
	 * @param blocked?
	 * @throws ConnectionExistsException 
	 */
	public Road(	RoadType type, JunctionInf end1, JunctionInf end2, boolean slip,
					boolean flood, boolean block) throws ConnectionExistsException {

		this(type, end1, end2, RoadCondition.dry, block);
		if(flood){setCondition(RoadCondition.flooded);}
		else if(slip){setCondition(RoadCondition.slippery);}
	}

	@Override
	public void setId(String ID) {this.id = ID;}

	@Override
	public String getId() {return id;}
	
	@Override
	public JunctionInf[] getEnds(){	return ends;	}
	
	@Override
	public String[] getEndIds() {
		return new String[]{ends[0].getId(), ends[1].getId()};
	}
	
	@Override
	public float getLength() {return length;}

	@Override
	public void setBlocked(boolean blocked) {	this.blocked = blocked;}

	@Override
	public boolean isBlocked() {return blocked;}

	@Override
	public void setCondition(RoadCondition cond){
		this.condition = cond;
	}

	@Override
	public boolean isSlippery() {return condition.equals(RoadCondition.slippery);}

	@Override
	public boolean isFlooded() {return condition.equals(RoadCondition.flooded);}

	@Override
	public RoadType getRoadType() {return this.type;}
	
	public String toString()	{	
		String ends1 = "(Safe)";
		if(ends[0].isDangerous())	{	ends1 = "(Dangerous)";	}
		String ends2 = "(Safe)";
		if(ends[1].isDangerous())	{	ends2 = "(Dangerous)";	}
		
		
		return 	type.toString() + " road: " + id + ", length " + length + 
				" from " + ends[0] + " " + ends1 + 
				" to " + ends[1] + " " + ends2 + ", blocked="
				+ blocked + ", condition = " + getConditions().name();
	}

	@Override
	public RoadCondition getConditions() {
		if(isFlooded())			return RoadCondition.flooded;
		else if(isSlippery())	return RoadCondition.slippery;
		else 					return RoadCondition.dry;
	}

	@Override
	public void setToxic(boolean b) {	toxic = b;		}

	@Override
	public boolean isToxic()		{	return toxic;	}

	@Override
	public Point2D getCentre() {
		Point2D p1 = ends[0].getCoordinate();
		Point2D p2 = ends[1].getCoordinate();
		return new Point2D.Double((p2.getX()-p1.getX())/2, (p2.getY() - p1.getY())/2);
	}
	
	@Override
	public Object clone()  throws CloneNotSupportedException	{
		return (RoadInf)super.clone();
	}

}
