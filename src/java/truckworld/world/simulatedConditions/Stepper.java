package truckworld.world.simulatedConditions;

import java.util.Collection;

import truckworld.world.World;
import truckworld.world.simulatedConditions.rain.RainStorm;

public interface Stepper {
	void stepEnvironment();
	void clearWorld();
	World getWorld();
	Collection<RainStorm> getStorms();
	boolean isWindy();
}
