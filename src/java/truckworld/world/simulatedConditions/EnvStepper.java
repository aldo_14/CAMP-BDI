package truckworld.world.simulatedConditions;

import java.awt.geom.Point2D;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Vector;
import java.util.logging.Logger;

import common.UniversalConstants;
import truckworld.env.Cargoworld;
import truckworld.world.World;
import truckworld.world.interfaces.JunctionInf;
import truckworld.world.simulatedConditions.rain.ProbabilisticRainController;
import truckworld.world.simulatedConditions.rain.RainController;
import truckworld.world.simulatedConditions.rain.RainStorm;

/**
 * Called after each action / before goal add to update the environment
 */
public class EnvStepper extends ParamUser implements Stepper {			
	private final Logger logger;
	private final Random random;
	private final Cargoworld env;
	private final RainController rainController; 
	private final ProbabilisticRainController probRainController; 
	private final Point2D windVector = new Point2D.Double(0d,0d);
	
	private final float windyChangeChance, closeRoadChance, openRoadChance, addDzChance, removeDzChance, proportionMaxBlocked;

	private final int maxDzCount, maxRoadClosuresPerStep, maxRoadOpeningsPerStep, maxDzPerStep, maxDzRemovalPerStep, windSpeed;
	
	private int stepCount = 0;
	private int windyChangeCountdown = 0;
	private Map<String, Integer> dzProhibitCountdown;
	
	private static final int GUST_MIN_DELAY = 15;
	private static final int WIND_MIN_DELAY = 15;
	
	private static final int DZ_GEN_MIN_DELAY = 10;
	
	private final Vector<String> closedLastTime, dzAddedLastTime;
	
	private final boolean stormMode;
	
	public EnvStepper(final Cargoworld env, final Properties propFile) {
		this.logger = Logger.getLogger(this.getClass().getName());
		logger.setLevel(UniversalConstants.ENV_LOGGER_LEVEL);
		this.env = env;
		random = new Random(env.getSimSeed());
		rainController = new RainController(env, this, propFile);
		
		this.windyChangeChance = getFloatParam(propFile, Param.windyChangeChange);
		this.closeRoadChance = getFloatParam(propFile, Param.closeRoadChance);
		this.openRoadChance = getFloatParam(propFile, Param.openRoadChance);
		this.addDzChance = getFloatParam(propFile, Param.addDzChance);
		this.removeDzChance = getFloatParam(propFile, Param.removeDzChance);

		final float roadCount = env.getWorld().getAllRoads().size();
		final int maxRoadClosuresPerStepTmp = new Float(roadCount * getFloatParam(propFile, Param.maxRoadClosuresPerStep)).intValue();	
		maxRoadClosuresPerStep = maxRoadClosuresPerStepTmp > 0 ? maxRoadClosuresPerStepTmp : 1;
		final int maxRoadOpeningsPerStepTmp = new Float(roadCount * getFloatParam(propFile, Param.maxRoadOpeningsPerStep)).intValue();	
		maxRoadOpeningsPerStep = maxRoadOpeningsPerStepTmp > 0 ? maxRoadOpeningsPerStepTmp : 1;
		
		final int maxDzPerStepTmp = new Float(roadCount * getFloatParam(propFile, Param.maxDzPerStep)).intValue();
		maxDzPerStep = maxDzPerStepTmp > 0 ? maxDzPerStepTmp : 1;
		final int maxDzRemovalPerStepTmp = new Float(roadCount * getFloatParam(propFile, Param.maxDzRemovalPerStep)).intValue();
		maxDzRemovalPerStep = maxDzRemovalPerStepTmp > 0 ? maxDzRemovalPerStepTmp : 1;
		
		this.windSpeed = (int) (env.getWorld().getCellDimensions()/3f);
		proportionMaxBlocked = getFloatParam(propFile, Param.proportionMaxBlocked);
		float proportionDzLimit = getFloatParam(propFile, Param.proportionDzLimit);
		this.maxDzCount = (int)(env.getWorld().getAllJunctions().size() * proportionDzLimit);
		this.dzProhibitCountdown = initalizeDzProhibitMap();
		closedLastTime = new Vector<>();
		dzAddedLastTime = new Vector<>();
		stormMode = getBooleanParam(propFile, Param.stormMode);
		probRainController = new ProbabilisticRainController(env, propFile);
	}
	
	private Map<String, Integer> initalizeDzProhibitMap() {
		final HashMap<String, Integer> proMap = new HashMap<>();
		for(final JunctionInf jId: env.getWorld().getAllJunctions()) {
			proMap.put(jId.getId(), 0);
		}
		return proMap;
	}

	public void stepEnvironment() {
		stepCount++;
		logger.info("Start step (" + stepCount + ")");
		clearDangerzones();
		addNewDangerzones();
		openRoads();
		blockRoads();
		doRainOnTheRoads();
		setIfWindy();
	}

	private void doRainOnTheRoads() {
		if(stormMode) {
			rainController.stepUpdateRain();
		}
		else {
			probRainController.roadStateUpdate();
		}
	}

	private void openRoads() {
		final Vector<String> blockedRoads = env.blockedSet();
		blockedRoads.removeAll(closedLastTime);
		int removeCount = (maxRoadOpeningsPerStep < 2) ? 1 : 1 + random.nextInt(maxRoadOpeningsPerStep-1);
		if(random.nextFloat() < openRoadChance) {
			while(!blockedRoads.isEmpty() && removeCount-- > 0) {
				env.openRoad(getRandomEntry(blockedRoads));
			}
		}
	}

	private void clearDangerzones() {
		final Vector<String> dzs = new Vector<>();
		dzs.addAll(env.getDangerZones().keySet());
		dzs.removeAll(dzAddedLastTime);
		int removeCount = (maxDzRemovalPerStep < 2) ? 1 : 1 + random.nextInt(maxDzRemovalPerStep-1);
		if(random.nextFloat() < removeDzChance) {
			while(env.getDangerZones().size()>0 && (removeCount-- > 0)) {
				env.removeDangerZone(getRandomEntry(env.getDangerZones().keySet()));
			}
		}
	}

	private void setIfWindy() {
		boolean change = (random.nextDouble() < windyChangeChance) && (windyChangeCountdown <= 0);
		if(change) {
			if(!env.isWindy()) {
				final int xFactor = random.nextBoolean() ? -windSpeed : windSpeed;
				final int yFactor = random.nextBoolean() ? -windSpeed : windSpeed;				
				windVector.setLocation(random.nextFloat()*xFactor, random.nextFloat()*yFactor);
				env.setWindyPercept(true);
				windyChangeCountdown = GUST_MIN_DELAY;
			}
			else {
				windVector.setLocation(0, 0);
				env.setWindyPercept(false);
				windyChangeCountdown = WIND_MIN_DELAY;
			}
		}
		else {
			windyChangeCountdown--;
		}
	}

	public Point2D getWindVector() {
		return windVector;
	}
	
	private void blockRoads() {
		if(wantToBlockMoreRoads() && canBlockMoreRoads()) {
			final int nextRandInt = random.nextInt(maxRoadClosuresPerStep);
			blockRoads(nextRandInt == 0? 1 : nextRandInt);
		}
	}

	private void blockRoads(int numToBlock) {
		final List<String> unblockedRoads = getUnblockedRoads();
		closedLastTime.clear();
		while (!unblockedRoads.isEmpty() && (numToBlock-- > 0) && canBlockMoreRoads()) {
			final String blockMe = (String)getRandomEntry(unblockedRoads);
			env.closeRoad(blockMe);
			unblockedRoads.remove(blockMe);
			closedLastTime.add(blockMe);
		}
	}

	private boolean wantToBlockMoreRoads() {
		return (random.nextFloat() < closeRoadChance);
	}
	
	private List<String> getUnblockedRoads() {
		final Vector<String> unblocked = new Vector<>();
		for(final String road: env.getWorld().getAllRoadsIds()) {
			if(	!env.getWorld().getBlockedRoadIds().contains(road) && !closedLastTime.contains(road) && 
				!env.getWorld().getRoad(road).isFlooded()) {
				unblocked.add(road);
			}
		}
		return unblocked;
	}

	private boolean canBlockMoreRoads() {
		return env.getBlockedCount() < (env.getWorld().getAllRoads().size()*proportionMaxBlocked);
	}

	private void addNewDangerzones() {
		decrementProhibitList();
		final int jCount = env.getWorld().getAllJunctions().size();
		if((random.nextDouble() < addDzChance) && (numberOfDzs() < maxDzCount)) {
			int toAdd = (maxDzPerStep<=1)? 1 : (1+random.nextInt(maxDzPerStep-1));
			toAdd = ((numberOfDzs()+toAdd)<jCount) ? numberOfDzs() : jCount - numberOfDzs();
			for(int i=0; (i<=toAdd) && (numberOfDzs()<maxDzCount); i++) {
				addDangerzone();
			}
		}
	}
	
	private int numberOfDzs() { 
		return env.getDangerZones().keySet().size();
	}

	private void decrementProhibitList() {
		for(final String jId: dzProhibitCountdown.keySet()) {
			final int newValue = dzProhibitCountdown.get(jId)-1;
			dzProhibitCountdown.put(jId, newValue);
		}
	}

	private void addDangerzone() {
		dzAddedLastTime.clear();
		final List<String> safeJunctions = new Vector<>();
		for(final String junctionIds : env.getWorld().getJunctionNames()) {
			if(!env.getDangerZones().containsKey(junctionIds) && (dzProhibitCountdown.get(junctionIds)<=0)) {
				safeJunctions.add(junctionIds);
			}
		}
		if(!safeJunctions.isEmpty()) {
			final String dzLocation = (String)getRandomEntry(safeJunctions);
			env.addDangerZone(dzLocation);
			dzAddedLastTime.addElement(dzLocation);
			dzProhibitCountdown.put(dzLocation, DZ_GEN_MIN_DELAY);
		}
	}

	private String getRandomEntry(final Collection<String> list) {
		return list.isEmpty() ? "" : (String) list.toArray()[random.nextInt(list.size())];
	}

	public Collection<RainStorm> getStorms() {
		return rainController.getStorms();
	}
	

	public void clearWorld() {
		this.windVector.setLocation(0, 0);
		final Vector<String> dzs = new Vector<String>();
		dzs.addAll(env.getDangerZones().keySet());
		for(final String dz: dzs) {
			env.removeDangerZone(dz);
		}
		final Vector<String> blocked = new Vector<String>();
		blocked.addAll(env.getWorld().getBlockedRoadIds());
		logger.info("Clear blocked: " + blocked);
		for(final String blockedRoad: blocked) {
			logger.info("Unblock " + blockedRoad);
			env.openRoad(blockedRoad);
		}
		rainController.dryEverythingOut();
	}

	public World getWorld() {
		return env.getWorld();
	}

	public RainController getRain() {
		return rainController;
	}

	public boolean isWindy() {
		return (getWindVector().getX() != 0) && (getWindVector().getY() != 0);
	}

}
