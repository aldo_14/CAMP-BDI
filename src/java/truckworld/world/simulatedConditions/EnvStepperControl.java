package truckworld.world.simulatedConditions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;

import common.UniversalConstants.RoadCondition;
import truckworld.world.World;
import truckworld.world.interfaces.JunctionInf;
import truckworld.world.interfaces.RoadInf;

public class EnvStepperControl extends JFrame implements ActionListener {
	private static final long serialVersionUID = -800756674582412154L;
	private final Stepper stepper;

	private boolean stepping = false;

	private JMenuItem start1s, start5s, start10s, singleStep, jump10, jump100, pause, reset, sampling;
	private JTextArea content;

	public EnvStepperControl(final Stepper stepper) {
		this.stepper = stepper;
		start1s = new JMenuItem("Start; 1ms interval");
		start1s.addActionListener(this);
		start5s = new JMenuItem("Start; 5ms interval");
		start5s.addActionListener(this);
		start10s = new JMenuItem("Start; 10ms interval");
		start10s.addActionListener(this);
		pause = new JMenuItem("Pause");
		pause.addActionListener(this);
		singleStep = new JMenuItem("Step once");
		singleStep.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, ActionEvent.CTRL_MASK));
		singleStep.addActionListener(this);
		jump10 = new JMenuItem("Step; 10 times");
		jump10.addActionListener(this);
		jump10.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_0, ActionEvent.CTRL_MASK));
		jump100 = new JMenuItem("Step; 100 times");
		jump100.addActionListener(this);
		jump100.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_1, ActionEvent.CTRL_MASK));
		reset = new JMenuItem("Reset world to clear");
		reset.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK));
		reset.addActionListener(this);
		sampling = new JMenuItem("Sampler");
		sampling.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
		sampling.addActionListener(this);

		JMenuBar menuBar = new JMenuBar();
		JMenu menu = new JMenu("Test");
		menu.add(start1s);
		menu.add(start5s);
		menu.add(start10s);
		menu.add(pause);
		menu.addSeparator();
		menu.add(singleStep);
		menu.add(jump10);
		menu.add(jump100);
		menu.addSeparator();
		menu.add(sampling);
		menu.add(reset);
		menuBar.add(menu);
		setJMenuBar(menuBar);
		
		content = new JTextArea("");
		content.setEditable(false);
		this.getContentPane().add(content);
		setSize(400, 250);
		setVisible(true);
	}

	private void startStepping(final int msDelay) {
		if (stepping) {
			return;
		}
		stepping = true;
		new Thread(new Runnable() {
			@Override
			public void run() {
				while (true) {
					if (stepping) {
						try {
							System.out.println("START STEPPIN'!");
							Thread.sleep(msDelay);
							stepper.stepEnvironment();
						} catch (InterruptedException e) {
						}
					}
				}
			}
		}).start();
	}

	@Override
	public void actionPerformed(final ActionEvent action) {
		if (action.getSource().equals(start1s) && !stepping) {
			startStepping(1000);
		} else if (action.getSource().equals(start5s) && !stepping) {
			startStepping(5000);
		} else if (action.getSource().equals(start10s) && !stepping) {
			startStepping(10000);
		} else if (action.getSource().equals(singleStep)) {
			stepper.stepEnvironment();
		} else if (action.getSource().equals(pause)) {
			pause.setText(stepping ? "Resume" : "Pause");
			this.stepping = !stepping;
		} else if (action.getSource().equals(reset)) {
			stepper.clearWorld();
		} else if (action.getSource().equals(jump10)) {
			for (int i = 0; i < 10; i++) {
				stepper.stepEnvironment();
			}
		} else if (action.getSource().equals(jump100)) {
			for (int i = 0; i < 100; i++) {
				stepper.stepEnvironment();
			}
		} else if (action.getSource().equals(sampling)) {
			doSamplingRun();
		}
	}

	private void doSamplingRun() {
		float dryTot = 0, slipTot = 0, floodTot = 0, blockTot = 0, dzTot = 0;
		final int iterations = 10000;
		for (int i = 0; i < iterations; i++) {
			stepper.stepEnvironment();
			
			final World world = stepper.getWorld();
			for (final RoadInf r : world.getAllRoads()) {
				if (r.getConditions().equals(RoadCondition.flooded)) {
					floodTot++;
				}
				else if (r.getConditions().equals(RoadCondition.slippery)) {
					slipTot++;
				} 
				else {
					dryTot++;
				}
				if (r.isBlocked()) {
					blockTot++;
				}
			}
			for (final JunctionInf j : world.getAllJunctions()) {
				if (j.isDangerous()) {
					dzTot++;
				}
			}
		}
		final int roadCount = stepper.getWorld().getAllRoads().size();
		final float averageDry = (dryTot/iterations)/roadCount;
		final float averageSlippery = (slipTot/iterations)/roadCount;
		final float averageFlooded = (floodTot/iterations)/roadCount;
		final float averageBlocked = (blockTot/iterations)/roadCount;
		final float averageDz = (dzTot/iterations)/stepper.getWorld().getJunctionNames().size();
		content.setText("Sampled Values;\n Dry " + (averageDry*100) + "%\nSlippery " + (averageSlippery*100) + "%\nFlooded "
				+ (averageFlooded*100) + "%\nBlocked " + (averageBlocked*100) + "%\nDangerous " + (averageDz*100) + "%");
	}
}
