package truckworld.world.simulatedConditions;

import static java.lang.Integer.parseInt;
import static java.lang.Float.parseFloat;
import static java.lang.Boolean.parseBoolean;

import java.util.Properties;

public abstract class ParamUser {
	protected enum Param{
		windyChangeChange("windyChangeChance", "0.3"),
		closeRoadChance("closeRoadChance", "0.6"),
		openRoadChance("openRoadChance", "0.01"),
		addDzChance("addDzChance", "0.5"),
		removeDzChance("removeDzChance", "0.5"),
		maxRoadClosuresPerStep("maxRoadClosuresPerStep", "0"),
		maxRoadOpeningsPerStep("maxRoadOpeningsPerStep", "0"),
		maxDzPerStep("maxDzPerStep", "0"),
		maxDzRemovalPerStep("maxDzRemovalPerStep", "0"),
		proportionMaxBlocked("proportionMaxBlocked", "0.5"),
		proportionDzLimit("proportionDzLimit","0.6"),
		minRain("minRainfallPerTurn", "60"),
		maxRain("maxRainfallPerTurn", "120"),
		dryRate("roadDryingPerTurn", "60"),
		stormChance("stormGenerationChance", "0"),
		minStormLife("minStormLife", "25"),
		maxStormLife("maxStormLife", "50"),
		maxAccumulation("maxRainOnRoad", "1800"),
		slipperyMinAccumulation("slipperyMin", "800"),
		slipperyMaxAccumulation("slipperyMax", "1100"),
		floodedMinAccumulation("floodedMin", "900"),
		floodedMaxAccumulation("floodedMax", "1200"),
		cargoDestroyChance("cargoDestroyChance","0"),
		hazardousCargoChance("hazardousCargoChance", "0"),
		cargoSpillChance("cargoSpillChance", "0"),
		chanceOfPostFailureDamage("chanceOfPostFailureDamage", "0"),
		chanceOfRandomDamage("chanceOfRandomDamage", "0"),
		idleHealTime("idleHealTime", "3"),
		unstuckTime("unstuckTime", "0"), 
		stuckChance("stuckChance", "0"),
		chanceOfDamageSecuringAnArea("chanceOfDamageSecuringAnArea", "0"),
		stormMode("stormMode", "false"),
		chanceOfFlooding("chanceOfFlooding", "0.0"),
		chanceOfDrying("chanceOfDrying", "0.0"),
		maxFlooded("maxFlooded", "0.0"),
		maxSlippery("maxSlippery", "0.0"),
		maxFloodPerTurn("maxFloodPerTurn", "1"),
		maxDryPerTurn("maxDryPerTurn", "1"),
		stateChangeChance("stateChangeChance", "1"),
		maxRoadChanges("maxRoadChanges", "0.41"),
		numStateChanges("numStateChanges", "1");
		
		private final String tagName;
		private final String defaultValue;
		Param(final String tagName, final String defaultValue) {
			this.tagName = tagName;
			this.defaultValue = defaultValue;
		}
		
		protected String key() {
			return tagName;
		}
		
		protected String defaultVal() {
			return defaultValue;
		}
	}
	
	protected final int getIntParam(final Properties propFile, final Param conf) {
		return parseInt(propFile.getProperty(conf.key(), conf.defaultVal()));
	}
	
	protected final float getFloatParam(final Properties propFile, final Param conf) {
		return parseFloat(propFile.getProperty(conf.key(), conf.defaultVal()));
	}
	
	protected final boolean getBooleanParam(final Properties propFile, final Param conf) {
		return parseBoolean(propFile.getProperty(conf.key(), conf.defaultVal()));
	}
}
