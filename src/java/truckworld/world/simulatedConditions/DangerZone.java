package truckworld.world.simulatedConditions;

import java.awt.Shape;

import truckworld.world.interfaces.JunctionInf;

public class DangerZone {
	/**
	 * Defines polygonal bounds of this DZ
	 */
	private Shape area;

	/**
	 * The nearest town to this DZ; where to send troops for help
	 */
	private JunctionInf source;

	/**
	 * Number of steps this DZ has existed for
	 */
	private int age;

	public DangerZone(JunctionInf source, Shape area){
		this.source = source;
		this.area = area;
		age = 0;
	}

	public JunctionInf getSource()	{	return source;	}
	public Shape getShape() 		{	return area;	}
	public int getAge()				{	return age;		}
	public void age()				{	age++;		}
}
