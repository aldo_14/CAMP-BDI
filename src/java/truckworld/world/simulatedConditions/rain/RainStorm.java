/**
 * 
 */
package truckworld.world.simulatedConditions.rain;

import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.util.logging.Logger;

import truckworld.world.interfaces.JunctionInf;

/**
 * @author Alan White
 * 
 *         Represents a circular area under rainy conditions
 */
public class RainStorm implements Cloneable{
	private Ellipse2D stormArea;
	private int lifespan, radius;
	private final Point2D centre;
	private final boolean moving;
	private final float movingXSpeed;
	private final float movingYSpeed;
	private final float intensity;

	public float getIntensity() {
		return intensity;
	}

	public RainStorm(final Point2D centre, final int radius, final int lifespan, final float intensity) {
		this(centre, radius, lifespan, false, 0, 0, intensity);
	}

	public RainStorm(final Point2D centre, final int radius, final int lifespan, final boolean moving, 
			final float moveX, final float moveY, final float intensity) {
		stormArea = new Ellipse2D.Double(centre.getX() - radius, centre.getY() - radius, radius * 2, radius * 2);
		Logger.getLogger(getClass().getSimpleName());
		this.lifespan = lifespan;
		this.radius = radius;
		this.centre = centre;
		this.moving = moving;
		this.movingXSpeed = moveX;
		this.movingYSpeed = moveY;
		this.intensity = intensity;
	}

	public void setLifespan(int newL) {
		if (newL <= 0)
			lifespan = 0;
		else
			lifespan = newL;
	}

	public int getRadius() {
		return radius;
	}

	public Ellipse2D getStormArea() {
		return stormArea;
	}

	public Point2D getCentre() {
		return centre;
	}

	public int getLifespan() {
		return lifespan;
	}

	public boolean isMoving() {
		return moving;
	}

	/**
	 * Called to update position, if storm is moving
	 */
	public void incrementMove() {
		if (moving) {
			move(movingXSpeed, movingYSpeed);
		}
	}

	/**
	 * Used to allow a storm to move over time
	 * 
	 * @param x
	 * @param y
	 */
	protected void move(final double x, final double y) {
		final double newX = centre.getX() + x;
		final double newY = centre.getY() + y;
		centre.setLocation(newX, newY);
		stormArea = new Ellipse2D.Double(centre.getX() - radius, centre.getY() - radius, radius * 2, radius * 2);
	}

	public float contribution(final JunctionInf j1, final JunctionInf j2) {
		final float factor = totallyCovered(j1, j2) ? 1 : (runningDownhill(j1, j2) ? 0.6f : (runningUphill(j1, j2) ? 0.3f : 0f));
		return this.intensity * factor;
	}

	private boolean totallyCovered(final JunctionInf j1, final JunctionInf j2) {
		final double distanceP1 = j1.getCoordinate().distance(getCentre());
		final double distanceP2 = j2.getCoordinate().distance(getCentre());
		return distanceP1 <= getRadius() && distanceP2 <= getRadius();
	}

	private boolean runningDownhill(final JunctionInf j1, final JunctionInf j2) {
		final double distanceP1 = j1.getCoordinate().distance(getCentre());
		final double distanceP2 = j2.getCoordinate().distance(getCentre());
		return ((distanceP1 <= getRadius()) && (j1.getElevation() >= j2.getElevation()))
				|| ((distanceP2 <= getRadius()) && (j2.getElevation() >= j1.getElevation()));
	}

	private boolean runningUphill(final JunctionInf j1, final JunctionInf j2) {
		final double distanceP1 = j1.getCoordinate().distance(getCentre());
		final double distanceP2 = j2.getCoordinate().distance(getCentre());
		return ((distanceP1 <= getRadius()) && (j1.getElevation() < j2.getElevation()))
				|| ((distanceP2 <= getRadius()) && (j2.getElevation() < j1.getElevation()));
	}

	public String toString() {
		return "Storm@" + centre.toString() + "(" + lifespan + " left, radius: " + radius + ")";
	}
	
	@Override
	public RainStorm clone() {
		return new RainStorm((Point2D) centre.clone(), this.radius, this.lifespan, this.moving, this.movingXSpeed, this.movingYSpeed, this.intensity);
	}

}
