package truckworld.world.simulatedConditions.rain;

import static common.UniversalConstants.RoadCondition.dry;
import static common.UniversalConstants.RoadCondition.flooded;
import static common.UniversalConstants.RoadCondition.slippery;

import java.util.List;
import java.util.Random;
import java.util.Vector;

import common.UniversalConstants.RoadCondition;
import truckworld.env.Cargoworld;
import truckworld.world.World;
import truckworld.world.interfaces.RoadInf;

public class RainControlUtils {

	public static void dryRandomRoadFrom(final Random random, final List<RoadInf> set, final Cargoworld env) {
		final RoadInf randomRoad = getRandomRoadFrom(random, set);
		env.setRoadCondition(randomRoad.getConditions().equals(flooded) ? slippery : dry, randomRoad.getId());
	}
	
	private static RoadInf getRandomRoadFrom(final Random random, final List<RoadInf> set) {
		return set.size()==1? set.get(0) : set.get(random.nextInt(set.size()-1));
	}
	
	public static void wetRandomEntry(final Random random, final List<RoadInf> set, final Cargoworld env) {
		final RoadInf randomRoad = getRandomRoadFrom(random, set);
		env.setRoadCondition(randomRoad.getConditions().equals(dry) ? slippery : flooded, randomRoad.getId());
	}
	
	public static List<RoadInf> getFloodedRoads(final World world) {
		return getRoadsWithCondition(world, flooded);
	}

	public static List<RoadInf> getWetRoads(final World world) {
		return getRoadsWithCondition(world, slippery);
	}

	public static List<RoadInf> getDryRoads(final World world) {
		return getRoadsWithCondition(world, dry);
	}

	private static List<RoadInf> getRoadsWithCondition(final World world, final RoadCondition condition) {
		final List<RoadInf> roads = new Vector<>();
		for (final RoadInf road : world.getAllRoads()) {
			if (road.getConditions().equals(condition)) {
				roads.add(road);
			}
		}
		return roads;
	}
	
}
