package truckworld.world.simulatedConditions.rain;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

import common.UniversalConstants;
import common.UniversalConstants.RoadCondition;
import common.UniversalConstants.RoadType;
import truckworld.env.Cargoworld;
import truckworld.world.interfaces.JunctionInf;
import truckworld.world.interfaces.RoadInf;
import truckworld.world.simulatedConditions.EnvStepper;
import truckworld.world.simulatedConditions.ParamUser;

public class RainController extends ParamUser {

	private final Logger logger;
	private final Cargoworld env;
	private final EnvStepper envStep;
	private final Random random;

	/**
	 * Max storm count
	 */
	private final int maxStorms;

	/**
	 * Storm min-max lifespan (turns)
	 */
	private final int minStormLife, maxStormLife;

	/**
	 * Indicates % chance per step
	 */
	private final float stormGenerationChance;

	/**
	 * Indicates rainfall from storms
	 */
	private final int minRainfallPerTurn, maxRainfallPerTurn;

	/**
	 * How much rain is lost per turn
	 */
	private final int roadDryingPerTurn;

	/**
	 * Default accumulation limits for roads
	 */
	private final int maxRainOnRoad;

	private final int minStormRadius, maxStormRadius;

	/**
	 * How much rain is on the road?
	 */
	private final Map<String, Integer> roadAccumulation = new ConcurrentHashMap<>(30,0.9f,1);

	/**
	 * Transition points for road state
	 */
	private final Map<String, Integer> slipperyWaterLevel = new ConcurrentHashMap<>(30,0.9f,1);
	private final Map<String, Integer> floodedWaterLevel = new ConcurrentHashMap<>(30,0.9f,1);
	private final Vector<RainStorm> rainStorms = new Vector<RainStorm>();

	private static final int ADD_STORM_OFFSET = 50;

	public RainController(final Cargoworld env, final EnvStepper envStep, final Properties propFile) {
		logger = Logger.getLogger(this.getClass().getName());
		logger.setLevel(UniversalConstants.SIM_LOGGER_LEVEL);
		this.env = env;
		this.envStep = envStep;
		this.random = new Random(env.getSimSeed());
		this.maxStorms = (int) (env.getWorld().getAllJunctions().size() / 3);
		this.minRainfallPerTurn = getIntParam(propFile, Param.minRain);
		this.maxRainfallPerTurn = getIntParam(propFile, Param.maxRain);
		this.roadDryingPerTurn = getIntParam(propFile, Param.dryRate);
		;
		this.stormGenerationChance = getFloatParam(propFile, Param.stormChance);
		;
		this.minStormLife = getIntParam(propFile, Param.minStormLife);
		this.maxStormLife = getIntParam(propFile, Param.maxStormLife);
		this.minStormRadius = (int) (env.getWorld().getCellDimensions());
		this.maxStormRadius = (int) (env.getWorld().getCellDimensions() * 1.5);
		this.maxRainOnRoad = getIntParam(propFile, Param.maxAccumulation);
		final int slipperyMin = getIntParam(propFile, Param.slipperyMinAccumulation);
		final int slipperyMax = getIntParam(propFile, Param.slipperyMaxAccumulation);
		final int floodedMin = getIntParam(propFile, Param.floodedMinAccumulation);
		final int floodedMax = getIntParam(propFile, Param.floodedMaxAccumulation);
		setRainAccumulationLimits(slipperyMin, slipperyMax, floodedMin, floodedMax);
		setInitialRainfall();
	}

	private void setRainAccumulationLimits(final int slipperyMin, final int slipperyMax, final int floodedMin,
			final int floodedMax) {
		final float longestRoad = getLongestRoad();
		final float lengthDiff = longestRoad - getShortestRoad();
		final int slipRange = slipperyMax - slipperyMin;
		final int floodRange = floodedMax - floodedMin;

		for (final RoadInf road : env.getWorld().getAllRoads()) {
			final float roadLengthDiff = (longestRoad - road.getLength());
			final float multiplier = (roadLengthDiff == 0) ? 1 : roadLengthDiff / lengthDiff;
			if (multiplier > 1) {
				throw new RuntimeException("Multiplier is weird " + multiplier);
			}
			slipperyWaterLevel.put(road.getId(), (int) ((multiplier * slipRange) + slipperyMin));
			floodedWaterLevel.put(road.getId(), (int) ((multiplier * floodRange) + floodedMin));
		}
	}

	private float getLongestRoad() {
		float longestRoad = 0;
		for (final RoadInf r : env.getWorld().getAllRoads()) {
			longestRoad = r.getLength() > longestRoad ? r.getLength() : longestRoad;
		}
		if (longestRoad == 0) {
			throw new RuntimeException("Longest road returned zero!");
		}
		return longestRoad;
	}

	private float getShortestRoad() {
		float shortestRoad = Integer.MAX_VALUE;
		for (final RoadInf r : env.getWorld().getAllRoads()) {
			shortestRoad = (r.getLength() < shortestRoad) ? r.getLength() : shortestRoad;
		}
		if (shortestRoad == 0) {
			throw new RuntimeException("shortest road returned zero!");
		}
		return shortestRoad;
	}

	private void setInitialRainfall() {
		for (final String road : env.getWorld().getAllRoadsIds()) {
			final Integer floodThreshold = floodedWaterLevel.get(road);
			if (env.getRoadCondition(road).equals(RoadCondition.flooded)) {
				final int diff = maxRainOnRoad - floodThreshold;
				roadAccumulation.put(road, floodThreshold + (diff / 2));
			} else if (env.getRoadCondition(road).equals(RoadCondition.slippery)) {
				final Integer slipperyThreshold = slipperyWaterLevel.get(road);
				final int diff = floodThreshold - slipperyThreshold;
				roadAccumulation.put(road, slipperyThreshold + (diff / 2));
			} else {
				roadAccumulation.put(road, 0);
			}
		}
	}

	public void stepUpdateRain() {
		synchronized (rainStorms) {
			updateRainfallAccumulation();
			updateRoadState();
			ageRainStorms();
			spawnNewRainStorms();
			if (env.isWindy()) {
				moveStorms();
			}
		}
	}

	private void moveStorms() {
		for (final RainStorm storm : rainStorms) {
			final Point2D windVector = envStep.getWindVector();
			storm.move(windVector.getX(), windVector.getY());
		}
	}

	private void spawnNewRainStorms() {
		if ((rainStorms.size() < maxStorms) && (random.nextFloat() < stormGenerationChance)) {
			addRandomRainStorm();
		}
	}

	private void addRandomRainStorm() {
		final Point2D location = getRandomStormLocation();
		final int xOff = random.nextBoolean() ? -random.nextInt(ADD_STORM_OFFSET) : random.nextInt(ADD_STORM_OFFSET);
		final int yOff = random.nextBoolean() ? -random.nextInt(ADD_STORM_OFFSET) : random.nextInt(ADD_STORM_OFFSET);
		location.setLocation(location.getX() + xOff, location.getY() + yOff);
		final int radius = minStormRadius != maxStormRadius
				? minStormRadius + random.nextInt(maxStormRadius - minStormRadius) : minStormRadius;
		final int life = minStormLife + random.nextInt(maxStormLife - minStormLife);
		final float intensity = this.minRainfallPerTurn
				+ (random.nextFloat() * (maxRainfallPerTurn - minRainfallPerTurn));
		final RainStorm newStorm = new RainStorm(location, radius, life, intensity);
		this.rainStorms.add(newStorm);
	}

	private Point2D getRandomStormLocation() {
		return new Point2D.Double(random.nextInt(env.getWorld().getDimensionsX()),
				random.nextInt(env.getWorld().getDimensionsY()));
	}

	private void ageRainStorms() {
		final Vector<RainStorm> deadlist = new Vector<RainStorm>();
		for (final RainStorm rain : rainStorms) {
			if (rain.getLifespan() == 0) {
				deadlist.add(rain);
			} else {
				rain.setLifespan(rain.getLifespan() - 1);
			}
		}
		logger.fine("Removing " + deadlist.size() + " rainstorms");
		rainStorms.removeAll(deadlist);
	}

	private void updateRoadState() {
		for (final String road : env.getWorld().getAllRoadsIds()) {
			final RoadCondition newCondition = getAccumulationState(road);
			final RoadCondition currentCondition = env.getRoadCondition(road);
			if (!currentCondition.equals(newCondition)) {
				if (legalToChangeState(road, newCondition)) {
					env.setRoadCondition(newCondition, road);
				} else {
					roadAccumulation.put(road,
							currentCondition.equals(RoadCondition.dry)
									? slipperyWaterLevel.get(road) - roadDryingPerTurn
									: floodedWaterLevel.get(road) - roadDryingPerTurn);
				}
			}
		}
	}

	private boolean legalToChangeState(final String roadId, final RoadCondition condition) {
		final RoadInf road = env.getWorld().getRoad(roadId);
		final boolean wouldClose = condition.equals(RoadCondition.flooded)
				|| (condition.equals(RoadCondition.slippery) || road.getRoadType().equals(RoadType.mud));
		return wouldClose ? !closingWouldOrphanAnEnd(road) : true;
	}

	private boolean closingWouldOrphanAnEnd(final RoadInf road) {
		return !isAccessibleByOtherMeans(road.getEnds()[0], road.getId())
				|| !isAccessibleByOtherMeans(road.getEnds()[1], road.getId());
	}

	private boolean isAccessibleByOtherMeans(final JunctionInf junction, final String omittedLink) {
		final Vector<String> connections = new Vector<>();
		connections.addAll(junction.getRoads());
		connections.remove(omittedLink);
		for (final String roadId : connections) {
			if (isUsable(env.getWorld().getRoad(roadId))) {
				return true;
			}
		}
		return false;
	}

	private boolean isUsable(final RoadInf road) {
		return road.getConditions().equals(RoadCondition.dry)
				|| (road.getConditions().equals(RoadCondition.slippery) && road.getRoadType().equals(RoadType.tarmac));
	}

	private RoadCondition getAccumulationState(final String road) {
		final int currentAccumulation = roadAccumulation.get(road);
		if (currentAccumulation < slipperyWaterLevel.get(road)) {
			return RoadCondition.dry;
		} else if (currentAccumulation >= floodedWaterLevel.get(road)) {
			return RoadCondition.flooded;
		} else {
			return RoadCondition.slippery;
		}
	}

	private void updateRainfallAccumulation() {
		for (final RoadInf road : env.getWorld().getAllRoads()) {
			final int contribution = (int) (getContribution(road, rainStorms));
			if (contribution > 0) {
				roadAccumulation.put(road.getId(), roadAccumulation.get(road.getId()) + contribution);
			} else {
				dryOut(road);
			}
		}
	}

	private void dryOut(final RoadInf road) {
		final int newLevel = roadAccumulation.get(road.getId()) - roadDryingPerTurn;
		roadAccumulation.put(road.getId(), (newLevel < 0) ? 0 : newLevel);
		final RoadCondition newCondition = getAccumulationState(road.getId());
		if (!road.getConditions().equals(newCondition)) {
			env.setRoadCondition(newCondition, road.getId());
		}
	}

	private float getContribution(final RoadInf road, final Vector<RainStorm> stormSet) {
		float contribution = 0;
		for (final RainStorm storm : stormSet) {
			contribution += storm.contribution(road.getEnds()[0], road.getEnds()[1]);
		}
		return contribution;
	}

	public Collection<RainStorm> getStorms() {
		synchronized (rainStorms) {
			final Collection<RainStorm> copyOf = new ArrayList<>();
			for (final RainStorm storm : rainStorms) {
				copyOf.add(storm.clone());
			}
			return copyOf;
		}
	}

	public void dryEverythingOut() {
		synchronized (rainStorms) {
			rainStorms.clear();
			for (final String road : env.getWorld().getAllRoadsIds()) {
				roadAccumulation.put(road, 0);
				env.setRoadCondition(RoadCondition.dry, road);
			}
		}
	}

	public int getAccumulation(final String id) {
		return roadAccumulation.get(id);
	}

	public int getSlipperyLimit(final String id) {
		return this.slipperyWaterLevel.get(id);
	}

	public int getFloodLimit(final String id) {
		return this.floodedWaterLevel.get(id);
	}

}
