package truckworld.world.simulatedConditions.rain;

import static truckworld.world.simulatedConditions.rain.RainControlUtils.dryRandomRoadFrom;
import static truckworld.world.simulatedConditions.rain.RainControlUtils.getDryRoads;
import static truckworld.world.simulatedConditions.rain.RainControlUtils.getFloodedRoads;
import static truckworld.world.simulatedConditions.rain.RainControlUtils.getWetRoads;
import static truckworld.world.simulatedConditions.rain.RainControlUtils.wetRandomEntry;

import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.Vector;
import java.util.logging.Logger;

import common.UniversalConstants;
import truckworld.env.Cargoworld;
import truckworld.world.interfaces.RoadInf;
import truckworld.world.simulatedConditions.ParamUser;

public class ProbabilisticRainController extends ParamUser {
	private final Logger logger;
	private final Random random;
	private final Cargoworld env;

	private final float floodChance, dryChance;
	private final int maxSlippery, maxFlooded, maxDryPerTurn, maxFloodPerTurn;

	public ProbabilisticRainController(final Cargoworld env, final Properties propFile) {
		this.logger = Logger.getLogger(this.getClass().getName());
		logger.setLevel(UniversalConstants.ENV_LOGGER_LEVEL);
		this.env = env;
		random = new Random(env.getSimSeed());
		dryChance = getFloatParam(propFile, Param.chanceOfDrying);
		floodChance = getFloatParam(propFile, Param.chanceOfFlooding);
		final float roadCount = env.getWorld().getAllRoads().size();
		maxDryPerTurn = new Float(roadCount * getFloatParam(propFile, Param.maxDryPerTurn)).intValue();
		maxFloodPerTurn =  new Float(roadCount * getFloatParam(propFile, Param.maxFloodPerTurn)).intValue();
		maxSlippery = new Float(roadCount * getFloatParam(propFile, Param.maxSlippery)).intValue();
		maxFlooded = new Float(roadCount * getFloatParam(propFile, Param.maxFlooded)).intValue();
		logger.info("Started with dryChance= " + dryChance + " " + maxDryPerTurn + "( pt), floodChance=" + floodChance
				+ " " + maxFloodPerTurn + "( pt)\nmaxSlippery=" + maxSlippery + ", maxFlooded=" + maxFlooded + " of " + roadCount);
	}

	public void roadStateUpdate() {
		logger.info("Entering state update");
		final List<RoadInf> dry = getDryRoads(env.getWorld());
		final List<RoadInf> wet = getWetRoads(env.getWorld());
		final List<RoadInf> flood = getFloodedRoads(env.getWorld());
		
		if (random.nextFloat() < dryChance) 	{
			final int roadChanges = (maxDryPerTurn<=1) ? 1: (1+random.nextInt(maxDryPerTurn-1));
			for(int i=0; i<roadChanges; i++) {
				if(!wet.isEmpty() && !flood.isEmpty()) {
					final List<RoadInf> set = new Vector<>();
					set.addAll(wet);
					set.addAll(flood);
					dryRandomRoadFrom(random, set, env);
				}
			}
		}
		if (random.nextFloat() < floodChance || wet.isEmpty() || flood.isEmpty()) {
			final int roadChanges = (maxFloodPerTurn<=1) ? 1: (1+random.nextInt(maxFloodPerTurn-1));
			for(int i=0; i<roadChanges; i++) {
				wetRandomRoad(dry, wet, flood);
			}
		}
		logger.info("Completed state update");
	}

	public void wetRandomRoad(final List<RoadInf> dry, final List<RoadInf> wet, final List<RoadInf> flood) {
		final List<RoadInf> set = new Vector<>();
		if (flood.size() < maxFlooded) {
			set.addAll(wet);
		}
		if (wet.size() < maxSlippery) {
			set.addAll(dry);
		}
		set.removeAll(env.getWorld().getBlockedRoads());
		if (!set.isEmpty()) {
			wetRandomEntry(random, set, env);
		}
	}


}
