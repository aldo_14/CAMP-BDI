package truckworld.world.simulatedConditions;

import java.util.Properties;

public class FailureConsequences extends ParamUser {
	private final float cargoDestroyAfterFailureChance;
	private final float hazardousCargoGenerationChance;
	private final float cargoSpillAfterFailureChance;
	private final float postFailureDamageChance;
	private final float randomDamageChance;
	private final float secureAreaDamageChance;
	private final float stuckChance;
	public float getStuckChance() {
		return stuckChance;
	}

	private final int idleHealingTime;
	private final int unstickingDelayTime;
	
	public FailureConsequences(final Properties prop) {
		cargoDestroyAfterFailureChance = getFloatParam(prop, Param.cargoDestroyChance);
		hazardousCargoGenerationChance = getFloatParam(prop, Param.hazardousCargoChance);
		cargoSpillAfterFailureChance = getFloatParam(prop, Param.cargoSpillChance);
		postFailureDamageChance = getFloatParam(prop, Param.chanceOfPostFailureDamage);
		randomDamageChance = getFloatParam(prop, Param.chanceOfRandomDamage);
		secureAreaDamageChance = getFloatParam(prop, Param.chanceOfDamageSecuringAnArea);
		idleHealingTime = getIntParam(prop, Param.idleHealTime);
		stuckChance = getFloatParam(prop, Param.stuckChance);
		unstickingDelayTime = getIntParam(prop, Param.unstuckTime);
	}
	
	public float getSecureAreaDamageChance() {
		return secureAreaDamageChance;
	}

	public float getCargoDestroyAfterFailureChance() {
		return cargoDestroyAfterFailureChance;
	}

	public float getHazardousCargoGenerationChance() {
		return hazardousCargoGenerationChance;
	}

	public float getCargoSpillAfterFailureChance() {
		return cargoSpillAfterFailureChance;
	}

	public float getPostFailureDamageChance() {
		return postFailureDamageChance;
	}
	
	public float getRandomDamageChance() {
		return randomDamageChance;
	}

	public int getIdleHealingTime() {
		return idleHealingTime;
	}

	public int getUnstickingDelayTime() {
		return unstickingDelayTime;
	}
}
