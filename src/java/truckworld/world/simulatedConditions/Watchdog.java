package truckworld.world.simulatedConditions;

import static common.UniversalConstants.SIM_LOGGER_LEVEL;
import static truckworld.env.Cargoworld.INACTIVITY_TIMEOUT_PERIOD;
import static truckworld.env.Cargoworld.SIM_STEP_TIME;
import static truckworld.env.EnvironmentConstants.PROPERTY_CARGO_DEMAND_GEN_CAP;
import static truckworld.env.EnvironmentConstants.PROPERTY_MAX_CARGO_GEN_COUNT;
import static truckworld.env.EnvironmentConstants.PROPERTY_MIN_CARGO_GEN_COUNT;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.Vector;
import java.util.logging.Logger;

import common.StatsRecorder;
import common.UniversalConstants.RoadType;
import truckworld.env.Cargoworld;
import truckworld.env.EnvironmentConstants.ActionResult;
import truckworld.vehicle.Vehicle;
import truckworld.world.Cargo;
import truckworld.world.Cargo.Type;
import truckworld.world.interfaces.JunctionInf;
import truckworld.world.interfaces.RoadInf;

/**
 * Monitoring thread; generates a new cargo object and demand when the world is
 * idle for a set period of time
 */
public class Watchdog implements Runnable {
	private Cargoworld env;
	private Logger logger;

	private final int maxCargoGenCount, minCargoGenCount;
	private long seed;

	private String lastGenLoc = "";
	private Random random, goalRandom;
	private final long startTimeMs;
	private boolean running = false;
	private boolean resetting = false;

	/**
	 * Number of cargo demands generated, and the max cap
	 */
	private int demandsGenerated = 0, demandCap = 100;
	private final DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss:SS");
	private long start = 0;

	public Watchdog(final Cargoworld env, final Properties p) {
		super();
		this.env = env;
		this.logger = Logger.getLogger(this.getClass().getName());
		logger.setLevel(SIM_LOGGER_LEVEL);
		random = new Random(env.getSimSeed());
		goalRandom = new Random(env.getSimSeed());
		demandCap = Integer.parseInt(p.getProperty(PROPERTY_CARGO_DEMAND_GEN_CAP, "100"));
		maxCargoGenCount = Integer.parseInt(p.getProperty(PROPERTY_MAX_CARGO_GEN_COUNT, "1"));
		minCargoGenCount = Integer.parseInt(p.getProperty(PROPERTY_MIN_CARGO_GEN_COUNT, "1"));
		startTimeMs = System.currentTimeMillis();
		logger.info("Created Simulator thread object at " + startTimeMs);
	}

	@Override
	public void run() {
		running = true;
		int cycle = 0;
		long lastReqGen = 0; // time in ms
		start = System.currentTimeMillis();
		env.setCycle(cycle);// startup
		logger.info("Started action watchdog at " + startTimeMs);
		try {
			/*
			 * wait for the environment to start up and for agents to register
			 */
			while (!env.isRunning() && !env.hasAgents()) {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					logger.warning("Did we just shut down? " + e.getMessage());
				}
			}
			while (env.isRunning()) {
				logger.finest("Sim loop...");
				// check for inactivity-triggers for resetting all state
				final long timeElapsedSinceLastAction = System.currentTimeMillis() - env.getLastActionTime();
				final long timeElapsedSinceLastRequestWasGenerated = System.currentTimeMillis() - lastReqGen;

				if (env.nobodyActing() && (timeElapsedSinceLastAction > INACTIVITY_TIMEOUT_PERIOD)) {
					if ((timeElapsedSinceLastRequestWasGenerated > INACTIVITY_TIMEOUT_PERIOD)) {
						logger.severe(dateFormat.format(new Date()) + "; Elapsed time since last " + "demand is " + timeElapsedSinceLastAction
								+ " ms; preparing for generation");
						if (metCargoRequestCap()) {
							endExperiment();
						} 
						else {
							env.addDisplayString("Generating a new cargo demand - timeout");
							logger.severe("Generating a new cargo demand - timeout - last action was " + env.getLastActionLiteral() + ":" + env.getLastActionResult());
							if(demandsGenerated>0 && env.getLastActionResult().equals(ActionResult.pass) && !env.getLastActionLiteral().getFunctor().equals("consume") ) {
								logger.severe("Timeout despite successful last action (" + env.getLastActionLiteral() + ")!");
							}
							else if (!env.getLastActionResult().equals(ActionResult.pass)) {
								logger.severe("Failure; (" + env.getLastActionLiteral() + ")!");
							}
							lastReqGen = System.currentTimeMillis();
							updateCargoDemand();
						}
					}
					else if (notFinishedExperiment() && (noOutstandingRequestsInEnvironment() || noCargoExistsInEnvironment())) {
							// special case skipping delays if we're not busy, and no extant requests exist
							logger.severe("Generating a new cargo demand - no cargo requests in env");
							lastReqGen = System.currentTimeMillis();
							updateCargoDemand();
					}
				}

				env.setCycle(++cycle);

				try {
					logger.finest("Sleeping Simulator thread");
					Thread.sleep(SIM_STEP_TIME);
				} catch (InterruptedException e) {
					logger.warning("Did we just shut down? " + e.getMessage());
				}
			} // end while loop
		} catch (Exception e) {
			String stack = "";
			for (StackTraceElement s : e.getStackTrace()) {
				stack = stack + "\n" + s;
			}
			logger.severe("Exception on cycle " + cycle + "\n" + e.toString() + "\n" + stack);
		}

		logger.info("Finished thread!");
	}

	private boolean metCargoRequestCap() {
		return demandsGenerated >= demandCap;
	}

	private boolean noCargoExistsInEnvironment() {
		return !env.cargoExists() && !env.cargoLoaded();
	}

	private boolean noOutstandingRequestsInEnvironment() {
		return env.sumCargoRequests() == 0;
	}

	private boolean notFinishedExperiment() {
		return demandsGenerated < demandCap;
	}

	private void endExperiment() throws Exception {
		env.updateStats();
		env.addDisplayString("Complete: shutting down");
		logger.severe("Complete; shutting agents/env down! - Elapsed: " + calculateElapsedTimeSince(start));
		StatsRecorder.getInstance().flush();
		env.stop();
		env.kill();
	}

	/**
	 * Creates (if probability / random chance triggers) a demand for cargo to
	 * be delivered to a random junction.
	 */
	protected void updateCargoDemand() {
		if (!resetting && (demandsGenerated + 1) <= demandCap) {
			generateCargoDemandInWorld();
		}
	}

	/**
	 * Creates a cargo request / cargo object pair
	 */
	private void generateCargoDemandInWorld() {
		synchronized (env.getCargoCounts()) {
			random.setSeed(++seed);
			env.destroyAllCargo();
			env.removeAllRequests();
			env.getStepper().stepEnvironment();

			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {}

			// remove loaded cargo
			destroyAllLoadedCargo();

			// need some cargo!
			List<JunctionInf> js = new ArrayList<>();
			js.addAll(env.getWorld().getAllJunctions()); // cloning
			if (env.getWorld().getJunctionNames().contains(lastGenLoc)) {
				js.remove(env.getWorld().getJunction(lastGenLoc));
			}

			Vector<JunctionInf> remove = identifyInappropriateTravelLocations(js);
			if ((!remove.isEmpty()) && (remove.size() < js.size())) {
				logger.info("Remove list; " + remove.toString());
				js.remove(remove);
			} else if (remove.size() >= js.size()) {
				logger.severe("Remove list calls for removal of ALL junctions!");
			}

			// work out how much cargo to create
			int cargoToGen = minCargoGenCount;
			if (maxCargoGenCount > minCargoGenCount) {
				cargoToGen += goalRandom.nextInt(maxCargoGenCount - minCargoGenCount);
			}
			logger.info("Creating " + cargoToGen + " cargo objects");

			boolean done = false;
			final JunctionInf addCargo = js.remove(goalRandom.nextInt(js.size() - 1));
			js = env.getWorld().filterTravelable(js, addCargo);

			while (!done && js.size()>0) {
				if (js.size() == 0) {
					logger.severe("Unable to find accessible junction form " + addCargo.getId());
				} 
				else {
					//sort by distance
					Collections.sort(js, new Comparator<JunctionInf>(){
						@Override
						public int compare(JunctionInf o1, JunctionInf o2) {
							final double distanceO1 = o1.getCoordinate().distance(addCargo.getCoordinate());
							final double distanceO2 = o2.getCoordinate().distance(addCargo.getCoordinate());
							return (int) (distanceO2-distanceO1);
						}
						
					});
					JunctionInf addDemand = js.remove(0);
					lastGenLoc = addDemand.getId();
					env.receiveCargo(addCargo, new Cargo(nextCargoType()));
					logger.info("Generate cargo demand/object - " + addDemand.getId() + "," + addCargo.getId());

					js.remove(addDemand);
					// random additions of cargo, if we're creating>1
					if (cargoToGen > 1 & !js.isEmpty()) {
						cargoToGen -= 1;
						addCargo(js, cargoToGen, addDemand);
					}

					env.needsCargo(addDemand);
					env.setGenDemandsSlice(demandsGenerated + 1);
					env.setGenDemands(env.getGenDemands() + 1);
					demandsGenerated++;
					done = true;
				}
			} // find loop
		} // end of synch block
	}

	private Type nextCargoType() {
		return (goalRandom.nextFloat() < env.getConsequences().getHazardousCargoGenerationChance()) ? Cargo.Type.Hazardous : Cargo.Type.Safe;
	}

	private void destroyAllLoadedCargo() {
		for (Vehicle v : env.getVehicles().values()) {
			if (v.getCargo() != null) {
				String id = v.getCargo().getName();
				env.destroyCargo(v, id);
				logger.info("Destroyed loaded cargo for " + v.getId() + ":" + id);
			}
		}
	}

	private void addCargo(final List<JunctionInf> possibleJunctions, int numberToCreate, final JunctionInf exclude) {
		for (int i = 0; i < numberToCreate && !possibleJunctions.isEmpty(); i++) {
			final JunctionInf a = possibleJunctions.remove(0);
			// add cargo... check type
			env.receiveCargo(a, new Cargo(nextCargoType()));
			logger.info("Generated additional cargo;" + a.getId());
		}
	}

	private Vector<JunctionInf> identifyInappropriateTravelLocations(final List<JunctionInf> js) {
		final Vector<JunctionInf> remove = new Vector<>();
		for (final JunctionInf j : js) {
			final Collection<String> rs = j.getRoads();
			int floodCount = 0;
			int slipperyCount = 0;
			for (final String rid : rs) {
				final RoadInf r = env.getWorld().getRoad(rid);
				if (r.isFlooded()) {
					floodCount++;
				}
				else if (r.isSlippery() & r.getRoadType().equals(RoadType.mud)) {
					slipperyCount++;
				}
			}
			if ((floodCount + slipperyCount) == rs.size() && (remove.size() < (js.size() - 2))) {
				remove.add(j);
			}
		}
		return remove;
	}

	public boolean isRunning() {
		return running;
	}

	public final static String calculateElapsedTimeSince(final long start) {
		final long runTime = System.currentTimeMillis() - start;
		final long second = (runTime / 1000) % 60;
		final long minute = (runTime / (1000 * 60)) % 60;
		final long hour = (runTime / (1000 * 60 * 60)) % 24;
		return String.format("%02d:%02d:%02d:%d", hour, minute, second, runTime);
	}
}
