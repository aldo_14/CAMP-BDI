package truckworld.world;

import java.awt.geom.Point2D;

import truckworld.world.WorldConstants.SettlementType;
import truckworld.world.interfaces.SettlementInf;

public class Settlement extends Junction implements SettlementInf {

	private final SettlementType type;
	private final String name;
	private boolean airport, repair;
	
	public Settlement	(final String id, double x, final double y, final int elevation, final SettlementType type, final boolean airport, final boolean repair) {
		super(id, x, y, elevation);
		this.type = type;
		this.name = id;
		this.airport = airport;
		this.repair = repair;
	}
	
	public Settlement(final String id, final Point2D cellCentre, final int elev, final SettlementType type,	final boolean airport, final boolean repair) {
		this(id, cellCentre.getX(), cellCentre.getY(), elev, type, airport, repair);
	}

	@Override
	public String getName() 	{	return name;	}

	@Override
	public SettlementType getType()	 	{	return type;	}

	@Override
	public String toString()	{
		String prepped = type.name();
		prepped = prepped.substring(0, 1).toUpperCase() + prepped.substring(1).toLowerCase();
		return prepped + " of " + name + ", located at " + this.getId() + 
				":(" + this.getCoordinate().getX() + "," + this.getCoordinate().getY() + ")";
	}

	@Override
	public boolean hasAirport() 				{	return airport;		}
	@Override
	public boolean hasRepairStation() 			{	return repair;		}
	@Override
	public void setAirport(boolean ap) 			{	this.airport = ap;	}
	@Override
	public void setRepairStation(boolean rs) 	{	this.repair = rs;	}
	
}
