/**
 * 
 */
package truckworld.world;

/**
 * @author Alan White
 *
 * Defines fixed constants for the sim
 */
public class WorldConstants {
	
	/**
	 * Used in geography creation
	 */
	public enum TerrainType {water, land};

	/**
	 * Settlement types enum...
	 */
	public enum SettlementType{city, village};
	
	
	/**
	 * Number of cells in the generated world, x and y count.  The maximum
	 * number of junctions is defined as X * Y
	 */
	public static final String 	PROPERTY_CELL_COUNT_X = "CellCountX",
								PROPERTY_CELL_COUNT_Y = "CellCountY";
	
	/**
	 * Defines the  number of junctions in the world grid
	 */
	public static final String 	PROPERTY_MIN_JUNCTIONS = "MinJunctions";
	
	public static final String 	PROPERTY_MAX_JUNCTIONS = "MaxJunctions";
	
	/**
	 * The most number of roads / neighbours that a junction may have
	 */
	public static final String 	PROPERTY_MAX_ROADS_PER_JUNCTION = "MaxRoadsPerJunction";
	
	/**
	 * Fixed dimension(s) of the square world cells, used for distance
	 * calculations.  All cells are equal size.
	 */
	public static final String 	PROPERTY_CELL_SIZE = "CellSize";
	
	/**
	 * For a boolean; if true, the generation algorithm allows for roads to cross over
	 * each other.
	 */
	public static final String 	PROPERTY_FLYOVER_ALLOWED = "FlyoverAllowed";
	
	/**
	 * For validity checking; each time a connection is added, (if true), the algorithm
	 * will check that each junction can be reached from each other cell.  Otherwise, 
	 * operates on the assumption that they will.  
	 * 
	 * NOTE: Likely to have high computational speed cost
	 */
	public static final String	PROPERTY_STRONG_VALIDATION = "StrongValidation";
	
	/**
	 * Chance of a road being tarmac.  If not tarmac, is mud.  Chance of mud is
	 * 1-this; if add road more types, will need to modify accordingly
	 */
	public static final String PROPERTY_TARMAC_CHANCE = "TarmacRoadChance";
	
	/**
	 * Fraction of junctions that are cities - 0..100%
	 */
	public static final String 	PROPERTY_CITY_MAX_NUM = "MaxCities",
								PROPERTY_CITY_MIN_NUM = "MinCities";

	/**
	 * Fraction of junctions that are villages - 0...100%
	 * 
	 * Villages + cities <= 100 OR cities = 100-villages
	 */
	public static final String 	PROPERTY_VILLAGE_MAX_NUM = "MaxVillages",
								PROPERTY_VILLAGE_MIN_NUM = "MinVillages";
	
	/**
	 * Objects - airports, seaports, etc
	 */
	public static final String 	PROPERTY_MAX_AIRPORT	=	"MaxAirports",
								PROPERTY_MIN_AIRPORT	=	"MinAirports",
								PROPERTY_MAX_REPAIR		=	"MaxRepairStations",
								PROPERTY_MIN_REPAIR		=	"MinRepairStations";
	
	/**
	 * Maximum elevation for a junction
	 */
	public static final String 	PROPERTY_ELEVATION_MAX = "MaxElevation";
}
