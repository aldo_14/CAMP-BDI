/**
 * 
 */
package truckworld.world.interfaces;

import truckworld.world.WorldConstants;

/**
 * @author Alan White
 *</br>
 *A city is any inhabited settlement.  Settlements are always connected to at least
 *one road (part of the road network); they must be accessible to ground traffic.
 *Ergo, they extend road Junctions.
 *</br>
 *Cities are critical parts of the Pacifica scenario; they act as both evacuee
 *gathering points, and as evacuation sites (i.e. field hospital locations)
 *</br>
 *Settlements have a 'Type' property - 'city' or 'village'.  These are abstract representations
 *of population and can be employed as prioritisation mechanisms, etc.
 *@see pacifica.geo.inf.JunctionInf
 */
public interface SettlementInf extends JunctionInf{
	/**
	 * Returns a reference name for this city
	 * @return
	 */
	public String getName();
	
	/**
	 * Get type - Settlement or City
	 * @return
	 */
	public WorldConstants.SettlementType getType();
	
	public void setAirport(boolean ap);
	public boolean hasAirport();
	
	public void setRepairStation(boolean rs);
	public boolean hasRepairStation();
}
