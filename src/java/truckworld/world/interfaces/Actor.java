/**
 * 
 */
package truckworld.world.interfaces;

import jason.asSyntax.Literal;

import java.util.logging.Logger;

import common.UniversalConstants;
import common.UniversalConstants.EntityType;
import common.UniversalConstants.WorldAction;


/**
 * @author Alan White
 * Generic interface for any task-performing entity in the environment
 */
public abstract class Actor implements ObjectInf {
	/**
	 * Logger object
	 */
	protected Logger logger;
	
	/**
	 * Unique identifier; required by ObjectInf
	 */
	private final String id;
	
	/**
	 * Entity type
	 */
	private EntityType type;
	
	private Literal currentAction;
	
	/**
	 * Generic constructor; just takes the id and type
	 */
	public Actor(String id, EntityType type){
		logger = Logger.getLogger(this.getClass().getSimpleName() + ":" + id);
		logger.setLevel(UniversalConstants.ACTOR_LOGGER_LEVEL);
		this.id = id;
		this.type = type;
		this.currentAction = null;
	}
	
	/** 
	 * Set the UUID; left as empty here, it is expected this is set in a/the constructor
	 */
	public void setId(String id){}
	
	/**
	 * Return UUID
	 */
	public final String getId() {return id;}
	
	/**
	 * Returns the type of this actor, based on the EntityType enum
	 * @return
	 * @see common.UniversalConstants.EntityType
	 */
	public final EntityType getType()	{return type;}
	
	/**
	 * Confirms that this type of actor can perform the specified action <i>type</i>.
	 * Does not assess a specific action instance.
	 * @return boolean if can do type of action...
	 */
	public abstract boolean isCapable(WorldAction action);

	/**
	 * Set acting boolean to true or false
	 * @see isActing()
	 */
	public void setActing(Literal action){
		if(action!=null)	{	currentAction = action.copy();	}
		else				{	currentAction = null;			}
	}
	
	public Literal getAction()	{return currentAction;	}
	
	/**
	 * Queries whether the entity is current acting, i.e. is busy
	 * @return boolean
	 */
	public boolean isActing()	{	return currentAction!=null;	}
	
	
}
