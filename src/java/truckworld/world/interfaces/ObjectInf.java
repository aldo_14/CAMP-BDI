/**
 * 
 */
package truckworld.world.interfaces;

/**
 * @author Alan White
 *
 * Generic interface for all objects.  Used to force ID-setting methods.
 */
public interface ObjectInf {
	public void setId(String ID);
	public String getId();
}
