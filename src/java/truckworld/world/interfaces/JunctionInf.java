/**
 * 
 */
package truckworld.world.interfaces;

import java.awt.geom.Point2D;
import java.util.Collection;
import java.util.Map;

import truckworld.exceptions.ConnectionExistsException;



/**
 * @author Alan White
 * A road junction is a point connected to one or more roads.
 * It is expected the junction ID is formed from the IDs of connected roads.
 */
public interface JunctionInf extends ObjectInf, Cloneable	{

	/**
	 * Returns UUID.  
	 */
	public String getId();
	
	/**
	 * Returns the number of neighbours.
	 * @return
	 */
	public int getNumNeighbours();
	
	/**
	 * Returns all junctions connected (by road) to this junction
	 * @return
	 */
	public Map<String, JunctionInf> getNeighbours();
	
	/**
	 * Returns connected road, indexed by neighbour JunctionInf Object
	 * @param JunctionInf neighbour
	 * @return Roadinf object
	 */
	public String getConnection(JunctionInf neighbour);
	
	/**
	 * Return all roads from this junction
	 * @return
	 */
	public Collection<String> getRoads();
	
	/**
	 * Connect a road to this junction; throws an exception if a road covering the same
	 * route already exists.  Used by connectee.
	 */
	public void connectRoad(RoadInf connectee) throws ConnectionExistsException;
	
	/**
	 * Returns the xy co-ordinate location (Centre) of this city.  Useful for drawing,
	 * distance calculation, etc.
	 * @return
	 */
	public Point2D getCoordinate();
	
	/**
	 * Checks if a dangerZone is present here
	 * @return true if dangerous
	 */
	public boolean isDangerous();
	
	/**
	 * Set whether dangerous
	 */
	public void setDangerous(boolean bool);

	/**
	 * Elevation from sea level
	 */
	public int getElevation();
	
	/**
	 * From cloneable!
	 * @return
	 */
	public Object clone()  throws CloneNotSupportedException;
}
