/**
 * 
 */
package truckworld.world.interfaces;

import java.awt.geom.Point2D;

import common.UniversalConstants.RoadCondition;
import common.UniversalConstants.RoadType;


/**
 * @author Alan White
 * A road is simply a connection between two junction points.  Roads have
 * various properties; length, type (relating to which traffic can use them), 
 * and 'health' variables.  The latter is relevant to where roads are rendered 
 * impassible, such as through flooding or volcanic eruption.
 * </br>
 * It is presumed that all roads are bi-directional.
 * </br>
 * NB: some interfaces are intended for future expansion
 * </br>
 */
public interface RoadInf extends ObjectInf, Cloneable {
	
	/**
	 * Returns the junctions at either end of this road, in no particular order
	 * @return array of 2 JunctionInf objects
	 */
	public JunctionInf[] getEnds();

	/**
	 * Returns the ids of the ends, in same order as getEnds.  Provided as a convenience method
	 * @return
	 */
	public String[] getEndIds();

	/**
	 * Returns the length of this road; i.e. the distance in a straight line from
	 * one end to the other.  Used for working out traversal time and route 
	 * generation.
	 * @return 
	 */
	public float getLength();
	
	/**
	 * Set whether road has been blocked
	 * @param blocked
	 */
	public void setBlocked(boolean blocked);
	
	/**
	 * Checks if the road is passable - i.e. if it is blocked by a landslide, etc
	 * @return boolean true if road is NOT passable
	 */
	public boolean isBlocked();
	

	/**
	 * Sets the road to toxic, i.e. due to hazard spill
	 * @param b
	 */
	public void setToxic(boolean b);
	
	/**
	 * If true, road is impassable due to contamination from toxic spills
	 * @return
	 */
	public boolean isToxic();
	
	/**
	 * Sets the road condition - dry, slippery or flooded
	 * @param condition
	 */
	public void setCondition(RoadCondition condition);
	
	/**
	 * Checks if the road is slippery - i.e. may be too hazardous for certain vehicles
	 */
	public boolean isSlippery();

	/**
	 * Checks and returns whether the road is flooded
	 * @return boolean true if flooded
	 */
	public boolean isFlooded();
	
	/**
	 * Returns the road type - i.e. TARMAC, MUD
	 * @see pacifica.constants
	 */
	public RoadType getRoadType();

	/**
	 * @return current road conditions
	 */
	public RoadCondition getConditions();
	
	/**
	 * From cloneable!
	 * @return
	 */
	public Object clone() throws CloneNotSupportedException;

	/**
	 * Midpoint between the junction locations
	 * @return
	 */
	public Point2D getCentre();
}
