/**
 * 
 */
package truckworld.world;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Stack;
import java.util.Vector;
import java.util.logging.Logger;

import common.UniversalConstants;
import common.UniversalConstants.RoadCondition;
import common.UniversalConstants.RoadType;
import common.Utils;
import truckworld.exceptions.ConnectionExistsException;
import truckworld.exceptions.WorldGenException;
import truckworld.vehicle.Vehicle;
import truckworld.vehicle.type.Truck;
import truckworld.world.WorldConstants.SettlementType;
import truckworld.world.WorldConstants.TerrainType;
import truckworld.world.interfaces.JunctionInf;
import truckworld.world.interfaces.RoadInf;
import truckworld.world.interfaces.SettlementInf;

/**
 * @author Alan White
 *
 * The collection of nodes, objects and roads that represents the 
 * traversable world.
 */
public class World {
	private Logger logger;
	private final Random random;

	/** World cell size */
	private int worldCellX, worldCellY;

	/** Individual dimensions of cells */
	private int cellSize;

	/**
	 * Geography...
	 */
	private TerrainType[][] cell;
	
	/** total number of world junctions */
	private int minJunctions, maxJunctions;

	/**
	 * Maximum number of roads per junction; must be less than the 
	 * junction count-1
	 */
	private int maxRoadsPerJunction;

	/** True if roads can cross over each other */
	private boolean flyoverAllowed;

	/** True if due full connectivity check each time a road is added */
	private boolean strongValidation;

	/** defines chance of a tarmac road */
	private float tarmacChance;

	/**
	 * Junctions mapped to IDs
	 */
	private Map<String, JunctionInf> junctions;
	
	/**
	 * Settlements; subset of junctions
	 */
	private Map<String, SettlementInf> settlements;

	/**
	 * All the roads in the world
	 */
	private Map<String, RoadInf> roads;
	
	private float maxProportionCities, minProportionCities, maxProportionVillages, minProportionVillages;
	private int maxAirports, minAirports, maxRepair, minRepair, maxElevation;
	
	/**
	 * Creates an 'empty' world into which junctions etc can be added
	 */
	public World(){
		logger = Logger.getLogger(this.getClass().getSimpleName());
		logger.setLevel(UniversalConstants.ENV_LOGGER_LEVEL);
		logger.info("New blank world created");
		random = new Random(System.currentTimeMillis()); //Note; this should not be needed...
		junctions = new HashMap<String, JunctionInf>();
		settlements = new HashMap<String, SettlementInf>();
		roads = new HashMap<String, RoadInf>();
	}

	/**
	 * New environment... auto-generates road layout et al
	 * @param p Properties used to define params
	 * @throws WorldGenException
	 * @throws ConnectionExistsException 
	 */
	public World(Properties p)	throws WorldGenException, ConnectionExistsException {
		logger = Logger.getLogger(this.getClass().getSimpleName());
		logger.setLevel(UniversalConstants.ENV_LOGGER_LEVEL);

		long seed = Long.parseLong(p.getProperty(UniversalConstants.PROPERTY_SEED, 
				new Long(/*1334324912887l*/System.currentTimeMillis()).toString()));

		random = new Random(seed);

		logger.info("Random number seed is " + seed);

		//get properties
		worldCellX 			= Integer.parseInt(p.getProperty(WorldConstants.PROPERTY_CELL_COUNT_X, "15"));
		worldCellY 			= Integer.parseInt(p.getProperty(WorldConstants.PROPERTY_CELL_COUNT_Y, "15"));
		cellSize 			= Integer.parseInt(p.getProperty(WorldConstants.PROPERTY_CELL_SIZE, "10"));
		minJunctions 		= Integer.parseInt(p.getProperty(WorldConstants.PROPERTY_MIN_JUNCTIONS, "10"));
		maxJunctions 		= Integer.parseInt(p.getProperty(WorldConstants.PROPERTY_MAX_JUNCTIONS, "14"));
		maxRoadsPerJunction	= Integer.parseInt(p.getProperty(WorldConstants.PROPERTY_MAX_ROADS_PER_JUNCTION, "6"));
		flyoverAllowed 		= Boolean.parseBoolean(p.getProperty(WorldConstants.PROPERTY_FLYOVER_ALLOWED, "false"));
		strongValidation 	= Boolean.parseBoolean(p.getProperty(WorldConstants.PROPERTY_STRONG_VALIDATION, "true"));
		tarmacChance 		= Float.parseFloat(p.getProperty(WorldConstants.PROPERTY_TARMAC_CHANCE, "0.75"));
		maxProportionCities 	= Float.parseFloat(p.getProperty(WorldConstants.PROPERTY_CITY_MAX_NUM, "0.4"));
		minProportionCities 	= Float.parseFloat(p.getProperty(WorldConstants.PROPERTY_CITY_MIN_NUM, "0.2"));
		maxProportionVillages 	= Float.parseFloat(p.getProperty(WorldConstants.PROPERTY_VILLAGE_MAX_NUM, "0.5"));
		minProportionVillages 	= Float.parseFloat(p.getProperty(WorldConstants.PROPERTY_VILLAGE_MIN_NUM, "0.3"));
		
		//validity check; if min > max, then max=min/min=max
		if(minProportionVillages > 1)						{	minProportionVillages = 1;						}
		if(maxProportionVillages > 1)						{	maxProportionVillages = 1;						}
		if(minProportionCities > 1)							{	minProportionCities = 1;						}
		if(maxProportionCities > 1)							{	maxProportionCities = 1;						}
		if(maxProportionVillages < minProportionVillages)	{	maxProportionVillages = minProportionVillages;	}
		if(maxProportionCities < minProportionCities)		{	maxProportionCities = minProportionCities;		}
		

		maxAirports 	= Integer.parseInt(p.getProperty(WorldConstants.PROPERTY_MAX_AIRPORT, "0"));
		minAirports 	= Integer.parseInt(p.getProperty(WorldConstants.PROPERTY_MIN_AIRPORT, "0"));
		minRepair	 	= Integer.parseInt(p.getProperty(WorldConstants.PROPERTY_MIN_REPAIR, "0"));
		maxRepair	 	= Integer.parseInt(p.getProperty(WorldConstants.PROPERTY_MAX_REPAIR, "0"));
		
		maxElevation	=	Integer.parseInt(p.getProperty(WorldConstants.PROPERTY_ELEVATION_MAX, "1"));
		
		junctions = new HashMap<String, JunctionInf>();
		roads = new HashMap<String, RoadInf>();
		settlements = new HashMap<String, SettlementInf>();

		generateTruckWorld();
	}

	/**
	 * Performs the generation of a truckworld environment
	 * @throws WorldGenException
	 * @throws ConnectionExistsException 
	 */
	protected void generateTruckWorld() throws WorldGenException, ConnectionExistsException	{
		//first, set out the grid
		Vector<Point2D> openCells = new Vector<Point2D>();

		cell = new WorldConstants.TerrainType[worldCellX][worldCellY];
		
		//setting up a list of available cells
		for(int i=0; i<worldCellX;i++)	{
			for(int j=0; j<worldCellY;j++)	{
				//border zone
				if(i==0 || i==worldCellX-1 || j==0 || j==worldCellX-1){
					cell[i][j] = WorldConstants.TerrainType.water;
				}
				else	{
					cell[i][j] = WorldConstants.TerrainType.land;
				}
			}
		}
		
		//setup open cells (suitable for cities/towns)
		for(int i=0; i<worldCellX;i++)	{
			for(int j=0; j<worldCellY;j++)	{
				if(cell[i][j].equals(WorldConstants.TerrainType.land))	{
					openCells.add(new Point2D.Double(i,j));
				}
			}
		}
		
		logger.finest("Setup world of " + openCells.size() + " cells");

		//how many junctions?
		if(minJunctions>=openCells.size()){
			throw new WorldGenException("MinJunctions is too high!;" + minJunctions);
		}

		int junctionCount;
		if(maxJunctions<minJunctions)	{	junctionCount = maxJunctions;	}
		else{
			junctionCount = (int) (random.nextDouble()*(maxJunctions-minJunctions));
			junctionCount += minJunctions;
		}

		logger.finest("Beginning to build world; " + junctionCount + " junctions to make");

		//how many cities / towns / villages?
		int minCCount 	= (int) (minProportionCities * junctionCount);
		int maxCCount 	= (int) (maxProportionCities * junctionCount);
		int minVCount 	= (int) (minProportionVillages * junctionCount);
		int maxVCount 	= (int) (maxProportionVillages * junctionCount);
		
		//work out villages first
		int vCount = (int) (minVCount + (random.nextFloat()*(maxVCount-minVCount)));
		int cCount = (int) (minCCount + (random.nextFloat()*(maxCCount-minCCount)));
		//check for max-exceeds...
		if(	(vCount + cCount) > junctionCount){	cCount = junctionCount-vCount;	}
		
		logger.finer("We will have " + vCount + " villages and " + cCount + " cities");
		
		Vector<Point2D> selectedCells = new Vector<Point2D>(); //i.e. where junctions are
		for(int i=0; i<junctionCount; i++)	{
			Point2D selected = 
					openCells.remove((int)(random.nextDouble()*openCells.size()));
			selectedCells.add(selected);
			logger.finest("Setting junction - " + selected);
		}

		int idNum = 0;
		while(!selectedCells.isEmpty()){
			Point2D p = selectedCells.remove((int)(random.nextFloat()*(selectedCells.size()-1)));
			idNum++;
			String jId = "J" + idNum;
			int elevation = random.nextInt(maxElevation);
			
			//build a city or village?
			if(vCount > 0 || cCount > 0){
				//city or village?
				boolean buildCity = (random.nextFloat()*cCount) > (random.nextFloat()*vCount);
				SettlementType type;
				if(buildCity)	{	type = WorldConstants.SettlementType.city;	--cCount; 	}
				else			{	type = SettlementType.village;				--vCount;	}
				/*
				 *jitter factor; i.e. just to decentralize junctions wrt cell centres.  Stops having
				 *such obviously cell-based map structures. 
				 */
				int jitterMax = (cellSize/2);
				if(jitterMax <1)	{	jitterMax = 1;	}//error handling case
				int jitterX = random.nextInt(jitterMax);
				if(random.nextBoolean())	{	jitterX = -jitterX;	}
				int jitterY = random.nextInt(jitterMax);
				if(random.nextBoolean())	{	jitterY = -jitterY;	}
				Point2D cc = getCellCentre((int)p.getX(), (int)p.getY());
				cc.setLocation(cc.getX() + jitterX, cc.getY() + jitterY);
				Settlement s = new Settlement(jId, cc, elevation, type, false, false);
				junctions.put(s.getId(), s);
				settlements.put(s.getId(), s);
				logger.finest("Created settlement - " + s.toString());
			}
			else	{
				JunctionInf j = new Junction(jId, getCellCentre((int)p.getX(), (int)p.getY()), elevation);
				junctions.put(j.getId(), j);
				logger.finest("Created junction - " + j.toString());
			}
		}

		
		//check values...
		if(maxRepair<minRepair){
			int tem = maxRepair;
			maxRepair = minRepair;
			minRepair = tem;
		}
		
		//create city/settlement airports, seaports, fuel depots and repair stations		
		//airport
		setupAirports();
		
		//repair
		setupRepairStations();
		
		/*
		 * At this point, all cities should be set up!
		 */
		
		//now have junctions!  Next up, create connections
		for(String jId: junctions.keySet()){
			JunctionInf current = junctions.get(jId);
			int rCount = 2 + ((int)(random.nextDouble()*(maxRoadsPerJunction-2)));
			logger.finest("Adding " + rCount + " roads to " + current);

			//add roads to this junction
			addRoads(current, rCount);
		}//for jId

		//if boolean set, do a connectivity validation
		if(strongValidation){
			if(!enoughSettlementItems()){
				throw new WorldGenException("Failed strong validation!: problem with generated settlements");
			}
			
			if(!flyoverAllowed && intersectionsOccur()){
				throw new WorldGenException("Failed strong validation!: roads cross");
			}

			if(!isFullyConnected()){
				throw new WorldGenException("Failed strong validation!: not all junctions accessible");
			}
		}
	}

	/**
	 * 
	 */
	private void setupAirports() {
		if(maxAirports > 0 && minAirports > 0)	{
			if(maxAirports > this.settlements.keySet().size())	{
				maxAirports = this.settlements.keySet().size();
			}
			
			if(maxAirports<minAirports){
				int tem = maxAirports;
				maxAirports = minAirports;
				minAirports = tem;
			}
			
			//calculate how many to create...
			int toCreate = (int) (minAirports + ((random.nextFloat()*(maxAirports-minAirports))));
			//assign to random settlement...
			Vector<String> potentialIds = new Vector<String>();
			potentialIds.addAll(settlements.keySet());
			if(toCreate > potentialIds.size()) { toCreate = potentialIds.size(); }
			
			//creation...
			while(!potentialIds.isEmpty() && toCreate>0 && potentialIds.size()>=toCreate){
				String randId = potentialIds.remove((int)(random.nextFloat()*(potentialIds.size()-1)));
				this.settlements.get(randId).setAirport(true);
				toCreate--;
			}
			
		}
	}

	/**
	 * 
	 */
	private void setupRepairStations() {
		if(maxRepair > 0 && minRepair > 0)	{
			if(maxRepair > this.settlements.keySet().size())	{
				maxRepair = this.settlements.keySet().size();
			}
			
			if(maxRepair<minRepair){
				int tem = maxRepair;
				maxRepair = minRepair;
				minRepair = tem;
			}
			
			//calculate how many to create...
			int toCreate = (int) (minRepair + ((random.nextFloat()*(maxRepair-minRepair))));
			//assign to random settlement...
			Vector<String> potentialIds = new Vector<String>();
			potentialIds.addAll(settlements.keySet());
			if(toCreate > potentialIds.size()) { toCreate = potentialIds.size(); }
			
			//creation...
			while(!potentialIds.isEmpty() && toCreate>0 && potentialIds.size()>=toCreate){
				String randId = potentialIds.remove((int)(random.nextFloat()*(potentialIds.size()-1)));
				this.settlements.get(randId).setRepairStation(true);
				toCreate--;
			}
			
		}
	}

	/**
	 * Adds the specified number of roads-to-neighbours to the named junction
	 * @param current
	 * @param rCount
	 * @throws ConnectionExistsException 
	 */
	private void addRoads(final JunctionInf current, int rCount) throws ConnectionExistsException {
		Vector<String> possibleNeighbours = new Vector<String>();
		possibleNeighbours.addAll(junctions.keySet());
		//remove this junction from consideration
		possibleNeighbours.remove(current.getId()); 

		//remove existing neighbours from consideration
		logger.finest("Pre pn; " + possibleNeighbours);
		logger.finest("remove from pn; " + current.getNeighbours().keySet());

		for(String key: current.getNeighbours().keySet()){
			possibleNeighbours.remove(key);
		}

		//possibleNeighbours.remove(current.getNeighbours().keySet());
		logger.finest("Post pn; " + possibleNeighbours);

		//sort remaining neighbours to bias by distance
		Comparator<String> c = new Comparator<String>(){
			@Override
			public int compare(String arg0, String arg1) {
				JunctionInf j0 = junctions.get(arg0);
				JunctionInf j1 = junctions.get(arg1);

				double d1 = current.getCoordinate().distance(j0.getCoordinate());
				double d2 = current.getCoordinate().distance(j1.getCoordinate());

				//if first arg is less than the second, return -ve
				//if equal, return zero
				//if first arg is greater than, return +ve
				return ((d1==d2)?0 :(d1>d2)?1 :-1);
			}

		};//end comparator

		Collections.sort(possibleNeighbours, c);

		//for debug...
		for(String pnR: possibleNeighbours){
			Point2D dest = this.junctions.get(pnR).getCoordinate();
			logger.finest(	"Sorted ordering; " + pnR + ", distance is " + 
					current.getCoordinate().distance(dest));
		}

		rtCount:
			for(int i=0; i<rCount; i++){
				//find an appropriate neighbour...
				JunctionInf neighbour = null;
				if(!this.flyoverAllowed){
					neighbour = getNonFlyoverNeighbour(current, possibleNeighbours);
					if(neighbour!=null){
						logger.info(current + " will have a connection to " + neighbour);
						possibleNeighbours.remove(neighbour.getId());
					}
					else	{
						logger.info("Could not find non-flyover neighbour for" + current);
						i = rCount;
						break rtCount;
					}
				}
				else	{
					neighbour = this.junctions.get(possibleNeighbours.remove(0));
				}

				//road properties
				final RoadType type = (random.nextDouble() <= this.tarmacChance) ? 
						UniversalConstants.RoadType.tarmac: UniversalConstants.RoadType.mud;
				final RoadInf newRoad = new Road( type, current, neighbour, RoadCondition.dry, false);
				roads.put(newRoad.getId(), newRoad);
				logger.info("Added a new road; " + newRoad);
			}//for i
	}

	/**
	 * Given the set of possible neighbours, return a junctionInf which can be reached from current without the
	 * resultant road crossing over any preexisting road.
	 * @param current
	 * @param possibleNeighbours
	 * @return
	 */
	private JunctionInf getNonFlyoverNeighbour(final JunctionInf current, final Vector<String> possibleNeighbours) {
		//brute force; check possible road against all other roads generated
		for(int i=0; i<possibleNeighbours.size(); i++){
			JunctionInf pn = this.getJunction(possibleNeighbours.get(i));
			Line2D newRoadLine = new Line2D.Double(current.getCoordinate(), pn.getCoordinate());
			
			//for initial setup with no roads...
			if(this.roads.values().isEmpty()){	return pn;	}
			//check against all existing roads; if it doesn't intersect anything, return as ok
			boolean ok = true;
			checkAgainstRoads:
				for(RoadInf otherRoad: this.roads.values()){
					Line2D otherRoadLine = new Line2D.Double(
							otherRoad.getEnds()[0].getCoordinate(), 
							otherRoad.getEnds()[1].getCoordinate());
					if(Utils.intersects(newRoadLine, otherRoadLine))	{	
						ok = false;	
						break checkAgainstRoads;
					}
				}
			if(ok)	{//not contradicted
				return pn;
			}
		}
		
		logger.warning("No suitable neighbour found for " + current.getId() + " from " + possibleNeighbours);
		logger.info("Existing neigbours = " + current.getNeighbours());
		
		//none found; return null
		return null;
	}

	/**
	 * Validation method; checks that a route can be found from every junction to every other junction.  Intended as 
	 * a once off call after the world is generated.  Does NOT account for road state - i.e. weather condition, closed, etc,
	 * just that an 'opened' network would be fully traversable.
	 * @return boolean if fully traversable
	 */
	private boolean isFullyConnected() {
		logger.finest("Roads in world;");

		for(String r: this.roads.keySet())	{
			logger.finest(roads.get(r).toString());
		}

		long timeMs = System.currentTimeMillis();
		logger.info("Peforming connection validation;" + timeMs);
		for(String id1: junctions.keySet())	{
			for(String id2:junctions.keySet()){
				if(id1.compareTo(id2)!=0){
					logger.finest("Checking " + id1 + " to " + id2);
					Vehicle traveller = new Truck("Test"){
						@Override
						public boolean canTravelAlong(RoadInf road)	{
							return true;//!road.isBlocked();
						}
						@Override
						public float maxSpeed(RoadInf road) {
							return 1;
						}
					};

					Stack<JunctionInf> route = 
							Utils.getRoute(junctions.get(id1), junctions.get(id2), roads, traveller);

					logger.finest("Route " + route);
					boolean hasRoute = (route!=null);
					logger.finest("Validation result - " + id1 + " to " + id2 + "; " + hasRoute);
					if(!hasRoute){	return false; 	}
				}//endif
			}//for id2
		}//for id1

		logger.info(	"Performed connection validation; took " + (System.currentTimeMillis()-timeMs) + " ms");
		return true;
	}

	private boolean intersectionsOccur(){
		boolean intersects = false;
		for(RoadInf r1: this.roads.values()){
			for(RoadInf r2: this.roads.values()){
				if(!r1.equals(r2) )	{
					//NB: log all intersections, so don't immediately break...
					if(intersects(r1, r2))	{
						logger.warning(r1.getId() + " intersects " + r2.getId());
						intersects = true;
					}
				}
			}//end for r2
		}//end for r1
		return intersects;
	}

	/**
	 * Check if r1's line intersects that of r2
	 * @see intersects(Line2D, Line2D)
	 * @param r1
	 * @param r2
	 * @return
	 */
	private boolean intersects(RoadInf r1, RoadInf r2)	{
		Line2D l1 = new Line2D.Double(
				r1.getEnds()[0].getCoordinate(), r1.getEnds()[1].getCoordinate());
		Line2D l2 = new Line2D.Double(
				r2.getEnds()[0].getCoordinate(), r2.getEnds()[1].getCoordinate());
		return Utils.intersects(l1, l2);
	}
	

	/**
	 * Checks that constraints for the min/max number of cities, villages, airports,
	 * etc are met in the generated world
	 * @return
	 */
	private boolean enoughSettlementItems() {
		int cCount = 0;
		int vCount = 0;
		int airportCount = 0;
		//int seaportCount = 0;
		int repairCount = 0;
		for(SettlementInf s: this.settlements.values()){
			if(s.getType().equals(SettlementType.city))			{	cCount++;		}
			else if(s.getType().equals(SettlementType.village))	{	vCount++;		}
			if(s.hasAirport())									{	airportCount++;	}
			if(s.hasRepairStation())							{ 	repairCount++;	}
		}
		
		boolean cityCheck 		= cCount <= (maxProportionCities*junctions.size());
		boolean villageCheck 	= vCount <= (maxProportionVillages*junctions.size());
		boolean settlementCheck = cityCheck && villageCheck;
		boolean air	=	(airportCount <= this.maxAirports) && (airportCount >= this.minAirports);
		boolean rep	=	(repairCount <= this.maxRepair) && (repairCount >= this.minRepair);
		
		logger.info("Check: settlementCheck; (of " + junctions.size() + " junctions)" +
				"cities = " + cityCheck + 
				"(" + cCount + "/" + (maxProportionCities*junctions.size()) + "), " +
				"villages = " + villageCheck + 
				"(" + vCount + "/" +  (maxProportionVillages*junctions.size()) + ")");
		logger.info("Check: airportCheck = " + air + " Count=" + airportCount);
		logger.info("Check: repairCheck = " + rep + " Count=" + repairCount);
		
		return settlementCheck && air && rep;
	}


	/**
	 * Returns the centre point of a cell located in grid cX, cY.
	 * i.e. if at 0,0 with dimensions 10,10, will return 5,5
	 * @param cX
	 * @param cY
	 * @return Point2D
	 */
	private Point2D getCellCentre(int cX, int cY){
		double x = (cX * cellSize) + (cellSize/2);
		double y = (cY * cellSize) + (cellSize/2);
		return new Point2D.Double(Math.round(x), Math.round(y));
	}

	/*==============================
	 * GET / SET METHODS
	 *==============================*/

	/**
	 * Get junction by ID
	 * @param id
	 * @return
	 */
	public JunctionInf getJunction(String id){
		return this.junctions.get(id);
	}

	/**
	 * Returns ids of all junctions
	 * @return Vector<String>
	 */
	public Vector<String> getJunctionNames(){
		Vector<String> rt = new Vector<String>(junctions.keySet().size());
		rt.addAll(junctions.keySet());
		return rt;
	}

	/**
	 * Get all junctions
	 */
	public Vector<JunctionInf> getAllJunctions()	{
		Vector<JunctionInf> rt = new Vector<JunctionInf>();
		rt.addAll(junctions.values());
		return rt;
	}

	public Vector<String> getAirports()	{
		Vector<String> rt = new Vector<String>();
		for(SettlementInf s: settlements.values())	{
			if(s.hasAirport())	{	rt.add(s.getId());	}
		}
		return rt;
	}
	
	/**
	 * Get settlements by ID
	 * @param id
	 * @return
	 */
	public SettlementInf getSettlement(String id){
		return this.settlements.get(id);
	}

	/**
	 * Returns ids of all settlements
	 * @return Vector<String>
	 */
	public Vector<String> getSettlementNames(){
		Vector<String> rt = new Vector<String>(settlements.keySet().size());
		rt.addAll(settlements.keySet());
		return rt;
	}

	/**
	 * Get all settlements
	 */
	public Vector<SettlementInf> getAllSettlements()	{
		Vector<SettlementInf> rt = new Vector<SettlementInf>();
		rt.addAll(settlements.values());
		return rt;
	}
	
	public RoadInf getRoad(String id){
		return roads.get(id);
	}

	public Vector<String> getAllRoadsIds(){
		Vector<String> rt = new Vector<String>();
		rt.addAll(roads.keySet());
		return rt;
	}

	public Vector<RoadInf> getAllRoads(){
		Vector<RoadInf> rt = new Vector<RoadInf>();
		rt.addAll(roads.values());
		return rt;
	}

	/**
	 * Returns a string listing all blocked roads
	 * NB: current using lazy option - check all roads and add the ids of those blocked... might be
	 * more efficient to keep a record based upon road state change calls?s
	 * @return
	 */
	public Vector<String> getBlockedRoadIds(){
		Vector<String> rt = new Vector<String>();
		for(RoadInf r: roads.values()){
			if(r.isBlocked()) 	{	rt.add(r.getId());	}
		} 
		return rt;
	}


	public int getDimensionsX() 	{return worldCellX*cellSize;}
	public int getDimensionsY() 	{return worldCellY*cellSize;}
	public int getCellDimensions() 	{return cellSize;}

	public boolean addRoad(RoadInf r){
		if	(roads.containsKey(r.getId()))	{
			logger.severe("Trying to add an existing road! " + r);
			return false;
		}
		if	(	!junctions.containsKey(r.getEndIds()[0])
				||
				!junctions.containsKey(r.getEndIds()[1]))	{
			logger.severe("Trying to add a road between non-existant junctions! " + r);
			return false;
		}

		roads.put(r.getId(), r);
		return true;
	}

	public boolean addJunction(JunctionInf j){
		if(junctions.containsKey(j.getId()))	{
			logger.severe("Already have junction " + j);
			return false;
		}
		this.junctions.put(j.getId(), j);
		return true;
	}

	/**
	 * Test constructor...
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception	{
		long seed = System.currentTimeMillis();
		Properties p = new Properties();
		p.setProperty(UniversalConstants.PROPERTY_SEED, ""+seed);//seed);
		new World(p);
	}

	public TerrainType[][] getCell() {	return cell;		}

	public List<JunctionInf> filterTravelable(final List<JunctionInf> js, final JunctionInf addCargo) {
		final Vehicle traveller = new Truck("Test"){
			@Override
			public boolean canTravelAlong(final RoadInf road)	{
				return !road.isFlooded()  && !(road.getRoadType().equals(RoadType.mud) && road.isSlippery());
			}
			@Override
			public float maxSpeed(final RoadInf road) {
				return 1;
			}
		};

		final List<JunctionInf> unreachable = new ArrayList<>();
		for(final JunctionInf j:js) {
			if(j.getId().equals(addCargo.getId()) ) {
				unreachable.add(j);
			}
			else {
				final Stack<JunctionInf> route = Utils.getRoute(j, addCargo, roads, traveller);
				if(route==null) {
					unreachable.add(j);
				}
			}
		}
		
		js.removeAll(unreachable);
		return js;
	}

	public Vector<RoadInf> getBlockedRoads(){
		Vector<RoadInf> rt = new Vector<>();
		for(RoadInf r: roads.values()){
			if(r.isBlocked()) 	{	rt.add(r);	}
		} 
		return rt;
	}
}
