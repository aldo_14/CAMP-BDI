/**
 * 
 */
package truckworld.world;

import java.awt.geom.Point2D;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import truckworld.exceptions.ConnectionExistsException;
import truckworld.world.interfaces.JunctionInf;
import truckworld.world.interfaces.RoadInf;



/**
 * @author Alan White
 *
 * A junction is a point connected to >0 other points via roads.
 */
public class Junction implements JunctionInf {
	/**
	 * Identified
	 */
	private String id;
	
	/**
	 * All the junctions this one has a road leading to, keyed by ID
	 */
	private Map<String, JunctionInf> neighbours;

	/**
	 * Maps roads onto the junction they connect this one to
	 */
	private Map<JunctionInf, String> connections;
	
	/**
	 * 2D space location
	 */
	private final int x, y;//Point2D coordinate;

	/**
	 * Is there a danger zone present
	 */
	private boolean dangerous = false;
	
	/**
	 * Elevation from sea level (abstract units, but consider equiv to metres for simplicity)
	 */
	private final int elev;
	
	public Junction(String id, double x, double y){
		this(id, new Point2D.Double(x,y), 0);
	}
	
	public Junction(String id, Point2D p) {	this(id, p, 0);	}
	
	public Junction(String id, double x, double y, int elev){
		this(id, new Point2D.Double(x,y), elev);
	}
	
	public Junction(String id, Point2D location, int elev){
		this.id = id.toLowerCase(); //for planner
		//this.coordinate = location;
		x = (int) location.getX();
		y = (int) location.getY();
		this.elev = elev;
		neighbours = new HashMap<String, JunctionInf>();
		connections = new HashMap<JunctionInf, String>();
	}
	
	

	@Override
	public void setId(String ID) {	this.id = ID;	}

	@Override
	public String getId() {	return id;	}
	
	@Override
	public boolean equals(Object o) {
		if(o instanceof JunctionInf){
			JunctionInf j = (JunctionInf)o;
			return	(	(this.getId().compareTo(j.getId())==0) &&
						(this.getCoordinate().equals(j.getCoordinate())) &&
						(this.getNumNeighbours() == j.getNumNeighbours()) &&
						(this.getNeighbours().equals(j.getNeighbours()))
					);
		}
		else return false;
	}

	@Override
	public int getNumNeighbours() {	return neighbours.size();	}

	@Override
	public Map<String, JunctionInf> getNeighbours() {	return neighbours;	}

	@Override
	public String getConnection(JunctionInf neighbour) {
		return connections.get(neighbour);
	}

	@Override
	public void connectRoad(RoadInf connector)
			throws ConnectionExistsException {
		JunctionInf connected = connector.getEnds()[0];
		if(connected.equals(this))	{
			connected = connector.getEnds()[1];
		}
		
		if(this.connections.containsKey(connected))	{
			throw new ConnectionExistsException(
					this.getId(), connections.get(connected), connector);
		}
		
		//else...
		this.connections.put(connected, connector.getId());
		this.neighbours.put(connected.getId(), connected);
	}

	@Override
	public final Point2D getCoordinate() {	//return this.coordinate;	
		return new Point2D.Double(x, y); //in case of possible reference-related modification?
	}
	
	public String toString(){
		return getId() + "@(" + getCoordinate();
	}

	public JunctionInf clone()  throws CloneNotSupportedException	{
		return (JunctionInf)super.clone();
	}

	@Override
	public boolean isDangerous() {	return dangerous ;	}

	@Override
	public void setDangerous(boolean bool) {	dangerous = bool;	}

	@Override
	public Collection<String> getRoads() {	return connections.values();	}
	
	@Override
	public int getElevation()	{	return elev;	}
}
