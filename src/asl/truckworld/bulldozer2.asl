/*
This ASL represents a Bulldozer agent, which can move down and clear blocked roads (so long as they
aren't flooded)
*/
{ include("bhvr\bulldozer.asl") }  //i.e. extend truck

/* Initial goals */
!start("j9").	