//failure case; retry unload(s).  If on a road, move to the nearest junction then drop cargo.
@defaultUnloadAtJ
-unload(AGENT, DEST, C):	atJ(AGENT, J) & not busy(AGENT)
						<- .print("Default cargo drop");
							unload(AGENT, J, C).
@defaultUnloadOnR
-unload(AGENT, DEST, C):	onR(AGENT, R) & not busy(AGENT)
						<- .print("Move and cargo drop at nearest junction...");
							common.ia.findNearestJunction(AGENT, N);
							!moveTo(AGENT, N);
							unload(AGENT, N, C).