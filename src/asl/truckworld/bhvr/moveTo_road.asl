   /*
 * This file is to hold 'standard' shared behaviour for vehicle agents
 */

 //movement
 @moveToAtJ
+!moveTo(AGENT, CLOC, CDES):	not busy(AGENT) & atJ(AGENT, CLOC) & not mortal(AGENT) & not stuck(AGENT) & not resting(AGENT) &
								location(CLOC) & location(CDES) & not dangerZone(CLOC) & not dangerZone(CDES)	
							<-	vehicle.ia.planRoute(CLOC, CDES); !doRoute(AGENT, CLOC, CDES).

@moveToOnR
+!moveTo(AGENT, CLOC, CDES):	not busy(AGENT) & onR(AGENT, CLOC) & not mortal(AGENT) & not stuck(AGENT) & not resting(AGENT) &
								connection(CLOC) & location(CDES) & not dangerZone(CDES)	
							<-	vehicle.ia.planRoute(CLOC, CDES); !doRoute(AGENT, CLOC, CDES).
/*
@movetoDoNothing //already at destination
+!moveTo(AGENT, CLOC, CDES):	atJ(AGENT, CLOC) & atJ(AGENT, CDES)
							<- .print(AGENT, " already at ", CLOC).
	 				
//debug behaviour executed when the agent is no longer stuck in a location and is not at a junction & is ME!
@unStuckMoveToClosest[atomic]
-stuck(AGENT):	onR(AGENT, O) & common.ia.isMe(AGENT)
				<- 	common.ia.findNearestJunction(AGENT, K);
					.print(AGENT, " became unstuck at ", O, ", move to ", K); 
					vehicle.ia.planRoute(O, K); 
					!doRoute(AGENT, O, K).*/	