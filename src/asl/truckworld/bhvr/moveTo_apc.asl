/*
 * Allows an APC to move into a dangerzone in order to clear it.
 */

 //movement
 @moveToAtJNOdZ
+!moveTo(AGENT, CLOC, CDES):	not busy(AGENT) & atJ(AGENT, CLOC) & not mortal(AGENT) & not stuck(AGENT) & not resting(AGENT) & 
								location(CLOC) & location(CDES)	& not dangerZone(CLOC)
							<-	vehicle.ia.planRoute(CLOC, CDES); !doRoute(AGENT, CLOC, CDES).
							
@moveToOnR
+!moveTo(AGENT, CLOC, CDES):	not busy(AGENT) & onR(AGENT, CLOC) & not mortal(AGENT) & not stuck(AGENT) & not resting(AGENT) &
								connection(CLOC) & location(CDES) 
							<-	vehicle.ia.planRoute(CLOC, CDES); !doRoute(AGENT, CLOC, CDES).