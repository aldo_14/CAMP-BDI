//failure case; retry unload(s).  If on a road, move to the nearest junction then drop cargo.
/*
 * 
 */
@defaultUnloadFlyingAtJ
-unload(AGENT, DEST, C):	atJ(AGENT, DEST) & not busy(AGENT) & flying(AGENT) & airport(DEST)
						<- .print("Default cargo drop");
							land(AGENT, DEST);
							unload(AGENT, DEST, C).
/*
 * 
 */
@defaultUnloadAtJ
-unload(AGENT, DEST, C):	atJ(AGENT, DEST) & not busy(AGENT) & not flying(AGENT) & airport(DEST)
						<- .print("Default cargo drop");
							unload(AGENT, DEST, C).

/*
 * 
 */
@defaultUnloadFlying
-unload(AGENT, DEST, C):	not atJ(AGENT, DEST) & flying(AGENT) & not busy(AGENT) & airport(DEST)
						<- .print("Move and cargo drop at nearest junction...");
							common.ia.findNearestJunction(AGENT, N);
							!moveTo(AGENT, N);
							unload(AGENT, N, C).