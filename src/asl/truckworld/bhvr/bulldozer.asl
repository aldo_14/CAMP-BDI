/*
This ASL represents a Bulldozer agent, which can move down and clear blocked roads (so long as they
aren't flooded)
*/
{ include("moveTo_road.asl")	}  
{ include("register.asl") 		}  	