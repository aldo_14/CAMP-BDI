/*
 * This file is to hold 'standard' shared behaviour for helicopter agents
 */
 //movement
 @moveFly
+!moveTo(AGENT, HELISTART, HELIDEST):	not busy(AGENT) & not flying(AGENT) & atJ(AGENT, HELISTART) & not mortal(AGENT) & 
								location(HELISTART) & location(HELIDEST) & helicopter(AGENT) & not atJ(AGENT, HELIDEST) &
								not dangerZone(HELISTART) & not dangerZone(HELIDEST) & airport(HELISTART) & airport(HELIDEST)
							<-	takeOff(AGENT, HELISTART); fly(AGENT, HELISTART, HELIDEST); land(AGENT, HELIDEST).
 @moveAlreadyFlying
+!moveTo(AGENT, HELISTART, HELIDEST):	not busy(AGENT) & flying(AGENT) & overJ(AGENT, HELISTART) & not mortal(AGENT) & 
								location(HELISTART) & location(HELIDEST) & helicopter(AGENT) & not atJ(AGENT, HELIDEST)&
								not dangerZone(HELISTART) & not dangerZone(HELIDEST) & airport(HELIDEST)
							<-	fly(AGENT, HELISTART, HELIDEST); land(AGENT, HELIDEST).
							
 @moveAlreadyFlyingThere //effectively for LANDING
+!moveTo(AGENT, HELISTART, HELIDEST):	not busy(AGENT) & flying(AGENT) & overJ(AGENT, HELISTART) & not mortal(AGENT) & 
								location(HELISTART) & location(HELIDEST) & helicopter(AGENT) & atJ(AGENT, HELIDEST)&
								not dangerZone(HELISTART) & not dangerZone(HELIDEST) & airport(HELIDEST)
							<-	land(AGENT, HELIDEST).