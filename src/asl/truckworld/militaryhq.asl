// military HQ agent - organizes hazmat and secureRoad operations

!start. 
+!start : true <- register.

/*
 * SECURING AREAS
 * 
 * Note; we transform the AGENT var mapping into a vehicle ID when continuing execution.
 */
+!secure(AGENT, SECURE):	dangerZone(SECURE)
					<- 	common.ia.findNearestApc(AG, LOC, SECURE);
						!sendToSecure(AG, LOC, SECURE).

+!sendToSecure(AGENT, LOC, SECURE): not dangerZone(SECURE) <- .print("Dangerzone absent; ", SECURE).

+!sendToSecure(AGENT, LOC, SECURE):	dangerZone(SECURE) & apc(AGENT) & atJ(AGENT, LOC) & atJ(AGENT, SECURE) & 
									not busy(AGENT) & not mortal(AGENT) //LOC==DZ...
					<- secureArea(AGENT, SECURE).
						
+!sendToSecure(AGENT, LOC, SECURE):	dangerZone(SECURE) & apc(AGENT) & atJ(AGENT, LOC) & not atJ(AGENT, SECURE) & 
									not busy(AGENT) & not mortal(AGENT) & not dangerZone(LOC)
					<- 	moveTo(AGENT, LOC, SECURE);
						secureArea(AGENT, SECURE).

+!sendToSecure(AGENT, LOC, SECURE):	dangerZone(SECURE) & apc(AGENT) & onR(AGENT, LOC) & not atJ(AGENT, SECURE) & 
									not busy(AGENT) & not mortal(AGENT) & not dangerZone(LOC) & not stuck(AGENT) &
									not resting(AGENT) & not blockedRd(LOC)
					<- 	moveTo(AGENT, LOC, SECURE);
						secureArea(AGENT, SECURE).
						
//for DZ at origin...					
+!sendToSecure(AGENT, LOC, SECURE):	dangerZone(SECURE) & apc(AGENT) & atJ(AGENT, LOC) & not atJ(AGENT, SECURE) & 
									not busy(AGENT) & not mortal(AGENT) & dangerZone(LOC)
					<- 	secureArea(AGENT,LOC);
						moveTo(AGENT, LOC, SECURE);
						secureArea(AGENT, SECURE).

+!sendToSecure(AGENT, LOC, SECURE):	dangerZone(SECURE) & apc(AGENT) & onR(AGENT, LOC) & not atJ(AGENT, SECURE) & 
									not busy(AGENT) & not mortal(AGENT) & dangerZone(LOC) & not stuck(AGENT) &
									not resting(AGENT) & not blockedRd(LOC)
					<- 	secureArea(AGENT,LOC);
						moveTo(AGENT, LOC, SECURE);
						secureArea(AGENT, SECURE).

//test only
//@unblockTest
//+blocked(R1, R2):  road(RID, R1, R2)   <- !unblock("militaryhq", RID, R1, R2).

/*
 * Unblocking roads - delegate to me, then i delegate to logistics hq...
 */
 @unblockNoOp
+!unblock(AID, RID, R1, R2): location(R1) & location(R2) & road(RID, R1, R2) & not blocked(R1, R2)  <- .print("Noop for unblock").
 
 @unblock1
 +!unblock(AID, RID, R1, R2):	location(R1) & location(R2) & road(RID, R1, R2) & blocked(R1, R2) & 
 								not dangerZone(R1) & not dangerZone(R2) & not toxic(R1, R2)
 								& tarmac(R1, R2) & not flooded(R1, R2)
 					<- unblock("logisticshq", RID, R1, R2).

@unblock2
 +!unblock(AID, RID, R1, R2):	location(R1) & location(R2) & road(RID, R1, R2) & blocked(R1, R2) & 
 								not dangerZone(R1) & not dangerZone(R2) & dry(R1, R2) &
 								not toxic(R1, R2) & mud(R1, R2)
 					<- unblock("logisticshq", RID, R1, R2).

/*
 * DECONTAMINATION PLANS
*/
 //select & dispatch an agent to clear a road
//needs conf. function based on whether agents are available or not; i.e. conf==0 if no agents can be sent
@deconNoOp
+!decontaminateRoad(SELF, RID, R1, R2): location(R1) & location(R2) & road(RID, R1, R2) & not toxic(R1, R2)  <- .print("Noop for decon").

@deconTarmac
+!decontaminateRoad(SELF, RID, R1, R2):	road(RID, R1, R2) & toxic(R1, R2) & tarmac(R1,R2) & not flooded(R1, R2) & not blocked(R1, R2)
				<- 	common.ia.findNearestHazmat(HAZMAT, O, R1); //find nearest agent to R1, set location of O
					!sendHazmat(HAZMAT, O, RID, R1, R2). //semaphore removal
			
@deconMud
+!decontaminateRoad(SELF, RID, R1, R2):	road(RID, R1, R2) & toxic(R1, R2) & mud(R1, R2) & dry(R1, R2) & not blocked(R1, R2)
				<- 	common.ia.findNearestHazmat(HAZMAT, O, R1); //find nearest agent to R1, set location of O
					!sendHazmat(HAZMAT, O, RID, R1, R2). //semaphore removal
					

//need to ID start, move to location, clear road
@hazmatSend
+!sendHazmat(AGENT, AGST, ROAD, ROADST, ROADEND):	hazmat(AGENT) & not mortal(AGENT) & not busy(AGENT) & 
													not atJ(AGENT, ROADST) & atJ(AGENT, AGST) & not mortal(AGENT) 
				<- 	moveTo(AGENT, AGST, ROADST);
					decontaminate(AGENT, ROAD, ROADST, ROADEND).

+!sendHazmat(AGENT, AGST, ROAD, ROADST, ROADEND):	hazmat(AGENT) & not mortal(AGENT) & not busy(AGENT) & not stuck(AGENT) &
													not atJ(AGENT, ROADST) & onR(AGENT, AGST) & not mortal(AGENT) 
				<- 	moveTo(AGENT, AGST, ROADST);
					decontaminate(AGENT, ROAD, ROADST, ROADEND).
					
//decontaminate(AGENT, ROAD, START, END)			
@hazmatThere
+!sendHazmat(AGENT, AGST, ROAD, ROADST, ROADEND): 	hazmat(AGENT) & not mortal(AGENT) & not busy(AGENT) & 
													atJ(AGENT, ROADST) & atJ(AGENT, AGST) & not mortal(AGENT) &
													not dangerZone(ROADST) & not dangerZone(ROADEND)
				<- decontaminate(AGENT, ROAD, ROADST, ROADEND).	

//need to ID start, move to location, clear road
@hazmatSendFromRoad
+!sendHazmat(AGENT, AGST, ROAD, ROADST, ROADEND):	hazmat(AGENT) & not mortal(AGENT) & not busy(AGENT) &
													not atJ(AGENT, ROADST) & not mortal(AGENT) & onR(AGENT, AGST) & not stuck(AGENT)
				<- 	moveTo(AGENT, AGST, ROADST);
					decontaminate(AGENT, ROAD, ROADST, ROADEND).
				
@stuckFreeUnblock
+!unstick(ME, A, R, R1, R2): 	not mortal(A) & not resting(A) & road(R, R1, R2) & 
							stuck(A) & onR(A, R) & blocked(R1, R2) & not toxic(R1, R2)
		<- unblock("logisticshq", R, R1, R2); free(A, R, R1, R2).
		
@stuckFreeDecon
+!unstick(ME, A, R, R1, R2): 	not mortal(A) & not resting(A) & road(R, R1, R2) & 
							stuck(A) & onR(A, R) & not blocked(R1, R2) & toxic(R1, R2)
		<- !decontaminateRoad("militaryhq", RID, R1, R2); free(A, R, R1, R2).