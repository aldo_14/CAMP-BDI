/*
This ASL represents a logistics HQ agent
*/

/* Initial beliefs*/

/* rules */
//legality of addding a cargo need goal
can_add_delivery_goal(J) :-
					cargoAt(CARGO, SOMEWHERE)// & not claimed(ANYAGENT, CARGO).//cargo is available
					& (truck(AGENT) | helicopter(AGENT)) & not busy(AGENT) & not mortal(AGENT) & not resting(AGENT).


/* Initial goals */
//register with the simulator as a vehicle with random start position
!start. 

/* Plans */
//triggering event: context/preconditions <- body/actions

+!start : true <- register.

//respond to cargo required events...
//transform a cargo request into a delivery goal; used to filter against root goals
+cargoNeeded(J):	can_add_delivery_goal(J)	
				<-	.print("Add delivery goal"); !!supply(J).

+cargoNeeded(J):	not cargoAt(CARGO, SOMEWHERE) & not can_add_delivery_goal(J)	<- .print("ERROR1").
+cargoNeeded(J):	not (truck(AGENT)|helicopter(AGENT)) & not can_add_delivery_goal(J)	<- .print("ERROR2").

//forwarding plans
@deconNotNecessary
+!decontaminateRoad(ID, RID, R1, R2): location(R1) & location(R2) & road(RID, R1, R2) & not toxic(R1, R2)  <- .print("Noop for deconFwd").

@deconAdvertised1
+!decontaminateRoad(ID, RID, R1, R2):	location(R1) & location(R2) & road(RID, R1, R2) & tarmac(R1,R2) & not flooded(R1, R2) & toxic(R1, R2)
									<- 	decontaminateRoad("militaryhq", RID, R1, R2).
@deconAdvertised2
+!decontaminateRoad(ID, RID, R1, R2):	location(R1) & location(R2) & road(RID, R1, R2) & mud(R1, R2) & dry(R1, R2) & toxic(R1, R2)
									<- 	decontaminateRoad("militaryhq", RID, R1, R2).
								
@unblockNotNecessary
+!unblock(AID, RID, R1, R2): location(R1) & location(R2) & road(RID, R1, R2) & not blocked(R1, R2)  <- .print("Noop for unblock").
									
@unblockAdvertisedTarmac
+!unblock(AID, RID, R1, R2):	location(R1) & location(R2) & road(RID, R1, R2) & blocked(R1, R2) & 
								not dangerZone(R1) & not dangerZone(R2) & not toxic(R1, R2)
								& tarmac(R1, R2) & not flooded(R1, R2)
							<-	common.ia.findNearestBulldozer(BDAGENT, ALOC, R1);//find nearest bulldozer to R1
								!goUnblock(BDAGENT, ALOC, RID, R1, R2). //subdivide
@unblockAdvertisedMud					
+!unblock(AID, RID, R1, R2):	location(R1) & location(R2) & road(RID, R1, R2) & blocked(R1, R2) & 
								not dangerZone(R1) & not dangerZone(R2) & not toxic(R1, R2) & dry(R1, R2) & mud(R1, R2)
							<-	common.ia.findNearestBulldozer(BDAGENT, ALOC, R1);//find nearest bulldozer to R1
								!goUnblock(BDAGENT, ALOC, RID, R1, R2). //subdivide
								
@goUbMoveToEndOk
+!goUnblock(BDAGENT, ALOC, RID, R1, R2): 	bulldozer(BDAGENT) & not busy(BDAGENT) & 
											atJ(BDAGENT, ALOC) & not atJ(BDAGENT, R1) & not atJ(BDAGENT, R2) & 
											connection(RID) & location(ALOC) & location(R1) & location(R2) & 
											blocked(R1, R2) & road(RID, R1, R2) & not dangerZone(R2) & not flooded(R1, R2)
										<- 	!moveAndSecure(BDAGENT, ALOC, R1);//move to R1 from current loc; secure LOC if required
											unblock(BDAGENT, RID, R1, R2).//unblock
@goUbMoveToEndUnsafe		
+!goUnblock(BDAGENT, ALOC, RID, R1, R2): 	bulldozer(BDAGENT) & not busy(BDAGENT) & 
											atJ(BDAGENT, ALOC) & not atJ(BDAGENT, R1) & not atJ(BDAGENT, R2) & 
											connection(RID) & location(ALOC) & location(R1) & location(R2) & 
											blocked(R1, R2) & road(RID, R1, R2) & dangerZone(R2) & not flooded(R1, R2)
										<- 	!moveAndSecure(BDAGENT, ALOC, R1);//move to R1 from current loc; secure LOC if required
											secure("militaryhq", R2); //secure other end
											unblock(BDAGENT, RID, R1, R2).//unblock			
				
//below are for plans where agent is located at the end point	
@goUbAtEnd1BothUnsafe
+!goUnblock(BDAGENT, ALOC, RID, R1, R2): 	bulldozer(BDAGENT) & not busy(BDAGENT) & 
											atJ(BDAGENT, ALOC) & atJ(BDAGENT, R1) & not atJ(BDAGENT, R2) & 
											connection(RID) & location(ALOC) & location(R1) & location(R2) & 
											blocked(R1, R2) & road(RID, R1, R2) & dangerZone(R1) & dangerZone(R2) & 
											not flooded(R1, R2)
										<- 	secure("militaryhq", R1);
											secure("militaryhq", R2);
											unblock(BDAGENT, RID, ALOC, R2).//unblock

@goUbAtEnd1End1Unsafe
+!goUnblock(BDAGENT, ALOC, RID, R1, R2): 	bulldozer(BDAGENT) & not busy(BDAGENT) & 
											atJ(BDAGENT, ALOC) & atJ(BDAGENT, R1) & not atJ(BDAGENT, R2) & 
											connection(RID) & location(ALOC) & location(R1) & location(R2) & 
											blocked(R1, R2) & road(RID, R1, R2) & dangerZone(R1) & not dangerZone(R2) & 
											not flooded(R1, R2)
										<- 	secure("militaryhq", R1);
											unblock(BDAGENT, RID, ALOC, R2).//unblock

@goUbAtEnd1End2Unsafe
+!goUnblock(BDAGENT, ALOC, RID, R1, R2): 	bulldozer(BDAGENT) & not busy(BDAGENT) & 
											atJ(BDAGENT, ALOC) & atJ(BDAGENT, R1) & not atJ(BDAGENT, R2) & 
											connection(RID) & location(ALOC) & location(R1) & location(R2) & 
											blocked(R1, R2) & road(RID, R1, R2) & not dangerZone(R1) & dangerZone(R2) & 
											not flooded(R1, R2)
										<- 	secure("militaryhq", R2);
											unblock(BDAGENT, RID, ALOC, R2).//unblock


@goUbAtEnd1BothSafe
+!goUnblock(BDAGENT, ALOC, RID, R1, R2): 	bulldozer(BDAGENT) & not busy(BDAGENT) & 
											atJ(BDAGENT, ALOC) & atJ(BDAGENT, R1) & not atJ(BDAGENT, R2) & 
											connection(RID) & location(ALOC) & location(R1) & location(R2) & 
											blocked(R1, R2) & road(RID, R1, R2) & not dangerZone(R1) & 
											not dangerZone(R2) & not flooded(R1, R2)
										<- 	secure("militaryhq", R2);
											unblock(BDAGENT, RID, ALOC, R2).//unblock
				
//flipside...
@goUbAtEnd2BothUnsafe
+!goUnblock(BDAGENT, ALOC, RID, R1, R2): 	bulldozer(BDAGENT) & not busy(BDAGENT) & 
											atJ(BDAGENT, ALOC) & not atJ(BDAGENT, R1) & atJ(BDAGENT, R2) & 
											connection(RID) & location(ALOC) & location(R1) & location(R2) & 
											blocked(R1, R2) & road(RID, R1, R2) & dangerZone(R1) & dangerZone(R2) & 
											not flooded(R1, R2)
										<- 	secure("militaryhq", R1);
											secure("militaryhq", R2);
											unblock(BDAGENT, RID, ALOC, R1).//unblock

@goUbAtEnd2End1Unsafe
+!goUnblock(BDAGENT, ALOC, RID, R1, R2): 	bulldozer(BDAGENT) & not busy(BDAGENT) & 
											atJ(BDAGENT, ALOC) & not atJ(BDAGENT, R1) & atJ(BDAGENT, R2) & 
											connection(RID) & location(ALOC) & location(R1) & location(R2) & 
											blocked(R1, R2) & road(RID, R1, R2) & dangerZone(R1) & not dangerZone(R2) & 
											not flooded(R1, R2)
										<- 	secure("militaryhq", R1);
											unblock(BDAGENT, RID, ALOC, R1).//unblock

@goUbAtEnd2End2Unsafe
+!goUnblock(BDAGENT, ALOC, RID, R1, R2): 	bulldozer(BDAGENT) & not busy(BDAGENT) & 
											atJ(BDAGENT, ALOC) & not atJ(BDAGENT, R1) & atJ(BDAGENT, R2) & 
											connection(RID) & location(ALOC) & location(R1) & location(R2) & 
											blocked(R1, R2) & road(RID, R1, R2) & not dangerZone(R1) & dangerZone(R2) & 
											not flooded(R1, R2)
										<- 	secure("militaryhq", R2);
											unblock(BDAGENT, RID, ALOC, R1).//unblock


@goUbAtEnd2BothSafe
+!goUnblock(BDAGENT, ALOC, RID, R1, R2): 	bulldozer(BDAGENT) & not busy(BDAGENT) & 
											atJ(BDAGENT, ALOC) & not atJ(BDAGENT, R1) & atJ(BDAGENT, R2) & 
											connection(RID) & location(ALOC) & location(R1) & location(R2) & 
											blocked(R1, R2) & road(RID, R1, R2) & not dangerZone(R1) & not dangerZone(R2) & 
											not flooded(R1, R2)
										<- 	unblock(BDAGENT, RID, ALOC, R1).//unblock
					
/*
 * Unblock onR
 */
 @goUbMoveToEndOkOnR
+!goUnblock(BDAGENT, ALOC, RID, R1, R2): 	bulldozer(BDAGENT) & not busy(BDAGENT) & 
											onR(BDAGENT, ALOC) & not atJ(BDAGENT, R1) & not atJ(BDAGENT, R2) & 
											connection(RID) & connection(ALOC) & location(R1) & location(R2) & 
											blocked(R1, R2) & road(RID, R1, R2) & not dangerZone(R2) & 
											not flooded(R1, R2)
										<- 	!moveAndSecure(BDAGENT, ALOC, R1);//move to R1 from current loc; secure LOC if required
											unblock(BDAGENT, RID, R1, R2).//unblock
@goUbMoveToEndUnsafeOnR		
+!goUnblock(BDAGENT, ALOC, RID, R1, R2): 	bulldozer(BDAGENT) & not busy(BDAGENT) & 
											onR(BDAGENT, ALOC) & not atJ(BDAGENT, R1) & not atJ(BDAGENT, R2) & 
											connection(RID) & connection(ALOC) & location(R1) & location(R2) & 
											blocked(R1, R2) & road(RID, R1, R2) & dangerZone(R2) & 
											not flooded(R1, R2)
										<- 	!moveAndSecure(BDAGENT, ALOC, R1);//move to R1 from current loc; secure LOC if required
											secure("militaryhq", R2); //secure other end
											unblock(BDAGENT, RID, R1, R2).//unblock			
/*
Cover the non-necessary-any-more case
*/
@unblockNotNeeded1
+!goUnblock(BDAGENT, ALOC, RID, R1, R2):	bulldozer(BDAGENT) & atJ(BDAGENT, ALOC) & road(RID, R1, R2) & not blocked(R1, R2)
										<-	.print("No-op 1").
				
@unblockNotNeeded2
+!goUnblock(BDAGENT, ALOC, RID, R1, R2):	bulldozer(BDAGENT) & not atJ(BDAGENT, ALOC) & road(RID, R1, R2) & not blocked(R1, R2)
										<-	.print("No-op 2").
						
/*
This is a bit of a hack, as want to stimulate goals only under certain conditions BUT need to integrate some checks for whether there are free, capable agents
*/

//respond to new demand... creates a desire
@addCargoNeedGoalToAirport//[atomic]
+!supply(J):	cargoNeeded(J) & location(J)
					<-	.print("Generating delivery goal for ", J);
						common.ia.findNearestCargo(C, K, J);
						common.ia.findNearestTruckOrHeli(A, K, J);
						.print("cargo /v Id & setup; A=",A," C=", C," K=", K," J=", J);
						!moveCargo(A, LOC, C, K, J);
						consume("logisticshq", J, C). 
						
//we have this to avoid failure response for the subgoal check above
+!supply(J):	not cargoNeeded(J)
					<-	.print("No cargo needed at ", J, "? - ending in confusion").

-!cargoNeededGoal(J):	 true
					<-	.print("Not adding goal", J ,",", I).

//TODO handle overJ case... land!
@lhqSecureAlreadyInPos
+!moveAndSecure(A, O, K):	dangerZone(O) & dangerZone(K) & atJ(A,O) & atJ(A, K)
						<-	secure("militaryhq", O).

@lhQMoveAgent//[atomic]
+!moveAndSecure(A, O, K):	not dangerZone(O) & not dangerZone(K) & atJ(A,O) & not atJ(A, K)
						<-	moveTo(A, O, K).
						
@lhQMoveAgentDzBoth//[atomic]
+!moveAndSecure(A, O, K):	dangerZone(O) & dangerZone(K) & atJ(A,O)  & not atJ(A, K)
						<-	secure("militaryhq", O);
							secure("militaryhq", K);
							moveTo(A, O, K).
							
@lhQMoveAgentDzSt//[atomic]
+!moveAndSecure(A, O, K):	dangerZone(O) & not dangerZone(K) & atJ(A,O)  & not atJ(A, K)	
						<-	secure("militaryhq", O);
							moveTo(A, O, K).
@lhQMoveAgentDzEnd//[atomic]
+!moveAndSecure(A, O, K):	not dangerZone(O) & dangerZone(K) & atJ(A,O)  & not atJ(A, K)
						<-	secure("militaryhq", K);
							moveTo(A, O, K).

/*
 * Truck onR moveandSecure case
 */				
//A is on road...
@lhQMoveAgentFromRoad//[atomic]
+!moveAndSecure(A, O, K):	not dangerZone(K) & onR(A,O) & not atJ(A, K)
						<-	moveTo(A, O, K).
						
@lhQMoveAgentFromRoadSecureEnde//[atomic]
+!moveAndSecure(A, O, K):	dangerZone(K) & onR(A,O)  & not atJ(A, K)
						<-	secure("militaryhq", K);
							moveTo(A, O, K).

/*
 * Helicopter flying (oveRJ) move and secure case
 * 
 * only need to secure K (destination) if necessary
 * then move...
 */
 @lhqSecureAlreadyInPosHeli
+!moveAndSecure(A, O, K):	dangerZone(K) & flying(A) & overJ(A, O) & airport(K)
						<-	secure("militaryhq", K);
							moveTo(A, O, K). //should land as a consequence...

@lhQMoveHeliSecured//[atomic]
+!moveAndSecure(A, O, K):	not dangerZone(K) & flying(A) & overJ(A, O) & airport(K)
						<-	moveTo(A, O, K).
												
												
/*
 * Brokerage plans for securing dzs
 */				
@secureForward//[atomic]		
+!secure(A, K):	 dangerZone(K) <- secure("militaryhq", K).

@secureNoop//[atomic]		
+!secure(A, K):	 not dangerZone(K) <- .print("Noop for secure ", A, ",", K).

//allow use of heli for overJ case...
//performs a move cargo action, then 'releases' the cargo 
@moveCargo //args; agent, agent loc, cargo name, cargo origin, cargo destination
+!moveCargo(A, O, C, K, J):	not busy(A) & cargoAt(C, K) & atJ(A, O) & not atJ(A, K) & 
							location(O) & location(K) & location(J) & cargo(C)
					<-	!moveAndSecure(A, O, K);
						load(A, K, C);
						!moveAndSecure(A, K, J);
						unload(A, J, C).
						
@moveCargoAtDest //args; agent, agent loc, cargo name, cargo origin, cargo destination
+!moveCargo(A, O, C, K, J):	not busy(A) & cargoAt(C, K) & atJ(A, O) & atJ(A, K) & 
							location(O) & location(K) & location(J) & cargo(C) & not dangerZone(K)
					<-	load(A, K, C);
						!moveAndSecure(A, K, J);
						unload(A, J, C).
										
@secureThenMoveCargoAtDest //args; agent, agent loc, cargo name, cargo origin, cargo destination
+!moveCargo(A, O, C, K, J):	not busy(A) & cargoAt(C, K) & atJ(A, O) & atJ(A, K) & 
							location(O) & location(K) & location(J) & cargo(C) & dangerZone(K)
					<-	secure("militaryhq", K);
						load(A, K, C);
						!moveAndSecure(A, K, J);
						unload(A, J, C).

@moveCargoOnR //args; agent, agent loc, cargo name, cargo origin, cargo destination
+!moveCargo(A, O, C, K, J):	not busy(A) & cargoAt(C, K) & onR(A, O) & not atJ(A, K) & 
							connection(O) & location(K) & location(J) & cargo(C)
					<-	!moveAndSecure(A, O, K);
						load(A, K, C);
						!moveAndSecure(A, K, J);
						unload(A, J, C).
						
/*
 * Special case plans for where helicopter (A) is flying over junction O at start
 */
@heliMoveCargoOverJ
+!moveCargo(A, O, C, K, J):	not busy(A) & cargoAt(C, K) & overJ(A, O) & not atJ(A, O) & not atJ(A, K) & flying(A) &
							location(O) & location(K) & location(J) & cargo(C) & airport(K)
					<-	!moveAndSecure(A, O, K);
						load(A, K, C);
						!moveAndSecure(A, K, J);
						unload(A, J, C).
						
@heliMoveCargoAtDest//[atomic] //args; agent, agent loc, cargo name, cargo origin, cargo destination
+!moveCargo(A, O, C, K, J):	not busy(A) & cargoAt(C, K) & overJ(A, O) & overJ(A, K) & flying(A) & 
							location(O) & location(K) & location(J) & cargo(C) & not dangerZone(K) &
							airport(K)
					<-	load(A, K, C);
						!moveAndSecure(A, K, J);
						unload(A, J, C).
										
@heliSecureThenMoveCargoAtDest//[atomic] //args; agent, agent loc, cargo name, cargo origin, cargo destination
+!moveCargo(A, O, C, K, J):	not busy(A) & cargoAt(C, K) & overJ(A, O) & overJ(A, K) & flying(A) & 
							location(O) & location(K) & location(J) & cargo(C) & dangerZone(K) &
							airport(K)
					<-	secure("militaryhq", K);
						load(A, K, C);
						!moveAndSecure(A, K, J);
						unload(A, J, C).
						
//unstick an agent A on road R / R1, R2...
@stuckFreeUnblock
+!unstick(ME, A, R, R1, R2): 	not mortal(A) & not resting(A) & road(R, R1, R2) & not flooded(R1, R2) &
								stuck(A) & onR(A, R) & blocked(R1, R2) & not toxic(R1, R2)
		<- !unblock(A, R, R1, R2); free(A, R, R1, R2).
		
@stuckFreeDeconTarmac
+!unstick(ME, A, R, R1, R2): 	not mortal(A) & not resting(A) & road(R, R1, R2) & not flooded(R1, R2) & tarmac(R1, R2) &
								stuck(A) & onR(A, R) & not blocked(R1, R2) & toxic(R1, R2)
		<- !decontaminateRoad("logisticshq", R, R1, R2); free(A, R, R1, R2).
		
@stuckFreeDeconMudc
+!unstick(ME, A, R, R1, R2): 	not mortal(A) & not resting(A) & road(R, R1, R2) & dry(R1, R2) & mud(R1, R2) &
								stuck(A) & onR(A, R) & not blocked(R1, R2) & toxic(R1, R2)
		<- !decontaminateRoad("logisticshq", R, R1, R2); free(A, R, R1, R2).
		
@stuckAllGood
+!unstick(ME, A, R, R1, R2): 	not mortal(A) & not resting(A) & road(R, R1, R2) & stuck(A) & onR(A, R) & not blocked(R1, R2) & not toxic(R1, R2)
		<- free(A, R, R1, R2).	