package agent.type;

import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.Vector;

import org.junit.Test;

public class ContractFormerAgentTest {
	
	@Test
	public void shouldReturnTrueForTwoEmptySets() {
		Collection<String> changes1 = new Vector<>();
		Collection<String> changes2 = new Vector<>();
		assertTrue(ContractFormerAgent.changesEqual(changes1, changes2));
	}


	@Test
	public void shouldReturnFalseForDifferentSizeSets() {
		Collection<String> changes1 = new Vector<>();
		Collection<String> changes2 = new Vector<>();
		changes2.add("+thingy");
		assertTrue(!ContractFormerAgent.changesEqual(changes1, changes2));
	}
	
	@Test
	public void shouldReturnTrueForTwoEqualSets() {
		Collection<String> changes1 = new Vector<>();
		changes1.add("+thingy1");
		changes1.add("+thingy2");
		changes1.add("+thingy3");
		Collection<String> changes2 = new Vector<>();
		changes2.add("+thingy1");
		changes2.add("+thingy2");
		changes2.add("+thingy3");
		assertTrue(ContractFormerAgent.changesEqual(changes1, changes2));
	}
}
