package agent;

import static org.junit.Assert.assertTrue;

import java.util.LinkedList;

import org.junit.Test;

import jason.asSemantics.IntendedMeans;
import jason.asSemantics.Intention;
import jason.asSemantics.Option;
import jason.asSemantics.Unifier;
import jason.asSyntax.ASSyntax;
import jason.asSyntax.Literal;
import jason.asSyntax.Plan;
import jason.asSyntax.Trigger;
import jason.asSyntax.Trigger.TEOperator;
import jason.asSyntax.Trigger.TEType;
import jason.asSyntax.parser.ParseException;

public class AgentUtilsTest {

	/*
	 * @defaultUnloadOnR
-unload(AGENT, DEST, C):	onR(AGENT, R) & not busy(AGENT)
						<- 	move(AGENT, N, C);
							unload(AGENT, N, C).
	 */
	@Test
	public void extractOrderedLeafActivityListShouldReturnOrderedIntentionList() throws ParseException {
		final Intention i = new Intention();
		Plan p = ASSyntax.parsePlan("@defaultUnloadOnR -unload(AGENT, DEST, C):	onR(AGENT, R) & not busy(AGENT)	<- 	move(AGENT, N, C); unload(AGENT, N, C).");
		i.push(new IntendedMeans(new Option(p, new Unifier()), new Trigger(TEOperator.add, TEType.achieve, ASSyntax.parseLiteral("unload"))));
		LinkedList<Literal> outcome = AgentUtils.extractOrderedLeafActivityList(i);
		assertTrue(outcome.size()==2);
		assertTrue(outcome.getFirst().toString().equals("move(AGENT,N,C)"));
		assertTrue(outcome.get(1).toString().equals("unload(AGENT,N,C)"));
	}
	
	@Test
	public void extractOrderedLeafActivityListShouldReturnOrderedIntentionListForMultiIms() throws ParseException {
		final Intention i = new Intention();
		Plan p1 = ASSyntax.parsePlan("@defaultUnloadOnR -unload(AGENT, DEST, C):	onR(AGENT, R) & not busy(AGENT)	<- 	move(AGENT, N, C); unload(AGENT, N, C).");
		Plan p2 = ASSyntax.parsePlan("+move(AGENT, DEST, C):	onR(AGENT, R) & not busy(AGENT)	<- 	thing1(AGENT, N, C); thing2(AGENT, N, C).");
		i.push(new IntendedMeans(new Option(p1, new Unifier()), new Trigger(TEOperator.add, TEType.achieve, ASSyntax.parseLiteral("unload"))));
		i.push(new IntendedMeans(new Option(p2, new Unifier()), new Trigger(TEOperator.add, TEType.achieve, ASSyntax.parseLiteral("move"))));
		LinkedList<Literal> outcome = AgentUtils.extractOrderedLeafActivityList(i);
		System.out.println(outcome);
		assertTrue(outcome.size()==3);
		assertTrue(outcome.get(0).toString().equals("thing1(AGENT,N,C)"));
		assertTrue(outcome.get(1).toString().equals("thing2(AGENT,N,C)"));
		assertTrue(outcome.get(2).toString().equals("unload(AGENT,N,C)"));
	}
	@Test
	public void extractOrderedLeafActivityListShouldReturnOrderedIntentionListForLongerMultiIms() throws ParseException {
		final Intention i = new Intention();
		Plan p1 = ASSyntax.parsePlan("@defaultUnloadOnR -unload(AGENT, DEST, C):	onR(AGENT, R) & not busy(AGENT)	<- 	move(AGENT, N, C); unload(AGENT, N, C).");
		Plan p2 = ASSyntax.parsePlan("+move(AGENT, DEST, C):	onR(AGENT, R) & not busy(AGENT)	<- 	thing1(AGENT, N, C); thing2(AGENT, N, C).");
		Plan p3 = ASSyntax.parsePlan("+thing1(AGENT, DEST, C):	onR(AGENT, R) & not busy(AGENT)	<- 	sub1(AGENT, N, C); sub2(AGENT, N, C).");
		i.push(new IntendedMeans(new Option(p1, new Unifier()), new Trigger(TEOperator.add, TEType.achieve, ASSyntax.parseLiteral("unload"))));
		i.push(new IntendedMeans(new Option(p2, new Unifier()), new Trigger(TEOperator.add, TEType.achieve, ASSyntax.parseLiteral("move"))));
		i.push(new IntendedMeans(new Option(p3, new Unifier()), new Trigger(TEOperator.add, TEType.achieve, ASSyntax.parseLiteral("thing1"))));
		System.out.println(i);
		LinkedList<Literal> outcome = AgentUtils.extractOrderedLeafActivityList(i);
		System.out.println(outcome);
		assertTrue(outcome.size()==4);
		assertTrue(outcome.get(0).toString().equals("sub1(AGENT,N,C)"));
		assertTrue(outcome.get(1).toString().equals("sub2(AGENT,N,C)"));
		assertTrue(outcome.get(2).toString().equals("thing2(AGENT,N,C)"));
		assertTrue(outcome.get(3).toString().equals("unload(AGENT,N,C)"));
	}
	@Test
	public void extractOrderedLeafActivityListShouldReturEmptyListForEmptyIntention() throws ParseException {
		final Intention i = new Intention();
		System.out.println(i);
		LinkedList<Literal> outcome = AgentUtils.extractOrderedLeafActivityList(i);
		System.out.println(outcome);
		assertTrue(outcome.isEmpty());
	}
}
