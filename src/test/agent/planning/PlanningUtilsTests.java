package agent.planning;

import static org.junit.Assert.assertTrue;
import jason.asSyntax.Literal;
import jason.bb.BeliefBase;
import jason.bb.DefaultBeliefBase;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import vehicle.capabilities.truck.Load;
import vehicle.capabilities.truck.MoveRoad;
import vehicle.capabilities.truck.Unload;
import agent.model.capability.Capability;

public class PlanningUtilsTests {
	private ArrayList<String> effectSet;
	private ArrayList<Literal> goalSet;
	private Capability mockMove, mockLoad, mockUnload;
	
	@Before
	public void setUp() throws Exception {
		effectSet = new ArrayList<>();
		goalSet = new ArrayList<>();
		goalSet.add(Literal.parseLiteral("state1(yaldy)"));
		goalSet.add(Literal.parseLiteral("state2(blah)"));
		goalSet.add(Literal.parseLiteral("state3(blah,blah)"));
		goalSet.add(Literal.parseLiteral("state4(blugh)"));
		mockMove = Mockito.mock(MoveRoad.class);
		Mockito.when(mockMove.getAddEffects()).thenReturn(
				new String[] {"atJ(AGENT, J)"});
		Mockito.when(mockMove.getDeleteEffects()).thenReturn(
				new String[] {"onR(AGENT, RID)", "atJ(AGENT, O)"});
		mockLoad = Mockito.mock(Load.class);
		Mockito.when(mockLoad.getAddEffects()).thenReturn(
				new String[] {"loaded(AGENT, C)", "carryingCargo(AGENT)"});
		Mockito.when(mockLoad.getDeleteEffects()).thenReturn(
				new String[] {"cargoAt(C,J)"});
		mockUnload = Mockito.mock(Unload.class);
		Mockito.when(mockUnload.getAddEffects()).thenReturn(
				new String[] {"cargoAt(C,J)"});
		Mockito.when(mockUnload.getDeleteEffects()).thenReturn(
				new String[] {"loaded(AGENT, C)", "carryingCargo(AGENT)"});
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void shouldReturnCannotAchieveGoalIfNoLiteralsInEffSet()	{
		effectSet.clear();
		effectSet.add("state1");
		effectSet.add("state2");
		effectSet.add("state3");
		assertTrue(PlanningUtils.cannotAchieve(goalSet, effectSet));
	}

	@Test
	public void shouldReturnCanPotentiallyAchieveGoalIfLiteralsInEffSet()	{
		effectSet.clear();
		effectSet.add("state1");
		effectSet.add("state2");
		effectSet.add("state3");
		effectSet.add("state4");
		effectSet.add("state5");
		assertTrue(!PlanningUtils.cannotAchieve(goalSet, effectSet));
	}
	
	@Test
	public void shouldReturnCorrectAchievableLiteralsForCapability()	{
		Collection<Capability> caps = new ArrayList<>();
		caps.add(mockMove);
		Collection<String> returned = PlanningUtils.getAchievableEffectNames(caps);
		assertTrue(returned.contains("atJ"));
		assertTrue(returned.contains("onR"));
		assertTrue(!returned.contains("mortal"));
	}
	
	@Test
	public void shouldReturnCorrectAchievableLiteralsForMultipleCapabilities()	{
		Collection<Capability> caps = new ArrayList<>();
		caps.add(mockMove);
		caps.add(mockUnload);
		caps.add(mockLoad);
		
		Collection<String> returned = PlanningUtils.getAchievableEffectNames(caps);
		assertTrue(returned.contains("atJ"));
		assertTrue(returned.contains("onR"));
		assertTrue(returned.contains("cargoAt"));
		assertTrue(returned.contains("loaded"));
		assertTrue(returned.contains("carryingCargo"));
		assertTrue(!returned.contains("mortal"));
	}
	
	@Test
	public void shouldReturnNoAchievableLiteralsForNoCapability()	{
		Collection<Capability> caps = new ArrayList<>();
		Collection<String> returned = PlanningUtils.getAchievableEffectNames(caps);
		assertTrue(returned.isEmpty());
	}
	
	@Test
	public void shouldBeAbleToAchieveMoveGoalWithMoveCapAndEmptyBB()	{
		final List<List<Literal>> goals = new ArrayList<>();
		final BeliefBase bb = new DefaultBeliefBase();
		
		//goal one; can't do
		final List<Literal> goalOne = new ArrayList<>();
		goalOne.add(Literal.parseLiteral("thingy(thing)"));
		goals.add(goalOne);
		
		//goal two; is possible
		final List<Literal> goalTwo = new ArrayList<>();
		goalTwo.add(Literal.parseLiteral("atJ(location1)"));
		goalTwo.add(Literal.parseLiteral("~onR(location1)"));
		goals.add(goalTwo);
		
		final Collection<Capability> caps = new ArrayList<>();
		caps.add(mockMove);
		caps.add(mockLoad);
		caps.add(mockUnload);
		boolean result = PlanningUtils.isGoalUnachievable(goals, caps, bb);
		assertTrue(!result);
	}
	

	@Test
	public void shouldBeAbleToAchieveMoveGoalWithMoveCapAndUnhandledStatesInBB()	{
		final List<List<Literal>> goals = new ArrayList<>();
		final BeliefBase bb = Mockito.mock(BeliefBase.class);
		final Literal locationLit = Literal.parseLiteral("location(Glasgow)");
		final Literal danger = Literal.parseLiteral("dangerZone(Glasgow)");
		Mockito.when(bb.contains(locationLit)).thenReturn(locationLit);
		Mockito.when(bb.contains(danger)).thenReturn(danger);
		
		//goal one; can't do
		final List<Literal> goalOne = new ArrayList<>();
		goalOne.add(Literal.parseLiteral("thingy(thing)"));
		goals.add(goalOne);
		
		//goal two; is possible
		final List<Literal> goalTwo = new ArrayList<>();
		goalTwo.add(Literal.parseLiteral("atJ(location1)"));
		goalTwo.add(Literal.parseLiteral("~onR(location1)"));
		goalTwo.add(Literal.parseLiteral("location(Glasgow)"));
		goalTwo.add(Literal.parseLiteral("dangerZone(Glasgow)"));
		goals.add(goalTwo);
		
		final Collection<Capability> caps = new ArrayList<>();
		caps.add(mockMove);
		caps.add(mockLoad);
		caps.add(mockUnload);
		boolean result = PlanningUtils.isGoalUnachievable(goals, caps, bb);
		assertTrue(!result);
	}
	
	@Test
	public void shouldNotBeAbleToAchieveMoveGoalWithoutMoveCapAndEmptyBB()	{
		final List<List<Literal>> goals = new ArrayList<>();
		final BeliefBase bb = new DefaultBeliefBase();
		//goal one; can't do
		final List<Literal> goalOne = new ArrayList<>();
		goalOne.add(Literal.parseLiteral("thingy(thing)"));
		goals.add(goalOne);
		
		//goal two; is possible
		final List<Literal> goalTwo = new ArrayList<>();
		goalTwo.add(Literal.parseLiteral("atJ(location1)"));
		goalTwo.add(Literal.parseLiteral("~onR(location1)"));
		goals.add(goalTwo);
		
		final Collection<Capability> caps = new ArrayList<>();
		caps.add(mockLoad);
		caps.add(mockUnload);
		boolean result = PlanningUtils.isGoalUnachievable(goals, caps, bb);
		assertTrue(result);
	}
	
	@Test
	public void shouldNotBeAbleToAchieveMoveGoalWithoutMoveCapAndNonEmptyBB()	{
		final List<List<Literal>> goals = new ArrayList<>();
		final BeliefBase bb = Mockito.mock(BeliefBase.class);
		//final Literal locationLit = Literal.parseLiteral("location(Glasgow)");
		final Literal danger = Literal.parseLiteral("dangerZone(Glasgow)");
		Mockito.when(bb.contains(danger)).thenReturn(danger);
		Mockito.when(bb.contains(Mockito.any(Literal.class))).thenReturn(null);
		
		//goal one; can't do
		final List<Literal> goalOne = new ArrayList<>();
		goalOne.add(Literal.parseLiteral("thingy(thing)"));
		goals.add(goalOne);
		
		//goal two; is possible
		final List<Literal> goalTwo = new ArrayList<>();
		goalTwo.add(Literal.parseLiteral("atJ(location1)"));
		goalTwo.add(Literal.parseLiteral("~onR(location1)"));
		goalTwo.add(Literal.parseLiteral("location(Glasgow)"));
		goalTwo.add(Literal.parseLiteral("dangerZone(Glasgow)"));
		goals.add(goalTwo);
		
		final Collection<Capability> caps = new ArrayList<>();
		caps.add(mockMove);
		caps.add(mockLoad);
		caps.add(mockUnload);
		boolean result = PlanningUtils.isGoalUnachievable(goals, caps, bb);
		assertTrue(result);
	}
	
	@Test
	public void shouldOnlyReturnStatesNotInBBForRemoveExistingStates()	{
		final List<Literal> goals = new ArrayList<>();
		final BeliefBase beliefs = Mockito.mock(BeliefBase.class);
		final Literal locationLit = Literal.parseLiteral("location(Glasgow)");
		final Literal danger = Literal.parseLiteral("dangerZone(Glasgow)");
		final Literal nonExist = Literal.parseLiteral("iAmNotInBB(Glasgow)");
		goals.add(Literal.parseLiteral("location(Glasgow)"));
		goals.add(Literal.parseLiteral("dangerZone(Glasgow)"));
		goals.add(Literal.parseLiteral("iAmNotInBB(Glasgow)"));
		Mockito.when(beliefs.contains(locationLit)).thenReturn(locationLit);
		Mockito.when(beliefs.contains(danger)).thenReturn(danger);
		Mockito.when(beliefs.contains(nonExist)).thenReturn(null);
		
		
		List<Literal> returned = 
				PlanningUtils.removeExistingStatesFromList(goals, beliefs );
		assertTrue(returned.size()==1);
		assertTrue(returned.contains(nonExist));
		assertTrue(!returned.contains(locationLit));
		assertTrue(!returned.contains(danger));
	}
	
}
