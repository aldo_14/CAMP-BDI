/**
 * 
 */
package truckworld.test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.Random;

import javax.swing.JDialog;

import org.junit.Before;
import org.junit.Test;

import common.Utils;


/**
 * @author Alawhite
 *
 */
public class LineUtilsTestSuite {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * Randonly generated points...
	 */
	@Test
	public void testContainsDiagonalLine() {
		Random random = new Random(1000l);
		
		for(int i=0; i<100; i++){
			int m = i;
			int c = i;
			int x1 = random.nextInt(100);
			int x2 = random.nextInt(100);
			while(x1==x2){	x2 = random.nextInt(100);	}
			double y1 = (m*x1) + c;
			double y2 = (m*x2) + c;
			Line2D test1 = new Line2D.Double(x1, y1, x2, y2);
			
			x2 = random.nextInt(100);
			y2 = (m*x2) + c;
			//get a line with a start within the test1 comparator line
			while((test1.ptSegDist(x2, y2)>0) || (x1==x2)){	
				x2 = random.nextInt(100);	
				y2 = (m*x2) + c;
			}
			Line2D test2 = new Line2D.Double(x1, y1, x2, y2);
			
			boolean result = !Utils.containsLine(test1, test2);
			
			
			if(result)	{
				JDialog jd = new JDialog();
				jd.setSize(500,500);
				LineDrawContainer ldc = new LineDrawContainer(test1, test2);
				jd.setContentPane(ldc);
				jd.setVisible(true);
				while(jd.isVisible()){
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				System.out.println("(" + i + ") Fail; " + test1.getP1() + test1.getP2() + ", and " + test2.getP1() + test2.getP2() );
				fail("(" + i + ") Failure; " + test1.getP1() + test1.getP2() + ", and " + test2.getP1() + test2.getP2() );
			}
			else{
				System.out.println("(" + i + ") Pass; " + test1.getP1() + test1.getP2() + ", and " + test2.getP1() + test2.getP2() );
			}
		}
		
		assertTrue(true);
	}//end test

	@Test
	public void testNotContainsDiagonalLine() {
		Random random = new Random(1000l);
		
		for(int i=0; i<100; i++){
			int m = i;
			int c = i;
			int x1 = random.nextInt(100);
			int x2 = random.nextInt(100);
			while(x1==x2){	x2 = random.nextInt(100);	}
			double y1 = (m*x1) + c;
			double y2 = (m*x2) + c;
			Line2D test1 = new Line2D.Double(x1, y1, x2, y2);
			
			m = i + 1; //i.e. change equation
			x2 = random.nextInt(100);
			y2 = (m*x2) + c;
			Line2D test2 = new Line2D.Double(x1, y1, x2, y2);
			
			boolean result = Utils.containsLine(test1, test2);
			
			
			if(result)	{
				JDialog jd = new JDialog();
				jd.setSize(500,500);
				LineDrawContainer ldc = new LineDrawContainer(test1, test2);
				jd.setContentPane(ldc);
				jd.setVisible(true);
				while(jd.isVisible()){
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				System.out.println("(" + i + ") Fail; " + test1.getP1() + test1.getP2() + ", and " + test2.getP1() + test2.getP2() );
				fail("(" + i + ") Failure; " + test1.getP1() + test1.getP2() + ", and " + test2.getP1() + test2.getP2() );
			}
			else{
				System.out.println("(" + i + ") Pass; " + test1.getP1() + test1.getP2() + ", and " + test2.getP1() + test2.getP2() );
			}
		}
		
		assertTrue(true);
	}//end test

	
	@Test
	public void testIntersection1() {
		/*
		 *  j2(65,35):j1(105,75)
		 *  j11(75, 25):j9(135, 15)
		 */
		Point2D j2 	= new Point2D.Double(65,35);
		Point2D j1 	= new Point2D.Double(105,75);
		Point2D j11 = new Point2D.Double(75, 25);
		Point2D j9	= new Point2D.Double(135, 15);
		Line2D l1 = new Line2D.Double(j2, j1);
		Line2D l2 = new Line2D.Double(j11, j9);
		

		JDialog jd = new JDialog();
		jd.setSize(500,500);
		LineDrawContainer ldc = new LineDrawContainer(l1, l2);
		jd.setTitle("Intersection test: j21j1 to j11j9");
		jd.setContentPane(ldc);
		jd.setVisible(true);
		while(jd.isVisible()){
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		assertTrue(!l1.intersectsLine(l2));//Utils.intersects(l1, l2));
	}
	
	@Test
	public void testIntersection2() {
		Point2D j3 	= new Point2D.Double(85,45);
		Point2D j11 	= new Point2D.Double(75,25);
		Point2D j2 	= new Point2D.Double(65,45);
		Point2D j1 	= new Point2D.Double(105,85);

		Line2D l1 = new Line2D.Double(j3, j11);
		Line2D l2 = new Line2D.Double(j2, j1);

		JDialog jd = new JDialog();
		jd.setSize(500,500);
		LineDrawContainer ldc = new LineDrawContainer(l1, l2);
		jd.setTitle("Intersection test: j3j11 to j2j1");
		jd.setContentPane(ldc);
		jd.setVisible(true);
		while(jd.isVisible()){
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		assertTrue(!l1.intersectsLine(l2));//Utils.intersects(l1, l2));
	}
	
	@Test
	public void testIntersection3() {
		Point2D j11 	= new Point2D.Double(75,25);
		Point2D j9 	= new Point2D.Double(135,25);
		Point2D j2 	= new Point2D.Double(65,45);
		Point2D j1 	= new Point2D.Double(105,85);

		Line2D l1 = new Line2D.Double(j9, j11);
		Line2D l2 = new Line2D.Double(j2, j1);

		JDialog jd = new JDialog();
		jd.setSize(500,500);
		LineDrawContainer ldc = new LineDrawContainer(l1, l2);
		jd.setTitle("Intersection test: j9j11 to j2j1");
		jd.setContentPane(ldc);
		jd.setVisible(true);
		while(jd.isVisible()){
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		assertTrue(!l1.intersectsLine(l2));//Utils.intersects(l1, l2));
	}
}
