/**
 * 
 */
package truckworld.test;

import truckworld.vehicle.type.Truck;
import truckworld.world.World;




/**
 * @author Alan White
 * Tests A* algorithm implementation using a variety of maps and vehicle implementations
 * @deprecated broken
 */
public class AStarTestSuite{
	@SuppressWarnings("unused")
	private World w;
	@SuppressWarnings("unused")
	private Truck goAnywhere, noWet;

	/**
	 * @throws java.lang.Exception
	 *
	@Before
	public void setUp() throws Exception {
		//vehicle type; no restrictions
		goAnywhere = new Truck("goAnywhere"){
			@Override
			public boolean canTravelAlong(RoadInf road) {
				return !road.isBlocked();
			}
		};
		
		//vehicle type; can't do slippery or wet
		noWet = new Truck("noWet"){
			@Override
			public boolean canTravelAlong(RoadInf road) {
				if(road.isSlippery())	{
					return false;
				}
				else if(road.isFlooded())	{
					return false;
				}
				return !road.isBlocked();
			}
		};

		w = new World();
	}
	
	@Test
	public void testIsolatedNode()	{
		//unconnected
		try	{
			System.out.println("==testIsolatedNode==");
			JunctionInf a = new Junction("a", new Point2D.Double(0,0));
			JunctionInf b = new Junction("b", new Point2D.Double(0,10));
			JunctionInf c = new Junction("c", new Point2D.Double(0,20));
			w.addJunction(a);
			w.addJunction(b);
			w.addJunction(c);
	
			RoadInf ab = new Road(UniversalConstants.RoadType.tarmac, a, b, false, false, false);
			//new Road(w, UniversalConstants.RoadType.tarmac, b, c, false, false, false);
			RoadInf ac = new Road(UniversalConstants.RoadType.tarmac, a, c, false, false, false);
			w.addRoad(ab);
			w.addRoad(ac);
			ab.setBlocked(true);
			ac.setBlocked(true);
			
			Stack<JunctionInf> route = Utils.getRoute(b, c, goAnywhere);
			
			System.out.println(w.getAllRoads() + ", a-c Route: " + route);
			
			assertTrue(route == null);
		}
		catch(Exception e){
			e.printStackTrace();
			fail(e.toString());
		}
	}
	
	@Test
	public void testLinearRoute() {
		try	{
			System.out.println("==testLinearRoute==");
			JunctionInf a = new Junction("a", new Point2D.Double(0,0));
			JunctionInf b = new Junction("b", new Point2D.Double(0,10));
			JunctionInf c = new Junction("c", new Point2D.Double(0,20));
			w.addJunction(a);
			w.addJunction(b);
			w.addJunction(c);
	
			new Road(UniversalConstants.RoadType.tarmac, a, b, false, false, false);
			new Road(UniversalConstants.RoadType.tarmac, b, c, false, false, false);
			
			Stack<JunctionInf> route = Utils.getRoute(a, c, goAnywhere);
			
			System.out.println("Route: " + route.toString());
			
			Stack<JunctionInf> expected = new Stack<JunctionInf>();
			expected.add(c); expected.add(b); expected.add(a);
			
			assertTrue(route.equals(expected));
		}
		catch(Exception e){
			e.printStackTrace();
			fail(e.toString());
		}
	}
	
	
	@Test
	public void testNoRoute() {
		try	{
			System.out.println("==testNoRoute==");
			JunctionInf a = new Junction("a", new Point2D.Double(0,0));
			JunctionInf b = new Junction("b", new Point2D.Double(0,10));
			JunctionInf c = new Junction("c", new Point2D.Double(0,20));
			w.addJunction(a);
			w.addJunction(b);
			w.addJunction(c);
	
			new Road(UniversalConstants.RoadType.tarmac, a, b, false, false, false);
			
			Stack<JunctionInf> route = Utils.getRoute(a, c, goAnywhere);
			assertTrue(route == null);
		}
		catch(Exception e){
			e.printStackTrace();
			fail(e.toString());
		}
	}
	
	@Test
	public void testSimpleTriangle() {
		try{
			System.out.println("==testSimpleTriangle==");
			JunctionInf a = new Junction("a", new Point2D.Double(0,0));
			JunctionInf b = new Junction("b", new Point2D.Double(10,10));
			JunctionInf c = new Junction("c", new Point2D.Double(0,20));
			w.addJunction(a);
			w.addJunction(b);
			w.addJunction(c);
	
			new Road(UniversalConstants.RoadType.tarmac, a, b, false, false, false);
			new Road(UniversalConstants.RoadType.tarmac, b, c, false, false, false);
			new Road(UniversalConstants.RoadType.tarmac, a, c, false, false, false);
			
			Stack<JunctionInf> route = Utils.getRoute(a, c, goAnywhere);
			
			System.out.println("Route: " + route.toString());
			
			Stack<JunctionInf> expected = new Stack<JunctionInf>();
			expected.add(c); expected.add(a);
			
			assertTrue(route.equals(expected));
		}
		catch(Exception e){
			e.printStackTrace();
			fail(e.toString());
		}
	}
	
	/**
	 * A diamond with a central connection; misses direct a-d link
	 *
	@Test
	public void testDiamondMesh() {
		try{
			System.out.println("==testDiamondMesh==");
			JunctionInf a = new Junction("a", new Point2D.Double(0,0));
			JunctionInf b = new Junction("b", new Point2D.Double(10,10));
			JunctionInf c = new Junction("c", new Point2D.Double(-20,10));
			JunctionInf d = new Junction("d", new Point2D.Double(0,20));
			w.addJunction(a);
			w.addJunction(b);
			w.addJunction(c);
			w.addJunction(d);
	
			new Road(UniversalConstants.RoadType.tarmac, a, b, false, false, false);
			new Road(UniversalConstants.RoadType.tarmac, b, c, false, false, false);
			new Road(UniversalConstants.RoadType.tarmac, a, c, false, false, false);
			new Road(UniversalConstants.RoadType.tarmac, b, d, false, false, false);
			new Road(UniversalConstants.RoadType.tarmac, c, d, false, false, false);
			
			Stack<JunctionInf> route = Utils.getRoute(a, d, goAnywhere);
			
			System.out.println("Route: " + route.toString());
			
			Stack<JunctionInf> expected = new Stack<JunctionInf>();
			expected.add(d); expected.add(b); expected.add(a);
			
			assertTrue(route.equals(expected));
		}
		catch(Exception e){
			e.printStackTrace();
			fail(e.toString());
		}
	}
	
	@Test
	public void testSimpleMeshWithTraversalConditions() {
		try{
			System.out.println("==testSimpleMeshWithTraversalConditions==");
			JunctionInf a = new Junction("a", new Point2D.Double(0,0));
			JunctionInf b = new Junction("b", new Point2D.Double(10,10));
			JunctionInf c = new Junction("c", new Point2D.Double(-20,10));
			JunctionInf d = new Junction("d", new Point2D.Double(0,20));
			w.addJunction(a);
			w.addJunction(b);
			w.addJunction(c);
			w.addJunction(d);
	
			new Road(UniversalConstants.RoadType.tarmac, a, b, false, false, false);
			new Road(UniversalConstants.RoadType.mud, b, c, false, false, false);
			new Road(UniversalConstants.RoadType.tarmac, a, c, false, false, false);
			RoadInf bd = new Road(UniversalConstants.RoadType.mud, b, d, false, false, false);
			new Road(UniversalConstants.RoadType.tarmac, c, d, false, false, false);
			bd.setCondition(RoadCondition.flooded);
			Stack<JunctionInf> route = Utils.getRoute(a, d, noWet);
			
			System.out.println("Route: " + route.toString());
			
			Stack<JunctionInf> expected = new Stack<JunctionInf>();
			expected.add(d); expected.add(c); expected.add(a);
			
			assertTrue(route.equals(expected));
		}
		catch(Exception e){
			e.printStackTrace();
			fail(e.toString());
		}
	}
	
	@Test
	public void testComplexMesh() {
		System.out.println("==testComplexMesh==");
		try{
			JunctionInf a = new Junction("a", new Point2D.Double(	10,		60	));
			JunctionInf b = new Junction("b", new Point2D.Double(	30,		40	));
			JunctionInf c = new Junction("c", new Point2D.Double(	25,	85	));
			JunctionInf d = new Junction("d", new Point2D.Double(	60,		60	));
			JunctionInf e = new Junction("e", new Point2D.Double(	55,		120	));
			JunctionInf f = new Junction("f", new Point2D.Double(	90,		70	));
			JunctionInf g = new Junction("g", new Point2D.Double(	130,	50	));
			JunctionInf h = new Junction("h", new Point2D.Double(	90,		20	));
			JunctionInf i = new Junction("i", new Point2D.Double(	110,	110	));
			JunctionInf j = new Junction("j", new Point2D.Double(	150,	120	));
			JunctionInf k = new Junction("k", new Point2D.Double(	180,	100	));

			w.addJunction(a);
			w.addJunction(b);
			w.addJunction(c);
			w.addJunction(d);
			w.addJunction(e);
			w.addJunction(f);
			w.addJunction(g);
			w.addJunction(h);
			w.addJunction(i);
			w.addJunction(j);
			w.addJunction(k);
	
			RoadInf ab = new Road(UniversalConstants.RoadType.tarmac, a, b, false, false, false);
			System.out.println(ab.toString());
			RoadInf ac = new Road( UniversalConstants.RoadType.tarmac, a, c, false, false, false);
			System.out.println(ac.toString());
			RoadInf ad = new Road(UniversalConstants.RoadType.tarmac, a, d, false, false, false);
			System.out.println(ad.toString());
			RoadInf ah = new Road(UniversalConstants.RoadType.tarmac, a, h, false, false, false);
			System.out.println(ah.toString());
			RoadInf bh = new Road(UniversalConstants.RoadType.tarmac, b, h, false, false, false);
			System.out.println(bh.toString());
			RoadInf cd = new Road(UniversalConstants.RoadType.tarmac, c, d, false, false, false);
			System.out.println(cd.toString());
			RoadInf ce = new Road(UniversalConstants.RoadType.tarmac, c, e, false, false, false);
			System.out.println(ce.toString());
			RoadInf dh = new Road(UniversalConstants.RoadType.tarmac, d, h, false, false, false);
			System.out.println(dh.toString());
			RoadInf df = new Road(UniversalConstants.RoadType.tarmac, d, f, false, false, false);
			System.out.println(df.toString());
			RoadInf de = new Road(UniversalConstants.RoadType.tarmac, d, e, false, false, false);
			System.out.println(de.toString());
			RoadInf ef = new Road(UniversalConstants.RoadType.tarmac, e, f, false, false, false);
			System.out.println(ef.toString());
			RoadInf ei = new Road(UniversalConstants.RoadType.tarmac, e, i, false, false, false);
			System.out.println(ei.toString());
			RoadInf fh = new Road(UniversalConstants.RoadType.tarmac, f, h, false, false, false);
			System.out.println(fh.toString());
			RoadInf fg = new Road(UniversalConstants.RoadType.tarmac, f, g, false, false, false);
			System.out.println(fg.toString());
			RoadInf fi = new Road(UniversalConstants.RoadType.tarmac, f, i, false, false, false);
			System.out.println(fi.toString());
			RoadInf gh = new Road(UniversalConstants.RoadType.tarmac, g, h, false, false, false);
			System.out.println(gh.toString());
			RoadInf gk = new Road(UniversalConstants.RoadType.tarmac, g, k, false, false, false);
			System.out.println(gk.toString());
			RoadInf ij = new Road(UniversalConstants.RoadType.tarmac, i, j, false, false, false);
			System.out.println(ij.toString());
			RoadInf ik = new Road(UniversalConstants.RoadType.tarmac, i, k, false, false, false);
			System.out.println(ik.toString());
			
			Stack<JunctionInf> rt1 = Utils.getRoute(a, k, this.goAnywhere);
			System.out.println("Route1: " + rt1);
			Stack<JunctionInf> rt2 = Utils.getRoute(d, k, this.goAnywhere);
			System.out.println("Route2: " + rt2);
			Stack<JunctionInf> rt3 = Utils.getRoute(e, j, this.goAnywhere);
			System.out.println("Route3: " + rt3);
			Stack<JunctionInf> rt4 = Utils.getRoute(g, a, this.goAnywhere);
			System.out.println("Route4: " + rt4);
			
			assertTrue(rt1!=null && rt2!=null && rt3!=null && rt4!=null);
		}
		catch(Exception e){
			e.printStackTrace();
			fail(e.toString());
		}
	}
	
	@Test
	public void testComplexMeshWithTraversalConditions1() {
		System.out.println("==testComplexMeshWithTraversalConditions1==");
		try{
			JunctionInf a = new Junction("a", new Point2D.Double(	10,		60	));
			JunctionInf b = new Junction("b", new Point2D.Double(	30,		40	));
			JunctionInf c = new Junction("c", new Point2D.Double(	25,	85	));
			JunctionInf d = new Junction("d", new Point2D.Double(	60,		60	));
			JunctionInf e = new Junction("e", new Point2D.Double(	55,		120	));
			JunctionInf f = new Junction("f", new Point2D.Double(	90,		70	));
			JunctionInf g = new Junction("g", new Point2D.Double(	130,	50	));
			JunctionInf h = new Junction("h", new Point2D.Double(	90,		20	));
			JunctionInf i = new Junction("i", new Point2D.Double(	110,	110	));
			JunctionInf j = new Junction("j", new Point2D.Double(	150,	120	));
			JunctionInf k = new Junction("k", new Point2D.Double(	180,	100	));

			w.addJunction(a);
			w.addJunction(b);
			w.addJunction(c);
			w.addJunction(d);
			w.addJunction(e);
			w.addJunction(f);
			w.addJunction(g);
			w.addJunction(h);
			w.addJunction(i);
			w.addJunction(j);
			w.addJunction(k);
			
			RoadInf ab = new Road(UniversalConstants.RoadType.tarmac, a, b, false, false, false);
			System.out.println(ab.toString());
			RoadInf ac = new Road(UniversalConstants.RoadType.tarmac, a, c, false, false, false);
			System.out.println(ac.toString());
			RoadInf ad = new Road(UniversalConstants.RoadType.tarmac, a, d, false, false, false);
			System.out.println(ad.toString());
			RoadInf ah = new Road(UniversalConstants.RoadType.tarmac, a, h, false, false, false);
			System.out.println(ah.toString());
			RoadInf bh = new Road(UniversalConstants.RoadType.tarmac, b, h, false, false, false);
			System.out.println(bh.toString());
			RoadInf cd = new Road(UniversalConstants.RoadType.tarmac, c, d, false, false, false);
			System.out.println(cd.toString());
			RoadInf ce = new Road(UniversalConstants.RoadType.tarmac, c, e, false, false, false);
			System.out.println(ce.toString());
			RoadInf dh = new Road(UniversalConstants.RoadType.tarmac, d, h, false, false, false);
			System.out.println(dh.toString());
			RoadInf df = new Road(UniversalConstants.RoadType.tarmac, d, f, false, false, false);
			System.out.println(df.toString());
			RoadInf de = new Road(UniversalConstants.RoadType.tarmac, d, e, false, false, false);
			System.out.println(de.toString());
			RoadInf ef = new Road(UniversalConstants.RoadType.tarmac, e, f, false, false, false);
			System.out.println(ef.toString());
			RoadInf ei = new Road(UniversalConstants.RoadType.tarmac, e, i, false, false, false);
			System.out.println(ei.toString());
			RoadInf fh = new Road(UniversalConstants.RoadType.tarmac, f, h, false, false, false);
			System.out.println(fh.toString());
			RoadInf fg = new Road(UniversalConstants.RoadType.tarmac, f, g, false, false, false);
			System.out.println(fg.toString());
			RoadInf fi = new Road(UniversalConstants.RoadType.tarmac, f, i, false, false, false);
			System.out.println(fi.toString());
			RoadInf gh = new Road(UniversalConstants.RoadType.tarmac, g, h, false, false, false);
			System.out.println(gh.toString());
			RoadInf gk = new Road(UniversalConstants.RoadType.tarmac, g, k, false, false, false);
			System.out.println(gk.toString());
			RoadInf ij = new Road(UniversalConstants.RoadType.tarmac, i, j, false, false, false);
			System.out.println(ij.toString());
			RoadInf ik = new Road(UniversalConstants.RoadType.tarmac, i, k, false, false, false);
			System.out.println(ik.toString());
			
			//test 1; slippery ij; j inaccessible to non slippery vehicles
			ij.setCondition(RoadCondition.slippery);
			Stack<JunctionInf> rt1 = Utils.getRoute(h, j, this.goAnywhere);
			System.out.println("Route1: " + rt1);
			Stack<JunctionInf> rt2 = Utils.getRoute(h, j, this.noWet);
			System.out.println("Route2: " + rt2);
			ij.setCondition(RoadCondition.dry);
			boolean test1 = (rt1!=null) && (rt2==null);
			assertTrue(test1);
		}
		catch(Exception e){
			e.printStackTrace();
			fail(e.toString());
		}
	}


	@Test
	public void testComplexMeshWithTraversalConditions2() {
		System.out.println("==testComplexMeshWithTraversalConditions2==");
		try{
			JunctionInf a = new Junction("a", new Point2D.Double(	10,		60	));
			JunctionInf b = new Junction("b", new Point2D.Double(	30,		40	));
			JunctionInf c = new Junction("c", new Point2D.Double(	25,	85	));
			JunctionInf d = new Junction("d", new Point2D.Double(	60,		60	));
			JunctionInf e = new Junction("e", new Point2D.Double(	55,		120	));
			JunctionInf f = new Junction("f", new Point2D.Double(	90,		70	));
			JunctionInf g = new Junction("g", new Point2D.Double(	130,	50	));
			JunctionInf h = new Junction("h", new Point2D.Double(	90,		20	));
			JunctionInf i = new Junction("i", new Point2D.Double(	110,	110	));
			JunctionInf j = new Junction("j", new Point2D.Double(	150,	120	));
			JunctionInf k = new Junction("k", new Point2D.Double(	180,	100	));

			w.addJunction(a);
			w.addJunction(b);
			w.addJunction(c);
			w.addJunction(d);
			w.addJunction(e);
			w.addJunction(f);
			w.addJunction(g);
			w.addJunction(h);
			w.addJunction(i);
			w.addJunction(j);
			w.addJunction(k);
			
			RoadInf ab = new Road(UniversalConstants.RoadType.tarmac, a, b, false, false, false);
			System.out.println(ab.toString());
			RoadInf ac = new Road(UniversalConstants.RoadType.tarmac, a, c, false, false, false);
			System.out.println(ac.toString());
			RoadInf ad = new Road(UniversalConstants.RoadType.tarmac, a, d, false, false, false);
			System.out.println(ad.toString());
			RoadInf ah = new Road(UniversalConstants.RoadType.tarmac, a, h, false, false, false);
			System.out.println(ah.toString());
			RoadInf bh = new Road(UniversalConstants.RoadType.tarmac, b, h, false, false, false);
			System.out.println(bh.toString());
			RoadInf cd = new Road(UniversalConstants.RoadType.tarmac, c, d, false, false, false);
			System.out.println(cd.toString());
			RoadInf ce = new Road(UniversalConstants.RoadType.tarmac, c, e, false, false, false);
			System.out.println(ce.toString());
			RoadInf dh = new Road(UniversalConstants.RoadType.tarmac, d, h, false, false, false);
			System.out.println(dh.toString());
			RoadInf df = new Road(UniversalConstants.RoadType.tarmac, d, f, false, false, false);
			System.out.println(df.toString());
			RoadInf de = new Road(UniversalConstants.RoadType.tarmac, d, e, false, false, false);
			System.out.println(de.toString());
			RoadInf ef = new Road(UniversalConstants.RoadType.tarmac, e, f, false, false, false);
			System.out.println(ef.toString());
			RoadInf ei = new Road(UniversalConstants.RoadType.tarmac, e, i, false, false, false);
			System.out.println(ei.toString());
			RoadInf fh = new Road(UniversalConstants.RoadType.tarmac, f, h, false, false, false);
			System.out.println(fh.toString());
			RoadInf fg = new Road(UniversalConstants.RoadType.tarmac, f, g, false, false, false);
			System.out.println(fg.toString());
			RoadInf fi = new Road(UniversalConstants.RoadType.tarmac, f, i, false, false, false);
			System.out.println(fi.toString());
			RoadInf gh = new Road(UniversalConstants.RoadType.tarmac, g, h, false, false, false);
			System.out.println(gh.toString());
			RoadInf gk = new Road(UniversalConstants.RoadType.tarmac, g, k, false, false, false);
			System.out.println(gk.toString());
			RoadInf ij = new Road(UniversalConstants.RoadType.tarmac, i, j, false, false, false);
			System.out.println(ij.toString());
			RoadInf ik = new Road(UniversalConstants.RoadType.tarmac, i, k, false, false, false);
			System.out.println(ik.toString());
			
			//test2; only one open road for a-h; MUST use GH
			bh.setCondition(RoadCondition.slippery);
			ah.setCondition(RoadCondition.slippery);
			dh.setCondition(RoadCondition.slippery);
			fh.setCondition(RoadCondition.slippery);
			Stack<JunctionInf> rt3 = Utils.getRoute(a, h, this.noWet);
			System.out.println("Route: " + rt3);
			Stack<JunctionInf> expected = new Stack<JunctionInf>();
			expected.add(h);
			expected.add(g);
			expected.add(f);
			expected.add(d);
			expected.add(a);
			boolean test2 = rt3.equals(expected);
			
			assertTrue(test2);
		}
		catch(Exception e){
			e.printStackTrace();
			fail(e.toString());
		}
	}
*/
}
