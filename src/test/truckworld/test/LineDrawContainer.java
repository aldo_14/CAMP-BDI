/**
 * 
 */
package truckworld.test;

import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;

/**
 * @author Alawhite
 *
 */
public class LineDrawContainer extends Container{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3632965938211556273L;
	private Line2D l1, l2;

	/**
	 * @param test1
	 * @param test2
	 */
	public LineDrawContainer(Line2D test1, Line2D test2) {
		super();
		this.l1 = test1;
		this.l2 = test2;
	}
	
	public void paint(Graphics g){
		Graphics2D g2 = (Graphics2D)g;
		g2.setPaint(Color.BLACK);
		g2.draw(l1);
		g2.setPaint(Color.red);
		g2.draw(l2);
		
	}

}
