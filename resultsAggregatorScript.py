import os, csv

msgStatsHeader = ""
planStatsHeader = ""
stepStatsHeader = ""

msgStats = {}
stepStats = {}
planStats = {}


def traverse(root):
    for dirName, subDirList, fileList in os.walk(root):
        print('Found directory %s' % dirName)
        if len(fileList) > 0:
            handleExperiments(root, dirName, fileList)
    summaryDir = root + '/'
    displayStepStats(summaryDir)
    displayPlanStats(summaryDir)
    displayMsgStats(summaryDir)

def displayMsgStats(summaryDir):
    f = open(summaryDir+'msgStatSummary.csv', 'w')
    global msgStats
    global msgStatsHeader
    msgKeys = sorted(msgStats.keys())
    f.write(msgStatsHeader)
    for key in msgKeys:
        lines  = msgStats[key].keys()
        for lineSet in lines:
            for entry in msgStats[key][lineSet]:
                f.write("\n" + key + ", " + entry);
            f.write('\nAverage\nst.dev\np-value\n')
    f.close()

def displayPlanStats(summaryDir):
    f = open(summaryDir+'planStatSummary.csv', 'w')
    global planStats
    global planStatsHeader
    planKeys = sorted(planStats.keys())
    f.write(planStatsHeader)
    for key in planKeys:
        lines  = planStats[key].keys()
        for lineSet in lines:
            for entry in planStats[key][lineSet]:
                f.write("\n" + key + ", " + entry);
            f.write('\nAverage\nst.dev\np-value\n')
    f.close()

def displayStepStats(summaryDir):
    f = open(summaryDir+'stepStatSummary.csv', 'w')
    global stepStats
    global stepStatsHeader
    stepKeys = sorted(stepStats.keys())
    f.write(stepStatsHeader)
    for key in stepKeys:
        lines  = stepStats[key].keys()
        for lineSet in lines:
            for entry in stepStats[key][lineSet]:
                f.write("\n" + key + ", " + entry);
            f.write('\nAverage\nst.dev\np-value\n')
    f.close()

def handleExperiments(root, dirName, fileList):
    config, system, experimentNumber = experimentInfo(dirName.split('/'))
    msgSummary = []
    planSummary = []
    stepSummary = []
    for file in fileList:
        msgSummaryEntry, planSummaryEntry, stepSummaryEntry = handle(system, experimentNumber, dirName, file)
        if(len(msgSummaryEntry)>0):
            msgSummary.append(system + msgSummaryEntry)
        if(len(planSummaryEntry)>0):
            planSummary.append(system + planSummaryEntry)
        if(len(stepSummaryEntry)>0):
            stepSummary.append(system + stepSummaryEntry)
        addToDictionary(config, system, stepSummaryEntry, planSummaryEntry, msgSummaryEntry)

def experimentInfo(split):
    experimentNumber = split[len(split)-1]
    system = split[len(split)-2]
    config = split[len(split)-3]
    return config, system, experimentNumber

def handle(system, experimentNumber, dirName, file):
    msgSummaryEntry = ""
    planSummaryEntry = ""
    stepSummaryEntry = ""
    if file == "msgStats.txt":
        msgSummaryEntry = handleMsgStats(system, experimentNumber, dirName + "/" + file)
    elif file == "planStats.txt":
        planSummaryEntry = handlePlanStats(system, experimentNumber, dirName + "/" + file)
    elif file == "stepStats.txt":
        stepSummaryEntry = handleStepStats(system, experimentNumber, dirName + "/" + file)
    return msgSummaryEntry, planSummaryEntry, stepSummaryEntry

def handleMsgStats(system, experimentNumber, file):
    global msgStatsHeader
    statsFile = open(str(file))
    msgStatsHeader = updateHeader(msgStatsHeader, statsFile)
    summary = lastLine(statsFile)
    statsFile.close()
    return system + ", " + experimentNumber + ", " + summary

def handlePlanStats(system, experimentNumber, file):
    global planStatsHeader
    statsFile = open(str(file))
    planStatsHeader = updateHeader(planStatsHeader, statsFile)
    summary = lastLine(statsFile)
    statsFile.close()
    return system + ", " + experimentNumber + ", " + summary

def handleStepStats(system, experimentNumber, file):
    global stepStatsHeader
    statsFile = open(str(file))
    stepStatsHeader = updateHeader(stepStatsHeader, statsFile)
    summary = lastLine(statsFile)
    statsFile.close()
    return system + ", " + experimentNumber + ", " + summary

def updateHeader(header, statsFile):
    if(len(header)==0):
        return "nDebil, System, Run, " + statsFile.readline().rstrip('\n')
    else:
        return header

def lastLine(file):
    for line in file:
        pass
    return line.rstrip('\n')

def addToDictionary(config, system, stepSummaryEntry, planSummaryEntry, msgSummaryEntry):
    global msgStats
    global stepStats
    global planStats
    if(len(msgSummaryEntry)>0):
        msgStatsForApproach = msgStats.get(config, {})
        msgStatsSet = msgStatsForApproach.get(system, [])
        msgStatsSet.append(msgSummaryEntry)
        msgStatsForApproach[system] = msgStatsSet
        msgStats[config] = msgStatsForApproach
    if(len(planSummaryEntry)>0):
        planStatsForApproach = planStats.get(config, {})
        planStatsSet = planStatsForApproach.get(system, [])
        planStatsSet.append(planSummaryEntry)
        planStatsForApproach[system] = planStatsSet
        planStats[config] = planStatsForApproach
    if(len(stepSummaryEntry)>0):
        stepStatsForApproach = stepStats.get(config, {})
        stepStatsSet = stepStatsForApproach.get(system, [])
        stepStatsSet.append(stepSummaryEntry)
        stepStatsForApproach[system] = stepStatsSet
        stepStats[config] = stepStatsForApproach


if __name__=="__main__":
    import sys
    traverse(sys.argv[1])