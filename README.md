This project includes the CAMP-BDI system, built upon Jason 1.39, and the Cargoworld simulator used for evaluation.  Please note the latter is tightly coupled to the former due to a shared dependency upon a logging singleton, so some decoupling is required.

Readme will be updated with further details in future; as this is experimental code and needs general improvements in test coverage and quality, a new project will be created to re-develop the Cargoworld simulator for more general usage.

Please also see http://www.aiai.ed.ac.uk/project/ix/project/white/ for the thesis evaluating CAMP-BDI as a robustness approach, based upon this implementation.

**NOTE**
**(2/9/21)**

I feel I should add a disclaimer here; this code is essentially not up to the standards I'd expect from myself.  There are many failures in terms of code quality, test coverage, structure, etc.  I make no excuses beyond the recognition it was written up in a flurry of train rides, coffee shops and very late night sessions, and to acknowledge the extent of work required to refactor and/or re-implement it.  I do have an intent to rebuild key sections (principally, the simulator aspect as this may be valuable to others) as or if time permits.  

Anyone who wishes to extend or adapt my work is regardless more than encouraged; I'd just ask that you look towards my thesis (https://era.ed.ac.uk/handle/1842/31465?show=full) rather than this codebase for ideas how to proceed.

Regardless, any direct questions can be addressed to my personal email alan_g_white@yahoo.co.uk.
