import sys
	
def main(argv):
	print 'main; readfile, writefile, paperworld, nrisk, ndebil, seed'
	readFile = sys.argv[1]
	writeFile = sys.argv[2]
	paperworld = sys.argv[3]
	nrisk = sys.argv[4]
	ndebil = sys.argv[5]
	seed = sys.argv[6]
	createFile(readFile, writeFile, paperworld, nrisk, ndebil, seed)
	print 'updated ' + readFile + ' and saved as ' + writeFile
	
#EDIT   environment: truckworld.env.Cargoworld("paperWorld1.properties", "nrisk3/ndebil05.properties", "9", "replan")
#args - read filename, write filename, __PAPERWORLD__, __NRISK__, __NDEBIL__, __SEED__
def createFile(readFile, writeFile, paperworld, nrisk, ndebil, seed):
	with open(readFile, "r") as read:
		write = open(writeFile + ".mas2j", "w+")
		for line in read:
			line = line.replace("__PAPERWORLD__", paperworld).replace("__NRISK__", nrisk).replace("__NDEBIL__", ndebil).replace("__SEED__", seed)
			write.write(line)
		write.close()
	read.close()
	
if __name__ == "__main__":
	main(sys.argv)